#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import subprocess

from jflow.workflow import Workflow
from ngspipelines.config_reader import NGSPipelinesConfigReader

class RunInstance (Workflow):
    
    def get_description(self):
        return "Run the specified instance"

    def define_parameters(self, function="process"):
        self.add_parameter("instance_name", "Which is the name of the instance", required=True, default="default")
        self.add_parameter("command", "Exectue another command than run [restart]. Allowed commands <start|stop|run|restart|check|supervise>", 
                           choices=["start", "stop", "run", "restart", "check", "supervise"], default="restart")
        
    def process(self):
        ngs_reader = NGSPipelinesConfigReader()
        instance_directory = ngs_reader.get_instance_directory(self.instance_name)
        if not os.path.isdir(instance_directory):
            raise Exception("The specified instance '" + self.instance_name + "' does not exist! " + \
                            "Please use the sub command addinstance before or select an existing instance.")
        
        ngspipelines_dist_dir = os.path.join( instance_directory, 'dist')
        os.environ[ 'BIOMART_HOME' ] = ngspipelines_dist_dir
        subprocess.check_call( 'cd {0} ; scripts/biomart-server.sh {1} -Dbiomart.properties={0}/biomart.properties '.format(ngspipelines_dist_dir, self.command ) , shell = True )

