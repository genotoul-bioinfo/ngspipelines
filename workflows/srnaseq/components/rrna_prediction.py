#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from jflow.component import Component

def run_rnammer(locus_file, genome_id, genome, rnammer_path, samtools_path, clade, boundary, out_dir, out_gff3, out_struct, out_fasta, log):
    """
    @summary: Run RNAmmer on fasta file
    """
    
    import workflows.srnaseq.lib.utils as utils
    import workflows.srnaseq.lib.rRNA as rRNA
    import subprocess, re
    
    log=open(log,"w")
    locus_file=open(locus_file,"r")
    out_gff3 = open(out_gff3,"w")
    out_fasta = open(out_fasta, "w")
    out_struct  = open(out_struct,"w")
    id_file = os.path.splitext(os.path.basename(locus_file.name))[0]
    
    list_eukaryotes = ["Metazoa","Viridiplantae"]
    opt_clade = ' -S euk ' if clade in list_eukaryotes else ' -S bac '
    
    cpt=0
    nb_pred=0
    
    out_gff3.write("##gff-version 3\n")
    file_id = os.path.basename(locus_file.name).split(".")[0]
    for line in locus_file:
        cpt+=1
        
        line_array = line.split('\t')
        chr = line_array[2]
        start = max(1,int(line_array[3]) - int(boundary))
        stop = int(line_array[4]) + int(boundary)
        strand = line_array[5]
        new_path =os.path.join(out_dir,chr+"_"+str(start)+"_"+str(stop)+"_"+strand+"_"+str(cpt)+"_"+file_id) 
        os.mkdir(new_path)
        os.chdir(new_path)
        
        temp_file = os.path.join(out_dir,chr+"_"+str(start)+"_"+str(stop)+"_"+str(cpt)+".fa")
        res_file = os.path.join(out_dir,chr+"_"+str(start)+"_"+str(stop)+"_"+str(cpt)+".result.csv")
        
        # Extracts nuc sequence of locus with samtools
        samtools_cmd = samtools_path + " faidx " + genome +" " + chr+":"+str(start)+"-"+str(stop) + "> "+ temp_file
        proc = subprocess.Popen(samtools_cmd, shell=True,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc.wait()
        
        # Runs RNAmmer on FASTA file
        cmd = rnammer_path + ' ' + opt_clade + ' -m lsu,ssu,tsu -gff - ' + temp_file + " > " + res_file
        proc = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = proc.communicate()
        #stdr.write(stderr)
        res_file = open(res_file,'r')
        for line in res_file:
            if not line.startswith("#"):
                nb_pred+=1
                infos = re.split('\t',line)
                chr = infos[0].split(":")[0]
                start = int(infos[3])
                stop = int(infos[4])
                strand = infos[6]
                new_rrna = rRNA.rRNA(genome_id, "rRNA_"+id_file+"_"+str(nb_pred), chr, start, stop, strand)
                #new_rrna = rRNA.rRNA("rRNA_"+file_id+"_"+str(nb_pred))
                #new_rrna.chromosome = infos[0].split(":")[0]
                #new_rrna.start = int(infos[3])
                #new_rrna.stop = int(infos[4])
                #new_rrna.strand = (infos[6])
                new_rrna.score = float(infos[5])
                new_rrna.prediction_start = new_rrna.start
                new_rrna.prediction_stop = new_rrna.stop
                #fasta_id = "|".join([new_rrna.name,new_rrna.chromosome,str(new_rrna.start),str(new_rrna.stop),new_rrna.strand])
                #new_rrna.name = fasta_id
                out_gff3.write(new_rrna.write_GFF3()+"\n")
                
                # Extracts nuc sequence of rRNA prediction with samtools
                tmp_file = os.path.join(out_dir,new_rrna.chromosome+"_"+str(new_rrna.start)+"_"+str(new_rrna.stop)+"_"+str(nb_pred)+".fa")
                samtools_cmd = samtools_path + " faidx " + genome +" " + new_rrna.chromosome+":"+str(new_rrna.start)+"-"+str(new_rrna.stop) + "> "+ tmp_file
                proc = subprocess.Popen(samtools_cmd, shell=True,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                proc.wait()
                tmp = open(tmp_file,"r")
                seq = ""
                for line in tmp:
                    if not ">" in line:
                        seq += line.rstrip()
                new_rrna.prediction_sequence = seq
                out_fasta.write(">"+new_rrna.name+"\n"+new_rrna.prediction_sequence+"\n")
                try:
                    os.remove(tmp.name)
                except OSError:
                    sys.exit("Unable to remove seq_"+tmp.name)
        
        try:
            os.remove(temp_file)
        except OSError:
            sys.exit("Unable to remove "+temp_file)
        res_file.close()
        try:
            os.remove(res_file.name)
        except OSError:
            sys.exit("Unable to remove "+res_file.name)
        os.chdir(out_dir)
        os.rmdir(new_path)
    log.write(str(nb_pred)+" rRNA found")
    out_gff3.close()
    out_fasta.close()
    out_struct.close()
    locus_file.close()

class RRNAPrediction (Component):
    
    def define_parameters(self, genome_id, locus_files, locus, clade, boundary, genome):
        """
            @param locus_file_list: [file list] of locus
            @param clade: [str] clade of species [Metazoa, Viruses, Viridiplantae]
            @param boundary: [str] Boundary
            @param genome: [file] genome file
        """
        self.add_parameter("genome_id", "Genome ID", default=genome_id, required=True)
        self.add_input_file_list("locus_files", "List of locus files", default=locus_files, required=True)
        self.add_input_file("genome", "Reference genome file", default=genome, required=True)
        self.add_input_file("locus", "Locus file", default=locus, required=True)
        self.add_parameter("boundary", "Boundary", default=boundary, type="int", required=True)
        self.add_parameter("clade", "Clade of species", default=clade, required=True)
        self.add_output_file_list("predictions", "List of gff3 files", pattern='{basename}.rrna.gff3', items=locus_files)
        self.add_output_file_list("strucs", "List of struc files", pattern='{basename}.rrna.struc', items=locus_files)
        self.add_output_file_list("fastas", "List of FASTA files", pattern='{basename}.rrna.fasta', items=locus_files)
        self.add_output_file_list("logs", "log files", pattern='{basename}.log', items=locus_files)
        
    def process(self):
        parameters = [self.genome_id, self.genome, self.get_exec_path("rnammer"), self.get_exec_path("samtools"), self.clade, str(self.boundary), self.output_directory]
        self.add_python_execution(run_rnammer,
                                  inputs=[self.locus_files],
                                  outputs= [self.predictions, self.strucs, self.fastas, self.logs],
                                  includes=[self.locus],
                                  cmd_format='{executable} {inputs} ' + ' '.join(parameters) + ' {outputs} ',
                                  map=True)
        
