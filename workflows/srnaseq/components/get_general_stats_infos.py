#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from jflow.component import Component




def get_infos(annotations, best_annotations, *predictions):
    from jflow.featureio import GFF3IO
    import subprocess
    from operator import itemgetter
    import workflows.srnaseq.lib.utils as utils
    
    def _get_annotations(annotations):
        """Get annotations from several annotation files
        @param annotations: List of annotation objects (self.args["annotation"])
        @return: the dictionnary of annotations
        """
        dic_annot={}
        for i in annotations:
            reader = GFF3IO(i, "r")
            for entry in reader:
                if not entry.attributes["Parent"] in dic_annot:
                    dic_annot[entry.attributes["Parent"]]=[]
                dic_annot[entry.attributes["Parent"]].append(utils.get_family_from_databank_id(entry.source))
        return dic_annot
    
    dic_best_annot = {}
    best_annot_reader = GFF3IO(best_annotations,"r")
    for a_entry in best_annot_reader:
        if a_entry.attributes["Parent"] in dic_best_annot:
            sys.exit("Duplicated field in best annotations file")
        else:
            dic_best_annot[a_entry.attributes["Parent"]]={}
            dic_best_annot[a_entry.attributes["Parent"]]["score"]=float(a_entry.score)
            dic_best_annot[a_entry.attributes["Parent"]]["best_id"]=a_entry.attributes["ID"]
            dic_best_annot[a_entry.attributes["Parent"]]["infos"]=a_entry
        
    dic_best_by_family = {}
    for i in dic_best_annot:
        type = i.split("_")[0]
        if not type in dic_best_by_family:
            dic_best_by_family[type]={}
            if not dic_best_annot[i]["infos"].source in dic_best_by_family[type]:
                dic_best_by_family[type][dic_best_annot[i]["infos"].source]=0
            dic_best_by_family[type][dic_best_annot[i]["infos"].source]+=1
        else:
            if not dic_best_annot[i]["infos"].source in dic_best_by_family[type]:
                dic_best_by_family[type][dic_best_annot[i]["infos"].source]=0
            dic_best_by_family[type][dic_best_annot[i]["infos"].source]+=1
    

    annotations = annotations.split(",")
    annot_infos =  _get_annotations(annotations)
    dic_annots = {}
    dic_locus_annot = {}
    dic_lengths = {}
    family = ""
    for idx, file in enumerate(predictions):
        reader = GFF3IO(file, "r")
        for entry in reader:
            if entry.type.endswith("_primary_transcript"):
                good = False
                other = False
                locus_name = entry.attributes["ID"]
                family = entry.type.split("_primary_transcript")[0]
                ## Prepare hash
                if not family in dic_annots:
                    dic_annots[family]={}
                    dic_annots[family]["predicted"]=0
                    dic_annots[family]["annotated"]=0
                    dic_annots[family]["misannotated"]=0
                    dic_annots[family]["both"]=0
                    dic_annots[family]["new"]=0
                    
                if not family in dic_locus_annot:
                    dic_locus_annot[family]={}
                if not locus_name in dic_locus_annot[family]:
                    dic_locus_annot[family][locus_name]={}
                    dic_locus_annot[family][locus_name]["score"]=""
                    dic_locus_annot[family][locus_name]["annot"]="new"
                if not family in dic_lengths:
                    dic_lengths[family]=[]
                
                dic_lengths[family].append(entry.end-entry.start+1)
                dic_locus_annot[family][locus_name]["score"]=float(entry.score)
                dic_annots[family]["predicted"]+=1
                if locus_name in annot_infos:
                    for elem in annot_infos[locus_name]:
                        if family == elem:
                            good = True
                        else:
                            other = True
                    only = good and not other
                    both = only and other
                    excluded = not only and other
                    nothing = not only and not other
                    
                    if only:
                        dic_annots[family]["annotated"]+=1
                        dic_locus_annot[family][locus_name]["annot"] = "only"
                    if both:
                        dic_annots[family]["both"]+=1
                        dic_locus_annot[family][locus_name]["annot"] = "both"
                    if excluded:
                        dic_annots[family]["misannotated"]+=1
                        dic_locus_annot[family][locus_name]["annot"] = "misannotated"
                    if nothing:
                        dic_locus_annot[family][locus_name]["annot"] = "new"
                    
                else:
                    dic_annots[family]["new"]+=1
        if family in dic_best_by_family:
            dic_best_by_family[family]["none"]=dic_annots[family]["new"]
    # Case of loci
    if "loci" in dic_annots:
        dic_annots["loci"]["annotated"]=dic_annots["loci"]["predicted"] - dic_annots["loci"]["both"]
        dic_annots["loci"]["predicted"]=0
        dic_annots["loci"]["misannotated"]=0
    
    return [dic_annots, dic_locus_annot, dic_lengths, dic_best_by_family]
    
    
    
class GetGeneralStatsInfos (Component):
    
    def define_parameters(self, predictions, annotations, best_annotations):
        """
        @summary: 
            @param predictions: [file] GFF3 predictions list
            @param annotations: [list] GFF3 annotations list
            @param best_annotations: [file] The GFF3 of best annotations
        """
        self.add_input_file_list("predictions", "The list of GFF3 prediction files", default=predictions, required=True)
        self.add_input_file_list("annotations", "The list of GFF3 annotation files", default=annotations, required=True)
        self.add_input_file("best_annotations", "GFF3 of best annotations", default=best_annotations)
        self.add_output_object("list_infos", "Output object")
        
    def process(self):
        cmd_format = '{executable} {outputs} ' +  ",".join(self.annotations) + " " + self.best_annotations + ' {inputs} '
        self.add_python_execution(get_infos,
                                  cmd_format=cmd_format,
                                  inputs=[self.predictions],
                                  outputs=[self.list_infos],
                                  includes=[self.annotations, self.best_annotations]
                                  )
        
        