#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, re
from subprocess import Popen, PIPE
from jflow.component import Component

class Bowtie2 (Component):
    
    def define_parameters(self, reference_files, sequences_file, reference_ids, seed_sizes, seed_mismatchs, aln_reports):
        """
            @param reference_files: [list] of reference files
            @param sequences_file: [file] file with sequences to map
            @param reference_ids: [list of string] databank ids 
            @param seed_sizes: [list of int] the seed size relative to each reference
            @param seed_mismatchs: [list of int] the number of mismatches authorized in the seed relative to each reference
            @param aln_reports: [list of int] how many localisations to report relative to each reference
        """
        
        self.add_input_file_list("reference_files", "Reference files",  default=reference_files, file_format="fasta", required=True)
        self.add_input_file("sequences_file", "File with sequences to map against reference_files",  default=sequences_file, file_format="fasta", required=True)
        self.add_parameter_list("reference_ids", "Databank ids", default=reference_ids, required=True)
        self.add_parameter_list("seed_sizes", "Size of the seed", default=seed_sizes, required=True)
        self.add_parameter_list("seed_mismatchs", "Number of mismatches authorized in the seed", default=seed_mismatchs, required=True)
        self.add_parameter_list("aln_reports", "How many localisations to report", default=aln_reports, required=True)
        new_ids = []
        for cpt, name in enumerate(self.reference_files):
            new_ids.append(os.path.join(os.path.dirname(name),self.reference_ids[cpt]))
        self.add_output_file_list("bam_files", "BAM file", pattern="{basename}.bam", items=new_ids)
        self.add_output_file_list("bowtie2_logs", "stdout files", pattern="{basename}.bowtie2.log", items=new_ids)
    
    def process(self):
        for cpt, iref in enumerate(self.reference_files):
            bowtie2_cmd = self.get_exec_path("bowtie2") + " -x $1 -k $3 -L $2 -N $4  -f " + self.sequences_file + " 2>> $6 | " + \
                          self.get_exec_path("samtools") + " view -bS - 2>> $6 > $5"
            bowtie2 = self.add_shell_execution(bowtie2_cmd, 
                                               inputs=[self.reference_files[cpt]], 
                                               arguments=[self.seed_sizes[cpt], self.aln_reports[cpt], self.seed_mismatchs[cpt]], 
                                               outputs=[self.bam_files[cpt], self.bowtie2_logs[cpt]],  
                                               includes=self.sequences_file, 
                                               cmd_format='{EXE} {IN} {ARG} {OUT}')
                