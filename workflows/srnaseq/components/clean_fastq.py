#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from ngspipelines.component_analysis import ComponentAnalysis


def clean_fastq(fastq_file, five_prime_adapters, three_prime_adapters, min_length, max_length, cleaned_file, low_complex_file, too_short_file, too_long_file, log_file):
    """
    @summary: Cleans FASTQ file : removes adapters if needed, filters on read length, removes low-complex reads and collapses.
        @param fastq_file: [file] to clean in FASTQ format
        @param five_prime_adapters: [file] or [str] containing 5' adapter(s)
        @param three_prime_adapters: [file] or [str] containing 3' adapter(s)
        @param min_length: [str] the min length for a sequence
        @param max_length: [str] the max length for a sequence
        @param cleaned_file: [file] with cleaned sequences inside (FASTA format)
        @param low_complex_file: [file] with low-complex sequences inside (FASTA format)
        @param too_short_file: [file] with too short sequences inside (FASTA format)
        @param too_long_file: [file] with too long sequences inside (FASTA format)
        @param log_file: [file] with cleaning statistics inside
    @requires: utils.is_low_complex()
    """
    
    from workflows.srnaseq.lib.cutadapt.scripts.cutadapt import Adapter
    from workflows.srnaseq.lib.cutadapt.scripts.cutadapt import BACK
    from workflows.srnaseq.lib.cutadapt.scripts.cutadapt import FRONT
    from workflows.srnaseq.lib.cutadapt.scripts.cutadapt import AdapterCutter
    from workflows.srnaseq.lib.cutadapt.scripts.cutadapt import read_sequences
    from workflows.srnaseq.lib.cutadapt.scripts.cutadapt import write_read
    from workflows.srnaseq.lib.cutadapt.scripts.cutadapt import ReadFilter
    from workflows.srnaseq.lib.cutadapt.seqio import FormatError
    from jflow.seqio import FastqReader
    
    def _get_adapters_object(adapters_object, adapters, position):
        """
        @summary: Get cutadapt Adapter list
            @param adapters_object: [list] Adapter list
            @param adapters: [str] or [file] or [None] adapters
            @param position: [BACK or FRONT] according to 3' or 5' adapter position
        """
        if adapters != "None":
            # Adapter is a string
            if not os.path.isfile(adapters):
                adapters_object.append((Adapter(adapters, position, error_rate,
                                            overlap, match_read_wildcards,
                                            colorspace,
                                            match_adapter_wildcards,
                                            wildcard_file,
                                            rest_file)))
            # List of adapters otherwise
            else:
                adapters_file = open(adapters,"r")
                for line in adapters_file:
                    if not line.startswith(">"):
                         seq = line.rstrip()
                         adapters_object.append((Adapter(seq, position, error_rate,
                                                         overlap, match_read_wildcards,
                                                         colorspace,
                                                         match_adapter_wildcards,
                                                         wildcard_file,
                                                         rest_file)))
                adapters_file.close()        
        return adapters_object

    def _is_low_complex(uread):
        """
        @summary: Check if the read is low_complex (contains N, dinuc{6} or less than 3 different nucleotides)
            @param uread: [str] the nuclotide sequence
        @return: [boolean]
        """
        
        char = {}
        cpt=0
        sequence = uread
        dinuc = {}
        contains_N = False
        contains_dinuc = False
        low_char = False
        for c in uread:
            if c == 'N':
                contains_N = True
                break
            char[c]=1
            seq = sequence[cpt:cpt+2]
            if not seq in dinuc:
                dinuc[seq]=1
            else:
                dinuc[seq]+=1
            cpt+=1
        
        for all in dinuc:
            if dinuc[all]>=6:
                contains_dinuc = True
        
        if len(char)<3:
            low_char = True
        
        if (contains_N or contains_dinuc or low_char):
            return True


    # Set some default parameters for cutadapt process
    colorspace = False
    quality_cutoff = None
    trim_primer = False
    quality_base = 33
    quality_filename = None
    times = 1
    error_rate = 0.1
    overlap = 3
    match_read_wildcards = False
    match_adapter_wildcards = True
    wildcard_file = None
    rest_file = None
    discard_trimmed = False
    length_tag = None
    
    # Open files
    cleaned_file = open(cleaned_file,'w')
    low_complex_file = open(low_complex_file,'w')
    too_long_file = open(too_long_file,"w")
    too_short_file = open(too_short_file,"w")
    log_file = open(log_file,'w')
    
    # Variables
    min_length=int(min_length)
    max_length=int(max_length)
    cpt_too_short = 0
    cpt_too_long = 0
    cpt_low_complex=0
    cpt_kept=0
    cpt_initial_seq=0
    length_dic = {}
    length_dic_uniq = {}
    seqs = {}
    shorts = {}
    longs = {}
    lows = {}
    for i in range(min_length,max_length+1):
        length_dic[str(i)]=0
        length_dic_uniq[str(i)]=0
    adapters_object = []
    
    if three_prime_adapters != "None" or five_prime_adapters != "None":
        process_cutadapt = True
    else:
        process_cutadapt = False
    
    # Must adapters be removed ?
    if process_cutadapt:
        adapters_object = _get_adapters_object(adapters_object, three_prime_adapters, BACK)
        adapters_object = _get_adapters_object(adapters_object, five_prime_adapters, FRONT)
        
        file_basename = os.path.splitext(os.path.basename(fastq_file))[0]
        file_ext = os.path.splitext(os.path.basename(fastq_file))[1]
        if file_ext == ".gz":
            file_ext = os.path.splitext(os.path.basename(file_basename))[1]
            file_basename = os.path.splitext(os.path.basename(file_basename))[0]
        
        cutter = AdapterCutter(adapters_object, times, rest_file, colorspace, wildcard_file)
        readfilter = ReadFilter(min_length, max_length, None, None, discard_trimmed, cutter.stats)
        try:
            reader = read_sequences(fastq_file, quality_filename, colorspace=colorspace, fileformat="fastq")
            for read in reader:
                # In colorspace, the first character is the last nucleotide of the primer base
                # and the second character encodes the transition from the primer base to the
                # first real base of the read.
                cpt_initial_seq += 1
                if trim_primer:
                    read.sequence = read.sequence[2:]
                    if read.qualities is not None:
                        read.qualities = read.qualities[1:]
                    initial = ''
                elif colorspace:
                    initial = read.sequence[0]
                    read.sequence = read.sequence[1:]
                else:
                    initial = ''
    
                if quality_cutoff is not None:
                    index = quality_trim_index(read.qualities, quality_cutoff, quality_base)
                    read = read[:index]
    
                read, trimmed = cutter.cut(read)                
                read.qualities = None
                if readfilter.keep(read, trimmed):
                    # Length is ok
                    length_dic[str(len(read.sequence))]+=1
                    read.sequence = initial + read.sequence
                    try:
                        if seqs.has_key(read.sequence):
                            seqs[read.sequence] += 1
                        else :
                            seqs[read.sequence] = 1
                    except IOError as e:
                        if e.errno == errno.EPIPE:
                            return 1
                        raise
                else:
                    # Bad length
                    if len(read.sequence)<min_length:
                        if shorts.has_key(read.sequence): shorts[read.sequence] += 1
                        else : shorts[read.sequence] = 1
                    else:
                        if longs.has_key(read.sequence): longs[read.sequence] += 1
                        else : longs[read.sequence] = 1
                                
            for uread in seqs:
                if len(uread) != 0:
                    length_dic_uniq[str(len(uread))]+=1
                    if not _is_low_complex(uread):
                        cpt_kept+=1
                        seq_id = "seq"+str(cpt_kept)+"#"+str(seqs[uread])
                        cleaned_file.write('>%s\n%s\n' % (seq_id, uread))
                    else:
                        cpt_low_complex+=1
                        seq_id = "seq"+str(cpt_low_complex)+"#"+str(seqs[uread])
                        low_complex_file.write(">"+seq_id+"\n"+uread+"\n")
            
            for uread in shorts:
                if len(uread) != 0:
                    cpt_too_short += 1
                    seq_id = "seq"+str(cpt_too_short)+"#"+str(shorts[uread])
                    too_short_file.write('>%s\n%s\n' % (seq_id, uread))
            for uread in longs:
                if len(uread) != 0:
                    cpt_too_long += 1
                    seq_id = "seq"+str(cpt_too_long)+"#"+str(longs[uread])
                    too_long_file.write('>%s\n%s\n' % (seq_id, uread))

        except FormatError as e:
            sys.exit("Format Error in FASTQ file : "+fastq_file.name)
            
        if rest_file is not None: rest_file.close()
        if wildcard_file is not None: wildcard_file.close()
        cutter.stats.print_statistics(error_rate, file=log_file)
    
    # Adapters already removed by the user
    else:
        
        for seq in FastqReader(fastq_file):
            seq = seq.sequence
            cpt_initial_seq+=1
            if (len(seq) < min_length):
                cpt_too_short += 1
                if seq in shorts: shorts[seq]+=1 
                else: shorts[seq]=1
                
            elif  (len(seq) > max_length):
                cpt_too_long += 1
                if seq in longs: longs[seq]+=1 
                else: longs[seq]=1
                
            else:
                if not _is_low_complex(seq):
                    length_dic[str(len(seq))]+=1
                    if seq in seqs:seqs[seq]+=1
                    else:seqs[seq]=1
                else:
                    if seq in lows: lows[seq]+=1 
                    else: lows[seq]=1
                    cpt_low_complex+=1
        
        cpt=1
        for i in lows:
            seq_id = "seq"+str(cpt)+"#"+str(lows[i])
            low_complex_file.write('>'+seq_id+'\n'+str(i)+'\n')
            cpt+=1
        cpt=1
        for i in shorts:
            seq_id = "seq"+str(cpt)+"#"+str(shorts[i])
            too_short_file.write('>'+seq_id+'\n'+str(i)+'\n')
            cpt+=1
        cpt=1
        for i in longs:
            seq_id = "seq"+str(cpt)+"#"+str(longs[i])
            too_long_file.write('>'+seq_id+'\n'+str(i)+'\n')
            cpt+=1
        
        
        cpt=1
        for i in seqs:
            seq_id = "seq"+str(cpt)+"#"+str(seqs[i])
            cleaned_file.write('>'+seq_id+'\n'+str(i)+'\n')
            length_dic_uniq[str(len(i))]+=1
            cpt+=1
            cpt_kept += 1
    
    # Write statistics in stat file
    log_file.write('#Processed reads\t'+str(cpt_initial_seq)+'\n')
    log_file.write('#Too short reads\t'+str(cpt_too_short)+'\n')
    log_file.write('#Too long reads\t'+str(cpt_too_long)+'\n')
    log_file.write('#Low-complex reads\t'+str(cpt_low_complex)+'\n')
    log_file.write('#Kept reads\t'+str(cpt_initial_seq-(cpt_too_short+cpt_too_long+cpt_low_complex))+'\n')
    log_file.write('#Kept sequences\t'+str(cpt_kept)+'\n')
    log_file.write("#Length")
    for i in sorted(length_dic.iterkeys(),key=int):
        log_file.write("\t"+str(i))
    log_file.write("\n#Before redundancy")
    for j in sorted(length_dic.iterkeys(),key=int):
        log_file.write("\t"+str(length_dic[j]))
    log_file.write("\n#After redundancy")
    for k in sorted(length_dic_uniq.iterkeys(),key=int):
        log_file.write("\t"+str(length_dic_uniq[k]))
    log_file.write("\n\n")
    
    # Close files
    if low_complex_file is not None: low_complex_file.close()
    too_long_file.close()
    too_short_file.close()
    cleaned_file.close()
    log_file.close()
    
    
class CleanFastq (ComponentAnalysis):
    
    def define_parameters(self, fastq_files, min_length, max_length, five_prime_adapters=None, three_prime_adapters=None):
        """
            @param fastq_files: [list] of input FASTQ files
            @param min_length: [int] the min length for sequences to be kept
            @param max_length: [int] the max length for sequences to be kept
            @param five_prime_adapters: [list] of [file or str or None] containing 5' adapters
            @param three_prime_adapters: [list] of [file or str or None] containing 3' adapters
        """
        self.add_input_file_list("fastq_files", "List of FASTQ files to clean", default=fastq_files, required=True, file_format="fastq")
        self.add_parameter("min_length", "Min length of a trimmed read to be kept", default=min_length, type="int", required=True)
        self.add_parameter("max_length", "Max length of a trimmed read to be kept", default=max_length, type="int", required=True)
        self.add_parameter_list("five_prime_adapters", "5' Adapters string or file", default=five_prime_adapters, required=True)
        self.add_parameter_list("three_prime_adapters", "3' Adapters string or file", default=three_prime_adapters, required=True)
        self.add_output_file_list("low_complex_files", "The FASTA file of low-complex sequences", pattern="{basename_woext}.low_complex.fa", items=self.fastq_files, file_format="fasta")
        self.add_output_file_list("too_short_files", "The FASTA file of too short sequences", pattern="{basename_woext}.too_short.fa", items=self.fastq_files, file_format="fasta")
        self.add_output_file_list("too_long_files", "The FASTA file of too long sequences", pattern="{basename_woext}.too_long.fa", items=self.fastq_files, file_format="fasta")
        self.add_output_file_list("cleaned_files", "The FASTA file of kept sequences", pattern="{basename_woext}.clean.fa", items=self.fastq_files, file_format="fasta")
        self.add_output_file_list("logs", "The log files of cleaning process", pattern="{basename_woext}.log", items=self.fastq_files)
        
    def define_analysis(self):
        self.analysis_name = "Cleaning fastq files"
        self.soft_name = "cutadapt and in-house scripts"
        self.soft_parameters =  "min_length: " + str(self.min_length) + "bp"
        self.soft_parameters += ", max_length: " + str(self.max_length) + "bp"
        self.comments = "Remove adapter, filter low complex reads and merge by sample"
        self.files = []
        self.files.extend(self.low_complex_files)
        self.files.extend(self.too_short_files)
        self.files.extend(self.too_long_files)
        self.files.extend(self.cleaned_files)
        
    def get_version(self):
        return "1.0"
    
    def post_process(self):
        for file_name in self.logs:
            f = open(file_name, 'r')
            sample_name = os.path.splitext(os.path.basename(file_name))[0]
            for line in f:
                if line.startswith("#"):
                    if line.startswith("#Processed reads"):
                       self._add_result_element("nb_reads",        line.split('\t')[1], "int", sample_name)
                    elif line.startswith("#Too short reads"):
                        self._add_result_element("short_reads",    line.split('\t')[1], "int", sample_name) 
                    elif line.startswith("#Too long reads"):
                        self._add_result_element("long_reads",     line.split('\t')[1], "int", sample_name)
                    elif line.startswith("#Low-complex reads"):
                        self._add_result_element("low_complex",    line.split('\t')[1], "int", sample_name)
                    elif line.startswith("#Kept reads"):
                        self._add_result_element("cleaned_reads",     line.split('\t')[1], "int", sample_name)
                    elif line.startswith("#Kept sequences"):
                        self._add_result_element("cleaned_sequences", line.split('\t')[1], "int", sample_name)
                    elif line.startswith("#Length"):
                        self._add_result_element("length",            ",".join(line.split('\t')[1:]), "string", sample_name)
                    elif line.startswith("#Before redundancy"):
                        self._add_result_element("before_redundancy", ",".join(line.split('\t')[1:]), "string", sample_name)
                    elif line.startswith("#After redundancy"):
                        self._add_result_element("after_redundancy",  ",".join(line.split('\t')[1:]), "string", sample_name)
            f.close()
        
    def process(self):
        for cpt, ifastq in enumerate(self.fastq_files):
            cmd_format = '{executable} {inputs} {arguments} '+ str(self.min_length) + ' ' + str(self.max_length) + ' {outputs}'
            clean =  self.add_python_execution(clean_fastq, inputs=ifastq, outputs=[self.cleaned_files[cpt], self.low_complex_files[cpt], self.too_short_files[cpt], self.too_long_files[cpt], self.logs[cpt]], arguments=[self.five_prime_adapters[cpt], self.three_prime_adapters[cpt] ], cmd_format=cmd_format)

    
    