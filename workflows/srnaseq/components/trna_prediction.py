#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from jflow.component import Component

def predict_tRNA(locus_file, genome_id, genome, boundary, tRNAScanSE_path, samtools_path, out_dir, out_gff3, out_struct, out_fasta, stdo, stdr):
    """
    @summary: Predicts tRNA from loci. Processes tRNAscanSE on each locus coordinate. Constructs a list of tRNA objects stored in a pickle file.
        @param locus_file: [file] loci
        @param genome: [file] genome
        @param boundary: [str] grows up the loci coordinates of boundary bases
        @param tRNAScanSE_path: [str] the executable path of tRNAScanSE
        @param samtools_path: [str] the executable path of samtools
        @param out_dir: [str] the output directory
        @param out_gff3: [file] the GFF3 output file
        @param out_struct: [file] the structure output file
        @param out_fasta: [file] the FASTA output file
        @param stdo: [file] stdout
        @param stdr: [file] stderr
    """
    import subprocess
    import workflows.srnaseq.lib.tRNA as tRNA

    locus_file=open(locus_file,"r")
    out_gff3 = open(out_gff3,"w")
    out_fasta = open(out_fasta, "w")
    out_struct  = open(out_struct,"w")
    stdo=open(stdo,"w")
    stdr=open(stdr,"w")
    out_gff3.write("##gff-version 3\n")
    
    id_file = os.path.splitext(os.path.basename(locus_file.name))[0]
    cpt=0
    
    for line in locus_file:
        line_array = line.split('\t')
        chr = line_array[2]
        start = int(line_array[3]) - int(boundary)
        stop = int(line_array[4]) + int(boundary)
        strand = line_array[5]
        new_path =os.path.join(out_dir,chr+"_"+str(start)+"_"+str(stop)+"_"+strand+"_"+str(cpt)+"_"+id_file)
        os.mkdir(new_path)
        os.chdir(new_path)
        temp_file = os.path.join(out_dir,chr+"_"+str(start)+"_"+str(stop)+"_"+str(cpt)+".fa")
        res_file = os.path.join(out_dir,chr+"_"+str(start)+"_"+str(stop)+"_"+str(cpt)+".result.csv")
        
        # Extracts nuc sequence of locus with samtools
        samtools_cmd = samtools_path + " faidx " + genome +" " + chr+":"+str(start)+"-"+str(stop) + "> "+ temp_file
        proc = subprocess.Popen(samtools_cmd, shell=True,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc.wait()
        
        # Runs tRNAscan-SE on FASTA file
        cmd = tRNAScanSE_path + " -Q " + temp_file + " -f " + res_file
        proc = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = proc.communicate()
        ok = True
        stdr.write(stdout)
        stdo.write(stderr)
        res_file = open(res_file,'r')
        if not 'No tRNAs found.' in stderr:
            cpt+=1
            new_trna = tRNA.tRNA(genome_id, "tRNA_"+id_file+"_"+str(cpt), chr, start, stop, "")
            current_trna = tRNA.parse_tRNAscanSE_structure(res_file, new_trna)
            #fasta_id = "|".join([current_trna.name,current_trna.chromosome,str(current_trna.start),str(current_trna.stop),current_trna.strand])
            #current_trna.name = fasta_id
            out_gff3.write(current_trna.write_GFF3()+"\n")
            out_struct.write(">"+current_trna.name+"\n"+current_trna.sequence+"\n"+current_trna.structure+"\t"+"("+str(current_trna.freenrj)+")\n")
            out_fasta.write(">"+current_trna.name+"\n"+current_trna.sequence+"\n")
        
        os.remove(temp_file)
        res_file.close()
        os.remove(res_file.name)
        os.chdir(out_dir)
        os.rmdir(new_path)

    out_gff3.close()
    out_fasta.close()
    out_struct.close()
    locus_file.close()
    stdr.write(str(cpt)+" tRNA found")
    stdr.close()
    stdo.close()

class TRNAPrediction (Component):
    
    def define_parameters(self, genome_id, locus_files, locus, genome, bound):
        """
            @param locus_files: [list] List of locus files
            @param genome: [file] Reference genome
            @param bound: [int] Bound for tRNAScan-SE
        """
        self.add_parameter("genome_id", "Genome ID", default=genome_id, required=True)
        self.add_input_file_list("locus_files", "List of locus files", default=locus_files, required=True)
        self.add_input_file("genome", "Reference genome", default=genome, required=True)
        self.add_input_file("locus", "Locus file", default=locus, required=True)
        self.add_parameter("bound", "tRNAScan-SE bound", default=bound, type="int", required=True)
        self.add_output_file_list("predictions", "List of gff3 files", pattern='{basename_woext}.trna.gff3', items=locus_files)
        self.add_output_file_list("strucs", "List of struc files", pattern='{basename_woext}.trna.struc', items=locus_files)
        self.add_output_file_list("fastas", "List of FASTA files", pattern='{basename_woext}.trna.fasta', items=locus_files)
        self.add_output_file_list("stderrs", "stderr files", pattern='{basename_woext}.stderr', items=locus_files)
        self.add_output_file_list("stdouts", "stdout files", pattern='{basename_woext}.stdout', items=locus_files)

    def process(self):
        parameters = [self.genome_id, self.genome, str(self.bound), self.get_exec_path("tRNAscan-SE"), self.get_exec_path("samtools"), self.output_directory]
        self.add_python_execution(predict_tRNA,
                                  inputs = [self.locus_files],
                                  outputs = [self.predictions, self.strucs, self.fastas, self.stderrs, self.stdouts],
                                  includes = [self.genome, self.locus],
                                  cmd_format = "{EXE} {IN} " + " ".join(parameters) + " {OUT}",
                                  map = True)
        
