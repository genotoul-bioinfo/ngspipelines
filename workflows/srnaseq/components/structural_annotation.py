#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from jflow.component import Component
        
def struct_annot(bed_input, bedtools_path, window_size, annotation, gff3, stdout_file, stderr_file):
    """
    @summary: 
        @param annotation: [file] GFF annotation file
        @param bedtools_path: [str] path
        @param window_size: [int] the limit size to consider the prediction close to a track
        @param bed_input: [file] BED
        @param gff3: [file] output
        @param stdout_file: [file] stdout
        @param stderr_file: [file] stderr
    """
    import subprocess, re
    
    stdout_file = open(stdout_file,"w")
    stderr_file = open(stderr_file,"w")
    gff3 = open(gff3,"w")
    window_size = int(window_size)
    
    annot = {}
    
    # BEDTOOLS PROCESS
    # Closest
    cmd = bedtools_path + " -a " + bed_input + " -b " + annotation + " -D ref -t all"
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    for line in iter(p.stdout.readline,''):
        tab = re.split("\t",line.rstrip())
        if not 'mature' in tab[8]:
            id = tab[8].split(";")[0].split("=")[1]
            distance = int(tab[-1:][0])
            size_target = int(tab[12])-int(tab[13])+1
            strand_pred = tab[6]
            strand_target = tab[15]
            tab.pop()
            if distance != -1:
                if distance == 0:
                    if not id in annot:
                        annot[id]={}
                        annot[id]["line"]="\t".join(tab[-9:])+";PParent="+id+";Distance="+str(distance)+"\n"
                        annot[id]["strand"]=strand_target
                        annot[id]["size"]=size_target
                    else:
                        if annot[id]["size"]<size_target:
                            annot[id]["line"]="\t".join(tab[-9:])+";PParent="+id+";Distance="+str(distance)+"\n"
                            annot[id]["size"]=size_target
                            annot[id]["strand"]=strand_target
                        elif annot[id]["size"]==size_target:
                            if annot[id]["strand"] != strand_pred:
                                annot[id]["line"]="\t".join(tab[-9:])+";PParent="+id+";Distance="+str(distance)+"\n"
                                annot[id]["size"]=size_target
                                annot[id]["strand"]=strand_target
                elif (abs(distance) <= window_size):
                    if not id in annot:
                        annot[id]={}
                        annot[id]["line"]="\t".join(tab[-9:])+";PParent="+id+";Distance="+str(distance)+"\n"
                        annot[id]["size"]=size_target
                        annot[id]["strand"]=strand_target
                    else:
                        if annot[id]["strand"] != strand_pred:
                            annot[id]["line"]="\t".join(tab[-9:])+";PParent="+id+";Distance="+str(distance)+"\n"
                            annot[id]["size"]=size_target
                            annot[id]["strand"]=strand_target
    
    for i in annot:
        gff3.write(annot[i]["line"])
    
    for line in iter(p.stderr.readline,''):
        stderr_file.write(line)

    stdout_file.close()
    stderr_file.close()
    gff3.close()
    
class StructuralAnnotation(Component):
    
    def define_parameters(self, predictions, structural_annotation, window_size=10000):
        """
            @param predictions: [list] prediction files list
            @param structural_annotation: [file] structural annotation file
            @param window_size: [int] Window size to get the closest track
        """
        self.add_parameter("window_size", "window size", type="int", default=window_size)
        self.add_input_file_list("predictions", "BED file", default=predictions, required=True)
        self.add_input_file("structural_annotation", "Structural annotation file", default=structural_annotation, required=True)
        self.add_output_file_list("sorted_predictions", "List of GFF3", pattern='{basename_woext}.strucannot.sort.gff3', items=self.predictions)
        self.add_output_file_list("structural_annotations", "List of GFF3", pattern='{basename_woext}.strucannot.gff3', items=self.predictions)
        self.add_output_file_list("stdouts", "stdout", pattern='{basename_woext}.strucannot.stdout', items=self.predictions)
        self.add_output_file_list("stderrs", "stderr", pattern='{basename_woext}.strucannot.stderr', items=self.predictions)
        
    def process(self):
        
        cmd = "sort -k1,1 -k4,4n $1 > $2"
        self.add_shell_execution(cmd,
                                 cmd_format="{EXE} {IN} {OUT}",
                                 inputs=[self.predictions],
                                 outputs=[self.sorted_predictions],
                                 map=True)
        
        
        self.add_python_execution(struct_annot,
                                  cmd_format='{executable} {inputs} ' + self.get_exec_path('closestBed') + ' ' + str(self.window_size) + ' ' + self.structural_annotation + ' {outputs} ',
                                  inputs=[self.sorted_predictions],
                                  outputs=[self.structural_annotations, self.stdouts, self.stderrs],
                                  includes=[self.structural_annotation],
                                  map=True)