#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, re
from subprocess import Popen, PIPE
from ngspipelines.component_analysis import ComponentAnalysis

def filter_bam(input_bam, samtools_path, mismatch, max_best_hits, indel, stderr, stats, out_sam):
    """
    @summary: Filters a bowtie2 BAM file 
        @param input_bam: [file] (BAM) to filter
        @param samtools_path: [path] the executable samtools path
        @param mismatch: [str] the mismatch number authorized
        @param max_sub_hits: [str] the maximum of suboptimal hits
        @param max_best_hits: [str] the maximum of best hits
        @param indel: [str] are INDELS authorized in the alignment ? (True or False)
        @param stderr: [file] the stderr
        @param stats: [file] the stats file
        @param out_sam: [file] the output SAM
    @requires: utils.get_strand_signification()
    """
    import workflows.srnaseq.lib.utils as utils
    import subprocess, re, sys
    
    dic_strand = utils.get_strand_signification()
    
    stderr_out = open(stderr,"w")
    stats_out = open(stats,"w")
    out_sam = open(out_sam,'w')
    bam = open(input_bam,"r")
    
    mismatch = int(mismatch)
    max_best_hits = int(max_best_hits)
    indel = True if indel == 'True' else False
    dic_all_seq = {}
    dic_stat = {}               # dictionary to store statistics
    alignment = {}              # dictionary to store alignment informations
    dic_stat['unmapped']=0
    dic_stat['mapped']=0
    dic_stat['alignments']=0
    dic_stat['mismatch']=0
    dic_stat['indel']=0
    dic_stat['too_many_reports']=0
    
    # Write BAM file
    cmd = samtools_path + " view -h " + bam.name
    proc = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)
    
    line_number = 0
    for line in iter(proc.stdout.readline,''):
        nb_filters = 0
        if re.search(r"^@", line):
            out_sam.write(line)
        else:
            line_number+=1
            line_array = line.split()
            try:
                alignment['QNAME']=line_array[0]
                alignment['FLAG']=line_array[1]
                alignment['RNAME']=line_array[2]
                alignment['POS']=line_array[3]
                alignment['MAPQ']=line_array[4]
                alignment['CIGAR']=line_array[5]
                alignment['RNEXT']=line_array[6]
                alignment['PNEXT']=line_array[7]
                alignment['TLEN']=line_array[8]
                alignment['SEQ']=line_array[9]
                alignment['QUAL']=line_array[10]
            except IndexError:
                stderr_out.write("... Line "+str(line_number)+" is malformed in file "+bam.name)
                sys.exit()
            
            if dic_strand[alignment['FLAG']]=="unmapped":
                dic_stat['unmapped']+=1
            else:
                dic_stat['mapped']+=1
            
                if (re.search('D|I',line_array[5])):
                    dic_stat['indel']+=1
                    if not indel:
                        nb_filters+=1
    
                try:
                    nm = int(re.search("NM:i:(\d+)",line).group(0).split(":")[2])
                    if nm > mismatch:
                        dic_stat['mismatch']+=1
                        nb_filters+=1
                except AttributeError:
                    stderr_out.write("... Line "+str(line_number)+" has no NM tag\n"+line+"\n")
                    sys.exit()
        
            # Stores sequence in dic of all sequences
            if nb_filters == 0:
                if not alignment['QNAME'] in dic_all_seq:
                    dic_all_seq[alignment['QNAME']]=[]
                    dic_all_seq[alignment['QNAME']].append(line.rstrip())
                else:
                    dic_all_seq[alignment['QNAME']].append(line.rstrip())

    for seq in dic_all_seq:
        if len(dic_all_seq[seq])<=max_best_hits:
            for each in dic_all_seq[seq]:
                out_sam.write(each+"\n")
                dic_stat['alignments']+=1
        else:
            dic_stat['too_many_reports']+=len(dic_all_seq[seq])
    
    stats_out.write("Processed alignments\t" + str(line_number)+"\n")
    stats_out.write("Mapped alignments before filtering\t" + str(dic_stat['mapped'])+"\n")
    stats_out.write("Unmapped\t" + str(dic_stat['unmapped'])+"\n")
    stats_out.write("Containing Indels\t" + str(dic_stat['indel'])+"\n")
    stats_out.write("Too many mismatches\t" + str(dic_stat['mismatch'])+"\n")
    stats_out.write("Too many hits\t" + str(dic_stat['too_many_reports'])+"\n")
    stats_out.write("Reported alignments\t" + str(dic_stat['alignments']-dic_stat['unmapped'])+"\n")
        
    out_sam.close()
    bam.close()
    stderr_out.close()
    stats_out.close()
    
class FilterBAM (ComponentAnalysis):
    
    def define_parameters(self, bam_files, mismatchs, max_best_hits, indels, versions=[], map_logs=[]):
        """
            @param bam_files: [list of files] to filter
            @param mismatchs: [list of int] the mismatch number authorized relative to each input BAM
            @param max_best_hits: [list of int] the maximum of best hits relative to each input BAM
            @param indels: [list of bool] are INDELS authorized in the alignment ? relative to each input BAM
            @param versions: [list of strings] References version
            @param map_logs: [list of files] mapping log files
        """
        self.add_input_file_list("bam_files", "BAM files", default=bam_files, required=True)
        self.add_parameter_list("mismatchs", "Number of mismatch authorized", default=mismatchs, required=True)
        self.add_parameter_list("max_best_hits", "Max of best hits", default=max_best_hits, required=True)
        self.add_parameter_list("indels", "INDEL allowed?", default=indels, required=True)
        self.add_parameter_list("versions", "References versions", default=versions)
        self.add_input_file_list("map_logs", "map log files", default=map_logs)
        self.add_output_file_list("filtered_bam_files", "The filtered BAM files", pattern="{basename_woext}.filter.bam", items=self.bam_files) 
        self.add_output_file_list("filtered_sam_files", "The filtered SAM files", pattern="{basename_woext}.tmp.sam", items=self.bam_files)
        self.add_output_file_list("stdout_files", "stdout files", pattern="{basename_woext}.stdout", items=self.bam_files)
        self.add_output_file_list("stderr_files", "stderr files", pattern="{basename_woext}.stderr", items=self.bam_files)
        self.add_output_file_list("log_files", "log files", pattern="{basename_woext}.log", items=self.bam_files)
    
    def define_analysis(self):
        if "genome" in self.get_nameid():
            self.analysis_name = "Mapping"
        else:
            self.analysis_name = "Sequences annotation"
        self.soft_name = os.path.split(self.map_logs[0])[1].split(".")[1]+", samtools and in-house scripts"
        self.soft_parameters = "-"
        self.comments = "Align (against " + ", ".join([os.path.split(i)[1].split(".bam")[0] for i in self.bam_files]) + ") and filter"
        self.files = []
        self.files.extend(self.filtered_bam_files)
        
    def get_version(self):
        versions=[]
        #cmd = [self.get_exec_path(os.path.split(self.map_logs[0])[1].split(".")[1]), "--version"]
        cmd = [self.get_exec_path("bowtie2"), "--version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        versions.append(stdout.split()[2])

        cmd = [self.get_exec_path("samtools")]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        for line in iter(p.stderr.readline,''):
            line = line.rstrip()
            if "Version:" in line:
                versions.append(re.split("\s+", line)[1])

        return ", ".join(versions)
    
    def post_process(self):
        # Bowtie2 logs
        self._add_result_element("keys", "Seq number,Mapped,Unique location,Multiple locations,Unmapped", "string", "bowtie")
        for cpt, i in enumerate(self.map_logs):
            name = os.path.basename(i.split(".bowtie2.log")[0])
            values = ""
            fh = open(i,"r")
            for line in fh:
                line = line.rstrip()
                if "reads" in line:
                    values = re.split("\s+", line)[0]
                    total  = int(re.split("\s+", line)[0])
                elif "aligned 0 times" in line:
                    unmapped = int(re.split("\s+", line)[1])
                    values += "," + str(total-unmapped)
                elif "exactly 1 time" in line:
                    values += ',' + re.split("\s+", line)[1]
                elif ">1 times" in line:
                    values += "," + re.split("\s+", line)[1]
            values += "," + str(unmapped)
            self._add_result_element(name, values, "string", "bowtie")
       
        # Add ref versions
            if len(self.versions)>0: 
                self._add_result_element(name, self.versions[cpt], "string", "version")
                
        # Filter BAM logs
        self._add_result_element("keys", "Total,Mapped,Unmapped,INDEL,Too many mismatches,Too many locations,Filtered", "string", "filter")
        reflist = ["nb_seq"]
        vallist = [str(total)]
        for i in self.log_files:
            values = ""
            name = os.path.basename(i.split(".log")[0])
            reflist.append(name)
            fh = open(i,"r")
            values  = re.split("\t", fh.readline().rstrip())[1]
            values += "," + re.split("\t", fh.readline().rstrip())[1]
            values += "," + re.split("\t", fh.readline().rstrip())[1]
            values += "," + re.split("\t", fh.readline().rstrip())[1]
            values += "," + re.split("\t", fh.readline().rstrip())[1]
            values += "," + re.split("\t", fh.readline().rstrip())[1]
            tmp = re.split("\t", fh.readline().rstrip())[1]
            values += "," + tmp
            vallist.append(tmp)
            self._add_result_element(name, values, "string", "filter")
 
        # Global stat
        self._add_result_element("annot_keys",   ",".join([i for i in reflist]), "string", "default")
        self._add_result_element("annot_values", ",".join([i for i in vallist]), "string", "default")

    def process(self):
        for cpt, ibam in enumerate(self.bam_files):
            filter = self.add_python_execution(filter_bam, 
                                               inputs=[ibam], 
                                               arguments=[self.mismatchs[cpt], self.max_best_hits[cpt], self.indels[cpt]],
                                               outputs=[self.stderr_files[cpt], self.log_files[cpt], self.filtered_sam_files[cpt]], 
                                               cmd_format='{executable} {inputs} ' +self.get_exec_path("samtools")+' {arguments} {outputs}' )
    
            cmd =   self.get_exec_path("samtools") + " view -bS $1 2>> $3 | " \
                    + self.get_exec_path("samtools") + " sort - $2 2>> $3;" \
                    "mv $2.bam $2;"\
                    "rm -f $1;"
            
            sam_to_bam = self.add_shell_execution(cmd, 
                                                  inputs=self.filtered_sam_files[cpt], 
                                                  outputs=[self.filtered_bam_files[cpt], self.stdout_files[cpt]],  
                                                  cmd_format='{EXE} {IN} {OUT}')
            