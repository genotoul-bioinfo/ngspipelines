#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from jflow.component import Component

class GetAnnotatedLoci (Component):
    
    def define_parameters(self, predictions, annotations):        
        """
             @param predictions: [list] prediction files
             @param annotations: [file] annotation files
        """
        self.add_input_file_list("predictions", "GFF3 predictions file", default=predictions, required=True)
        self.add_input_file_list("annotations", "GFF3 predictions file", default=annotations, required=True)
        self.add_output_file_list("tmp1", "GFF3 file of sorted predictions", pattern='{basename_woext}.sort.gff3', items=self.predictions)
        self.add_output_file_list("tmp2", "GFF3 file of sorted annotations", pattern='{basename_woext}.sort.gff3', items=self.annotations)
        self.add_output_file_list("annotated_regions", "GFF3 file of unpredicted loci", pattern='{basename_woext}.kept.gff3', items=self.predictions)
        
    def process(self):
        cmd1 = "sort -k1,1 -k4,4n $1 > $3"
        cmd2 = "sort -k1,1 -k4,4n $2 > $4"
        cmd3 = self.get_exec_path("intersectBed") + " -sorted -s " + "-a " + "$3" + " -b " + "$4" + " -wa |sort |uniq |sort -k1,1 -k4n,4 > " + "$5"
        all_cmd = cmd1+";"+cmd2+";"+cmd3
        
        self.add_shell_execution(all_cmd,
                                 cmd_format='{EXE} {IN} {OUT}',
                                 inputs=[self.predictions, self.annotations],
                                 outputs=[self.tmp1, self.tmp2, self.annotated_regions],
                                 map=True
                                 )