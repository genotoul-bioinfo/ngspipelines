#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from jflow.component import Component

def annot(fasta, prediction, blast_path, blast_evalue, blast_percent_ident, blast_align_length, blast_nm, blast_gap, blast_bit_score, databanks, databanks_id, out_dir, separator, out_log, out_gff3):
    """
    @summary: 
        @param fasta: [file] the FASTA file
        @param blast_path: [str] the blast path
        @param blast_evalue: [str] min e-value
        @param blast_percent_ident: [str] min percent identity
        @param blast_align_length: [str] min alignment length
        @param blast_nm: [str] max nm
        @param blast_gap: [str] max gap
        @param blast_bit_score: [str] min bit-score
        @param databanks: [list] Databank files
        @param databanks_id: [list] Databank ids
        @param out_dir: [str] output directory
        @param separator: [str] separator
        @param out_pickle: [file] the output pickle file
    @requires: utils.blast()
    """
    import workflows.srnaseq.lib.utils as utils
    from jflow.seqio import FastaReader
    from jflow.featureio import GFF3Record
    from jflow.featureio import GFF3IO
    
    dic_infos ={}
    gff3_reader = GFF3IO(prediction, "r")
    for entry in gff3_reader:
        dic_infos[entry.attributes["ID"]]=[]
        dic_infos[entry.attributes["ID"]].append(entry.seq_id)
        dic_infos[entry.attributes["ID"]].append(entry.start)
        dic_infos[entry.attributes["ID"]].append(entry.end)
        dic_infos[entry.attributes["ID"]].append(entry.strand)
    
    separator = "|"    
    log = open(out_log,"w")
    out_gff3 = open(out_gff3,'w')
    out_gff3.write("##gff-version 3\n")
    # Stores databanks id
    databanks_id = databanks_id.split(",")
    databanks = databanks.split(",")
    blast_evalue = blast_evalue.split(",")
    blast_percent_ident = blast_percent_ident.split(",")
    blast_align_length = blast_align_length.split(",")
    blast_nm = blast_nm.split(",")
    blast_gap = blast_gap.split(",")
    blast_bit_score = blast_bit_score.split(",")
    
    dic_databanks = {}    
    for i in range(0,len(databanks)):
        dic_databanks[databanks[i]]=databanks_id[i]
    
    cpt=0
    annotation_details = {} # To avoid double annotations
    file = FastaReader(fasta)
    for seq in file:
        cpt+=1
        #infos = seq.name.split("|")
        seq_name = seq.name
        chr = dic_infos[seq_name][0]
        start = dic_infos[seq_name][1]
        stop = dic_infos[seq_name][2]
        strand = dic_infos[seq_name][3]
        locus_id = os.path.basename(fasta)+"_"+seq_name
        temp_seq = open(os.path.join(out_dir,"seq_"+locus_id+".fa"),"w")
        temp_seq.write(">"+locus_id+"\n"+seq.sequence)
        temp_seq.close()
        for j in range(0,len(databanks)):
            annot = utils.blast(log, blast_path, databanks[j], databanks_id[j], temp_seq.name, blast_evalue[j], blast_percent_ident[j], blast_align_length[j], blast_nm[j], blast_gap[j], blast_bit_score[j], separator)
            if "score" in annot:
                record = GFF3Record.fromGff(chr+"\t"+databanks_id[j]+"\t"+"match_part"+"\t"+str(int(start)+int(annot["query_start"])-1)+"\t"+str(int(start)+int(annot["query_stop"])-1)+"\t"+annot["score"]+"\t"+strand+"\t"+"."+"\t"+"Name="+seq.name+";")
                record.setAttribute("id",annot["id"])
                record.setAttribute("hit",annot["hit"])
                record.setAttribute("species",annot["species"])
                record.setAttribute("percent_ident",annot["percent_id"])
                record.setAttribute("align_length",annot["align_length"])
                record.setAttribute("mism",annot["mismatchs"])
                record.setAttribute("gap",annot["gaps"])
                record.setAttribute("sbjt_start",annot["subject_start"])
                record.setAttribute("sbjt_stop",annot["subject_stop"])
                record.setAttribute("evalue",annot["evalue"])
                record.setAttribute("description",annot["description"])
                if not annot["description"] in annotation_details:
                    out_gff3.write(record.toGff()+"\n")
                    annotation_details[annot["description"]]=1
        
        try:
            os.remove(temp_seq.name)
        except OSError:
            sys.exit("Unable to remove seq_"+temp_seq.name)

    log.close()

class FunctionalAnnotation(Component):
    
    def define_parameters(self, fastas, predictions, blast_evalues, blast_percent_idents, blast_align_lengths, blast_nms, blast_gaps, blast_bit_scores, databanks, databanks_ids, separator='!'):
        """
            @param fasta: [list] FASTA files
            @param blast_evalue: [str] Blast min e-value
            @param blast_percent_ident: [str] Blast min percent identity
            @param blast_align_length: [str] Blast min align_length
            @param blast_nm: [str] Blast max nm
            @param blast_gap: [str] Blast max gap
            @param blast_bit_score: [str] Blast min bit-score
            @param databanks: [list] databank files
            @param databanks_id: [list] databank ids
            @param separator: [char] separator
        """
        self.add_input_file_list("fastas", 'FASTA files', default=fastas, required=True)
        self.add_input_file_list("predictions", 'GFF3 files', default=predictions, required=True)
        self.add_parameter_list("blast_evalues", "Blast e-value", default=blast_evalues, required=True)
        self.add_parameter_list("blast_percent_idents", "Blast percent identities", default=blast_percent_idents, required=True)
        self.add_parameter_list("blast_align_lengths", "Blast align length", default=blast_align_lengths, required=True)
        self.add_parameter_list("blast_nms", "Blast nm", default=blast_nms, required=True)
        self.add_parameter_list("blast_gaps", "Blast gap", default=blast_gaps, required=True)
        self.add_parameter_list("blast_bit_scores", "Blast bit score", default=blast_bit_scores, required=True)
        self.add_parameter("separator", "Separator", default=separator, required=True)
        self.add_input_file_list("databanks", "List of databank files", default=databanks, required=True)
        self.add_parameter_list("databanks_ids", "List of databank ids", default=databanks_ids, required=True)
        self.add_output_file_list("functional_annotations", "List of GFF3 files", pattern='{basename_woext}.annot.gff3', items=self.fastas)
        self.add_output_file_list("logs", "List of log files", pattern='{basename_woext}.log', items=self.fastas)
        
    def process(self):
        parameters = [self.get_exec_path("blastall"), ",".join(self.blast_evalues), ",".join(self.blast_percent_idents), ",".join(self.blast_align_lengths), 
                      ",".join(self.blast_nms), ",".join(self.blast_gaps), ",".join(self.blast_bit_scores), ",".join(self.databanks), ",".join(self.databanks_ids),
                      self.output_directory, self.separator]
        self.add_python_execution(annot,
                                  cmd_format='{executable} ' + ' {inputs} ' + " ".join(parameters) + ' {outputs} ',
                                  inputs=[self.fastas, self.predictions],
                                  outputs=[self.logs, self.functional_annotations],
                                  includes=[self.databanks],
                                  map=True
                                  )
        