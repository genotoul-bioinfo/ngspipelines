#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from jflow.component import Component

def get_structure_infos(predictions, *structures):
    from jflow.featureio import GFF3IO
    import re
    
    predictions = predictions.split(",")
    dic_strand_pred = {}
    nb_predictions = 0
    for file in predictions:
        gff3_reader = GFF3IO(file,"r")
        for entry in gff3_reader:
            nb_predictions+=1
            try:
                pred_id = entry.attributes["Parent"]
            except:
                pred_id = entry.attributes["ID"]
            dic_strand_pred[pred_id]=entry.strand
    
    dic_personal_struct = {}
    for file in structures:
        file = open(file,"r")
        for line in file:
            if line.startswith(">"):
                id = line.rstrip().replace(">","")
                if id in dic_personal_struct:
                    sys.exit(id + "is present at least two times in structure files ! Ids must be unique !")
                else:
                    dic_personal_struct[id]={}
            elif "(" in line:
                infos = re.split("\s+",line.rstrip())
                if dic_strand_pred[id] == "-":
                    struc = infos[0][::-1].replace("(","&").replace(")","(").replace("&",")")
                else:
                    struc = infos[0]
                freenrj = infos[1].replace("(","").replace(")","")
                dic_personal_struct[id]["struc"]=struc
                dic_personal_struct[id]["freenrj"]=freenrj
            else:
                pass
        file.close()
    
    return dic_personal_struct
    
class GetComputedStructures (Component):
    
    def define_parameters(self, structures, predictions):
        """
        @summary: Extracts isoform informations for each prediction
            @param bam_file: [file] BAM file
            @param gff3_prediction_files_list: [list] The list of GFF3 prediction files
            @param count_matrix_file: [file] The count matrix file
            @param annotation_matrix_file: [file] The reads annotation file
            @note: pfile            
        """
        self.add_input_file_list("structures", "structures files", default=structures, required=True)
        self.add_input_file_list("predictions", "The list of GFF3 prediction files", default=predictions, required=True)
        self.add_output_object("dic", "Output object")
        
    def process(self):
        cmd_format = '{executable} {outputs} ' +  ",".join(self.predictions) + ' {inputs} '
        self.add_python_execution(get_structure_infos,
                                  cmd_format=cmd_format,
                                  inputs=[self.structures],
                                  outputs=[self.dic],
                                  includes = [self.predictions])
        