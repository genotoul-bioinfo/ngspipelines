#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from jflow.component import Component

    
def is_fasta_indexed(ref_fa):
    """
    @summary: Checks if the genome's index files are present
    @see: __init__.py
        @param ref_fa: [file] the genome fasta 
    @return: [Boolean]
    """
    
    list_ext = [".nhr",".nsq",".nin"]
    all_indexed = 0
    for ext in list_ext:
        index=ref_fa+ext
        if not os.path.isfile(index):
            return False
        else:
            all_indexed+=1

    if all_indexed == 3:
        return True
    else:
        return False

def blast_formatdb(input_fasta, databank, stdout_path, stderr_path, exec_path, type):
    """ 
    @summary: Blast Index a FASTA file
        @param input_fasta: the FASTA to index [FILE]
        @param databank: the link to the FASTA [FILE]
        @param stdout_path: the stdout_path [FILE]
        @param stderr_path: the stderr path [FILE]
        @param exec_path: the exec path of formatdb [BIN]
        @param type: the type (protein or nucl) [char]
    """
    from subprocess import Popen, PIPE
    # first make the symbolic link
    os.symlink(input_fasta, databank)
    # then execute bwa index
    cmd = [exec_path, "-p", type, "-i", databank]
    p = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    # write down the stdout
    stdoh = open(stdout_path, "w")
    stdoh.write(stdout)
    stdoh.close()
    # write down the stderr
    stdeh = open(stderr_path, "w")
    stdeh.write(stderr)
    stdeh.close()
        
class BlastallIndex (Component):

    def define_parameters(self, files, protein=False):
        """
            @param fasta_file: [file] The FASTA file to index
            @param protein: [boolean] Protein FASTA file ?
        """
        self.add_input_file_list("files", "FASTA files to index", default=files, required=True)
        self.add_parameter("protein", "Proteins in file ?", default=False, required=True)
        self.add_output_file_list("stderr_files", "stderr file", pattern="{basename_woext}.stderr", items=self.files)
        self.add_output_file_list("stdout_files", "stdout file", pattern="{basename_woext}.stdout", items=self.files)
        self.add_output_file_list("indexed_files", "Indexed reference file", pattern="{basename}", items=self.files)
        
    def process(self):
        p = "T" if self.protein == True else "F"
        # Already indexed files
        ai_files, ai_indexed_files, ai_stdout, ai_stderr = [], [], [], []
        # Files to index
        files_to_index, indexed_files, stdout_files, stderr_files = [], [], [], []
        
        for cpt, file in enumerate(self.files):
            if is_fasta_indexed(file):
                ai_files.append(file)
                ai_indexed_files.append(self.indexed_files[cpt])
                ai_stdout.append(self.stdout_files[cpt])
                ai_stderr.append(self.stderr_files[cpt])
            else:
                files_to_index.append(file)
                indexed_files.append(self.indexed_files[cpt])
                stdout_files.append(self.stdout_files[cpt])
                stderr_files.append(self.stderr_files[cpt])
        
        # Files to be indexed
        if len(files_to_index) > 0:
            index =  self.add_python_execution(blast_formatdb,
                                              inputs=files_to_index, outputs=[indexed_files, stdout_files, stderr_files], 
                                              cmd_format="{EXE} {IN} {OUT} " + self.get_exec_path("formatdb") + " " + p, map=True)
        # Symlink of already indexed files
        if len(ai_files) > 0:
            sym_link = self.add_shell_execution("ln -s $1 $2 > $3 2>> $4; ln -s $1*.n* "+ self.output_directory + " > $3 2>> $4;", 
                                              inputs=ai_files, outputs=[ai_indexed_files, ai_stdout, ai_stderr], 
                                              cmd_format="{EXE} {IN} {OUT}", map=True)
        
        
        
        