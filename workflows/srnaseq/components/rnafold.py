#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, subprocess
from jflow.component import Component
from weaver.function import ShellFunction

class RNAfold (Component):
    
    def define_parameters(self, fasta_file, options="--noPS -c"):
        """
        @summary: Performs RNAfold on a FASTA file
            @param fasta_file : [file] FASTA file
        """
        self.add_input_file("fasta_file", "FASTA file", default=fasta_file, required=True)
        self.add_parameter("options", "RNAfold options", default=options, required=True)
        self.add_output_file("rnafold_file", "", filename='predictions_struct.csv')
    
    def get_version(self):
        cmd = [self.get_exec_path("RNAfold"),"--version"]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()        
        return stdout.split()[1]
    
    def process(self):
        rnafold = ShellFunction(self.get_exec_path("RNAfold") + " " + self.options + " < $1 > $2", cmd_format='{EXE} {IN} {OUT}')
        rnafoldrun = rnafold(inputs=[self.fasta_file], outputs=[self.rnafold_file])
