#
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from jflow.component import Component

def best_hit( out_best, out_all, *ind):
    """
    TODO
    """
    
    from jflow.featureio import GFF3IO
    
    out_all_fh = GFF3IO(out_all,'w')
    dic_best_annot={}
    for i in ind:
        reader = GFF3IO(i, "r")
        for entry in reader:
            out_all_fh.write(entry)
            if entry.attributes["Parent"] in dic_best_annot:
                if float(entry.score)>dic_best_annot[entry.attributes["Parent"]].score:
                    dic_best_annot[entry.attributes["Parent"]]=entry
            else:
                dic_best_annot[entry.attributes["Parent"]]=entry
                
    out_fh = GFF3IO(out_best,'a')
    for i in dic_best_annot:
        out_fh.write(dic_best_annot[i])
    
    
    
    
class BestAnnotSearch (Component):
    """
    TODO
    """

    def define_parameters(self, annotations):
        """
        TODO
        """
        
        self.add_input_file_list( "annotations", "List of GFF3 annotations files", default=annotations,required=True )
        self.add_output_file("best_annotations", "GFF3 with only best annotations", filename="best_annotations.gff3")
        self.add_output_file("all_annotations", "GFF3 with all annotations", filename="all_annotations.gff3")
        

    def process(self):
        self.add_python_execution(best_hit, outputs=[self.best_annotations, self.all_annotations], inputs=[self.annotations], cmd_format='{EXE} {OUT} {IN}')
        
            