#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from jflow.component import Component

def get_predictions_infos(bam, matrix, samtools_path, *gff3_prediction_list):
    from jflow.featureio import GFF3IO
    import subprocess
    from operator import itemgetter
    import workflows.srnaseq.lib.utils as utils
    
    dic_strand = utils.get_strand_signification()
    (dic_matrix, dic_total_lib) = utils.load_count_matrix(matrix)
    dic_predictions_infos = {}
    dic_predictions_coord = {}
    
    cmd = samtools_path + " view -F 4 " + bam
    proc = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)
    dic_forward = {}
    dic_reverse = {}
    
    for line in iter(proc.stdout.readline,''):
        l = line.split()
        chromosome = l[2]
        isoform_name = l[0]
        isoform_start = int(l[3])
        isoform_end = int(isoform_start) + len(l[9]) - 1
        if dic_strand[l[1]] == "-":
            if chromosome in dic_reverse:
                dic_reverse[chromosome].append(isoform_name+"|"+str(isoform_start)+"|"+str(isoform_end))
            else:
                dic_reverse[chromosome]=[]
                dic_reverse[chromosome].append(isoform_name+"|"+str(isoform_start)+"|"+str(isoform_end))
        else:
            if chromosome in dic_forward:
                dic_forward[chromosome].append(isoform_name+"|"+str(isoform_start)+"|"+str(isoform_end))
            else:
                dic_forward[chromosome]=[]
                dic_forward[chromosome].append(isoform_name+"|"+str(isoform_start)+"|"+str(isoform_end))
    
    dic_counts = {}
    
    
    for gff in gff3_prediction_list:
        reader = GFF3IO(gff, "r")
        for entry in reader:
            if entry.type.endswith("primary_transcript"):
                locus_name = entry.attributes["ID"]
                chromosome = entry.seq_id
                score = entry.score
                locus_start = entry.start
                locus_end = entry.end
                strand = entry.strand
                family = entry.type.split("_primary_transcript")[0]
                if not family in dic_counts:
                    dic_counts[family]=0
                dic_counts[family]+=1
                dic_predictions_infos[locus_name]={}
                
                list_aln = []
                nb_reads_locus = 0 # Not used because nb_reads is based on the majoritary. Maybe will change...
                if strand == "+":
                    if chromosome in dic_forward:
                        for i in dic_forward[chromosome]:
                            nb_reads=0
                            dic_aln = {}
                            isoform_name = i.split('|')[0]
                            isoform_start = int(i.split('|')[1])
                            isoform_end = int(i.split('|')[2])
                            isoform_length = isoform_end - isoform_start + 1
                            
                            if ((isoform_start >= locus_start) and (isoform_end <= locus_end)):
                            
                                dic_aln["name"]=isoform_name
                                dic_aln["start"]=isoform_start
                                dic_aln["end"]=isoform_end
                                
                                for (sample, reads) in dic_matrix[isoform_name].items():
                                    nb_reads += int(reads)
                                dic_aln["nb_reads"]=nb_reads
                                list_aln.append(dic_aln)
                                nb_reads_locus += nb_reads
                else:
                    if chromosome in dic_reverse:
                        for i in dic_reverse[chromosome]:
                            nb_reads=0
                            dic_aln = {}
                            isoform_name = i.split('|')[0]
                            isoform_start = int(i.split('|')[1])
                            isoform_end = int(i.split('|')[2])
                            isoform_length = isoform_end - isoform_start + 1
                            
                            if ((isoform_start >= locus_start) and (isoform_end <= locus_end)):
                            
                                dic_aln["name"]=isoform_name
                                dic_aln["start"]=isoform_start
                                dic_aln["end"]=isoform_end
                                
                                for (sample, reads) in dic_matrix[isoform_name].items():
                                    nb_reads += int(reads)
                                dic_aln["nb_reads"]=nb_reads
                                list_aln.append(dic_aln)
                                nb_reads_locus += nb_reads
                
                
                if len(list_aln)>0:
                    sorted_list = sorted(list_aln, key=itemgetter("nb_reads"), reverse=True)
                    best = sorted_list[0]
                    dic_predictions_infos[locus_name]["nb_reads"]=best["nb_reads"]
                    dic_predictions_infos[locus_name]["score"]=score
                    dic_predictions_infos[locus_name]["start"]=entry.attributes["StartPred"] if "StartPred" in entry.attributes else best["start"]
                    dic_predictions_infos[locus_name]["end"]=entry.attributes["StopPred"] if "StopPred" in entry.attributes else best["end"]
                    
                else:
                    dic_predictions_infos[locus_name]["score"]=score
                    dic_predictions_infos[locus_name]["start"]=locus_start
                    dic_predictions_infos[locus_name]["end"]=locus_end
                    dic_predictions_infos[locus_name]["nb_reads"]=0                
            else:
                locus_name = entry.attributes["Parent"]
                dic_predictions_coord[locus_name]={}
                dic_predictions_coord[locus_name]["start"] = entry.start
                dic_predictions_coord[locus_name]["end"] = entry.end
    
    for i in dic_predictions_coord.iterkeys():
        dic_predictions_infos[i]["start"]=dic_predictions_coord[i]["start"]
        dic_predictions_infos[i]["end"]=dic_predictions_coord[i]["end"]
    
    dic=[]
    dic.append(dic_predictions_infos)
    dic.append(dic_counts)
    return dic[0]
    
class GetPredictionsInfos (Component):
    
    def define_parameters(self, bam, predictions, count_matrix):
        """
        @summary: 
            @param bam: [file] BAM file
            @param predictions: [list] The list of GFF3 prediction files
            @param count_matrix: [file] The count matrix file
            @note: pfile
            {'srnaplan_miRNA_21': 
                { 'start': '1873262',
                  'score': '155.0',
                  'end': 1873283,
                  'nb_reads': 571
                }
        """
        self.add_input_file("bam", "BAM file", default=bam, required=True)
        self.add_input_file_list("predictions", "The list of GFF3 prediction files", default=predictions, required=True)
        self.add_input_file("count_matrix", "The count matrix file", default=count_matrix, required=True)
        self.add_output_object("dic", "Output object")
        
    def process(self):
        cmd_format = '{executable} {outputs} ' +  self.bam + ' ' + self.count_matrix + ' ' + self.get_exec_path("samtools") + ' {inputs} '
        self.add_python_execution(get_predictions_infos,
                                  cmd_format=cmd_format,
                                  inputs=[self.predictions],
                                  outputs=[self.dic],
                                  includes=[self.bam, self.count_matrix]
                                  )
        
        