#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from jflow.component import Component

def predict_miRNA(locus_file, genome_id, clade, hybridation_matrix_file, stat_file, genome, out_dir, ssearch_path, samtools_path, 
                  min_appariements, min_expr_level, min_loop_size, max_loop_size,
                  min_majseq_size, max_majseq_size, max_cpt_bulges, max_max_bulge, 
                  out_gff3, out_fasta, out_struc, log):
    """
    @summary: Predicts miRNA from loci. Process ssearch36 with majoritary sequence of a locus and its neighborhood. Keeps ssearch36 result if it looks like
              a miRNA (miRbase stats and expression profile)
        @param locus_file: [file] containing locus
        @param hybridation_matrix_file: [file] empty hybridation matrix file
        @param stat_file: [file] miRbase stats
        @param genome: [file] FASTA reference genome
        @param out_dir: [str] output directory
        @param ssearch_path: [str] executable path
        @param samtools_path: [str] executable path
        @param min_appariements: [str] min appariements to keep a ssearch result
        @param min_expr_level: [str] min expression level to keep a prediction
        @param min_loop_size: [str] min loop size to keep a ssearch result
        @param max_loop_size: [str] max loop size to keep a ssearch result
        @param min_majseq_size: [str] min size of majoritary sequence to keep a ssearch result
        @param max_majseq_size: [str] max size of majoritary sequence to keep a ssearch result
        @param max_cpt_bulges: [str] max cpt bulges
        @param max_max_bulges: [str] max max bulge
        @param out_gff3: [file] GFF3 output
        @param out_fasta: [file] FASTA output
        @param out_struc: [file] structure output
        
    @requires: utils.create_hybridation_matrix()
    @requires: utils.reverse()
    @requires: utils.is_already_treated()
    @requires: utils.find_nearest_coordinate()
    @requires: utils.ssearch36()
    @requires: utils.best_ssearch()
    @requires: utils.locus ###
    @requires: Locus.reformat_tab_seq()
    @requires: Locus.get_maj_seq()
    @requires: Locus.compute_tab_coverage()
    @requires: Locus.update_coord_majseq()
    @requires: Locus.update_locus()
    @requires: Locus.find_coord_locus()
    @requires: SsearchMapping.is_enough_good() ###
    @requires: SsearchMapping.compute_bulges()
    @requires: SsearchMapping.compute_global_score()
    @requires: PreMiRNA.find_coordinates_premir()
    @requires: PreMiRNA.seems_to_be_a_mirna()
    @requires: PreMiRNA.get_seq_in_zone_premir()
    @requires: PreMiRNA.premir_coverage()
    @requires: PreMiRNA.compute_structure()
    @requires: utils.Profiler
    @requires: Profil.setTreshold()
    @requires: Profil.setPlateauShape()
    @requires: Profil.setLoopSize()
    @requires: Profil.setMinValleyRatio()
    @requires: Profil.run()
    """
    import workflows.srnaseq.lib.miRNA as miRNA
    import workflows.srnaseq.lib.utils as utils
    import workflows.srnaseq.lib.locus as locus
    import workflows.srnaseq.lib.profiler as profiler
    import subprocess, copy, re
    
    OVERHANG = 5
    min_expr_level = int(min_expr_level)
    min_loop_size = int(min_loop_size)
    max_loop_size = int(max_loop_size)
    min_majseq_size = int(min_majseq_size)
    max_majseq_size = int(max_majseq_size)
    max_cpt_bulges = int(max_cpt_bulges)
    max_max_bulge = int(max_max_bulge)
    min_appariements = int(min_appariements)
    all_predictions = []
    chromosome=""
    
    #locus_file = open(locus_file,"r")
    log = open(log,"w")
    
    # Writes the hybridation matrix file
    hybridation_matrix = open(hybridation_matrix_file,"w")
    utils.create_hybridation_matrix(hybridation_matrix.name)

    # If no values defined for the specific characteristics, get them in the stats file
    if stat_file != 'None':
        stats = open(stat_file,"r")
        (min_appariements, min_loop_size, max_loop_size, min_majseq_size, max_majseq_size, max_cpt_bulges, max_max_bulge) = utils.get_parameters_from_mirbase(min_appariements, min_loop_size, max_loop_size, min_majseq_size, max_majseq_size, max_cpt_bulges, max_max_bulge, stats)
        stats.close()
    space_locus = max_loop_size + max_majseq_size
    
    
    #for line in locus_file:
    
    locus_reader = locus.LocusReader(locus_file, "r")
    file_id = os.path.basename(locus_file).split(".")[0]
    cpt_premir = 0
    for new_locus in locus_reader:
        length_potential_mature = new_locus.stop_majseq - new_locus.start_majseq +1
        
        # Store the genome sequence
        if not (new_locus.chromosome == chromosome):
            cmd = samtools_path + " faidx " + genome + " " + new_locus.chromosome
            proc = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = proc.communicate()
            lines_genome = stdout.split()
            tab_genome = utils.get_seq_from_fasta(lines_genome)
            size_chromosome = len(tab_genome)
            chromosome = new_locus.chromosome
        
        # Save the locus coverage
        original_tab_seq = copy.deepcopy(new_locus.tab_seq)
        locus_nb_isoforms = new_locus.nb_isoforms

        cpt=0
        coord_zones_treated = []
        log.write("### " + new_locus.name + " " + new_locus.chromosome+":"+str(new_locus.start)+"-"+str(new_locus.stop) + " ###\n")
        index_analyzed_seq = 0
        while(new_locus.majseq!=""):
            index_analyzed_seq += 1
            log.write("... " + new_locus.majseq + " ...\n")
            tab_two_ssearch=[]
            inspect_before = False
            inspect_after = False
            compute_seq_for_files = True
            new_locus.update_coord_majseq()
            
            length_mature = new_locus.stop_majseq-new_locus.start_majseq+1
            
            # TEST 1 : Size of MajSeq
            if (length_mature < min_majseq_size or length_mature > max_majseq_size):
                log.write("TEST1 wrong\t"+new_locus.name+"\t"+new_locus.chromosome+":"+str(new_locus.start_majseq)+"-"+str(new_locus.stop_majseq)+"\t"+str(new_locus.majseq)+"\tBad length of mature seq :"+str(min_majseq_size)+" < "+str(length_mature)+" < "+str(max_majseq_size)+"\n")
            else:
                # TEST 2 : Expression of MajSeq
                expr_maj = int(new_locus.majseq.split("#")[2].split("_")[0])
                if expr_maj<min_expr_level:
                    log.write("TEST2 wrong\t"+new_locus.name+"\t"+new_locus.chromosome+":"+str(new_locus.start_majseq)+"-"+str(new_locus.stop_majseq)+"\t"+str(new_locus.majseq)+"\t"+str(new_locus.majseq.split("#")[2])+"<"+str(min_expr_level)+"\n")
                else:
                    if not utils.is_already_treated(coord_zones_treated,max(0,new_locus.start_majseq),new_locus.stop_majseq-1):
                        inspect_after = True
                    if not utils.is_already_treated(coord_zones_treated,new_locus.start_majseq,min(new_locus.stop_majseq,size_chromosome)):
                        inspect_before = True
                    
                    if inspect_after:
                        if compute_seq_for_files:
                            # Get sequence of maj seq
                            tab_maj_sequence = tab_genome[new_locus.start_majseq-1:new_locus.stop_majseq:1]
                            maj_sequence = "".join(tab_maj_sequence)
                            maj_sequence_reverse = utils.reverse(maj_sequence)
                            maj_seq_id = new_locus.chromosome+"_"+str(new_locus.start_majseq)+"_"+str(cpt)
                            cpt+=1
                            # Write files for ssearch
                            potential_mir_file = open(os.path.join(out_dir,"pm_"+maj_seq_id+".fa"),"w")
                            potential_mir_file.write(">"+maj_seq_id+"\n")
                            for i in maj_sequence_reverse:
                                potential_mir_file.write(i)              
                            potential_mir_file.close()
                            # Open files for ssearch
                            potential_mir_file = open(potential_mir_file.name,"r")
                            compute_seq_for_files = False
                        
                        # Ssearch with left zone [MajSeq stands AFTER]
                        down_nearest_position = utils.find_nearest_coordinate(coord_zones_treated, new_locus.start_majseq, space_locus, "AFTER")
                        down_nearest_position = max( 0, down_nearest_position+1)
                        
                        if( new_locus.start_majseq-down_nearest_position ) >= (min_loop_size + min_majseq_size):
                            after_samtools = "<("+samtools_path+" faidx "+ genome + " " + new_locus.chromosome + ":" + str(down_nearest_position) + "-" + str(int(new_locus.start_majseq)-1) + ')'
                            list_ssearch = utils.ssearch36(ssearch_path, hybridation_matrix.name, potential_mir_file.name, after_samtools, "AFTER")
                            if isinstance(list_ssearch, list):
                                best = utils.best_ssearch(list_ssearch)
                                tab_two_ssearch.append(best)
        
                    if inspect_before:
                        if compute_seq_for_files:
                            # Get sequence of maj seq
                            tab_maj_sequence = tab_genome[new_locus.start_majseq-1:new_locus.stop_majseq:1]
                            maj_sequence = "".join(tab_maj_sequence)
                            maj_sequence_reverse = utils.reverse(maj_sequence)
                            maj_seq_id = new_locus.chromosome+"_"+str(new_locus.start_majseq)+"_"+str(cpt)
                            cpt+=1
                            # Write files for ssearch
                            potential_mir_file = open(os.path.join(out_dir,"pm_"+maj_seq_id+".fa"),"w")
                            potential_mir_file.write(">"+maj_seq_id+"\n")
                            for i in maj_sequence_reverse:
                                potential_mir_file.write(i)              
                            potential_mir_file.close()
                            # Open files for ssearch
                            potential_mir_file = open(potential_mir_file.name,"r")
                            compute_seq_for_files = False
                        
                        # Ssearch with right zone [MajSeq stands BEFORE]
                        up_nearest_position = utils.find_nearest_coordinate(coord_zones_treated, new_locus.stop_majseq, space_locus, "BEFORE")
                        up_nearest_position = min( up_nearest_position+1, size_chromosome )
                        
                        if( up_nearest_position - new_locus.stop_majseq ) >= (min_loop_size + min_majseq_size):
                            before_samtools = "<("+samtools_path+" faidx "+ genome + " " + new_locus.chromosome + ":" + str(new_locus.stop_majseq+1) + "-" + str(up_nearest_position) + ')'
                            list_ssearch = utils.ssearch36(ssearch_path, hybridation_matrix.name, potential_mir_file.name, before_samtools, "BEFORE")
                            if isinstance(list_ssearch, list):
                                best = utils.best_ssearch(list_ssearch)
                                tab_two_ssearch.append(best)
                    
                    # Close files
                    if inspect_before or inspect_after:
                        potential_mir_file.close()
                        try:
                            os.remove(potential_mir_file.name)
                        except OSError:
                            sys.exit("Unable to remove pm_"+maj_seq_id+".fa")
                    
                    # GET THE BEST SSEARCH OBJECT
                    supra_best = utils.best_ssearch(tab_two_ssearch)
                    
                    # TEST 3 : Does MajSeq aligns against neighborhood ?
                    if supra_best is None:
                        log.write("TEST3 wrong\t"+new_locus.name+"\t"+new_locus.chromosome+":"+str(new_locus.start_majseq)+"-"+str(new_locus.stop_majseq)+"\t"+str(new_locus.majseq)+"\n")
                    else:
                        # Computes coordinates of mature and star                    
                        if supra_best.zone=="AFTER":
                            start_mature = new_locus.start_majseq + (length_mature - supra_best.query_stop)
                            stop_mature = new_locus.stop_majseq - supra_best.query_start + 1
                            supra_best.loop = (length_mature - supra_best.query_stop) + ((space_locus if down_nearest_position !=0 else 0) - supra_best.library_stop) - 1 - (supra_best.query_start -1)
                            stop_star = start_mature - supra_best.loop - (supra_best.query_start - 1) - 1 
                            start_star = stop_star - (supra_best.library_stop - supra_best.library_start)
                        else:
                            start_mature = new_locus.start_majseq + (length_mature - supra_best.query_stop)
                            stop_mature = new_locus.stop_majseq - (supra_best.query_start - 1)
                            supra_best.loop = supra_best.library_start - 1
                            start_star = stop_mature + supra_best.loop + (supra_best.query_start - 1) + 1 
                            stop_star = start_star + (supra_best.library_stop - supra_best.library_start)
                        
                        # Computes coordinates of the current PreMiRNA
                        start_zone = min(start_mature,start_star)
                        stop_zone = max(stop_mature,stop_star)
                        mirna_sequence = "".join(tab_genome[start_mature-1:stop_mature:1])
                        star_sequence = "".join(tab_genome[start_star-1:stop_star:1])
                        
                        # Constructs the PreMiRNA object
                        new_premir = miRNA.PreMiRNA("miRNA_"+file_id+"_"+str(cpt_premir), genome_id, new_locus.chromosome, new_locus.strand, length_mature, start_mature, stop_mature, start_star, stop_star, supra_best.loop, supra_best.appariements, mirna_sequence, star_sequence)
                        new_premir.set_locus_nb_isoforms(locus_nb_isoforms)
                        new_premir.set_size_mature_sequence(length_mature)
                        cpt_premir+=1
                        new_premir.clade = clade
                        new_premir.start_majseq = new_locus.start_majseq
                        new_premir.stop_majseq = new_locus.stop_majseq
                        new_premir.find_coordinates_premir(new_locus.start_majseq, new_locus.stop_majseq)
                        new_premir.ssearch = supra_best
                        new_premir.majseq = new_locus.majseq
                        new_premir.tab_seq = new_premir.get_seq_in_zone_premir(original_tab_seq, start_zone, stop_zone)
                        new_premir.find_coordinates_premir(new_locus.start_majseq, new_locus.stop_majseq)
                        new_premir.expression = new_premir.premir_coverage(original_tab_seq)
                        new_premir.sequence = "".join(tab_genome[new_premir.start_premir-1:new_premir.stop_premir:1])
                        new_premir.compute_structure(supra_best)
                        (query_max_bulge, query_cpt_bulges) = supra_best.compute_bulges(supra_best.query_structure)
                        (library_max_bulge, library_cpt_bulges) = supra_best.compute_bulges(supra_best.library_structure)
                        supra_best.max_bulge = max(query_max_bulge, library_max_bulge)
                        supra_best.cpt_bulges = max(query_cpt_bulges, library_cpt_bulges)
                        
                        # TEST 4 : Premir looks like a miRNA ?
                        (seems, msg) = new_premir.seems_to_be_a_mirna(min_loop_size, max_loop_size, min_majseq_size, max_majseq_size, max_cpt_bulges, max_max_bulge, min_appariements)
                        if not seems:
                            log.write("TEST4 wrong\t"+new_locus.name+"\t"+new_locus.chromosome+":"+str(new_premir.start_premir)+"-"+str(new_premir.stop_premir)+"\t"+str(new_locus.majseq)+"\t"+msg+"\n")
                            log.write(",".join(str(j) for j in new_premir.expression)+"\n---\n")
                        else:
                            # Check coverage profile
                            profil = profiler.Profiler()
                            profil.setTreshold(0.7)
                            profil.setPlateauShape(14,25,0)
                            profil.setLoopSize(min_loop_size)
                            profil.setMinValleyRatio(10)
                            (passed, mature_and_star, s6, c6, s12, c12) = profil.run(new_premir.expression)
                            # TEST 5 : Premir coverage looks like a miRNA ?
                            if not passed:
                                log.write("TEST5 wrong\t"+new_locus.name+"\t"+new_locus.chromosome+":"+str(new_premir.start_premir)+"-"+str(new_premir.stop_premir)+"\t"+str(new_locus.majseq)+"\t"+ str(",".join(str(i) for i in new_premir.expression))+"\t"+str(s6)+":"+str(c6)+";"+str(s12)+":"+str(c12)+"\n")
                            else:
                                dicer_compliant, drosha_compliant, star_expressed = new_premir.check_structure_and_expression(original_tab_seq, clade, OVERHANG)
                                new_premir.is_dicer_compliant = dicer_compliant
                                new_premir.is_drosha_compliant = drosha_compliant
                                supra_best.length_query = length_potential_mature
                                new_premir.score = supra_best.compute_global_score(stat_file, star_expressed)
                                new_premir.has_star = star_expressed
                                new_premir.start = new_premir.start_premir
                                new_premir.stop = new_premir.stop_premir
                                new_premir.expr_maj = expr_maj
                                new_premir.index_analyzed_seq = index_analyzed_seq
                                new_locus.suppress_seq_in_zone(new_premir.start_premir, new_premir.stop_premir)
                                coord_zones_treated.append(str(new_premir.start_premir)+"-"+str(new_premir.stop_premir))
                                all_predictions.append(new_premir)
                                log.write(" -> miRNA kept\n")
                                log.write(",".join(str(j) for j in new_premir.expression)+"\n---\n")
                        del new_premir
            try:
                new_locus.tab_seq.remove(new_locus.majseq)
            except ValueError:
                pass
            new_locus.update_locus(new_locus.start_majseq,new_locus.stop_majseq)

    out_gff3 = open(out_gff3,"w")
    out_fasta = open(out_fasta,"w")
    out_struc  = open(out_struc,"w")
    cpt=0
    
    for i in all_predictions:
        cpt+=1
        #i.name = "miRNA_"+str(cpt)
        #fasta_id = "|".join([i.name,i.chromosome,str(i.start),str(i.stop),i.strand])
        #i.name = fasta_id
        out_gff3.write(i.write_GFF3().rstrip()+"\n")
        #i.name = "miRNA_"+str(cpt)+"_mature"
        #fasta_id = "|".join([i.name,i.chromosome,str(i.start),str(i.stop),i.strand])
        #i.name = fasta_id
        out_gff3.write(i.write_GFF3_mature().rstrip()+"\n")
        if i.has_star:
            #i.name = "miRNA_"+str(cpt)+"_star"
            #fasta_id = "|".join([i.name,i.chromosome,str(i.start),str(i.stop),i.strand])
            #i.name = fasta_id
            out_gff3.write(i.write_GFF3_star().rstrip()+"\n")
        # -----
        #out_fasta.write(">"+i.name+"\n"+i.mirna_sequence+"\n")
        # A decider si on annote le mature ou le premir
        out_fasta.write(">"+i.name+"\n"+i.sequence+"\n")
        # -----
        out_struc.write(">"+i.name+"\n"+i.sequence+"\n"+i.structure+"\t"+"("+str(0)+")\n")
    out_gff3.close()
    out_fasta.close()
    out_struc.close()
    log.close()

class MIRNAPrediction (Component):
    
    def define_parameters(self, genome_id, locus_files, locus, clade, genome, stats, hybridation_matrix, min_appariements, min_expr_level, min_loop_size, max_loop_size, min_majseq_size, max_majseq_size, max_cpt_bulges, max_bulge_size):
        """
            @param locus_files: [file list] loci
            @param clade: [str]: clade of genome species
            @param genome: [file] FASTA reference genome
            @param stats: [file] miRbase stats
            @param hybridation_matrix: [file] the hybridation matrix file
            @param min_appariements: [int] min appariements to keep a ssearch result
            @param min_expr_level: [int] min expression level to keep a prediction
            @param min_loop_size: [int] min loop size to keep a ssearch result
            @param max_loop_size: [int] max loop size to keep a ssearch result
            @param min_majseq_size: [int] min size of majoritary sequence to keep a ssearch result
            @param max_majseq_size: [int] max size of majoritary sequence to keep a ssearch result
            @param max_cpt_bulges: [int] max bulges count to keep a ssearch result
            @param max_bulge_size: [int] max size of a bulge to keep a ssearch result
        """
        self.add_parameter("genome_id", "Genome ID", default=genome_id, required=True)
        self.add_input_file_list("locus_files", "Locus files list", default=locus_files, required=True)
        self.add_input_file("locus", "Locus file", default=locus, required=True)
        self.add_parameter("clade", "Clade", default=clade, required=True)
        self.add_input_file("genome", "Reference genome file", default=genome, required=True)
        self.add_input_file("hybridation_matrix", "Hybridation matrix file", default=hybridation_matrix, required=True)
        self.add_parameter("stats", "Mirbase stats file", default=stats, required=True)
        self.add_parameter("min_appariements", "min_appariements", default=min_appariements, type="int", required=True)
        self.add_parameter("min_expr_level", "min_expr_level", default=min_expr_level, type="int", required=True)
        self.add_parameter("min_loop_size", "min_loop_size", default=min_loop_size, type="int", required=True)
        self.add_parameter("max_loop_size", "max_loop_size", default=max_loop_size, type="int", required=True)
        self.add_parameter("min_majseq_size", "min_majseq_size", default=min_majseq_size, type="int", required=True)
        self.add_parameter("max_majseq_size", "max_majseq_size", default=max_majseq_size, type="int", required=True)
        self.add_parameter("max_cpt_bulges", "max_cpt_bulges", default=max_cpt_bulges, type="int", required=True)
        self.add_parameter("max_bulge_size", "max_bulge_size", default=max_bulge_size, type="int", required=True)
        self.add_output_file_list("log", "Log file", pattern='{basename_woext}.log', items=locus_files)
        self.add_output_file_list("predictions", "List of gff3 files", pattern='{basename_woext}.mirna.gff3', items=locus_files)
        self.add_output_file_list("fastas", "List of FASTA files", pattern='{basename_woext}.mirna.fasta', items=locus_files)
        self.add_output_file_list("strucs", "TSV structures of miRNA predictions file", pattern='{basename_woext}.mirna.struc', items=locus_files)
    
    def process(self):
        parameters = [self.genome_id, self.clade, self.hybridation_matrix, self.stats, self.genome, self.output_directory, self.get_exec_path("ssearch36"),
                      self.get_exec_path("samtools"), str(self.min_appariements), str(self.min_expr_level), str(self.min_loop_size), str(self.max_loop_size),
                      str(self.min_majseq_size), str(self.max_majseq_size), str(self.max_cpt_bulges), str(self.max_bulge_size)]
        
        self.add_python_execution(predict_miRNA,
                                  inputs = [self.locus_files],
                                  outputs = [self.predictions, self.fastas, self.strucs, self.log],
                                  includes = [self.stats, self.genome, self.hybridation_matrix, self.locus],
                                  cmd_format = "{EXE} {IN} {ARG}" + " ".join(parameters) + " {OUT}",
                                  map = True)
        
        
