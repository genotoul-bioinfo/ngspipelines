#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import re
from subprocess import Popen, PIPE
from ngspipelines.component_analysis import ComponentAnalysis

def bam_to_locus(bam, out_dir, min_nb_isoforms, max_nb_isoforms, min_size_locus, offset, threads, samtools_path, out_gff3, log, stderr):
    """
    @summary: Constructs and outputs threads loci files from a BAM file.
        @param bam: [file] BAM input
        @param out_dir: [str] the output directory
        @param min_nb_isoforms: [str] min nb_isoforms to keep a locus
        @param max_nb_isoforms: [str] max nb_isoforms to keep a locus
        @param min_size_locus: [str] min size of a locus to keep it
        @param offset: [str] min bases between two loci
        @param threads: [str] number of locus files to output
        @param samtools_path: [str] the executable path 
        @param out_gff3: [file] GFF3 of loci positions
        @param log: [file] log
        @param stderr: [file] stderr
    @requires: utils.get_strand_signification()
    @requires: locus.Locus
    """
    
    import workflows.srnaseq.lib.utils as utils
    import workflows.srnaseq.lib.locus as locus
    import os, subprocess

    log = open(log,'w')
    stderr = open(stderr,'w')
    out_gff3 = open(out_gff3,"w")
    bam = open(bam,"r")
    
    dic_strand = utils.get_strand_signification()
    list_locus=[]
    previous_strand={}
    previous_strand['+']=None
    previous_strand['-']=None
    threads = int(threads)
    offset = int(offset)
    min_nb_isoforms = int(min_nb_isoforms)
    max_nb_isoforms = int(max_nb_isoforms)
    min_size_locus = int(min_size_locus)

    cmd = samtools_path + " view " + bam.name
    proc = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)
    
    first = True
    line_number=0
    for line in iter(proc.stdout.readline,''):
        line_number+=1
        array_line = line.split()
        flag = array_line[1]
        if dic_strand[flag]!="unmapped":
            try:
                aln_chr=array_line[2]
                aln_start=int(array_line[3])
                aln_seq=array_line[9]
                aln_stop=aln_start+int(len(aln_seq)-1)
                aln_seqid=array_line[0]+"_"+str(aln_start)+"_"+str(aln_stop)
                if dic_strand[flag] == "+":
                    aln_strand="+"
                else:
                    aln_strand="-"
                
            except IndexError:
                stderr.write("Error : Line "+str(line_number)+" is malformed\n")
                sys.exit()
                
            if first:
                new_locus = locus.Locus()
                new_locus.name = "locus_0"
                new_locus.id = 0
                new_locus.nb_isoforms = 1
                new_locus.chromosome = aln_chr
                new_locus.start = aln_start
                new_locus.stop = aln_stop
                new_locus.strand = aln_strand
                previous_strand[aln_strand]=new_locus.id
                list_locus.append(new_locus)
                first = False

            current_locus = list_locus[len(list_locus)-1]
            if aln_chr == current_locus.chromosome:
                if previous_strand[aln_strand] is not None:
                    if aln_start < list_locus[previous_strand[aln_strand]].stop+offset:
                        list_locus[previous_strand[aln_strand]].nb_isoforms+=1
                        list_locus[previous_strand[aln_strand]].tab_seq.append(aln_seqid)
                        list_locus[previous_strand[aln_strand]].stop = max(list_locus[previous_strand[aln_strand]].stop,(aln_stop))
                        list_locus[current_locus.id]=current_locus
                    else:
                        new_locus = locus.Locus()
                        new_locus.name = "locus_"+str(current_locus.id+1)
                        new_locus.id = current_locus.id+1
                        new_locus.nb_isoforms = 1
                        new_locus.chromosome = aln_chr
                        new_locus.start = aln_start
                        new_locus.stop = aln_stop
                        new_locus.strand = aln_strand
                        previous_strand[aln_strand]=new_locus.id
                        list_locus.append(new_locus)
                        list_locus[previous_strand[aln_strand]].tab_seq.append(aln_seqid)
                else:
                    new_locus = locus.Locus()
                    new_locus.name = "locus_"+str(current_locus.id+1)
                    new_locus.id = current_locus.id+1
                    new_locus.nb_isoforms = 1
                    new_locus.chromosome = aln_chr
                    new_locus.start = aln_start
                    new_locus.stop = aln_stop
                    new_locus.strand = aln_strand
                    previous_strand[aln_strand]=new_locus.id
                    list_locus.append(new_locus)
                    list_locus[new_locus.id].tab_seq.append(aln_seqid)
            else:
                new_locus = locus.Locus()
                new_locus.name = "locus_"+str(current_locus.id+1)
                new_locus.id = current_locus.id+1
                new_locus.nb_isoforms = 1
                new_locus.chromosome = aln_chr
                new_locus.start = aln_start
                new_locus.stop = aln_stop
                new_locus.strand = aln_strand
                previous_strand['+']=None
                previous_strand['-']=None
                previous_strand[aln_strand]=new_locus.id
                list_locus.append(new_locus)
                list_locus[previous_strand[aln_strand]].tab_seq.append(aln_seqid)

    # Filter locus
    not_enough_isoforms=0
    too_many_isoforms=0
    not_enough_long=0
    cpt_kept=0
    list_filtered_locus = []
    dict_length = {}
    dict_nbseq  = {}
    for i in sorted(list_locus, key=lambda x: (x.chromosome, x.start)):
        good_length = False
        good_nb_seq = False
        i.length = i.stop - i.start + 1
        
        # How many locus by length and isoforms number
        if i.length in dict_length:
            dict_length[i.length] += 1
        else:
            dict_length[i.length] = 1
        if i.nb_isoforms in dict_nbseq:
            dict_nbseq[i.nb_isoforms] += 1
        else:
            dict_nbseq[i.nb_isoforms] = 1
        
        # Filter
        if i.length >= min_size_locus:
            good_length = True
        else:
            not_enough_long+=1
        if i.nb_isoforms >= min_nb_isoforms:
            good_nb_seq = True
        else:
            not_enough_isoforms+=1
        if i.nb_isoforms <= max_nb_isoforms:
            good_nb_seq = True
        else:
            good_nb_seq = False
            too_many_isoforms+=1
        if good_length and good_nb_seq :
            i.majseq = i.get_maj_seq()
            i.genome_id = ""
            out_gff3.write(i.write_GFF3()+"\n")
            list_filtered_locus.append(i)
            cpt_kept+=1
    
    # Write logs
    log.write("Locus number:\t"+str(len(list_locus))+"\n")
    log.write("Too short:\t"+str(not_enough_long)+"\n")
    log.write("Too few sequences:\t"+str(not_enough_isoforms)+"\n")
    log.write("Too many sequences:\t"+str(too_many_isoforms)+"\n")
    log.write("Filtered:\t"+str(cpt_kept)+"\n")
    
    log.write("#Length_keys")
    for i in sorted(dict_length.iterkeys(),key=int):
        log.write("\t"+str(i))
    log.write("\n#Length_values")
    for i in sorted(dict_length.iterkeys(),key=int):
        log.write("\t"+str(dict_length[i]))

    log.write("\n#Isoforms_keys")
    for i in sorted(dict_nbseq.iterkeys(),key=int):
        log.write("\t"+str(i))
    log.write("\n#Isoforms_values")
    for i in sorted(dict_nbseq.iterkeys(),key=int):
        log.write("\t"+str(dict_nbseq[i]))
    
    if len(list_filtered_locus) == 0:
        stderr.write("#No locus obtained - End of analysis\n")
        sys.exit()
    else:
        # Split locus into threads files
        nb_lines_per_file = max(cpt_kept/threads,1)
        chunks=[list_filtered_locus[x:x+nb_lines_per_file] for x in xrange(0, len(list_filtered_locus), nb_lines_per_file)]
        if len(chunks)>threads:
            chunks[-2].extend(chunks[-1])
            chunks.pop()
        
        for cpt, i in enumerate(chunks):
            file_name = str(cpt+1)+".locus"
            current_file_path = open(os.path.join(out_dir, file_name),"w")
            for cpt2, j in enumerate(i):
                current_file_path.write(str(i[cpt2])+"\n")
            current_file_path.close()
    
    bam.close()
    out_gff3.close()
    log.close()
    stderr.close()

class BAMToLocus (ComponentAnalysis):
    
    def define_parameters(self, bam, min_nb_isoforms, max_nb_isoforms, min_size_locus, offset, threads=1):
        """
            @param bam: [file] BAM input
            @param min_nb_isoforms: [int] min nb_isoforms to keep a locus
            @param min_size_locus: [int] min size to keep a locus
            @param max_size_locus: [int] max size to keep a locus
            @param offset: [int] min distance between two locus
            @param threads: [int] number of loci generated (linked to threads)
        """
        self.add_input_file("bam", "BAM file", default=bam, required=True)
        self.add_parameter("min_nb_isoforms", "Min nb isoforms", default=min_nb_isoforms, type="int", required=True)
        self.add_parameter("max_nb_isoforms", "Max nb isoforms", default=max_nb_isoforms, type="int", required=True)
        self.add_parameter("min_size_locus", "Min size of a locus", default=min_size_locus, type="int", required=True)
        self.add_parameter("offset", "offset", default=offset, type="int", required=True)
        self.add_parameter("threads", "threads", default=threads, type="int", required=True)
        self.add_output_file("log", "log file", filename=self.__class__.__name__+'.log')
        self.add_output_file("stderr", "stderr file", filename=self.__class__.__name__+'.stderr')
        self.add_output_file("gff3", "GFF3 file", filename="locus.gff3")
        self.add_output_file_endswith("output_files", "Output files", pattern='.locus', behaviour="include")
    
    def define_analysis(self):
        self.analysis_name = "Loci builder"
        self.soft_name = "samtools and in-house scripts"
        self.soft_parameters = "offset: "+str(self.offset)+", min_nb_isoforms: "+str(self.min_nb_isoforms)+", max_nb_isoforms: "+str(self.max_nb_isoforms)+", min_size_locus: "+str(self.min_size_locus)
        self.comments = "Build loci (overlapped strand-specific sequences +/-"+ str(self.offset) +"bp) from mapping and filter"
        self.files = [self.gff3]
        
    def get_version(self):
        cmd = [self.get_exec_path("samtools")]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        for line in iter(p.stderr.readline,''):
            if "Version:" in line:
                return re.split("\s+", line.rstrip())[1]
    
    def post_process(self):
        self._add_result_element("offset",          str(self.offset), "int")
        self._add_result_element("min_nb_isoforms", str(self.min_nb_isoforms), "int")
        self._add_result_element("max_nb_isoforms", str(self.max_nb_isoforms), "int")
        self._add_result_element("min_size_locus",  str(self.min_size_locus), "int")
        
        f = open(self.log, 'r')
        self._add_result_element("locus_number",   f.readline().split('\t')[1], "int")
        self._add_result_element("too_short",      f.readline().split('\t')[1], "int")
        self._add_result_element("too_few_seq",    f.readline().split('\t')[1], "int")
        self._add_result_element("too_many_seq",    f.readline().split('\t')[1], "int")
        self._add_result_element("filtered_locus", f.readline().split('\t')[1], "int")
        self._add_result_element("length_keys",   ",".join(f.readline().split('\t')[1:]), "string")
        self._add_result_element("length_values", ",".join(f.readline().split('\t')[1:]), "string")
        self._add_result_element("isoforms_keys",   ",".join(f.readline().split('\t')[1:]), "string")
        self._add_result_element("isoforms_values", ",".join(f.readline().split('\t')[1:]), "string")
    
    def process(self):
        self.add_python_execution(bam_to_locus, 
                                  inputs=self.bam,
                                  outputs=[self.gff3, self.log, self.stderr],
                                  arguments=[self.output_directory, str(self.min_nb_isoforms), str(self.max_nb_isoforms), str(self.min_size_locus), str(self.offset), str(self.threads), self.get_exec_path("samtools")],
                                  cmd_format = "{EXE} {IN} {ARG} {OUT}"
                                  )