#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from jflow.component import Component

def get_isoforms_infos(bam, matrix, annot_matrix, samtools_path, *gff3_prediction_list):
    from jflow.featureio import GFF3IO
    import subprocess, re
    import workflows.srnaseq.lib.utils as utils
    
    dic_strand = utils.get_strand_signification()
    (dic_matrix, dic_total_lib) = utils.load_count_matrix(matrix)
    dic_annot_matrix = utils.load_annotation_matrix(annot_matrix)
    dic_isoforms_infos = {}
    
    cmd = samtools_path + " view -F 4 " + bam
    proc = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)
    dic_forward = {}
    dic_reverse = {}
    
    for line in iter(proc.stdout.readline,''):
        l = line.split()
        chromosome = l[2]
        isoform_name = l[0]
        isoform_start = int(l[3])
        isoform_end = int(isoform_start) + len(l[9]) - 1
        isoform_seq = l[9]
        if dic_strand[l[1]] == "-":
            if chromosome in dic_reverse:
                dic_reverse[chromosome].append(isoform_name+"|"+str(isoform_start)+"|"+str(isoform_end)+"|"+isoform_seq)
            else:
                dic_reverse[chromosome]=[]
                dic_reverse[chromosome].append(isoform_name+"|"+str(isoform_start)+"|"+str(isoform_end)+"|"+isoform_seq)
        else:
            if chromosome in dic_forward:
                dic_forward[chromosome].append(isoform_name+"|"+str(isoform_start)+"|"+str(isoform_end)+"|"+isoform_seq)
            else:
                dic_forward[chromosome]=[]
                dic_forward[chromosome].append(isoform_name+"|"+str(isoform_start)+"|"+str(isoform_end)+"|"+isoform_seq)
    
    for gff in gff3_prediction_list:
        reader = GFF3IO(gff, "r")
        for entry in reader:
            if entry.type.endswith("primary_transcript"):
                locus_name = entry.attributes["ID"]
                dic_isoforms_infos[locus_name]=[]
                chromosome = entry.seq_id
                locus_start = entry.start
                locus_end = entry.end
                strand = entry.strand
                
                if strand == "+":
                    if chromosome in dic_forward:
                        for i in dic_forward[chromosome]:
                            nb_reads=0
                            nb_samples=0
                            a_libraries = []
                            a_line = line.split()
                            isoform_name = i.split('|')[0]
                            isoform_start = int(i.split('|')[1])
                            isoform_end = int(i.split('|')[2])
                            isoform_length = isoform_end - isoform_start + 1
                            sequence = i.split('|')[3]
                            
                            if ((isoform_start >= locus_start) and (isoform_end <= locus_end)):
                    
                                try:
                                    NM = int(re.search("NM:i:(\d+)",line).group(0).split(":")[2])
                                except AttributeError:
                                    sys.exit("No NM tag found")
                                try:
                                    MD = re.search("MD:Z:(\w+)",line).group(0).split(":")[2]
                                except AttributeError:
                                    sys.exit("No MD tag found")
            
                                for (sample, reads) in dic_matrix[isoform_name].items():
                                    nb_reads += int(reads)
                                    if int(reads)>0:
                                        nb_samples+=1
                                        a_libraries.append(sample)
                                
                                isoform={}
                                isoform["chromosome"]=chromosome
                                isoform["start"]=isoform_start
                                isoform["stop"]=isoform_end
                                isoform["strand"]=strand
                                isoform["NM"]=NM
                                isoform["MD"]=MD
                                isoform["sequence"]=sequence
                                isoform["name"]=isoform_name
                                isoform["nb_reads"]=nb_reads
                                isoform["nb_samples"]=nb_samples
                                isoform["libraries"]=";".join(a_libraries)
                                
                                annotation_array = []
                                for (database, value) in dic_annot_matrix[isoform_name].items():
                                    if value != "":
                                        annotation_array.append(database+":"+value)
                                
                                isoform["annotation"]=",".join(annotation_array)
                                
                                dic_isoforms_infos[locus_name].append(isoform)
                else:
                    if chromosome in dic_reverse:
                        for i in dic_reverse[chromosome]:
                            nb_reads=0
                            nb_samples=0
                            a_libraries = []
                            a_line = line.split()
                            isoform_name = i.split('|')[0]
                            isoform_start = int(i.split('|')[1])
                            isoform_end = int(i.split('|')[2])
                            isoform_length = isoform_end - isoform_start + 1
                            sequence = utils.reverseComplement(i.split('|')[3])
                            
                            if ((isoform_start >= locus_start) and (isoform_end <= locus_end)):
                    
                                try:
                                    NM = int(re.search("NM:i:(\d+)",line).group(0).split(":")[2])
                                except AttributeError:
                                    sys.exit("No NM tag found")
                                try:
                                    MD = re.search("MD:Z:(\w+)",line).group(0).split(":")[2]
                                except AttributeError:
                                    sys.exit("No MD tag found")
            
                                for (sample, reads) in dic_matrix[isoform_name].items():
                                    nb_reads += int(reads)
                                    if int(reads)>0:
                                        nb_samples+=1
                                        a_libraries.append(sample)
                                
                                isoform={}
                                isoform["chromosome"]=chromosome
                                isoform["start"]=isoform_start
                                isoform["stop"]=isoform_end
                                isoform["strand"]=strand
                                isoform["NM"]=NM
                                isoform["MD"]=MD
                                isoform["sequence"]=sequence
                                isoform["name"]=isoform_name
                                isoform["nb_reads"]=nb_reads
                                isoform["nb_samples"]=nb_samples
                                isoform["libraries"]=";".join(a_libraries)
                                
                                annotation_array = []
                                for (database, value) in dic_annot_matrix[isoform_name].items():
                                    if value != "":
                                        annotation_array.append(database+":"+value)
                                
                                isoform["annotation"]=",".join(annotation_array)
                                
                                dic_isoforms_infos[locus_name].append(isoform)

    return dic_isoforms_infos
    
    
    
class GetIsoformsInfos (Component):
    
    def define_parameters(self, bam, predictions, count_matrix, annotation_matrix):
        """
        @summary: Extracts isoform informations for each prediction
            @param bam_file: [file] BAM file
            @param gff3_prediction_files_list: [list] The list of GFF3 prediction files
            @param count_matrix_file: [file] The count matrix file
            @param annotation_matrix_file: [file] The reads annotation file
            @note: self.dic
            {'srnaplan_miRNA_21': ## prediction ID
                [ { 'MD': '0T21',    ## list of isoform informations
                    'NM': 1,
                    'nb_reads': 5,
                    'sequence': 'ATAGCCATGCTGGTTAAGTGTG',
                    'stop': 1873278,
                    'nb_samples': 1,
                    'libraries': 's_7_uT40',
                    'start': '1873257',
                    'chromosome': 'scaffold_14',
                    'strand': '-',
                    'name': 'seq148186#1#5',
                    'annotation': 'mirbase:MI000095:mir-10'
                  },
                  ...
        """
        self.add_input_file("bam", "BAM file", default=bam, required=True)
        self.add_input_file_list("predictions", "The list of GFF3 prediction files", default=predictions, required=True)
        self.add_input_file("count_matrix", "The count matrix file", default=count_matrix, required=True)
        self.add_input_file("annotation_matrix", "The reads annotation matrix file", default=annotation_matrix, required=True)
        self.add_output_object("dic", "Object")
        
    def process(self):
        cmd_format = '{executable} {outputs} ' +  self.bam + ' ' + self.count_matrix + ' ' + self.annotation_matrix + ' ' + self.get_exec_path("samtools") + ' {inputs} '
        self.add_python_execution(get_isoforms_infos,
                                  cmd_format=cmd_format,
                                  inputs=[self.predictions],
                                  outputs=self.dic,
                                  includes = [self.bam, self.count_matrix, self.annotation_matrix])
        