#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, json
from ngspipelines.component_analysis import ComponentAnalysis


def merge(out_gff3, out_annot_gff3, out_annot_struc_gff3, out_fasta, out_struc, gff3_pred, gff3_annot, gff3_struc_annot, fasta, struc):
    """
    @param out_gff3: [file] GFF3 prediction
    @param out_annot_gff3: [file] GFF3 annotation
    @param out_fasta: [file] FASTA
    @param out_struc: [file] struc file
    @param gff3_pred: [list] of GFF3 prediction files
    @param gff3_annot: [list] of GFF3 annotation files
    @param fasta: [list] of FASTA files
    @param struc: [list] of struc files
    """
    
    
    from jflow.featureio import GFF3IO
    from jflow.seqio import FastaReader
    import re, json
    
    def def_new_name(dic, old_id, old_name):
        infos = old_name.split("-")
        species = infos[0]
        type = infos[1]
        try:
            position = "-"+old_name.split("-")[3].split("_")[0]
        except:
            position = ""
        if not old_id in dic:
            if type != "miR":
                return old_name[:-3]
            else:
                return old_name
        elif len(dic[old_id])==1:
            annotation_name = dic[old_id][0]
            if type != "miR":
                new_name = species+"-"+annotation_name 
                return  new_name[:-3]
            else:
                if "5p" in annotation_name or "3p" in annotation_name:
                    return species+"-"+annotation_name
                else:
                    return species+"-"+dic[old_id][0]+position
        else:
            if type != "miR":
                return species+"-"+type+"-ambigous"
            else:
                return species+"-"+type+"-ambigous"+position 
            
    
    gff3_pred = json.load(open(gff3_pred, 'r'))
    gff3_annot = json.load(open(gff3_annot, 'r'))
    gff3_struc_annot = json.load(open(gff3_struc_annot, 'r'))
    fasta = json.load(open(fasta, 'r'))
    struc = json.load(open(struc, 'r'))
    if "None" in struc:
        struc = []
    
    out_gff3 = GFF3IO(out_gff3,'a')
    out_annot_gff3 = GFF3IO(out_annot_gff3,'a')
    out_annot_struc_gff3 = GFF3IO(out_annot_struc_gff3,'a')
    out_fasta = open(out_fasta,'w')
    out_struc = open(out_struc,"w")
    
    tab_pred = []
    tab_annot = []
    tab_struc_annot = []
    tab_fasta = []
    cpt=1
    dic_corr = {}
    dic_struc_corr = {}
    dic_scaff = {}
    
    dic_annotations = {}
    for i in range(0,len(gff3_annot)):  
        reader = GFF3IO(gff3_annot[i], "r")
        for entry in reader:
            name = entry.attributes["Name"]
            id_annot = entry.attributes["id"]
            if name in dic_annotations:
                dic_annotations[name].append(id_annot)
            else:
                dic_annotations[name]=[id_annot]
            tab_annot.append(entry)
    
    dic_coor_old_and_new_ids = {}
    tab_expr_maj = []
    cpt=0
    for i in range(0,len(gff3_pred)):
        reader = GFF3IO(gff3_pred[i], "r")
        for entry in reader:
            if "primary_transcript" in entry.type:
                cpt+=1
                old_id = entry.attributes["ID"]
                new_id = old_id.split("_")[0]+"_"+str(cpt)
                old_name = entry.attributes["Name"]
                new_name = def_new_name(dic_annotations, old_id, old_name)
                dic_coor_old_and_new_ids[old_id]=new_id
                entry.setAttribute("ID", new_id)
                entry.setAttribute("Name", new_name)
            elif "star" in entry.type:
                old_id = entry.attributes["ID"]
                new_id = old_id.split("_")[0]+"_"+str(cpt)+"_star"
                old_name = entry.attributes["Name"]
                new_name = def_new_name(dic_annotations, old_id, old_name)
                dic_coor_old_and_new_ids[old_id]=new_id
                entry.setAttribute("ID", new_id)
                entry.setAttribute("Name", new_name)
                entry.setAttribute("Parent", dic_coor_old_and_new_ids[old_id.replace("_star","")])
            else:
                old_id = entry.attributes["ID"]
                new_id = old_id.split("_")[0]+"_"+str(cpt)+"_mature"
                old_name = entry.attributes["Name"]
                new_name = def_new_name(dic_annotations, old_id, old_name)
                dic_coor_old_and_new_ids[old_id]=new_id
                entry.setAttribute("ID", new_id)
                entry.setAttribute("Name", new_name)
                entry.setAttribute("Parent", dic_coor_old_and_new_ids[old_id.replace("_mature","")])
            tab_pred.append(entry)
            if "ExprMax" in entry.attributes:
                tab_expr_maj.append(entry.attributes["ExprMax"])
        
        try:
            reader = FastaReader(fasta[i],"r")
            for seq in reader:
                tab_fasta.append(seq)
        except:
            pass
        struc_reader = GFF3IO(gff3_struc_annot[i], "r")
        for entry in struc_reader:
            entry.attributes["PParent"] = dic_coor_old_and_new_ids[entry.attributes["PParent"]]
            tab_struc_annot.append(entry)
        
    if (len(struc)>0):
        for i in range(0,len(struc)):
            f = open(struc[i],"r")
            for line in f:
                if line.startswith(">"):
                    line = line.rstrip()
                    name = re.search(">(\S+)",line).group(0).replace(">","")
                    out_struc.write(">"+dic_coor_old_and_new_ids[name]+"\n")
                else:
                    out_struc.write(line)
    
    # Re-computes score of miRNA predictions            
    tab_expr_maj_s = sorted(tab_expr_maj, reverse=True, key=int)
    tab_pred_sorted = sorted(tab_pred, key = lambda x: (x.seq_id, x.start, x.end)) 
    
    dic={}
    for i,j in enumerate(tab_expr_maj_s):
        if not j in dic:
            dic[j]=i
    
    
    if (len(tab_pred)>0):
        for i in tab_pred_sorted:
            if "ExprMax" in i.attributes:
                i.score = str(int(i.score) + int(750.0/float(dic[i.attributes["ExprMax"]]+1)))
            out_gff3.write(i)
    
    if (len(tab_annot)>0):
        for i in sorted(tab_annot, key = lambda x: (x.seq_id, x.start, x.end)):
            i.setAttribute("Parent", dic_coor_old_and_new_ids[i.attributes["Name"]])
            #if '_mature' in i.attributes["Parent"]:
            #    i.setAttribute("Parent", i.attributes["Parent"].replace("_mature",""))
            #if '_star' in i.attributes["Parent"]:
            #    i.setAttribute("Parent", i.attributes["Parent"].replace("_star",""))
            i.setAttribute("Name", i.attributes["Parent"]+"_"+i.source)
            i.setAttribute("ID", i.attributes["Parent"]+"_"+i.source)
            out_annot_gff3.write(i)
    
    if (len(tab_struc_annot)>0):
        for i in sorted(tab_struc_annot, key = lambda x: (x.seq_id, x.start, x.end)):
            i.setAttribute("Name", i.attributes["PParent"])
            i.setAttribute("ID", i.attributes["PParent"])
            out_annot_struc_gff3.write(i)
    
    if (len(tab_fasta)>0):
        for i in sorted(tab_fasta, key = lambda x: (x.name)):
            if i.name in dic_coor_old_and_new_ids:
                out_fasta.write(">"+dic_coor_old_and_new_ids[i.name]+"\n"+i.sequence+"\n")
    
    
    
    
    
    """
    for i in range(0,len(gff3_pred)):  
        reader = GFF3IO(gff3_pred[i], "r")
        for entry in reader:        
            old_name = entry.attributes["Name"]
            
            if entry.type.endswith("primary_transcript"):
                tmp_name = entry.type.split("_")[0]+"_"+str(cpt)
                entry.setAttribute("Name", entry.type.split("_")[0]+"_"+str(cpt))
                entry.setAttribute("ID", entry.type.split("_")[0]+"_"+str(cpt))
                cpt+=1
            else:
                if "_star" in entry.type:
                    entry.setAttribute("Name", tmp_name+"_star")
                    entry.setAttribute("ID", tmp_name+"_star")
                    entry.setAttribute("Parent", tmp_name)
                else:
                    entry.setAttribute("Name", tmp_name+"_mature")
                    entry.setAttribute("ID", tmp_name+"_mature")
                    entry.setAttribute("Parent", tmp_name)
            dic_corr[old_name]=entry.attributes["Name"]
            dic_struc_corr[old_name]=entry.attributes["Name"]
            dic_scaff[old_name]=entry.seq_id
            tab_pred.append(entry)
        try:
            reader = GFF3IO(gff3_annot[i], "r")
            for entry in reader:
                entry.attributes["Parent"] = dic_corr[entry.attributes["Name"]]
                tab_annot.append(entry)
        except:
            pass
        struc_reader = GFF3IO(gff3_struc_annot[i], "r")
        for entry in struc_reader:
            entry.attributes["Parent"] = dic_struc_corr[entry.attributes["Parent"]]
            tab_struc_annot.append(entry)
        try:
            reader = FastaReader(fasta[i],"r")
            for seq in reader:
                tab_fasta.append(seq)
        except:
            pass

    if (len(struc)>0):
        for i in range(0,len(struc)):
            f = open(struc[i],"r")
            for line in f:
                if line.startswith(">"):
                    line = line.rstrip()
                    name = re.search(">(\S+)",line).group(0).replace(">","")
                    out_struc.write(">"+"_".join(dic_corr[name].split("_")[:2])+"\n")
                else:
                    out_struc.write(line)

    # Re-computes score of miRNA predictions
    tab_expr_maj = []
    if (len(tab_pred)>0):
        for i in tab_pred:
            if "ExprMax" in i.attributes:
                tab_expr_maj.append(i.attributes["ExprMax"])
    tab_expr_maj_s = sorted(tab_expr_maj, reverse=True, key=int)
    tab_pred_sorted = sorted(tab_pred, key = lambda x: (x.seq_id, x.start, x.end)) 
    
    dic={}
    for i,j in enumerate(tab_expr_maj_s):
        if not j in dic:
            dic[j]=i
    
    
    if (len(tab_pred)>0):
        for i in tab_pred_sorted:
            if "ExprMax" in i.attributes:
                i.score = str(int(i.score) + int(750.0/float(dic[i.attributes["ExprMax"]]+1)))
            out_gff3.write(i)
    
    if (len(tab_annot)>0):
        for i in sorted(tab_annot, key = lambda x: (x.seq_id, x.start, x.end)):
            if '_mature' in i.attributes["Parent"]:
                i.setAttribute("Parent", i.attributes["Parent"].replace("_mature",""))
            if '_star' in i.attributes["Parent"]:
                i.setAttribute("Parent", i.attributes["Parent"].replace("_star",""))
            i.setAttribute("Name", i.attributes["Parent"]+"_"+i.source)
            i.setAttribute("ID", i.attributes["Parent"]+"_"+i.source)
            out_annot_gff3.write(i)
    
    if (len(tab_struc_annot)>0):
        for i in sorted(tab_struc_annot, key = lambda x: (x.seq_id, x.start, x.end)):
            i.setAttribute("Name", i.attributes["Parent"])
            i.setAttribute("ID", i.attributes["Parent"])
            out_annot_struc_gff3.write(i)
    
    if (len(tab_fasta)>0):
        for i in sorted(tab_fasta, key = lambda x: (x.name)):
            if i.name in dic_corr:
                out_fasta.write(">"+"_".join(dic_corr[i.name].split("_")[:2])+"\n"+i.sequence+"\n")
    """     
    out_struc.close()
    out_fasta.close()
    
    
class Merge(ComponentAnalysis):
    
    def define_parameters(self, predictions, functional_annotations, structural_annotations, fastas, secondary_structures=[]):
        """
            @param predictions: [list] of GFF3 prediction files
            @param functional_annotations: [list] of GFF3 functional annotation files
            @param structural_annotations: [list] of GFF3 structural annotation files
            @param fastas: [list] of FASTA files
            @param strucstructural_annotations: [list] of struc files
        """
        self.add_input_file_list("predictions", "GFF3 prediction files", default=predictions, required=True)
        self.add_input_file_list("functional_annotations", "GFF3 functional annotation files", default=functional_annotations, required=True)
        self.add_input_file_list("fastas", "FASTA files", default=fastas, required=True)
        self.add_input_file_list("structural_annotations", "structural annotation files", default=structural_annotations, required=True)
        self.add_input_file_list("secondary_structures", "secondary structure files", default=secondary_structures, required=True)
        self.add_output_file("prediction", "GFF3 file", filename=self.prefix+"_predictions.gff3")
        self.add_output_file("functional_annotation", "GFF3 file", filename=self.prefix+"_functional_annotations.gff3")
        self.add_output_file("structural_annotation", "GFF3 file", filename=self.prefix+"_structural_annotations.gff3")
        self.add_output_file("fasta", "FASTA file", filename=self.prefix+"_predictions.fasta")
        self.add_output_file("secondary_structure", "STRUC file", filename=self.prefix+"_predictions.struc")
        if len(self.secondary_structures)==0:
            self.secondary_structures = ["None"]
        
    def define_analysis(self):
        family = self.get_nameid().split(".")[1]
        self.files = [self.prediction, self.functional_annotation, self.structural_annotation, self.fasta, self.secondary_structure]
        if family == "locus":
            self.analysis_name = "Functional and structural annotation of unpredicted but expressed regions"
            self.soft_name = "bedtools, samtools, blastall and in-house scripts"
            self.soft_parameters = "-"
            self.comments = "Annotate expressed regions from unpredicted loci"
        else:
            self.analysis_name = "Prediction, functional and structural annotation of " + family
            self.comments = "Predicts ncRNA from expressed loci and annotate them"
            if family == "rRNA":
                self.soft_name = "rnammer, samtools, blastall and in-house scripts"
                self.soft_parameters = "-"
            elif family == "miRNA":
                self.soft_name = "ssearch36, samtools, blastall and in-house scripts"
                self.soft_parameters = ""
            elif family == "tRNA":
                self.soft_name = "tRNAscan-SE, samtools, blastall and in-house scripts"
                self.soft_parameters = "-"
        
    def get_version(self):
        return "-"
    
    def post_process(self):
        pass
    
    def process(self):
        predictions_dump_path = self.get_temporary_file(".dump")
        f_pred = open(predictions_dump_path,'a')
        json.dump(self.predictions, f_pred)
        f_pred.close()
        
        fun_annot_dump_path = self.get_temporary_file(".dump")
        f_fun_annot = open(fun_annot_dump_path,'a')
        json.dump(self.functional_annotations, f_fun_annot)
        f_fun_annot.close()
        
        struc_annot_dump_path = self.get_temporary_file(".dump")
        f_struc_annot = open(struc_annot_dump_path,'a')
        json.dump(self.structural_annotations, f_struc_annot)
        f_struc_annot.close()
        
        fasta_dump_path = self.get_temporary_file(".dump")
        f_fasta = open(fasta_dump_path,'a')
        json.dump(self.fastas, f_fasta)
        f_fasta.close()
        
        secstruc_dump_path = self.get_temporary_file(".dump")
        f_secstruc = open(secstruc_dump_path,'a')
        json.dump(self.secondary_structures, f_secstruc)
        f_secstruc.close()
        
        parameters = [predictions_dump_path, fun_annot_dump_path, struc_annot_dump_path, fasta_dump_path, secstruc_dump_path]
        self.add_python_execution(merge,
                                  cmd_format='{executable} {outputs} ' + " ".join(parameters),
                                  outputs=[self.prediction, self.functional_annotation, self.structural_annotation, self.fasta, self.secondary_structure],
                                  includes=[self.predictions, self.functional_annotations, self.structural_annotations, self.fastas, predictions_dump_path, fun_annot_dump_path, struc_annot_dump_path, fasta_dump_path, secstruc_dump_path]
                                  )
