#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from jflow.component import Component

def retrieves_loci_informations(locus_file, genome_id, unpredicted_gff3, genome, min_size_locus, samtools_path, out_gff3, out_fasta):
    """
    @summary: 
        @param locus_file: [file] the locus file
        @param unpredicted_gff3: [file] GFF3 of unpredicted loci
        @param genome: [file] Reference
        @param samtools_path: [str] the samtools path
    @requires: utils.Locus
    @requires: Locus.reformat_tab_seq()
    @requires: Locus.are_coord_overlapping_locus()
    @requires: Locus.keep_seq_in_zone()
    @requires: Locus.get_maj_seq()
    @requires: Locus.update_coord_majseq()
    """

    import copy, subprocess
    import workflows.srnaseq.lib.locus as locus
    
    id_file = os.path.splitext(os.path.basename(locus_file))[0]
    unpredicted_gff3_reader = open(unpredicted_gff3,"r")
    out_gff3 = open(out_gff3,"w")
    out_fasta = open(out_fasta,"w")
    #locus_file = open(locus_file,"r")
    
    min_size_locus = int(min_size_locus)
    
    bed_infos = {}
    for line in unpredicted_gff3_reader:
        infos = line.split("\t")
        chr = infos[0]
        start = str(int(infos[3])-1)
        stop = str(int(infos[4])-1)
        strand = infos[6]
        if not chr in bed_infos:
            bed_infos[chr]=[]
            bed_infos[chr].append(start+":"+stop+":"+strand)
        else:
            bed_infos[chr].append(start+":"+stop+":"+strand)
    
    list_new_locus = []
    nb_loci = 0
    chromosome = ""
    
    locus_reader = locus.LocusReader(locus_file, "r")
    for old_locus in locus_reader:
        
        if old_locus.chromosome in bed_infos:
            for coord in bed_infos[old_locus.chromosome]:
                new_start = int(coord.split(":")[0])
                new_start = 1 if new_start == 0 else new_start
                new_stop = int(coord.split(":")[1])
                new_strand = coord.split(":")[2]
                if old_locus.are_coord_overlapping_locus(new_start, new_stop) and new_strand == old_locus.strand and ((new_stop-new_start)>min_size_locus):

                    if not (old_locus.chromosome == chromosome):
                        cmd = samtools_path + " faidx " + genome + " " + old_locus.chromosome
                        proc = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                        stdout, stderr = proc.communicate()
                        lines_genome = stdout.split()
                        tab_genome = []
                        for i in lines_genome:
                            if not i.startswith(">"):
                                for j in i.split('\n'):
                                    for k in list(j):
                                        tab_genome.append(k)
                    chromosome = old_locus.chromosome
                    nb_loci += 1
                    new = locus.Locus()
                    new.name = "tRNA_"+id_file+"_"+str(nb_loci)
                    new.genome_id = genome_id
                    new.chromosome = old_locus.chromosome
                    new.start = new_start
                    new.stop = new_stop
                    new.strand = old_locus.strand
                    new.sequence = "".join(tab_genome[new.start-1:new.stop:1])
                    #new.name = "|".join([new.name,new.chromosome,str(new.start),str(new.stop),new.strand])
                    out_gff3.write(new.write_GFF3().rstrip()+"\n")
                    out_fasta.write(">"+new.name+"\n"+new.sequence+"\n")
    
    unpredicted_gff3_reader.close()
    out_gff3.close()
    out_fasta.close()

class ExtractUnpredictedLoci (Component):
    
    def define_parameters(self, genome_id, predictions, locus_file, bam, genome, min_size_locus, locis):        
        """
             @param predictions: [list] predictions in BED format
             @param locus_file: [file] all locus in BED format
             @param bam: [file] BAM file
             @param genome: [file] FASTA reference
             @param locis: [list] loci files
        """
        self.add_parameter("genome_id", "Genome ID", default=genome_id, required=True)
        self.add_input_file_list("predictions", "predictions in BED format", default=predictions, required=True)
        self.add_input_file_list("locis", "loci files", default=locis, required=True)
        self.add_input_file("locus_file", "loci in BED format", default=locus_file, required=True)
        self.add_input_file("bam", "BAM file", default=bam, required=True)
        self.add_input_file("genome", "Reference", default=genome, required=True)
        self.add_parameter("min_size_locus", "min_size_locus", default=min_size_locus, type="int", required=True)
        self.add_output_file("temp1", "Temp file", filename="temp1")
        self.add_output_file("temp2", "Temp file", filename="temp2")
        self.add_output_file("temp3", "Temp file", filename="temp3")
        self.add_output_file("temp4", "Temp file", filename="temp4")
        self.add_output_file("temp5", "Temp file", filename="temp5")
        self.add_output_file("temp6", "Temp file", filename="temp6")
        self.add_output_file("temp7", "Temp file", filename="temp7")
        self.add_output_file("temp8", "Temp file", filename="temp8")
        self.add_output_file("temp9", "Temp file", filename="temp9")
        self.add_output_file("merged_bed", "Merged BED file", filename="merge.bed")
        self.add_output_file("region", "GFF3 file of unpredicted loci", filename="unpredicted_regions.gff3")
        self.add_output_file("stdout", "stdout", filename="merge.stdout")
        self.add_output_file("out_bed", "BED file of unpredicted loci", filename="unpredicted_regions.bed")
        self.add_output_file_list("regions", "List of gff3 files", pattern='{basename_woext}.loci.gff3', items=self.locis)
        self.add_output_file_list("fastas", "List of FASTA files", pattern='{basename_woext}.loci.fasta', items=self.locis)
        self.add_output_file("log", "Log file", filename="details.log")
        
    def process(self):
        
        cmd1 = "cat " + " ".join(self.predictions) + " >> " + self.temp1
        cmd2 = "sort -k1,1 -k4,4n " + self.temp1 + " > " + self.temp2
        cmd3 = self.get_exec_path("mergeBed") + " -c 7 -o distinct -s -i " + self.temp2 + " > " + self.temp3
        cmd4 = self.get_exec_path("perl") + " -lane 'print join \"\\t\",$F[0],$F[1],$F[2],\".\",\".\",$F[3],\".\",\".\",\"0\";' "+  self.temp3 + " > " + self.merged_bed
        cmd5 = self.get_exec_path("subtractBed") + " -s " + "-a " + self.locus_file + " -b " + self.merged_bed + " > " + self.temp4
        cmd6 = "sort -k1,1 -k4,4n " + self.temp4 + " > " + self.temp5
        cmd7 = self.get_exec_path("samtools") + " view -H " + self.bam + " > " + self.temp6
        cmd8 = self.get_exec_path("samtools") + " view " + self.bam + " | sort -k3,3 -k4,4n > " + self.temp7
        cmd9 = "cat " + self.temp6 + " " + self.temp7 + " >> " + self.temp8
        cmd10 = self.get_exec_path("samtools") + " view -bS " + self.temp8 + " > " + self.temp9
        cmd11 = self.get_exec_path("intersectBed") + " -sorted -bed -wb -abam " + self.temp9 + " -b " + self.temp5 + " -s -f 1e-9 | cut -f13-20 | sort -nk2,2 | uniq > " + "$1"
        cmd = cmd1+";"+cmd2+";"+cmd3+";"+cmd4+";"+cmd5+";"+cmd6+";"+cmd7+";"+cmd8+";"+cmd9+";"+cmd10+";"+cmd11
        self.add_shell_execution(cmd,
                                 cmd_format='{EXE} {OUT}',
                                 outputs=self.region,
                                 includes=[self.predictions, self.locus_file, self.bam])
        parameters = [self.genome_id, self.region, self.genome, str(self.min_size_locus), self.get_exec_path("samtools")]
        self.add_python_execution(retrieves_loci_informations,
                                  cmd_format = '{executable} ' + ' {inputs} ' + " ".join(parameters) + ' {outputs}',
                                  inputs=self.locis, 
                                  outputs=[self.regions, self.fastas], 
                                  includes=self.region,
                                  map=True
                                  )