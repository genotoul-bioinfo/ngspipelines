#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from jflow.component import Component

def extract_region(reference_genome, gff3, output):
    """
        @param reference_genome: [file] The reference genome in FASTA format
        @param gff3: [file] The GFF3 predictions file
        @param output: [file] The FASTA sequences file 
    """
    from jflow.featureio import GFF3IO
    from jflow.seqio import FastaReader
    import workflows.srnaseq.lib.utils as utils
    
    output = open(output,"w")
    gff3_reader = GFF3IO(gff3, "r")
    fasta_reader = FastaReader(reference_genome)
    
    res = {}
    for seq in fasta_reader :
        res[seq.name] = seq.sequence
    
    for entry in gff3_reader:
        if entry.type.endswith("primary_transcript"):
            if entry.strand == "-":
                sequence = "".join(list(res[entry.seq_id])[entry.start-1:entry.end])
                output.write(">"+entry.attributes["ID"]+"\n"+utils.reverseComplement(sequence)+"\n")
            else:   
                output.write(">"+entry.attributes["ID"]+"\n"+"".join(list(res[entry.seq_id])[entry.start-1:entry.end])+"\n")
            
    output.close()

def merge_fasta_predictions_files(merged_fasta_predictions_file, *fasta_list):
    """
        @param merged_fasta_predictions_file: [file] FASTA of merged predictions sequences
        @param fasta_list: [list] List of FASTA files to merge
    """
    merged_fasta_predictions_file = open(merged_fasta_predictions_file,'w')
    for file in fasta_list:
        file = open(file,'r')
        for line in file:
            merged_fasta_predictions_file.write(line)
        file.close()
    merged_fasta_predictions_file.close()
    
    
class GetLocusSequence (Component):
    
    def define_parameters(self, reference_genome, predictions):
        """
        @summary: Get DNA sequences of predictions and outputs FASTA files (list and merged file)
            @param reference_genome_file: [file] The reference genome
            @param gff3_prediction_files_list: [list] The list of GFF3 prediction files
        """
        self.add_input_file("reference_genome", "FASTA file", default=reference_genome, required=True)
        self.add_input_file_list("predictions", "GFF3 file list", default=predictions, required=True)
        self.add_output_file_list("fastas", "Ouptut FASTA file list", pattern='{basename_woext}.fa', items=self.predictions)
        self.add_output_file("fasta", "Merged output file", filename='predictions.fa')
       
    def process(self):
        self.add_python_execution(extract_region,
                                  cmd_format="{EXE} " + self.reference_genome + " {IN} {OUT}",
                                  inputs=[self.predictions], 
                                  outputs=[self.fastas],
                                  map=True)
        
        
        self.add_python_execution(merge_fasta_predictions_files,
                                  cmd_format = '{executable} {outputs} {inputs} ',
                                  inputs=[self.fastas],
                                  outputs=[self.fasta]
                                  )
