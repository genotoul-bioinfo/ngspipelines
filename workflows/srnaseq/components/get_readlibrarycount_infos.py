#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from jflow.component import Component

def get_rlc_infos(bam, matrix, samtools_path, *gff3_prediction_list):
    from jflow.featureio import GFF3IO
    import subprocess, pickle
    from operator import itemgetter
    import workflows.srnaseq.lib.utils as utils
        
    def compute_coverage(tab_cov, nb_read, start, stop):
        new_start = 0 if start < 0 else start
        new_stop = len(tab_cov) if stop > len(tab_cov) else stop
        for j in range(new_start,new_stop):
            tab_cov[j]+=int(nb_read)
        return tab_cov
    
    dic_strand = utils.get_strand_signification()
    
    cmd = samtools_path + " view -F 4 " + bam
    proc = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)
    dic_forward = {}
    dic_reverse = {}
    
    for line in iter(proc.stdout.readline,''):
        l = line.split()
        chromosome = l[2]
        isoform_name = l[0]
        isoform_start = int(l[3])
        isoform_end = int(isoform_start) + len(l[9]) - 1
        if dic_strand[l[1]] == "-":
            if chromosome in dic_reverse:
                dic_reverse[chromosome].append(isoform_name+"|"+str(isoform_start)+"|"+str(isoform_end))
            else:
                dic_reverse[chromosome]=[]
                dic_reverse[chromosome].append(isoform_name+"|"+str(isoform_start)+"|"+str(isoform_end))
        else:
            if chromosome in dic_forward:
                dic_forward[chromosome].append(isoform_name+"|"+str(isoform_start)+"|"+str(isoform_end))
            else:
                dic_forward[chromosome]=[]
                dic_forward[chromosome].append(isoform_name+"|"+str(isoform_start)+"|"+str(isoform_end))
    
    (dic_matrix, dic_total_lib) = utils.load_count_matrix(matrix)
    most_expressed = {}
    dic_rlc_infos = {}
    dic_pred_count = {}
    dic_strand_pred = {}
    for gff in gff3_prediction_list:
        reader = GFF3IO(gff, "r")
        for entry in reader:
            if entry.type.endswith("primary_transcript"):
                dic_strand_pred[entry.attributes["ID"]]=entry.strand
                locus_name = entry.attributes["ID"]
                family = entry.type.replace("_primary_transcript","") if not entry.type == "primary_transcript" else "locus"
                most_expressed[locus_name]=0
                chromosome = entry.seq_id
                score = entry.score
                locus_start = int(entry.start)
                locus_end = int(entry.end)
                strand = entry.strand
                length_locus = int(locus_end) - int(locus_start) + 1
                
                dic_cov = {}
                dic_rlc_infos[locus_name]={}
                if strand == "+":
                    if chromosome in dic_forward:
                        for i in dic_forward[chromosome]:
                            isoform_name = i.split('|')[0]
                            isoform_start = int(i.split('|')[1])
                            isoform_end = int(i.split('|')[2])
                            isoform_length = isoform_end - isoform_start + 1
                            dic_samples = dic_matrix[isoform_name]
                            
                            if ((isoform_start >= locus_start) and (isoform_end <= locus_end)):
                            
                                for sample in dic_samples:
                                    if int(dic_samples[sample])>0:
                                        most_expressed[locus_name]+=int(dic_samples[sample])
                                        if not sample in dic_rlc_infos[locus_name]:
                                            dic_rlc_infos[locus_name][sample]={}
                                            dic_rlc_infos[locus_name][sample]["COVERAGE"]=[0]*length_locus
                                            dic_rlc_infos[locus_name][sample]["COVERAGE"] = compute_coverage(dic_rlc_infos[locus_name][sample]["COVERAGE"], dic_samples[sample], isoform_start-locus_start, isoform_end-locus_start)
                                            dic_rlc_infos[locus_name][sample]["NB_READ"] = int(dic_samples[sample])
                                            dic_rlc_infos[locus_name][sample]["MEAN_DEPTH"] = float(int(dic_rlc_infos[locus_name][sample]["NB_READ"])*isoform_length)/float(length_locus)
                                        else:
                                            dic_rlc_infos[locus_name][sample]["NB_READ"] += int(dic_samples[sample])
                                            dic_rlc_infos[locus_name][sample]["COVERAGE"] = compute_coverage(dic_rlc_infos[locus_name][sample]["COVERAGE"], dic_samples[sample], isoform_start-locus_start, isoform_end-locus_start)
                                            dic_rlc_infos[locus_name][sample]["MEAN_DEPTH"] = float(int(dic_rlc_infos[locus_name][sample]["NB_READ"])*isoform_length)/float(length_locus)
                else:
                    if chromosome in dic_reverse:
                        for i in dic_reverse[chromosome]:
                            isoform_name = i.split('|')[0]
                            isoform_start = int(i.split('|')[1])
                            isoform_end = int(i.split('|')[2])
                            isoform_length = isoform_end - isoform_start + 1
                            dic_samples = dic_matrix[isoform_name]
                            
                            if ((isoform_start >= locus_start) and (isoform_end <= locus_end)):
                            
                                for sample in dic_samples:
                                    if int(dic_samples[sample])>0:
                                        most_expressed[locus_name]+=int(dic_samples[sample])
                                        if not sample in dic_rlc_infos[locus_name]:
                                            dic_rlc_infos[locus_name][sample]={}
                                            dic_rlc_infos[locus_name][sample]["COVERAGE"]=[0]*length_locus
                                            dic_rlc_infos[locus_name][sample]["COVERAGE"] = compute_coverage(dic_rlc_infos[locus_name][sample]["COVERAGE"], dic_samples[sample], isoform_start-locus_start, isoform_end-locus_start)
                                            dic_rlc_infos[locus_name][sample]["NB_READ"] = int(dic_samples[sample])
                                            dic_rlc_infos[locus_name][sample]["MEAN_DEPTH"] = float(int(dic_rlc_infos[locus_name][sample]["NB_READ"])*isoform_length)/float(length_locus)
                                        else:
                                            dic_rlc_infos[locus_name][sample]["NB_READ"] += int(dic_samples[sample])
                                            dic_rlc_infos[locus_name][sample]["COVERAGE"] = compute_coverage(dic_rlc_infos[locus_name][sample]["COVERAGE"], dic_samples[sample], isoform_start-locus_start, isoform_end-locus_start)
                                            dic_rlc_infos[locus_name][sample]["MEAN_DEPTH"] = float(int(dic_rlc_infos[locus_name][sample]["NB_READ"])*isoform_length)/float(length_locus)
    # Reverse coverage informations if reverse strand
    for i in dic_rlc_infos:
        if dic_strand_pred[i] == "-":
            for j in dic_rlc_infos[i]:
                dic_rlc_infos[i][j]["COVERAGE"] = dic_rlc_infos[i][j]["COVERAGE"][::-1]
                     
    return dic_rlc_infos

    
class GetReadLibraryCountInfos (Component):
    
    def define_parameters(self, bam_file, gff3_prediction_files_list, count_matrix_file):
        """
        @summary: Outputs pickle files containing informations about libraries
            @param bam_file: [file] BAM file
            @param gff3_prediction_files_list: [list] The list of GFF3 prediction files
            @param count_matrix_file: [file] The count matrix file
            @note: rlc_file :
            'srnaplan_miRNA_21': ## prediction ID 
                {'s_1_uT19':     ## library ID
                    { 'NB_READ': 559,
                      'MEAN_DEPTH': 126.44047619047619,
                      'COVERAGE': [0,0...]
                    }
                }
            @note: plc_file :
            's_1_uT19': ## library ID 
                { 'tRNA': 
                    { 'pred': 4,
                      'reads': 2035
                    },
                    'total_reads': 37090196,
                  'miRNA': 
                    { 'pred': 25, 
                      'reads': 15370}
                    }
        """
        self.add_input_file("bam_file", "", default=bam_file, required=True)
        self.add_input_file_list("gff3_prediction_files_list", "The list of GFF3 prediction files", default=gff3_prediction_files_list, required=True)
        self.add_input_file("count_matrix_file", "The count matrix file", default=count_matrix_file, required=True)
        self.add_output_object("dic", "Output object")
        
    def process(self):
        self.add_python_execution(get_rlc_infos,
                                  cmd_format = '{executable} {outputs} ' +  self.bam_file + ' ' + self.count_matrix_file + ' ' + self.get_exec_path("samtools") + ' {inputs} ',
                                  inputs=[self.gff3_prediction_files_list],
                                  outputs=[self.dic],
                                  includes = [self.bam_file, self.count_matrix_file])
        