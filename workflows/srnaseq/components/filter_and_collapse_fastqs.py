#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import pickle
from ngspipelines.component_analysis import ComponentAnalysis


def remove_inter_redundancy(matrix_file, cleaned_file, removed_file, log_file, minimum_copy_count, minimum_samples, *fasta_files):
    """
    @summary: Removes inter-redundancy from a list of FASTA files. Reads are filtered on their abundance (minimum_copy_count and at least 5 occurences
              per sample in mean). Produces a FASTA file with cleaned reads, a FASTA file of discarded reads and a CSV file, the count matrix. 
        @param matrix_file: [file] the count matrix
        @param cleaned_file: [file] FASTA containing the cleaned reads
        @param removed_file: [file] FASTA containing the discarded reads
        @param log_file: [file] containing the log_file of the process
        @param minimum_copy_count: [str] the minimum copy count for a read to be kept
        @param minimum_samples: [str] the minimum count of samples in which the sequence is expressed
        @param *fasta_files: [list of files] FASTA with reads to collapse
    """
    import pickle
    from jflow.seqio import FastaReader
    
    cleaned_file = open(cleaned_file,"w")
    removed_file = open(removed_file,"w")
    matrix_file = open(matrix_file,"w")
    log_file = open(log_file,"w")
     
    matrix_file.write("#SEQID\tSEQ")
     
    dic_seq={}              # ATTTCCCTTT -> [nb_samples,nb_occurences]
    dic_matrix={}           # ATTTCCCTTT -> {file_id} -> nb_occurences

    min_length = 10000000
    max_length = 0
    minimum_copy_count = int(minimum_copy_count)
    minimum_samples = int(minimum_samples)
     
    total_reads = 0
    total_sequences = 0
    library_names = []

    for fasta in sorted(fasta_files):
        file = FastaReader(fasta)
        library_name = os.path.basename(fasta).replace('.clean.fa','')
        if not library_name in library_names : library_names.append(library_name)
        matrix_file.write("\t"+library_name)
        for seq in file:
            nb_occ = int(seq.name.split('#')[1])
            total_reads += nb_occ
            total_sequences += 1
            min_length = min(len(seq.sequence),min_length)
            max_length = max(len(seq.sequence),max_length)
            if seq.sequence in dic_seq:
                dic_seq[seq.sequence][0]+=1
                dic_seq[seq.sequence][1]=int(dic_seq[seq.sequence][1])+nb_occ
            else:
                dic_seq[seq.sequence]=[1,nb_occ]
 
            if seq.sequence in dic_matrix:
                dic_matrix[seq.sequence][library_name]=nb_occ
            else:
                dic_matrix[seq.sequence]={}
                dic_matrix[seq.sequence][library_name]=nb_occ
     
    length_dic_reads = {}
    length_dic_sequences = {}
    cpt_rm_reads = 0
    cpt_rm_sequences = 0
    cpt_kept_reads = 0
    cpt_kept_sequences = 0
    
    # Prepare length_dic with predefined lengths
    for i in (range(min_length,max_length+1)):
        length_dic_reads[str(i)]=0
        length_dic_sequences[str(i)]=0
     
    for seq in dic_seq:
        nb_samples = dic_seq[seq][0]
        nb_occurences = dic_seq[seq][1]
        
        if ( (nb_samples>=minimum_samples) and ( nb_occurences>=minimum_copy_count) ):
            cpt_kept_sequences+=1
            cpt_kept_reads += nb_occurences
            length_dic_sequences[str(len(seq))]+=1
            length_dic_reads[str(len(seq))]+=nb_occurences
            seq_id = "seq"+str(cpt_kept_sequences)+"#"+str(nb_samples)+"#"+str(nb_occurences)
            # Write FASTA of kept sequences
            cleaned_file.write(">"+seq_id+"\n"+seq+"\n")
            # Write matrix
            matrix_file.write("\n"+seq_id+"\t"+seq)
            for lib in library_names:
                if not lib in dic_matrix[seq]:
                    matrix_file.write("\t0")
                else:
                    matrix_file.write("\t"+str(dic_matrix[seq][lib])) 

        else:
            cpt_rm_reads+=nb_occurences
            cpt_rm_sequences+=1
            # Write FASTA of removed sequences
            removed_file.write(">seq"+str(cpt_rm_sequences)+"#"+str(nb_samples)+"#"+str(nb_occurences)+"\n"+seq+"\n")
    
    # Write log_file
    log_file.write("#Initial reads\t"+ str(total_reads) +"\n")
    log_file.write("#Initial sequences\t"+ str(total_sequences) +"\n")
    log_file.write("#Collapsed sequences\t"+ str(len(dic_seq)) +"\n")
    log_file.write("#Lost reads\t "+str(cpt_rm_reads)+"\n")
    log_file.write("#Lost sequences\t "+str(cpt_rm_sequences)+"\n")
    log_file.write("#Filtered reads\t "+str(cpt_kept_reads)+"\n")
    log_file.write("#Filtered sequences\t "+str(cpt_kept_sequences)+"\n")
    log_file.write("#Length")
    for i in range(min_length,max_length+1):
        log_file.write("\t"+str(i))
    log_file.write("\n")
    log_file.write("#Reads")
    for l in sorted(length_dic_reads.iterkeys(), key=int):
        log_file.write("\t"+str(length_dic_reads[l]))
    log_file.write("\n")
    log_file.write("#Sequences")
    for l in sorted(length_dic_sequences.iterkeys(), key=int):
        log_file.write("\t"+str(length_dic_sequences[l]))
    log_file.write("\n")
 
    removed_file.close()
    matrix_file.close()
    cleaned_file.close()
    log_file.close()


class FilterAndCollapseFastqs (ComponentAnalysis):
    
    def define_parameters(self, fasta_files, min_copy_count, minimum_samples):
        """
            @param fasta_files: [list] of input FASTA files
            @param min_copy_count: [int] minimum count of occurences for a sequence to be kept
            @param minimum_samples: [int] a sequence has to be viewed in at least minimum_samples samples to be kept
        """
        self.add_input_file_list("fasta_files", "The list of FASTA files to collapse", default=fasta_files, required=True, file_format="fasta")
        self.add_parameter("min_copy_count", "Minimum count of occurences for a read to be kept", default=min_copy_count, type="int", required=True)
        self.add_parameter("minimum_samples", "Min nb samples", default=minimum_samples, type="int", required=True)
        self.add_output_file("matrix_count_file", "The count matrix file", filename='count_matrix.tsv')
        self.add_output_file("log", "The log file", filename=self.__class__.__name__+'.log')
        self.add_output_file("cleaned_fasta", "The FASTA file of cleaned, filtered and collapsed sequences", filename='cleaned_sequences.fa', file_format="fasta")
        self.add_output_file("removed_fasta", "The FASTA file of removed sequences", filename='not_enough_abundant_sequences.fa', file_format="fasta")
        
    def define_analysis(self):
        self.analysis_name = "Merging all sequences"
        self.soft_name = "in-house scripts"
        self.soft_parameters = "min_copy_count: "+str(self.min_copy_count) + ", min_samples: " + str(self.minimum_samples)
        self.comments = "Merge all samples and filter by expression level"
        self.files = [self.matrix_count_file, self.cleaned_fasta, self.removed_fasta]
        
    def get_version(self):
        return "-"
    
    def post_process(self):
        f = open(self.log, 'r')
        self._add_result_element("nb_reads_initial", f.readline().split('\t')[1], "int")
        self._add_result_element("nb_seq_initial",   f.readline().split('\t')[1], "int")
        self._add_result_element("nb_collapsed_seq", f.readline().split('\t')[1], "int")
        self._add_result_element("nb_lost_reads",    f.readline().split('\t')[1], "int")
        self._add_result_element("nb_lost_seqs",     f.readline().split('\t')[1], "int")
        self._add_result_element("nb_filtered_reads",f.readline().split('\t')[1], "int")
        self._add_result_element("nb_filtered_seqs", f.readline().split('\t')[1], "int")
        self._add_result_element("length",           ",".join(f.readline().split('\t')[1:]), "string")
        self._add_result_element("reads",            ",".join(f.readline().split('\t')[1:]), "string")
        self._add_result_element("seq",              ",".join(f.readline().split('\t')[1:]), "string")
        f.close()
    
    def process(self):
        cmd_format = '{executable} {outputs} ' + str(self.min_copy_count) + ' ' + str(self.minimum_samples) + ' ' + '{inputs} '
        remove = self.add_python_execution(remove_inter_redundancy, 
                                           inputs=[self.fasta_files], 
                                           outputs=[self.matrix_count_file, self.cleaned_fasta, self.removed_fasta, self.log], 
                                           cmd_format=cmd_format)
        