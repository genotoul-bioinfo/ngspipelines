#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from ngspipelines.component_analysis import ComponentAnalysis
import operator

def write_matrix(out_annotation, samtools_path, *bam):
    """
    @summary:  Parse a list of BAM files and writes merged informations into an annotation matrix.
        @param out_annotation: [file] the output annotation matrix
        @param samtools_path: [str] executable software
        @param *bam: [file list] BAM containing annotations to merge
    @requires: utils.get_strand_signification()
    @requires: utils.reverseComplement()
    """
    import os, subprocess
    import workflows.srnaseq.lib.utils as utils
    
    dic_strand = utils.get_strand_signification()
    out_annotation = open(out_annotation, 'w')
    
    dic_seq = {}
    dic_count = {}
    list_ids = []
      
    for file in bam:
        file = open(file,"r")
        id = os.path.basename(file.name).split(".filter.bam")[0]
        list_ids.append(id)
        
        cmd = samtools_path + " view " + file.name
        proc = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)    
        for line in iter(proc.stdout.readline,''):
            line_array = line.split()
            try:
                seq_id=line_array[0]
                flag=line_array[1]
                annot=line_array[2] if line_array[2] != "*" else "-"
                annot = annot.replace("'","")
                seq=line_array[9]
            except IndexError:
                sys.exit("... Line "+str(line_number)+" is malformed\n")           
                
            if dic_strand[flag] == "-":
                seq=utils.reverseComplement(seq)
            
            if not seq_id in dic_seq:
                dic_seq[seq_id]={}
                dic_seq[seq_id]["SEQUENCE"]=seq
                if not id in dic_seq[seq_id]:
                    dic_seq[seq_id][id]=annot
            else:                    
                if not id in dic_seq[seq_id]:
                    dic_seq[seq_id][id]=annot
        file.close()
    
    for i in list_ids:
        dic_count[i]=0
    out_annotation.write("#SEQID\tSEQUENCE\t"+"\t".join(f for f in list_ids)+"\n")

    for seq_id in dic_seq:
        out_annotation.write(seq_id+"\t"+dic_seq[seq_id]["SEQUENCE"]+"\t")
        list_infos=[]
        for id in list_ids:
            if id in dic_seq[seq_id]:
                if dic_seq[seq_id][id] != "-":
                    dic_count[id]+=1
                list_infos.append(dic_seq[seq_id][id])
            else:
                list_infos.append("-")
        out_annotation.write("\t".join(list_infos)+"\n")
    
    out_annotation.close()


class SequencesAnnotation (ComponentAnalysis):
        
    def define_parameters(self, bam_files, versions=[]):
        """
            @param bam_files: [list of files] BAM files containing annotations to merge
            @param versions: [list of strings] Databank versions
        """
        self.add_input_file_list("bam_files", "List of BAM files containing annotations to merge", default=bam_files, required=True)
        self.add_parameter_list("versions", "Databank versions", default=versions)
        self.add_output_file("annotations_matrix", "TSV file containing annotations matrix", filename="annotations_matrix.tsv")
        
    def define_analysis(self):
        self.analysis_name = "Merging all sequences annotations"
        self.soft_name = "in-house scripts"
        self.soft_parameters = "-"
        self.comments = "Merge and build sequences annotation matrix"
        self.files = [self.annotations_matrix]
        
    def get_version(self):
        return "1.0"
    
    def post_process(self):
        f = open(self.annotations_matrix, 'r')
        header_list = []
        matrix      = []
        venndict    = {}
        itobase     = {}
        for line in f:
            line = line.rstrip()
            fields = line.split("\t")
            if line.startswith("#"):
                header_list = [i.split(".")[0] for i in fields]
                for cpt,i in enumerate(header_list[2:]):
                    venndict[i]  = []
                    itobase[cpt+2] = i
            else:
                id         = fields[0].split("#")[0]
                nb_samples = int(fields[0].split("#")[1])
                nb_reads   = int(fields[0].split("#")[2])
                tmplist    = [id,nb_samples,nb_reads]
                tmplist.extend(fields[1:])
                matrix.append(tmplist)
                for cpt,i in enumerate(fields):
                    if i != "-":
                        if cpt in itobase:
                            venndict[itobase[cpt]].append(id)
        
        tmplist = ["ID", "NbSamples", "NbReads", "Sequences"]
        tmplist.extend([i for i in header_list[2:]])
        self._add_result_element("annot_meheader", ",".join([i for i in tmplist]), "string", "default")
        mematrix = ""
        for items in sorted(matrix, key=operator.itemgetter(2), reverse=True)[0:500]:
            mematrix += ",".join([str(i) for i in items]) + "#"
        self._add_result_element("annot_mematrix", mematrix[:-1], "string", "default")
        
        for i in venndict:
            self._add_result_element(i, ",".join([i for i in venndict[i]]), "string", "venn")
        
        if len(self.versions)>0: 
            for cpt,i in enumerate(header_list[2:]):
                self._add_result_element(i, self.versions[cpt], "string", "version")
        
    def process(self):
        get_matrix = self.add_python_execution(write_matrix, 
                                               inputs=[self.bam_files], 
                                               outputs=self.annotations_matrix,
                                               cmd_format = '{executable} {outputs} ' + self.get_exec_path("samtools") + ' {inputs}')

    