#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

class PreMiRNA(object):
    def __init__(self, name, genome_id, chromosome, strand, length_mature, start_mature, stop_mature, start_star, stop_star, loop, appariements, mirna_sequence, star_sequence):
        self.name = name
        self.genome_id = genome_id
        self.strand = strand
        self.length_mature = length_mature
        self.chromosome = chromosome
        self.start_mature = start_mature
        self.stop_mature = stop_mature
        self.start_star = start_star
        self.stop_star = stop_star
        self.start_premir = -1
        self.stop_premir = -1
        self.size_premir = -1
        self.size_loop = int(loop)
        self.appariements = int(appariements)
        self.sequence = ""
        self.mirna_sequence = mirna_sequence
        self.prediction_sequence = mirna_sequence
        self.star_sequence = star_sequence
        self.tab_seq = []
        self.expression = []
        self.is_mirna_annotated = False
        self.is_other_annotated = False
        self.mirna_annotations = []
        self.other_annotations = []
        self.libraries_count = {}
        self.neighboors = []
        self.majseq = []
        self.has_star = False
        self.mirna = None
        self.mirna_start = None
        self.mirna_stop = None
        self.mirna_star = None
        self.score = -1
        self.expression = {}
        self.structure = ""
        self.ssearch = None
        self.blast_annotations = []
        #self.family = "miRNA"
        self.start = self.start_premir
        self.stop = self.stop_premir
        self.struc_annotations = []
        self.max_bulge = 0
        self.cpt_bulges = 0
        self.expr_maj = 0
        self.start_majseq = 0
        self.stop_majseq = 0
        self.is_dicer_compliant = False
        self.is_drosha_compliant = False
        self.clade = ""
        self.locus_nb_isoforms = 0
        self.size_mature_sequence = 0
        self.index_analyzed_seq = 0
    
    def get_position_mature(self):
        if self.strand == "+":
            if self.start_mature - self.start < 15:
                return "5p"
            else:
                return "3p"
        else:
            if self.start_mature - self.start < 15:
                return "3p"
            else:
                return "5p"
    
    def get_position_star(self):
        if self.strand == "+":
            if self.start_mature - self.start < 15:
                return "3p"
            else:
                return "5p"
        else:
            if self.start_mature - self.start < 15:
                return "5p"
            else:
                return "3p"
    
    def get_final_name_primary(self):
        position = ""
        if self.strand == "+":
            if self.start_mature - self.start < 15:
                position = self.get_position_mature()
            else:
                position = self.get_position_mature()
        else:
            if self.start_mature - self.start < 15:
                position = self.get_position_mature()
            else:
                position = self.get_position_mature()
        return self.genome_id+"-miR-XXXX-"+position
    
    def set_locus_nb_isoforms(self, l):
        self.locus_nb_isoforms = l
    
    def set_size_mature_sequence(self, s):
        self.size_mature_sequence = s
    
    def find_coordinates_premir(self, start_majseq, stop_majseq):
        """
        @summary: Updates coordinates of PreMiRNA object from the list of seq (self.tab_seq)
        @see: mirna_prediction.py
            @param start_majseq: [int]
            @param stop_majseq: [int]
        """
        
        min_start = 10000000000000000000000000000000000000000000000000000 
        max_stop = -1
        min_star_mature = min(start_majseq,min(self.start_mature, self.start_star))
        max_star_mature = max(stop_majseq,max(self.stop_mature, self.stop_star))
        
        for i in self.tab_seq:
            min_start = min(min_start,int(i.split('_')[1]))
            max_stop = max(max_stop,int(i.split('_')[2]))
        
        self.start_premir = max(1,min(min_start,min_star_mature))
        self.stop_premir = max(max_stop,max_star_mature)
        self.size_premir = int(self.stop_premir)-int(self.start_premir)+1
    
    def seems_to_be_a_mirna(self, min_loop_size, max_loop_size, min_majseq_size, max_majseq_size, max_cpt_bulges, max_max_bulge, min_appariements):
        """
        @summary: Checks if elements are similar to a miRNA
        @see: mirna_prediction.py
            @param min_loop_size: [int]
            @param max_loop_size: [int]
            @param min_majseq_size: [int]
            @param max_majseq_size: [int]
            @param max_cpt_bulges: [int] max_cpt_bulges
            @param max_max_bulge: [int] max_max_bulge
            @param min_appariements: [int] min_appariements
        @return: [boolean]
        """
        
        really = True
        if (self.appariements >= min_appariements):
            really=True
        else:
            return (False, "Bad appariements count : "+str(self.appariements) + " < " + str(min_appariements))
        if (self.length_mature >= min_majseq_size) and (self.length_mature<=max_majseq_size):
            really=True
        else:
            return (False, "Bad length of maj sequence : " + str(self.length_mature) + " vs " + str(min_majseq_size)+ "-"+ str(max_majseq_size))
        if (self.size_loop >= min_loop_size) and (self.size_loop <= max_loop_size):
            really=True
        else:
            return (False, "Bad loop size : " + str(self.size_loop) + " vs " + str(min_loop_size)+ "-"+ str(max_loop_size))
        if self.ssearch.cpt_bulges <= max_cpt_bulges:
            really=True
        else:
            return (False, "Bad cpt bulges : " + str(self.ssearch.cpt_bulges) + " > " + str(max_cpt_bulges))
        if self.ssearch.max_bulge <= max_max_bulge:
            really=True
        else:
            return (False, "Bad max bulge size : " + str(self.ssearch.max_bulge) + " > " + str(max_max_bulge))

        return (really,"")
    
    def get_seq_in_zone_premir(self, tab, start, stop):
        """
        @summary: Get seq in tab from start to stop
        @see: mirna_prediction.py
            @param tab: [list] of seq
            @param start: [int]
            @param stop: [int]
            @param overlap: [int]
        @return: [list] list of seq
        """
        
        temp_tab_seq=[]
        start = int(start)
        stop = int(stop)
        for seq in tab:
            seq_start = int(seq.split('_')[1])
            seq_stop = int(seq.split('_')[2])
            if ((seq_start >= start and seq_start <= stop) or (seq_stop <= stop and seq_stop >= start)):
                temp_tab_seq.append(seq)
        tab=list(temp_tab_seq)
        return tab
    
    def premir_coverage(self, tab_seq):
        """
        @summary: Computes the coverage
        @see: mirna_prediction.py
            @param tab_seq: [list] locus sequences
        @return: [list] the coverage at each position
        """
        self.tab_coverage = [0] * (self.size_premir)
        for i in tab_seq:
            nb_occ = int(i.split('_')[0].split('#')[2].split('-')[0])
            seq_start = int(i.split('_')[1])
            seq_stop = int(i.split('_')[2])
            if not (seq_start < self.start_premir or seq_stop > self.stop_premir):
                idx_seq_start = max(0,int(i.split('_')[1]) - self.start_premir)
                idx_seq_stop = min(int(i.split('_')[2]) - self.start_premir,self.size_premir)
                for j in range(idx_seq_start,min(self.size_premir,idx_seq_stop)+1):
                    self.tab_coverage[j]+=nb_occ
        
        return self.tab_coverage
    
    def compute_structure(self, ssearch):
        """
        @summary: Updates the PreMiRNA structure from the SsearchMapping object
        @see: mirna_prediction.py
            @param ssearch: [SsearchMapping] object
        @requires: utils.dic_appariements()
        """
        
        import workflows.srnaseq.lib.utils as utils
        import sys
        dic = utils.dic_appariements()
        struc = ""        
        log = False
        
        if ssearch.zone == "BEFORE": 
            for i in range(0,(self.start_mature-self.start_premir)):
                struc+="."
            
            for i in reversed(range(0,len(ssearch.query_sequence))):
                if not ssearch.query_sequence[i] == "-":
                    if ssearch.library_sequence[i] in dic[ssearch.query_sequence[i]]:
                        struc+="("
                    else:
                        struc+="."
            
            for i in range(0,ssearch.loop):
                struc+="."
            
            for i in range(0,ssearch.query_start-1):
                struc+="."
            
            for i in range(0,len(ssearch.query_sequence)):
                if not ssearch.query_sequence[i] == "-":
                    if not ssearch.library_sequence[i] == "-":
                        if ssearch.library_sequence[i] in dic[ssearch.query_sequence[i]]:
                            struc+=")"
                        else:
                            struc+="."
                else:
                    struc+="."
            
            for i in range(0,self.stop_premir-self.stop_star):
                struc+="."
            
        else: # ssearch.zone == "AFTER"
            for i in range(0,(self.start_star-self.start_premir)):
                struc+="."
            
            for i in range(0,len(ssearch.query_sequence)):
                if not ssearch.query_sequence[i] == "-":
                    if not ssearch.library_sequence[i] == "-":
                        if ssearch.library_sequence[i] in dic[ssearch.query_sequence[i]]:
                            struc+="("
                        else:
                            struc+="."
                else:
                    struc+="."
            
            for i in range(0,ssearch.loop):
                struc+="."
            
            for i in range(0,ssearch.query_start-1):
                struc+="."
            
            for i in reversed(range(0,len(ssearch.query_sequence))):
                if not ssearch.query_sequence[i] == "-":
                    if ssearch.library_sequence[i] in dic[ssearch.query_sequence[i]]:
                        struc+=")"
                    else:
                        struc+="."
            
            for i in range(0,self.stop_premir-self.stop_mature):
                struc+="."
        
        self.structure = struc
        self.appariements = len([k for k in self.structure if k=="("])
        #print self.size_loop
        #print self.structure
        #print self.sequence
        #if (len(self.structure)!=len(self.sequence)) or self.size_loop < 0:
        #    sys.exit("Bad structure computation !!!")
        
    def write_BED(self):
        """
        @summary: Get informations to write in the BED file 
        @see: mirna_prediction.py
        @return: [str] line of BED
        """
        fields=[]
        fields.append(self.chromosome)
        fields.append(str(self.start_premir))
        fields.append(str(self.stop_premir))
        fields.append(self.name)
        fields.append(str(float(self.score)))
        fields.append(self.strand)
        fields.append(".")
        fields.append(".")
        return "\t".join(fields)
    
    def write_GFF3(self):
        """
        @summary: Get informations to write in the GFF3 file 
        @see: mirna_prediction.py
        @return: [str] line of GFF3
        """
        infos=[]
        infos.append(self.chromosome)
        infos.append("srnaplan_prediction")
        infos.append("miRNA_primary_transcript")
        infos.append(str(self.start_premir))
        infos.append(str(self.stop_premir))
        infos.append(str(int(self.score)))
        infos.append(self.strand)
        infos.append(".")
        attributes=[]
        maj_seq = str(self.majseq[0].split("_")[0])
        attributes.append("ID="+self.name)
        attributes.append("Name="+self.get_final_name_primary())
        attributes.append("ExprMax="+str(self.expr_maj))
        if self.clade != "Viridiplantae":
            attributes.append("Drosha="+str(self.is_drosha_compliant))
        attributes.append("Dicer="+str(self.is_dicer_compliant))
        attributes.append("Isoforms="+str(self.locus_nb_isoforms))
        attributes.append("SizeMatSeq="+str(self.size_mature_sequence))
        attributes.append("IdxAnSeq="+str(self.index_analyzed_seq))
        #attributes.append("StartPred="+str(self.start_mature))
        #attributes.append("StopPred="+str(int(self.start_mature)+int(self.length_mature)-1))
        infos.append(";".join(attributes))
        return "\t".join(infos)
    
    def write_GFF3_star(self):
        """
        @summary: Get informations to write in the GFF3 file 
        @see: mirna_prediction.py
        @return: [str] line of GFF3
        """
        infos=[]
        infos.append(self.chromosome)
        infos.append("srnaplan_prediction")
        infos.append("miRNA_star")
        infos.append(str(self.start_star))
        infos.append(str(self.stop_star))
        infos.append(str(int(self.score)))
        infos.append(self.strand)
        infos.append(".")
        attributes=[]
        maj_seq = str(self.majseq[0].split("_")[0])
        attributes.append("ID="+self.name+"_star")
        attributes.append("Name="+self.genome_id+"-miR-XXXX-"+self.get_position_star()+"_star")
        attributes.append("ExprMax="+str(self.expr_maj))
        attributes.append("Parent="+self.name)
        infos.append(";".join(attributes))
        return "\t".join(infos)
    
    def write_GFF3_mature(self):
        """
        @summary: Get informations to write in the GFF3 file 
        @see: mirna_prediction.py
        @return: [str] line of GFF3
        """
        infos=[]
        infos.append(self.chromosome)
        infos.append("srnaplan_prediction")
        infos.append("miRNA")
        infos.append(str(self.start_majseq))
        infos.append(str(self.stop_majseq))
        infos.append(str(int(self.score)))
        infos.append(self.strand)
        infos.append(".")
        attributes=[]
        maj_seq = str(self.majseq[0].split("_")[0])
        attributes.append("ID="+self.name+"_mature")
        attributes.append("Name="+self.genome_id+"-miR-XXXX-"+self.get_position_mature()+"_mature")
        attributes.append("ExprMax="+str(self.expr_maj))
        attributes.append("Parent="+self.name)
        infos.append(";".join(attributes))
        return "\t".join(infos)
    
    def write_gff3_annotation(self):
        """
        @summary: Get informations to write in the GFF3 file
        @see: functional_annotation.py
        @return: [str] line of GFF3
        """
        infos=[]
        for i in sorted(self.blast_annotations, key=lambda k: float(k['score']),reverse=True):
            infos.append("\n"+self.chromosome)
            infos.append(i["databank"])
            infos.append("match_part")
            infos.append(str(int(self.start_premir)+int(i["query_start"])-1))
            infos.append(str(int(self.start_premir)+int(i["query_stop"])-1))
            infos.append(str(i["score"]))
            infos.append(self.strand)
            infos.append(".")
            attributes=[]
            attributes.append("ID="+self.name+"_"+i["databank"])
            attributes.append("Name="+self.name+"_"+i["databank"])
            attributes.append("Parent="+self.name)
            attributes.append("hit="+i["hit"])
            attributes.append("species="+i["species"])
            attributes.append("id="+i["id"])
            attributes.append("percent_ident="+i["percent_id"])
            attributes.append("align_length="+i["align_length"])
            attributes.append("mism="+i["mismatchs"])
            attributes.append("gap="+i["gaps"])
            attributes.append("sbjt_start="+i["subject_start"])
            attributes.append("sbjt_stop="+i["subject_stop"])
            attributes.append("evalue="+i["evalue"])
            attributes.append("description="+i["description"])
            infos.append(";".join(attributes))
        return "\t".join(infos)
    
    def write_gff3_struc_annotation(self):
        """
        @summary: Get informations to write in the GFF3 file
        @see: structural_annotation.py
        @return: [str] line of GFF3
        """
        infos=[]
        for i in self.struc_annotations:
            infos.append("\n"+i["seqid"])
            infos.append(i["source"])
            infos.append(i["type"])
            infos.append(i["start"])
            infos.append(i["end"])
            infos.append(i["score"])
            infos.append(i["strand"])
            infos.append(i["phase"])
            infos.append(i["attributes"])
        return "\t".join(infos)


    def compute_bulges(self, struc):
        """
        @summary: Computes bulges count and max length of bulges
        @see: mirbase_analysis.py, mirna_prediction.py, miRNA.py
            @param struc: [str] the structure
        @return: [tuple] max_bulge and cpt_bulges
        """
        bulge = 0
        max_bulge = 0
        cpt_bulge = 0
        new=True
        for i in struc:
            if i == '.':
                bulge+=1
                max_bulge = max(max_bulge,bulge)
                if new:
                    cpt_bulge+=1
                new=False
            else:
                new=True
                bulge=0
        
        return (max_bulge, cpt_bulge)
    
    def check_star(self): ### COUVERTURE TOTALE DU MATURE ET DU STAR A VENIR EVENTUELLEMENT ####
        """
        @summary: Checks if star sequence is expressed
        @see: mirna_prediction.py
        @return: [boolean]
        """    
        mature_expressed = False
        
        for i in range(self.start_mature - self.start_premir,self.stop_mature - self.start_premir):
            if self.tab_coverage[i] != 0:
                mature_expressed = True
                break
        
        star_expressed = False
        for i in range(self.start_star - self.start_premir,self.stop_star - self.start_premir):
            if self.tab_coverage[i] != 0:
                star_expressed = True
                break

        return (mature_expressed and star_expressed)
    
    def profile_representation(self):
        expr = []
        app = []
        for idx,val in enumerate(self.expression):
            if (idx >= self.start_mature - self.start_premir and idx <= self.stop_mature- self.start_premir):
                app.append("X")
            elif (idx >= self.start_star- self.start_premir and idx <= self.stop_star- self.start_premir):
                app.append("x")
            else:
                app.append(".")
            if val > 0:
                expr.append("1")
            else:
                expr.append("0")
        return (expr, app)
        
    def check_structure_and_expression(self, tab_seq, clade, overhang):
        """
        @summary: Checks if star position is expressed and if Dicer and/or Drosha cuts is/are respected
        @see: mirna_prediction.py
        @return: (boolean,boolean)
        """

        dicer=True
        drosha = True
        if self.start_mature < self.start_star:
            start_loop = self.stop_mature + 1
            stop_loop = self.start_star - 1
            for j in tab_seq:
                isoform_start = int(j.split("_")[1])
                isoform_stop = int(j.split("_")[2])
                if ((isoform_start < start_loop + overhang) and (isoform_stop > start_loop + overhang)) or ((isoform_start < stop_loop - overhang) and (isoform_stop > stop_loop - overhang)):
                    dicer = False
                    break
            if clade != "Viridiplantae":
                if self.start_premir + overhang < self.start_mature:
                    drosha = False
        else:
            start_loop = self.stop_star + 1
            stop_loop = self.start_mature - 1
            for j in tab_seq:
                isoform_start = int(j.split("_")[1])
                isoform_stop = int(j.split("_")[2])
                if ((isoform_start < start_loop + overhang) and (isoform_stop > start_loop + overhang)) or ((isoform_start < stop_loop - overhang) and (isoform_stop > stop_loop - overhang)):
                    dicer = False
                    break
            if clade != "Viridiplantae":
                if self.stop_premir - overhang > self.stop_mature:
                    drosha = False
        
        star_expressed = False
        length_star = self.stop_star - self.start_star + 1
        nb_pb_star_expr = 0
        for i in range(self.start_star - self.start_premir,self.stop_star - self.start_premir):
            if self.tab_coverage[i] != 0:
                nb_pb_star_expr += 1
        
        if float(nb_pb_star_expr) > 0.9*float(length_star):
            star_expressed = True
        
        if clade != "Viridiplantae":
            return (dicer, drosha, star_expressed)
        else:
            return (dicer, None, star_expressed)