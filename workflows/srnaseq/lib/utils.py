#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    

import os, subprocess, datetime, sys, re, copy, time
from jflow.featureio import GFF3IO
from jflow.seqio import FastaReader

def get_strand_signification():
    """
    @summary: Get signification of each FLAG of BAM file
    @see: filter_bam.py, bam_to_locus.py
        @param: None
    @return: [dict] the dictionnary
    """
    
    dic={}
    dic["*"]="unmapped"
    dic["0"]="+"
    dic["4"]="unmapped"
    dic["20"]="unmapped"
    dic["16"]="-"
    dic["256"]="+"
    dic["272"]="-"
    
    return dic

def create_hybridation_matrix(file):
    """
    @summary: Writes the hybridation matrix file
    @see: mirbase_analysis.py, mirna_prediction.py
        @param file: [file] The empty hybridation matrix file
    @return: [str] the file name
    """

    file = open(file,'w')
    file.write("# RNA matrix favorising hybridation\n \
   A  C  G  T  U  R  Y  M  W  S  K  D  H  V  B  N  X\n \
A -1 -1 -1  2  2 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
C -1 -1  2 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
G -1  2 -1  0  0 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
T  2 -1  0 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
U  2 -1  0 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
R -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
Y -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
M -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
W -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
S -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
K -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
D -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
H -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
V -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
B -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
N -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1\n \
X -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1")
    file.close()
    return file.name

def load_count_by_library(matrix):
    matrix=open(matrix,"r")
    dic = {}
    for line in matrix:
        if line.startswith("#"):
            tab = line.rstrip().split("\t")
            for i in range(2,len(tab)):
                sample_id[i]=tab[i]
                
                

def load_count_matrix(matrix):
    """Load informations contained in the matrix file into a dictionnary "seq_id" -> {"sample_id" -> count} 
    @param matrix: the matrix file [FILE]
    @return: the dictionaries
    """
    matrix=open(matrix,"r")
    dic = {}
    dic_total_lib = {}
    sample_id = {}
    for line in matrix:
        if line.startswith("#"):
            tab = line.rstrip().split("\t")
            for i in range(2,len(tab)):
                sample_id[i]=tab[i]
        else:
            tab = line.rstrip().split("\t")
            for i in range(2,len(tab)):
                if not sample_id[i] in dic_total_lib:
                    dic_total_lib[sample_id[i]]=0
                dic_total_lib[sample_id[i]]+=int(tab[i])
                try:
                    dic[tab[0]][sample_id[i]]=tab[i]
                except KeyError:
                    dic[tab[0]]={}
                    dic[tab[0]][sample_id[i]]=tab[i]
                     
    matrix.close()
    return (dic, dic_total_lib)

def load_annotation_matrix(matrix):
    """Load informations contained in the matrix file into a dictionnary "seq_id" -> {"sample_id" -> count} 
    @param matrix: the matrix file [FILE]
    @return: the dictionaries
    """
    matrix=open(matrix,"r")
    dic = {}
    database_id = {}
    for line in matrix:
        line = line.rstrip()
        tab = line.rstrip().split("\t")
        if line.startswith("#"):
            for i in range(2,len(tab)):
                database_id[i]=tab[i]
        else:
            dic[tab[0]]={}
            tab = line.rstrip().split("\t")
            for i in range(2,len(tab)):
                if not tab[i] == "-":
                    dic[tab[0]][database_id[i]]=tab[i]
                     
    matrix.close()
    return (dic)

def get_personal_structures(list_struc_files, gff3_prediction_files):
    """
    @summary: Get personal structures (provided by user)
        @param list_struc: List of files containing structures to parse
        @param gff3_prediction_files: List of GFF3 files
        @return: the dictionary of structures
    """
    
    dic_strand_pred = {}
    nb_predictions = 0
    for file in gff3_prediction_files:
        gff3_reader = GFF3IO(file,"r")
        for entry in gff3_reader:
            nb_predictions+=1
            dic_strand_pred[entry.attributes["Name"]]=entry.strand
    
    dic_personal_struct = {}
    for file in list_struc_files:
        file = open(file,"r")
        for line in file:
            if line.startswith(">"):
                id = line.rstrip().replace(">","")
                if id in dic_personal_struct:
                    sys.exit(id + "is present at least two times in structure files ! Ids must be unique !")
                else:
                    dic_personal_struct[id]={}
            elif "(" in line:
                infos = re.split("\s+",line.rstrip())
                if dic_strand_pred[id] == "-":
                    struc = infos[0][::-1].replace("(","&").replace(")","(").replace("&",")")
                else:
                    struc = infos[0]
                freenrj = infos[1].replace("(","").replace(")","")
                dic_personal_struct[id]["struc"]=struc
                dic_personal_struct[id]["freenrj"]=freenrj
            else:
                pass
        file.close()
    return dic_personal_struct

def get_best_annotations(annotations):
    """Get best annotations from several annotation files
    @param annotations: List of annotation objects (self.args["annotation"])
    @return: the dictionnary of best annotations
    """
    dic_best_annot={}
    for i in annotations:
        reader = GFF3IO(i, "r")
        for entry in reader:
            if entry.attributes["Parent"] in dic_best_annot:
                if float(entry.score)>dic_best_annot[entry.attributes["Parent"]]["score"]:
                    dic_best_annot[entry.attributes["Parent"]]["score"]=float(entry.score)
                    dic_best_annot[entry.attributes["Parent"]]["best_id"]=entry.attributes["Name"]
                    dic_best_annot[entry.attributes["Parent"]]["infos"]=entry
            else:
                dic_best_annot[entry.attributes["Parent"]]={}
                dic_best_annot[entry.attributes["Parent"]]["score"]=float(entry.score)
                dic_best_annot[entry.attributes["Parent"]]["best_id"]=entry.attributes["Name"]
                dic_best_annot[entry.attributes["Parent"]]["infos"]=entry
                
    return dic_best_annot

def get_family_from_databank_id(id):
    
    dic_type_annot = {}
    dic_type_annot["mirbase"]="miRNA"
    dic_type_annot["mirbase-hairpin"]="miRNA"
    dic_type_annot["mirbase-mature"]="miRNA"
    dic_type_annot["rfam-mirna"]="miRNA"
    dic_type_annot["rfam-other"]="other"
    dic_type_annot["rfam-rrna"]="rRNA"
    dic_type_annot["silva-lsu"]="rRNA"
    dic_type_annot["silva-ssu"]="rRNA"
    dic_type_annot["rfam-trna"]="tRNA"
    dic_type_annot["eukaryotic-trnas"]="tRNA"
    dic_type_annot["pirna"]="piRNA"
    dic_type_annot["rfam-rnasemrp"]="RNAseMRP"
    dic_type_annot["rfam-rnasesrp"]="RNAseSRP"
    dic_type_annot["rfam-rnasep"]="RNAseP"
    dic_type_annot["rfam-snorna"]="snoRNA"
    dic_type_annot["rfam-snrna"]="snRNA"
    dic_type_annot["rfam-telomerase"]="telomerase"
    dic_type_annot["transposable-elements"]="transposable-elements"
    dic_type_annot["other"]="other"
    
    
    return dic_type_annot[id]

def reverseComplement(sequence):
    """
    @summary: Reverse-complement a nucleotidic sequence
    @see: bam_to_annotation_matrix.py
    @see: create_aln_line()
        @param sequence: [str] the sequence to transform
    @return: [str] the transformed sequence
    """
    
    complement_dict = {'A':'T','C':'G','G':'C','T':'A','N':'N','a':'T','c':'G','g':'C','t':'A','n':'N'}
    return "".join([complement_dict.get(nt.upper(), '') for nt in sequence[::-1]])

def get_parameters_from_mirbase(min_appariements, min_loop_size, max_loop_size, min_majseq_size, max_majseq_size, max_cpt_bulges, max_max_bulge, stats) :
    """
        
    """
    for line in stats:
        t = re.split("\t", line)
        if re.search(r"^appariements", line):
            min_appariements = int(t[1]) if int(min_appariements) == -1 else int(min_appariements)
        if re.search(r"^loop_size", line):
            min_loop_size = int(t[1]) if int(min_loop_size) == -1 else int(min_loop_size)
            max_loop_size = int(t[-1]) if int(max_loop_size) == -1 else int(max_loop_size)
        if re.search(r"^mature_length", line):
            min_majseq_size = int(t[1]) if int(min_majseq_size) == -1 else int(min_majseq_size)
            max_majseq_size = int(t[-1]) if int(max_majseq_size) == -1 else int(max_majseq_size)
        if re.search(r"^cpt_bulges", line):
            max_cpt_bulges = int(t[-1]) if int(max_cpt_bulges) == -1 else int(max_cpt_bulges)
        if re.search(r"^max_bulge", line):
            max_max_bulge = int(t[-1]) if int(max_max_bulge) == -1 else int(max_max_bulge)

    return (min_appariements, min_loop_size, max_loop_size, min_majseq_size, max_majseq_size, max_cpt_bulges, max_max_bulge)

def get_seq_from_fasta(lines_genome):
    """
    @summary: Stores a list with sequence nucleotides
        @param lines_genome: A "samtools faidx" output
    @return: list
    """
    tab_genome = []
    for i in lines_genome:
        if not i.startswith(">"):
            for j in i.split('\n'):
                for k in list(j):
                    tab_genome.append(k)
    return tab_genome


def is_already_treated(coord, start, stop):
    """
    @summary: Check if coordinates overlap start and stop
    @see: mirna_prediction.py
        @param coord: [list] the coordinates already treated (["start-stop",...])
        @param start: [int] the start to check
        @param stop: [int] the stop to check
    @return: [boolean]
    """
    
    if len(coord)==0:
        return False
    for i in coord:
        s1 = int(i.split('-')[0])
        s2 = int(i.split('-')[1])
        if ((start <= s1) & (stop <= s1)):
            return False
        if ((start >= s2) & (stop >= s2)):
            return False
    return True

def reverse(sequence):
    """
    @summary: Reverse a nucleotidic sequence
    @see: mirbase_analysis.py, mirna_prediction.py
        @param sequence: [str] the sequence to transform
    @return: [str] the transformed sequence
    """
    return ''.join(reversed(sequence))

def find_nearest_coordinate(coord, pos, space_locus, zone):
    """
    @summary: Find the nearest position
    @see: mirna_prediction.py
        @param coord: [list] coordinates already treated
        @param pos: [int] current position (start majseq)
        @param space_locus: [int] space between 2 locus at least
        @param zone: [str] BEFORE or AFTER
    @return: [int] a position
    """
    nearest = -1000000000000000
    if zone == "BEFORE":
        if len(coord)==0:
            return pos+space_locus
        for i in coord:
            s2 = int(i.split('-')[0])
            if s2 > pos:
                nearest = s2 if (abs(s2-pos)<(pos-nearest)) else nearest
        return nearest if nearest < pos+space_locus and s2 > pos+1 else pos+space_locus
    elif zone == "AFTER":
        if len(coord)==0:
            return pos-space_locus
        for i in coord:
            s2 = int(i.split('-')[1])
            if s2 < pos:
                nearest = s2 if (abs(s2-pos)<(pos-nearest)) else nearest
        return nearest if nearest > pos-space_locus and nearest < pos-1 else pos-space_locus
    else:
        sys.exit("Bad zone ID")


def ssearch36(ssearch_path, matrix, query, library, zone):
    """
    @summary: Performs a ssearch appariement
    @see: mirbase_analysis.py, mirna_prediction.py
        @param ssearch_path: [str] the ssearch path
        @param matrix: [str] the hybridation matrix file name
        @param query: [str] the file name containing the query sequence
        @param library: [str] the file name containing the library sequence (or inline command)
        @param zone: [str] After if the library starts after the query start, Before otherwise
    @return: array of SsearchMapping objects or None if no appariements
    @requires: utils.ssearch
    @requires: ssearch.SsearchMapping
    @requires: ssearch.compute_structure_without_loop()
    @requires: ssearch.compute_global_score()
    """
    import workflows.srnaseq.lib.ssearch as ssearch
    
    list_ssearch = []
        
    cmd = "bash -c '" +ssearch_path+" -z -1 -X LIB_MEMK=1G -T 1 -b 10 -m B -3 -f -1 -g -1 -w 200 -s " + matrix + ' ' + query + ' ' + library +"'"
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    process.wait()
        
    for line in iter(process.stdout.readline,''):
        if 'No sequences with E()' in line:
            return None
        if "Score =" in line:
            score_search = re.search('\((.*)\),', line.split()[4], re.IGNORECASE)
            score = score_search.group(1)
        if "Query" in line and not "Query:" in line:
            query_start = int(line.split()[1])
            query_stop = int(line.split()[3])
            query_sequence = line.split()[2]
        if "Sbjct" in line:
            library_start = int(line.split()[1])
            library_stop = int(line.split()[3])
            library_sequence = line.split()[2]
            new_ssearch = ssearch.SsearchMapping(query_start, query_stop, library_start, library_stop, score, query_sequence, library_sequence, zone)
            new_ssearch.compute_structure_without_loop()
            new_ssearch.personal_score = new_ssearch.compute_global_score(None, False)
            list_ssearch.append(new_ssearch)
    return list_ssearch if len(list_ssearch)>0 else None

def best_ssearch(app_tab):
    """
    @summary: Selects the best ssearch appariement from a list of SsearchMapping objects
    @see: mirbase_analysis.py, mirna_prediction.py
        @param app_tab: the array of SsearchMapping objects [List]
    @return: [SsearchMapping] the best SsearchMapping object or None
    """
    if len(app_tab)==0:
        return None
    good_tab=[]
    for i in app_tab:
        if i is not None:
            good_tab.append(i)
    sorted_tab = sorted(good_tab, key=lambda x: int(x.personal_score), reverse=True)
    try:
        return sorted_tab[0]
    except IndexError:
        sys.exit("best_ssearch function error")

def dic_appariements():
    """
    @summary: Get correspondance of apparied nucleotides
    @see: SsearchMapping.compute_structure_without_loop()
    @return: [dict] 
    """
    dic={}
    dic['A']=['T','U']
    dic['C']='G'
    dic['G']=['C','T','U']
    dic['T']=['A','G']
    dic['U']=['A','G']
    dic['N']=[]
    
    return dic

def evaluate_score_from_mirbase(value, array, coeff):
    """
    @summary: Scores thanks to mirbase values
    @see: ssearch.py
        @param value: [int] the value to compare with array values
        @param array: [list] the array of values
        @param coeff: [int] the coefficient to apply
    @return: the score [int]
    """
    
    score=0
    if int(array[3]) <= value <= int(array[4]):
        score+=5*coeff
        return score
    elif int(array[4]) <= value <= int(array[5]):
        score+=5*coeff
        return score
    elif int(array[2]) <= value <= int(array[3]):
        score+=4*coeff
        return score
    elif int(array[5]) <= value <= int(array[6]):
        score+=4*coeff
        return score
    elif int(array[1]) <= value <= int(array[2]):
        score+=3*coeff
        return score
    elif int(array[6]) <= value <= int(array[7]):
        score+=3*coeff
        return score
    elif int(array[0]) <= value <= int(array[1]):
        score+=2*coeff
        return score
    elif int(array[7]) <= value <= int(array[8]):
        score+=2*coeff
        return score
    elif value <= int(array[0]):
        score-=-1*coeff
        return score
    elif int(array[8]) <= value:
        score-=-1*coeff
        return score

def blast(log, blast_path, databank, databank_id, query, evalue, percent_ident, align_length, nm, gap, bit_score, separator):
    """
    @summary: Performs a Blast and add infos to the ncRNA object
        @param log: 
        @param blast_path: [str]
        @param databank: [file]
        @param databank_id: [str]
        @param query: [file]
        @param ncrna: [PreMiRNA | tRNA | Locus] object
        @param evalue: [str] blast e-value
        @param separator: [char]
    @requires: utils.add_hit_and_species()
    """
    import workflows.srnaseq.lib.utils as utils
    
    cmd = "bash -c '" + blast_path + " -p blastn " + " -b 1 -v 1 -F T -m 8 -i " + query + " -d "+ databank + "'"
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    annot = {}
    if not process.stdout is None:
        infos = []
        for line in process.stdout:
            if not line == "\n":
                line.rstrip()
                infos = re.split("\s+",line)
                if utils.keep_blast(infos, evalue, percent_ident, align_length, nm, gap, bit_score):
                    utils.add_hit_and_species(annot,infos,databank_id,separator)
                    annot['databank']=databank_id
                    annot['percent_id']=infos[2]
                    annot['align_length']=infos[3]
                    annot['mismatchs']=infos[4]
                    annot['gaps']=infos[5]
                    annot['query_start']=infos[6]
                    annot['query_stop']=infos[7]
                    annot['subject_start']=infos[8]
                    annot['subject_stop']=infos[9]
                    annot['evalue']=infos[10]
                    annot['score']=infos[11]
                    annot['description']=infos[1].replace(separator,"|")
                log.write(databank_id+"\t"+"\t".join(infos)+"\n")
    return annot

def keep_blast(infos, evalue, percent_ident, align_length, nm, gap, bit_score):
    """
    """
    if (float(infos[2])<float(percent_ident)):
        return False
    elif (int(infos[3])<int(align_length)):
        return False
    elif (int(infos[4])>int(nm)):
        return False
    elif (int(infos[5])>int(gap)):
        return False
    elif (float(infos[10])>float(evalue)):
        return False
    elif (float(infos[11])<float(bit_score)):
        return False
    else:
        return True

def add_hit_and_species(annot, infos, databank_id, separator):
    """
    @summary: Add hit and species infos to the dictionnary annot relatively to databank id
        @param annot: [dict] dictionnary of blast informations
        @param infos: [list] infos from blast
        @param databank_id: [str] Databank Id
        @param separator: [char] separator
    """
    list_transposable_elements = ["transposable-elements"]
    list_rfam = ['rfam-mirna', 'rfam-other', 'rfam-rrna', 'rfam-trna', 'rfam', 'rfam-rnasep', 'rfam-rnasemrp', 'rfam-rnasesrp', 'rfam-telomerase']
    list_mirbase = ['mirbase-hairpin', 'mirbase-mature', 'mirbase']
    list_silva = ['silva-lsu', 'silva-ssu']
    dic_corr = {'silva-lsu':'rRNA', 'silva-ssu':'rRNA', 'transposable-elements':'TE', "eukaryotic-trnas":'tRNA' }
    if databank_id in list_mirbase:
        if databank_id in ["mirbase-mature","mirbase"]:
            try:
                annot['hit']=infos[1].split(separator)[1]
            except IndexError:
                annot['hit']="ErrorParsingMirbaseHIT"+infos[1]
            try:    
                annot['species']=infos[1].split('-')[0]
            except IndexError:
                annot['species']="ErrorParsingMirbaseSPECIES"+infos[1]
            try:
                annot['id']=infos[1].split(separator)[-1]
            except IndexError:
                annot['id']="none"
        else:
            try:
                annot['hit']=infos[1].split(separator)[1]
            except IndexError:
                annot['hit']="ErrorParsingMirbaseHIT"+infos[1]
            try:    
                annot['species']=infos[1].split('-')[0]
            except IndexError:
                annot['species']="ErrorParsingMirbaseSPECIES"+infos[1]
            try:
                annot['id']=infos[1].split(separator)[-2]
            except IndexError:
                annot['id']="none"
            
    elif databank_id == "eukaryotic-trnas":
        annot['hit']="none"
        annot['id']=infos[1].split("|")[0].split(".")[1].replace("-","_")
        try:
            annot['species']=infos[1]+"".join(infos[1].split("_")[1][i] for i in (0,1,2,3))
            annot['species']=infos[1].split("_")[0][0]+("".join(infos[1].split("_")[1][i] for i in (0,1,2,3))).lower()
        except IndexError:
            annot['species']="ErrorParsingTRNASPECIES"+infos[1]
    
    elif databank_id == "pirna":
        try:
            annot['hit']=infos[1].split(separator)[0]
        except IndexError:
            annot['hit']="ErrorParsingMirbaseHIT"+infos[1]
        try:    
            annot['species']=infos[1].split('_')[0]
        except IndexError:
            annot['species']="ErrorParsingMirbaseSPECIES"+infos[1]
        try:
            annot['id']=infos[1].split(separator)[2]
        except IndexError:
            annot['id']="none"
    
    elif databank_id in list_rfam:
        annot['species']='none'
        try:
            annot['hit']=infos[1].split(";")[0]
        except IndexError:
            annot['hit']="ErrorParsingRfamSPECIES"+infos[1]
        try:
            annot['id']=infos[1].split(";")[1]
        except IndexError:
            annot['id']="none"
    
    elif databank_id in list_silva:
        annot['id']=dic_corr[databank_id]
        try:
            annot['hit']=infos[1].split('.')[0]
        except IndexError:
            annot['hit']="ErrorParsingSilvaSPECIES "+infos[1]
        annot['species']=" ".join("".join(infos[1].split(separator)[-1]).split(separator))

    else:
        annot['id']="none"
        annot['species']='none'
        annot['hit']="none"

