#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

class rRNA(object):
    def __init__(self, genome_id, name, chr, start, stop, strand):
        self.name = name
        self.genome_id = genome_id
        self.chromosome = chr
        self.start = start
        self.stop = stop
        self.size = 0
        self.strand = strand
        self.length = 0
        self.type = ""
        self.score = 0.0
        self.structure = ""
        self.freenrj = 0
        self.sequence = ""
        self.tab_seq = []
        self.tab_coverage = []
        self.majseq = []
        self.prediction_start = 0
        self.prediction_stop = 0
        self.annotations = []
        self.blast_annotations = []
        #self.family = "rRNA"
        self.struc_annotations = []

    def write_GFF3(self):
        """
        @summary: Get informations to write in the GFF3 file 
        @see: trna_prediction.py
        @return: [str] line of GFF3
        """
        infos=[]
        attributes=[]
        infos.append(self.chromosome)
        infos.append("srnaplan_prediction")
        infos.append("rRNA_primary_transcript")
        infos.append(str(self.prediction_start))
        infos.append(str(self.prediction_stop))
        infos.append(str(float(self.score)))
        infos.append(self.strand)
        infos.append(".")
        attributes.append("ID="+self.name)
        attributes.append("Name="+self.genome_id+"-rRNA-XXXX-5p")
        infos.append(";".join(attributes))
        return "\t".join(infos)

    def write_BED(self):
        """
        @summary: Get informations to write in the BED file 
        @see: trna_prediction.py
        @return: [str] line of BED
        """
        fields=[]
        fields.append(self.chromosome)
        fields.append(str(self.prediction_start))
        fields.append(str(self.prediction_stop))
        fields.append(self.name)
        fields.append(str(float(self.score)))
        fields.append(self.strand)
        fields.append(".")
        fields.append(".")
        return "\t".join(fields)

    def write_gff3_annotation(self):
        """
        @summary: Get informations to write in the GFF3 file
        @see: functional_annotation.py
        @return: [str] line of GFF3
        """
        infos=[]
        for i in sorted(self.blast_annotations, key=lambda k: float(k['score']),reverse=True):
            infos.append("\n"+self.chromosome)
            infos.append(i["databank"])
            infos.append("match_part")
            infos.append(str(int(self.start)+int(i["query_start"])-1))
            infos.append(str(int(self.start)+int(i["query_stop"])-1))
            infos.append(str(i["score"]))
            infos.append(self.strand)
            infos.append(".")
            attributes=[]
            attributes.append("ID="+self.name+"_"+i["databank"])
            attributes.append("Name="+self.name+"_"+i["databank"])
            attributes.append("Parent="+self.name)
            attributes.append("hit="+i["hit"])
            attributes.append("species="+i["species"])
            attributes.append("id="+i["id"])
            attributes.append("percent_ident="+i["percent_id"])
            attributes.append("align_length="+i["align_length"])
            attributes.append("mism="+i["mismatchs"])
            attributes.append("gap="+i["gaps"])
            attributes.append("sbjt_start="+i["subject_start"])
            attributes.append("sbjt_stop="+i["subject_stop"])
            attributes.append("evalue="+i["evalue"])
            attributes.append("description="+i["description"])
            infos.append(";".join(attributes))
        return "\t".join(infos)

    def write_gff3_struc_annotation(self):
        """
        @summary: Get informations to write in the GFF3 file
        @see: structural_annotation.py
        @return: [str] line of GFF3
        """
        infos=[]
        for i in self.struc_annotations:
            infos.append("\n"+i["seqid"])
            infos.append(i["source"])
            infos.append(i["type"])
            infos.append(i["start"])
            infos.append(i["end"])
            infos.append(i["score"])
            infos.append(i["strand"])
            infos.append(i["phase"])
            infos.append(i["attributes"])
        return "\t".join(infos)
