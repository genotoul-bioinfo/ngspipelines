#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import re, os, subprocess, pickle
import numpy as np
import operator
from operator import itemgetter

from ngspipelines.biomart_db import *
from ngspipelines.analysis import *
from ngspipelines.utils import *
from decimal import *
from jflow.seqio import FastaReader
from jflow.featureio import GFF3IO

import utils as utils

class Locus(BiomartMain):

    GRAPH_CLASS_PARTITION = 30
    
    def get_mart_display_name(self):
        return "smallRNAs"
    
    def define_fields(self):
        self.name =                     BiomartVARCHARField("name", size=64, is_primary=True, favorites_position=1)
        self.display_name =             BiomartVARCHARField("display_name", size=64, favorites_position=2)
        self.family =                   BiomartVARCHARField("family", size=16, favorites_position=3)
        self.chromosome =               BiomartVARCHARField("chromosome", size=64, favorites_position=4)
        self.start =                    BiomartINTField("start", size=10, favorites_position=5)
        self.stop =                     BiomartINTField("stop", size=10, favorites_position=6)
        self.best_hit_type =            BiomartVARCHARField("best_hit_type", size=255, favorites_position=7)
        self.best_hit_family =          BiomartVARCHARField("best_hit_family", size=255, favorites_position=8)
        self.best_hit_database =        BiomartVARCHARField("best_hit_database", size=255, favorites_position=9)
        self.prediction_score =         BiomartFLOATField("prediction_score", size=16, favorites_position=10)
        self.strand =                   BiomartINTField("strand", size=4)
        self.length =                   BiomartINTField("length", size=4)
        self.best_hit_description =     BiomartMEDIUMTEXTField("best_hit_description")
        self.best_hit_accession =       BiomartVARCHARField("best_hit_accession", size=255)
        self.best_hit_query_start =     BiomartINTField("best_hit_query_start", size=10)
        self.best_hit_query_stop =      BiomartINTField("best_hit_query_stop", size=10)
        self.best_hit_subject_start =   BiomartINTField("best_hit_subject_start", size=10)
        self.best_hit_subject_stop =    BiomartINTField("best_hit_subject_stop", size=10)
        self.best_hit_score =           BiomartFLOATField("best_hit_score", size=16)
        self.best_hit_evalue =          BiomartFLOATField("best_hit_evalue", size=16)
        self.best_hit_perc_identities = BiomartFLOATField("best_hit_perc_identities", size=16)
        self.best_hit_mismatch =        BiomartINTField("best_hit_mismatch", size=8)
        self.best_hit_align_length =    BiomartINTField("best_hit_align_length", size=10)
        self.best_hit_species =         BiomartVARCHARField("best_hit_species", size=255)
        
        self.name.add_filter(display_name = 'Name (one line per locus name)', type = FilterTypes.UPLOAD, group = 'Locus description')
        self.family.add_filter(type = FilterTypes.MULTI_SELECT, group = 'Locus description')
        self.chromosome.add_filter(display_name = 'Database', type = FilterTypes.MULTI_SELECT, group = 'Localization')
        self.chromosome.add_filter(display_name = 'Name (one line per reference name)', type = FilterTypes.UPLOAD, group = 'Localization')
        self.start.add_filter(display_name = 'Start >=', qualifier = FilterQualifiers.EQUALS_GREATER, group = 'Localization')
        self.stop.add_filter(display_name = 'End <=', qualifier = FilterQualifiers.EQUALS_GREATER, group = 'Localization')
        self.length.add_filter(display_name = 'Length >=', qualifier = FilterQualifiers.EQUALS_GREATER, group = 'Length')
        self.length.add_filter(display_name = 'Length <=', qualifier = FilterQualifiers.EQUALS_LOWER, group = 'Length')
        self.best_hit_accession.add_filter(type = FilterTypes.MULTI_SELECT, group = 'Best annotation filter')
        self.best_hit_database.add_filter(type = FilterTypes.MULTI_SELECT, group = 'Best annotation filter')
        self.add_link_to_report(self.name)
    
    def _get_pred_scores_infos(self, dic_locus_annot):
        ## Scores distribution ############################
        dic_scores = {}    
        dic_scores={}
        dic_scores["only"]=[]
        dic_scores["both"]=[]
        dic_scores["misannotated"]=[]
        dic_scores["new"]=[]
        for elem in sorted(dic_locus_annot.items(), key=lambda i: (i[1]["score"]), reverse=True):
            annot = elem[1]["annot"]
            score = elem[1]["score"]
            for a in dic_scores.iterkeys():
                if a == elem[1]["annot"]:
                    dic_scores[a].append(elem[1]["score"])
                else:
                    dic_scores[a].append("null")
    
        x_values = []
        labels = []
        y_values = []
        idx = 0
        for i in dic_scores.iterkeys():
            labels.append(i)
            x_values.append(idx)
            y_values.append(dic_scores[i])
            idx+=1
        
        return labels, x_values, y_values
    
    def load(self, predictions, best_annotations_file, dic_type_annot, general_stats_obj):
        
        nb_locus, nb_bases, locus_len = 0, 0, []
        locus_depth = []
        maj_len = []
        family = ''
        
        
        dic_best_annot = {}
        best_annot_reader = GFF3IO(best_annotations_file,"r")
        for a_entry in best_annot_reader:
            if a_entry.attributes["Parent"] in dic_best_annot:
                sys.exit("Duplicated field in best annotations file")
            else:
                dic_best_annot[a_entry.attributes["Parent"]]={}
                dic_best_annot[a_entry.attributes["Parent"]]["score"]=float(a_entry.score)
                dic_best_annot[a_entry.attributes["Parent"]]["best_id"]=a_entry.attributes["ID"]
                dic_best_annot[a_entry.attributes["Parent"]]["infos"]=a_entry
        
        if not isinstance(predictions, basestring):
            for pred in predictions:
                reader = GFF3IO(pred, "r")
                for entry in reader:
                    if "primary_transcript" in entry.type:
                        locus_name = entry.attributes["ID"]
                        display_name = entry.attributes["Name"]
                        start = entry.start
                        stop = entry.end
                        strand = entry.strand
                        length = entry.end - entry.start + 1
                        chromosome = entry.seq_id
                        family = entry.type.split("_primary_transcript")[0]
                        self.add_row({        "name":                   locus_name,
                                              "display_name":           display_name,
                                              #"family":                 family,
                                              "family":                 family,
                                              "start":                  start,
                                              "stop":                   stop,
                                              "strand":                 1 if strand == "+" else -1,
                                              "length":                 length,
                                              "chromosome":             chromosome,
                                              "prediction_score":       entry.score,
                                              "best_hit_database":      dic_best_annot[locus_name]["infos"].source if locus_name in dic_best_annot else "\N",
                                              "best_hit_accession":     dic_best_annot[locus_name]["infos"].attributes["hit"] if locus_name in dic_best_annot and dic_best_annot[locus_name]["infos"].attributes["hit"] != 'none' else "\N",
                                              "best_hit_description":   dic_best_annot[locus_name]["infos"].attributes["description"] if locus_name in dic_best_annot else "\N",
                                              "best_hit_query_start":   dic_best_annot[locus_name]["infos"].start if locus_name in dic_best_annot else "\N",
                                              "best_hit_query_stop":    dic_best_annot[locus_name]["infos"].end if locus_name in dic_best_annot else "\N",
                                              "best_hit_subject_start": dic_best_annot[locus_name]["infos"].attributes["sbjt_start"] if locus_name in dic_best_annot else "\N",
                                              "best_hit_subject_stop":  dic_best_annot[locus_name]["infos"].attributes["sbjt_stop"] if locus_name in dic_best_annot else "\N",
                                              "best_hit_score":         dic_best_annot[locus_name]["infos"].score if locus_name in dic_best_annot else "\N",
                                              "best_hit_evalue":        dic_best_annot[locus_name]["infos"].attributes["evalue"] if locus_name in dic_best_annot else "\N",
                                              "best_hit_perc_identities":   dic_best_annot[locus_name]["infos"].attributes["percent_ident"] if locus_name in dic_best_annot else "\N",
                                              "best_hit_mismatch":      dic_best_annot[locus_name]["infos"].attributes["mism"] if locus_name in dic_best_annot else "\N",
                                              "best_hit_align_length":  dic_best_annot[locus_name]["infos"].attributes["align_length"] if locus_name in dic_best_annot else "\N",
                                              "best_hit_family":        dic_best_annot[locus_name]["infos"].attributes["id"] if locus_name in dic_best_annot else "\N",
                                              "best_hit_type":          dic_type_annot[dic_best_annot[locus_name]["infos"].source] if locus_name in dic_best_annot else "\N",
                                              "best_hit_species":       dic_best_annot[locus_name]["infos"].attributes["species"] if locus_name in dic_best_annot else "\N",
                                });
            
                ## General statistics table #######################
                table_infos = general_stats_obj[0][family]
                self.project.add_table(analysis_name="General statistics", 
                               header=["", "Predicted", "Annotated", "Misannotated", "Both", "Potential new"],
                               rows=[[family, table_infos["predicted"], table_infos["annotated"], table_infos["misannotated"], table_infos["both"], table_infos["new"]]],
                               soft_name="",
                               comments="",
                               chart_title="General",
                               chart_subtitle="statistics")
                
                ## Scores distribution ############################
                if family != "locus":
                    dic_locus_annot = general_stats_obj[1][family]
                    labels, x_values, y_values = self._get_pred_scores_infos(dic_locus_annot)
                    analysis_name = family + " scores"
                    self.project.add_scatterplot(analysis_name=analysis_name, 
                                 soft_name="sRNA-PlAn",
                                 chart_title=family + " prediction scores",
                                 chart_subtitle="Distribution",
                                 x_values = x_values,
                                 y_values = y_values,
                                 labels = labels,
                                 x_title="x axis", 
                                 y_title="y axis",
                                 tooltip="###Z### : <br>Rank : ###X###, Score : <strong>###Y###</strong>")
            
                ## Length distribution ############################
                dic_lengths = general_stats_obj[2][family]
                uniq_keys = np.unique(dic_lengths)
                uniq_keys_two = []
                for i in range(uniq_keys[0],uniq_keys[-1]+1):
                    uniq_keys_two.append(i)
                values = list(np.bincount(np.asarray(uniq_keys_two).searchsorted(dic_lengths)))
                
                if len(uniq_keys) < self.GRAPH_CLASS_PARTITION*2:                    
                    hist_labels = uniq_keys_two
                    self.project.add_basicline( analysis_name=family+" length distribution", 
                                            soft_name="-",
                                            labels = uniq_keys_two,
                                            values=values,
                                            y_title="Number of elements",
                                            chart_title="Length",
                                            tooltip="There are <strong>###Y### </strong>###Z### with a size of <strong>###X### bp</strong>",
                                            chart_subtitle="distribution")
                else:
                    hist_labels = sorted(uniq_keys)
                    for idx, val in enumerate(sorted(uniq_keys)):
                        values[idx] += dic_lengths.count(val)
                    self.project.add_column( analysis_name=family+" length distribution", 
                                    soft_name="-",
                                    labels = hist_labels,
                                    values=values,
                                    y_title="Number of elements",
                                    chart_title="Locus Length",
                                    tooltip="There are <strong>###Y### </strong>###Z### with a size of <strong>###X### bp</strong>",
                                    chart_subtitle="distribution")
            
                ## Best annotations ###############################
                if family in general_stats_obj[3]:
                    dic_best_by_family = general_stats_obj[3][family]
                    self.project.add_piechart(analysis_name=family+" best annotations", 
                                                chart_title=family + " annotations", 
                                                labels=dic_best_by_family.keys(), 
                                                values=dic_best_by_family.values(),
                                                chart_subtitle="distribution", 
                                                tooltip="###X### <b>###Y###</b>")
            
        else:
            reader = GFF3IO(predictions, "r")
            for entry in reader:
                if "primary_transcript" in entry.type:
                    locus_name = entry.attributes["ID"]
                    display_name = entry.attributes["Name"]
                    #print ">>> ", display_name
                    start = entry.start
                    stop = entry.end
                    strand = entry.strand
                    length = entry.end - entry.start + 1
                    chromosome = entry.seq_id
                    family = entry.type.split("_primary_transcript")[0]
                    self.add_row({        "name":                   locus_name,
                                          "display_name":           display_name,
                                          #"family":                 family,
                                          "family":                 family,
                                          "start":                  start,
                                          "stop":                   stop,
                                          "strand":                 1 if strand == "+" else -1,
                                          "length":                 length,
                                          "chromosome":             chromosome,
                                          "prediction_score":       entry.score,
                                          "best_hit_database":      dic_best_annot[locus_name]["infos"].source if locus_name in dic_best_annot else "\N",
                                          "best_hit_accession":     dic_best_annot[locus_name]["infos"].attributes["hit"] if locus_name in dic_best_annot and dic_best_annot[locus_name]["infos"].attributes["hit"] != 'none' else "\N",
                                          "best_hit_description":   dic_best_annot[locus_name]["infos"].attributes["description"] if locus_name in dic_best_annot else "\N",
                                          "best_hit_query_start":   dic_best_annot[locus_name]["infos"].start if locus_name in dic_best_annot else "\N",
                                          "best_hit_query_stop":    dic_best_annot[locus_name]["infos"].end if locus_name in dic_best_annot else "\N",
                                          "best_hit_subject_start": dic_best_annot[locus_name]["infos"].attributes["sbjt_start"] if locus_name in dic_best_annot else "\N",
                                          "best_hit_subject_stop":  dic_best_annot[locus_name]["infos"].attributes["sbjt_stop"] if locus_name in dic_best_annot else "\N",
                                          "best_hit_score":         dic_best_annot[locus_name]["infos"].score if locus_name in dic_best_annot else "\N",
                                          "best_hit_evalue":        dic_best_annot[locus_name]["infos"].attributes["evalue"] if locus_name in dic_best_annot else "\N",
                                          "best_hit_perc_identities":   dic_best_annot[locus_name]["infos"].attributes["percent_ident"] if locus_name in dic_best_annot else "\N",
                                          "best_hit_mismatch":      dic_best_annot[locus_name]["infos"].attributes["mism"] if locus_name in dic_best_annot else "\N",
                                          "best_hit_align_length":  dic_best_annot[locus_name]["infos"].attributes["align_length"] if locus_name in dic_best_annot else "\N",
                                          "best_hit_family":        dic_best_annot[locus_name]["infos"].attributes["id"] if locus_name in dic_best_annot else "\N",
                                          "best_hit_type":          dic_type_annot[dic_best_annot[locus_name]["infos"].source] if locus_name in dic_best_annot else "\N",
                                          "best_hit_species":       dic_best_annot[locus_name]["infos"].attributes["species"] if locus_name in dic_best_annot else "\N",
                            });
        
            ## General statistics table #######################
            table_infos = general_stats_obj[0][family]
            self.project.add_table(analysis_name="General statistics", 
                           header=["", "Predicted", "Annotated", "Misannotated", "Both", "Potential new"],
                           rows=[[family, table_infos["predicted"], table_infos["annotated"], table_infos["misannotated"], table_infos["both"], table_infos["new"]]],
                           soft_name="",
                           comments="",
                           chart_title="General",
                           chart_subtitle="statistics")
            
            ## Scores distribution ############################
            if family != "locus":
                dic_locus_annot = general_stats_obj[1][family]
                labels, x_values, y_values = self._get_pred_scores_infos(dic_locus_annot)   
                analysis_name = family + " scores"
                self.project.add_scatterplot(analysis_name=analysis_name, 
                             soft_name="sRNA-PlAn",
                             chart_title=family + " prediction scores",
                             chart_subtitle="Distribution",
                             x_values = x_values,
                             y_values = y_values,
                             labels = labels,
                             x_title="x axis", 
                             y_title="y axis",
                             tooltip="###Z### : <br>Rank : ###X###, Score : <strong>###Y###</strong>")
        
            ## Length distribution ############################
            dic_lengths = general_stats_obj[2][family]
            uniq_keys = np.unique(dic_lengths)
            uniq_keys_two = []
            for i in range(uniq_keys[0],uniq_keys[-1]+1):
                uniq_keys_two.append(i)
            values = list(np.bincount(np.asarray(uniq_keys_two).searchsorted(dic_lengths)))
            
            if len(uniq_keys) < self.GRAPH_CLASS_PARTITION*2:                    
                hist_labels = uniq_keys_two
                self.project.add_basicline( analysis_name=family+" length distribution", 
                                        soft_name="-",
                                        labels = uniq_keys_two,
                                        values=values,
                                        y_title="Number of elements",
                                        chart_title="Length",
                                        tooltip="There are <strong>###Y### </strong>###Z### with a size of <strong>###X### bp</strong>",
                                        chart_subtitle="distribution")
            else:
                hist_labels = sorted(uniq_keys)
                for idx, val in enumerate(sorted(uniq_keys)):
                    values[idx] += dic_lengths.count(val)
                self.project.add_column( analysis_name=family+" length distribution", 
                                soft_name="-",
                                labels = hist_labels,
                                values=values,
                                y_title="Number of elements",
                                chart_title="Locus Length",
                                tooltip="There are <strong>###Y### </strong>###Z### with a size of <strong>###X### bp</strong>",
                                chart_subtitle="distribution")
        
            ## Best annotations ###############################
            dic_best_by_family = general_stats_obj[3][family]
            self.project.add_piechart(analysis_name=family+" best annotations", 
                                        chart_title=family, 
                                        labels=dic_best_by_family.keys(), 
                                        values=dic_best_by_family.values(),
                                        chart_subtitle="best annotations", 
                                        tooltip="###X### <b>###Y###</b>")

class Annotation(BiomartDimension):
    
    def define_fields(self):
        self.hit_database =         BiomartVARCHARField("annotation_hit_database", size=255)
        self.hit_type =             BiomartVARCHARField("annotation_hit_type", size=124)
        self.hit_accession =        BiomartVARCHARField("annotation_hit_accession", size=255)
        self.hit_id =               BiomartVARCHARField("annotation_hit_id", size=255)
        self.hit_description =      BiomartMEDIUMTEXTField("annotation_hit_description")
        self.hit_query_start =      BiomartINTField("annotation_hit_query_start", size=10)
        self.hit_query_stop =       BiomartINTField("annotation_hit_query_stop", size=10)
        self.hit_subject_start =    BiomartINTField("annotation_hit_subject_start", size=10)
        self.hit_subject_stop =     BiomartINTField("annotation_hit_subject_stop", size=10)
        self.hit_is_best_annot =    BiomartTINYINTField("annotation_hit_is_best_annot", size=1)
        self.hit_score =            BiomartFLOATField("annotation_hit_score", size=16)
        self.hit_evalue =           BiomartFLOATField("annotation_hit_evalue", size=16)
        self.hit_perc_identities =  BiomartFLOATField("annotation_hit_perc_identities", size=16)
        self.hit_mismatch =         BiomartINTField("annotation_hit_mismacth", size=8)
        self.hit_align_length =     BiomartINTField("annotation_hit_align_length", size=10)
        self.hit_species =          BiomartVARCHARField("annotation_hit_species", size=255)
        
        self.hit_type.add_filter(display_name = 'Type', type = FilterTypes.MULTI_SELECT) #OK
        self.hit_database.add_filter(display_name = 'Database', type = FilterTypes.MULTI_SELECT) #OK
        self.hit_accession.add_filter(display_name = 'Accession', type = FilterTypes.UPLOAD)
        self.hit_accession.add_filter(display_name = 'Accession', type = FilterTypes.MULTI_SELECT)
        self.hit_description.add_filter(display_name = 'Description (use % as joker)', qualifier = FilterQualifiers.LIKE)
        self.hit_is_best_annot.add_filter(display_name = 'Is best annot', type = FilterTypes.BOOLEAN )
    
    def load(self, annotations, best_annotations_file, dic_type_annot):
        
        dic_best_annot = {}
        best_annot_reader = GFF3IO(best_annotations_file,"r")
        for entry in best_annot_reader:
            if entry.attributes["Parent"] in dic_best_annot:
                sys.exit("Duplicated field in best annotations file")
            else:
                dic_best_annot[entry.attributes["Parent"]]={}
                dic_best_annot[entry.attributes["Parent"]]["score"]=float(entry.score)
                dic_best_annot[entry.attributes["Parent"]]["best_id"]=entry.attributes["ID"]
                dic_best_annot[entry.attributes["Parent"]]["infos"]=entry
        
        family = ""
        if not isinstance(annotations, basestring):
            for annot in annotations:
                reader = GFF3IO(annot, "r")
                for entry in reader:
                    if entry.attributes["Parent"] in dic_best_annot:
                        family = entry.attributes["Parent"].split("_")[0]
                        self.add_row(entry.attributes["Parent"],{  
                                               "hit_database":      entry.source,
                                               "hit_type":          dic_type_annot[entry.source],
                                               "hit_accession":     entry.attributes["hit"],
                                               "hit_id":            entry.attributes["id"],
                                               "hit_species":       entry.attributes["species"],
                                               "hit_description":   entry.attributes["description"],
                                               "hit_query_start":   entry.start,
                                               "hit_query_stop":    entry.end,
                                               "hit_subject_start": entry.attributes["sbjt_start"],
                                               "hit_subject_stop":  entry.attributes["sbjt_stop"],
                                               "hit_is_best_annot": 0 if not entry.attributes["ID"] in dic_best_annot[entry.attributes["Parent"]]["best_id"] else 1,
                                               "hit_score":         entry.score,
                                               "hit_evalue":        entry.attributes["evalue"],
                                               "hit_perc_identities":   entry.attributes["percent_ident"],
                                               "hit_mismatch":      entry.attributes["mism"],
                                               "hit_align_length":  entry.attributes["align_length"]
                                        });
        else:
            reader = GFF3IO(annotations, "r")
            for entry in reader:
                if entry.attributes["Parent"] in dic_best_annot:
                    family = entry.attributes["Parent"].split("_")[0]
                    self.add_row(entry.attributes["Parent"],{  
                                           "hit_database":      entry.source,
                                           "hit_type":          dic_type_annot[entry.source],
                                           "hit_accession":     entry.attributes["hit"],
                                           "hit_id":            entry.attributes["id"],
                                           "hit_species":       entry.attributes["species"],
                                           "hit_description":   entry.attributes["description"],
                                           "hit_query_start":   entry.start,
                                           "hit_query_stop":    entry.end,
                                           "hit_subject_start": entry.attributes["sbjt_start"],
                                           "hit_subject_stop":  entry.attributes["sbjt_stop"],
                                           "hit_is_best_annot": 0 if not entry.attributes["ID"] in dic_best_annot[entry.attributes["Parent"]]["best_id"] else 1,
                                           "hit_score":         entry.score,
                                           "hit_evalue":        entry.attributes["evalue"],
                                           "hit_perc_identities":   entry.attributes["percent_ident"],
                                           "hit_mismatch":      entry.attributes["mism"],
                                           "hit_align_length":  entry.attributes["align_length"]
                                    });
    
        
class DNA(BiomartDimension):
    
    def define_fields(self):
        self.sequence =         BiomartTEXTField("dna_sequence")

    def load(self, fastas):

        infos_fasta = {}
        for i in fastas:
            fasta = FastaReader(i)
            for seq in fasta :
                infos_fasta[seq.name] = seq.sequence
        
        for i in infos_fasta.iterkeys():
            self.add_row(i , {"sequence": infos_fasta[i]})

class StructuralAnnotation(BiomartDimension):
    def define_fields(self):
        self.type =     BiomartVARCHARField("structural_annotation_type", size=124)
        self.gene_id = BiomartVARCHARField("structural_annotation_gene_id", size=124)
        self.distance = BiomartINTField("structural_annotation_distance", size=10)
        self.description =    BiomartVARCHARField("structural_annotation_description", size=124)
        self.start =    BiomartINTField("structural_annotation_start", size=10)
        self.stop =     BiomartINTField("structural_annotation_stop", size=10)
        self.strand =   BiomartINTField("structural_annotation_strand", size=4)
        
        self.type.add_filter(display_name = 'Type', type = FilterTypes.MULTI_SELECT)
        
    def load(self, structural_annotations):
        
        if not isinstance(structural_annotations, basestring):
            for annot in structural_annotations:
                reader = GFF3IO(annot, "r")
                for entry in reader:
                    locus_name = entry.attributes["PParent"]
                    distance = entry.attributes["Distance"]
                    try:
                        gene_id = entry.attributes["gene_id"]
                    except:
                        try:
                            gene_id = entry.attributes["Parent"]
                        except:
                            try:
                                gene_id = entry.attributes["Name"]
                            except:
                                sys.exit("Structural annotation file must have gene_id or Parent field ! Exit...")
                    description=""
                    for i in entry.attributes:
                        description += i+":"+entry.attributes[i]
                    self.add_row(entry.attributes["PParent"],{
                                        "type"              : entry.type,
                                        "gene_id"           : gene_id,
                                        "distance"          : distance,
                                        "description"       : description,
                                        "start"             : entry.start,
                                        "stop"              : entry.end,
                                        "strand"            : 1 if entry.strand == "+" else -1
                    });
        else:
            reader = GFF3IO(structural_annotations, "r")
            for entry in reader:
                locus_name = entry.attributes["PParent"]
                distance = entry.attributes["Distance"]
                try:
                    gene_id = entry.attributes["gene_id"]
                except:
                    try:
                        gene_id = entry.attributes["Parent"]
                    except:
                        try:
                            gene_id = entry.attributes["Name"]
                        except:
                            sys.exit("Structural annotation file must have gene_id or Parent field ! Exit...")
                description=""
                for i in entry.attributes:
                    description += i+":"+entry.attributes[i]
                self.add_row(entry.attributes["PParent"],{
                                    "type"              : entry.type,
                                    "gene_id"           : gene_id,
                                    "distance"          : distance,
                                    "description"       : description,
                                    "start"             : entry.start,
                                    "stop"              : entry.end,
                                    "strand"            : 1 if entry.strand == "+" else -1
                });
        
class Structure(BiomartDimension):
    
    def define_fields(self):
        self.structure1 =        BiomartTEXTField("structure_structure1")
        self.free_energy1 =      BiomartFLOATField("structure_free_energy1", size=16)
        self.structure2 =        BiomartTEXTField("structure_structure2")
        self.free_energy2 =      BiomartFLOATField("structure_free_energy2", size=16)
        
    def load(self, structures_file, dic_personal_struct):
        
        file = open(structures_file,"r")
        for line in file:
            if line.startswith(">"):
                name = re.search(">(\S+)",line).group(0).replace(">","")
            elif not re.match("^[ACTGUN]", line):
                line = line.replace("( ","(").replace("(  ","(")
                free_energy1 = re.split("\s+",line)[1].replace("(","").replace(")","")
                structure1 = re.split("\s+",line)[0]
                structure2 = ""
                free_energy2 = ""
                if name in dic_personal_struct:
                    structure2 = dic_personal_struct[name]["struc"]
                    free_energy2 = dic_personal_struct[name]["freenrj"]

                self.add_row(name,{ "structure1":       structure1,
                                    "free_energy1":     free_energy1,
                                    "structure2":       structure2,
                                    "free_energy2":     free_energy2
                                });
        file.close()
        
class Prediction(BiomartDimension):
    
    SELECT_MOST_SCORED = 40
    
    def define_fields(self):
        self.name =             BiomartVARCHARField("prediction_name", size=64)
        self.score =            BiomartFLOATField("prediction_score", size=6)
        self.description =      BiomartVARCHARField("prediction_description", size=255)
        self.start =            BiomartINTField("prediction_start", size=10)
        self.stop =             BiomartINTField("prediction_stop", size=10)
        self.nb_reads =         BiomartINTField("prediction_nb_read", size=20)
    
        self.nb_reads.add_filter(display_name = 'Expression >=', qualifier = FilterQualifiers.EQUALS_GREATER, group='Expression')
        self.nb_reads.add_filter(display_name = 'Expression <=', qualifier = FilterQualifiers.EQUALS_LOWER, group='Expression')
        self.score.add_filter(display_name = 'Score >=', qualifier = FilterQualifiers.EQUALS_GREATER, group='Score')
        self.score.add_filter(display_name = 'Score <=', qualifier = FilterQualifiers.EQUALS_LOWER, group='Score')
    
    def load(self, my_predictions_infos):
        
        most_scored = {}
        for locus in my_predictions_infos:
            most_scored[locus]=float(my_predictions_infos[locus]["score"])
            self.add_row(locus,{    "score":        my_predictions_infos[locus]["score"],
                                    "start":        my_predictions_infos[locus]["start"],
                                    "stop":         my_predictions_infos[locus]["end"],
                                    "nb_reads":     my_predictions_infos[locus]["nb_reads"]
                                });
        
        # add the best scored locus as favorites
        for i, best in enumerate(sorted(most_scored.iteritems(), key=operator.itemgetter(1), reverse=True)[:Prediction.SELECT_MOST_SCORED]):
            self.add_favorite(best[0])
    
class ReadLibraryCount(BiomartDimension):
    
    
    
    def define_fields(self):
        self.library_id =       BiomartINTField("readlibrarycount_library_id", size=10)
        self.sample_name =      BiomartVARCHARField("readlibrarycount_sample_name", size=255)
        self.replicat =         BiomartINTField("readlibrarycount_replicat", size=4)
        self.nb_read =          BiomartBIGINTField("readlibrarycount_nb_read", size=20)
        self.coverage =         BiomartTEXTField("readlibrarycount_coverage")
        self.mean_depth =       BiomartFLOATField("readlibrarycount_mean_depth", size=16)
        self.tissue =           BiomartVARCHARField("readlibrarycount_tissue", size=124)
        self.dev_stage =        BiomartVARCHARField("readlibrarycount_dev_stage", size=124)
    
        self.sample_name.add_filter(type = FilterTypes.MULTI_SELECT, group = 'Library field')
        self.tissue.add_filter(type = FilterTypes.MULTI_SELECT, group = 'Library field')
        self.nb_read.add_filter(display_name = 'Nb reads >=', qualifier = FilterQualifiers.EQUALS_GREATER, group = 'Number of reads')
        self.nb_read.add_filter(display_name = 'Nb reads <=', qualifier = FilterQualifiers.EQUALS_LOWER, group = 'Number of reads')
        self.mean_depth.add_filter(display_name = 'Mean depth <=', qualifier = FilterQualifiers.EQUALS_LOWER, group = 'Depth per library')
        self.mean_depth.add_filter(display_name = 'Mean depth >=', qualifier = FilterQualifiers.EQUALS_GREATER, group = 'Depth per library')
    
    def load(self, my_rlc_infos, libraries):
        
        dic_libraries = {}
        for i in libraries:
            dic_libraries[i.library_name] = {}
            dic_libraries[i.library_name]["dev_stage"] =    i.dev_stage
            dic_libraries[i.library_name]["tissue"] =       i.tissue
            dic_libraries[i.library_name]["sample_name"] =  i.sample_name
            dic_libraries[i.library_name]["replicat"] =     i.replicat
            dic_libraries[i.library_name]["id"] =           i.id
        
        most_expressed = {}
        for locus in my_rlc_infos:
            most_expressed[locus] = 0
            for sample in my_rlc_infos[locus]:
                most_expressed[locus] += int(my_rlc_infos[locus][sample]["NB_READ"])
                self.add_row(locus,{    "library_id":   dic_libraries[sample]["id"],
                                        "sample_name":  dic_libraries[sample]["sample_name"],
                                        "replicat":     dic_libraries[sample]["replicat"],
                                        "nb_read":      my_rlc_infos[locus][sample]["NB_READ"],
                                        "coverage":     ",".join([str(x) for x in my_rlc_infos[locus][sample]["COVERAGE"]]),
                                        "tissue":       dic_libraries[sample]["tissue"],
                                        "dev_stage":    dic_libraries[sample]["dev_stage"],
                                        "mean_depth":   float(my_rlc_infos[locus][sample]["MEAN_DEPTH"])
                            });
        
class Isoform(BiomartDimension): 
    
    def define_fields(self):
        self.chromosome =           BiomartVARCHARField("isoform_chromosome", size=64)
        self.start =                BiomartINTField("isoform_start", size=8)
        self.stop =                 BiomartINTField("isoform_stop", size=8)
        self.strand =               BiomartINTField("isoform_strand", size=4)
        self.NM =                   BiomartINTField("isoform_NM", size=1)
        self.MD =                   BiomartVARCHARField("isoform_MD", size=10)
        self.sequence =             BiomartVARCHARField("isoform_sequence", size=124)
        self.annotation =           BiomartVARCHARField("isoform_annotation", size=255)
        self.nb_reads =             BiomartINTField("isoform_nb_reads", size=10)
        self.name =                 BiomartVARCHARField("isoform_name", size=64)
        self.nb_samples =           BiomartINTField("isoform_nb_samples", size=10)
        self.libraries =            BiomartVARCHARField("isoform_libraries", size=2048)
        self.annotations =          BiomartVARCHARField("isoform_annotations", size=2048)

        self.name.add_filter(display_name = 'Name (use % as joker)', qualifier = FilterQualifiers.LIKE)

    def load(self, my_isoforms_infos):
        
        for locus in my_isoforms_infos:
            for isoform in my_isoforms_infos[locus]:
                self.add_row(locus,{    "chromosome":   isoform["chromosome"],
                                        "start":        isoform["start"],
                                        "stop":         isoform["stop"],
                                        "strand":       1 if isoform["strand"] == "+" else -1,
                                        "NM":           isoform["NM"],
                                        "MD":           isoform["MD"],
                                        "sequence":     isoform["sequence"],
                                        "name":         isoform["name"],
                                        "nb_reads":     isoform["nb_reads"],
                                        "nb_samples":   isoform["nb_samples"],
                                        "libraries":    isoform["libraries"],
                                        "annotations":  isoform["annotation"]
                                    });
