#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

class tRNA(object):
    def __init__(self, genome_id, name, chr, start, stop, strand):
        self.name = name
        self.genome_id = genome_id
        self.chromosome = chr
        self.start = int(start)
        self.stop = int(stop)
        self.size = stop - start + 1
        self.strand = strand
        self.length = 0
        self.type = ""
        self.anticodon = ""
        self.score = 0.0
        self.structure = ""
        self.freenrj = 0
        self.sequence = ""
        self.tab_seq = []
        self.tab_coverage = []
        self.majseq = []
        self.prediction_start = 0
        self.prediction_stop = 0
        self.annotations = []
        self.reads_annotations = []
        self.blast_annotations = []
        #self.family = "tRNA"
        self.struc_annotations = []

    def get_seq_in_zone(self,tab, start, stop):
        """
        @summary: From a list of Locus strings (seq45#45#17_start_stop), get those overlapping start and stop
        @see: trna_prediction.py
            @param tab: [list] list of Locus strings
            @param start: [str] the start
            @param stop: [str] the stop
        @return: the list of overlapping Locus strings
        """
        temp_tab_seq=[]
        start = int(start)
        stop = int(stop)
        for seq in tab:
            if ( (start <= int(seq.split('_')[1]) <= stop)  | (start <= int(seq.split('_')[2]) <= stop) ): 
                temp_tab_seq.append(seq)
        tab=list(temp_tab_seq)
        return tab

    def find_coordinates_prediction(self):
        """
        @summary: Find coordinates of prediction : min of prediction start and seq min start, idem with end
        @see: trna_prediction.py
        """
        min_start = 10000000000000000000000000000000000000000000000000000 
        max_stop = -1
        
        for i in self.tab_seq:
            min_start = min(min_start,min(self.prediction_start,int(i.split('_')[1])))
            max_stop = max(max_stop,max(self.prediction_stop,int(i.split('_')[2])))

        self.start = max(max(0,min_start),self.prediction_start)
        self.stop = min(max_stop,self.prediction_stop)
        self.size = int(self.stop)-int(self.start)+1

    def coverage(self):
        """
        @summary: Computes the coverage of the tRNA
        @see: trna_prediction.py
        @return: [list] coverage at each position
        """
        self.tab_coverage = [0] * (self.size)
        for i in self.tab_seq:
            nb_occ = int(i.split('_')[0].split('#')[2].split('-')[0])
            seq_start = max(0,int(i.split('_')[1]) - self.prediction_start)
            seq_stop = min(int(i.split('_')[2]) - self.prediction_start,self.size)
            for j in range(seq_start,min(self.size,seq_stop)):
                self.tab_coverage[j]+=nb_occ

        return self.tab_coverage

    def write_GFF3(self):
        """
        @summary: Get informations to write in the GFF3 file 
        @see: trna_prediction.py
        @return: [str] line of GFF3
        """
        infos=[]
        attributes=[]
        infos.append(self.chromosome)
        infos.append("srnaplan_prediction")
        infos.append("tRNA_primary_transcript")
        infos.append(str(self.prediction_start))
        infos.append(str(self.prediction_stop))
        infos.append(str(float(self.score)))
        infos.append(self.strand)
        infos.append(".")
        attributes.append("ID="+self.name)
        attributes.append("Name="+self.genome_id+"-tRNA-XXXX-5p")
        infos.append(";".join(attributes))
        return "\t".join(infos)

    def write_BED(self):
        """
        @summary: Get informations to write in the BED file 
        @see: trna_prediction.py
        @return: [str] line of BED
        """
        fields=[]
        fields.append(self.chromosome)
        fields.append(str(self.prediction_start))
        fields.append(str(self.prediction_stop))
        fields.append(self.name)
        fields.append(str(float(self.score)))
        fields.append(self.strand)
        fields.append(".")
        fields.append(".")
        return "\t".join(fields)

    def write_gff3_annotation(self):
        """
        @summary: Get informations to write in the GFF3 file
        @see: functional_annotation.py
        @return: [str] line of GFF3
        """
        infos=[]
        for i in sorted(self.blast_annotations, key=lambda k: float(k['score']),reverse=True):
            infos.append("\n"+self.chromosome)
            infos.append(i["databank"])
            infos.append("match_part")
            infos.append(str(int(self.prediction_start)+int(i["query_start"])-1))
            infos.append(str(int(self.prediction_start)+int(i["query_stop"])-1))
            infos.append(str(i["score"]))
            infos.append(self.strand)
            infos.append(".")
            attributes=[]
            attributes.append("ID="+self.name+"_"+i["databank"])
            attributes.append("Name="+self.name+"_"+i["databank"])
            attributes.append("Parent="+self.name)
            attributes.append("hit="+i["hit"])
            attributes.append("species="+i["species"])
            attributes.append("id="+i["id"])
            attributes.append("percent_ident="+i["percent_id"])
            attributes.append("align_length="+i["align_length"])
            attributes.append("mism="+i["mismatchs"])
            attributes.append("gap="+i["gaps"])
            attributes.append("sbjt_start="+i["subject_start"])
            attributes.append("sbjt_stop="+i["subject_stop"])
            attributes.append("evalue="+i["evalue"])
            attributes.append("description="+i["description"])
            infos.append(";".join(attributes))
        return "\t".join(infos)

    def write_gff3_struc_annotation(self):
        """
        @summary: Get informations to write in the GFF3 file
        @see: structural_annotation.py
        @return: [str] line of GFF3
        """
        infos=[]
        for i in self.struc_annotations:
            infos.append("\n"+i["seqid"])
            infos.append(i["source"])
            infos.append(i["type"])
            infos.append(i["start"])
            infos.append(i["end"])
            infos.append(i["score"])
            infos.append(i["strand"])
            infos.append(i["phase"])
            infos.append(i["attributes"])
        return "\t".join(infos)


def parse_tRNAscanSE_structure(struc_file, trna):
    """
    @summary: Parse the tRNAScan-SE output and constructs and adds informations to the tRNA object
    @see: trna_prediction.py
        @param struc_file: [str] the file name of tRNAScan-SE output
        @param cpt_id: [int] the id of current tRNA
        @param new_locus: [Locus] the current locus
    @return: trna [tRNA] the trna object
    @requires: utils.reverseComplement()
    """
    import workflows.srnaseq.lib.utils as utils
    import re
    
    for line in struc_file:
        array_line = re.split("\s+", line)
        if 'Length:' in line:
            trna.length = int(array_line[3])
            trna.chromosome = array_line[0].split(":")[0]
            start_trna_on_seq = int(array_line[1].split("-")[0].replace("(",""))
            stop_trna_on_seq = int(array_line[1].split("-")[1].replace(")",""))
            start_seq = int(array_line[0].split("-")[0].split(":")[1])
            
            trna.prediction_start = start_seq + min(start_trna_on_seq,stop_trna_on_seq)-1
            trna.prediction_stop = start_seq + max(start_trna_on_seq,stop_trna_on_seq)-1
            trna.strand = "+" if (start_trna_on_seq < stop_trna_on_seq) else "-"
        
        if "Type" in line:
            trna.type = array_line[1]
            trna.score = float(array_line[8])
        
        if "Seq:" in line:
            if trna.strand == "-":
                trna.sequence = utils.reverseComplement(array_line[1])
                trna.prediction_sequence = trna.sequence
            else:
                trna.sequence = array_line[1]
                trna.prediction_sequence = trna.sequence
        if "Str:" in line:
            if trna.strand == "-":
                trna.structure = array_line[1].replace(">",")").replace("<","(")[::-1]
            else:
                trna.structure = array_line[1].replace("<",")").replace(">","(")
            trna.freenrj = 0
    return trna