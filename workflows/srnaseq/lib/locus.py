#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    

class LocusReader:
    """
     @summary : Specific handler for locus file.
    """
    def __init__( self, file_path, mode="w" ):
        self._path = file_path
        self._handle = open( file_path, mode )
        self._line = 1
    
    def __del__( self ):
        self.close()
    
    def __iter__( self ):
        for line in self._handle:
            line = line.rstrip()
            self._line += 1
            if line.startswith('#') :
                continue
            try:
            
                locus_record = Locus.fromLocus(line)
            except:
                raise IOError( "The line " + str(self._line) + " in '" + self._path + "' cannot be parsed by " + self.__class__.__name__ + ".\n" +
                               "Line content : " + line )
            else:
                yield locus_record
            
    def close( self ) :
        if self._handle is not None:
            self._handle.close()
    
    def write( self, locus_record ):
        """
         @summary : Write one line on locus file.
            @param locus_record : [Locus] the object to write.
        """
        self._handle.write( str(locus_record) + "\n" )
        self._line += 1
    
class Locus:
    
    def __init__(self):
        self.name = None
        self.genome_id = None
        self.id = None
        self.nb_isoforms = None
        self.chromosome = None
        self.start = None
        self.stop = None
        self.strand = None
        self.tab_seq = []
    
    @staticmethod
    def fromLocus(line):
        line_fields = line.split("\t")
        locus_record = Locus()
        locus_record.name = line_fields[0]
        locus_record.nb_isoforms = int(line_fields[1])
        locus_record.chromosome = line_fields[2]
        locus_record.start = int(line_fields[3])
        locus_record.stop = int(line_fields[4])
        locus_record.strand = line_fields[5]
        locus_record.start_majseq = int(line_fields[7].split('_')[1])
        locus_record.stop_majseq = int(line_fields[7].split('_')[2])
        locus_record.tab_seq = line_fields[6]
        locus_record.tab_seq = locus_record.reformat_tab_seq(locus_record.tab_seq)
        locus_record.majseq = locus_record.get_maj_seq()
        return locus_record
    
    def reformat_tab_seq(self, line):
        """
        @summary: From the locus line, format seq into a list
        @see: trna_prediction.py, mirna_prediction.py, extract_unpredicted_loci.py
            @param line: [str] Locus strings (['seq7167#1#9_3942_3964', 'seq1287#1#8_3979_4002', ... ])
        @return: [list] well-formated list
        """
        tab = []
        all = line.replace(' ','').replace('[','').replace(']','').replace('\'','').split(",")
        for i in all:
            tab.append(i)
        return tab
    
    def get_maj_seq(self):
        """
        @summary: Get the majseq from the list of tab_seq
        @see: bam_to_locus.py, trna_prediction.py, mirna_prediction.py, extract_unpredicted_loci.py
        @return: [list] the list containing the majoritary sequence(s)
        """
        
        max=0
        if len(self.tab_seq)==0:
            self.majseq=""
            return 0
        for i in self.tab_seq:
            nb_occ = int(i.split('_')[0].split('#')[2].split('-')[0])
            if int(nb_occ)>=max:
                max=int(nb_occ)
                seq=i
        
        return seq
    
    def update_coord_majseq(self):
        """
        @summary: Updates start_majseq and stop_majseq
        @see: mirna_prediction.py, extract_unpredicted_loci.py
        """
        self.start_majseq = int(self.majseq.split('_')[1])
        self.stop_majseq = int(self.majseq.split('_')[2])
    
    def update_locus(self, start, stop):
        """
        @summary: Update the locus object : suppress seq between start and stop,
                                            constructs tab coverage
                                            updates majseq and its coordinates
        @see: mirna_prediction.py
            @param start: [int]
            @param stop: [int]
        @requires: Locus.suppress_seq_in_zone()
        @requires: Locus.get_maj_seq()
        @requires: Locus.update_coord_majseq()
        """
        self.suppress_seq_in_zone(start, stop)
        self.majseq=[]
        if self.get_maj_seq() != 0:
            self.majseq = self.get_maj_seq()
            self.update_coord_majseq()
            self._update_coord_locus()
    
    def suppress_seq_in_zone(self, start, stop):
        """
        @summary: Suppress seq overlapping start or stop
            @param start: [int] start position
            @param stop: [int] stop position
        """
        temp_tab_seq=list(self.tab_seq)
        for seq in self.tab_seq:
            seq_start = int(seq.split('_')[1])
            seq_stop = int(seq.split('_')[2])
            if ((seq_start >= start and seq_start <= stop) or (seq_stop <= stop and seq_stop >= start)):
                temp_tab_seq.remove(seq)
            
        self.tab_seq=list(temp_tab_seq)
    
    def _update_coord_locus(self):
        """
        @summary: Updates coordinates of the Locus object
        """
        tab_start = []
        tab_stop = []
        if len(self.tab_seq)>0:
            for i in self.tab_seq:
                tab_start.append(int(i.split("_")[1]))
                tab_stop.append(int(i.split("_")[2]))
            self.start = min(tab_start)
            self.stop = max(tab_stop)
    
    def write_GFF3(self):
        """
        @summary: Get informations to write in the GFF3 file 
        @see: bam_to_locus.py, extract_unpredicted_loci.py
        @return: [str] line of GFF3
        """
        infos=[]
        infos.append(self.chromosome)
        infos.append("srnaplan_prediction")
        infos.append("locus_primary_transcript")
        infos.append(str(self.start))
        infos.append(str(self.stop))
        infos.append(str(0))
        infos.append(self.strand)
        infos.append(".")
        attributes=[]
        attributes.append("ID="+self.name)
        attributes.append("Name="+self.genome_id+"-loci-XXXX-5p")
        infos.append(";".join(attributes))
        return "\t".join(infos)
    
    def are_coord_overlapping_locus(self, start, stop):
        """
        @summary: Checks if start and stop are overlapping the Locus coordinates
        @see: extract_unpredicted_loci.py
            @param start: [int]
            @param stop: [stop]
        @return: [boolean] 
        """
        if ((start >= self.start and start <= self.stop) or (stop >= self.start and stop <= self.stop)):
            return True
        else:
            return False
    
    def keep_seq_in_zone(self, start, stop):
        """
        @summary: Get seq between start and stop from tab_seq
        @see: extract_unpredicted_loci.py
            @param start: [int]
            @param stop: [int]
        @return: [list] seq 
            
        """
        temp_tab_seq=list(self.tab_seq)
        for seq in self.tab_seq:
            seq_start = int(seq.split('_')[1])
            seq_stop = int(seq.split('_')[2])
            if ( (seq_stop <= start)  | (seq_start >= stop) ):
                temp_tab_seq.remove(seq)
        return list(temp_tab_seq)
    
    def __str__(self):
        """
        @summary: toString()
        @see: bam_to_locus.py
        @return: [str] Locus
        """
        return self.name+"\t"+str(self.nb_isoforms)+"\t"+self.chromosome+"\t"+str(self.start)+"\t"+str(self.stop)+"\t"+self.strand+"\t"+str(self.tab_seq)+"\t"+str(self.majseq)
    