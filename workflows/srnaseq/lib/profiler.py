#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    

#from rpy import *
THRESHOLD = 1.00

class Point(object):
    
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def __str__(self):
        return "(%d, %d)" % (self.x, self.y)

class Points(object):
    
    def __init__(self, size):
        self.points = [Point(0, 0) for i in range(size)]

    def copy(self, points):
        for i in range(len(self.points)):
            self.setX(i, points.getX(i))
            self.setY(i, points.getY(i))

    def setPoint(self, i, x, y):
        self.points[i].x = x
        self.points[i].y = y

    def getPoint(self, i):
        return self.points[i]

    def getPoints(self):
        for point in self.points:
            yield point

    def setX(self, i, x):
        self.points[i].x = x

    def getX(self, i):
        return self.points[i].x

    def setY(self, i, y):
        self.points[i].y = y

    def getY(self, i):
        return self.points[i].y

    def __str__(self):
        return " ".join(["%s" % (point) for point in self.points])

class MiRNAProfile(object):

    def __init__(self, nbPoints, data, verbosity = 0):
        self.verbosity = verbosity
        self.nbPoints  = nbPoints
        self.data      = data
        self.points    = Points(self.nbPoints)
        self.observedSurface = sum(self.data)
        self.xs = set()
        for x in range(len(self.data)-1):
            if self.data[x] != self.data[x+1]:
                self.xs.add(x)
                self.xs.add(x+1)
            self.xs.add(0)
            self.xs.add(len(self.data)-1)
        self.ys = set(self.data)

    def setPlateauShape(self, minPlateauWidth, maxPlateauWidth, minPlateauHeight):
        self.minPlateauHeight = minPlateauHeight
        self.minPlateauWidth  = minPlateauWidth
        self.maxPlateauWidth  = maxPlateauWidth

    def setLoopSize(self, minLoop):
        self.minLoop = minLoop

    def setMinValleyRatio(self, minValleyRatio):
        self.minValleyRatio = minValleyRatio

    def _getHeightAfter(self, i, x):
        x1 = self.points.getX(i)
        x2 = self.points.getX(i+1)
        y1 = self.points.getY(i)
        y2 = self.points.getY(i+1)
        a = float(y2 - y1) / (x2 - x1)
        b = (y1 * x2 - y2 * x1) / (x2 - x1)
        return a * x + b

    def _getHeight(self, x):
        for i in range(NB_POINTS):
            if x < self.points[i+1].x:
                return self._getHeightAfter(i, x)

    def _getSurface(self):
        surface = 0.0
        for i in range(self.nbPoints - 1):
            if self.points.getX(i) != self.points.getX(i+1):
                surface += (float(self.points.getY(i+1) + self.points.getY(i)) / 2) * (self.points.getX(i+1) - self.points.getX(i)) - self.points.getY(i+1)
        return surface + self.points.getY(self.nbPoints-1)

    def _findBestMovePoint(self, i):
        save = Points(self.nbPoints)
        save.copy(self.points)
        best = Points(self.nbPoints)
        best.copy(self.points)
        bestScore = self.getScore()
        startX = 0                if i == 0               else self.points.getX(i-1)
        endX   = len(self.data)-1 if i == self.nbPoints-1 else self.points.getX(i+1)
        for x in self.xs:
            if startX <= x <= endX:
                for y in self.ys:
                    self.points.setPoint(i, x, y)
                    score = self.getScore()
                    if score < bestScore:
                        best.copy(self.points)
                        bestScore = score
        self.points.copy(save)
        return (bestScore, best)

    def _findBestMoveStep(self):
        best = Points(self.nbPoints)
        best.copy(self.points)
        bestScore = self.getScore()
        for i in range(self.nbPoints):
            score, best = self._findBestMovePoint(i)
            if score < bestScore:
                best.copy(self.points)
                bestScore = score
        return (bestScore, best)

    def findBestMove(self):
        bestScore = self.getScore()
        score, points = self._findBestMoveStep()
        #print "From", self.points
        while score < bestScore:
            self.points.copy(points)
        #    print "\tto", points
            bestScore = score
            score, points = self._findBestMoveStep()

    def _findBestPlace(self, i, bestScore, best):
        if i == self.nbPoints-1:
            score = self.getScore()    
            if bestScore == None or score < bestScore:
                bestScore = score
                best.copy(self.points)
            return (bestScore, best)
        for x in self.xs:
            if i == 0:
                print x
            if i == 0 or x >= self.points.getX(i-1):
                for y in self.ys:
                    self.points.setPoint(i, x, y)
                    score = self.getScoreUpTo(i)
                    if bestScore == None or score < bestScore:
                        score, points = self._findBestPlace(i+1, bestScore, best)
                        if bestScore == None or score < bestScore:
                            bestScore = score
                            best.copy(self.points)
        return (bestScore, best)

    def findBestPlace(self):
        bestScore = None
        best      = Points(self.nbPoints)
        bestScore, best = self._findBestPlace(0, bestScore, best)
        self.points.copy(best)

    def plot(self, fileName):
        r.png(fileName, width=700, height=500)
        r.plot(range(len(self.data)), self.data, 'l', xlab = "", ylab = "")
        r.points([point.x for point in self.points.getPoints()], [point.y for point in self.points.getPoints()], pch = range(65, 65+self.nbPoints))
        r.dev_off()

    def getScoreUpTo(self, last):
        surface = 0
        if self.points.getX(last) == 0:
            return 0
        for x in range(self.points.getX(0)):
            if self.data[x] != 0:
                surface += 1.0
        for i in range(last + 1):
            for x in range(self.points.getX(i), self.points.getX(i+1)):
                height = self._getHeightAfter(i, x)
                surface += 0 if max(height, self.data[x]) == 0 else abs(height - self.data[x])/float(max(height, self.data[x]))
        return surface / self.points.getX(last)

    def getScore(self):
        surface = 0
        for x in range(self.points.getX(0)):
            if self.data[x] != 0:
                surface += 1.0
        for i in range(self.nbPoints - 1):
            for x in range(self.points.getX(i), self.points.getX(i+1)):
                height = self._getHeightAfter(i, x)
                surface += 0 if max(height, self.data[x]) == 0 else abs(height - self.data[x])/float(max(height, self.data[x]))
        for x in range(self.points.getX(self.nbPoints-1), len(self.data)):
            if self.data[x] != 0:
                surface += 1.0
        #print surface
        return surface / len(self.data)


class MiRNAProfile12Points(MiRNAProfile):

    def __init__(self, data, verbosity = 0):
        super(MiRNAProfile12Points, self).__init__(12, data, verbosity)

    def placePoints(self):
        size   = len(self.data)
        startX = 0      if self.data[0] != 0      else min([x for x in range(size) if self.data[x] != 0])-1
        endX   = size-1 if self.data[size-1] != 0 else max([x for x in range(size) if self.data[x] != 0])+1

        try:
            firstMax    = min([x for x in range(size-1) if self.data[x] > self.data[x+1]])
            lastMax     = max([x for x in reversed(range(1, size)) if self.data[x] > self.data[x-1]])
            minMiddle   = min(self.data[firstMax:lastMax+1])
            middleRange = [x for x in range(firstMax, lastMax+1) if self.data[x] == minMiddle]
            middle      = (min(middleRange) + max(middleRange)) / 2
        except:
            middle = (endX + startX) / 2

        firstYPart = self.data[startX:middle]
        lastYPart  = self.data[middle:endX+1]

        maxY      = max(self.data)
        maxYFirst = max(firstYPart)
        maxYLast  = max(lastYPart)

        maxFirst = [x for x in range(startX, middle) if self.data[x] == maxYFirst]
        maxLast  = [x for x in range(middle, endX+1) if self.data[x] == maxYLast]

        """ POINT C
        """
        firstMaxXFirst = min(maxFirst)
        delta = 1
        delta = (self.data[firstMaxXFirst] - self.data[firstMaxXFirst-1])/self.data[firstMaxXFirst]
        new_max = firstMaxXFirst
        while(delta<0.01 and delta != 0):
            new_max-=1
            delta = (self.data[firstMaxXFirst] - self.data[new_max])/self.data[firstMaxXFirst]
        firstMaxXFirst = new_max+1
        #firstMaxXFirst = min(maxFirst)
        
        """ POINT D
        """
        lastMaxXFirst  = max(maxFirst)
        delta = 1
        if lastMaxXFirst == size-1:
            pass
        else:
            delta = (self.data[lastMaxXFirst] - self.data[lastMaxXFirst+1])/self.data[lastMaxXFirst]
            new_max = lastMaxXFirst
            while(delta<0.01 and new_max<=size-1 and delta != 0):
                delta = (self.data[lastMaxXFirst] - self.data[new_max])/self.data[lastMaxXFirst]
                new_max+=1
            lastMaxXFirst = new_max-1
        #lastMaxXFirst  = max(maxFirst)
        
        """ POINT I
        """
        
        firstMaxXLast  = min(maxLast)
        delta = 1
        delta = (self.data[firstMaxXLast] - self.data[firstMaxXLast-1])/self.data[firstMaxXLast]
        new_max = firstMaxXLast
        while(delta<0.01 and delta != 0 and (new_max<=size-1 or new_max==1)):
            new_max-=1
            delta = (self.data[firstMaxXLast] - self.data[new_max])/self.data[firstMaxXLast]
        firstMaxXLast = new_max+1
        
        
        
        """ POINT J
        """
        lastMaxXLast   = max(maxLast)
        delta = 1
        if lastMaxXLast == size-1:
            pass
        else:
            delta = (self.data[lastMaxXLast] - self.data[lastMaxXLast+1])/self.data[lastMaxXLast]
            new_max = lastMaxXLast
            while(delta<0.01 and new_max<=size-1 and delta != 0):
                
                delta = (self.data[lastMaxXLast] - self.data[new_max])/self.data[lastMaxXLast]
                new_max+=1
            lastMaxXLast = new_max-1




        try:
            startValley = max([x for x in reversed(range(1, middle+1)) if self.data[x] != self.data[x-1]])
            endValley   = min([x for x in range(middle, size-1) if self.data[x] != self.data[x+1]])
        except:
            startValley = middle
            endValley   = middle+1

        beforeFirstMaxXFirst = max(startX, firstMaxXFirst-1)
        afterLastMaxXLast    = min(endX, lastMaxXLast+1)

        self.points.setPoint(0, startX, self.data[startX])
        self.points.setPoint(1, beforeFirstMaxXFirst, self.data[beforeFirstMaxXFirst])
        #self.points.setPoint(2, firstMaxXFirst, maxYFirst)
        self.points.setPoint(2, firstMaxXFirst, self.data[firstMaxXFirst])
        #self.points.setPoint(3, lastMaxXFirst, maxYFirst)
        self.points.setPoint(3, lastMaxXFirst, self.data[lastMaxXFirst])
        self.points.setPoint(4, min(size-1,lastMaxXFirst+1), self.data[min(size-1,lastMaxXFirst+1)])
        self.points.setPoint(5, startValley, self.data[startValley])
        self.points.setPoint(6, endValley, self.data[endValley])
        self.points.setPoint(7, firstMaxXLast-1, self.data[firstMaxXLast-1])
        self.points.setPoint(8, firstMaxXLast, self.data[firstMaxXLast])
        self.points.setPoint(9, lastMaxXLast, self.data[lastMaxXLast])
        self.points.setPoint(10, afterLastMaxXLast, self.data[afterLastMaxXLast])
        self.points.setPoint(11, endX, self.data[endX])
        
        """
        print "A : " + str(startX) + " -> " + str(self.data[startX])
        print "B : " + str(beforeFirstMaxXFirst) + " -> " + str(self.data[beforeFirstMaxXFirst])
        print "C : " + str(firstMaxXFirst) + " -> " + str(self.data[firstMaxXFirst])
        print "D : " + str(lastMaxXFirst) + " -> " + str(self.data[lastMaxXFirst])
        print "E : " + str(min(size-1,lastMaxXFirst+1)) + " -> " + str(self.data[min(size-1,lastMaxXFirst+1)])
        print "F : " + str(startValley) + " -> " + str(self.data[startValley])
        print "G : " + str(endValley) + " -> " + str(self.data[endValley])
        print "H : " + str(firstMaxXLast-1) + " -> " + str(self.data[firstMaxXLast-1])
        print "I : " + str(firstMaxXLast) + " -> " + str(self.data[firstMaxXLast])
        print "J : " + str(lastMaxXLast) + " -> " + str(self.data[lastMaxXLast])
        print "K : " + str(afterLastMaxXLast) + " -> " + str(self.data[afterLastMaxXLast])
        print "K : " + str(endX) + " -> " + str(self.data[endX])
        """

    def check(self):
        if self.points.getX(3) - self.points.getX(2) < self.minPlateauWidth:
            return "First plateau is too small: %d vs %d" % (self.points.getX(3) - self.points.getX(2), self.minPlateauWidth)
        if self.points.getX(3) - self.points.getX(2) > self.maxPlateauWidth:
            return "First plateau is too big: %d vs %d" % (self.points.getX(3) - self.points.getX(2), self.maxPlateauWidth)
        if self.points.getX(9) - self.points.getX(8) < self.minPlateauWidth:
            return "Second plateau is too small: %d vs %d" % (self.points.getX(9) - self.points.getX(8), self.minPlateauWidth)
        if self.points.getX(9) - self.points.getX(8) > self.maxPlateauWidth:
            return "Second plateau is too big: %d vs %d" % (self.points.getX(9) - self.points.getX(8), self.maxPlateauWidth)
        if self.points.getX(6) - self.points.getX(5) < self.minLoop:
            return "Valley is too small: %d vs %d" % (self.points.getX(5) - self.points.getX(6), self.minLoop)
        if max(self.points.getY(2), self.points.getY(3), self.points.getY(8), self.points.getY(9)) < self.minPlateauHeight:
            return "Plateaus are too low: %d vs %d" % (max(self.points.getY(2), self.points.getY(3), self.points.getY(8), self.points.getY(9)), self.minPlateauHeight)
        if max(self.points.getY(2), self.points.getY(3), self.points.getY(8), self.points.getY(9)) < self.minValleyRatio * min(self.points.getY(5), self.points.getX(6)):
            return "Ratio plateau/valley is too low: %d vs %d" % (max(self.points.getY(2), self.points.getY(3), self.points.getY(8), self.points.getY(9)) / min(self.points.getY(5), self.points.getX(6)), self.minValleyRatio)
        return False


class MiRNAProfile6Points(MiRNAProfile):

    def __init__(self, data, verbosity = 0):
        super(MiRNAProfile6Points, self).__init__(6, data, verbosity)

    def placePoints(self):
        size   = len(self.data)
        
        """ POINT A
        """
        startX = 0      if self.data[0] != 0      else min([x for x in range(size) if self.data[x] != 0])-1
        
        """ POINT F
        """
        endX   = size-1 if self.data[size-1] != 0 else max([x for x in range(size) if self.data[x] != 0])+1

        maxY = max(self.data)
        maxX = [x for x in range(startX, endX+1) if self.data[x] == maxY]
        
        """ POINT C
        """
        maxFirst = min(maxX)
        delta = 1
        delta = (self.data[maxFirst] - self.data[maxFirst-1])/self.data[maxFirst]
        new_max = maxFirst
        while(delta<0.01 and delta != 0 and (new_max<=size-1 or new_max==1)):
            new_max-=1
            delta = (self.data[maxFirst] - self.data[new_max])/self.data[maxFirst]
        maxFirst = new_max+1
        
        """ POINT B
        """
        beforeMaxXFirst = max(0, maxFirst-4)
        
        """ POINT D
        """
        """
        if maxLast == size:
            pass
        else:
            delta = (self.data[lastMaxXFirst] - self.data[lastMaxXFirst+1])/self.data[lastMaxXFirst]
            new_max = lastMaxXFirst
            while(delta<0.01 and new_max<=size-1):
                delta = (self.data[lastMaxXFirst] - self.data[new_max])/self.data[lastMaxXFirst]
                new_max+=1
            lastMaxXFirst = new_max-1
        """
        
        maxLast  = max(maxX)
        
        if maxLast == size-1:
            pass
        else:
            delta = 1
            delta = (self.data[maxLast] - self.data[maxLast+1])/self.data[maxLast]
            new_max = maxLast
            while(delta<0.01 and delta != 0 and (new_max<=size-1 or new_max==1)):
                new_max+=1
                try:
                    delta = (self.data[maxLast] - self.data[new_max])/self.data[maxLast]
                except IndexError:
                    delta = -1
            maxLast = new_max-1
        
        """ POINT E
        """
        afterMaxXLast   = min(size-1, maxLast+4)
        
        self.points.setPoint(0, startX, self.data[startX])
        self.points.setPoint(1, beforeMaxXFirst, self.data[beforeMaxXFirst])
        self.points.setPoint(2, maxFirst, self.data[maxFirst])
        self.points.setPoint(3, maxLast, self.data[maxLast])
        self.points.setPoint(4, afterMaxXLast, self.data[afterMaxXLast])
        self.points.setPoint(5, endX, self.data[endX])

        """
        print "A : " + str(startX) + " -> " + str(self.data[startX])
        print "B : " + str(beforeMaxXFirst) + " -> " + str(self.data[beforeMaxXFirst])
        print "C : " + str(maxFirst) + " -> " + str(maxY)
        print "D : " + str(maxLast) + " -> " + str(self.data[maxLast])
        print "E : " + str(afterMaxXLast) + " -> " + str(self.data[afterMaxXLast])
        print "F : " + str(endX) + " -> " + str(self.data[endX])
        """
        
    def check(self):
        
        decrease=True
        for i in range(self.points.getX(4), self.points.getX(5)):
            if not (self.data[i+1]-self.data[i]<=0):
                decrease=False
            if not decrease:
                return "Expression remonte entre l'avant-dernier et le dernier point"
        
        if self.points.getX(3) - self.points.getX(2) < self.minPlateauWidth:
            return "Plateau is too small: %d vs %d" % (self.points.getX(3) - self.points.getX(2), self.minPlateauWidth)
        if self.points.getX(3) - self.points.getX(2) > self.maxPlateauWidth:
            return "Plateau is too big: %d vs %d" % (self.points.getX(3) - self.points.getX(2), self.maxPlateauWidth)
        if max(self.points.getY(2), self.points.getY(3)) < self.minPlateauHeight:
            return "Plateau is too low: %d vs %d" % (max(self.points.getY(2), self.points.getY(3)), self.minPlateauHeight)
        return False


class Profiler(object):

    def __init__(self, verbosity = 0):
        self.verbosity = verbosity

    def setInputFileName(self, fileName):
        self.inputHandle = open(fileName)

    def setOutputFileName(self, fileName):
        self.outputFileName = fileName

    def setTreshold(self, threshold):
        self.threshold = threshold

    def setPlateauShape(self, minPlateauWidth, maxPlateauWidth, minPlateauHeight):
        self.minPlateauWidth  = minPlateauWidth
        self.maxPlateauWidth  = maxPlateauWidth
        self.minPlateauHeight = minPlateauHeight

    def setLoopSize(self, minLoop):
        self.minLoop = minLoop

    def setMinValleyRatio(self, minValleyRatio):
        self.minValleyRatio = minValleyRatio

    def _profile(self, profiler, cpt, data, nbPoints):
        p = profiler(data, self.verbosity)
        p.setPlateauShape(self.minPlateauWidth, self.maxPlateauWidth, self.minPlateauHeight)
        p.setLoopSize(self.minLoop)
        p.setMinValleyRatio(self.minValleyRatio)
        p.placePoints()
        p.findBestMove()
#p.findBestPlace()
        #p.plot("%s_%d_%d.png" % (self.outputFileName, cpt+1, nbPoints))
        score = p.getScore()
        check = p.check()
        #print check
        if check:
            score = THRESHOLD
        return (score, check)

    def run(self, seq):

        cpt=0
        data = map(float, seq)
        #print "PROFILING 12 POINTS ---------------------"
        s12, c12 = self._profile(MiRNAProfile12Points, cpt, data, 12)
        #print "\n\n\n"
        #print "PROFILING 6 POINTS ---------------------"
        s6,  c6  = self._profile(MiRNAProfile6Points, cpt, data, 6)
        #print "\n\n\n"
        if min(s6, s12) > self.threshold:
            
            #print "Found nothing for line %d " % (cpt+1),
            #if min(s6, s12) == THRESHOLD:
                #if c6:
                    #print "(%s)" % (c6)
                #elif c12:
                    #print "(%s)" % (c12)
            
            #else:
            #    print "(score are %.2f and %.2f vs %.2f)" % (s6, s12, self.threshold)
            return (False, False, s6, c6, s12, c12)
        elif s6 <= 1.2 * s12:
            #print "Line %d: miRNA alone (score: %.2f vs %.2f)" % (cpt+1, s6, s12)
            return (True, False, s6, c6, s12, c12)
        else:
            #print "Line %d: miRNA+star (score: %.2f vs %.2f)" % (cpt+1, s12, s6)
            return (True, True, s6, c6, s12, c12)