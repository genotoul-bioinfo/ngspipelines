#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    

class SsearchMapping(object):
    
    def __init__(self, query_start, query_stop, library_start, library_stop, score, query_sequence, library_sequence, zone):
        self.query_start = query_start
        self.query_stop = query_stop
        self.library_start = library_start
        self.library_stop = library_stop
        self.score = score
        self.query_sequence = query_sequence
        self.library_sequence = library_sequence
        self.zone = zone
        self.personal_score = 0
        self.query_gaps = 0
        self.library_gaps = 0
        self.appariements = 0
        self.misappariements = 0
        self.shift_query = 0
        self.shift_library = 0
        self.structure = ""
        self.query_structure = ""
        self.library_structure = ""
        self.loop = 0
        self.cpt_bulges = 0
        self.max_bulge = 0
        self.length_query = 0
        self.length_maj = 0
    
    def __str__(self):
        """
        @summary: ToString of SsearchMapping
        @see: mirbase_analysis.py
        """
        tab = []
        tab.append(str(self.query_start)+"-"+str(self.query_stop))
        tab.append(str(self.library_start)+"-"+str(self.library_stop))
        tab.append(str(self.score))
        tab.append(str(self.personal_score))
        tab.append(str(self.appariements))
        tab.append(str(self.misappariements))
        tab.append(str(self.cpt_bulges))
        tab.append(str(self.max_bulge))
        tab.append(str(self.loop))
        tab.append(self.structure)
        tab.append("["+str(self.shift_query)+"-"+str(self.shift_library)+"]")
        return "\t".join(tab)+"\t"+self.query_sequence+"\t"+self.library_sequence
    
    def compute_loop_mirbase(self, zone, mirbase_start_mature_on_hairpin):
        """
        @summary: 
        @see: mirbase_analysis.py
            @param param: [str] zone used for computing SsearchMapping
            @param mirbase_start_mature_on_hairpin: [int]
        """
        loop = 0
        if zone == "BEFORE":
            loop = self.library_start - mirbase_start_mature_on_hairpin
            cpt=len(self.query_structure)-1
            while(self.query_structure[cpt] == "."):
                self.shift_query += 1
                cpt+=1
            cpt=0
            while(self.library_structure[cpt] == "."):
                self.shift_library += 1
                cpt+=1
        
        else:            
            loop = mirbase_start_mature_on_hairpin - self.library_stop
            cpt=0
            while(self.query_structure[cpt] == "."):
                self.shift_query += 1
                cpt+=1
            cpt=len(self.library_structure)-1
            while(self.library_structure[cpt] == "."):
                self.shift_library += 1
                cpt+=1
        
        self.loop = loop + self.shift_library + self.shift_query
    
    def compute_structure_mirbase(self, zone):
        """
        @summary: Computes structure
        @see: mirbase_analysis.py
            @param zone: [str] zone used for computing SsearchMapping
        """
        if zone == "BEFORE":
            self.structure += self.query_structure
            for i in range(0,self.loop):
                self.structure+="."
            self.structure += self.library_structure
        else:
            self.structure += self.query_structure
            for i in range(0,self.loop):
                self.structure+="."
            self.structure += self.library_structure
    
    def compute_bulges(self, struc):
        """
        @summary: Computes bulges count and max length of bulges
        @see: mirbase_analysis.py, mirna_prediction.py
            @param struc: [str] the structure
        @return: [tuple] max_bulge and cpt_bulges
        """
        bulge = 0
        max_bulge = 0
        cpt_bulge = 0
        new=True
        for i in struc:
            if i == '.':
                bulge+=1
                max_bulge = max(max_bulge,bulge)
                if new:
                    cpt_bulge+=1
                new=False
            else:
                new=True
                bulge=0
        
        return (max_bulge, cpt_bulge)
    
    def is_enough_good(self, min_appariements):
        """
        @summary: Checks if the ssearchMapping has appariements enough
        @see: mirna_prediction.py
            @param min_appariements: [int] appariements
        @return: [boolean]
        """
        if int(self.appariements) < int(min_appariements):
            return False
        else:
            return True
    
    def compute_global_score(self, stats, mature_and_star):
        """
        @summary: Computes the personal score of the SsearchMapping
        @see: mirna_prediction.py
        @see: utils.ssearch36()
            @param stats: [file or None] the stats file or None 
        @return: [float] the score
        @requires: utils.evaluate_score_from_mirbase()
        """
        import workflows.srnaseq.lib.utils as utils
        
        score=0
        if str(stats) == "None":
            score += int(self.appariements)
            score += int(self.score)*100
            score -= int(float(self.query_gaps)*10)
            score -= int(float(self.library_gaps)*10)
            if (self.misappariements > 7):
                score -= 100
            if (self.library_gaps > 8 or self.query_gaps > 8):
                score -= 100
        else:
            stat_file = open(stats,"r")
            for line in stat_file:
                if 'mature_length' in line:
                    array_values = []
                    for i in range(1,10):
                        array_values.append(int(line.rstrip().split("\t")[i]))
                    score+=utils.evaluate_score_from_mirbase(self.length_query,array_values, 20)
                if 'loop_size' in line:
                    array_values = []
                    for i in range(1,10):
                        array_values.append(int(line.rstrip().split("\t")[i]))
                    score+=utils.evaluate_score_from_mirbase(self.loop,array_values, 10)
                if 'cpt_bulges' in line:
                    array_values = []
                    for i in range(1,10):
                        array_values.append(int(line.rstrip().split("\t")[i]))
                    score+=utils.evaluate_score_from_mirbase(self.cpt_bulges,array_values, 5)
                if 'max_bulge' in line:
                    array_values = []
                    for i in range(1,10):
                        array_values.append(int(line.rstrip().split("\t")[i]))
                    score+=utils.evaluate_score_from_mirbase(self.max_bulge,array_values, 5)
                if 'appariements' in line:
                    array_values = []
                    for i in range(1,10):
                        array_values.append(int(line.rstrip().split("\t")[i]))
                    score+=utils.evaluate_score_from_mirbase(self.appariements,array_values, 10)
            score += 1000 if mature_and_star else 0
            stat_file.close()
        
        return score
    
    def compute_structure_without_loop(self):
        """
        @summary: Computes structure
        @see: utils.ssearch36()
        @requires: utils.dic_appariements()
        """
        import workflows.srnaseq.lib.utils as utils
        dic = utils.dic_appariements()
        structure = ""
        appariements = 0
        for i in range(0,self.query_start-1):
            self.query_structure+"."
        
        for i in range(self.query_start-1,len(self.query_sequence)):
            if self.query_sequence[i] == "-":
                self.query_gaps+=1
            else:
                if self.query_sequence[i] in dic:
                    if self.library_sequence[i] in dic[self.query_sequence[i]]:
                        self.appariements+=1
                        self.query_structure+="("
                    else:
                        self.query_structure+="."
                        self.misappariements += 1
                else:
                    self.query_structure+="."
                    self.misappariements += 1

        temp_seq = self.library_sequence[::-1]
        temp_query_seq = self.query_sequence[::-1]
        for i in range(0,len(temp_seq)):
            if temp_seq[i] == "-":
                self.library_gaps+=1
            else:
                if temp_seq[i] in dic:
                    if temp_query_seq[i] in dic[temp_seq[i]]:
                        self.library_structure+=")"
                    else:
                        self.library_structure+="."
                else:
                    self.library_structure+="."
