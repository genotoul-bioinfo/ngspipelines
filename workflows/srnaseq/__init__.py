#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import ngspipelines
from ngspipelines.application import Application
from lib.srnaseq import Locus, DNA, ReadLibraryCount, Prediction, Annotation, Structure, Isoform, StructuralAnnotation
from ngspipelines.analysis import Analysis
from ngspipelines.library import Library

import os, sys
import StringIO
from ConfigParser import ConfigParser

from lib.utils import *
import numpy as np
from decimal import *
from ngspipelines.utils import *
from jflow.featureio import GFF3IO
from collections import OrderedDict

class SRNAseq (Application):
    
    dic_type_annot = {}
    dic_type_annot["mirbase"]="miRNA"
    dic_type_annot["mirbase-hairpin"]="miRNA"
    dic_type_annot["mirbase-mature"]="miRNA"
    dic_type_annot["rfam-mirna"]="miRNA"
    dic_type_annot["rfam-other"]="other"
    dic_type_annot["rfam-rrna"]="rRNA"
    dic_type_annot["silva-lsu"]="rRNA"
    dic_type_annot["silva-ssu"]="rRNA"
    dic_type_annot["rfam-trna"]="tRNA"
    dic_type_annot["eukaryotic-trnas"]="tRNA"
    dic_type_annot["pirna"]="piRNA"
    dic_type_annot["rfam-rnasemrp"]="RNAseMRP"
    dic_type_annot["rfam-rnasesrp"]="RNAseSRP"
    dic_type_annot["rfam-rnasep"]="RNAseP"
    dic_type_annot["rfam-snorna"]="snoRNA"
    dic_type_annot["rfam-snrna"]="snRNA"
    dic_type_annot["rfam-telomerase"]="telomerase"
    dic_type_annot["transposable-elements"]="transposable-elements"
    dic_type_annot["other"]="other"
    
    def get_description(self):
        return "Cleaning and/or Annotation and ncRNA Prediction of small-RNAseq data or load results"
    
    def define_parameters(self, function="process"):
        
        # PROCESS
        if function == "process":
            # CLEANING SECTION
            self.add_parameter("five_prime_adapter", "Library adapter", add_to="library")
            self.add_parameter("three_prime_adapter", "Library adapter", add_to="library")
            self.add_parameter("run_cleaning", "Should the cleaning of reads be processed ? [False]", default=False, type="bool", group="CLEANING section")
            self.add_parameter("min_length", "Discard trimmed reads shorter than MIN_LENGTH [16]", default=16, group="CLEANING section")
            self.add_parameter("max_length", "Discard trimmed reads longer than MAX_LENGTH [28]", default=28, group="CLEANING section")
            self.add_parameter("min_expression_level", "Discard reads whose expression level overall libraries is less than MIN_COPY_COUNT reads [1]", default=10, group="CLEANING section")
            self.add_parameter("min_samples_count", "Discard reads not expressed in more than MIN_SAMPLES_COUNT libraries [1]", default=1, group="CLEANING section")
            self.add_input_file("cleaned_sequences", "FASTA file containing cleaned sequences", group="CLEANING section")
            self.add_input_file("count_matrix", "Expression level for each sequence and each sample", group="CLEANING section")
            
            # ANNOTATION SECTION
            self.add_parameter("run_sequences_annotation", "Should the sequences annotation be processed ? [False]", default=False, type="bool", group="ncRNA ANNOTATION section")
            self.add_parameter("annotate_unpredicted_regions", "Should the unpredicted regions be annotated ? [False]", default=False, type="bool", group="ncRNA ANNOTATION section")
            self.add_multiple_parameter_list("databank", "Define the databanks", group="ncRNA ANNOTATION section")
            self.add_input_file("file", "Path to the databank file in FASTA format", required=True, add_to="databank")
            self.add_parameter("id", "Databank id", required=True, choices=["mirbase","eukaryotic-trnas","rfam-other","rfam-mirna","rfam-other", "rfam-rrna","rfam-trna","rfam","silva-ssu","silva-lsu", "pirna","mirbase-mature","mirbase-hairpin","pmrd-mature","pmrd-hairpin","plantnatsdb","rfam-rnasemrp","rfam-rnasep","rfam-snorna","rfam-snrna","rfam-rnasesrp","rfam-telomerase", "transposable-elements"], add_to="databank")
            self.add_parameter("version", "Databank version", required=True, add_to="databank")
            self.add_parameter("blast_align_length", "Blast alignment length [17]", default=17, add_to="databank")
            self.add_parameter("blast_evalue", "Min blast e-value [1]", default=1, add_to="databank")
            self.add_parameter("blast_percent_ident", "Min blast percentage identity [90]", default=90, add_to="databank")
            self.add_parameter("blast_mismatch", "Max mismatch number in blast alignment [1]", default=1, add_to="databank")
            self.add_parameter("blast_gap", "Max number of gaps in blast alignment [0]", default=0, add_to="databank")
            self.add_parameter("blast_bit_score", "Min blast bit score [20]", default=20, add_to="databank")
            self.add_parameter("bowtie2_seed_size", "Seed size for bowtie2 [20]", default=20, type="int", add_to="databank")
            self.add_parameter("bowtie2_seed_mismatch", "Seed mismatch allowed for bowtie2 [0]", default=0, type="int", add_to="databank")
            self.add_parameter("bowtie2_aln_reports", "Reports allowed for bowtie2 [0]", default=1, type="int", add_to="databank")
            self.add_parameter("bowtie2_global_mismatch", "Mismatch allowed for bowtie2 (whole sequence) [0]", default=0, type="int", add_to="databank")
            self.add_input_file("annotation_matrix", "Annotation for each sequence and each databank", group="ncRNA ANNOTATION section")
            
            # GENOME SECTION
            self.add_input_file("genome_annotation", "Genome annotation file (GFF3)", required=True, group="GENOME section")
            self.add_parameter("genome_id", "Genome id", required=True, group="GENOME section")
            self.add_parameter("genome_version", "Genome version", required=True, group="GENOME section")
            self.add_parameter("seed_size", "Seed size for bowtie2 [16]", default=16, type="int", group="GENOME section")
            self.add_parameter("seed_mismatch", "Seed mismatch allowed for bowtie2 [0]", default=0, type="int", group="GENOME section")
            self.add_parameter("aln_reports", "Reports allowed for bowtie2 [10]", default=10, type="int", group="GENOME section")
            self.add_parameter("global_mismatch", "Mismatch allowed for bowtie2 (whole sequence) [0]", default=0, type="int", group="GENOME section")
            self.add_parameter("clade", "Clade of the species", default="Metazoa", choices=["Metazoa","Viridiplantae","Viruses"], group="GENOME section")
            
            # LOCUS SECTION
            self.add_parameter("min_nb_isoforms", "Min number of isoforms in a locus [1]", default=1, type="int", group="LOCUS section")
            self.add_parameter("offset", "Min distance between two locus [100]", default=100, type="int", group="LOCUS section")
            self.add_parameter("min_size_locus", "Min size of a locus [15]", default=15, type="int", group="LOCUS section")
            self.add_parameter("max_nb_isoforms", "Max number of isoforms in a locus [1000]", default=1000, type="int", group="LOCUS section")
            
            # tRNA PREDICTION SECTION
            self.add_parameter("run_trna_prediction", "Should the tRNA prediction be processed ? [False]", default=False, type="bool", group="tRNA PREDICTION section")
            self.add_parameter("trna_boundary", "Boundary from locus [100]", default=100, group="tRNA PREDICTION section")
            
            # rRNA PREDICTION SECTION
            self.add_parameter("run_rrna_prediction", "Should the rRNA prediction be processed ? [False]", default=False, type="bool", group="rRNA PREDICTION section")
            self.add_parameter("rrna_boundary", "Boundary from locus [2000]", default=2000, group="rRNA PREDICTION section")
            
            # miRNA PREDICTION SECTION
            self.add_parameter("run_mirna_prediction", "Should the miRNA prediction be processed ? [False]", default=False, type="bool", group="miRNA PREDICTION section")
            self.add_input_file("mirbase_stats_file", "miRBase stats file", group="miRNA PREDICTION section")
            self.add_parameter("min_appariements", "Min count of appariements needed for a miRNA appariement. Computed from miRbase if -1 [-1]", default=-1, group="miRNA PREDICTION section")
            self.add_parameter("min_expr_level", "Min expression level of the majoritary sequence of a locus to keep it [100]", default=100, group="miRNA PREDICTION section")
            self.add_parameter("min_loop_size", "Min loop size for a miRNA appariement. Computed from miRbase if -1 [-1]", default=-1, group="miRNA PREDICTION section")
            self.add_parameter("max_loop_size", "Max loop size for a miRNA appariement. Computed from miRbase if -1 [-1]", default=-1, group="miRNA PREDICTION section")
            self.add_parameter("min_majseq_size", "Min size of the majoritary sequence for a miRNA appariement. Computed from miRbase if -1 [-1]", default=-1, group="miRNA PREDICTION section")
            self.add_parameter("max_majseq_size", "Max size of the majoritary sequence for a miRNA appariement. Computed from miRbase if -1 [-1]", default=-1, group="miRNA PREDICTION section")
            self.add_parameter("max_cpt_bulges", "Max count of bulges for a miRNA appariement. Computed from miRbase if -1 [-1]", default=-1, group="miRNA PREDICTION section")
            self.add_parameter("max_bulge_size", "Max size of the longest bulge for a miRNA appariement. Computed from miRbase if -1 [-1]", default=-1, group="miRNA PREDICTION section")
        
        # LOAD
        else:
            # Required files built by the process
            self.add_input_file("annotation_matrix", "File of reads annotation (line) per database (column)", required=True, group="ANNOTATION section")
            self.add_input_file("count_matrix", "File of reads counts (line) per library (column)", required=True, group="EXPRESSION section")
            
            # Prediction parameters
            self.add_multiple_parameter_list("prediction", "Define the prediction process", required=True, group="PREDICTION section")
            self.add_input_file("file", "Path to the prediction file in GFF3 format", required=True, add_to="prediction")
            self.add_parameter("family", "ncRNA family involved in this file", required=True, choices=["miRNA", "tRNA", "rRNA", "loci", "other"], add_to="prediction")
            self.add_parameter("software_name", "Which software was used to perform the prediction", required=True, add_to="prediction")
            self.add_parameter("software_parameters", "Which parameters were used when performing the prediction", required=True, add_to="prediction")
            self.add_parameter("software_version", "Which version of the prediction software was used", required=True, add_to="prediction")
            self.add_parameter("comments", "Add some comments on this analysis", required=True, add_to="prediction")
            
            # Functional annotation parameters
            self.add_multiple_parameter_list("annotation", "Define the annotation process", group="ANNOTATION section")
            self.add_input_file("file", "Path to the annotation file in GFF3 format", add_to="annotation")
            self.add_parameter("software_name", "Which software was used to perform the annotation", add_to="annotation")
            self.add_parameter("software_parameters", "Which parameters were used when performing the annotation", add_to="annotation")
            self.add_parameter("software_version", "Which version of the annotation software was used", add_to="annotation")
            self.add_parameter("comments", "Add some comments on this analysis", add_to="annotation")
            self.add_parameter("database", "Which database was used when performing the annotation", add_to="annotation")
            self.add_parameter("database_type", "Which type of the information is contained in the database", add_to="annotation")
            self.add_parameter("database_version", "Which version of database was used when performing the annotation", add_to="annotation")
            self.add_parameter("annotation_threshold", "Score found for the hit annotated", add_to="annotation")
            
            # Structural Annotation parameters
            self.add_multiple_parameter_list("structural_annotation", "Define the structural annotation process", group="ANNOTATION section")
            self.add_input_file("file", "Path to the structural annotation file in GFF3 format", add_to="structural_annotation")
            self.add_parameter("software_name", "Which software was used to perform the structural annotation", add_to="structural_annotation")
            self.add_parameter("software_parameters", "Which parameters were used when performing the structural annotation", add_to="structural_annotation")
            self.add_parameter("software_version", "Which version of the structural annotation software was used", add_to="structural_annotation")
            self.add_parameter("comments", "Add some comments on this analysis", add_to="structural_annotation")
            
            # Alignment parameters
            self.add_multiple_parameter("alignment", "Define the alignment process", required=True, group="ALIGNMENT section")
            self.add_input_file("file", "Path to the alignment file in BAM format", required=True, add_to="alignment")
            self.add_parameter("software_name", "Which software was used to perform the alignment", required=True, add_to="alignment")
            self.add_parameter("software_parameters", "Which parameters were used when performing the alignment", required=True, add_to="alignment")
            self.add_parameter("software_version", "Which version of the alignment software was used", required=True, add_to="alignment")
            self.add_parameter("comments", "Add some comments on this analysis", required=True, add_to="alignment")
            
            # Structure parameters
            self.add_multiple_parameter_list("structure", "Define the strucutre computing process", required=True, group="STRUCTURE section")
            self.add_input_file("file", "Path to the structure file", required=True, add_to="structure")
            self.add_parameter("software_name", "Which software was used to perform the structure computing", required=True, add_to="structure")
            self.add_parameter("software_parameters", "Which parameters were used when performing the structure computing", required=True, add_to="structure")
            self.add_parameter("software_version", "Which version of the structure computing software was used", required=True, add_to="structure")
            self.add_parameter("comments", "Add some comments on this analysis", required=True, add_to="structure")
        
    def process(self):
        
        THREADS = 1
        
        ### CLEANING ####################################################################
        if self.run_cleaning:
            sample_cleaning = self.add_component("CleanFastq", [self.library["files"], self.min_length, self.max_length, self.library["five_prime_adapter"], self.library["three_prime_adapter"]])
            global_cleaning = self.add_component("FilterAndCollapseFastqs", [sample_cleaning.cleaned_files, self.min_expression_level, self.min_samples_count])
            cleaned_fasta = global_cleaning.cleaned_fasta
            count_matrix = global_cleaning.matrix_count_file
        cleaned_fasta = cleaned_fasta if self.run_cleaning else self.cleaned_sequences
        count_matrix = count_matrix if self.run_cleaning else self.count_matrix
        
        ### SEQUENCES ANNOTATION ########################################################
        if self.run_sequences_annotation:
            bowtie2_build = self.add_component("Bowtie2Build",[self.databank["file"]])
            bowtie2 = self.add_component("Bowtie2",[bowtie2_build.indexed_files, cleaned_fasta, self.databank["id"], self.databank["bowtie2_seed_size"], self.databank["bowtie2_seed_mismatch"], [x+1 for x in self.databank["bowtie2_aln_reports"]]], component_prefix=str("reads_annotations"))
            filter_bam = self.add_component("FilterBAM", [bowtie2.bam_files, self.databank["bowtie2_global_mismatch"], self.databank["bowtie2_aln_reports"], [False for x in self.databank["bowtie2_aln_reports"]], self.databank["version"], bowtie2.bowtie2_logs], component_prefix=str("reads_annotations"))
            annotation = self.add_component("SequencesAnnotation",[filter_bam.filtered_bam_files, self.databank["version"]])
        annotation_matrix = annotation.annotations_matrix if self.run_sequences_annotation else self.annotation_matrix
        
        ### PREDICTION AND ANNOTATION ###################################################
        all_predictions = []
        all_annotations = []
        all_structural_annotations = []
        all_structures = []
        all_fastas = []
        predictions_for_locusview = []
        
        if self.run_trna_prediction or self.run_mirna_prediction or self.run_rrna_prediction or self.annotate_unpredicted_regions:
            # Index genome and databanks
            genome_bowtie2_build = self.add_component("Bowtie2Build",[self.reference_genome])
            genome_samtools = self.add_component("SamtoolsFaidx", [self.reference_genome])
            formatdb = self.add_component("BlastallIndex",[self.databank["file"]])
            
            ### MAPPING AND LOCI CREATION ###############################################
            genome_bowtie2 = self.add_component("Bowtie2",[genome_bowtie2_build.indexed_files, cleaned_fasta, self.genome_id, self.seed_size, self.seed_mismatch, self.aln_reports+1], component_prefix=str("genome"))
            genome_filter = self.add_component("FilterBAM", [genome_bowtie2.bam_files, self.global_mismatch, self.aln_reports, False, [self.genome_version], genome_bowtie2.bowtie2_logs], component_prefix=str("genome"))
            locus = self.add_component("BAMToLocus", [genome_filter.filtered_bam_files[0], self.min_nb_isoforms, self.max_nb_isoforms, self.min_size_locus, self.offset, THREADS])
            
            ### PREDICTION AND ANNOTATION ###############################################
            # tRNA
            if self.run_trna_prediction:
                tRNA_prediction = self.add_component("TRNAPrediction", [self.genome_id, locus.output_files, locus.gff3, genome_samtools.indexed_files[0], self.trna_boundary])
                tRNA_annotations = self.add_component("FunctionalAnnotation",[tRNA_prediction.fastas, tRNA_prediction.predictions, self.databank["blast_evalue"], self.databank["blast_percent_ident"], self.databank["blast_align_length"], self.databank["blast_mismatch"], self.databank["blast_gap"], self.databank["blast_bit_score"], formatdb.indexed_files, self.databank["id"]], component_prefix="tRNA")
                tRNA_struc_annotations = self.add_component("StructuralAnnotation",[tRNA_prediction.predictions, self.genome_annotation], component_prefix=str("tRNA"))
                tRNA_merge = self.add_component("Merge",[tRNA_prediction.predictions, tRNA_annotations.functional_annotations, tRNA_struc_annotations.structural_annotations, tRNA_prediction.fastas, tRNA_prediction.strucs], component_prefix=str("tRNA"))
                all_predictions.append(tRNA_merge.prediction)
                all_annotations.append(tRNA_merge.functional_annotation)
                all_fastas.append(tRNA_merge.fasta)
                all_structural_annotations.append(tRNA_merge.structural_annotation)
                all_structures.append(tRNA_merge.secondary_structure)
                predictions_for_locusview.append(tRNA_merge.prediction)
            # rRNA
            if self.run_rrna_prediction:
                rRNA_prediction = self.add_component("RRNAPrediction", [self.genome_id, locus.output_files, locus.gff3, genome_samtools.indexed_files[0], self.trna_boundary, genome_samtools.indexed_files[0]])
                rRNA_annotations = self.add_component("FunctionalAnnotation",[rRNA_prediction.fastas, rRNA_prediction.predictions, self.databank["blast_evalue"], self.databank["blast_percent_ident"], self.databank["blast_align_length"], self.databank["blast_mismatch"], self.databank["blast_gap"], self.databank["blast_bit_score"], formatdb.indexed_files, self.databank["id"]], component_prefix="rRNA")
                rRNA_struc_annotations = self.add_component("StructuralAnnotation",[rRNA_prediction.predictions, self.genome_annotation], component_prefix=str("rRNA"))
                rRNA_merge = self.add_component("Merge",[rRNA_prediction.predictions, rRNA_annotations.functional_annotations, rRNA_struc_annotations.structural_annotations, rRNA_prediction.fastas, rRNA_prediction.strucs], component_prefix=str("rRNA"))
                all_predictions.append(rRNA_merge.prediction)
                all_annotations.append(rRNA_merge.functional_annotation)
                all_fastas.append(rRNA_merge.fasta)
                all_structural_annotations.append(rRNA_merge.structural_annotation)
                all_structures.append(rRNA_merge.secondary_structure)
            # miRNA
            if self.run_mirna_prediction:
                hybr_file = self.get_temporary_file(suffix="tsv")
                create_hybridation_matrix(hybr_file)
                miRNA_prediction = self.add_component("MIRNAPrediction", [self.genome_id, locus.output_files, locus.gff3, self.clade, genome_samtools.indexed_files[0], self.mirbase_stats_file, hybr_file, self.min_appariements, self.min_expr_level, self.min_loop_size, self.max_loop_size, self.min_majseq_size, self.max_majseq_size, self.max_cpt_bulges, self.max_bulge_size])
                miRNA_annotations = self.add_component("FunctionalAnnotation",[miRNA_prediction.fastas , miRNA_prediction.predictions, self.databank["blast_evalue"], self.databank["blast_percent_ident"], self.databank["blast_align_length"], self.databank["blast_mismatch"], self.databank["blast_gap"], self.databank["blast_bit_score"], formatdb.indexed_files, self.databank["id"]], component_prefix="miRNA")
                miRNA_struc_annotations = self.add_component("StructuralAnnotation",[miRNA_prediction.predictions, self.genome_annotation], component_prefix=str("miRNA"))
                miRNA_merge = self.add_component("Merge",[miRNA_prediction.predictions, miRNA_annotations.functional_annotations, miRNA_struc_annotations.structural_annotations, miRNA_prediction.fastas, miRNA_prediction.strucs], component_prefix=str("miRNA"))
                all_predictions.append(miRNA_merge.prediction)
                all_annotations.append(miRNA_merge.functional_annotation)
                all_fastas.append(miRNA_merge.fasta)
                all_structural_annotations.append(miRNA_merge.structural_annotation)
                all_structures.append(miRNA_merge.secondary_structure)
                predictions_for_locusview.append(miRNA_merge.prediction)
            # orphan prediction loci
            if self.annotate_unpredicted_regions:
                unpredicted_loci = self.add_component("ExtractUnpredictedLoci",[self.genome_id, all_predictions, locus.gff3, genome_filter.filtered_bam_files[0], genome_samtools.indexed_files[0], 20, locus.output_files])
                loci_annotations = self.add_component("FunctionalAnnotation",[unpredicted_loci.fastas, unpredicted_loci.regions ,self.databank["blast_evalue"], self.databank["blast_percent_ident"], self.databank["blast_align_length"], self.databank["blast_mismatch"], self.databank["blast_gap"], self.databank["blast_bit_score"], formatdb.indexed_files, self.databank["id"]], component_prefix=str("loci"))
                annotated_loci = self.add_component("GetAnnotatedLoci",[unpredicted_loci.regions,loci_annotations.functional_annotations])
                loci_struc_annotations = self.add_component("StructuralAnnotation",[annotated_loci.annotated_regions, self.genome_annotation], component_prefix=str("loci"))
                loci_merge = self.add_component("Merge",[annotated_loci.annotated_regions, loci_annotations.functional_annotations, loci_struc_annotations.structural_annotations, unpredicted_loci.fastas], component_prefix=str("loci"))
                all_annotations.append(loci_merge.functional_annotation)
                all_predictions.append(loci_merge.prediction)
                all_fastas.append(loci_merge.fasta)
                all_structural_annotations.append(loci_merge.structural_annotation)
                
            ### LOAD ####################################################################
            # Get best annotations
            get_best_annot = self.add_component( "BestAnnotSearch", [all_annotations] )
            # Get infos for general statistics
            get_general_infos = self.add_component("GetGeneralStatsInfos",[all_predictions, all_annotations, get_best_annot.best_annotations])
            # BIOMART LOCUS table (MAIN) ##################
            locus = self.add_biomart_load(Locus, [all_predictions, get_best_annot.best_annotations, self.dic_type_annot, get_general_infos.list_infos],analysis_hidden=True)
            # BIOMART ANNOTATION table ####################
            
            if len(all_annotations)==0:
                self.project.add_biomart_table(Annotation)
            else:
                annotation = self.add_biomart_load(Annotation, [all_annotations, get_best_annot.best_annotations, self.dic_type_annot],analysis_hidden=True)
            # BIOMART DNA table ###########################
            get_locus_sequence = self.add_component("GetLocusSequence", [self.reference_genome, all_predictions])
            dna = self.add_biomart_load(DNA, [get_locus_sequence.fastas])
            #dna = self.add_biomart_load(DNA, [all_fastas]) Error with ncRNA not reversed in all_fastas
            # BIOMART blast search ########################
            concatenate_fastas = self.add_component("ConcatenateFiles",[all_fastas, "merged.fa"])
            self.add_blast_search( Locus, concatenate_fastas.output_file)
            # BIOMART ISOFORM table #######################
            get_iso_infos = self.add_component("GetIsoformsInfos", [genome_filter.filtered_bam_files[0], all_predictions, count_matrix, annotation_matrix])
            isoform = self.add_biomart_load(Isoform, [get_iso_infos.dic],analysis_hidden=True)
            # BIOMART PREDICTION table ####################
            get_pred_infos = self.add_component("GetPredictionsInfos", [genome_filter.filtered_bam_files[0],all_predictions, count_matrix])
            prediction = self.add_biomart_load(Prediction, [get_pred_infos.dic],analysis_hidden=True)
            # BIOMART STRUCTURALANNOTATION table ##########
            if len(all_structural_annotations)==0:
                self.project.add_biomart_table(StructuralAnnotation)
            struc_annotation = self.add_biomart_load(StructuralAnnotation, [all_structural_annotations], analysis_hidden=True)
             # BIOMART STRUCTURE table ####################
            get_computed_structures = self.add_component("GetComputedStructures", [all_structures, all_predictions])
            get_rnafold = self.add_component("RNAfold", [concatenate_fastas.output_file])
            # Add one or two secondary structures
            structure = self.add_biomart_load(Structure, [get_rnafold.rnafold_file, get_computed_structures.dic])
            # BIOMART READ LIBRARY COUNT table ############
            get_rlc_infos = self.add_component("GetReadLibraryCountInfos", [genome_filter.filtered_bam_files[0],predictions_for_locusview, count_matrix])
            read_library_count = self.add_biomart_load(ReadLibraryCount, [get_rlc_infos.dic, self.project.libraries])
            
    def load(self):
        
        # Alignment analysis ##############################
        alignment_analysis = self.add_analysis( analysis_name="Mapping",
                                                analysis_type="mapping",
                                                analysis_hidden="0",
                                                soft_name=self.alignment["software_name"],
                                                soft_parameters=self.alignment["software_parameters"],
                                                soft_version=self.alignment["software_version"],
                                                comments=self.alignment["comments"],
                                                files=[self.alignment["file"]])
        # Get best annotations
        get_best_annot = self.add_component( "BestAnnotSearch", [self.annotation["file"]] )
        # Get infos for general statistics
        get_general_infos = self.add_component("GetGeneralStatsInfos",[self.prediction["file"], self.annotation["file"], get_best_annot.best_annotations])
        # Get list of prediction files for which we want to build the LocusView/DepthView
        predictions_for_locusview = []
        families_for_locusview = ["miRNA", "tRNA"]
        for p in self.prediction:
            if p["family"] in families_for_locusview:
                predictions_for_locusview.append(p["file"])
        
        # BIOMART LOCUS table (MAIN) ######################
        for prediction in self.prediction:
            locus = self.add_biomart_load(Locus, 
                                          [prediction["file"], get_best_annot.best_annotations, self.dic_type_annot,get_general_infos.list_infos],
                                          analysis_name=prediction["family"]+" prediction",
                                          analysis_type="prediction",
                                          analysis_hidden="0",
                                          soft_name=prediction["software_name"], 
                                          soft_parameters=prediction["software_parameters"], 
                                          soft_version=prediction["software_version"], 
                                          comments=prediction["comments"],
                                          files=[prediction["file"]])
        # BIOMART ANNOTATION table ########################    
        for annotation in self.annotation:
            annotation = self.add_biomart_load(Annotation, 
                                               [annotation["file"], get_best_annot.best_annotations, self.dic_type_annot],
                                               analysis_name="Annotation against "+annotation["database"], 
                                               analysis_type="functional annotation", 
                                               soft_name=annotation["software_name"], 
                                               soft_parameters=annotation["software_parameters"], 
                                               soft_version=annotation["software_version"], 
                                               comments=annotation["comments"], 
                                               files=[annotation["file"]])
        # BIOMART DNA table ###############################
        get_locus_sequence = self.add_component("GetLocusSequence", [self.reference_genome, self.prediction["file"]])
        dna = self.add_biomart_load(DNA, [get_locus_sequence.fastas])
        # BIOMART blast search ############################
        self.add_blast_search( Locus, get_locus_sequence.fasta)
        # BIOMART ISOFORM table ###########################
        get_iso_infos = self.add_component("GetIsoformsInfos", [self.alignment["file"], self.prediction["file"], self.count_matrix, self.annotation_matrix])
        isoform = self.add_biomart_load(Isoform, [get_iso_infos.dic])
        # BIOMART PREDICTION table ########################
        get_pred_infos = self.add_component("GetPredictionsInfos", [self.alignment["file"],self.prediction["file"], self.count_matrix])
        prediction = self.add_biomart_load(Prediction, [get_pred_infos.dic])
        for structural_annotation in self.structural_annotation:
            struc_annotation = self.add_biomart_load(StructuralAnnotation, 
                                                     [structural_annotation["file"]],
                                                     analysis_name="Structural Annotation", 
                                                     analysis_type="structural annotation", 
                                                     soft_name=structural_annotation["software_name"], 
                                                     soft_parameters=structural_annotation["software_parameters"], 
                                                     soft_version=structural_annotation["software_version"], 
                                                     comments=structural_annotation["comments"], 
                                                     files=[structural_annotation["file"]])
        # BIOMART STRUCTURE table #########################
        for structure in self.structure:
            personal_structure = self.add_analysis( analysis_name="Secondary structure Computing",
                                                    analysis_type="strucutre",
                                                    analysis_hidden="0",
                                                    soft_name=structure["software_name"],
                                                    soft_parameters=structure["software_parameters"],
                                                    soft_version=structure["software_version"],
                                                    comments=structure["comments"],
                                                    files=[structure["file"]]
                                                    )
        
        # Must be replaced later : double search for miRNA and tRNA sequences
        get_locus_sequence = self.add_component("GetLocusSequence",[self.reference_genome, predictions_for_locusview], component_prefix="2")
        rnafold = self.add_component("RNAfold", [get_locus_sequence.fasta])
        # Get already computed structures
        get_computed_structures = self.add_component("GetComputedStructures", [self.structure["file"], predictions_for_locusview])
        structure = self.add_biomart_load(Structure, [rnafold.rnafold_file, get_computed_structures.dic])
        # Add RNAfold analysis
        rnafold_analysis = self.add_analysis( analysis_name="RNAfold",
                                              analysis_type="folding",
                                              soft_name="RNAfold",
                                              soft_parameters=rnafold.options,
                                              soft_version=rnafold.get_version(),
                                              comments="Computes secondary structure of ncRNA predictions",
                                              analysis_hidden="0",
                                              files=[rnafold.rnafold_file])
        # BIOMART READ_LIBRARY_COUNT table ################
        read_library_count_infos = self.add_component("GetReadLibraryCountInfos", [self.alignment["file"],predictions_for_locusview, self.count_matrix])
        read_library_count = self.add_biomart_load(ReadLibraryCount, [read_library_count_infos.dic, self.project.libraries])
        
    @staticmethod
    def config_parser(arg_lines):
        config = ConfigParser()
        config.readfp(StringIO.StringIO('\n'.join(arg_lines)))
        arguments = []
        
        if config.has_section('project'):
            section = 'project'
            for option in config.options(section) :
                if config.has_option('project', option) :
                    arguments.extend( [ '--'+option.replace('_','-'),  config.get('project', option)  ] )

        if config.has_section('libraries') :
            section = 'libraries'
            lhash = {}
            for option in config.options(section) :
                if config.get(section, option) :
                    name, prop = option.split('.', 2)
                    if not lhash.has_key(name) :
                        lhash[name] = []
                    if prop == "files":
                        for file in config.get(section, option).split(","):
                            lhash[name].append( prop.replace('_','-') +'=' + file )
                    else:
                        lhash[name].append( prop.replace('_','-') +'=' + config.get(section, option) )
            for x in lhash.values() :
                arguments.extend( [ '--library'] + x)

        if config.has_section('cleaning') :
            section = 'cleaning'
            if config.has_option(section, 'run_cleaning'):
                if config.get(section, 'run_cleaning') and config.getboolean(section, 'run_cleaning'):
                    arguments.extend( [ '--run-cleaning'] )
            if config.has_option(section, 'cleaned_sequences') :
                if config.get(section, 'cleaned_sequences'):
                    arguments.extend( [ '--cleaned-sequences', config.get(section, 'cleaned_sequences') ] )
            if config.has_option(section, 'count_matrix') :
                if config.get(section, 'count_matrix'):
                    arguments.extend( [ '--count-matrix', config.get(section, 'count_matrix') ] )
            if config.has_option(section, 'min_length') :
                arguments.extend( [ '--min-length', config.get(section, 'min_length') ] )
            if config.has_option(section, 'max_length') :
                arguments.extend( [ '--max-length', config.get(section, 'max_length') ] )
            if config.has_option(section, 'min_expression_level') :
                arguments.extend( [ '--min-expression-level', config.get(section, 'min_expression_level') ] )
            if config.has_option(section, 'min_samples_count') :
                arguments.extend( [ '--min-samples-count', config.get(section, 'min_samples_count') ] )

        if config.has_section('genome'):
            section='genome'
            if config.has_option(section, 'file') :
                if config.get(section,'file'):
                    arguments.extend( [ '--reference-genome', config.get(section, 'file') ] )
            if config.has_option(section, 'id'):
                if config.get(section, 'id'):
                    arguments.extend( [ '--genome-id', config.get(section, 'id') ] )
            if config.has_option(section, 'version'):
                if config.get(section, 'version'):
                    arguments.extend( [ '--genome-version', config.get(section, 'version') ] )
            if config.has_option(section, 'annotation') :
                if config.get(section,'annotation'):
                    arguments.extend( [ '--genome-annotation', config.get(section, 'annotation') ] )
            if config.has_option(section, 'clade') :
                if config.get(section,'clade'):
                    arguments.extend( [ '--clade', config.get(section, 'clade') ] )
            if config.has_option(section, 'seed_size'):
                if config.get(section, 'seed_size'):
                    arguments.extend( [ '--seed-size', config.get(section, 'seed_size') ] )
            if config.has_option(section, 'seed_mismatch'):
                if config.get(section, 'seed_mismatch'):
                    arguments.extend( [ '--seed-mismatch', config.get(section, 'seed_mismatch') ] )
            if config.has_option(section, 'align_reports'):
                if config.get(section, 'align_reports'):
                    arguments.extend( [ '--aln-reports', config.get(section, 'align_reports') ] )
            if config.has_option(section, 'global_mismatch'):
                if config.get(section, 'global_mismatch'):
                    arguments.extend( [ '--global-mismatch', config.get(section, 'global_mismatch') ] )
            
        if config.has_section('annotation') :
            global_options = []
            section = 'annotation'
            if config.has_option(section, 'run_sequences_annotation'):
                if config.get(section, 'run_sequences_annotation') and config.getboolean(section, 'run_sequences_annotation'):
                    arguments.extend( [ '--run-sequences-annotation'] )
                global_options.append('run_sequences_annotation')
            if config.has_option(section, 'annotation_matrix') :
                if config.get(section, 'annotation_matrix'):
                    arguments.extend( [ '--annotation-matrix', config.get(section, 'annotation_matrix') ] )
                global_options.append('annotation_matrix')
            if config.has_option(section, 'annotate_unpredicted_regions'):
                if config.get(section, 'annotate_unpredicted_regions') and config.getboolean(section, 'annotate_unpredicted_regions'):
                    arguments.extend( [ '--annotate-unpredicted-regions'] )
                global_options.append('annotate_unpredicted_regions')
            lhash = {}
            for option in config.options(section) :
                if config.get(section, option) :
                    if not option in global_options:
                        name, prop = option.split('.', 2)
                        if not lhash.has_key(name) :
                             lhash[name] = []
                        if prop == 'file' :
                            lhash[name].append( 'file=' + config.get(section, option) )
                        else :
                            lhash[name].append( prop.replace('_', '-') +'=' + config.get(section, option) )
            for x in lhash.values() :
                arguments.extend( [ '--databank'] + x )

        if config.has_section('locus'):
            section = 'locus'
            if config.get(section, 'offset'):
                arguments.extend( [ '--offset', config.get(section, 'offset') ] )
            if config.get(section, 'min_nb_isoforms'):
                arguments.extend( [ '--min-nb-isoforms', config.get(section, 'min_nb_isoforms') ] )
            if config.get(section, 'min_size_locus'):
                arguments.extend( [ '--min-size-locus', config.get(section, 'min_size_locus') ] )
        
        if config.has_section('tRNA_prediction'):
            section = 'tRNA_prediction'
            if config.has_option(section, 'run_trna_prediction'):
                if config.get(section, 'run_trna_prediction') and config.getboolean(section, 'run_trna_prediction'):
                    arguments.extend( [ '--run-trna-prediction'] )
            if config.has_option(section, 'trna_boundary'):
                if config.get(section, 'trna_boundary'):
                    arguments.extend( [ '--trna-boundary', config.get(section, 'trna_boundary') ] )
        
        if config.has_section('miRNA_prediction'):
            section = 'miRNA_prediction'
            if config.has_option(section, 'run_mirna_prediction'):
                if config.get(section, 'run_mirna_prediction') and config.getboolean(section, 'run_mirna_prediction'):
                    arguments.extend( [ '--run-mirna-prediction'] )
            if config.has_option(section, 'mirbase_stats_file'):
                if config.get(section, 'mirbase_stats_file'):
                    arguments.extend( [ '--mirbase-stats-file', config.get(section, 'mirbase_stats_file')] )
            if config.has_option(section, 'min_appariements'):
                    if config.get(section, 'min_appariements'):
                        arguments.extend( [ '--min-appariements', config.get(section, 'min_appariements') ] )         
            
            if config.has_option(section, 'min_expr_level'):
                if config.get(section, 'min_expr_level'):
                    arguments.extend( [ '--min-expr-level', config.get(section, 'min_expr_level') ] )
            
            if config.has_option(section, 'min_loop_size'):
                if config.get(section, 'min_loop_size'):
                    arguments.extend( [ '--min-loop-size', config.get(section, 'min_loop_size') ] )
            
            if config.has_option(section, 'max_loop_size'):
                if config.get(section, 'max_loop_size'):
                    arguments.extend( [ '--max-loop-size', config.get(section, 'max_loop_size') ] )
                    
            if config.has_option(section, 'max_loop_size'):
                if config.get(section, 'max_loop_size'):
                    arguments.extend( [ '--max-loop-size', config.get(section, 'max_loop_size') ] )
                    
            if config.has_option(section, 'min_majseq_size'):
                if config.get(section, 'min_majseq_size'):
                    arguments.extend( [ '--min-majseq-size', config.get(section, 'min_majseq_size') ] )
                    
            if config.has_option(section, 'max_majseq_size'):
                if config.get(section, 'max_majseq_size'):
                    arguments.extend( [ '--max-majseq-size', config.get(section, 'max_majseq_size') ] )
        
            if config.has_option(section, 'max_cpt_bulges'):
                if config.get(section, 'max_cpt_bulges'):
                    arguments.extend( [ '--max-cpt-bulges', config.get(section, 'max_cpt_bulges') ] )
            
            if config.has_option(section, 'max_bulge_size'):
                if config.get(section, 'max_bulge_size'):
                    arguments.extend( [ '--max-bulge-size', config.get(section, 'max_bulge_size') ] )
        
        if config.has_section('rRNA_prediction'):
            section = 'rRNA_prediction'
            if config.has_option(section, 'run_rrna_prediction'):
                if config.get(section, 'run_rrna_prediction') and config.getboolean(section, 'run_rrna_prediction'):
                    arguments.extend( [ '--run-rrna-prediction'] )
            if config.has_option(section, 'rrna_boundary'):
                if config.get(section, 'rrna_boundary'):
                    arguments.extend( [ '--rrna-boundary', config.get(section, 'rrna_boundary') ] )

        return arguments

    @staticmethod
    def load_config_parser(arg_lines):
        config = ConfigParser()
        config.readfp(StringIO.StringIO('\n'.join(arg_lines)))
        arguments = []
        add_analysis_options = ['software_name', 'software_parameters',  'software_version', 'comments']
        
        if config.has_section('project'):
            section = 'project'
            for option in config.options(section) :
                if config.has_option('project', option) :
                    arguments.extend( [ '--'+option.replace('_','-'),  config.get('project', option)  ] )

        if config.has_section('genome'):
            section='genome'
            for option in config.options(section) :
                if config.get(section, option) :
                    arguments.extend(["--reference-genome", config.get(section,option)])
        
        if config.has_section('alignment') :
            section = 'alignment'
            arguments.append('--alignment');
            if config.has_option(section, 'file') :
                arguments.append(  'file=' + config.get(section, 'file') )
            for option in add_analysis_options :
                if config.has_option('alignment', option) :
                    arguments.append(  option.replace('_', '-')  + '=' + config.get(section, option) )
        
        if config.has_section('expression'):
            section='expression'
            for option in config.options(section) :
                if config.get(section, option) :
                    arguments.extend(["--count-matrix", config.get(section,option)])
        
        if config.has_section('reads_annotation'):
            section='reads_annotation'
            for option in config.options(section) :
                if config.get(section, option) :
                    arguments.extend(["--annotation-matrix", config.get(section,option)])
            
        if config.has_section('libraries') :
            section = 'libraries'
            lhash = {}
            for option in config.options(section) :
                if config.get(section, option) :
                    name, prop = option.split('.', 2)
                    if not lhash.has_key(name) :
                        lhash[name] = []
                    if prop == "files":
                        for file in config.get(section, option).split(","):
                            lhash[name].append( prop.replace('_','-') +'=' + file )
                    else:
                        lhash[name].append( prop.replace('_','-') +'=' + config.get(section, option) )
            for x in lhash.values() :
                arguments.extend( [ '--library'] + x)
        
        if config.has_section('annotations') :
            section = 'annotations'
            lhash = {}
            for option in config.options(section) :
                if config.get(section, option) :
                    name, prop = option.split('.', 2)
                    if not lhash.has_key(name) :
                        lhash[name] = []
                    lhash[name].append( prop.replace('_','-') +'=' + config.get(section, option) )
            for x in lhash.values() :
                arguments.extend( [ '--annotation'] + x)
        
        if config.has_section('structural_annotations') :
            section = 'structural_annotations'
            lhash = {}
            for option in config.options(section) :
                if config.get(section, option) :
                    name, prop = option.split('.', 2)
                    if not lhash.has_key(name) :
                        lhash[name] = []
                    lhash[name].append( prop.replace('_','-') +'=' + config.get(section, option) )
            for x in lhash.values() :
                arguments.extend( [ '--structural-annotation'] + x)
        
        if config.has_section('structures') :
            section = 'structures'
            lhash = {}
            for option in config.options(section) :
                if config.get(section, option) :
                    name, prop = option.split('.', 2)
                    if not lhash.has_key(name) :
                        lhash[name] = []
                    lhash[name].append( prop.replace('_','-') +'=' + config.get(section, option) )
            for x in lhash.values() :
                arguments.extend( [ '--structure'] + x)
        
        if config.has_section('predictions') :
            section = 'predictions'
            lhash = {}
            for option in config.options(section) :
                if config.get(section, option) :
                    name, prop = option.split('.', 2)
                    if not lhash.has_key(name) :
                        lhash[name] = []
                    lhash[name].append( prop.replace('_','-') +'=' + config.get(section, option) )
            for x in lhash.values() :
                arguments.extend( [ '--prediction'] + x)
        
        return arguments
