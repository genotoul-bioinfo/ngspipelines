#
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'alpha'

from optparse import *
import os,sys,re,datetime,shutil
from subprocess import Popen, PIPE
import glob

def version_string ():
    """
    Return the rarefaction_curve version
    """
    return "rarefaction_curve.py " + __version__

if __name__ == '__main__':
    BAM_FPROPER_PAIR  =2
    
    parser = OptionParser(usage="Usage: %prog", description = "Read non sorted bam file as stdin and compute the rarefaction curve.")
    
    igroup = OptionGroup(parser, "Input options","")
    igroup.add_option("-i", "--input", dest="bam", help="Input bam file.")
    igroup.add_option("-s", "--step", dest="step", type="int", help="number of reads to add to compute numbers of contigs view at each step.", default=50000)
    igroup.add_option("-e", "--exec", dest="exec_samtools", help="Samtools exec path (if not in PATH).", default="samtools")
    parser.add_option_group(igroup)
    
    ogroup = OptionGroup(parser, "Output files options","")
    ogroup.add_option("-o", "--output", dest="output", help="the output file")
    parser.add_option_group(ogroup)
    
    (options, args) = parser.parse_args()
    if options.output == None:
        sys.stderr.write("Output file is missing")
        parser.print_help()
        sys.exit(1)
    
    #temp output directory
    tmp_dir = options.output+"_tmp"
    if os.path.exists(tmp_dir) :
        shutil.rmtree(tmp_dir) 
    os.mkdir(tmp_dir)
    
    #Filter and shuffle data
    cmd_bamshuf = [options.exec_samtools + " view " + options.bam + " | cut -f 3 | shuf > "+ os.path.join(tmp_dir,"shuf") ]
    exec_shuffle = Popen(cmd_bamshuf, shell=True)
    exec_shuffle.wait()
    cmd_split = ["split " + os.path.join(tmp_dir,"shuf") + " -l " + str(options.step)+ " " + options.output+"_tmp/split"]
    exec_split = Popen(cmd_split, shell=True)
    exec_split.wait()
    
    files = glob.glob(os.path.join(tmp_dir,"split*"))
    cmd_touch = ["touch " + os.path.join(tmp_dir,"reference_uniq")]
    exec_touch = Popen(cmd_touch, shell=True) 
    exec_touch.wait()
    
    cpt=0
    fout=open (options.output, "w")
    fout.write (str(cpt) + "\t0\n")
    
    for f in files :
        cmd_count1 = ["sort -u " + f + " > "+os.path.join(tmp_dir,"unique")]
        exec_count1 = Popen(cmd_count1, shell=True)
        exec_count1.wait()
        cmd_count2 = ["cat " + os.path.join(tmp_dir,"unique") + " " + os.path.join(tmp_dir,"reference_uniq") + " > " + os.path.join(tmp_dir,"reference_cumul")]
        exec_count2 = Popen(cmd_count2, shell=True)
        exec_count2.wait()
        cmd_count3 = ["sort -u " + os.path.join(tmp_dir,"reference_cumul")+ ">" + os.path.join(tmp_dir,"reference_uniq") +";" ]
        exec_count3 = Popen(cmd_count3, shell=True)
        exec_count3.wait()
        cmd_count4 = ["wc -l " + os.path.join(tmp_dir,"reference_uniq")]
        exec_count4 = Popen(cmd_count4, stdout=PIPE, shell=True)
        exec_count4.wait()
        res = exec_count4.stdout.readline().split(" ")[0]
        cpt += options.step
        fout.write (str(cpt) + "\t"+res+"\n")
    shutil.rmtree(tmp_dir) 