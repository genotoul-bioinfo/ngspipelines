#!/usr/bin/env python2.7
import os
import sys
import time
import argparse
import subprocess


# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


class Sequence:
    def __init__(self, id, string, description=None, quality=None):
        """
        @param id : [str] Id of the sequence.
        @param string : [str] Sequence of the sequence.
        @param description : [str] The sequence description.
        @param quality : [str] The quality of the sequence (same length as string).
        """
        self.id = id
        self.description = description
        self.string = string
        self.quality = quality


class NucleicSequence(Sequence):    
    complement_rules = {'A':'T','T':'A','G':'C','C':'G','U':'A','a':'t','t':'a','g':'c','c':'g','u':'a'}
    
    def revcom( self ):
        """
        @summary : Reverse complement the sequence.
        """
        self.string = "".join([NucleicSequence.complement_rules[base] for base in self.string[::-1]])
    
    def complement( self ):
        """
        @summary : Complement the sequence.
        """
        self.string = "".join([NucleicSequence.complement_rules[base] for base in list(self.string)])


class Codon(NucleicSequence):
    genetic_code = { 'TCA':'Ser', 'TCC':'Ser', 'TCG':'Ser', 'TCT':'Ser', 'AGC':'Ser', 'AGT':'Ser',
                     'TTC':'Phe', 'TTT':'Phe',
                     'TTA':'Leu', 'TTG':'Leu', 'CTA':'Leu', 'CTC':'Leu', 'CTG':'Leu', 'CTT':'Leu',
                     'TAC':'Tyr', 'TAT':'Tyr',
                     'TAA':'*', 'TAG':'*', 'TGA':'*',
                     'TGC':'Cys', 'TGT':'Cys',
                     'TGG':'Trp',
                     'CCA':'Pro', 'CCC':'Pro', 'CCG':'Pro', 'CCT':'Pro',
                     'CAC':'His', 'CAT':'His',
                     'CAA':'Gln', 'CAG':'Gln',
                     'CGA':'Arg', 'CGC':'Arg', 'CGG':'Arg', 'CGT':'Arg', 'AGA':'Arg', 'AGG':'Arg',
                     'ATA':'Ile', 'ATC':'Ile', 'ATT':'Ile',
                     'ATG':'Met',
                     'ACA':'Thr', 'ACC':'Thr', 'ACG':'Thr', 'ACT':'Thr',
                     'AAC':'Asn', 'AAT':'Asn',
                     'AAA':'Lys', 'AAG':'Lys',
                     'GTA':'Val', 'GTC':'Val', 'GTG':'Val', 'GTT':'Val',
                     'GCA':'Ala', 'GCC':'Ala', 'GCG':'Ala', 'GCT':'Ala',
                     'GAC':'Asp', 'GAT':'Asp',
                     'GAA':'Glu', 'GAG':'Glu',
                     'GGA':'Gly', 'GGC':'Gly', 'GGG':'Gly', 'GGT':'Gly' }

    def trad( self ):
        """
        @summary : Returns the 3 letters amino acid code for the codon.
        @return : [str]
        """
        return Codon.genetic_code[self.string]
        
    def __str__(self):
        return self.string


# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


class FastaIO:
    def __init__( self, filepath, mode="r" ):
        """
        @param filepath : [str] The filepath.
        @param mode : [str] Mode to open the file ('r', 'w', 'a').
        """
        self.filepath = filepath
        self.mode = mode
        self.file_handle = open( filepath, mode )
        self.current_line_nb = 1
        self.current_line = None
    
    def __del__( self ):
        self.close()
        
    def close( self ) :
        if hasattr(self, 'file_handle') and self.file_handle is not None:
            self.file_handle.close()
            self.file_handle = None
            self.current_line_nb = None
    
    def __iter__( self ):
        seq_id = None
        seq_desc = None
        seq_str = None
        try:
            for line in self.file_handle:
                line = line.rstrip()
                self.current_line_nb += 1
                if line.startswith('>'):
                    if seq_id is not None:
                        seq_record = Sequence( seq_id, seq_str, seq_desc )
                        yield seq_record
                    # New seq
                    fields = line[1:].split("\s", 1)
                    seq_id = fields[0]
                    seq_desc = fields[1] if len(fields) == 2 else None
                    seq_str = ""
                else:
                    seq_str += line
            if seq_id is not None:
                seq_record = Sequence( seq_id, seq_str, seq_desc )
                yield seq_record
        except:
            raise IOError( "The line " + str(self.current_line_nb) + " in '" + self.filepath + "' cannot be parsed by " + self.__class__.__name__ + "." )
            
    def next_seq( self ):
        """
        @summary : Returns the next sequence.
        @return : [Sequence] The next sequence.
        """
        seq_record = None
        line = ""
        try:
            # First line in file
            if self.current_line_nb == 1:
                self.next_id = self.file_handle.readline().strip()
                self.current_line_nb += 1
            # Sequence
            seq_str = ""
            while not line.startswith('>'):
                seq_str += line.strip()
                line = self.file_handle.readline()
                if not line:
                    break
                self.current_line_nb += 1
            fields = self.next_id[1:].split(None, 1)
            seq_id = fields[0]
            seq_desc = fields[1] if len(fields) == 2 else None
            seq_record = Sequence( seq_id, seq_str, seq_desc )
            self.next_id = line # next seq_id
        except:
            raise IOError( "The line " + str(self.current_line_nb) + " in '" + self.filepath + "' cannot be parsed by " + self.__class__.__name__ + ".\n"
                            + "content : " + line )
        return seq_record
    
    def write( self, sequence_record ):
        self.file_handle.write( self.seqToFastaLine(sequence_record) + "\n" )
    
    def seqToFastaLine( self, sequence ):
        """
        @summary : Returns the sequence in fasta format.
        @param sequence : [Sequence] The sequence to process.
        @return : [str] The sequence.
        """
        header = ">" + sequence.id + (" " + sequence.description if sequence.description is not None else "")
        return header + "\n" + sequence.string


# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


class VCFRecord:
    def __init__(self, region=None, position=None, knownSNPId=None, refAllele=None, altAlleles=None, qual=None, pFilter=None, info=None, pFormat=None, samples=None):
        self.chrom   = region
        self.pos     = position
        self.id      = knownSNPId
        self.ref     = refAllele
        self.alt     = altAlleles
        self.qual    = qual
        self.filter  = pFilter
        self.info    = info if info is not None else dict()
        self.format  = pFormat if pFormat is not None else list()
        self.samples = samples if samples is not None else list()
    
    def isIndel(self):
        """
        @summary : Return True if the variant is an insertio or a deletion.
        @return : [bool]
        """
        isIndel = False
        if self.ref == "." or self.ref == "-":
            isIndel = True
        else:
            for allele in self.alt:
                if len(allele) != len(self.ref) or allele == "." or allele == "-":
                    isIndel = True
        return isIndel
        
    def type(self):
        """
        @summary : Returns the vrariant type.
        @return : [str] 'snp' or 'indel' or 'variation'.
        """
        record_type = "snp"
        if self.isIndel():
            record_type = "indel"
        elif len(self.ref) > 1:
            record_type = "variation"
        return record_type


class VCFIO:
    def __init__( self, filepath, mode="r" ):
        """
        @param filepath : [str] The filepath.
        @param mode : [str] Mode to open the file ('r', 'w', 'a').
        """
        self.filepath = filepath
        self.mode = mode
        self.file_handle = open( filepath, mode )
        self.current_line_nb = 0
        self.current_line = None
    
    def __del__( self ):
        self.close()
    
    def __iter__( self ):
        for line in self.file_handle:
            self.current_line = line.rstrip()
            self.current_line_nb += 1
            if self.current_line.startswith('#') :
                continue
            try:
                vcf_record = self._parse_line()
            except:
                raise IOError( "The line " + str(self.current_line_nb) + " in '" + self.filepath + "' cannot be parsed by " + self.__class__.__name__ + ".\n" +
                               "Line content : " + self.current_line )
            else:
                yield vcf_record
            
    def close( self ) :
        if hasattr(self, 'file_handle') and self.file_handle is not None:
            self.file_handle.close()
            self.file_handle = None
            self.current_line_nb = None
            self.current_line = None
    
    def _parse_line( self ):
        """
        @summary : Returns a structured record from the VCF current line.
        @return : [VCFRecord]
        """
        fields = self.current_line.rstrip().split('\t')
        variation = VCFRecord() 
        variation.chrom  = fields[0]
        variation.pos    = int(fields[1])
        variation.id     = fields[2]
        variation.ref    = fields[3]
        variation.alt    = fields[4].split(',')
        variation.qual   = fields[5]
        variation.filter = fields[6]
        variation.format = fields[8].split(':')

        # Field INFO
        if fields[7] != '.' :
            info = dict()
            for tag_and_value in fields[7].split(';'):
                if "=" in tag_and_value:
                    tag, value = tag_and_value.split('=')
                    if "," in value:
                        info[tag] = value.split(",")
                    else:
                        info[tag] = value
                else:
                    info[tag_and_value] = True
            variation.info = info
        #~ # Fields samples
        #~ regexp_none=re.compile("\.(\/\.)*")
        #~ for lib_infos in range (9,len(fields)) :
            #~ if not regexp_none.match(fields[lib_infos]):
                #~ sformat = fields[lib_infos].split(':')
                #~ variation.samples.append( Entry(**{ autocast(variation.format[i]) : autocast(sformat[i]) if sformat[i] != '.' else None for i in range(0,len(variation.format)) } )  )
            #~ else :
                #~ variation.samples.append( Entry(**{ autocast(variation.format[i]) : None for i in range(0,len(variation.format)) }) )
        return variation


# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


class Logger:
    """
    @summary : Class to log program execution. You can defined the output stream and the levels managed.
    @todo : Singleton and factory for logger.
    """
    
    def __init__(self, filepath=None, levels=None, time_function=None):
        """
        @param filepath : [str] The log filepath. [default : STDOUT]
        @param levels : [list] The list of levels managed by logger (ex : "INFO", "DEBUG", "USER" ).
        @param time_function : [func] The function used to display time in log message. [default : time.time]
        """
        self.filepath = filepath
        self.time_function = time_function if time_function is not None else time.time
        self.levels = levels if levels is not None else ["INFO"]
        if self.filepath is not None:
            self.file_handle = open( self.filepath, "a" )
        else:
            self.file_handle = sys.stdout
    
    def __del__(self):
        """
        @summary : Closed file handler when the logger is detroyed.
        """
        if self.filepath is not None:
            self.file_handle.close()
            self.file_handle = None
    
    def write(self, msg, level="INFO", section=None):
        """
        @summary : Writes the log message if the logger manage the message level.
        @param msg : [str] The message.
        @param level : [str] Level of the message.
        @param section : [str] The section for the message.
        """
        if level in self.levels:
            if section is None:
                self.file_handle.write( "[" + str(self.time_function()) + "] " + level + " - " + msg + "\n" )
            else:
                self.file_handle.write( "[" + str(self.time_function()) + "] " + level + " - " + section + " : " + msg + "\n" )


# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


if __name__ == "__main__":
	def date_log(): return time.strftime("%Y-%m-%d %H:%M:%S")
	logger = Logger(None, "INFO", date_log)
	
	# Manage parameters
	parser = argparse.ArgumentParser( description='Extract sub-sequences centered on vrariants and blast these sequences on databank.' )
	parser.add_argument( '-u', '--max-upstream', type=int, default=120, help='Maximum number of nucleotids before variant in variant the new variant seq.' )
	parser.add_argument( '-d', '--max-downstream', type=int, default=120, help='Maximum number of nucleotids after variant in variant the new variant seq.' )
	parser.add_argument( "--version", action='version', version=__version__ )
	group_input = parser.add_argument_group( 'Inputs' ) # Inputs
	group_input.add_argument( '--sequences-file', required=True, help='Reference sequences (format : FASTA).' )
	group_input.add_argument( '--variants-file', required=True, help='Variants to annotate (format : VCF).' )
	group_blast = parser.add_argument_group( 'Alignment options' ) # Alignments
	group_blast.add_argument( '--aln-db', required=True, help='Databank used to find similarities.' )
	group_blast.add_argument( '-b', '--blast-path', required=True, help='The blast program path.' )
	group_blast.add_argument( '-t', '--nb-threads', type=int, default=2, help='Number of threads.' )
	group_output = parser.add_argument_group( 'Outputs' ) # Outputs
	group_output.add_argument( '-q', '--queries-file', required=True, help='The new sequences file (short sequences centered on vraiants).')
	group_output.add_argument( '-x', '--blast-file', required=True, help='The blast output file path.')
	args = parser.parse_args()

	# Convert variants to fasta
	logger.write( "Variants to queries." )
	queries_file = FastaIO( args.queries_file, "w" )
	sequences_file = FastaIO( args.sequences_file )
	current_seq = sequences_file.next_seq()
	vcf_to_annot = VCFIO( args.variants_file )
	for variant in vcf_to_annot:
		if variant.type() == "snp":
			while current_seq.id != variant.chrom:
				current_seq = sequences_file.next_seq()
			sub_seq_start = max( variant.pos-args.max_upstream-1, 0 )
			sub_seq_end = min( variant.pos+args.max_downstream, len(current_seq.string) )
			sub_seq_str = current_seq.string[sub_seq_start:sub_seq_end+1]
			sub_seq = Sequence( variant.chrom + "_chr@" + str(variant.pos) + '.' + '_'.join(variant.alt), sub_seq_str, "sub@:" + str(variant.pos - sub_seq_start) )
			queries_file.write( sub_seq )
	queries_file.close()

	# Launch blast
	command_line_options = " -db " + args.aln_db + " -query " + args.queries_file + " -outfmt 5 -num_threads " + str(args.nb_threads)
	logger.write( "Align queries : " + args.blast_path + command_line_options + " > " + args.blast_file )
	FH_output = open( args.blast_file, "w" )
	subprocess.check_call( [args.blast_path] + command_line_options.split(), stdout=FH_output )

	logger.write( "Execution complete." )
