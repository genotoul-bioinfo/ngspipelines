#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'alpha'

import sys
import os
sys.path.insert(0, os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'lib'))
from optparse import *
import NCBIXML


def version_string ():
    """
    Return the blast_filter version
    """
    return "blast_filter " + __version__


if __name__ == '__main__':


    parser = OptionParser(usage="Usage: %prog -i FILE|- -o FILE", description = "Filter an xml blast file", version = version_string())
    
    igroup = OptionGroup(parser, "Input file options","")
    igroup.add_option("-i", "--input", dest="input",
                  help="The input file, if set to - read STDIN, REQUIRE")
    parser.add_option_group(igroup)
    
    ogroup = OptionGroup(parser, "Output files options","")
    ogroup.add_option("-o", "--output", dest="output",
                  help="The output file REQUIRE")
    parser.add_option_group(ogroup)
    
    fgroup = OptionGroup(parser, "Filters options","")
    fgroup.add_option("-q", "--pcq", dest="pcq",
                  help="Minimun poucentage of query length per alignment REQUIRE ",  default=50 )
    fgroup.add_option("-p", "--pci", dest="pci",
                  help="Minimun poucentage of identity per alignment REQUIRE ",  default=50 )
    parser.add_option_group(fgroup)
    
    (options, args) = parser.parse_args()

    
    blast_records = NCBIXML.parse(options.input)
    for blast_record in blast_records:
        
        no_annot_regions_saved=[]
        no_annot_regions_work=[]
        nb_no_annot_region=0
        for alignment in blast_record.alignments:
            for hsp in alignment.hsps:
                keep_hsp = 0
                if nb_no_annot_region == 0 :
                    no_annot_regions_saved.append([0,hsp.query_start])
                    no_annot_regions_saved.append([hsp.query_end,blast_record.query_letters])
                    nb_no_annot_region=nb_no_annot_region+1
                    keep_hsp = 1
                else :
                    for [hole_start,hole_end] in no_annot_regions_work :
                        #for each hole
                        delete_no_annot_region = 0
                        # hsp include a no annot region
                        if hsp.query_start < hole_start and hsp.query_end > hole_end :
                            delete_no_annot_region = 1
                            keep_hsp = 1
                        #hsp start is in a no annot region
                        if hsp.query_start >  hole_start and hsp.query_start < hole_end:
                            #hsp is in the none annotated region
                            if (hsp.query_end < hole_end) :
                                keep_hsp = 1
                                # adding the 2 new none annotated region to region table
                                no_annot_regions_saved.append([hole_start,hsp.query_start])
                                no_annot_regions_saved.append([hsp.query_end,hole_end])
                            else :
                                keep_hsp = 1;
                                # update end of the existing none annotated region 
                                no_annot_regions_saved.append([hole_start,hsp.query_start])
                        else :
                            
                            # no annot region include query end
                            if hsp.query_end > hole_start and hsp.query_end < hole_end :
                                if hsp.query_start <= hole_start :
                                    keep_hsp = 1;
                                    no_annot_regions_saved.append([hsp.query_end,hole_end])
                            else :
                                if delete_no_annot_region == 0 :
                                    no_annot_regions_saved.append([hole_start,hole_end])
                if keep_hsp :
                    print blast_record.query +" "+str(hsp.query_start)+" "+str(hsp.query_end) + " " +alignment.hit_def + " " +str(hsp.sbjct_start) + " " +str(hsp.sbjct_end)
                no_annot_regions_work=no_annot_regions_saved[:]
                no_annot_regions_saved=[]
                
