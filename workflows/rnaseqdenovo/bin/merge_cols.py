#
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'beta'

from optparse import *
import sys,re,datetime
from os.path import join, splitext, basename, exists, split

def read_file (file,libname,dict_col, col_number=1):
    """
    Extract count from the count file 
      @param file          : the input count file (output of sam2count)
      @param libname       : the library name   
      @param dict_col    : the dict of values
      @param col_number    : column to extract in each files [fisrt column = 0]
    """
    fin=open (file, "r")
    #parse data
    for line in fin :
        
        tab = line.rstrip().split("\t") # split line
        if line.startswith("#") or len(tab) < int(col_number) or tab[0] == "*":
            continue

        if not dict_col.has_key(tab[0]) : # if new contig
            dict_col[tab[0]] = {}
        if not dict_col[tab[0]].has_key(libname) : # if new library for this contig
            dict_col[tab[0]][libname] = {}
        
        dict_col[tab[0]][libname] = tab[int(col_number)]
    fin.close


def version_string ():
    """
    Return the merge_count version
    """
    return "merge_count.py " + __version__


if __name__ == '__main__':
    
    parser = OptionParser(usage="Usage: %prog", description = "Merge files columns on first column, skip lines starting with #")
    
    igroup = OptionGroup(parser, "Input options","")
    igroup.add_option("-f", "--files", dest="files", help="tabulated files separated by ',' ")
    igroup.add_option("-n", "--names", dest="names", help="matrix columns names in the same order as files ',' ")
    igroup.add_option("-c", "--column", dest="col_number", help="column of each files to extract, first column is 0, default 1", default=1, metavar="INT")
    parser.add_option_group(igroup)
    
    ogroup = OptionGroup(parser, "Output files options","")
    ogroup.add_option("-o", "--output", dest="output", help="the output count file")
    parser.add_option_group(ogroup)
    
    (options, args) = parser.parse_args()

    if options.files == None :
        sys.stderr.write("Need files to merge\n")
        parser.print_help()
        sys.exit(1)

    if options.col_number < 1 :
        sys.stderr.write("Column must be greater than 0 \n")
        parser.print_help()
        sys.exit(1)
        
    if options.output == None:
        sys.stderr.write("Output file is missing\n")
        parser.print_help()
        sys.exit(1)
    
    dict_col={}
    files = options.files.split(",")
    libs_name = []
    get_lib_name = True
    if options.names :
        libs_name=options.names.split(",")
        get_lib_name = False
        if len(libs_name) != len(files):
             sys.stderr.write("You must provide same number of files and names\n")
             parser.print_help()
             sys.exit(1)

    current_libname=""
    for idx, line in enumerate(files):        
        file = line.rstrip()
        if exists(file) :
            if get_lib_name : 
                current_libname = splitext(basename(file))[0]
                libs_name.append(current_libname)
            else : 
                current_libname = libs_name[idx]
            read_file(file,current_libname,dict_col,options.col_number)
        else :
            sys.stderr.write("File : " + file + " doesn't exist")
            sys.exit(1)
    fout = open (options.output,"w")
    fout.write ( '#contig_name\t' + '\t'.join(libs_name) +"\n")
    # Pour tous les contigs : contruction de la ligne 
    for contig_name in sorted(dict_col.keys()) :
        line = []
        line.append( contig_name )
        # Pour toutes les contigs 
        for lib in libs_name :
            if dict_col[contig_name].has_key(lib) :
                line.append( str(dict_col[contig_name][lib]) )
            else :
                line.append( "0" )
        fout.write ( '\t'.join(line) +"\n")
    fout.close
    sys.exit(0)
        
