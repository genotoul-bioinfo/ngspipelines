#
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'beta'

from optparse import *
import sys,re,datetime
from os.path import join, splitext, basename, exists, split

def read_count_file (file,libname,dict_count):
    """
    Extract count from the count file 
      @param file          : the input count file (output of sam2count)
      @param libname       : the library name   
      @param dict_count    : the dict of contigs to update
    """
    fin=open (file, "r")
    
    re_parsed   = re.compile("# total read parsed : (\d+)")
    re_mapped   = re.compile("# total read mapped : (\d+)")
    re_parsed_paired        = re.compile("# total paired read parsed : (\d+)")
    re_mapped_paired        = re.compile("# total paired read mapped : (\d+)")
    re_mapped_paired_isize  = re.compile("# total paired read mapped with good insert size : (\d+)")

    total_parsed_read       = 0
    total_mapped_read       = 0
    total_parsed_pair       = -1
    total_mapped_pair       = -1
    total_mapped_pair_isize = -1
    
    # parse headers
    line = fin.readline()
    while not line.startswith("#id") :
        if line.startswith("# total read parsed") :
            m = re_parsed.match(line)
            if m != None :
                total_parsed_read = int(m.group(1))
        if line.startswith("# total read mapped") :
            m = re_mapped.match(line)
            if m != None :
                total_mapped_read = int(m.group(1))  
        if line.startswith("# total paired read parsed"):
            m = re_parsed_paired.match(line)
            if m != None :
                total_parsed_pair = int(m.group(1))
        if line.startswith("# total paired read mapped") :
            m = re_mapped_paired.match(line)
            if m != None :
                total_mapped_pair = int(m.group(1))
        if line.startswith("# total paired read mapped with good insert size") :
            m = re_mapped_paired_isize.match(line)
            if m != None :
                total_mapped_pair_isize = int(m.group(1))

        line = fin.readline()
    #split header
    tab     = line.split()
    headers = tab[2:]

    #parse data
    line = fin.readline()
    while line != "":
        tab = line.split("\t") # contig count line
        if len(tab) > 0 :
            if not dict_count.has_key(tab[0]) : # if new contig
                dict_count[tab[0]] = {}
            if not dict_count[tab[0]].has_key(libname) : # if new library for this contig
                dict_count[tab[0]][libname] = {}
            
            index = 2 # headers/ values begin at 3rd column
            for type_val in headers :
                dict_count[tab[0]][libname][type_val] = str(int(tab[index]))
                index = index+1
                
        line = fin.readline()
    fin.close
    return total_parsed_read,total_mapped_read,total_parsed_pair,total_mapped_pair,total_mapped_pair_isize

def version_string ():
    """
    Return the merge_count version
    """
    return "merge_count.py " + __version__


if __name__ == '__main__':
    
    parser = OptionParser(usage="Usage: %prog", description = "Merge count files into a global counting file,\
     for single file use the column nb_single, for paired file use the count column nb_pair_isize.")
    
    igroup = OptionGroup(parser, "Input options","")
    igroup.add_option("-f", "--fof", dest="fof", help="file of count file")
    igroup.add_option("-c", "--correct_pair_only", dest="correct_pair_only", help="If paired count file take column of correct pair only (default use column with good insert size count if available)",action="store_true",default=False)
    parser.add_option_group(igroup)
    
    ogroup = OptionGroup(parser, "Output files options","")
    ogroup.add_option("-o", "--output", dest="output", help="the output count file")
    parser.add_option_group(ogroup)
    
    (options, args) = parser.parse_args()

    if options.fof == None :
        sys.stderr.write("Need a file of file\n")
        parser.print_help()
        sys.exit(1)

    if options.output == None:
        sys.stderr.write("Output file is missing\n")
        parser.print_help()
        sys.exit(1)
    
    ouput_single = join(split(options.output)[0], "single."+ basename(options.output)) 
    ouput_paired = join(split(options.output)[0], "paired."+ basename(options.output))
    dict_count={}

    indexlib=0
    #parcours des fichiers 
    if options.fof != None :
        fin=open (options.fof, "r")
        line=fin.readline()
        while line != "":
            file = line.rstrip()
            if exists(file) :
                current_libname = splitext(basename(file))[0]
                
                (total_parsed_read,total_mapped_read,total_parsed_pair,total_mapped_pair,total_mapped_pair_isize)=read_count_file(file,current_libname,dict_count)
                if not dict_count.has_key("total_parsed") :
                    dict_count["total_parsed"] = {}
                    dict_count["total_mapped"] = {}
                    
                if not dict_count["total_parsed"].has_key(current_libname) :
                    dict_count["total_parsed"][current_libname] = {}
                    dict_count["total_mapped"][current_libname] = {}
                    
                dict_count["total_parsed"][current_libname]["nb_single"]            = total_parsed_read
                dict_count["total_parsed"][current_libname]["nb_read_paired"]       = total_parsed_pair
                dict_count["total_parsed"][current_libname]["nb_read_paired_isize"] = None
                
                dict_count["total_mapped"][current_libname]["nb_single"]            = total_mapped_read
                dict_count["total_mapped"][current_libname]["nb_read_paired"]       = total_mapped_pair
                dict_count["total_mapped"][current_libname]["nb_read_paired_isize"] = total_mapped_pair_isize
            else :
                sys.stderr.write("File : " + file + " doesn't exist")
                sys.exit(1)
            line        = fin.readline()
            indexlib    = indexlib+1
        fin.close
        fout = open (options.output,"w")

        write_paired_as_single = 0
        
        header_total_parsed = [ "#TotalParsed" ]
        header_total_mapped = [ "#TotalMapped" ]
        header_contig = [ "#contig_name" ]
        libs_write_paired_as_single=[]
        for lib in sorted(dict_count["total_parsed"].keys()) :
            if dict_count["total_parsed"][lib]["nb_read_paired"] > -1 :
                write_paired_as_single=1
                libs_write_paired_as_single.append(lib)
                header_total_parsed.append( str(dict_count["total_parsed"][lib]["nb_read_paired"]) )
                if ( options.correct_pair_only or dict_count["total_mapped"][lib]["nb_read_paired_isize"] == -1) : 
                    header_total_mapped.append( str(dict_count["total_mapped"][lib]["nb_read_paired"]) )
                else :
                    header_total_mapped.append( str(dict_count["total_mapped"][lib]["nb_read_paired_isize"]) )
            else :
                header_total_parsed.append( str(dict_count["total_parsed"][lib]["nb_single"]) )
                header_total_mapped.append( str(dict_count["total_mapped"][lib]["nb_single"]) )
            header_contig.append( lib )
            
        fout.write ( '\t'.join( header_contig ) +"\n")
        fout.write ( '\t'.join( header_total_parsed ) +"\n")
        fout.write ( '\t'.join( header_total_mapped ) +"\n")

        # Create single output only if paired data are in merged count
        f_single_out = None
        if write_paired_as_single :
            f_single_out = open (ouput_single,"w")
            header_total_single_parsed = [ "#TotalParsed" ]
            header_total_single_mapped = [ "#TotalMapped" ]
            for lib in sorted(libs_write_paired_as_single) :
                header_total_single_parsed.append( str(dict_count["total_parsed"][lib]["nb_single"]) )
                header_total_single_mapped.append( str(dict_count["total_mapped"][lib]["nb_single"]) )
            f_single_out.write( '\t'.join( ["#contig_name"] + sorted(libs_write_paired_as_single) ) + "\n")
            f_single_out.write( '\t'.join(header_total_single_parsed) + "\n")
            f_single_out.write( '\t'.join(header_total_single_mapped) + "\n")
            
        for contig_name in sorted(dict_count.keys()) :
            line = []
            if (contig_name == "total_parsed" or contig_name == "total_mapped") : continue 
            line.append( contig_name ) 
            line_single = line[:]
            for lib in sorted(dict_count[contig_name].keys()) :
                if  dict_count["total_parsed"][lib]["nb_read_paired"] >- 1 : # If lib is paired
                    if  (options.correct_pair_only or dict_count["total_mapped"][lib]["nb_read_paired_isize"] == -1 ) :
                        line.append(str(dict_count[contig_name][lib]["nb_read_paired"]) )
                    else :
                        line.append(str(dict_count[contig_name][lib]["nb_read_paired_isize"]))
                else :
                    line.append( str(dict_count[contig_name][lib]["nb_single"]) )
            for lib in sorted (libs_write_paired_as_single):
                line_single.append( str(dict_count[contig_name][lib]["nb_single"]) )
                
            fout.write ( '\t'.join(line) +"\n")
            if write_paired_as_single :
                f_single_out.write( '\t'.join(line_single) +"\n")
        
        if write_paired_as_single :
            f_single_out.close()
        fout.close
    sys.exit(0)
        