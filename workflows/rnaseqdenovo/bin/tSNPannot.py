#!/usr/bin/env python2.7
import os
import sys
import re
import time
import argparse


# Copyright 1999-2000 by Jeffrey Chang.  All rights reserved.
# This code is part of the Biopython distribution and governed by its
# license.  Please see the LICENSE file that should have been included
# as part of this package.

"""Record classes to hold BLAST output.

Classes:
Blast              Holds all the information from a blast search.
PSIBlast           Holds all the information from a psi-blast search.

Header             Holds information from the header.
Description        Holds information about one hit description.
Alignment          Holds information about one alignment hit.
HSP                Holds information about one HSP.
MultipleAlignment  Holds information about a multiple alignment.
DatabaseReport     Holds information from the database report.
Parameters         Holds information from the parameters.

"""
# XXX finish printable BLAST output

class Header(object):
    """Saves information from a blast header.

    Members:
    application         The name of the BLAST flavor that generated this data.
    version             Version of blast used.
    date                Date this data was generated.
    reference           Reference for blast.

    query               Name of query sequence.
    query_letters       Number of letters in the query sequence.  (int)
    
    database            Name of the database.
    database_sequences  Number of sequences in the database.  (int)
    database_letters    Number of letters in the database.  (int)

    """
    def __init__(self):
        self.application = ''
        self.version = ''
        self.date = ''
        self.reference = ''

        self.query = ''
        self.query_letters = None

        self.database = ''
        self.database_sequences = None
        self.database_letters = None

class Description(object):
    """Stores information about one hit in the descriptions section.

    Members:
    title           Title of the hit.
    score           Number of bits.  (int)
    bits            Bit score. (float)
    e               E value.  (float)
    num_alignments  Number of alignments for the same subject.  (int)
    
    """
    def __init__(self):
        self.title = ''
        self.score = None
        self.bits = None
        self.e = None
        self.num_alignments = None
    def __str__(self):
        return "%-66s %5s  %s" % (self.title, self.score, self.e)

class Alignment(object):
    """Stores information about one hit in the alignments section.

    Members:
    title      Name.
    hit_id     Hit identifier. (str)
    hit_def    Hit definition. (str)
    length     Length.  (int)
    hsps       A list of HSP objects.

    """
    def __init__(self):
        self.title = ''
        self.hit_id = ''
        self.hit_def = ''
        self.length = None
        self.hsps = []
    def __str__(self):
        lines = self.title.split('\n')
        lines.append("Length = %s\n" % self.length)
        return '\n           '.join(lines)

class HSP(object):
    """Stores information about one hsp in an alignment hit.

    Members:
    score           BLAST score of hit.  (float)
    bits            Number of bits for that score.  (float) 
    expect          Expect value.  (float) #CN (string)
    num_alignments  Number of alignments for same subject.  (int)
    identities      Number of identities (int) if using the XML parser.
                    Tuple of numer of identities/total aligned (int, int)
                    if using the (obsolete) plain text parser.
    positives       Number of positives (int) if using the XML parser.
                    Tuple of numer of positives/total aligned (int, int)
                    if using the (obsolete) plain text parser.
    gaps            Number of gaps (int) if using the XML parser.
                    Tuple of numer of gaps/total aligned (int, int) if
                    using the (obsolete) plain text parser.
    align_length    Length of the alignment. (int)
    strand          Tuple of (query, target) strand.
    frame           Tuple of 1 or 2 frame shifts, depending on the flavor.

    query           The query sequence.
    query_start     The start residue for the query sequence.  (1-based)
    query_end       The end residue for the query sequence.  (1-based)
    match           The match sequence.
    sbjct           The sbjct sequence.
    sbjct_start     The start residue for the sbjct sequence.  (1-based)
    sbjct_end       The end residue for the sbjct sequence.  (1-based)
    
    Not all flavors of BLAST return values for every attribute:
              score     expect     identities   positives    strand  frame
    BLASTP     X          X            X            X
    BLASTN     X          X            X            X          X
    BLASTX     X          X            X            X                  X
    TBLASTN    X          X            X            X                  X
    TBLASTX    X          X            X            X                 X/X

    Note: for BLASTX, the query sequence is shown as a protein sequence,
    but the numbering is based on the nucleotides.  Thus, the numbering
    is 3x larger than the number of amino acid residues.  A similar effect
    can be seen for the sbjct sequence in TBLASTN, and for both sequences
    in TBLASTX.

    Also, for negative frames, the sequence numbering starts from
    query_start and counts down.

    """
    def __init__(self):
        self.score = None
        self.bits = None
        self.expect = None
        self.num_alignments = None
        self.identities = (None, None)
        self.positives = (None, None)
        self.gaps = (None, None)
        self.align_length = None
        self.strand = (None, None)
        self.frame = ()
        
        self.query = ''
        self.query_start = None
        self.query_end = None
        self.match = ''
        self.sbjct = ''
        self.sbjct_start = None
        self.sbjct_end = None

    def __str__(self):
        lines = ["Score %i (%i bits), expectation %s, alignment length %i" \
                 % (self.score, self.bits, self.expect, self.align_length)]
        if self.align_length < 50:
            lines.append("Query:%s %s %s" % (str(self.query_start).rjust(8),
                                       str(self.query),
                                       str(self.query_end)))
            lines.append("               %s" \
                         % (str(self.match)))
            lines.append("Sbjct:%s %s %s" % (str(self.sbjct_start).rjust(8),
                                       str(self.sbjct),
                                       str(self.sbjct_end)))
        else:
            lines.append("Query:%s %s...%s %s" \
                         % (str(self.query_start).rjust(8),
                            str(self.query)[:45],
                            str(self.query)[-3:],
                            str(self.query_end)))
            lines.append("               %s...%s" \
                         % (str(self.match)[:45],
                            str(self.match)[-3:]))
            lines.append("Sbjct:%s %s...%s %s" \
                         % (str(self.sbjct_start).rjust(8),
                            str(self.sbjct)[:45],
                            str(self.sbjct)[-3:],
                            str(self.sbjct_end)))
        return "\n".join(lines)

class MultipleAlignment(object):
    """Holds information about a multiple alignment.

    Members:
    alignment  A list of tuples (name, start residue, sequence, end residue).

    The start residue is 1-based.  It may be blank, if that sequence is
    not aligned in the multiple alignment.

    """
    def __init__(self):
        self.alignment = []

class Round(object):
    """Holds information from a PSI-BLAST round.

    Members:
    number       Round number.  (int)
    reused_seqs  Sequences in model, found again.  List of Description objects.
    new_seqs     Sequences not found, or below threshold.  List of Description.
    alignments          A list of Alignment objects.
    multiple_alignment  A MultipleAlignment object.
    
    """
    def __init__(self):
        self.number = None
        self.reused_seqs = []
        self.new_seqs = []
        self.alignments = []
        self.multiple_alignment = None

class DatabaseReport(object):
    """Holds information about a database report.
    
    Members:
    database_name              List of database names.  (can have multiple dbs)
    num_letters_in_database    Number of letters in the database.  (int)
    num_sequences_in_database  List of number of sequences in the database.
    posted_date                List of the dates the databases were posted.
    ka_params                  A tuple of (lambda, k, h) values.  (floats)
    gapped                     # XXX this isn't set right!
    ka_params_gap              A tuple of (lambda, k, h) values.  (floats)

    """
    def __init__(self):
        self.database_name = []
        self.posted_date = []
        self.num_letters_in_database = []
        self.num_sequences_in_database = []
        self.ka_params = (None, None, None)
        self.gapped = 0
        self.ka_params_gap = (None, None, None)

class Parameters(object):
    """Holds information about the parameters.

    Members:
    matrix              Name of the matrix.
    gap_penalties       Tuple of (open, extend) penalties.  (floats)
    sc_match            Match score for nucleotide-nucleotide comparison
    sc_mismatch         Mismatch penalty for nucleotide-nucleotide comparison
    num_hits            Number of hits to the database.  (int)
    num_sequences       Number of sequences.  (int)
    num_good_extends    Number of extensions.  (int)
    num_seqs_better_e   Number of sequences better than e-value.  (int)
    hsps_no_gap         Number of HSP's better, without gapping.  (int)
    hsps_prelim_gapped  Number of HSP's gapped in prelim test.  (int)
    hsps_prelim_gapped_attemped  Number of HSP's attempted in prelim.  (int)
    hsps_gapped         Total number of HSP's gapped.  (int)
    query_length        Length of the query.  (int)
    query_id            Identifier of the query sequence. (str)
    database_length     Number of letters in the database.  (int)
    effective_hsp_length         Effective HSP length.  (int)
    effective_query_length       Effective length of query.  (int)
    effective_database_length    Effective length of database.  (int)
    effective_search_space       Effective search space.  (int)
    effective_search_space_used  Effective search space used.  (int)
    frameshift          Frameshift window.  Tuple of (int, float)
    threshold           Threshold.  (int)
    window_size         Window size.  (int)
    dropoff_1st_pass    Tuple of (score, bits).  (int, float)
    gap_x_dropoff       Tuple of (score, bits).  (int, float)
    gap_x_dropoff_final Tuple of (score, bits).  (int, float)
    gap_trigger         Tuple of (score, bits).  (int, float)
    blast_cutoff        Tuple of (score, bits).  (int, float)
    """
    def __init__(self):
        self.matrix = ''
        self.gap_penalties = (None, None)
        self.sc_match = None
        self.sc_mismatch = None
        self.num_hits = None
        self.num_sequences = None
        self.num_good_extends = None
        self.num_seqs_better_e = None
        self.hsps_no_gap = None
        self.hsps_prelim_gapped = None
        self.hsps_prelim_gapped_attemped = None
        self.hsps_gapped = None
        self.query_id = None
        self.query_length = None
        self.database_length = None
        self.effective_hsp_length = None
        self.effective_query_length = None
        self.effective_database_length = None
        self.effective_search_space = None
        self.effective_search_space_used = None
        self.frameshift = (None, None)
        self.threshold = None
        self.window_size = None
        self.dropoff_1st_pass = (None, None)
        self.gap_x_dropoff = (None, None)
        self.gap_x_dropoff_final = (None, None)
        self.gap_trigger = (None, None)
        self.blast_cutoff = (None, None)

#TODO - Add a friendly __str__ method to BLAST results    
class Blast(Header, DatabaseReport, Parameters):
    """Saves the results from a blast search.

    Members:
    descriptions        A list of Description objects.
    alignments          A list of Alignment objects.
    multiple_alignment  A MultipleAlignment object.
    + members inherited from base classes

    """
    def __init__(self):
        Header.__init__(self)
        DatabaseReport.__init__(self)
        Parameters.__init__(self)
        self.descriptions = []
        self.alignments = []
        self.multiple_alignment = None

class PSIBlast(Header, DatabaseReport, Parameters):
    """Saves the results from a blastpgp search.

    Members:
    rounds       A list of Round objects.
    converged    Whether the search converged.
    + members inherited from base classes

    """
    def __init__(self):
        Header.__init__(self)
        DatabaseReport.__init__(self)
        Parameters.__init__(self)
        self.rounds = []
        self.converged = 0


# Copyright 2000 by Bertrand Frottier .  All rights reserved.
# Revisions 2005-2006 copyright Michiel de Hoon
# Revisions 2006-2009 copyright Peter Cock
# This code is part of the Biopython distribution and governed by its
# license.  Please see the LICENSE file that should have been included
# as part of this package.
"""This module provides code to work with the BLAST XML output
following the DTD available on the NCBI FTP
ftp://ftp.ncbi.nlm.nih.gov/blast/documents/xml/NCBI_BlastOutput.dtd

Classes:
BlastParser         Parses XML output from BLAST (direct use discouraged).
                    This (now) returns a list of Blast records.
                    Historically it returned a single Blast record.
                    You are expected to use this via the parse or read functions.

_XMLParser          Generic SAX parser (private).

Functions:
parse               Incremental parser, this is an iterator that returns
                    Blast records.  It uses the BlastParser internally.
read                Returns a single Blast record. Uses the BlastParser internally.
"""
import xml.sax
from xml.sax.handler import ContentHandler

class _XMLparser(ContentHandler):
    """Generic SAX Parser

    Just a very basic SAX parser.

    Redefine the methods startElement, characters and endElement.
    """
    def __init__(self, debug=0):
        """Constructor

        debug - integer, amount of debug information to print
        """
        self._tag = []
        self._value = ''
        self._debug = debug
        self._debug_ignore_list = []

    def _secure_name(self, name):
        """Removes 'dangerous' from tag names

        name -- name to be 'secured'
        """
        # Replace '-' with '_' in XML tag names
        return name.replace('-', '_')
    
    def startElement(self, name, attr):
        """Found XML start tag

        No real need of attr, BLAST DTD doesn't use them

        name -- name of the tag

        attr -- tag attributes
        """
        self._tag.append(name)
        
        # Try to call a method (defined in subclasses)
        method = self._secure_name('_start_' + name)

        #Note could use try / except AttributeError
        #BUT I found often triggered by nested errors...
        if hasattr(self, method):
            eval("self.%s()" % method)
            if self._debug > 4:
                print "NCBIXML: Parsed:  " + method
        else:
            # Doesn't exist (yet)
            if method not in self._debug_ignore_list:
                if self._debug > 3:
                    print "NCBIXML: Ignored: " + method
                self._debug_ignore_list.append(method)

        #We don't care about white space in parent tags like Hsp,
        #but that white space doesn't belong to child tags like Hsp_midline
        if self._value.strip():
            raise ValueError("What should we do with %s before the %s tag?" \
                             % (repr(self._value), name))
        self._value = ""

    def characters(self, ch):
        """Found some text

        ch -- characters read
        """
        self._value += ch # You don't ever get the whole string

    def endElement(self, name):
        """Found XML end tag

        name -- tag name
        """
        # DON'T strip any white space, we may need it e.g. the hsp-midline
        
        # Try to call a method (defined in subclasses)
        method = self._secure_name('_end_' + name)
        #Note could use try / except AttributeError
        #BUT I found often triggered by nested errors...
        if hasattr(self, method):
            eval("self.%s()" % method)
            if self._debug > 2:
                print "NCBIXML: Parsed:  " + method, self._value
        else:
            # Doesn't exist (yet)
            if method not in self._debug_ignore_list:
                if self._debug > 1:
                    print "NCBIXML: Ignored: " + method, self._value
                self._debug_ignore_list.append(method)
        
        # Reset character buffer
        self._value = ''
        
class BlastParser(_XMLparser):
    """Parse XML BLAST data into a Record.Blast object

    All XML 'action' methods are private methods and may be:
    _start_TAG      called when the start tag is found
    _end_TAG        called when the end tag is found
    """

    def __init__(self, debug=0):
        """Constructor

        debug - integer, amount of debug information to print
        """
        # Calling superclass method
        _XMLparser.__init__(self, debug)
        
        self._parser = xml.sax.make_parser()
        self._parser.setContentHandler(self)
        
        # To avoid ValueError: unknown url type: NCBI_BlastOutput.dtd
        self._parser.setFeature(xml.sax.handler.feature_validation, 0)
        self._parser.setFeature(xml.sax.handler.feature_namespaces, 0)
        self._parser.setFeature(xml.sax.handler.feature_external_pes, 0)
        self._parser.setFeature(xml.sax.handler.feature_external_ges, 0)

        self.reset()

    def reset(self):
        """Reset all the data allowing reuse of the BlastParser() object"""
        self._records = []
        self._header = Header()
        self._parameters = Parameters()
        self._parameters.filter = None #Maybe I should update the class?

    def _start_Iteration(self):
        self._blast = Blast()
        pass

    def _end_Iteration(self):
        # We stored a lot of generic "top level" information
        # in self._header (an object of type Record.Header)
        self._blast.reference = self._header.reference
        self._blast.date = self._header.date
        self._blast.version = self._header.version
        self._blast.database = self._header.database
        self._blast.application = self._header.application

        # These are required for "old" pre 2.2.14 files
        # where only <BlastOutput_query-ID>, <BlastOutput_query-def>
        # and <BlastOutput_query-len> were used.  Now they
        # are suplemented/replaced by <Iteration_query-ID>,
        # <Iteration_query-def> and <Iteration_query-len>
        if not hasattr(self._blast, "query") \
        or not self._blast.query:
            self._blast.query = self._header.query
        if not hasattr(self._blast, "query_id") \
        or not self._blast.query_id:
            self._blast.query_id = self._header.query_id
        if not hasattr(self._blast, "query_letters") \
        or not self._blast.query_letters:
            self._blast.query_letters = self._header.query_letters

        # Hack to record the query length as both the query_letters and
        # query_length properties (as in the plain text parser, see
        # Bug 2176 comment 12):
        self._blast.query_length = self._blast.query_letters
        # Perhaps in the long term we should deprecate one, but I would
        # prefer to drop query_letters - so we need a transition period
        # with both.

        # Hack to record the claimed database size as database_length
        # (as well as in num_letters_in_database, see Bug 2176 comment 13):
        self._blast.database_length = self._blast.num_letters_in_database
        # TODO? Deprecate database_letters next?

        # Hack to record the claimed database sequence count as database_sequences
        self._blast.database_sequences = self._blast.num_sequences_in_database

        # Apply the "top level" parameter information
        self._blast.matrix = self._parameters.matrix
        self._blast.num_seqs_better_e = self._parameters.num_seqs_better_e
        self._blast.gap_penalties = self._parameters.gap_penalties
        self._blast.filter = self._parameters.filter
        self._blast.expect = self._parameters.expect
        self._blast.sc_match = self._parameters.sc_match
        self._blast.sc_mismatch = self._parameters.sc_mismatch

        #Add to the list
        self._records.append(self._blast)
        #Clear the object (a new empty one is create in _start_Iteration)
        self._blast = None

        if self._debug : "NCBIXML: Added Blast record to results"

    # Header
    def _end_BlastOutput_program(self):
        """BLAST program, e.g., blastp, blastn, etc.

        Save this to put on each blast record object
        """
        self._header.application = self._value.upper()

    def _end_BlastOutput_version(self):
        """version number and date of the BLAST engine.

        e.g. "BLASTX 2.2.12 [Aug-07-2005]" but there can also be
        variants like "BLASTP 2.2.18+" without the date.

        Save this to put on each blast record object
        """
        parts = self._value.split()
        #TODO - Check the first word starts with BLAST?

        #The version is the second word (field one)
        self._header.version = parts[1]
        
        #Check there is a third word (the date)
        if len(parts) >= 3:
            if parts[2][0] == "[" and parts[2][-1] == "]":
                self._header.date = parts[2][1:-1]
            else:
                #Assume this is still a date, but without the
                #square brackets
                self._header.date = parts[2]

    def _end_BlastOutput_reference(self):
        """a reference to the article describing the algorithm

        Save this to put on each blast record object
        """
        self._header.reference = self._value

    def _end_BlastOutput_db(self):
        """the database(s) searched

        Save this to put on each blast record object
        """
        self._header.database = self._value

    def _end_BlastOutput_query_ID(self):
        """the identifier of the query

        Important in old pre 2.2.14 BLAST, for recent versions
        <Iteration_query-ID> is enough
        """
        self._header.query_id = self._value

    def _end_BlastOutput_query_def(self):
        """the definition line of the query

        Important in old pre 2.2.14 BLAST, for recent versions
        <Iteration_query-def> is enough
        """
        self._header.query = self._value

    def _end_BlastOutput_query_len(self):
        """the length of the query

        Important in old pre 2.2.14 BLAST, for recent versions
        <Iteration_query-len> is enough
        """
        self._header.query_letters = int(self._value)

    def _end_Iteration_query_ID(self):
        """the identifier of the query
        """
        self._blast.query_id = self._value

    def _end_Iteration_query_def(self):
        """the definition line of the query
        """
        self._blast.query = self._value

    def _end_Iteration_query_len(self):
        """the length of the query
        """
        self._blast.query_letters = int(self._value)

##     def _end_BlastOutput_query_seq(self):
##         """the query sequence
##         """
##         pass # XXX Missing in Record.Blast ?

##     def _end_BlastOutput_iter_num(self):
##         """the psi-blast iteration number
##         """
##         pass # XXX TODO PSI

    def _end_BlastOutput_hits(self):
        """hits to the database sequences, one for every sequence
        """
        self._blast.num_hits = int(self._value)

##     def _end_BlastOutput_message(self):
##         """error messages
##         """
##         pass # XXX What to do ?

    # Parameters
    def _end_Parameters_matrix(self):
        """matrix used (-M)
        """
        self._parameters.matrix = self._value
        
    def _end_Parameters_expect(self):
        """expect values cutoff (-e)
        """
        # NOTE: In old text output there was a line:
        # Number of sequences better than 1.0e-004: 1
        # As far as I can see, parameters.num_seqs_better_e
        # would take the value of 1, and the expectation
        # value was not recorded.
        #
        # Anyway we should NOT record this against num_seqs_better_e
        self._parameters.expect = self._value

##     def _end_Parameters_include(self):
##         """inclusion threshold for a psi-blast iteration (-h)
##         """
##         pass # XXX TODO PSI
    
    def _end_Parameters_sc_match(self):
        """match score for nucleotide-nucleotide comparaison (-r)
        """
        self._parameters.sc_match = int(self._value)

    def _end_Parameters_sc_mismatch(self):
        """mismatch penalty for nucleotide-nucleotide comparaison (-r)
        """
        self._parameters.sc_mismatch = int(self._value)

    def _end_Parameters_gap_open(self):
        """gap existence cost (-G)
        """
        self._parameters.gap_penalties = int(self._value)

    def _end_Parameters_gap_extend(self):
        """gap extension cose (-E)
        """
        self._parameters.gap_penalties = (self._parameters.gap_penalties,
                                         int(self._value))

    def _end_Parameters_filter(self):
        """filtering options (-F)
        """
        self._parameters.filter = self._value

##     def _end_Parameters_pattern(self):
##         """pattern used for phi-blast search
##         """
##         pass # XXX TODO PSI

##     def _end_Parameters_entrez_query(self):
##         """entrez query used to limit search
##         """
##         pass # XXX TODO PSI

    # Hits
    def _start_Hit(self):
        self._blast.alignments.append(Alignment())
        self._blast.descriptions.append(Description())
        self._blast.multiple_alignment = []
        self._hit = self._blast.alignments[-1]
        self._descr = self._blast.descriptions[-1]
        self._descr.num_alignments = 0

    def _end_Hit(self):
        #Cleanup
        self._blast.multiple_alignment = None
        self._hit = None
        self._descr = None

    def _end_Hit_id(self):
        """identifier of the database sequence
        """
        self._hit.hit_id = self._value
        self._hit.title = self._value + ' '

    def _end_Hit_def(self):
        """definition line of the database sequence
        """
        self._hit.hit_def = self._value
        self._hit.title += self._value
        self._descr.title = self._hit.title

    def _end_Hit_accession(self):
        """accession of the database sequence
        """
        self._hit.accession = self._value
        self._descr.accession = self._value

    def _end_Hit_len(self):
        self._hit.length = int(self._value)

    # HSPs
    def _start_Hsp(self):
        #Note that self._start_Hit() should have been called
        #to setup things like self._blast.multiple_alignment
        self._hit.hsps.append(HSP())
        self._hsp = self._hit.hsps[-1]
        self._descr.num_alignments += 1
        self._blast.multiple_alignment.append(MultipleAlignment())
        self._mult_al = self._blast.multiple_alignment[-1]

    # Hsp_num is useless
    def _end_Hsp_score(self):
        """raw score of HSP
        """
        self._hsp.score = float(self._value)
        if self._descr.score == None:
            self._descr.score = float(self._value)

    def _end_Hsp_bit_score(self):
        """bit score of HSP
        """
        self._hsp.bits = float(self._value)
        if self._descr.bits == None:
            self._descr.bits = float(self._value)

    def _end_Hsp_evalue(self):
        """expect value value of the HSP
        """
        self._hsp.expect = float(self._value)
        if self._descr.e == None:
            self._descr.e = str(self._value)

    def _end_Hsp_query_from(self):
        """offset of query at the start of the alignment (one-offset)
        """
        self._hsp.query_start = int(self._value)

    def _end_Hsp_query_to(self):
        """offset of query at the end of the alignment (one-offset)
        """
        self._hsp.query_end = int(self._value)

    def _end_Hsp_hit_from(self):
        """offset of the database at the start of the alignment (one-offset)
        """
        self._hsp.sbjct_start = int(self._value)

    def _end_Hsp_hit_to(self):
        """offset of the database at the end of the alignment (one-offset)
        """
        self._hsp.sbjct_end = int(self._value)

##     def _end_Hsp_pattern_from(self):
##         """start of phi-blast pattern on the query (one-offset)
##         """
##         pass # XXX TODO PSI

##     def _end_Hsp_pattern_to(self):
##         """end of phi-blast pattern on the query (one-offset)
##         """
##         pass # XXX TODO PSI

    def _end_Hsp_query_frame(self):
        """frame of the query if applicable
        """
        self._hsp.frame = (int(self._value),)

    def _end_Hsp_hit_frame(self):
        """frame of the database sequence if applicable
        """
        self._hsp.frame += (int(self._value),)

    def _end_Hsp_identity(self):
        """number of identities in the alignment
        """
        self._hsp.identities = int(self._value)

    def _end_Hsp_positive(self):
        """number of positive (conservative) substitutions in the alignment
        """
        self._hsp.positives = int(self._value)

    def _end_Hsp_gaps(self):
        """number of gaps in the alignment
        """
        self._hsp.gaps = int(self._value)

    def _end_Hsp_align_len(self):
        """length of the alignment
        """
        self._hsp.align_length = int(self._value)

##     def _en_Hsp_density(self):
##         """score density
##         """
##         pass # XXX ???

    def _end_Hsp_qseq(self):
        """alignment string for the query
        """
        self._hsp.query = self._value

    def _end_Hsp_hseq(self):
        """alignment string for the database
        """
        self._hsp.sbjct = self._value

    def _end_Hsp_midline(self):
        """Formatting middle line as normally seen in BLAST report
        """
        self._hsp.match = self._value # do NOT strip spaces!
        assert len(self._hsp.match)==len(self._hsp.query)
        assert len(self._hsp.match)==len(self._hsp.sbjct)

    # Statistics
    def _end_Statistics_db_num(self):
        """number of sequences in the database
        """
        self._blast.num_sequences_in_database = int(self._value)

    def _end_Statistics_db_len(self):
        """number of letters in the database
        """
        self._blast.num_letters_in_database = int(self._value)

    def _end_Statistics_hsp_len(self):
        """the effective HSP length
        """
        self._blast.effective_hsp_length = int(self._value)

    def _end_Statistics_eff_space(self):
        """the effective search space
        """
        self._blast.effective_search_space = float(self._value)

    def _end_Statistics_kappa(self):
        """Karlin-Altschul parameter K
        """
        self._blast.ka_params = float(self._value)

    def _end_Statistics_lambda(self):
        """Karlin-Altschul parameter Lambda
        """
        self._blast.ka_params = (float(self._value),
                                 self._blast.ka_params)

    def _end_Statistics_entropy(self):
        """Karlin-Altschul parameter H
        """
        self._blast.ka_params = self._blast.ka_params + (float(self._value),)
    
def read(handle, debug=0):
   """Returns a single Blast record (assumes just one query).

   This function is for use when there is one and only one BLAST
   result in your XML file.

   Use the Bio.Blast.NCBIXML.parse() function if you expect more than
   one BLAST record (i.e. if you have more than one query sequence).

   """
   iterator = parse(handle, debug)
   try:
       first = iterator.next()
   except StopIteration:
       first = None
   if first is None:
       raise ValueError("No records found in handle")
   try:
       second = iterator.next()
   except StopIteration:
       second = None
   if second is not None:
       raise ValueError("More than one record found in handle")
   return first


def NCBIXML_parse(handle, debug=0):
    """Returns an iterator a Blast record for each query.

    handle - file handle to and XML file to parse
    debug - integer, amount of debug information to print

    This is a generator function that returns multiple Blast records
    objects - one for each query sequence given to blast.  The file
    is read incrementally, returning complete records as they are read
    in.

    Should cope with new BLAST 2.2.14+ which gives a single XML file
    for mutliple query records.

    Should also cope with XML output from older versions BLAST which
    gave multiple XML files concatenated together (giving a single file
    which strictly speaking wasn't valid XML)."""
    from xml.parsers import expat
    BLOCK = 1024
    MARGIN = 10 # must be at least length of newline + XML start
    XML_START = "<?xml"

    text = handle.read(BLOCK)
    pending = ""

    if not text:
        #NO DATA FOUND!
        raise ValueError("Your XML file was empty")
    
    while text:
        #We are now starting a new XML file
        if not text.startswith(XML_START):
            raise ValueError("Your XML file did not start with %s... "
                             "but instead %s" \
                             % (XML_START, repr(text[:20])))

        expat_parser = expat.ParserCreate()
        blast_parser = BlastParser(debug)
        expat_parser.StartElementHandler = blast_parser.startElement
        expat_parser.EndElementHandler = blast_parser.endElement
        expat_parser.CharacterDataHandler = blast_parser.characters

        expat_parser.Parse(text, False)
        while blast_parser._records:
            record = blast_parser._records[0]
            blast_parser._records = blast_parser._records[1:]
            yield record

        while True:
            #Read in another block of the file...
            text, pending = pending + handle.read(BLOCK), ""
            if not text:
                #End of the file!
                expat_parser.Parse("", True) # End of XML record
                break

            #Now read a little bit more so we can check for the
            #start of another XML file...
            pending = handle.read(MARGIN)

            if (text+pending).find("\n" + XML_START) == -1:
                # Good - still dealing with the same XML file
                expat_parser.Parse(text, False)        
                while blast_parser._records:
                    yield blast_parser._records.pop(0)
            else:
                # This is output from pre 2.2.14 BLAST,
                # one XML file for each query!
                
                # Finish the old file:
                text, pending = (text+pending).split("\n" + XML_START,1)
                pending = XML_START + pending

                expat_parser.Parse(text, True) # End of XML record
                while blast_parser._records:
                    yield blast_parser._records.pop(0)
               
                #Now we are going to re-loop, reset the
                #parsers and start reading the next XML file
                text, pending = pending, ""
                break

        #this was added because it seems that the Jython expat parser
        #was adding records later then the Python one
        while blast_parser._records:
            yield blast_parser._records.pop(0)
            
        #At this point we have finished the first XML record.
        #If the file is from an old version of blast, it may
        #contain more XML records (check if text=="").
        assert pending==""
        assert len(blast_parser._records) == 0
        
    #We should have finished the file!
    assert text==""
    assert pending==""
    assert len(blast_parser._records) == 0


# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


class AppHSP(HSP):
    """
    @summary : An AppHSP is a Bio.Blast.Record.HSP with many utilities to get correspondance between subject, query and alignment positions.
    """
    
    def __init__(self):
        self.hit_id = None
        self.hit_species = None
        self.strand = self.get_strand
        # aln is used to store an list representation of the alignment of each positon.
        # Example :
        #        alignment on strand -
        #        start residue for the query 5 ; end residue for the query 19 (1 gap in query)
        #        start residue for the subject 48 ; end residue for the subject 52 (1 gap in subject)
        #        3 identities and 4 positives
        #    aln['query']   = [  5,  6,  7,  8,  9,  10,  11,  12,  13, "", "", "", 14, 15, 16, 17, 18, 19]
        #    aln['match']   = [  =,  =,  =, SG, SG,  SG,   =,   =,   =, QG, QG, QG,  =,  =,  =,  +,  +,  +]
        #    aln['subject'] = [ 52, 52, 52, "", "",  "",  51,  51,  51, 50, 50, 50, 49, 49, 49, 48, 48, 48]
        self.aln = { "query":[],
                     "subject":[],
                     "match":[] }

    @staticmethod
    def from_HSP(hsp):
        """
        @summary : Returns an AppHSP from an HSP object.
        @param hsp : [Bio.Blast.Record.HSP] The HSP.
        @return : [AppHSP]
        """
        app_hsp = AppHSP()
        app_hsp.score = hsp.score
        app_hsp.bits = hsp.bits
        app_hsp.expect = hsp.expect
        app_hsp.num_alignments = hsp.num_alignments
        app_hsp.identities = hsp.identities
        app_hsp.positives = hsp.positives
        app_hsp.gaps = hsp.gaps
        app_hsp.align_length = hsp.align_length
        app_hsp.frame = hsp.frame
        app_hsp.strand = app_hsp.get_strand()
        app_hsp.query = hsp.query
        app_hsp.query_start = hsp.query_start
        app_hsp.query_end = hsp.query_end
        app_hsp.match = hsp.match
        app_hsp.sbjct = hsp.sbjct
        app_hsp.sbjct_start = hsp.sbjct_start
        app_hsp.sbjct_end = hsp.sbjct_end
        return app_hsp

    def get_subject_pos(self, aln_pos):
        """
        @summary : Returns the position on subject (in aa) corresponding to position in alignment.
        @param aln_pos : [int] Position in alignment.
        @return : [int]
        """
        return self.aln["subject"][aln_pos]

    def get_query_pos(self, aln_pos):
        """
        @summary : Returns the position on query (in nt) corresponding to position in alignment.
        @param aln_pos : [int] Position in alignment.
        @return : [int]        
        """
        return self.aln["query"][aln_pos]

    def get_aln_pos_from_query(self, query_pos):
        """
        @summary : Returns the position on alignment corresponding to position in query.
        @param query_pos : [int] Position in query (in nt).
        @return : [int]        
        """
        aln_pos = self.aln["query"].index( query_pos )
        if aln_pos == -1:
            raise Exception( "The Query position '" + str(query_pos) + "' is not implicated in the HSP." )
        return aln_pos

    def get_strand(self):
        """"
        @summary : Returns strand of the query alignment.
        @return : [str]
        """
        strand = "+"
        if (self.frame[0] < 0):
            strand = "-"
        return strand
    
    def get_codon_pos(self, aln_pos):
        """
        @summary : Returns the position on codon for the nucleotid in the specified alignment position.
        @param aln_pos : [int] Position in alignment.
        @return : [int]
        @warning : self.sbjct_start must be inferior than self.sbjct_end.
        """
        codon_pos = None
        if aln_pos != (len(self.aln["subject"])-1) and self.aln["subject"][aln_pos] == self.aln["subject"][aln_pos+1]:
            if aln_pos != 0 and self.aln["subject"][aln_pos] == self.aln["subject"][aln_pos-1]:
                codon_pos = 2
            else:
                codon_pos = 1
        else:
            codon_pos = 3
        return codon_pos

    def _tri_pos(self, pos):
        """
        @summary : Returns the three query positions for the codon which strat on pos in stranded alignment.
        @retruns : [list] The three position in order.
        """
        return { 'next':(pos+3), 'codon':[pos, pos+1, pos+2] }

    def _tri_rev_pos(self, pos):
        """
        @summary : Returns the three query positions for the codon which strat on pos in reverse stranded alignment.
        @retruns : [list] The three position in order.
        """
        return { 'next':(pos-3), 'codon':[pos, pos-1, pos-2] }

    def set_pos(self):
        """
        @summary : set aln["query"], aln["match"] and aln["subject"].
        """
        query_pos = None
        codon_positions = None
        subject_pos = self.sbjct_start
        if self.strand == "+":
            query_pos = self.query_start
            codon_positions = self._tri_pos
        else:
            query_pos = self.query_end
            codon_positions = self._tri_rev_pos

        for align_idx, align_char in enumerate( self.match ):
            if align_char == '+': # conservation
                positions = codon_positions(query_pos)
                query_pos = positions["next"]
                self.aln["query"] += positions["codon"]
                self.aln["subject"] += [subject_pos, subject_pos, subject_pos]
                subject_pos += 1
                self.aln["match"] += ["+", "+", "+"]
            elif align_char.isalpha(): # identity
                positions = codon_positions(query_pos)
                query_pos = positions["next"]
                self.aln["query"] += positions["codon"]
                self.aln["subject"] += [subject_pos, subject_pos, subject_pos]
                subject_pos += 1
                self.aln["match"] += ["=", "=", "="] 
            else:
                if self.query[align_idx] == "-": # gap in query
                    self.aln["query"] += ["", "", ""]
                    self.aln["subject"] += [subject_pos, subject_pos, subject_pos]
                    subject_pos += 1
                    self.aln["match"] += ["QG", "QG", "QG"]
                elif self.sbjct[align_idx] == "-": # gap in subject
                    positions = codon_positions(query_pos)
                    query_pos = positions["next"]
                    self.aln["query"] += positions["codon"]
                    self.aln["subject"] += ["", "", ""]
                    self.aln["match"] += ["SG", "SG", "SG"]
                else: # mismatch
                    positions = codon_positions(query_pos)
                    query_pos = positions["next"]
                    self.aln["query"] += positions["codon"]
                    self.aln["subject"] += [subject_pos, subject_pos, subject_pos]
                    subject_pos += 1
                    self.aln["match"] += ["-", "-", "-"]


# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


class Sequence:
    def __init__(self, id, string, description=None, quality=None):
        """
        @param id : [str] Id of the sequence.
        @param string : [str] Sequence of the sequence.
        @param description : [str] The sequence description.
        @param quality : [str] The quality of the sequence (same length as string).
        """
        self.id = id
        self.description = description
        self.string = string
        self.quality = quality


class NucleicSequence(Sequence):    
    complement_rules = {'A':'T','T':'A','G':'C','C':'G','U':'A','N':'N','W':'W','S':'S','M':'K','K':'M','R':'Y','Y':'R','B':'V','V':'B','D':'H','H':'D',
                        'a':'t','t':'a','g':'c','c':'g','u':'a','n':'n','w':'w','s':'s','m':'k','k':'m','r':'y','y':'r','b':'v','v':'b','d':'h','h':'d'}

    def revcom( self ):
        """
        @summary : Reverse complement the sequence.
        """
        self.string = "".join([NucleicSequence.complement_rules[base] for base in self.string[::-1]])

    def complement( self ):
        """
        @summary : Complement the sequence.
        """
        self.string = "".join([NucleicSequence.complement_rules[base] for base in list(self.string)])


class Codon(NucleicSequence):
    genetic_code = {'GTA':'Val', 'GTC':'Val', 'GTG':'Val', 'GTT':'Val', 'GTN':'Val', 'GTW':'Val', 'GTS':'Val', 'GTM':'Val', 'GTK':'Val', 'GTR':'Val', 'GTY':'Val', 'GTB':'Val', 'GTD':'Val', 'GTH':'Val', 'GTV':'Val', 'GUA':'Val', 'GUC':'Val', 'GUG':'Val', 'GUU':'Val', 'GUN':'Val', 'GUW':'Val', 'GUS':'Val', 'GUM':'Val', 'GUK':'Val', 'GUR':'Val', 'GUY':'Val', 'GUB':'Val', 'GUD':'Val', 'GUH':'Val', 'GUV':'Val',
                    'TGC':'Cys', 'TGT':'Cys', 'TGY':'Cys', 'UGC':'Cys', 'UGU':'Cys', 'UGY':'Cys',
                    'GAC':'Asp', 'GAT':'Asp', 'GAU':'Asp', 'GAY':'Asp',
                    'TAA':'*', 'TAG':'*', 'TAR':'*', 'TGA':'*', 'TRA':'*', 'UAA':'*', 'UAG':'*', 'UAR':'*', 'UGA':'*', 'URA':'*',
                    'TTC':'Phe', 'TTT':'Phe', 'TTY':'Phe', 'UUC':'Phe', 'UUU':'Phe', 'UUY':'Phe',
                    'ATG':'Met', 'AUG':'Met',
                    'CTA':'Leu', 'CTC':'Leu', 'CTG':'Leu', 'CTT':'Leu', 'CTN':'Leu', 'CTW':'Leu', 'CTS':'Leu', 'CTM':'Leu', 'CTK':'Leu', 'CTR':'Leu', 'CTY':'Leu', 'CTB':'Leu', 'CTD':'Leu', 'CTH':'Leu', 'CTV':'Leu', 'CUA':'Leu', 'CUC':'Leu', 'CUG':'Leu', 'CUU':'Leu', 'CUN':'Leu', 'CUW':'Leu', 'CUS':'Leu', 'CUM':'Leu', 'CUK':'Leu', 'CUR':'Leu', 'CUY':'Leu', 'CUB':'Leu', 'CUD':'Leu', 'CUH':'Leu', 'CUV':'Leu', 'TTA':'Leu', 'TTG':'Leu', 'TTR':'Leu', 'UUA':'Leu', 'UUG':'Leu', 'UUR':'Leu', 'YTA':'Leu', 'YTG':'Leu', 'YTR':'Leu', 'YUA':'Leu', 'YUG':'Leu', 'YUR':'Leu',
                    'AAC':'Asn', 'AAT':'Asn', 'AAU':'Asn', 'AAY':'Asn',
                    'TAC':'Tyr', 'TAT':'Tyr', 'TAY':'Tyr', 'UAC':'Tyr', 'UAU':'Tyr', 'UAY':'Tyr',
                    'ATA':'Ile', 'ATC':'Ile', 'ATT':'Ile', 'ATW':'Ile', 'ATM':'Ile', 'ATY':'Ile', 'ATH':'Ile', 'AUA':'Ile', 'AUC':'Ile', 'AUU':'Ile', 'AUW':'Ile', 'AUM':'Ile', 'AUY':'Ile', 'AUH':'Ile',
                    'CAA':'Gln', 'CAG':'Gln', 'CAR':'Gln',
                    'ACA':'Thr', 'ACC':'Thr', 'ACG':'Thr', 'ACT':'Thr', 'ACU':'Thr', 'ACN':'Thr', 'ACW':'Thr', 'ACS':'Thr', 'ACM':'Thr', 'ACK':'Thr', 'ACR':'Thr', 'ACY':'Thr', 'ACB':'Thr', 'ACD':'Thr', 'ACH':'Thr', 'ACV':'Thr',
                    'GGA':'Gly', 'GGC':'Gly', 'GGG':'Gly', 'GGT':'Gly', 'GGU':'Gly', 'GGN':'Gly', 'GGW':'Gly', 'GGS':'Gly', 'GGM':'Gly', 'GGK':'Gly', 'GGR':'Gly', 'GGY':'Gly', 'GGB':'Gly', 'GGD':'Gly', 'GGH':'Gly', 'GGV':'Gly',
                    'CAC':'His', 'CAT':'His', 'CAU':'His', 'CAY':'His',
                    'TGG':'Trp', 'UGG':'Trp',
                    'GAA':'Glu', 'GAG':'Glu', 'GAR':'Glu',
                    'AGC':'Ser', 'AGT':'Ser', 'AGU':'Ser', 'AGY':'Ser', 'TCA':'Ser', 'TCC':'Ser', 'TCG':'Ser', 'TCT':'Ser', 'TCN':'Ser', 'TCW':'Ser', 'TCS':'Ser', 'TCM':'Ser', 'TCK':'Ser', 'TCR':'Ser', 'TCY':'Ser', 'TCB':'Ser', 'TCD':'Ser', 'TCH':'Ser', 'TCV':'Ser', 'UCA':'Ser', 'UCC':'Ser', 'UCG':'Ser', 'UCU':'Ser', 'UCN':'Ser', 'UCW':'Ser', 'UCS':'Ser', 'UCM':'Ser', 'UCK':'Ser', 'UCR':'Ser', 'UCY':'Ser', 'UCB':'Ser', 'UCD':'Ser', 'UCH':'Ser', 'UCV':'Ser',
                    'CCA':'Pro', 'CCC':'Pro', 'CCG':'Pro', 'CCT':'Pro', 'CCU':'Pro', 'CCN':'Pro', 'CCW':'Pro', 'CCS':'Pro', 'CCM':'Pro', 'CCK':'Pro', 'CCR':'Pro', 'CCY':'Pro', 'CCB':'Pro', 'CCD':'Pro', 'CCH':'Pro', 'CCV':'Pro',
                    'GCA':'Ala', 'GCC':'Ala', 'GCG':'Ala', 'GCT':'Ala', 'GCU':'Ala', 'GCN':'Ala', 'GCW':'Ala', 'GCS':'Ala', 'GCM':'Ala', 'GCK':'Ala', 'GCR':'Ala', 'GCY':'Ala', 'GCB':'Ala', 'GCD':'Ala', 'GCH':'Ala', 'GCV':'Ala',
                    'AGA':'Arg', 'AGG':'Arg', 'AGR':'Arg', 'CGA':'Arg', 'CGC':'Arg', 'CGG':'Arg', 'CGT':'Arg', 'CGU':'Arg', 'CGN':'Arg', 'CGW':'Arg', 'CGS':'Arg', 'CGM':'Arg', 'CGK':'Arg', 'CGR':'Arg', 'CGY':'Arg', 'CGB':'Arg', 'CGD':'Arg', 'CGH':'Arg', 'CGV':'Arg', 'MGA':'Arg', 'MGG':'Arg', 'MGR':'Arg',
                    'AAA':'Lys', 'AAG':'Lys', 'AAR':'Lys'}

    def trad( self ):
        """
        @summary : Returns the 3 letters amino acid code for the codon.
        @return : [str]
        """
        try:
            return Codon.genetic_code[self.string.upper()]
        except:
            return '?'

    def __str__(self):
        return self.string


# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


class Reference:
    def __init__(self, id, name, sequence, description=None) :
        self.id          = id
        self.name        = name
        self.sequence    = sequence
        self.description = description if (description is not None) else ""

class Region:
    def __init__(self, start, end, strand, reference):
        self.start     = int(start)
        self.end       = int(end)
        self.strand    = strand
        self.reference = reference # object reference
        
    def length( self ):
        return( self.end - self.start + 1 )


class BiologicalRegion( Region ):
    def __init__(self, start, end, strand, reference, id, name):
        Region.__init__( self, start, end, strand, reference )
        self.id   = id
        self.name = name
                
class Exon( BiologicalRegion ):
    def __str__(self):
        string = ""
        if self.strand == "+":
            string = self.id + " : " + self.reference.name + "[" + str(self.start) + ":" + str(self.end) + "]"
        else:
            string = self.id + " : " + self.reference.name + "[" + str(self.end) + ":" + str(self.start) + "]"
        return string

    def getPosOnRef( self, region_pos ):
        reference_pos = None
        if self.strand == '+':
            reference_pos = region_pos + self.start - 1
        else:
            reference_pos = self.end - region_pos + 1
        return reference_pos

class CDS( BiologicalRegion ):
    def __str__(self):
        string = ""
        if self.strand == "+":
            string = self.id + " : " + self.reference.name + "[" + str(self.start) + ":" + str(self.end) + "]"
        else:
            string = self.id + " : " + self.reference.name + "[" + str(self.end) + ":" + str(self.start) + "]"
        return string

    def getPosOnRef( self, region_pos ):
        reference_pos = None
        if self.strand == '+':
            reference_pos = region_pos + self.start - 1
        else:
            reference_pos = self.end - region_pos + 1
        return reference_pos

class Transcript:
    def __init__(self, id, name, gene, strand, regions):
        self.id      = id
        self.name    = name
        self.gene    = gene # object gene
        self.strand  = strand
        self.regions = regions # objects Exon (order : start increase if strand + OR start decrease if strand -)
        
    def __str__(self):
        string = ""
        for current_region in self.regions:
            string += " {" + current_region.__class__.__name__ + " " + current_region.__str__() + "} "
        return self.id + " (" + self.strand + ") : [ " + string + " ]"
 
    def addRegion( self, exon ):
        if exon.strand == "+":
            if len(self.regions) == 0 or exon.start > self.regions[-1].start:
                self.regions.append( exon )
            else:
                self.regions.insert( 0, exon )
        else: # strand -
            if len(self.regions) == 0 or exon.end < self.regions[0].end:
                self.regions.append( exon )
            else:
                self.regions.insert( 0, exon )
    
    def length(self):
        length = 0
        for current_region in self.regions:
            length += current_region.length()
        return length

    def getEltByPos( self, transcript_pos ):
        walk_pos = 0
        region_idx = 0
        find = False
        pos_on_region = None
        while not find:
            if walk_pos <= transcript_pos and (walk_pos + self.regions[region_idx].length()) >= transcript_pos:
                find = True
                pos_on_region = transcript_pos - walk_pos
            else:
                walk_pos += self.regions[region_idx].length()
                region_idx += 1
        return self.regions[region_idx], pos_on_region
    
    def getPosOnRef( self, transcript_pos ):
        reference_pos = None
        region, region_pos = self.getEltByPos( transcript_pos )
        return region.getPosOnRef( region_pos )

class Protein:
    def __init__(self, id, name, transcript, strand, regions, function=None, evidence=None):
        self.id      = id
        self.name    = name
        self.transcript = transcript # object transcript
        self.strand  = strand
        self.regions = regions # objects CDS
        self.function = None
        self.evidence = None
        
    def __str__(self):
        string = ""
        for current_region in self.regions:
            string += " {" + current_region.__class__.__name__ + " " + current_region.__str__() + "} "
        return self.id + " (" + self.strand + ") : [ " + string + " ]"

    def addRegion( self, cds ):
        if cds.strand == "+":
            if len(self.regions) == 0 or cds.start > self.regions[-1].start:
                self.regions.append( cds )
            else:
                self.regions.insert( 0, cds )
        else: # strand -
            if len(self.regions) == 0 or cds.end < self.regions[0].end:
                self.regions.append( cds )
            else:
                self.regions.insert( 0, cds )
    
    def length(self, aa_unit=True):
        length = 0
        for current_region in self.regions:
            length += current_region.length()
        if aa_unit:
            return length/3
        return length

    def getEltByPos( self, protein_pos, codon_pos ):
        general_cds_pos = self.getPosOnGlobalCDS( protein_pos, codon_pos )
        walk_pos = 0
        region_idx = 0
        find = False
        pos_on_region = None
        while not find:
            if walk_pos <= general_cds_pos and (walk_pos + self.regions[region_idx].length()) >= general_cds_pos:
                find = True
                pos_on_region = general_cds_pos - walk_pos
            else:
                walk_pos += self.regions[region_idx].length()
                region_idx += 1
        return self.regions[region_idx], pos_on_region    
    
    def getPosOnRef( self, protein_pos, codon_pos ):
        reference_pos = None
        region, region_pos = self.getEltByPos( proetin_pos, codon_pos )
        return region.getPosOnRef( region_pos )

    def getPosOnGlobalCDS( self, protein_pos, codon_pos ):
        return ((protein_pos-1)*3) + codon_pos

    def getFivePrimUTR( self ):
        first_CDS_start = self.regions[0].getPosOnRef(1)
        find = False
        five_prim_UTR_length = 0
        exon_idx = 0
        while not find:
            if first_CDS_start >= self.transcript.regions[exon_idx].start and first_CDS_start <= self.transcript.regions[exon_idx].end:
                find = True
                if self.transcript.regions[exon_idx].strand == "+":
                    five_prim_UTR_length += first_CDS_start - self.transcript.regions[exon_idx].start
                else:
                    five_prim_UTR_length += self.transcript.regions[exon_idx].end - first_CDS_start
            else:
                five_prim_UTR_length += self.transcript.regions[exon_idx].length()
                exon_idx += 1
        return five_prim_UTR_length


# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


class FastaIO:
    def __init__( self, filepath, mode="r" ):
        """
        @param filepath : [str] The filepath.
        @param mode : [str] Mode to open the file ('r', 'w', 'a').
        """
        self.filepath = filepath
        self.mode = mode
        self.file_handle = open( filepath, mode )
        self.current_line_nb = 1
        self.current_line = None
    
    def __del__( self ):
        self.close()
        
    def close( self ) :
        if hasattr(self, 'file_handle') and self.file_handle is not None:
            self.file_handle.close()
            self.file_handle = None
            self.current_line_nb = None
    
    def __iter__( self ):
        seq_id = None
        seq_desc = None
        seq_str = None
        try:
            for line in self.file_handle:
                line = line.rstrip()
                self.current_line_nb += 1
                if line.startswith('>'):
                    if seq_id is not None:
                        seq_record = Sequence( seq_id, seq_str, seq_desc )
                        yield seq_record
                    # New seq
                    fields = line[1:].split("\s", 1)
                    seq_id = fields[0]
                    seq_desc = fields[1] if len(fields) == 2 else None
                    seq_str = ""
                else:
                    seq_str += line
            if seq_id is not None:
                seq_record = Sequence( seq_id, seq_str, seq_desc )
                yield seq_record
        except:
            raise IOError( "The line " + str(self.current_line_nb) + " in '" + self.filepath + "' cannot be parsed by " + self.__class__.__name__ + "." )
            
    def next_seq( self ):
        """
        @summary : Returns the next sequence.
        @return : [Sequence] The next sequence.
        """
        seq_record = None
        line = ""
        try:
            # First line in file
            if self.current_line_nb == 1:
                self.next_id = self.file_handle.readline().strip()
                self.current_line_nb += 1
            # Sequence
            seq_str = ""
            while not line.startswith('>'):
                seq_str += line.strip()
                line = self.file_handle.readline()
                if not line:
                    break
                self.current_line_nb += 1
            fields = self.next_id[1:].split(None, 1)
            seq_id = fields[0]
            seq_desc = fields[1] if len(fields) == 2 else None
            seq_record = Sequence( seq_id, seq_str, seq_desc )
            self.next_id = line # next seq_id
        except:
            raise IOError( "The line " + str(self.current_line_nb) + " in '" + self.filepath + "' cannot be parsed by " + self.__class__.__name__ + ".\n"
                            + "content : " + line )
        return seq_record
    
    def write( self, sequence_record ):
        self.file_handle.write( self.seqToFastaLine(sequence_record) + "\n" )
    
    def seqToFastaLine( self, sequence ):
        """
        @summary : Returns the sequence in fasta format.
        @param sequence : [Sequence] The sequence to process.
        @return : [str] The sequence.
        """
        header = ">" + sequence.id + (" " + sequence.description if sequence.description is not None else "")
        return header + "\n" + sequence.string


# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


class GFF3Record:
    """
     @summary : Record for GFF3.
    """
    def __init__( self ):
        self.seq_id = None
        self.source = None
        self.type = None
        self.start = None
        self.end = None
        self.score = None
        self.strand = None
        self.phase = None
        self.attributes = None
    
    def setAttribute( self, tag, value ):
        """
         @summary : Create or replace an attribute tag.
          @param tag : tag of the attribute.
          @param value : value of the attribute tag.
        """
        cleaned_tag = GFF3Record._getCleanedAttribute(tag)
        cleaned_value = GFF3Record._getCleanedAttribute(str(value))
        if self.attributes is not None :
            self.attributes[cleaned_tag] = cleaned_value
        else:
            raise "The attibute 'Attributes' is not initialized."

    def addToAttribute( self, tag, value ):
        """
         @summary : Add one value on an existing tag.
          @param tag : tag of the attribute.
          @param value : value to add of the tag.
        """
        cleaned_tag = GFF3Record._getCleanedAttribute(tag)
        cleaned_value = GFF3Record._getCleanedAttribute(str(value))
        if self.attributes is not None :
            if self.attributes.has_key( cleaned_tag ):
                self.attributes[cleaned_tag] = self.attributes[cleaned_tag] + ","  + cleaned_value
            else:
                self.attributes[cleaned_tag] = cleaned_value
        else:
            raise "The attibute 'Attributes' is not initialized."
    
    def _attributesToGff( self ):
        """
         @summary : Returns a string in GFF3 format attributes field from the GFF3Record.attributes.
         @return : [str] the attributes in GFF3 format.
        """
        gff_string = ""
        for tag in self.attributes:
            gff_string = gff_string + tag + "=" + str(self.attributes[tag]) + ";"
        
        return gff_string[:-1]
         
    def toGff( self ):
        """
         @summary : Returns a string in GFF3 format from the GFF3Record object.
         @return : [str] the line in GFF3 format.
        """
        gff_record = "\t".join( [self.seq_id, self.source, self.type, str(self.start), str(self.end), str(self.score), self.strand, str(self.phase), self._attributesToGff()] )
        
        return gff_record

    def attributesToStr( self, tag ):
        """
         @summary : Returns the attribute value in human readable format.
          @param tag : [str] the attribute tag.
         @return : [str] the human readable value.
         @see : RFC 3986 Percent-Encoding
        """
        cleaned_tag = GFF3Record._getCleanedAttribute(tag)
        if self.attributes.has_key( cleaned_tag ):
            readable_value = self.attributes[cleaned_tag].replace('%3B', ';')
            readable_value = readable_value.replace('%2C', ',')
            redable_value = readable_value.replace('%3D', '=')
            return redable_value
        else:
            raise "The attibute 'Attributes' is not initialized."      
    
    @staticmethod
    def _getCleanedAttribute( dirty_value ):
        """
         @summary : Returns value after GFF3 attribute cleaning. cleanning :
            - URL escaping rules are used for tags or values containing the following characters: ",=;".
            - Spaces are allowed in this field, but tabs must be replaced with the space.
            - Quotes ' and " are deleted.
          @param dirty_value : [str] value before cleaning.
         @return : [str] the clean value.
         @see : RFC 3986 Percent-Encoding
        """
        cleaned_value = dirty_value.replace(';', '%3B')
        cleaned_value = cleaned_value.replace(',', '%2C')
        cleaned_value = cleaned_value.replace('=', '%3D')
        cleaned_value = cleaned_value.replace('\t', ' ')
        cleaned_value = cleaned_value.replace("'", '')
        cleaned_value = cleaned_value.replace('"', '')
        
        return cleaned_value

    @staticmethod
    def fromGff( line ):
        """
         @summary : Returns a GFF3Record from a GFF3 line.
          @param line : line of the GFF.
         @return : [GFF3Record] the record.
        """
        gff_record = GFF3Record()
        line_fields = line.split("\t")
        gff_record.seq_id = line_fields[0]
        gff_record.source = line_fields[1]
        gff_record.type = line_fields[2]
        gff_record.start = int(line_fields[3])
        gff_record.end = int(line_fields[4])
        gff_record.score = line_fields[5]
        gff_record.strand = line_fields[6]
        gff_record.phase = line_fields[7]
        # Parse attributes
        gff_record.attributes = dict()
        attributes = "\t".join(line_fields[8:])
        if attributes.strip().endswith(";"): # if attributes end with ';'
            attributes = attributes.strip()[:-1]
        attributes_array = attributes.split(";")
        cleaned_attributes = list()
        for attribute in attributes_array:
            if not "=" in attribute:
                cleaned_attributes[len(cleaned_attributes)-1] += " %3B " + attribute
            else:
                cleaned_attributes.append(attribute)
        for attribute in cleaned_attributes:
            matches = re.match("^([^=]+)=(.*)", attribute)
            tag = matches.group(1)
            values = matches.group(2).split(',')
            for current_val in values:
                gff_record.addToAttribute(tag, current_val)
        return gff_record


class GFF3IO:
    """
     @summary : Specific handler for GFF3 file.
    """
    def __init__( self, file_path, mode="r" ):
        """
        @param file_path : [str] The filepath.
        @param mode : [str] Mode to open the file ('r', 'w', 'a').
        """
        self._path = file_path
        self._handle = open( file_path, mode )
        self._line = 1
        if mode == "w":
            self._handle.write( "##gff-version 3\n" )
    
    def __del__( self ):
        self.close()

    def close( self ) :
        if  hasattr(self, '_handle') and self._handle is not None:
            self._handle.close()
            self._handle = None
            self._line = None
            
    def __iter__( self ):
        for line in self._handle:
            line = line.rstrip()
            self._line += 1
            if line.startswith('#') :
                continue
            try:
                gff_record = GFF3Record.fromGff(line)
            except:
                raise IOError( "The line " + str(self._line) + " in '" + self._path + "' cannot be parsed by " + self.__class__.__name__ + ".\n" +
                               "Line content : " + line )
            else:
                yield gff_record

    def write( self, gff_record ):
        """
         @summary : Write one line on gff file.
          @param gff_record : [GFF3Record] the object to write.
        """
        self._handle.write( gff_record.toGff() + "\n" )
        self._line += 1


# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


class GTFI(object):
    def __init__( self, filepath ):
        """
        @param filepath : [str] The filepath.
        """
        self.filepath = filepath
        self.current_line = None
        self.current_line_nb = None
        self.file_handle = None

    def __del__( self ):
        self.close()
    
    def __iter__( self ):
        for line in self.file_handle:
            line = line.rstrip()
            self.current_line = line
            self.current_line_nb += 1
            if line.startswith('#') :
                continue
            try:
                record = self._parse_line()
            except:
                raise IOError( "The line " + str(self.current_line_nb) + " in '" + self.filepath + "' cannot be parsed by " + self.__class__.__name__ + ".\n" +
                                "Line content : " + self.current_line )
            else:
                yield record
      
    def open(self):
        self.file_handle = open( self.filepath )
        self.current_line = ""
        self.current_line_nb = 0

    def close(self):
        if hasattr(self, 'file_handle') and self.file_handle is not None:
            self.file_handle.close()
            self.file_handle = None
            self.current_line = None
            self.current_line_nb = None

    def load_model( self ):
        """
        @summary : Returns the structure model from the GTF.
        @return : [list] Two dict : transcripts and proteins. You can use transcript or protein ID to 
                  access at the corresponding object and her gene, exon, ....
        @see : bioStructure.
        """
        transcripts = dict()
        proteins = dict()
        prev_chromosome_name = ""
        chromosome = None
        current_gene = None
        
        self.open()
        for record in self:
            if prev_chromosome_name != record["region"]:
                chromosome = Reference( "", record["region"], "", "" )
                prev_chromosome_name = record["region"]
            if record["feature"] == "exon":
                # Transcript
                transcript = None
                if not transcripts.has_key(record["attr"]["transcript_id"]):
                    if current_gene is None:
                        gene_name = record["attr"]["gene_name"] if record["attr"].has_key("gene_name") else "Gene_noName"
                        current_gene = BiologicalRegion( -1, -1, record["strand"], chromosome, record["attr"]["gene_id"], gene_name )
                    transcript_name = record["attr"]["transcript_name"] if record["attr"].has_key("transcript_name") else "Transcript_noName"
                    transcript = Transcript( record["attr"]["transcript_id"], transcript_name, current_gene, record["strand"], list() )
                    transcripts[record["attr"]["transcript_id"]] = transcript
                else:
                    transcript = transcripts[record["attr"]["transcript_id"]]
                # Exon
                exon_id = record["attr"]["exon_id"] if record["attr"].has_key("exon_id") else record["attr"]["transcript_id"] + "_" + str(len(transcript.regions))
                exon = Exon( record["start"], record["end"], record["strand"], chromosome, exon_id, str(len(transcript.regions)) )
                transcript.addRegion( exon )
            elif record["feature"] == "CDS":
                # Transcript
                transcript = None
                if not transcripts.has_key(record["attr"]["transcript_id"]):
                    if current_gene is None:
                        gene_name = record["attr"]["gene_name"] if record["attr"].has_key("gene_name") else "Gene_noName"
                        current_gene = BiologicalRegion( -1, -1, record["strand"], chromosome, record["attr"]["gene_id"], gene_name )
                    transcript_name = record["attr"]["transcript_name"] if record["attr"].has_key("transcript_name") else "Transcript_noName"
                    transcript = Transcript( record["attr"]["transcript_id"], transcript_name, current_gene, record["strand"], list() )
                    transcripts[record["attr"]["transcript_id"]] = transcript
                else:
                    transcript = transcripts[record["attr"]["transcript_id"]]
                # Protein
                protein_id = record["attr"]["protein_id"]
                protein = None
                if not proteins.has_key(protein_id):
                    protein = Protein( protein_id, "unknown", transcript, record["strand"], list() )
                    proteins[protein_id] = protein
                else:
                    protein = proteins[protein_id]
                # CDS
                cds = CDS( record["start"], record["end"], record["strand"], chromosome, protein_id + "_" + str(len(protein.regions)), str(len(protein.regions)) )
                protein.addRegion( cds )
            elif record["feature"] == "gene":
                gene_name = record["attr"]["gene_name"] if record["attr"].has_key("gene_name") else "Gene_noName"
                current_gene = BiologicalRegion( record["start"], record["end"], record["strand"], chromosome, record["attr"]["gene_id"], gene_name )
        self.close()

        return transcripts, proteins
    
    def _parse_line(self):
        """
        @summary : Returns a structured record from the GTF current line.
        @Returns : [dict] The record.
        """
        record = dict()
        record_attr = dict()
        record["region"], record["source"], record["feature"], record["start"], record["end"], record["score"], record["strand"], record["frame"], attributes = self.current_line.split("\t")
        attributes = attributes.strip()
        for current_attr in attributes.split(";"):
            current_attr = current_attr.strip()
            if current_attr != "" :
                matches = re.search("(.+)\s+\"([^\"]+)\"", current_attr)
                record_attr[matches.groups(1)[0]] = matches.groups(1)[1]
        record["attr"] = record_attr
        return record


# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


class VCFRecord:
    def __init__(self, region=None, position=None, knownSNPId=None, refAllele=None, altAlleles=None, qual=None, pFilter=None, info=None, pFormat=None, samples=None):
        self.chrom   = region
        self.pos     = position
        self.id      = knownSNPId
        self.ref     = refAllele
        self.alt     = altAlleles
        self.qual    = qual
        self.filter  = pFilter
        self.info    = info if info is not None else dict()
        self.format  = pFormat if pFormat is not None else list()
        self.samples = samples if samples is not None else list()
    
    def isIndel(self):
        """
        @summary : Return True if the variant is an insertio or a deletion.
        @return : [bool]
        """
        isIndel = False
        if self.ref == "." or self.ref == "-":
            isIndel = True
        else:
            for allele in self.alt:
                if len(allele) != len(self.ref) or allele == "." or allele == "-":
                    isIndel = True
        return isIndel
        
    def type(self):
        """
        @summary : Returns the vrariant type.
        @return : [str] 'snp' or 'indel' or 'variation'.
        """
        record_type = "snp"
        if self.isIndel():
            record_type = "indel"
        elif len(self.ref) > 1:
            record_type = "variation"
        return record_type


class VCFIO:
    def __init__( self, filepath, mode="r" ):
        """
        @param filepath : [str] The filepath.
        @param mode : [str] Mode to open the file ('r', 'w', 'a').
        """
        self.filepath = filepath
        self.mode = mode
        self.file_handle = open( filepath, mode )
        self.current_line_nb = 0
        self.current_line = None
    
    def __del__( self ):
        self.close()
    
    def __iter__( self ):
        for line in self.file_handle:
            self.current_line = line.rstrip()
            self.current_line_nb += 1
            if self.current_line.startswith('#') :
                continue
            try:
                vcf_record = self._parse_line()
            except:
                raise IOError( "The line " + str(self.current_line_nb) + " in '" + self.filepath + "' cannot be parsed by " + self.__class__.__name__ + ".\n" +
                               "Line content : " + self.current_line )
            else:
                yield vcf_record
            
    def close( self ) :
        if hasattr(self, 'file_handle') and self.file_handle is not None:
            self.file_handle.close()
            self.file_handle = None
            self.current_line_nb = None
            self.current_line = None
    
    def _parse_line( self ):
        """
        @summary : Returns a structured record from the VCF current line.
        @return : [VCFRecord]
        """
        fields = self.current_line.rstrip().split('\t')
        variation = VCFRecord() 
        variation.chrom  = fields[0]
        variation.pos    = int(fields[1])
        variation.id     = fields[2]
        variation.ref    = fields[3]
        variation.alt    = fields[4].split(',')
        variation.qual   = fields[5]
        variation.filter = fields[6]
        variation.format = fields[8].split(':')

        # Field INFO
        if fields[7] != '.' :
            info = dict()
            for tag_and_value in fields[7].split(';'):
                if "=" in tag_and_value:
                    tag, value = tag_and_value.split('=')
                    if "," in value:
                        info[tag] = value.split(",")
                    else:
                        info[tag] = value
                else:
                    info[tag_and_value] = True
            variation.info = info
        #~ # Fields samples
        #~ regexp_none=re.compile("\.(\/\.)*")
        #~ for lib_infos in range (9,len(fields)) :
            #~ if not regexp_none.match(fields[lib_infos]):
                #~ sformat = fields[lib_infos].split(':')
                #~ variation.samples.append( Entry(**{ autocast(variation.format[i]) : autocast(sformat[i]) if sformat[i] != '.' else None for i in range(0,len(variation.format)) } )  )
            #~ else :
                #~ variation.samples.append( Entry(**{ autocast(variation.format[i]) : None for i in range(0,len(variation.format)) }) )
        return variation


# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


class Logger:
    """
    @summary : Class to log program execution. You can defined the output stream and the levels managed.
    @todo : Singleton and factory for logger.
    """
    
    def __init__(self, filepath=None, levels=None, time_function=None):
        """
        @param filepath : [str] The log filepath. [default : STDOUT]
        @param levels : [list] The list of levels managed by logger (ex : "INFO", "DEBUG", "USER" ).
        @param time_function : [func] The function used to display time in log message. [default : time.time]
        """
        self.filepath = filepath
        self.time_function = time_function if time_function is not None else time.time
        self.levels = levels if levels is not None else ["INFO"]
        if self.filepath is not None:
            self.file_handle = open( self.filepath, "a" )
        else:
            self.file_handle = sys.stdout
    
    def __del__(self):
        """
        @summary : Closed file handler when the logger is detroyed.
        """
        if self.filepath is not None:
            self.file_handle.close()
            self.file_handle = None
    
    def write(self, msg, level="INFO", section=None):
        """
        @summary : Writes the log message if the logger manage the message level.
        @param msg : [str] The message.
        @param level : [str] Level of the message.
        @param section : [str] The section for the message.
        """
        if level in self.levels:
            if section is None:
                self.file_handle.write( "[" + str(self.time_function()) + "] " + level + " - " + msg + "\n" )
            else:
                self.file_handle.write( "[" + str(self.time_function()) + "] " + level + " - " + section + " : " + msg + "\n" )


# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


#######################################################################################
#
#                                FUNCTIONS
#
#######################################################################################
def is_filtered( hsp, min_identity, min_conserv, min_conserved_border, variant_position ):
    """
    @summary : Return True if the HSP fail filter evaluation.
    @param hsp : [AppHSP] The HSP to evaluate.
    @param min_identity : [float] Minimum identity ratio to keep HSP.
    @param min_conserv : [float] Minimum positive ratio to keep HSP.
    @param min_conserved_border : [int] Minimum positive length before and after variant aa (in aa).
    @param variant_position : [int] The variant position in query.
    @return : [bool]
    """
    is_filtered = True
    if float(hsp.identities)/hsp.align_length >= min_identity and float(hsp.positives)/hsp.align_length >= min_conserv:
        if variant_position >= hsp.query_start and variant_position <= hsp.query_end:
            hsp.set_pos()
            aln_pos = hsp.get_aln_pos_from_query( variant_position )
            if hsp.aln["match"][aln_pos] != "SG" and (min_conserved_border == 0 or conserv_area(hsp, aln_pos, min_conserved_border) ):
                is_filtered = False
    return is_filtered
    
def conserv_area( hsp, aln_pos, size=2 ):
    """
    @summary : Return True if the HSP succeed positive area evaluation.
    @param hsp : [AppHSP] The HSP to evaluate.
    @param min_conserved_border : [int] Minimum positive length before and after variant aa (in aa).
    @param variant_position : [int] The variant position in query.
    @return : [bool]
    """
    is_conserved = True
    codon_pos = hsp.get_codon_pos( aln_pos )
    codon_before_end = aln_pos - codon_pos
    codon_after_start = aln_pos + (3 - codon_pos) + 1
    before_start = codon_before_end - (size*3) + 1
    after_end = codon_after_start + (size*3) - 1
    if before_start >= 0 and after_end < len(hsp.aln["match"]):
        aln_before = hsp.aln["match"][before_start:codon_before_end+1]
        aln_after  = hsp.aln["match"][codon_after_start:after_end+1]
        if "-" in aln_before or "-" in aln_after or "SG" in aln_before or "SG" in aln_after or "QG" in aln_before or "QG" in aln_after:
            is_conserved = False
    else:
        is_conserved = False
    return is_conserved

def load_known_variants( filepath ):
    """
    @summary : Return variants from VCF.
    @param filepath : [str] Path to the VCF file.
    @return : [dict] {Region_name:{SNP_position:[SNP_id]}}.
    """
    known = dict()
    FH = open( filepath )
    for line in FH:
        if not line.startswith("#"):
            fields = line.strip().split()
            if not known.has_key( fields[0] ):
                known[fields[0]] = dict()
            if not known[fields[0]].has_key( fields[1] ):
                known[fields[0]][fields[1]] = list()
            known[fields[0]][fields[1]].append(fields[2])
    FH.close()
    return known

def protein_mut_consequence( ref_aa, mut_aa ):
    """
    @summary : Return the SO consequence of the aa mutation or 'unknown'.
    @param ref_aa : [str] The reference amino acid.
    @param mut_aa : [str] The mutated amino acid.
    @See : SO:0001580 children.
    """
    consequence = None
    if ref_aa == '?' or mut_aa == '?':
        consequence = "unknown"
    elif ref_aa == mut_aa:
        consequence = "synonymous_variant"
    elif ref_aa != "*" and mut_aa != "*":
        consequence = "missence_variant"
    elif mut_aa == "*":
        consequence = "stop_gained"
    else:
        consequence = "stop_lost"
    return consequence

#######################################################################################
#
#                                STORAGE
#
#######################################################################################
class AnnotSequence(Sequence):
    def __init__(self, id, string, description=None, quality=None):
        """
        @param id : [str] Id of the sequence.
        @param string : [str] Sequence of the sequence.
        @param description : [str] The sequence description.
        @param quality : [str] The quality of the sequence (same length as string).
        """
        Sequence.__init__(self, id, string, description, quality)
        self.variant = None # Object variant
        self.hsps = list() # HSP after filtering
        self.annotations = list() # Annotations of the variant for each self.hsps
        
    def annot_to_gff(self):
        """
        @summary : Returns a generator on list of annotations.
        @return : [Generator] List of GFF3Record.
        """
        for idx, curr_annot in enumerate(self.annotations):
            annot = GFF3Record()
            annot.seq_id = str(self.id)
            annot.source = "tSNPannot"
            annot.type = "SNP"
            annot.start = self.variant.ref_pos
            annot.end = self.variant.ref_pos
            annot.score = self.hsps[idx].score
            annot.strand = self.hsps[idx].strand
            annot.phase = "*"
            annot.attributes = dict()
            annot.setAttribute( "variant_id", self.variant.id )
            # Alignment scores
            annot.setAttribute( "identities", self.hsps[idx].identities )
            annot.setAttribute( "positives", self.hsps[idx].positives )
            annot.setAttribute( "align_length", self.hsps[idx].align_length ) # subject end - subject start + gap in query + gap in subject
            annot.setAttribute( "evalue", self.hsps[idx].expect )
            # Target information
            annot.setAttribute( "Target", self.hsps[idx].hit_id + " " + str(self.hsps[idx].sbjct_start) + " " + str(self.hsps[idx].sbjct_end) + " " + self.hsps[idx].strand )
            annot.setAttribute( "species", self.hsps[idx].hit_species )
            annot.setAttribute( "ref_region", self.annotations[idx].ref_region_name )
            annot.setAttribute( "ref_position", self.annotations[idx].ref_region_position )
            annot.setAttribute( "exon_5prim_barrier", self.annotations[idx].exon_5prim_barrier )
            annot.setAttribute( "exon_3prim_barrier", self.annotations[idx].exon_3prim_barrier )
            annot.setAttribute( "gene_name", self.annotations[idx].gene_name )
            if self.annotations[idx].known_variants:
                annot.setAttribute( "known_variants", ','.join(self.annotations[idx].known_variants) )
            # Alleles consequences
            if self.hsps[idx].strand == "+":
                if self.annotations[idx].codon_pos == 3:
                    before = self.variant.before[-2] + self.variant.before[-1]
                    after = ""
                elif self.annotations[idx].codon_pos == 2:
                    before = self.variant.before[-1]
                    after = self.variant.after[0]
                else:
                    before = ""
                    after = self.variant.after[0] + self.variant.after[1]
            else:
                if self.annotations[idx].codon_pos == 3:
                    before = self.variant.after[1] + self.variant.after[0]
                    after = ""
                elif self.annotations[idx].codon_pos == 2:
                    before = self.variant.after[0]
                    after = self.variant.before[-1]
                else:
                    before = ""
                    after = self.variant.before[-1] + self.variant.before[-2]
            ref_codon = None
            for allele_idx, allele in enumerate(self.variant.alleles):
                codon = Codon( 'noId', before + allele + after )
                if self.hsps[idx].strand == "-":
                    codon.complement()
                annot.addToAttribute( "codon_consequences", codon.string )
                current_aa = Codon.trad(codon)
                annot.addToAttribute( "aa_consequences", current_aa )
                if allele_idx != 0: # mutated codon
                    annot.addToAttribute( "protein_consequences", protein_mut_consequence( ref_aa, current_aa ) )
                else: # ref codon
                    ref_aa = current_aa
                    annot.addToAttribute( "protein_consequences", "-" )
            yield annot
      
    def __str__(self):
        return ">" + self.id + " variant[" + str(self.variant) + "]\n" + self.string

 
class Variant:
    def __init__(self, id, pos, alleles=None, carrier=None ):
        """
        @id : [str] The identifiant of the variation.
        @pos : [int] The position position on carrier.
        @alleles : [list] The list of alleles (ref allele in first).
        @carrier : [AnnotSequence] The sequence where the variant is.
        """
        self.id = id
        self.pos = pos # position on seq
        self.ref_pos = None # position on real ref
        self.carrier = carrier
        self.alleles = alleles
        self.before = "" # use instead of AnnotSequence.string to reduce memory consumption
        self.after = "" # use instead of AnnotSequence.string to reduce memory consumption
    
    def __str__(self):
        return self.id + " " + self.before + "[" + "/".join(self.alleles) + "]" + self.after + " in " + self.carrier.id + ":" + str(self.pos)


class Annotation:
    def __init__( self, ref_region_name, ref_region_position, ref_strand, exon_5prim_barrier, exon_3prim_barrier, codon_pos, gene_name=None ):
        """
        @ref_region_name : [str] The name of the region/chromosome where the variant is find on reference species.
        @ref_region_position : [str] The position on the reference species region where the variant is find on reference species.
        @ref_strand : [str] The strand of the RNA where the variant is find on reference species.
        @exon_5prim_barrier : [int] Nucleic distance between variant and five prime exon start.
        @exon_3prim_barrier : [int] Nucleic distance between variant and three prime exon end.
        @codon_pos : [int] Position of variant in codon.
        @gene_name : [str] Hit gene name.
        """
        self.ref_region_name = ref_region_name
        self.ref_region_position = ref_region_position
        self.ref_strand = ref_strand
        self.exon_5prim_barrier = exon_5prim_barrier
        self.exon_3prim_barrier = exon_3prim_barrier
        self.codon_pos = codon_pos
        self.gene_name = gene_name
        self.known_variants = None


# 
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '2.0.2'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'


class AnnotDbParameter(argparse.Action):
    """
    @summary : Argparse parameter fo annotation databanks.
    """
    def __call__(self, parser, namespace, values, option_string=None):
        valid_param = ["species", "aln", "gtf", "vcf"]
        required_param = ["species", "aln", "gtf"]
        # Retrieve params
        current_annot_db = dict()
        for option in values:
            param, val = option.split("=")
            param = param.strip()
            val = val.strip()
            if not param in valid_param:
                parser.error("Unknown sub-parameter '" + param + "' in argument " + option_string + ".")
            current_annot_db[param] = val
        absents = list()
        for required in required_param:
            if not required in current_annot_db.keys():
                absents.append(required)
        if len(absents) > 0:
            parser.error("Missing sub-parameter '" + "', ".join(absents) + "' in '" + option_string + " " + " ".join(values) + "'.")
        # Set parser
        annotations_db = getattr(namespace, self.dest)
        if annotations_db is None:
            annotations_db = list()
        annotations_db.append( current_annot_db )
        setattr(namespace, self.dest, annotations_db) 


if __name__ == "__main__":
    def date_log(): return time.strftime("%Y-%m-%d %H:%M:%S")
    logger = Logger(None, "INFO", date_log)

    # Manage parameters
    parser = argparse.ArgumentParser( description='Annotates by similarities the SNPs called from RNSeq de novo data.' )
    parser.add_argument( "--version", action='version', version=__version__ )
    group_input_output = parser.add_argument_group( 'Inputs/outputs' ) # Input/outputs
    group_input_output.add_argument( '--sequences-file', type=str, dest='reference_file', required=True, help='Reference sequences (format : FASTA).' )
    group_input_output.add_argument( '--variants-file', type=str, dest='variants_file', required=True, help='Variants to annotate (format : VCF).' )
    group_input_output.add_argument( '--output', type=str, required=True, help='The GFF3 output file.' )
    group_annot_db = parser.add_argument_group( 'Annotation databanks' ) # Annotation databanks
    group_annot_db.add_argument( '--annot-db', dest='annotation_databanks', required=True, action=AnnotDbParameter, nargs='+', metavar=("species=NAME aln=FILEPATH gtf=FILEPATH [vcf=FILEPATH]", ""), help='Databanks used to annotate by similarities.' )
    group_filters = parser.add_argument_group( 'Filters' ) # Parameters used to filter HSP used in annotation
    group_filters.add_argument( '-i', '--min-identity', type=float, default=0.90, help='Use only HSPs with at least this identity.' )
    group_filters.add_argument( '-c', '--min-conserv', type=float, default=0.96, help='Use only HSPs with at least this conservation.' )
    group_filters.add_argument( '-b', '--min-conserv-border', type=int, default=4, help='Use only HSPs with this number of amino acids conserved before and after the amino acid containing the variant.' )
    group_filters.add_argument( '-n', '--nb-annot', type=int, default=1, help='Maximum number of different scores.' )
    args = parser.parse_args()

    # Load variants
    logger.write( "Loading variants." )
    nb_variants = 0
    queries_sequences = dict()
    FH_reference = FastaIO( args.reference_file )
    FH_variants_to_annot = VCFIO( args.variants_file )
    for variant in FH_variants_to_annot:
        nb_variants += 1
        if variant.type() == "snp":
            current_seq = FH_reference.next_seq()
            seq = AnnotSequence( variant.chrom, "" )
            alleles = [variant.ref] + variant.alt
            sub_seq_pos = int(current_seq.description.split(':')[1])
            variant_id = "snp_" + str(nb_variants)
            if variant.id != ".":
                variant_id = variant.id
            seq.variant = Variant( variant_id, sub_seq_pos, alleles, seq )
            seq.variant.ref_pos = variant.pos
            seq.variant.before = current_seq.string[max(sub_seq_pos-5, 0):sub_seq_pos-1]
            seq.variant.after = current_seq.string[sub_seq_pos:min(sub_seq_pos+4, len(current_seq.string)-1)]
            queries_sequences[variant.chrom + "_chr@" + str(variant.pos) + '.' + '_'.join(variant.alt)] = seq
    FH_variants_to_annot.close()
    FH_reference.close()

    # Filter hsp
    logger.write( "Start filtering." )
    for curr_annot_db in args.annotation_databanks:
        blast_fh = open( curr_annot_db["aln"] )
        blast_records = NCBIXML_parse( blast_fh )
        for blast_record in blast_records:
            query_id = blast_record.query.split()[0]
            for alignment in blast_record.alignments:
                for hsp in alignment.hsps:
                    app_hsp = AppHSP.from_HSP(hsp)
                    app_hsp.hit_id = alignment.hit_def.split()[0]
                    if not is_filtered( app_hsp, args.min_identity, args.min_conserv, args.min_conserv_border, queries_sequences[query_id].variant.pos ):
                        query = queries_sequences[query_id]
                        app_hsp.hit_species = curr_annot_db["species"]
                        if len(query.hsps) < args.nb_annot:
                            query.hsps.append( app_hsp )
                            query.annotations.append( None )
                            query.hsps = sorted(query.hsps, key=lambda hsp: -hsp.score) #sort by decreasing score
                        elif query.hsps[-1].score <= app_hsp.score:
                            query.hsps.append( app_hsp )
                            query.hsps = sorted(query.hsps, key=lambda hsp: -hsp.score) #sort by decreasing score
                            nb_diff = 0
                            prev_score = None
                            stop_idx = len(query.hsps)
                            for idx, hsp in enumerate(query.hsps):
                                if prev_score != hsp.score:
                                    prev_score = hsp.score
                                    nb_diff += 1
                                    if nb_diff == args.nb_annot + 1:
                                        stop_idx = idx
                            query.hsps = query.hsps[0:stop_idx]
                            query.annotations = [None for pos in range(0,stop_idx)]
        blast_fh.close()

    # Annotate variants
    for curr_annot_db in args.annotation_databanks:
        logger.write( "Process position in '" + curr_annot_db["species"] + "'." )
        logger.write( "Load '" + curr_annot_db["species"] + "' GTF.", "INFO", "Position annotation" )
        # Find variant subject position
        transcripts_model, proteins_model = GTFI( curr_annot_db["gtf"] ).load_model()
        logger.write( "Load '" + curr_annot_db["species"] + "' GTF OK.", "INFO", "Position annotation" )
        for query in queries_sequences.values():
            for idx, hsp in enumerate(query.hsps):
                # Filter by species
                if hsp.hit_species == curr_annot_db["species"]:
                    # Subject position
                    aln_pos = hsp.get_aln_pos_from_query( query.variant.pos )
                    subject_pos = hsp.get_subject_pos( aln_pos )
                    codon_pos = hsp.get_codon_pos( aln_pos )
                    # Genome position
                    subject_protein = proteins_model[hsp.hit_id]
                    cds, cds_pos = subject_protein.getEltByPos( subject_pos, codon_pos )
                    dna_region_pos = cds.getPosOnRef( cds_pos )
                    dna_region_name = cds.reference.name
                    # Exon barrier distance
                    transcript = transcripts_model[subject_protein.transcript.id]
                    exon, exon_pos = transcript.getEltByPos( subject_protein.getPosOnGlobalCDS(subject_pos, codon_pos) + subject_protein.getFivePrimUTR() )
                    if exon.strand == "+":
                        exon_5prim_barrier = exon_pos - 1
                        exon_3prim_barrier = exon.length() - exon_pos
                    else:
                        exon_5prim_barrier = exon.length() - exon_pos
                        exon_3prim_barrier = exon_pos - 1
                    query.annotations[idx] = Annotation( dna_region_name, dna_region_pos, transcript.strand, exon_5prim_barrier, exon_3prim_barrier, codon_pos, transcript.gene.name )
        del transcripts_model
        del proteins_model

        # Known variants
        if curr_annot_db.has_key( "vcf" ):
            logger.write( "Find Known variants in '" + curr_annot_db["species"] + "'." )
            logger.write( "Load '" + curr_annot_db["species"] + "' VCF.", "INFO", "Known variants" )
            known_variants = load_known_variants( curr_annot_db["vcf"] )
            logger.write( "Load '" + curr_annot_db["species"] + "' VCF OK.", "INFO", "Known variants" )
            for query in queries_sequences.values():
                for idx, hsp in enumerate(query.hsps):
                    # Filter by species
                    if hsp.hit_species == curr_annot_db["species"]:
                        annotation = query.annotations[idx]
                        if known_variants.has_key(annotation.ref_region_name) and known_variants[annotation.ref_region_name].has_key(str(annotation.ref_region_position)):
                            annotation.known_variants = known_variants[annotation.ref_region_name][str(annotation.ref_region_position)]
            del known_variants

    # Write output
    logger.write( "Write outpout." )
    annotation_file = GFF3IO( args.output, "w" )
    annotation_file._handle.write( "## command : " + " ".join(sys.argv) + "\n" )
    annotation_file._handle.write( "## version : " + __version__ + "\n" )
    for query in queries_sequences.values():
        for curr_annot in query.annot_to_gff():
            annotation_file.write( curr_annot )

    logger.write( "Execution complete." )
