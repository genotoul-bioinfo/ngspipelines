#!/usr/bin/env python2.7
#
# Copyright (C) 2014 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme genomique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.2'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'prod'

import os, sys
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))
sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(CURRENT_DIR))), "src"))

import argparse
import re
import math
import lib.NCBIXML as NCBIXML
from lib.annotationfile import AnnotationFile, AnnotationRecord

def getAlnType( blast_record ):
    """
     @summary : Returns the type of the alignment.
      @param blast_record : [Bio.Blast.Record] the alignment processed.
     @return : [str] the type of the alignment ('nucl2nucl' or 'nucl2prot' or 'prot2nucl' or 'prot2prot').
    """
    aln_type = None
    aln_query_ratio = 1
    aln_sbjct_ratio = 1
    if blast_record.application in ["BLASTN", "MEGABLAST", "TBLASTX"]:
        aln_type = "nucl2nucl"
    elif blast_record.application == "BLASTX":
        aln_type = "nucl2prot"
        aln_query_ratio = 3
    elif blast_record.application == "TBLASTN":
        aln_type = "prot2nucl"
        aln_sbjct_ratio = 3
    elif blast_record.application in ["BLASTP", "PSI-BLAST", "PHI-BLAST", "DELTA-BLAST"]:
        aln_type = "prot2prot"
    return aln_type, aln_query_ratio, aln_sbjct_ratio

def spFromHit( alignment ):
    """
     @summary : Returns subject's species.
                If the species can't be retrieved from alignment this function return None.
      @param alignment : [Bio::blast::Record::Alignment] the alignment.
    """
    species = None
    if re.match(".*OS=.*", alignment.hit_def) is not None: # refseq_prot, swissprot
        try:
            matches = re.search("OS=([^=]+)", alignment.hit_def)
            if matches is not None:
                species = matches.group(1)
                # Delete next tag
                clean_matches = re.search("(.+) [A-Z][A-Z]$", species)
                if clean_matches is not None:
                    species = clean_matches.group(1)
        except: 
            pass
    else :
        #refseq_genomic >gi|76496352|ref|NT_165333.1|NT_165333 Mus musculus genomic sequence, ENCODE region ENm001
        #refseq_rna >gi|31341369|ref|NM_176670.2| Bos taurus ATP synthase, H+ transporting, mitochondrial F1 complex, delta subunit (ATP5D), mRNA
        #refseq_prot >gi|66818355|ref|XP_642837.1| hypothetical protein DDB_G0276911 [Dictyostelium discoideum AX4]
        if re.match("gi\|\d+\|ref\|\S+\|", alignment.hit_id) is not None: #if refseq
            try:
                if re.match("^PREDICTED:\s", alignment.hit_def) is not None: 
                    # au 24/03  refseq_rna : toujours PREDICTED: SPECIES
                    matches = re.search("PREDICTED:\s(\S+\s\S+)", alignment.hit_def)
                    if matches is not None:
                        species = matches.group(1)
                elif re.match('\[[^\]]+)\]', alignment.hit_def) :
                    #si refseq_prot espece entre []
                    matches = re.search('\[([^\]]+)\]', alignment.hit_def) 
                    if matches is not None:
                        species = matches.group(1)
                else :
                    matches = re.search("gi\|\d+\|ref\|\S+\|\s(\S+\s\S+)", alignment.hit_def)
                    if matches is not None:
                        species = matches.group(1)
            except:
                pass
        else :
            try:
                matches = re.search('\[([^\]]+)\]', alignment.hit_def)
                species = matches.group(1)
            except: 
                species = None
                pass
    # Others databases are not implemented
    return species


def geneNameFromHit( alignment, gene_name_from_accession ):
    """
     @summary : Returns the subject's name.
                If the name can't be retrieved from alignment this function return None.
      @param alignment : [Bio::blast::Record::Alignment] the alignment.
      @todo : use efetch.
    """
    name = None
    # Select ID type
    id, id_type = None, None
    if re.match("^gi\|\d+\|[^\|]+\|.+", alignment.hit_id) is not None: 
        
        id_type = "gi"
        id = alignment.hit_id
    elif re.match("^gi\|\d+\|[^\|]+\|.+", alignment.hit_def) is not None:
        id_type = "gi"
        id = alignment.hit_def
    elif re.match('gnl\|\w+\|\d+', alignment.hit_id) is not None: #Ensembl ou Refseq_prot
        id_type = "gnl"
        id = alignment.hit_def
        
    
    # Retrieve gene name
    if id is not None and name is None :
        try:
            # GI
            if id_type == "gi":
                match_db_abrev = re.match("^gi\|\d+\|([^\|]+)\|.+", id)
                db_abrev = match_db_abrev.group(1)
                # Standard format (ex : 'gi|297792995|ref|XP_002864382.1|')
                if db_abrev == "ref":
                    #if refseqRNA
                    if re.match("^PREDICTED:\s", alignment.hit_def) is not None: 
                        matches = re.search("PREDICTED:\s.*\((\S+)\)", alignment.hit_def)
                        if matches is not None:
                            name = matches.group(1)
                    if name is None :
                        matches = re.search("^gi\|\d+\|[^\|]+\|([^\s]+)", id)
                        accession = matches.group(1)
                        if accession.endswith('|'):
                            accession = accession[:-1]
                        # refseq accession to name
                        if gene_name_from_accession.has_key(accession):
                            name = gene_name_from_accession[accession]
                            
                # Swissprot format (ex : 'gi|6015058|sp|O49169.1|EF1A_MANES')
                elif db_abrev == "sp":
                    matches = re.search("^gi\|\d+\|[^\|]+\|[^\|]+\|([^\s]+)", id)
                    name = matches.group(1).split("_")[0]
            
            # Uniprot
            elif id.startswith("sp") or id.startswith("tr"):
                    matches = re.search("^[^\|]+\|[^\|]+\|([^\s]+)_", id)
                    name = matches.group(1)
            # GNL
            elif id.startswith('ENS') :
                matches = re.search("\sgene_symbol:([^\s]+)", id)
                if len(matches.groups()) > 0 :
                    name = matches.group(1)
                else :
                    matches = re.search("\sgene:([^\s]+)", id)
                    name = matches.group(1)
        except:
            pass
    # Others databases are not implemented
    return name

def subjectAccessionFromHit( alignment ):
    """
     @summary : Returns the subject's accession.
                If the accession can't be retrieved from alignment this function return None.
      @param alignment : [Bio::blast::Record::Alignment] the alignment.
    """
    name = None
    # Select ID type
    id, id_type = None, None
    if re.match("^gi\|\d+\|[^\|]+\|.+", alignment.hit_id) is not None:
        id_type = "gi"
        id = alignment.hit_id
    elif re.match("^gi\|\d+\|[^\|]+\|.+", alignment.hit_def) is not None:
        id_type = "gi"
        id = alignment.hit_def
    elif re.match('gnl\|\w+\|\d+', alignment.hit_id) is not None:
        id_type = "gnl"
        id = alignment.hit_def
    # Retrieve accession
    if id is not None:
        try:
            # GI
            if id_type == "gi":
                match_db_abrev = re.match("^gi\|\d+\|([^\|]+)\|.+", id)
                db_abrev = match_db_abrev.group(1)
                # Standard format (ex : 'gi|297792995|ref|XP_002864382.1|')
                if db_abrev in ["ref", "gb", "emb", "dbj", "pdb", "tpg"]:
                    matches = re.search("^gi\|\d+\|[^\|]+\|([^\s]+)", id)
                    name = matches.group(1)
                    if name.endswith('|'):
                        name = name[:-1]
                # pir format (ex : 'gi|542629|pir||B48408')
                elif db_abrev == "pir":
                    matches = re.search("^gi\|\d+\|pir\|\|([^\s]+)", id)
                    name = matches.group(1)
                    if name.endswith('|'):
                        name = name[:-1]
                # Swissprot format (ex : 'gi|6015058|sp|O49169.1|EF1A_MANES')
                elif db_abrev == "sp":
                    matches = re.search("^gi\|\d+\|sp\|([^\|]+)\|.+", id)
                    name = matches.group(1)
                # Strip '.version' in accession.version
                matches = re.search("(\.\d+)$", name)
                if matches is not None:
                    name = name[:-len(matches.group(1))]
            # GNL
            else:
                # Uniprot
                if id.startswith("sp") or id.startswith("tr"):
                    matches = re.search("^[^\|]+\|([^\|]+)\|[^\s]+_", id)
                    name = matches.group(1)
                    # Strip '.version' in accession.version
                    matches = re.search("(\.\d+)$", name)
                    if matches is not None:
                        name = name[:-len(matches.group(1))]
                # Unknown format
                else:
                    matches = re.search("^([^\s]+)", id)
                    name = matches.group(1)
        except:
            pass
    # Others databases are not implemented
    if name is None :
        name=alignment.hit_id
    return name

def gene_name_by_accession( gene_by_gi, gi_by_accession, types=None ):
    """
     @summary : Returns a dictionary to link refseq accession number to gene name.
      @param gene_by_gi : [str] path to the NCBI's file who contains link between gene name and gene ID.
      @param gi_by_accession : [str] path to the NCBI's file who contains link between gene ID and accession.
     @return : [dict] Each key is a refseq accession. The value is the gene name.
    """
    gene_name_by_accession = dict()
    convenient_fields = list()
    # Select convenient types
    if types is None or "RNA" in types:
        convenient_fields.append( 3 )
    if types is None or "protein" in types:
        convenient_fields.append( 5 )
    if types is None or "genomic" in types:
        convenient_fields.append( 7 )
    # Load gene name by genebank ID from gene_name_file (gene name <-> genebank ID)
    gene_name_by_id = dict()
    gene_name_fh = open( gene_by_gi )
    for line in gene_name_fh:
        if not line.startswith('#'):
            line = line.strip()
            line_fields = line.split()
            if line_fields[2] != "NEWENTRY":
                gene_name_by_id[line_fields[1]] = line_fields[2].replace("\\", '_')
    # Load gene name by accession from gene_name_by_id and gene_id_file (accession <-> genebank ID)
    gene_id_fh = open( gi_by_accession )
    for line in gene_id_fh:
        if not line.startswith('#'):
            line = line.strip()
            line_fields = line.split()
            for fields in convenient_fields:
                accession = line_fields[fields]
                if accession != "-":
                    gene_name_by_accession[accession] = gene_name_by_id[line_fields[1]]
    del gene_name_by_id
    return gene_name_by_accession

def strand( hsp ):
    """
     @summary : Returns the strand of the alignment.
      @param hsp : [Bio.Blast.Record.HSP] the HSP processed.
     @return : [char] the alignment strand ('+' or '-').
    """
    strand = "+"
    if hsp.strand[0] is not None:
        strand = hsp.strand[0]
    phase = "."
    if hsp.frame[0] is not None:
        phase = abs(hsp.frame[0])-1
        if hsp.strand[0] is None and hsp.frame[0] < 0:
            strand = "-"
    return strand

def sbjct_insert_between( hsp, start, end ):
    """
     @summary : Returns the number of insertion positions on subject for the HSP between provided start and end position.
      @param hsp : [Bio.Blast.Record.HSP] the HSP processed.
      @param start : [int] first position used on subject. This position must be >= hsp.sbjct_start.
      @param end : [int] last position used on subject. This position must be <= hsp.sbjct_end.
     @return : [int] the number of insertions. The unit used correspond to the subject's molecules.
     @warning : Doesn't work with query protein align to nucleic subject.
     @note : Frameshift doesn't exist in NCBI+ blastx.
    """
    nb_insert = 0
    if '-' in hsp.query:
        aln_sbjct_str = hsp.sbjct
        aln_query_str = hsp.query
        real_pos = hsp.sbjct_start
        # Go to start
        i = 0
        while real_pos < start:
            # [A-Za-z] on subject
            if aln_sbjct_str[i].isalpha() or aln_sbjct_str[i] == "*":
                real_pos += 1
            # Else missing on subject ("-" or " ") : nothing
            i += 1
        # Count nb inserts
        while real_pos <= end:
            # [A-Za-z] on subject
            if aln_sbjct_str[i].isalpha() or aln_sbjct_str[i] == "*":
                real_pos += 1
                # Gap on query
                if aln_query_str[i] == "-":
                    nb_insert += 1
            # Else gap on subject ("-" or " ") : nothing
            i += 1
    return nb_insert

def query_stat_between( hsp, aln_type, start, end ):
    """
     @summary : Returns the number of insertion, identity and positive positions on query for the HSP between provided start and end position.
      @param hsp : [Bio.Blast.Record.HSP] the HSP processed.
      @param aln_type : [str] the type of alignment ('nucl2nucl' or 'nucl2prot' or 'prot2prot').
                        This information is used to determine the aln ratio. With 'nucl2prot' 1 position on subject = 3 positions on query.
      @param start : [int] first position used on query. This position must be >= hsp.query_start.
      @param end : [int] last position used on query. This position must be <= hsp.query_end.
     @return : [list] the number of insertions, identities, and positives. The unit used correspond to the query's molecules.
     @warning : Doesn't work with aln_type = 'prot2nucl'.
     @note : Frameshift doesn't exist in NCBI+ blastx.
    """
    nb_insert   = 0
    nb_identity = 0
    nb_conserv  = 0
    aln_sbjct_str = hsp.sbjct
    aln_query_str = hsp.query
    aln_match_str = hsp.match
    aln_ratio = ( 3 if aln_type == "nucl2prot" else 1 )
    # If query is aligned after reverse complement
    if strand(hsp) == "-":
        # Reverse alignment representation for use real positions on query
        aln_query_str = aln_query_str[::-1]
        aln_sbjct_str = aln_sbjct_str[::-1]
        aln_match_str = aln_match_str[::-1]
    real_pos = hsp.query_start
    # Go to start
    i = 0
    while real_pos < start:
        # [A-Za-z] on query
        if aln_query_str[i].isalpha() or aln_query_str[i] == "*":
            real_pos += aln_ratio * 1
        # Else missing on query ("-" or " ") : nothing
        i += 1
    # If start begin before hsp start on nucl2prot
    if i != 0 and aln_type == "nucl2prot":
        substract = (start - hsp.query_start) % 3
        # The nucleotides of the AA are shared between the previous HSP and the current HSP
        if substract != 0:
            # Identity
            if aln_query_str[i-1] == aln_sbjct_str[i-1]:
                nb_conserv += 3 - substract
                nb_identity += 3 - substract
            # Conservation
            elif aln_match_str[i-1] == "+":
                nb_conserv += 3 - substract
            # Insertion
            elif aln_sbjct_str[i-1] == "-":
                nb_insert += 3 - substract
    # Count nb inserts
    while real_pos <= end:
        # [A-Za-z] on query
        if aln_query_str[i].isalpha() or aln_query_str[i] == "*":
            real_pos += aln_ratio * 1
            # Gap on subject
            if aln_sbjct_str[i] == "-":
                nb_insert += aln_ratio * 1
            else:
                # Identity
                if aln_query_str[i] == aln_sbjct_str[i]:
                    nb_conserv += aln_ratio * 1
                    nb_identity += aln_ratio * 1
                # Conservation
                elif aln_match_str[i] == "+":
                    nb_conserv += aln_ratio * 1
        # Gap on query
        elif aln_query_str[i] == "-" or aln_query_str[i] == " ":
            pass
        i += 1
    return nb_insert, nb_identity, nb_conserv

def query_del_between( hsp, start, end ):
    """
     @summary : Returns the number of deletion positions on subject for the HSP between provided start and end position.
      @param hsp : [Bio.Blast.Record.HSP] the HSP processed.
      @param start : [int] first position used on subject. This position must be >= hsp.sbjct_start.
      @param end : [int] last position used on subject. This position must be <= hsp.sbjct_end.
     @return : [int] the number of deletions. The unit used correspond to the subject's molecules.
     @warning : Doesn't work with query protein align to nucleic subject.
     @note : Frameshift doesn't exist in NCBI+ blastx.
    """
    nb_del = 0
    if '-' in hsp.query:
        aln_query_str = hsp.query
        real_pos = hsp.query_start
        # Go to start
        i = 0
        while real_pos < start:
            # [A-Za-z] on subject
            if aln_query_str[i].isalpha() or aln_query_str[i] == "*":
                real_pos += 1
            # Else missing on subject ("-" or " ") : nothing
            i += 1
        # Count nb deletions
        while real_pos <= end:
            # [A-Za-z] on subject
            if aln_query_str[i].isalpha() or aln_query_str[i] == "*":
                real_pos += 1
            else: # gap on query ("-" or " ") 
                nb_del += 1
            i += 1
    return nb_del

def sorted_hsp( hsps, on ):
    """
     @summary : Returns the list of HSP sorted by ascending query or subject start. If two HSP have same start they are sorted by query or subject end.
      @param hsps : [list] the list of Bio.Blast.Record.HSP objects.
      @param on : [str] the element used to sort ('query' => query start OR 'subject' => subject start).
     @return : [list] the sorted list of HSP.
    """
    if on == "query":
        return sorted( hsps, key=lambda hsp: int(hsp.query_start) )
    else:
        return sorted( hsps, key=lambda hsp: int(hsp.sbjct_start) )

def hit_stat( best_hsp, others_hsp, aln_type ):
    """
     @summary : Returns 
                - the query_coverage : the number of query's positions aligned on subject for global HSPs (overlapped positions are counted only once).
                - the sbjct_coverage : the number of subject's positions aligned on query for global HSPs (overlapped positions on subject and HSPs are counted only once).
                - query_identity : the number of 'identities' positions between HSPs and the subject (overlapped positions are counted only once).
                - query_conservation : the number of 'positives' positions between HSPs and the subject (overlapped positions are counted only once).
      @param best_hsp : [Bio.Blast.Record.HSP] the best HSP of the hit.
      @param others_hsp : [list] the others HSP of the same hit.
      @param aln_type : [str] the type of alignment ('nucl2nucl' or 'nucl2prot' or 'prot2prot').
                        This information is used to determine the aln ratio. With 'nucl2prot' 1 position on subject = 3 positions on query.
     @return : [list] the query coverage, subject coverage, query identity and query conservation.
    """
    remove_query_start = dict()
    remove_sbjct_start = dict()
    query_coverage = 0
    query_identity = 0
    query_conservation = 0
    sbjct_coverage = 0
    # Sort HSP by query start
    hsps = sorted_hsp( [best_hsp] + others_hsp, "query" )
    # Process query coverage
    hsp_index = 0
    while hsp_index < len(hsps):
        if strand(hsps[hsp_index]) == strand(best_hsp):
            current_start = hsps[hsp_index].query_start
            current_end = hsps[hsp_index].query_end
            hsp_id = str(current_start) + "_" + str(current_end) + "_" + str(hsps[hsp_index].score)
            if not remove_query_start.has_key( hsp_id ):
                remove_query_start[hsp_id] = 0
            nb_insertions, nb_identities, nb_positives  = query_stat_between( hsps[hsp_index], aln_type, (current_start + remove_query_start[hsp_id]), current_end  )
            query_coverage += current_end - current_start - remove_query_start[hsp_id] - nb_insertions + 1
            query_identity += nb_identities
            query_conservation += nb_positives
            # If it exist a next hsp
            compare_with_next = True
            while (hsp_index != (len(hsps) - 1)) and compare_with_next:
                next_start = hsps[hsp_index + 1].query_start
                next_end = hsps[hsp_index + 1].query_end
                # If next overlap current
                if current_start == next_start or (current_start < next_start and current_end >= next_start):
                    # If current contains the next
                    if current_end >= next_end:
                        # Delete next from subject count
                        del( hsps[hsp_index + 1] )
                    else:
                        # Size to remove on next
                        next_hsp_id = str(next_start) + "_" + str(next_end) + "_" + str(hsps[hsp_index + 1].score)
                        remove_query_start[next_hsp_id] = current_end - next_start + 1
                        compare_with_next = False
                # Next doesn't overlap current
                else:
                    compare_with_next = False
        hsp_index +=1
    # Sort HSP by subject start
    hsps = sorted_hsp( hsps, "subject" )
    # Process subject coverage
    if aln_type == "nucl2prot":
        for hsp_id in remove_query_start:
            remove_query_start[hsp_id] = math.ceil(float(remove_query_start[hsp_id])/3)
    hsp_index = 0
    while hsp_index < len(hsps):
        current_strand = strand(hsps[hsp_index])
        if current_strand == strand(best_hsp):
            current_start = hsps[hsp_index].sbjct_start
            current_end = None
            hsp_id = str(hsps[hsp_index].query_start) + "_" + str(hsps[hsp_index].query_end) + "_" + str(hsps[hsp_index].score)
            if not remove_sbjct_start.has_key( hsp_id ):
                remove_sbjct_start[hsp_id] = 0
            # Adjust remove_query_start with query deletions and subject insertions
            if remove_query_start[hsp_id] > 0:
                remove_query_start[hsp_id] += query_del_between( hsps[hsp_index], hsps[hsp_index].query_start, hsps[hsp_index].query_start+remove_query_start[hsp_id]-1 )
                if current_strand == "+":
                    remove_query_start[hsp_id] -= hsps[hsp_index].sbjct.count( "-", 0, int(remove_query_start[hsp_id]-1) )
                else:
                    sbjct_length = len(hsps[hsp_index].sbjct)
                    remove_query_start[hsp_id] -= hsps[hsp_index].sbjct.count( "-", int(sbjct_length - remove_query_start[hsp_id]), int(sbjct_length - 1) )
            # Process HSP coverage
            if current_strand == "+":
                current_end = hsps[hsp_index].sbjct_end
                max_remove = max(remove_query_start[hsp_id], remove_sbjct_start[hsp_id])
                nb_insertions = sbjct_insert_between( hsps[hsp_index], (current_start + max_remove), current_end  )
                sbjct_coverage += -max_remove + current_end - current_start - nb_insertions + 1
            else:
                current_end = hsps[hsp_index].sbjct_end - remove_query_start[hsp_id]
                nb_insertions = sbjct_insert_between( hsps[hsp_index], (current_start + remove_sbjct_start[hsp_id]), current_end  )
                sbjct_coverage += -remove_sbjct_start[hsp_id] + current_end - current_start - nb_insertions + 1
            # If it exist a next hsp
            compare_with_next = True
            while (hsp_index != (len(hsps) - 1)) and compare_with_next:
                next_start = hsps[hsp_index + 1].sbjct_start
                next_end = hsps[hsp_index + 1].sbjct_end
                # If next overlap current
                if current_start == next_start or (current_start < next_start and current_end >= next_start):
                    # If current contains the next
                    if current_end >= next_end:
                        # Delete next from subject count
                        del( hsps[hsp_index + 1] )
                    else:
                        # Size to remove on next
                        compare_with_next = False
                        next_hsp_id = str(hsps[hsp_index+1].query_start) + "_" + str(hsps[hsp_index+1].query_end) + "_" + str(hsps[hsp_index+1].score)
                        remove_sbjct_start[next_hsp_id] = current_end - next_start + 1
                # Next doesn't overlap current
                else:
                    compare_with_next = False
        hsp_index +=1
    return query_coverage, sbjct_coverage, query_identity, query_conservation


if __name__ == "__main__":
    # Parameters
    parser = argparse.ArgumentParser(
        usage='%(prog)s -i IN_XML -o OUT_GFF -n DB_NAME -t DB_TYPE [-d MIN_IDENT] [-c MIN_COV] [-s SPECIES] [-w DB_WEIGHT] [-g GI_2_GENE] [-a ACCESSION_2_GI]',
        description="Filter and builds an annotation file from a blastxml file."
    )
    parser.add_argument("-a", "--accession-2-gi", help="NCBI's file who contains link between gene ID and accession. Only needed if database contains NCBI ids.")
    parser.add_argument("-c", "--min-coverage", type=float, default=0.5, help="The minimum subject coverage value to keep an HSP in annotation.")
    parser.add_argument("-d", "--min-identity", type=float, default=0.5, help="The minimum identity value to keep an HSP in annotation.")
    parser.add_argument("-g", "--gi-to-gene", default=None, help="Path to NCBI's file who contains link between gene name and gene ID. Only needed if database contains NCBI ids.")
    parser.add_argument("-i", "--input", help="Path to the blastxml processed.")
    parser.add_argument("-n", "--db-name", help="The name of the databank.")
    parser.add_argument("-o", "--output", help="Path to the output annotation file.")
    parser.add_argument("-s", "--db-species", default=None, help="The species of the databank.")
    parser.add_argument("-t", "--db-type", choices=['genome', 'nucleic', 'protein', 'transcript', 'unknown'], help="the type of the databank.")
    parser.add_argument("-w", "--db-weight", default=None, type=float, help="The weight of the databank. It modifies the hsp's score calculation used during selection of the best annotation.")
    parser.add_argument("--version", action='version', version=__version__)
    args = parser.parse_args()

    # Process
    gene_name_from_accession = None
    if args.gi_to_gene is not None and args.accession_2_gi is not None:
        gene_name_from_accession = gene_name_by_accession( args.gi_to_gene, args.accession_2_gi )
    annot_fh = AnnotationFile( args.output )
    blast_fh = open( args.input )
    blast_records = NCBIXML.parse( blast_fh )
    for blast_record in blast_records:
        query_id = blast_record.query.split()[0]
        query_length = blast_record.query_length
        aln_type, aln_query_ratio, aln_sbjct_ratio = getAlnType( blast_record )
        for alignment in blast_record.alignments:
            subject_length = alignment.length
            best_hsp = None
            others_kept_hsp = list()
            # Filter HSP and find the best HSP for the hit
            score = -1
            for hsp in alignment.hsps:
                identity = float(hsp.identities) / float(hsp.align_length)
                query_coverage =  (aln_query_ratio * (hsp.align_length - hsp.query.count('-'))) / float(query_length)
                subject_coverage =  (aln_query_ratio * (hsp.align_length - hsp.sbjct.count('-'))) / float(subject_length)
                # If hsp is kept
                if identity >= float(args.min_identity) and subject_coverage >= float(args.min_coverage):
                    if hsp.score > score:
                        if score != -1:
                            others_kept_hsp.append( best_hsp )
                        score = hsp.score
                        best_hsp = hsp
                    else:
                        others_kept_hsp.append( hsp )
            # If it exist many kept hsp for the current hit
            if len(others_kept_hsp) > 0:
                hit_query_coverage, hit_subject_coverage, hit_query_identities, hit_query_positives = hit_stat( best_hsp, others_kept_hsp, aln_type )
            # If it exist only one HSP
            elif best_hsp is not None:
                hit_query_coverage = aln_query_ratio * (best_hsp.align_length - best_hsp.query.count('-'))
                hit_subject_coverage = best_hsp.align_length - best_hsp.sbjct.count('-')
                hit_query_identities = aln_query_ratio * hsp.identities
                hit_query_positives = aln_query_ratio * hsp.positives
            # Create annotation record
            if best_hsp is not None:
                #store coverage of the best hsp of the hit.
                best_query_coverage =  (aln_query_ratio * (best_hsp.align_length - best_hsp.query.count('-'))) / float(query_length)
                alignment.hit_def = re.sub(r"\x01", "", alignment.hit_def) # clean hit_def
                annotation = AnnotationRecord.loadFromHsp( query_id, best_hsp, args.db_name )
                annotation.addToAttribute( "desc", alignment.hit_def )
                annotation.addToAttribute( "type", args.db_type )
                if args.db_weight is not None:
                    annotation.addToAttribute( "databank_weight", str(args.db_weight) )
                annotation.addToAttribute( "query_coverage", str(best_query_coverage) )
                prct_hit_sbjct_coverage = (float(hit_subject_coverage)/subject_length)*100
                annotation.addToAttribute( "perc_Scoverage", str(prct_hit_sbjct_coverage) )
                prct_hit_query_coverage = (float(hit_query_coverage)/query_length)*100
                annotation.addToAttribute( "perc_Qcoverage", str(prct_hit_query_coverage) )
                prct_hit_query_identities = (float(hit_query_identities)/query_length)*100
                annotation.addToAttribute( "perc_Qidentities", str(prct_hit_query_identities) )
                prct_hit_query_positives = (float(hit_query_positives)/query_length)*100
                annotation.addToAttribute( "perc_Qconserved", str(prct_hit_query_positives) )
                annotation.addToAttribute( "Name", subjectAccessionFromHit(alignment) )
                hit_name = geneNameFromHit( alignment, gene_name_from_accession )
                if hit_name is not None:
                    annotation.addToAttribute( "gene", hit_name )
                species = args.db_species
                if species is None:
                    species = spFromHit( alignment )
                if species is not None:
                    annotation.addToAttribute( "species", species )
                # Write annotation
                annot_fh.write( annotation )
    annot_fh.close()