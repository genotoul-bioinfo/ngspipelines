#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import re
import pickle
import numpy as np
from ngspipelines.biomart_db import *
from jflow.featureio import VCFReader
from jflow.featureio import GFF3IO
from jflow.seqio import SequenceReader

import utils as utils


class Variant(BiomartMain):

    GRAPH_CLASS_PARTITION = 100
    MAX_VARIANT_QUAL_IN_HISTOGRAM = 5000

    def get_mart_display_name(self):
        return "Variants"

    def define_fields(self):
        self.variant_name = BiomartField("variant_name", DBTypes.VARCHAR, size=124, is_primary=True, favorites_position=1)
        self.analysis_id = BiomartField("analysis_id", DBTypes.INT, size=4)
        self.type = BiomartField("type", DBTypes.VARCHAR, size=20, is_key=True, favorites_position=2)
        self.quality = BiomartField("quality", DBTypes.INT, size=5)
        self.alleles = BiomartField("alleles", DBTypes.VARCHAR, size=255, favorites_position=4)
        self.position = BiomartField("position", DBTypes.INT, size=4, favorites_position=5)
        self.previous_variant = BiomartField("previous_variant", DBTypes.INT, size=4)
        self.next_variant = BiomartField("next_variant", DBTypes.INT, size=4)
        self.previous_seq = BiomartField("previous_seq", DBTypes.MEDIUMTEXT)
        self.next_seq = BiomartField("next_seq", DBTypes.MEDIUMTEXT)
        self.indel_length = BiomartField("indel_length", DBTypes.INT, size=11)
        self.best_annot_species = BiomartField("best_annot_species", DBTypes.VARCHAR, size=255, favorites_position=6)
        self.best_annot_gene = BiomartField("best_annot_gene", DBTypes.VARCHAR, size=255, favorites_position=7)
        self.best_annot_exon_five_prime_limit = BiomartField("best_annot_exon_five_prime_limit", DBTypes.INT, size=4)
        self.best_annot_exon_three_prime_limit = BiomartField("best_annot_exon_three_prime_limit", DBTypes.INT, size=4)
        self.best_annot_mutation_type = BiomartField("best_annot_mutation_type", DBTypes.VARCHAR, size=255, favorites_position=8)
        self.best_annot_strand = BiomartField("best_annot_strand", DBTypes.INT, size=11)
        self.best_annot_mutation_aa = BiomartField("best_annot_mutation_aa", DBTypes.VARCHAR, size=255)
        self.best_annot_known_variants = BiomartField("best_annot_known_variants", DBTypes.VARCHAR, size=45)
        self.contig_name = BiomartField("contig_name", DBTypes.VARCHAR, size=64, favorites_position=3)

        self.variant_name.add_filter( type = FilterTypes.UPLOAD, group = 'General')
        self.contig_name.add_filter( type = FilterTypes.UPLOAD, group = 'General')
        self.type.add_filter( type = FilterTypes.MULTI_SELECT, group = 'General')
        self.quality.add_filter( display_name = 'Quality >=',  qualifier = FilterQualifiers.EQUALS_GREATER, group = 'Variant quality')
        self.quality.add_filter( display_name = 'Quality <=',  qualifier = FilterQualifiers.EQUALS_LOWER, group = 'Variant quality')
        self.previous_variant.add_filter( display_name = 'Position >=',  qualifier = FilterQualifiers.EQUALS_GREATER, group = 'Previous variant')
        self.previous_variant.add_filter( display_name = 'Position <=',  qualifier = FilterQualifiers.EQUALS_LOWER, group = 'Previous variant')
        self.next_variant.add_filter( display_name = 'Position >=',  qualifier = FilterQualifiers.EQUALS_GREATER, group = 'Next variant')
        self.next_variant.add_filter( display_name = 'Position <=',  qualifier = FilterQualifiers.EQUALS_LOWER, group = 'Next variant')
        self.indel_length.add_filter( display_name = 'Length >=',  qualifier = FilterQualifiers.EQUALS_GREATER, group = 'Indel length')
        self.indel_length.add_filter( display_name = 'Length <=',  qualifier = FilterQualifiers.EQUALS_LOWER, group = 'Indel length')
        self.best_annot_gene.add_filter( group = 'Best annotation')
        self.best_annot_species.add_filter( group = 'Best annotation')
        self.best_annot_mutation_type.add_filter( type = FilterTypes.MULTI_SELECT, group = 'Best annotation')
        self.add_link_to_report(self.variant_name)

    def _add_best_annot(self, gff_entry):
        """
        @summary: Add an annotation entry in self.best_annot_info.
        @param gff_record: [GFF3Record] The annotation.
        """
        # Strand
        strand = "\N"
        if gff_entry.strand:
            strand = "1" if gff_entry.strand == "+" else "-1"
        # Subject
        hit_subject = "\N"
        if gff_entry.attributes.has_key("Target"):
            match = re.match( "(.+)\s\d+\s\d+(\s[+-])?", gff_entry.attributesToStr("Target") )
            hit_subject = match.group(1)
        # Store
        self.best_annot_info[gff_entry.attributesToStr("variant_id")] = {
                                               "best_annot_species": gff_entry.attributesToStr("species") if gff_entry.attributes.has_key("species") else "\N",
                                               "best_annot_gene" : gff_entry.attributesToStr("gene_name") if gff_entry.attributes.has_key("gene_name") else "\N",
                                               "best_annot_accession" : hit_subject,
                                               "best_annot_exon_five_prime_limit": gff_entry.attributesToStr("exon_5prim_barrier") if gff_entry.attributes.has_key("exon_5prim_barrier") else "\N",
                                               "best_annot_exon_three_prime_limit": gff_entry.attributesToStr("exon_3prim_barrier") if gff_entry.attributes.has_key("exon_3prim_barrier") else "\N",
                                               "best_annot_mutation_type": gff_entry.attributesToStr("protein_consequences")[2:] if gff_entry.attributes.has_key("protein_consequences") else "\N",
                                               "best_annot_strand": strand,
                                               "best_annot_mutation_aa": gff_entry.attributesToStr("aa_consequences") if gff_entry.attributes.has_key("aa_consequences") else "\N",
                                               "best_annot_region": gff_entry.attributesToStr("ref_region") if gff_entry.attributes.has_key("ref_region") else "\N",
                                               "best_annot_position_on_ref": gff_entry.attributesToStr("ref_position") if gff_entry.attributes.has_key("ref_position") else "\N",
                                               "best_annot_known_variants": gff_entry.attributesToStr("known_variants") if gff_entry.attributes.has_key("known_variants") else "\N",
                                               "best_annot_score": gff_entry.score if gff_entry.score else "\N" }


    def load(self, vcf, dna, annot_files=None, best_file=None):
        """
        @summary: Load variant and variant stat and store best_annot_info
        @param vcf: [str] The path to the variant file (VCF format).
        @param dna:
        @param annot_files: [str] The list of variant annotations files (with best annotation file).
        @param best_file: [str] The path to the best annotation file. Without this param the best annotation is determined by best score for the variant in all the annot_files.
        """
        reader = SequenceReader(dna)
        contigs_dna={}
        for sequence in reader:
            contigs_dna[sequence.name.split()[0]]=sequence.sequence
        

        # Load best annotations
        if annot_files is None: annot_files = list()
        self.best_annot_info = {}
        for gff in annot_files:
            reader = GFF3IO(gff, "r")
            for entry in reader:
                if best_file is not None and gff == best_file:
                    if self.best_annot_info.has_key(entry.attributesToStr("variant_id")):
                        raise Exception("Duplicated entry '" + entry.attributesToStr("variant_id") + "' in best annotation gff file " + gff)
                    self._add_best_annot( entry )
                elif best_file is None:
                    if not self.best_annot_info.has_key(entry.attributesToStr("variant_id")) or float(entry.score) > float(self.best_annot_info[entry.attributesToStr("variant_id")]["best_annot_score"]):
                        self._add_best_annot( entry )

        # Load VCF
        reader = VCFReader(vcf)
        previous_variant = None
        current_variant = None
        stats_type={}
        stats_alleles={"A":{"A":0,"C":0,"G":0,"T":0,"Multi":0},
                       "C":{"A":0,"C":0,"G":0,"T":0,"Multi":0},
                       "G":{"A":0,"C":0,"G":0,"T":0,"Multi":0},
                       "T":{"A":0,"C":0,"G":0,"T":0,"Multi":0}}
        stats_qual={ "SNP":{10:0, 20:0, 30:0, 40:0, 50:0, 100:0},
                     "INSERTION":{10:0, 20:0, 30:0, 40:0, 50:0, 100:0},
                     "DELETION":{10:0, 20:0, 30:0, 40:0, 50:0, 100:0} }
        contigs_with_SNP={}
        indel_size={}
        contigs_with_variant={}
        first=True
        self.favorite_names=[]
        for entry in reader:
            # Store quality for stat graph
            type = utils.get_variant_type(entry.ref, entry.alt)
            interval_max_qual = None
            for interval_max_qual in sorted(stats_qual[type].keys()):
                if int(entry.qual) <= interval_max_qual:
                    stats_qual[type][interval_max_qual] += 1
            if int(entry.qual) > interval_max_qual:
                while int(entry.qual) > interval_max_qual:
                    stats_qual[type][interval_max_qual*10] = stats_qual[type][interval_max_qual]
                    interval_max_qual = interval_max_qual*10
                stats_qual[type][interval_max_qual] += 1
            # Process entry
            if not contigs_with_variant.has_key(entry.chrom) : contigs_with_variant[entry.chrom] = 1
            if previous_variant == None:
                previous_variant = entry
            elif current_variant == None:
                current_variant = entry
            if previous_variant != None and current_variant != None:
                dist_previous = "\N"
                dist_next = "\N"
                if first:
                    if entry.chrom == current_variant.chrom:
                        dist_next = current_variant.pos - previous_variant.pos
                    first = False
                    self.add_variant(previous_variant, contigs_dna[previous_variant.chrom], dist_previous, dist_next, contigs_with_variant, contigs_with_SNP, stats_type, stats_alleles, indel_size)
                else:
                    if previous_variant.chrom == current_variant.chrom:
                        dist_previous = current_variant.pos - previous_variant.pos
                    if entry.chrom == current_variant.chrom:
                        dist_next = entry.pos - current_variant.pos
                    self.add_variant(current_variant, contigs_dna[current_variant.chrom], dist_previous, dist_next, contigs_with_variant, contigs_with_SNP, stats_type, stats_alleles, indel_size)
                    previous_variant = current_variant
                    current_variant = entry
        #last record
        dist_previous = "\N"
        if current_variant.chrom == entry.chrom:
            dist_previous = current_variant.pos - previous_variant.pos + 1
        self.add_variant(entry, contigs_dna[entry.chrom], dist_previous, dist_next, contigs_with_variant, contigs_with_SNP, stats_type, stats_alleles, indel_size)

        # Add all favorites
        for favorite_name in self.favorite_names:
            self.add_favorite(favorite_name)

        # Add a donut to the interface
        default_labels=[]
        default_values=[]
        dict_sub_labels_values={}
        for key,values in stats_type.items(): # SNP/INSERTION/DELETION/nb
            default_values.append(str(values["nb"]))
            default_labels.append(key)
            del values['nb']
            sub_keys = ','.join([key_mut for (key_mut) in sorted(stats_type[key].keys())])
            counts =  ','.join([str(values_mut) for (key_mut,values_mut) in sorted(stats_type[key].items())])
            dict_sub_labels_values[key]=(sub_keys, counts)
        self.add_donutchart(analysis_name="Variants donut",
                           soft_name="Variant.add_from_vcf",
                           labels=",".join(default_labels),
                           values=",".join(default_values),
                           dict_sub_label_group=dict_sub_labels_values,
                           comments="There is "+str(len(contigs_with_variant.keys()))+" contigs containing only SNPs, and " + str(len(contigs_with_SNP.keys())) + " contigs with variants (SNP, InDel ...). ",
                           chart_title="General",
                           tooltip="###X### <b>###Y###</b>",
                           chart_subtitle="statistics")

        # Add a statistic table on allele substitution
        transition = stats_alleles["A"]["G"] + stats_alleles["G"]["A"] + stats_alleles["T"]["C"] + stats_alleles["C"]["T"]
        transversion = stats_alleles["A"]["C"] + stats_alleles["A"]["T"] + stats_alleles["G"]["C"] + stats_alleles["G"]["T"]
        transversion += stats_alleles["C"]["A"] + stats_alleles["C"]["G"] + stats_alleles["T"]["G"] + stats_alleles["T"]["A"]

        self.add_table(analysis_name="Alleles substitution statistics", 
                       header=["Reference", "A", "C", "G", "T", "Multi allele"],
                       rows=[["A",stats_alleles["A"]["A"],stats_alleles["A"]["C"],stats_alleles["A"]["G"],stats_alleles["A"]["T"],stats_alleles["A"]["Multi"]],
                             ["C",stats_alleles["C"]["A"],stats_alleles["C"]["C"],stats_alleles["C"]["G"],stats_alleles["C"]["T"],stats_alleles["C"]["Multi"]],
                             ["G",stats_alleles["G"]["A"],stats_alleles["G"]["C"],stats_alleles["G"]["G"],stats_alleles["G"]["T"],stats_alleles["G"]["Multi"]],
                             ["T",stats_alleles["T"]["A"],stats_alleles["T"]["C"],stats_alleles["T"]["G"],stats_alleles["T"]["T"],stats_alleles["T"]["Multi"]]],
                       soft_name="Variant.load",
                       comments="Repartition of SNP allele substitutions. Do not take into account SNP with more than 2 alleles. The SNP are distribuated in "+ str(transition) + " transitions and " + str(transversion) + " transversions.",
                       chart_title="Alleles substitution",
                       chart_subtitle="statistics")

        # Add the length as an histogram to the interface
        labels, values = [], []
        for key in sorted(indel_size.keys()):
            if key != "\N":
                labels.append(key)
                values.append(indel_size[key])
        self.add_histogram(analysis_name="InDels size",
                           soft_name="Variant.add_from_vcf",
                           labels=labels,
                           values=values,
                           y_title="Number of InDel",
                           chart_title="InDel",
                           tooltip="<strong>###Y### indels</strong> with a size of <strong>###X###</strong>",
                           chart_subtitle="size distribution")

        # Add a the quality distribution histogram to the interface
        type_with_max = "SNP"
        if len(stats_qual["SNP"]) < len(stats_qual["INSERTION"]):
            type_with_max = "INSERTION"
        if type_with_max == "SNP" and len(stats_qual["SNP"]) < len(stats_qual["DELETION"]):
            type_with_max = "DELETION"
        elif type_with_max == "INSERTION" and len(stats_qual["INSERTION"]) < len(stats_qual["DELETION"]):
            type_with_max = "DELETION"
        self.add_basicline(analysis_name="Variants quality",
                           soft_name="Variant.add_from_vcf",
                           labels=[key for key in sorted(stats_qual[type_with_max].keys())],
                           values={ "snps" : [stats_qual["SNP"][key] for key in sorted(stats_qual["SNP"].keys())],
                                    "insertions" : [stats_qual["INSERTION"][key] for key in sorted(stats_qual["INSERTION"].keys())],
                                    "deletions" : [stats_qual["DELETION"][key] for key in sorted(stats_qual["DELETION"].keys())] },
                           y_title="Cumulative number of variants",
                           x_title="Variant quality",
                           x_type="logarithmic",
                           chart_title="Variant quality",
                           tooltip="<strong>###Y###</strong> variants have a quality <= <strong>###X###</strong>",
                           chart_subtitle="cumulative distribution")

    def _add_variant_stat(self, entry, stats_type, stats_alleles, stats_indel_size):
        # Variant name and type
        type = utils.get_variant_type(entry.ref, entry.alt)
        variant_name = entry.id
        if variant_name == "." or variant_name == "-":
            variant_name = utils.get_variant_name(entry.chrom, str(entry.pos), type)

        # Detailed type
        detailed_type = type
        if type == "SNP":
            if len(entry.alt) == 1:
                detailed_type = "Biallelic SNP"
                # transformation stats
                stats_alleles[entry.ref][entry.alt[0]] = stats_alleles[entry.ref][entry.alt[0]]+1
            else:
                detailed_type = "Other SNP"
                # transformation stats
                stats_alleles[entry.ref]["Multi"] = stats_alleles[entry.ref]["Multi"]+1

        # Indel count
        len_indel="\N"
        if detailed_type == "INSERTION":
            len_indel = len(max(entry.alt, key=len))-len(entry.ref)
        elif detailed_type == "DELETION":
            len_indel = len(min(entry.alt, key=len))-len(entry.ref)
        stats_indel_size[len_indel] = stats_indel_size[len_indel]+1 if stats_indel_size.has_key(len_indel) else 1

        # Count by type and by mutation consequences
        if stats_type.has_key(detailed_type):
            stats_type[detailed_type]["nb"] = stats_type[detailed_type]["nb"] + 1
        else:
            stats_type[detailed_type] = {}
            stats_type[detailed_type]["nb"] = 1

        mutation_type = "\N"
        if self.best_annot_info.has_key(variant_name):
            if detailed_type != "Other SNP":
                mutation_type = self.best_annot_info[variant_name]["best_annot_mutation_type"].lower().replace("_"," ").replace(',', ' ; ')
            elif self.best_annot_info[variant_name]["best_annot_mutation_type"] != "\N":
                mutation_type = "annotated"
        if mutation_type == "\N":
            mutation_type = "no annotation"

        if stats_type[detailed_type].has_key(mutation_type):
            stats_type[detailed_type][mutation_type]= stats_type[detailed_type][mutation_type] + 1
        else:
            stats_type[detailed_type][mutation_type] = 1

    def add_variant(self, entry, dna, dist_previous, dist_next, contigs_with_variant, contigs_with_SNP, stats_type, stats_alleles, stats_indel_size):
        self._add_variant_stat(entry, stats_type, stats_alleles, stats_indel_size)

        # Variant name and type
        type = utils.get_variant_type(entry.ref, entry.alt)
        variant_name = entry.id
        if variant_name == "." or variant_name == "-":
            variant_name = utils.get_variant_name(entry.chrom, str(entry.pos), type)

        # InDel length
        len_indel="\N"
        if type == "INSERTION":
            len_indel = len(max(entry.alt, key=len))-len(entry.ref)
        elif type == "DELETION":
            len_indel = len(min(entry.alt, key=len))-len(entry.ref)

        # Select default favorites
        if len(self.favorite_names) < 5:
            if  len(self.best_annot_info.keys()) > 0:
                if self.best_annot_info.has_key(variant_name):
                    self.favorite_names.append(variant_name)
            else:
                self.favorite_names.append(variant_name)

        # Add variant
        if not contigs_with_variant.has_key(entry.chrom) : contigs_with_variant[entry.chrom] = 1
        if not contigs_with_SNP.has_key(entry.chrom) and type == "SNP": contigs_with_SNP[entry.chrom] = 1

        (previous_seq, next_seq) = utils.get_flanking_region(entry.pos, dna)

        self.add_row({ "variant_name": variant_name,
                       "analysis_id": "\N",
                       "type": type,
                       "quality": entry.qual,
                       "alleles": entry.ref + "/" + "/".join(entry.alt),
                       "position": entry.pos,
                       "previous_variant": dist_previous,
                       "next_variant": dist_next,
                       "previous_seq" : previous_seq,
                       "next_seq" : next_seq,
                       "best_annot_species" : self.best_annot_info[variant_name]["best_annot_species"] if self.best_annot_info.has_key(variant_name) else "\N",
                       "best_annot_gene" : self.best_annot_info[variant_name]["best_annot_gene"] if self.best_annot_info.has_key(variant_name) else "\N",
                       "best_annot_exon_five_prime_limit" : self.best_annot_info[variant_name]["best_annot_exon_five_prime_limit"] if self.best_annot_info.has_key(variant_name) else "\N",
                       "best_annot_exon_three_prime_limit" : self.best_annot_info[variant_name]["best_annot_exon_three_prime_limit"] if self.best_annot_info.has_key(variant_name) else "\N",
                       "best_annot_mutation_type" : self.best_annot_info[variant_name]["best_annot_mutation_type"] if self.best_annot_info.has_key(variant_name) else "\N",
                       "best_annot_strand" : self.best_annot_info[variant_name]["best_annot_strand"] if self.best_annot_info.has_key(variant_name) else "\N",
                       "best_annot_mutation_aa" : self.best_annot_info[variant_name]["best_annot_mutation_aa"] if self.best_annot_info.has_key(variant_name) else "\N",
                       "best_annot_known_variants" : self.best_annot_info[variant_name]["best_annot_known_variants"] if self.best_annot_info.has_key(variant_name) else "\N",
                       "indel_length" : len_indel,
                       "contig_name" : entry.chrom })


class Annotation(BiomartDimension):

    def define_fields(self):
        self.hit_accession = BiomartField("hit_accession", DBTypes.VARCHAR, size=64, is_key=True)
        self.species = BiomartField("species", DBTypes.VARCHAR, size=255)
        self.gene = BiomartField("gene", DBTypes.VARCHAR, size=64, is_key=True)

        self.gene_description = BiomartField("gene_description", DBTypes.MEDIUMTEXT)
        self.exon_five_prime_limit = BiomartField("exon_five_prime_limit", DBTypes.INT, size=4)
        self.exon_three_prime_limit = BiomartField("exon_three_prime_limit", DBTypes.INT, size=4)
        self.mutation_type = BiomartField("mutation_type", DBTypes.VARCHAR, size=64)
        self.mutation_aa = BiomartField("mutation_aa", DBTypes.VARCHAR, size=255)
        self.region = BiomartField("region", DBTypes.VARCHAR, size=64)
        self.strand = BiomartField("strand", DBTypes.INT, size=4)
        self.position_on_ref = BiomartField("position_on_ref", DBTypes.INT, size=10)
        self.known_variants = BiomartField("known_variants", DBTypes.VARCHAR, size=64)
        self.evalue = BiomartField("evalue", DBTypes.DOUBLE)
        self.score = BiomartField("score", DBTypes.VARCHAR, size=64)
        self.identities = BiomartField("identities", DBTypes.INT)
        self.positives = BiomartField("positives", DBTypes.INT)
        self.align_length = BiomartField("align_length", DBTypes.INT)
        self.is_best_annot = BiomartField("is_best_annot", DBTypes.BOOLEAN)
        self.analysis_id = BiomartField("analysis_id", DBTypes.INT, size=4)

        self.hit_accession.add_filter( type =  FilterTypes.UPLOAD)
        self.gene.add_filter( type =  FilterTypes.UPLOAD)
        self.gene_description.add_filter( display_name = 'Gene description (use % as joker)', qualifier = FilterQualifiers.LIKE)
        self.mutation_type.add_filter( type =  FilterTypes.MULTI_SELECT)
        self.is_best_annot.add_filter( type =  FilterTypes.BOOLEAN)

    def load(self, gff_files, variant):
        #TO BE CHANGED
        # retrieve best annotation from the contig object
        
        best_annot = variant.best_annot_info

        for gff in gff_files:
            reader = GFF3IO(gff, "r")
            for entry in reader:
                # Strand
                strand = "\N"
                if entry.strand:
                    strand = "1" if entry.strand == "+" else "-1"
                # Subject
                hit_subject = "\N"
                if entry.attributes.has_key("Target"):
                    match = re.match( "(.+)\s\d+\s\d+(\s[+-])?", entry.attributesToStr("Target") )
                    hit_subject = match.group(1)
                # Is best
                is_best="\N"
                variant_id = entry.attributesToStr("variant_id")
                if best_annot.has_key(variant_id) :
                    if (entry.attributes.has_key("Target") and best_annot[variant_id]["best_annot_accession"] == hit_subject) and \
                       (entry.attributes.has_key("ref_position") and best_annot[variant_id]["best_annot_position_on_ref"] == entry.attributesToStr("ref_position")) and \
                       (entry.attributes.has_key("ref_region") and best_annot[variant_id]["best_annot_region"] == entry.attributesToStr("ref_region")):
                        is_best="1"
                self.add_row( variant_id, {
                                  "hit_accession" : hit_subject,
                                  "gene" : entry.attributesToStr("gene_name") if entry.attributes.has_key("gene_name") else "\N",
                                  "gene_description" : entry.attributesToStr("gene_descr") if entry.attributes.has_key("gene_descr") else "\N",
                                  "exon_five_prime_limit" : entry.attributesToStr("exon_5prim_barrier") if entry.attributes.has_key("exon_5prim_barrier") else "\N",
                                  "exon_three_prime_limit" : entry.attributesToStr("exon_3prim_barrier") if entry.attributes.has_key("exon_3prim_barrier") else "\N",
                                  "mutation_type" : entry.attributesToStr("protein_consequences")[2:] if entry.attributes.has_key("protein_consequences") else "\N",
                                  "mutation_aa" :  entry.attributesToStr("aa_consequences") if entry.attributes.has_key("aa_consequences") else "\N",
                                  "strand" : strand,
                                  "species" : entry.attributesToStr("species") if entry.attributes.has_key("species") else "\N",
                                  "region" : entry.attributesToStr("ref_region"),
                                  "position_on_ref" : entry.attributesToStr("ref_position") if entry.attributes.has_key("ref_position") else "\N",
                                  "known_variants" : entry.attributesToStr("known_variants") if entry.attributes.has_key("known_variants") else "\N",
                                  "identities" : entry.attributesToStr("identities") if entry.attributes.has_key("identities") else "\N",
                                  "positives" : entry.attributesToStr("positives") if entry.attributes.has_key("positives") else "\N",
                                  "align_length" : entry.attributesToStr("align_length") if entry.attributes.has_key("align_length") else "\N",
                                  "evalue" :  entry.attributesToStr("evalue") if entry.attributes.has_key("evalue") else "\N",
                                  "score" : entry.score,
                                  "is_best_annot" : is_best,
                                  "analysis_id" : "\N"})

class Effect(BiomartDimension):

    def define_fields(self):
        
        # Effect ( Effect_Impact | Functional_Class | Codon_Change | Amino_Acid_Change| Amino_Acid_Length | Gene_Name | Transcript_BioType | Gene_Coding | Transcript_ID | Exon_Rank  | Genotype_Number [ | ERRORS | WARNINGS ] )
        #EFF=UTR_3_PRIME(MODIFIER||1243|||ORF%20type%3Acomplete%20len%3A246%20%28-%29|||Gene.2::test_143B1::g.2::m.2|1|1)
        #ANN=T|missense_variant|MODERATE|ORF%20type%3Acomplete%20len%3A100%20%28-%29|Gene.6::test_1433E::g.6|transcript|Gene.6::test_1433E::g.6::m.6|protein_coding|1/1|c.80C>A|p.Ser27Tyr|1749/1991|80/300|27/99||
        #Allele | Annotation | Annotation_Impact | Gene_Name | Gene_ID | Feature_Type | Feature_ID | Transcript_BioType | Rank | HGVS.c | HGVS.p | cDNA.pos / cDNA.length | CDS.pos / CDS.length | AA.pos / AA.length | Distance | ERRORS / WARNINGS / INFO' 
        self.allele = BiomartField("allele", DBTypes.VARCHAR, size=255)
        self.annotation = BiomartField("annotation", DBTypes.VARCHAR, size=255)
        self.annotation_impact = BiomartField("annotation_impact", DBTypes.VARCHAR, size=255)
        self.feature_id = BiomartField("feature_id", DBTypes.VARCHAR, size=255)
        self.feature_type = BiomartField("feature_type", DBTypes.VARCHAR, size=255)
        self.transcript_biotype = BiomartField("transcript_biotype", DBTypes.VARCHAR, size=255)
        self.hgvs_c = BiomartField("hgvs_c", DBTypes.VARCHAR, size=255)
        self.hgvs_p = BiomartField("hgvs_p", DBTypes.VARCHAR, size=255)
        self.cdna_pos_len = BiomartField("cdna_pos_len", DBTypes.VARCHAR, size=255)
        self.cds_pos_len = BiomartField("cds_pos_len", DBTypes.VARCHAR, size=255)
        self.aa_pos_len = BiomartField("aa_pos_len", DBTypes.VARCHAR, size=255)
        self.warning = BiomartField("warning", DBTypes.VARCHAR, size=255)
        
        self.annotation.add_filter( type =  FilterTypes.MULTI_SELECT)
        self.annotation_impact.add_filter( type =  FilterTypes.MULTI_SELECT)
        
    def load(self, vcf):
        """
        @param vcf : vcf file
        """
        #ANN=A|3_prime_UTR_variant|MODIFIER|xxx|xxx|transcript|Gene.571::RL12_MOUSE.2.2::g.571::m.571|protein_coding|1/1|c.*58T>A|||||58|
        eff_re42 = re.compile('([^|]*)\|([^|]*)\|([^|]*)\|[^|]*\|([^|]*)\|([^|]*)\|([^|]*)\|([^|]*)\|([^|]*)\|([^|]*)\|([^|]*)\|([^|]*)\|([^|]*)\|([^|]*)\|[^|]*\|([^|]*)')
        #EFF= Effect ( Effect_Impact | Functional_Class | Codon_Change | Amino_Acid_Change| Amino_Acid_Length | Gene_Name | Transcript_BioType | Gene_Coding | Transcript_ID | Exon_Rank  | Genotype_Number [ | ERRORS | WARNINGS ] )
        #EFF=NONE(MODIFIER||||||||||1|ERROR_CHROMOSOME_NOT_FOUND) 
        #EFF=UTR_3_PRIME(MODIFIER||80|||ORF%20type%3Acomplete%20len%3A472%20%28-%29|||Gene.205::LOC101068574_TAKRU.1.1::g.205::m.205|1|1)
        #EFF=NON_SYNONYMOUS_CODING(MODERATE|MISSENSE|gAt/gTt|D131V||ORF%20type%3Acomplete%20len%3A132%20%28%2B%29|||Gene.207::LOC100136196_ONCMY.1.2::g.207::m.207|1|1)
        #STOP_LOST(HIGH|MISSENSE|Taa/Aaa|*105K||ORF%20type%3Acomplete%20len%3A105%20%28%2B%29|||Gene.199::LOC100711909_ORENI.1.4::g.199::m.199|1|1)
        eff_re36 = re.compile('(\S+)\(([^|]+)\|([^|]*)\|([^|]*)\|([^|]*)\|(\d*)\|[^|]*\|[^|]*\|[^|]*\|([^|]*)\|\d*\|\d*\|?([^\)]*)\)')
        
        reader = VCFReader(vcf)
        for entry in reader:
            variant_name = entry.id
            if variant_name == "." or variant_name == "-":
                type = utils.get_variant_type(entry.ref, entry.alt)
                variant_name = utils.get_variant_name(entry.chrom, str(entry.pos), type)
            if entry.info.has_key('ANN') :
                for eff in entry.info['ANN'].split(',') :
                    result = eff_re42.match(eff)
                    if result :
                        (allele,annotation,annotation_impact,feature_id,feature_type,
                         transcript_biotype,hgvs_c,hgvs_p,
                         cdna_pos_len,cds_pos_len,aa_pos_len,warning)=result.group(1,2,3,6,5,7,9,10,11,12,13,14)
                        
                        self.add_row(variant_name, {"allele":allele if allele else "\N",
                                                    "annotation": annotation if annotation else "\N",
                                                    "annotation_impact": annotation_impact if annotation_impact else "\N",
                                                    "feature_id" :feature_id if feature_id else "\N",
                                                    "feature_type": feature_type  if feature_type else "\N",
                                                    "transcript_biotype" : transcript_biotype if transcript_biotype else "\N",
                                                    "hgvs_c" : hgvs_c if hgvs_c else "\N",
                                                    "hgvs_p" : hgvs_p if hgvs_p else "\N",
                                                    "cdna_pos_len" : cdna_pos_len if cdna_pos_len else "\N",
                                                    "cds_pos_len" : cds_pos_len if cds_pos_len else "\N",
                                                    "aa_pos_len" : aa_pos_len if aa_pos_len else "\N",
                                                    "warning" : warning if warning else "\N"
                                                    })
            if entry.info.has_key('EFF') :
                for eff in entry.info['EFF'].split(',') :
                    result = eff_re36.match(eff)
                    if result :
                        (annotation,annotation_impact,functional_class,codon_change,amino_acid_change,amino_acid_length,feature_id)=result.group(1,2,3,4,5,6,7)
                        warning=None
                        if result.group(8) :
                            warning=result.group(8)                                                                                              
                        
                        self.add_row(variant_name, {"feature_id":feature_id if feature_id else "\N",
                                                    "annotation": annotation if annotation else "\N",
                                                    "annotation_impact": annotation_impact if annotation_impact else "\N",
                                                    "hgvs_p": amino_acid_change if amino_acid_change else "\N",
                                                    "hgvs_c" : codon_change  if codon_change else "\N",                                                    
                                                    "warning" : warning if warning else "\N"
                                                    })    
class CountAlleleLib(BiomartDimension):

    def define_fields(self):
        self.library_id = BiomartField("library_id", DBTypes.INT, size=10,is_key=True)
        self.sample_name = BiomartField("sample_name", DBTypes.VARCHAR, size=255)
        self.replicat = BiomartField("replicat", DBTypes.INT, size=4)
        self.allele = BiomartField("allele", DBTypes.VARCHAR, size=255, is_key=True)
        self.nb_read = BiomartField("nb_read", DBTypes.BIGINT, size=22)
        self.frequency = BiomartField("frequency", DBTypes.FLOAT)
        self.tissue = BiomartField("tissue", DBTypes.VARCHAR, size=124, is_key=True)
        self.dev_stage = BiomartField("dev_stage", DBTypes.VARCHAR, size=124, is_key=True)

        self.sample_name.add_filter( type = FilterTypes.MULTI_SELECT, group = 'General')
        self.tissue.add_filter( type = FilterTypes.MULTI_SELECT, group = 'General')
        self.dev_stage.add_filter( type = FilterTypes.MULTI_SELECT, group = 'General')

        self.nb_read.add_filter( display_name = 'Number >=',  qualifier = FilterQualifiers.EQUALS_GREATER, group = 'Number of read')
        self.nb_read.add_filter( display_name = 'Number <=',  qualifier = FilterQualifiers.EQUALS_LOWER, group = 'Number of read')

        self.frequency.add_filter( display_name = 'Frequency >=',  qualifier = FilterQualifiers.EQUALS_GREATER, group = 'Frequency')
        self.frequency.add_filter( display_name = 'Frequency <=',  qualifier = FilterQualifiers.EQUALS_LOWER, group = 'Frequency')

    def load(self, vcf, libraries):
        """
        @param vcf : vcf file
        @param libraries : list of libraries
        """
        stats_maf={}
        lib_dict={}
        for (lib_index,lib) in enumerate(libraries) :
            lib_dict[lib.library_name]=lib_index
            stats_maf[str(lib.sample_name) + '.' + str(lib.replicat)]=[]

        reader = VCFReader(vcf)
        for entry in reader:
            variant_name = entry.id
            if variant_name == "." or variant_name == "-":
                type = utils.get_variant_type(entry.ref, entry.alt)
                variant_name = utils.get_variant_name(entry.chrom, str(entry.pos), type)
            allele_list = [entry.ref] + entry.alt
            for (i, sample) in enumerate(entry.samples):
                allelic_counts = sample.AD.split(',') if (sample.has('AD') and sample.AD != None) else [0 for a in allele_list]
                lib_index = lib_dict[reader.samples_name[i][1]]
                position_depth = sum(map(int,allelic_counts)) 
                list_af=[]
                for (a_index,allele) in enumerate(allele_list) :

                    freq = 0 if position_depth == 0 else round(float(allelic_counts[a_index])/float(position_depth)*100,2)
                    list_af.append(freq)
                    self.add_row(variant_name, {"library_id": libraries[lib_index].id,
                                                "sample_name" :libraries[lib_index].sample_name,
                                                "replicat": libraries[lib_index].replicat,
                                                "allele" : allele,
                                                "nb_read" : allelic_counts[a_index],
                                                "frequency" : freq,
                                                "tissue": libraries[lib_index].tissue,
                                                "dev_stage": libraries[lib_index].dev_stage
                                                })

                stats_maf[str(libraries[lib_index].sample_name) + '.' + str(libraries[lib_index].replicat)].append(float(min(list_af)))

        labels=""
        values={}
        for lib_name in sorted(stats_maf.keys()) :
            hist, edges = np.histogram(sorted(stats_maf[lib_name]), bins=51, range=(0,50))
            values[lib_name]=hist
            labels = edges

        self.add_histogram(analysis_name="Minimun Allele Frequency",
                       soft_name="CountAlleleLib.load",
                       labels=edges,
                       values=values,
                       y_title="Number of variant ",
                       x_title="Minimum Allele Frequency (in %)",
                       chart_title="Minimum Alleles frequency ",
                       tooltip="There is <strong>###Y### variants</strong> ",
                       chart_subtitle="of Variants")