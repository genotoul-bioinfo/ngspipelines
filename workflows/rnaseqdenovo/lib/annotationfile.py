import sys, re
from jflow.featureio import GFF3Record

class AnnotationRecord( GFF3Record ):
    """
     @summary : Record for annotation.
    """
    def innerScore( self, database_weight=None ):
        """
         @summary : Returns the inner score of the annotation. Formula : (identity*coverage) + weight * (identity*coverage).
          @param database_weight : [float] the database weight.
         @return : [float] the score.
        """
        inner_score = None
        
        if self.attributes.has_key('query_identity') and self.attributes.has_key('query_coverage'):
            if database_weight is None and self.attributes.has_key("databank_weight"):
                database_weight = self.attributes["databank_weight"]
            elif database_weight is None and not self.attributes.has_key("databank_weight"):
                database_weight = AnnotationRecord.databaseWeight( self.source )
            score = float(self.attributes['query_identity']) * float(self.attributes['query_coverage'])
            inner_score = score + float(database_weight) * score
        else:
            raise self.__class__.__name__ + ".innerScore() needs values for 'query_identity' and 'query_coverage'."
        
        return inner_score

    @staticmethod
    def fromGff( line ):
        """
         @summary : Returns an AnnotationRecord from a GFF3 line.
          @param line : [str] line of the GFF.
         @return : [AnnotationRecord] the annotation build from the GFF3 line.
        """
        gff_record= GFF3Record.fromGff( line )
        annot = AnnotationRecord()
        annot.seq_id = gff_record.seq_id
        annot.source = gff_record.source
        annot.type = gff_record.type
        annot.start = gff_record.start
        annot.end = gff_record.end
        annot.score = gff_record.score
        annot.strand = gff_record.strand
        annot.phase = gff_record.phase
        annot.attributes = gff_record.attributes
        return annot

    @staticmethod
    def loadFromHsp(seq_id, hsp, databank_name):
        """
         @summary : Returns an AnnotationRecord from an HSP.
          @param seq_id : [str] the sequence ID.
          @param hsp : [Bio.Blast.Record.HSP] the HSP processed.
          @param databank_name : [str] the databank's name.
         @return : [AnnotationRecord] the annotation.
        """
        annot = AnnotationRecord()
        annot.seq_id = seq_id
        annot.source = databank_name
        annot.type = "match_part"
        annot.start = hsp.query_start
        annot.end = hsp.query_end
        annot.score = hsp.bits
        annot.strand = "+"
        if hsp.strand[0] is not None :  
            annot.strand = hsp.strand[0]
        annot.phase = "."
        if hsp.frame[0] is not None :
            annot.phase = abs(hsp.frame[0])-1
            if hsp.strand[0] is None and hsp.frame[0] < 0:
                annot.strand = "-"
        annot.attributes = {
                  'evalue'     : str( hsp.expect ),      # e-value
                  'sbjt_start' : str( hsp.sbjct_start ), # start on the subject
                  'sbjt_stop'  : str( hsp.sbjct_end ),   # stop on the subject
                  'perc_ident' : str( (float(hsp.identities) / float(hsp.align_length))*100 ),  # Percent of identity for the HSP alignment
                  'query_identity' : str( float(hsp.identities) / float(hsp.align_length) )
        }

        return annot
     
    @staticmethod
    def databaseWeight( database ):
        """
         @summary : Returns the database weight for the annotation inner score.
          @param database : [str] the database name ('swissprot', 'refseq', 'ensembl', ...).
         @return : [float] the weight.
        """
        weight = 0.0 # Default value
         
        databases_weights = {
            'swissprot' : 0.05,
            'refseq'    : 0.02
        }
         
        if databases_weights.has_key(database) :
            weight = databases_weights[database]
        else:
            sys.stderr.write("[WARNING] hsp inner score doesn't have weight for database '" + database + "'. [Default weight : " + str(weight) + "]\n")

        return weight


class AnnotationFile:
    """
     @summary : Specific handler for annotation file.
      Example of usage :
        .................................................................................................
        # Convert blast file to annotation file
        annot_file = AnnotationFile( args.annotations )
        for hsp in alignment.hsps:
            annotation = AnnotationRecord.loadFromHsp( query_id, database_name, query_length, hsp )
            annotation.addToAttribute("Desc", alignment.hit_def)
            annot_file.write(annotation)
        .................................................................................................
        # Change sequences IDs
        new_annotations = AnnotationFile( new_annotation_file, "w" )
        old_annotations = AnnotationFile( old_annotation_file, "r" )
        for current_annotation in old_annotations:
            current_annotation.seq_id = rename[current_annotation.seq_id]
            new_annotations.write( current_annotation )
        .................................................................................................
    """
    def __init__( self, file_path, mode="w" ):
        self._path = file_path
        self._handle = open( file_path, mode )
        self._line = 1
    
    def __del__( self ):
        self.close()
        
    def __iter__(self):
        for line in self._handle:
            line = line.rstrip()
            self._line += 1
            if line.startswith('#') :
                continue
            try:
                annotation = AnnotationRecord.fromGff( line )
            except:
                raise IOError( "The line " + str(self._line) + " cannot be parsed by " + self.__class__.__name__ + ".\n" +
                               "Line content : " + line )
            else:
                yield annotation
    
    def close( self ) :
        if self._handle is not None:
            self._handle.close()

    def write( self, annotation_record ):
        """
         @summary : Write one line of GFF3 form an annotation record.
          @param annotation_records : [AnnotationRecord] the annotation to write.
        """
        self._handle.write( annotation_record.toGff() + "\n" )
        self._line += 1