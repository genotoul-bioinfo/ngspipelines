#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import math
import subprocess
import functools
from subprocess import Popen, PIPE

from jflow.config_reader import JFlowConfigReader


def get_vcf_unmatch_contigs(contigs, filepath):
    '''
        Check that the contigs present in the vcf file are present in the fasta file
        @param contigs: dictionary {string : size }
        @param filepath: path to a vcf file
    '''
    err_contigs=[]
    for name in subprocess.check_output( 'grep -v "^#" ' + filepath + ' | cut -f1,1 | sort | uniq ', shell = True).split('\n') :
        if name == "" : continue
        if not contigs.has_key(name) :
            err_contigs.append(name) 
    return (err_contigs)

def get_gff3_unmatch_contigs(contigs, filepath):
    '''
        Check that the contigs present in the gff3 file are present in the fasta file
        @param contigs: dictionary {string : size }
        @param filepath: path to a gff3 file
    '''
    err_contigs=[]
    for name in subprocess.check_output( 'grep -v "^#" ' + filepath + ' | cut -f1,1 | sort | uniq ', shell = True).split('\n') :
        if name == "" : continue
        if not contigs.has_key(name) :
            err_contigs.append(name) 
    return (err_contigs)

def get_BAM_unmatch_contigs(contigs, filepath, samtools_path):
    '''
        Check that the contigs present in the BAM file are present in the fasta file
        @param contigs: dictionary {string : size }
        @param filepath: path to a wego file
    '''
    err_contigs=[]
    for name in subprocess.check_output( samtools_path+' view -H ' + filepath + ' | grep "@SQ" | cut -f2,2 | sed -e "s/^SN://" ', shell = True).split('\n') :
        if name == "" : continue
        if not contigs.has_key(name) :
            err_contigs.append(name)  
    return (err_contigs)

def get_GO_unmatch_contigs(contigs, filepath):
    '''
        Check that the contigs present in the GO file are present in the fasta file
        @param contigs: dictionary {string : size }
        @param filepath: path to a GO file
    '''
    return get_unmatch_contigs_first_column(contigs, filepath)


def get_keyword_unmatch_contigs(contigs, filepath):
    '''
        Check that the contigs present in the GO file are present in the fasta file
        @param contigs: dictionary {string : size }
        @param filepath: path to a GO file
    '''
    return get_unmatch_contigs_first_column(contigs, filepath)  
        
def get_unmatch_contigs_first_column(contigs, filepath):
    '''
        Check that the contigs present in the GO file are present in the fasta file
        @param contigs: dictionary {string : size }
        @param filepath: path to a GO file
    '''
    err_contigs=[]
    for name in subprocess.check_output('cut -f1,1 ' + filepath + '| sort | uniq ', shell = True).split('\n') :
        if name == "" or name.startswith("#"): continue
        if not contigs.has_key(name) :
            err_contigs.append(name) 
    return (err_contigs)
        
def get_variant_type(ref,alleles):
    """
        @param ref : base on reference
        @param alleles : list of alleles
        Return the type of SNP: INSERTION DELETION SNP
    """
    type = "SNP"
    ref_len = len(ref)

    for a in alleles :
        if len(a) > ref_len :
            type = "INSERTION"
            return (type)
        elif len(a) < ref_len or a == "-":
            type = "DELETION"
            return (type)
    if ref_len > 1 :
        type = "VARIATION"
    return (type)

def get_flanking_region(position, dna_string):
    flanking_len=100
    start = position - flanking_len if (position - flanking_len )>=0 else 0
    end = position + flanking_len  if (position + flanking_len )<=len(dna_string) else len(dna_string)
        
    previous = dna_string [start : position-1]
    next = dna_string [position : end]
    return (previous,next)

def get_variant_name(seq_name,position,type):
    return (seq_name+"_"+str(position)+"_"+type[:3])
