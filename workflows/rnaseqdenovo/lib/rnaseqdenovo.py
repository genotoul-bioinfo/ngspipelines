#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy as np
import re 
import pickle
import operator

from ngspipelines.biomart_db import *
from jflow.seqio import SequenceReader
from jflow.seqio import FastaReader
from jflow.featureio import GFF3IO
from jflow.featureio import VCFReader

from ngspipelines.analysis import *
from ngspipelines.utils import min, first_quartile, median, third_quartile, max

import utils as utils

class Contig(BiomartMain):

    GRAPH_CLASS_PARTITION = 30
    BEST_ANNOTATION_MAX_SPECIES = 20
    MAX_CONTIG_LEN_IN_HISTOGRAM = 5000
    MAX_DEPTH_DOTS = 5000

    def get_mart_display_name(self):
        return "Contigs"

    def define_fields(self):
        self.name = BiomartVARCHARField("name", size=64, is_primary=True, favorites_position=1)
        self.length = BiomartINTField("length", size=4, favorites_position=2)
        self.depth = BiomartFLOATField("depth", favorites_position=3)
        self.best_hit_database = BiomartVARCHARField("best_hit_database", size=255, favorites_position=4)
        self.best_hit_species = BiomartVARCHARField("best_hit_species", size=255)
        self.best_hit_gene = BiomartVARCHARField("best_hit_gene", size=255)
        self.best_hit_accession = BiomartVARCHARField("best_hit_accession", size=255, favorites_position=5)
        self.best_hit_description = BiomartMEDIUMTEXTField("best_hit_description", favorites_position=6)
        self.best_hit_query_start = BiomartINTField("best_hit_query_start", size=10)
        self.best_hit_query_stop = BiomartINTField("best_hit_query_stop", size=10)
        self.best_hit_subject_start = BiomartINTField("best_hit_subject_start", size=10)
        self.best_hit_subject_stop = BiomartINTField("best_hit_subject_stop", size=10)
        self.best_hit_evalue = BiomartDOUBLEField("best_hit_evalue")
        self.best_hit_score = BiomartINTField("best_hit_score", size=10)
        self.best_hit_perc_ident = BiomartFLOATField("best_hit_perc_ident")
        self.best_hit_perc_Qcoverage = BiomartFLOATField("best_hit_perc_Qcoverage", favorites_position=7)
        self.best_hit_perc_Scoverage = BiomartFLOATField("best_hit_perc_Scoverage")
        self.best_hit_perc_Qidentities = BiomartFLOATField("best_hit_perc_Qidentities", favorites_position=8)
        self.best_hit_perc_Qconserved = BiomartFLOATField("best_hit_perc_Qconserved")
        self.name.add_filter(display_name = 'Name (one line per contig name)', type = FilterTypes.UPLOAD)
        self.length.add_filter(display_name = 'Length >=', qualifier = FilterQualifiers.EQUALS_GREATER, group = 'Length')
        self.length.add_filter(display_name = 'Length <=', qualifier = FilterQualifiers.EQUALS_LOWER, group = 'Length')
        self.depth.add_filter(display_name = 'Depth >=', qualifier = FilterQualifiers.EQUALS_GREATER, group = 'Depth')
        self.depth.add_filter(display_name = 'Depth <=', qualifier = FilterQualifiers.EQUALS_LOWER, group = 'Depth')
        self.best_hit_database.add_filter(type = FilterTypes.MULTI_SELECT, group = 'Best annotation filter')
        self.best_hit_species.add_filter(display_name = 'Best hit species (use % as joker)', qualifier = FilterQualifiers.LIKE, group = 'Best annotation filter')
        self.best_hit_accession.add_filter(type = FilterTypes.UPLOAD, group = 'Best annotation filter')
        self.best_hit_description.add_filter(display_name = 'Best hit description (use % as joker)', qualifier = FilterQualifiers.LIKE, group = 'Best annotation filter')
        self.best_hit_gene.add_filter(display_name = 'Best hit gene (use % as joker)', qualifier = FilterQualifiers.LIKE, group = 'Best annotation filter')
        self.add_link_to_report(self.name)

    def load(self, fasta_file, gff_files, best_annotation_source=None):

        self.best_annot_info, best_annot_sources, best_annot_species = {}, {}, {}
        for gff in gff_files:
            reader = GFF3IO(gff, "r")
            for entry in reader:
                #TODO : A verifier
                if not self.best_annot_info.has_key(entry.seq_id) or (best_annotation_source == entry.source and float(entry.score) > float(self.best_annot_info[entry.seq_id]["best_hit_score"])):
                    self.best_annot_info[entry.seq_id] = {"best_hit_database": entry.source,
                                                            "best_hit_accession" : entry.attributesToStr("Name") if entry.attributes.has_key("Name") else "\N",
                                                            "best_hit_species": entry.attributesToStr("species") if entry.attributes.has_key("species") else "\N",
                                                            "best_hit_gene": entry.attributesToStr("gene") if entry.attributes.has_key("gene") else "\N",
                                                            "best_hit_description": entry.attributesToStr("desc") if entry.attributes.has_key("desc") else "\N",
                                                            "best_hit_query_start": entry.start,
                                                            "best_hit_query_stop": entry.end,
                                                            "best_hit_subject_start": entry.attributesToStr("sbjt_start") if entry.attributes.has_key("sbjt_start") else "\N",
                                                            "best_hit_subject_stop": entry.attributesToStr("sbjt_stop") if entry.attributes.has_key("sbjt_stop") else "\N",
                                                            "best_hit_evalue": entry.attributesToStr("evalue") if entry.attributes.has_key("evalue") else "\N",
                                                            "best_hit_score": entry.score,
                                                            "best_hit_perc_ident": entry.attributesToStr("perc_ident") if entry.attributes.has_key("perc_ident") else "\N",
                                                            "best_hit_perc_Qcoverage": entry.attributesToStr("perc_Qcoverage") if entry.attributes.has_key("perc_Qcoverage") else "\N",
                                                            "best_hit_perc_Scoverage": entry.attributesToStr("perc_Scoverage") if entry.attributes.has_key("perc_Scoverage") else "\N",
                                                            "best_hit_perc_Qidentities": entry.attributesToStr("perc_Qidentities") if entry.attributes.has_key("perc_Qidentities") else "\N",
                                                            "best_hit_perc_Qconserved": entry.attributesToStr("perc_Qconserved") if entry.attributes.has_key("perc_Qconserved") else "\N"}
                
                    if best_annot_sources.has_key(entry.source): best_annot_sources[entry.source] += 1
                    else: best_annot_sources[entry.source] = 1
                    if entry.attributes.has_key("species"):
                        if best_annot_species.has_key(entry.attributesToStr("species")): best_annot_species[entry.attributesToStr("species")] += 1
                        else: best_annot_species[entry.attributesToStr("species")] = 1
                elif best_annotation_source is None:
                    raise Exception("Duplicated entry '" + entry.seq_id + "' in best annotation gff file " + gff)
        
        reader = SequenceReader(fasta_file)
        nb_contig, nb_bases, contigs_len, favorite_names = 0, 0, [], []
        for sequence in reader:
            nb_contig += 1
            nb_bases += len(sequence.sequence)
            contigs_len.append(len(sequence.sequence))
            seq_name=sequence.name
            
            if len(sequence.sequence) > 2000 and len(sequence.sequence) < 3500 and len(favorite_names) < 5 : 
                favorite_names.append(seq_name)
            
            if self.best_annot_info.has_key(seq_name) :
            
                  self.add_row({"name": seq_name,
                          "length": len(sequence.sequence),
                          "depth": 0, 
                          "best_hit_database": self.best_annot_info[seq_name]["best_hit_database"],
                          "best_hit_species": self.best_annot_info[seq_name]["best_hit_species"],
                          "best_hit_gene": self.best_annot_info[seq_name]["best_hit_gene"],
                          "best_hit_accession" : self.best_annot_info[seq_name]["best_hit_accession"],
                          "best_hit_description" : self.best_annot_info[seq_name]["best_hit_description"],
                          "best_hit_query_start" : self.best_annot_info[seq_name]["best_hit_query_start"],
                          "best_hit_query_stop" : self.best_annot_info[seq_name]["best_hit_query_stop"],
                          "best_hit_subject_start" : self.best_annot_info[seq_name]["best_hit_subject_start"],
                          "best_hit_subject_stop" : self.best_annot_info[seq_name]["best_hit_subject_stop"],
                          "best_hit_evalue" : self.best_annot_info[seq_name]["best_hit_evalue"],
                          "best_hit_score" : self.best_annot_info[seq_name]["best_hit_score"],
                          "best_hit_perc_ident" : self.best_annot_info[seq_name]["best_hit_perc_ident"],
                          "best_hit_perc_Qcoverage" : self.best_annot_info[seq_name]["best_hit_perc_Qcoverage"],
                          "best_hit_perc_Scoverage" : self.best_annot_info[seq_name]["best_hit_perc_Scoverage"],
                          "best_hit_perc_Qidentities" : self.best_annot_info[seq_name]["best_hit_perc_Qidentities"],
                          "best_hit_perc_Qconserved" : self.best_annot_info[seq_name]["best_hit_perc_Qconserved"]})
            else :
                self.add_row({"name": seq_name,
                          "length": len(sequence.sequence),
                          "depth": 0})
                
        # add all favorites
        for favorite_name in favorite_names:
            self.add_favorite(favorite_name)
            
        # find out the N50 and N90
        n50, n90, count = 0, 0, 0
        for i in reversed(sorted(contigs_len)):
            count += i
            if count >= int(nb_bases*0.5) and n50 == 0: n50 = i
            if count >= int(nb_bases*0.9) and n90 == 0: n90 = i
    
        # Add a statistic table on depth and length
        self.add_table(analysis_name="General statistics", 
                       header=["", "Min", "1st quartile", "Median", "Mean", "3rd quartile", "Max"],
                       rows=[["Length", str(min(sorted(contigs_len))), str(first_quartile(sorted(contigs_len))), 
                            str(median(sorted(contigs_len))), str(round(float(sum(contigs_len))/float(len(contigs_len)), 2)), 
                            str(third_quartile(sorted(contigs_len))), str(max(sorted(contigs_len)))]],
                       soft_name="Contig.add_from_fasta_and_gff",
                       comments="The assembly produced <strong>"+str(nb_contig)+" contigs</strong> ("+str(nb_bases)+" bases) with a N50 at "+str(n50)+" and a N90 at "+str(n90)+".",
                       chart_title="General",
                       chart_subtitle="statistics")
        
        sub_contigs_len = []
        for contig_len in sorted(contigs_len):
            if contig_len <= Contig.MAX_CONTIG_LEN_IN_HISTOGRAM:
                sub_contigs_len.append(contig_len)
        hist, edges = np.histogram(sub_contigs_len, bins=Contig.GRAPH_CLASS_PARTITION)
        
        # Add a the length as an histogram to the interface
        self.add_histogram(analysis_name="Contigs length", 
                           soft_name="Contig.add_from_fasta_and_gff",
                           labels=edges,
                           values=hist,
                           y_title="Number of contigs",
                           x_title="Lengths of contigs (nt)",
                           chart_title="Contigs length",
                           tooltip="There is <strong>###Y### contigs</strong> with a size of <strong>###X### nt</strong>",
                           chart_subtitle="distribution")
        
        # Add a the best annotation databank as a pie chart to the interface
        databank, count, nb_contig_annotated = [], [], 0
        for source in best_annot_sources.keys():
            databank.append(source)
            count.append(best_annot_sources[source])
            nb_contig_annotated += best_annot_sources[source]
        databank.append("no annotation")
        count.append(nb_contig-nb_contig_annotated)
        self.add_piechart(analysis_name="Contigs best annotations", 
                          soft_name="Contig.add_from_fasta_and_gff",
                          labels=databank,
                          values=count,
                          chart_title="Contigs",
                          tooltip="There is <strong>###Y### contigs</strong> annotated with <strong>###X###</strong>",
                          chart_subtitle="best annotations")
        
        # Add a the best annotation species as an histogram to the interface
        species, species_count = [], []
        for couple_sc in sorted(best_annot_species.iteritems(), key=operator.itemgetter(1), reverse=True)[:Contig.BEST_ANNOTATION_MAX_SPECIES]:
            species.append(couple_sc[0])
            species_count.append(couple_sc[1])
        self.add_bargraph(analysis_name="Species found as best annotation", 
                           soft_name="Contig.add_from_fasta_and_gff",
                           labels=species,
                           values=species_count,
                           x_title="Number of best hits",
                           tooltip="<strong>###Y### best hits</strong> as ###X###",
                           chart_title="The " + str(Contig.BEST_ANNOTATION_MAX_SPECIES) +" most representated species",
                           chart_subtitle="found as best hit annotation")


    def update_depth(self, contig_depth ):
        self.update_values("depth",contig_depth)
        length = self.select_values("length")
        depth = self.select_values("depth")
        xvalues, yvalues = [], []
        for i, cname in enumerate(length):
            if i == self.MAX_DEPTH_DOTS:
                break
            else:
                xvalues.append(int(length[cname])*int(float(depth[cname])))
                yvalues.append(int(length[cname]))
        self.add_scatterplot(analysis_name=" Contigs depth", 
                             soft_name="Contig.update_depth",
                             x_values=xvalues,
                             y_values=yvalues,
                             x_title="Sum of reads length",
                             y_title="Contigs length",
                             tooltip="Reads sum: ###X###bp <br /> Length: ###Y###bp",
                             chart_title="Contigs depth graph",
                             chart_subtitle="Only " + str(Contig.MAX_DEPTH_DOTS) + " are representated")
        
    @staticmethod
    def get_contigs( filepath ):
        """
            return a dictionary with contig names and sizes
            @param filepath: path to a fasta file
            @return: dictionary
        """
        res = {}
        fasta = FastaReader(filepath)
        for seq in fasta :
            res[seq.name] = len(seq.sequence)
        return res

        
class DNA(BiomartDimension):
    
    def define_fields(self):
        self.sequences = BiomartTEXTField("sequences")

        
    def load(self, fasta_file):
        reader = SequenceReader(fasta_file)
        self.dict_sequences={}
        for sequence in reader:
            self.add_row(sequence.name.split()[0], {"sequences": ">"+sequence.name+"\\n"+sequence.sequence})
            self.dict_sequences[sequence.name.split()[0]]=sequence.sequence
                    

class Annotation(BiomartDimension):
    
    def define_fields(self):
        self.name = BiomartVARCHARField("name", size=64)
        self.hit_database = BiomartVARCHARField("hit_database", size=255)
        self.hit_species = BiomartVARCHARField("hit_species", size=255)
        self.hit_type = BiomartVARCHARField("hit_type", size=124)
        self.hit_accession = BiomartVARCHARField("hit_accession", size=255)
        self.hit_description = BiomartMEDIUMTEXTField("hit_description")
        self.hit_query_start = BiomartINTField("hit_query_start", size=10)
        self.hit_query_stop = BiomartINTField("hit_query_stop", size=10)
        self.hit_query_strand = BiomartINTField("hit_query_strand", size=4)
        self.hit_subject_start = BiomartINTField("hit_subject_start", size=10)
        self.hit_subject_stop = BiomartINTField("hit_subject_stop", size=10)
        self.hit_subject_strand = BiomartINTField("hit_subject_strand", size=4)
        self.hit_evalue = BiomartDOUBLEField("hit_evalue")
        self.hit_score = BiomartINTField("hit_score", size=10)
        self.hit_perc_ident = BiomartFLOATField("hit_perc_ident", size=10)
        self.hit_perc_Qcoverage = BiomartFLOATField("hit_perc_Qcoverage")
        self.hit_perc_Scoverage = BiomartFLOATField("hit_perc_Scoverage")
        self.hit_perc_Qidentities = BiomartFLOATField("hit_perc_Qidentities")
        self.hit_perc_Qconserved = BiomartFLOATField("hit_perc_Qconserved")
        self.hit_is_best_annot = BiomartBOOLEANField("hit_is_best_annot")
        self.analysis_id = BiomartINTField("analysis_id", size=4)
        
        self.hit_type.add_filter(display_name = 'Type', type = FilterTypes.SINGLE_SELECT)
        self.hit_database.add_filter(display_name = 'Database', type = FilterTypes.MULTI_SELECT)
        self.hit_accession.add_filter(display_name = 'Accession', type = FilterTypes.UPLOAD)
        self.hit_description.add_filter(display_name = 'Description (use % as joker)', qualifier = FilterQualifiers.LIKE)
        self.hit_species.add_filter(display_name = 'Species (use % as joker)', qualifier = FilterQualifiers.LIKE)
        self.hit_is_best_annot.add_filter(display_name = 'Is best annot', type = FilterTypes.BOOLEAN )
        
    def load(self, gff_file, contig):
        ##TO BE CHANGED
        
        # retrieve best annotation from the contig object
        reader = GFF3IO(gff_file, "r")
        for entry in reader:
            if entry.strand and entry.strand in ["+","-"] :
                strand = "1" if entry.strand == "+" else "-1"
            else :
                strand = entry.strand
            
            is_best_annot = "\N"
            if ( entry.attributes.has_key("Name") and contig.best_annot_info.has_key(entry.seq_id) ):
                if contig.best_annot_info[entry.seq_id]["best_hit_accession"] == entry.attributesToStr("Name") \
                   and contig.best_annot_info[entry.seq_id]["best_hit_query_start"] == entry.start \
                   and contig.best_annot_info[entry.seq_id]["best_hit_query_stop"] == entry.end \
                   and contig.best_annot_info[entry.seq_id]["best_hit_score"] == entry.score:
                    is_best_annot = 1
            
            self.add_row(entry.seq_id, {"name": entry.seq_id,
                          "hit_database": entry.source,
                          "hit_species": entry.attributesToStr("species") if entry.attributes.has_key("species") else "\N",
                          "hit_type": entry.attributesToStr("type") if entry.attributes.has_key("type") else "\N",
                          "hit_accession" : entry.attributesToStr("Name") if entry.attributes.has_key("Name") else "\N",
                          "hit_description" : entry.attributesToStr("desc") if entry.attributes.has_key("desc") else "\N",
                          "hit_query_start" : entry.start,
                          "hit_query_stop" : entry.end,
                          "hit_query_strand" : strand,
                          "hit_subject_start" : entry.attributesToStr("sbjt_start") if entry.attributes.has_key("sbjt_start") else "\N",
                          "hit_subject_stop" : entry.attributesToStr("sbjt_stop") if entry.attributes.has_key("sbjt_stop") else "\N",
                          "hit_subject_strand" : entry.attributesToStr("sbjt_strand") if entry.attributes.has_key("sbjt_strand") else "\N",
                          "hit_evalue" : entry.attributesToStr("evalue") if entry.attributes.has_key("evalue") else "\N",
                          "hit_score" : entry.score,
                          "hit_perc_ident" : entry.attributesToStr("perc_ident") if entry.attributes.has_key("perc_ident") else "\N",
                          "hit_perc_Qcoverage" : entry.attributesToStr("perc_Qcoverage") if entry.attributes.has_key("perc_Qcoverage") else "\N",
                          "hit_perc_Scoverage" : entry.attributesToStr("perc_Scoverage") if entry.attributes.has_key("perc_Scoverage") else "\N",
                          "hit_perc_Qidentities" : entry.attributesToStr("perc_Qidentities") if entry.attributes.has_key("perc_Qidentities") else "\N",
                          "hit_perc_Qconserved" : entry.attributesToStr("perc_Qconserved") if entry.attributes.has_key("perc_Qconserved") else "\N",
                          "hit_is_best_annot" : is_best_annot,
                          "analysis_id" : "1"})
     
class Prediction(BiomartDimension):

    def define_fields(self):
        self.name = BiomartVARCHARField("name", size=64)
        self.description = BiomartMEDIUMTEXTField("description")
        self.start = BiomartINTField("start", size=10)
        self.stop = BiomartINTField("stop", size=10)
        self.strand = BiomartINTField("strand", size=4)
        self.type = BiomartVARCHARField("type", size=124)
        self.source = BiomartVARCHARField("source", size=124)
        self.score = BiomartINTField("score", size=10)
        self.analysis_id = BiomartINTField("analysis_id", size=4)

        self.type.add_filter(display_name = 'Type', type = FilterTypes.SINGLE_SELECT)
        self.description.add_filter(display_name = 'Description (use % as joker)', qualifier = FilterQualifiers.LIKE)

    def load(self, gff_file):
        reader = GFF3IO(gff_file, "r")
        for entry in reader:
            if entry.strand and entry.strand in ["+","-"] :
                strand = "1" if entry.strand == "+" else "-1"
            else :
                strand = entry.strand

            self.add_row(entry.seq_id, {
                          "name" : entry.attributesToStr("Name") if entry.attributes.has_key("Name") else "\N",
                          "description" : entry.attributesToStr("desc") if entry.attributes.has_key("desc") else "\N",
                          "start" : entry.start,
                          "stop" : entry.end,
                          "strand" : strand,
                          "type" : entry.type,
                          "source" : entry.source,
                          "score"  : entry.score,
                          "analysis_id" : "1"})

class GO(BiomartDimension):
    def define_fields(self):
        self.ontology = BiomartVARCHARField("ontology", size=64)
        self.code = BiomartVARCHARField("code", size=64)
        self.evidence = BiomartVARCHARField("evidence", size=16)
        self.source = BiomartVARCHARField("source", size=64)
        self.terminal_node_bool = BiomartBOOLEANField("terminal_node_bool")
        self.go_name = BiomartVARCHARField("go_name", size=255)

        self.ontology.add_filter(type = FilterTypes.SINGLE_SELECT)
        self.evidence.add_filter(type = FilterTypes.MULTI_SELECT)
        self.code.add_filter(display_name = 'Code (ex : GO:0008150)')
        self.go_name.add_filter(display_name = 'Name (use % as jocker)', qualifier = FilterQualifiers.LIKE)
        self.terminal_node_bool.add_filter(type = FilterTypes.BOOLEAN)


    def load(self, file):
        """
        TODO : - Check with OBO file
               - handle empty field 
        """
        go_pattern = re.compile("GO:\d{7}")

        fh = open(file)
        for line in fh.readlines():
            if line == "" or line.startswith("#"): continue
            #go file : contig_name    go_id    go_name    ontology(category)    evidence    source    terminal_node
            parts = line.split("\t")
            if re.match(go_pattern,parts[1]) :
                    self.add_row(parts[0], {"ontology": parts[3] if parts[3].strip() != "" else "\N",
                                    "code": parts[1] if parts[1].strip() != "" else "\N", 
                                    "evidence": parts[4] if parts[4].strip() != "" else "\N",
                                    "source": parts[5] if parts[5].strip() != "" else "\N",
                                    "terminal_node_bool": parts[6].strip()  if parts[6].strip() != "" else "\N",
                                    "go_name": parts[2] if parts[2].strip() != "" else "\N",})
        fh.close()


class Keyword(BiomartDimension):

    def define_fields(self):
        self.keyword = BiomartVARCHARField("keyword", size=124, is_key=True)
        self.keyword.add_filter('Keyword (use % as joker caracters)', qualifier = FilterQualifiers.LIKE)

    def load(self, file):
        """
        """
        fh = open(file)
        for line in fh.readlines():
            #kw file :  contig_name    kw1    kw2    kw3
            parts = line.rstrip().split("\t")
            for kw in parts[1:]:
                self.add_row(parts[0], {"keyword": kw})
        fh.close()


class Expression(BiomartDimension):

    def define_fields(self):
        self.library_id = BiomartINTField("library_id", size=10, is_key=True)
        self.sample_name = BiomartVARCHARField("sample_name", size=255)
        self.replicat = BiomartINTField("replicat", size=4)
        self.nb_read = BiomartBIGINTField("nb_read", size=22)
        self.window_size = BiomartINTField("window_size", size=11)
        self.coverage = BiomartTEXTField("coverage")
        self.mean_depth = BiomartFLOATField("mean_depth")
        self.tissue = BiomartVARCHARField("tissue", size=124, is_key=True)
        self.dev_stage = BiomartVARCHARField("dev_stage", size=124, is_key=True)

        self.sample_name.add_filter(type = FilterTypes.MULTI_SELECT, group = 'Library field')
        self.dev_stage.add_filter(type = FilterTypes.MULTI_SELECT, group = 'Library field')
        self.tissue.add_filter(type = FilterTypes.MULTI_SELECT, group = 'Library field')

        self.nb_read.add_filter(display_name = 'Nb reads >=', qualifier = FilterQualifiers.EQUALS_GREATER, group = 'Number of reads')
        self.nb_read.add_filter(display_name = 'Nb reads <=', qualifier = FilterQualifiers.EQUALS_LOWER, group = 'Number of reads')

        self.mean_depth.add_filter(display_name = 'Mean depth <=', qualifier = FilterQualifiers.EQUALS_LOWER, group = 'Depth per library')
        self.mean_depth.add_filter(display_name = 'Mean depth >=', qualifier = FilterQualifiers.EQUALS_GREATER, group = 'Depth per library')

    def load(self, matrix, libraries, coverage_files = [], rarefaction_files=[] ):
        '''
        @param matrix : [str] a tsv file formated as blow
                        #contig_names    lib1    lib2    ...
                        name1    0      12
                        name2    154    86
                        ...
        @param libraries : [list] list of Library object
        @param coverage_files : [list] files with coverage informations
                        #contig_name    lib_name    global_avg_depth    window_size    depth_histogram    avg_depth
                        contig_1    lib_A    15.0    50    10,10,10    10
                        contig_1    lib_B    15.0    50    20,20,20    20
                        contig_2    lib_A    5.0    24    5,5,5    5
                        '''
        lib_by_name = { lib.library_name : lib for lib in libraries }
        nb_mapped_by_lib = { lib.id : 0 for lib in libraries }
        nb_contig_by_lib = { lib.id : 0 for lib in libraries }
        self.contig_depth = dict()
        mapped = { lib.library_name : dict() for lib in libraries } # count by librabry by contig

        # Retrieve mapped count
        with open(matrix) as fh :
            # Header
            line = fh.readline()
            matrix_header = line.rstrip().split("\t")
            # Counts
            for line in fh.readlines():
                if line.startswith("#") : 
                    continue
                line_fields = line.rstrip().split("\t")
                contig_name = line_fields[0]
                libraries_counts = line_fields[1:]
                for idx in range( len(libraries_counts) ):
                    mapped[matrix_header[idx+1]][contig_name] = int(libraries_counts[idx])

        # Process coverage data
        for current_coverage_file in coverage_files:
            with open (current_coverage_file,"r") as fh:
                for coverage_line in fh:
                    if coverage_line.startswith("#") : 
                        continue
                    (contig_name, library_name, contig_depth_value, window_size, lib_hist, lib_avg_depth) = coverage_line.rstrip().split()
                    self.contig_depth[contig_name] = float(contig_depth_value)
                    if lib_by_name.has_key(library_name) :
                        lib = lib_by_name[library_name]
                        # Update library data
                        nb_mapped_by_lib[lib.id] += mapped[library_name][contig_name]
                        if int(mapped[library_name][contig_name]) > 0 : nb_contig_by_lib[lib.id] += 1
                        # Store contig coverage and count
                        self.add_row( contig_name, {"library_id"  : lib.id,
                                                    "sample_name" : lib.sample_name, 
                                                    "replicat"    : lib.replicat,
                                                    "tissue"      : lib.tissue,
                                                    "dev_stage"   : lib.dev_stage,
                                                    "window_size" : window_size if window_size != "" else "\N",
                                                    "coverage"    : lib_hist if lib_hist != "" else "\N",
                                                    "mean_depth"  : lib_avg_depth if lib_avg_depth != "" else "\N",
                                                    "nb_read"     : mapped[library_name][contig_name]
                                                    })

        # store librairies counts
        self.add_libraries_count(Contig, matrix)

        # Add a the librairies counts as an histogram to the interface
        lib_labels, lib_mapped_val, lib_unmapped_val, nb_contig_val = list(), list(), list(), list()
        for lib in libraries:
            current_label = str(lib.sample_name) + '.' + str(lib.replicat)
            if str(lib.dev_stage) != "":
                current_label += '(' + str(lib.dev_stage) + ')'
            lib_labels.append( current_label )
            lib_mapped_val.append( nb_mapped_by_lib[lib.id] )
            lib_unmapped_val.append( lib.nb_sequence - nb_mapped_by_lib[lib.id] )
            nb_contig_val.append( nb_contig_by_lib[lib.id] )
        self.add_stacked_histogram( analysis_name="Libraries mapping overview", 
                            soft_name="-",
                            labels=lib_labels,
                            values={ 'mapped':lib_mapped_val, 'unmapped':lib_unmapped_val },
                            y_title="Total fragments mapped",
                            chart_title="Mapping Statistics",
                            chart_subtitle="overview per library" )
        self.add_histogram( analysis_name="Contigs expression per library", 
                            soft_name="-",
                            labels=lib_labels,
                            values=nb_contig_val,
                            y_title="Contigs present in libraries",
                            chart_title="Mapping Statistics",
                            tooltip="###X###<br />Contigs : <strong>###Y###</strong>",
                            chart_subtitle="contigs mapped per library" )

        # Add a statistic table on depth and length
        self.add_table(analysis_name="General statistics", 
                       header=None,
                       rows=[["Depth", str(min(sorted(self.contig_depth.values()))), str(first_quartile(sorted(self.contig_depth.values()))), 
                            str(median(sorted(self.contig_depth.values()))), str(round(float(sum(self.contig_depth.values()))/float(len(self.contig_depth.values())), 2)), 
                            str(third_quartile(sorted(self.contig_depth.values()))), str(max(sorted(self.contig_depth.values())))]],
                       soft_name="",
                       comments="",
                       chart_title="",
                       chart_subtitle="")
        
        # Process rarefaction data
        labels=[]
        values={}
        for current_rarefaction_file in rarefaction_files:
            with open (current_rarefaction_file,"r") as fh:
                library_name =  os.path.splitext(os.path.basename(current_rarefaction_file))[0]
                lib = lib_by_name[library_name]
                current_label_name = str(lib.sample_name) + '.' + str(lib.replicat)
                values[current_label_name]=[]
                current_labels=[]
                for rarefaction_line in fh:
                    if rarefaction_line.startswith("#") : 
                        continue
                    current_labels.append(rarefaction_line.rstrip().split()[0])
                    values[current_label_name].append(rarefaction_line.rstrip().split()[1])
                if len(current_labels) > len(labels) :
                    labels = current_labels[:]
        # Add a the length as an histogram to the interface
        self.add_basicline(analysis_name="Library saturation", 
                       soft_name="Expression.load",
                       labels=labels,
                       values=values,
                       y_title="Number of contigs",
                       x_title="Number of reads",
                       chart_title="Saturation curves",
                       chart_subtitle="graph",
                       tooltip="###Z###: <br/>###Y### contigs viewed for ###X### sequenced reads",)    
        
class Variant(BiomartDimension):

    def define_fields(self):
        self.variant_name = BiomartVARCHARField("variant_name", size=124, is_key=True)
        self.analysis_id = BiomartINTField("analysis_id", size=4)
        self.type = BiomartVARCHARField("type", size=20)
        self.quality = BiomartINTField("quality", size=5)
        self.alleles = BiomartVARCHARField("alleles", size=255)
        self.position = BiomartINTField("position", size=4)
        self.previous_variant = BiomartINTField("previous_variant", size=4)
        self.next_variant = BiomartINTField("next_variant", size=4)
        self.best_annot_species = BiomartVARCHARField("best_annot_species", size=255)
        self.best_annot_gene = BiomartVARCHARField("best_annot_gene", size=255)
        self.best_annot_exon_five_prime_limit = BiomartINTField("best_annot_exon_five_prime_limit", size=4)
        self.best_annot_exon_three_prime_limit = BiomartINTField("best_annot_exon_three_prime_limit", size=4)
        self.best_annot_consequences = BiomartVARCHARField("best_annot_consequences", size=255)
        self.best_annot_known_variants = BiomartVARCHARField("best_annot_known_variants", size=45)

        self.type.add_filter( type = FilterTypes.MULTI_SELECT )
        self.best_annot_consequences.add_filter( type = FilterTypes.MULTI_SELECT )

    def load(self, vcf, variant):
        # retrieve variant object
        
        annot = variant.best_annot_info

        reader = VCFReader(vcf)
        previous_variant = None
        current_variant = None
        first=True
        for entry in reader:
            if previous_variant  == None:
                previous_variant = entry
            elif current_variant == None:
                current_variant = entry

            if previous_variant != None and current_variant != None:
                dist_previous = "\N"
                dist_next = "\N"
                if first:
                    first = False
                    if entry.chrom == current_variant.chrom:
                        dist_next = current_variant.pos - previous_variant.pos
                    self._add_variant(previous_variant, annot, dist_previous, dist_next)
                else:
                    if previous_variant.chrom == current_variant.chrom:
                        dist_previous = current_variant.pos - previous_variant.pos
                    if entry.chrom == current_variant.chrom:
                        dist_next = entry.pos - current_variant.pos
                    self._add_variant(current_variant, annot, dist_previous, dist_next)
                    previous_variant = current_variant
                    current_variant = entry

        #last record
        dist_previous = "\N"
        if current_variant.chrom == entry.chrom:
            dist_previous = current_variant.pos - previous_variant.pos + 1
        self._add_variant(entry, annot, dist_previous, "\N")

    def _add_variant(self, entry, annot, dist_previous, dist_next):
        type = utils.get_variant_type(entry.ref, entry.alt)

        variant_name = entry.id
        if variant_name == "." or variant_name == "-":
            variant_name = utils.get_variant_name(entry.chrom, str(entry.pos), type)

        self.add_row(entry.chrom, { "variant_name": variant_name,
                                    "analysis_id": "\N", #TODO
                                    "type": type,
                                    "quality":entry.qual,
                                    "alleles": entry.ref + "/" + "/".join(entry.alt),
                                    "position": entry.pos,
                                    "previous_variant": dist_previous,
                                    "next_variant": dist_next,
                                    "best_annot_species": annot[variant_name]["best_annot_species"] if annot.has_key(variant_name) else "\N",
                                    "best_annot_gene": annot[variant_name]["best_annot_gene"] if annot.has_key(variant_name) else "\N",
                                    "best_annot_exon_five_prime_limit": annot[variant_name]["best_annot_exon_five_prime_limit"] if annot.has_key(variant_name) else "\N",
                                    "best_annot_exon_three_prime_limit": annot[variant_name]["best_annot_exon_three_prime_limit"] if annot.has_key(variant_name) else "\N",
                                    "best_annot_consequences": annot[variant_name]["best_annot_mutation_type"] if annot.has_key(variant_name) else "\N",
                                    "best_annot_known_variants": annot[variant_name]["best_annot_known_variants"] if annot.has_key(variant_name) else "\N"})