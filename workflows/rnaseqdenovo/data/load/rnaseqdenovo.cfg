#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

[project]
#project_name must not have an "_"
project_name = DemoBrainLiverLoad
project_description = SeaBass brain and liver transcriptome 500 examples contigs
species = Dicentrarchus labrax
species_common_name = SeaBass
instance_name = myinstance
image = workflows/rnaseqdenovo/data/DLabrax.png

[libraries]
#Library name must be unique
lib1.library_name=liver_250
#sample_name can be duplicate, it's replicate number which differenciate samples_name 
lib1.sample_name=Liver
lib1.replicat=1
#metadata for user interface :
lib1.sequencer=HiSeq2000
lib1.tissue=Liver
lib1.dev_stage=
lib1.insert_size=
#pe : paired-end ; se : single-end; ope : oriented paired-end ; ose ...
lib1.type=pe
#do not used in user interface, just stored in db :
lib1.remark=
# To avoid to be computed:
lib1.nb_sequence=410001
#Does the library is public for example SRA,NG6...
lib1.public=0
#If library is public :
# - you can provide the accession, not used for the moment
lib1.accession=
# - you can provide the database, not used for the moment
lib1.database=
#Fastq files, can be gzipped or not
lib1.files=workflows/rnaseqdenovo/data/process/lib_172.1.fastq.gz,workflows/rnaseqdenovo/data/process/lib_172.2.fastq.gz

lib2.library_name=brain_250
lib2.sample_name=Brain
lib2.replicat=1
lib2.tissue=Brain
lib2.dev_stage=
lib2.type=se
lib2.insert_size=
lib2.remark=
lib2.nb_sequence=568709
lib2.sequencer=HiSeq2000
lib2.public=0
lib2.accession=
lib2.database=
lib2.files=workflows/rnaseqdenovo/data/load/brain_250.fastq.gz
lib3.library_name=liver_400
lib3.sample_name=Liver
lib3.replicat=2
lib3.tissue=Liver
lib3.dev_stage=
lib3.type=se
lib3.insert_size=
lib3.remark=
lib3.nb_sequence=569334
lib3.sequencer=HiSeq2000
lib3.public=0
lib3.accession=
lib3.database=
lib3.files=workflows/rnaseqdenovo/data/load/liver_400.fastq.gz
lib4.library_name=brain_400
lib4.sample_name=Brain
lib4.replicat=2
lib4.tissue=Brain
lib4.dev_stage=
lib4.type=se
lib4.insert_size=
lib4.remark=
lib4.nb_sequence=453047
lib4.sequencer=HiSeq2000
lib4.public=0
lib4.accession=
lib4.database=
lib4.files=workflows/rnaseqdenovo/data/load/brain_400.fastq.gz

[assembly]
#Fasta files (must be named .fa or .fasta) of contigs
file = workflows/rnaseqdenovo/data/load/contigs.fasta
#Metadata of how contigs where built 
software_name = velvet oases
software_parameters = k25 k40
software_version = v1
comments = metassembly of kmers

[assembly_annotations]
#The user must provide one annotation db with is_best=true 
#or fix the parameter best_annotation_source (corresponding to the second column in gff3 file)
#gff3 file for contig annotation see doc on mulcyber for more information.
annot1.file = workflows/rnaseqdenovo/data/load/best_annotation_file.gff3
#metadata of the analysis
annot1.software_name = blast
annot1.software_version = 2.2.26
annot1.software_parameters = -e 10e-10 
annot1.comments = Best annotations against swissprot and refseq
annot1.is_best = true
annot2.file = workflows/rnaseqdenovo/data/load/annotation_refseq_rna.gff3
annot2.software_name = blast
annot2.software_version = 2.2.26
annot2.software_parameters = -e 10e-10 -d refseq_rna
annot2.comments = Annotation against refseq_rna v3
annot2.is_best = false
annot3.file = workflows/rnaseqdenovo/data/load/annotation_unigene_danio.gff3
annot3.software_name = blast
annot3.software_version = 2.2.26
annot3.software_parameters = -e 10e-10 -d unigene_danio
annot3.comments = Annotation against unigene danio rerio v126
annot3.is_best = false
annot4.file = workflows/rnaseqdenovo/data/load/annotation_swissprot.gff3
annot4.software_name = blast
annot4.software_version = 2.2.26
annot4.software_parameters = -e 10e-10 -d swissprot
annot4.comments = Annotation against swissprot v2.0
annot4.is_best = false
best_annotation_source =

[predictions]

predict1.file = workflows/rnaseqdenovo/data/load/interproscan_predictions.gff3
predict1.software_name = interproscan
predict1.software_version = 5.3-46.0
predict1.software_parameters = --goterms --pathways
predict1.comments =

predict2.file = workflows/rnaseqdenovo/data/load/contigs.fasta.transdecoder.gff3
#Metadata of how orf were detected
predict2.software_name = Transdecoder
predict2.software_parameters = -b transcript 
predict2.software_version = 3.0.1
predict2.comments =
 
[alignments]
#metadata for alignment files
alignment1.software_name = bwa
alignment1.software_version = 2.2.26
alignment1.software_parameters = samse
alignment1.comments =
#For one kind of metadata you can provide several files
alignment1.file1 = workflows/rnaseqdenovo/data/load/brain_250.bam
alignment1.file2 = workflows/rnaseqdenovo/data/load/brain_400.bam

#You can add another analyses with other files
alignment2.software_name = bwa
alignment2.software_version = 2.2.26
alignment2.software_parameters = sampe
alignment2.comments =
alignment2.file1 = workflows/rnaseqdenovo/data/load/liver_250.bam
alignment2.file2 = workflows/rnaseqdenovo/data/load/liver_400.bam

[gene_ontology]
#User can provide a GO file, expected format: seven columns
#    contig name
#    GO code
#    GO name : Name corresponding to the GO code
#    Onthology [C|M|B]
#    GO evidence : http://www.geneontology.org/GO.evidence.shtml
#    Source : [Refseq|Swissprot|InterPro|...]
#    is a terminal node ? [0|1] 
file = workflows/rnaseqdenovo/data/load/go.txt

[keyword]
#Two columns are expected :
#    1: contig name
#    2 .. n : keywords separated by tabulation 
file = workflows/rnaseqdenovo/data/load/keyword.txt

[expression]
#Contigs are in line and library count in column. First line must contain the libraries names .
#eg : #contig_name	liver_250	brain_250	liver_400	brain_400
#Library name MUST correspond to previously provided library names (libX.library_name)
file = workflows/rnaseqdenovo/data/load/count_matrix.csv

[variant]
#VCF header contain library name, their MUST correspond to previously provided library names (libX.library_name) 
file = workflows/rnaseqdenovo/data/load/variants.eff.vcf
#file = workflows/rnaseqdenovo/data/load/variants.eff36.vcf
#Metadata of how snp was detected
software_name = GATK
software_parameters = --glm BOTH
software_version = 2
comments = variant calling (realignment, recalibration)

[variant_annotations]
#gff3 file for snp annotation see doc on mulcyber for more information.
annot1.file = workflows/rnaseqdenovo/data/load/variants_bests_annotations.gff
#Metadata of how snp annotation was performed
annot1.software_name = tSNPannot
annot1.software_version = 2.0.0
annot1.software_parameters = --nb-annot 2 --min-conserv-border 1 --sequences-file danio.fasta --variants variant.vcf --output variants_annotation.gff --annot-db aln=danio.xml species="Danio rerio" gtf=Danio_rerio.gtf vcf=Danio_rerio.vcf --annot-db aln=tetraodon.xml species="Tetraodon_nigroviridis" gtf=Tetraodon_nigroviridis.gtf vcf=Tetraodon_nigroviridis.vcf --annot-db aln=takifugu.xml species="Takifugu rubripes" gtf=Takifugu_rubripes.gtf
annot1.comments =
annot1.is_best = true

[variant_effect]
#Assume effect are stored in vcf.
#Metadata of how snp effect was performed
software_name = SnpEff
software_version = 2.0.0
software_parameters = eff 
comments =
