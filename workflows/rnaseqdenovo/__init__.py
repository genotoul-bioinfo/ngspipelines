#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from lib.utils import *
from lib.rnaseqdenovo import Contig, DNA, GO, Annotation, Prediction, Keyword, Variant, Expression
from lib.variantdenovo import Variant as vVariant, Annotation as vAnnotation, CountAlleleLib, Effect

import ngspipelines
from ngspipelines.application import ApplicationWithoutReferenceGenome
from ngspipelines.analysis import Analysis
from ngspipelines.library import Library

from jflow.seqio import SequenceReader
from jflow.featureio import VCFReader

import os
import sys
import StringIO
from ConfigParser import ConfigParser

class RNAseqDenovo (ApplicationWithoutReferenceGenome):

    def get_description(self):
        return "RNAseq pipeline without reference genome"

    def define_parameters(self, function="process"):
        # Assembly parameter
        self.add_multiple_parameter("assembly", "Define the assembly process", required=True, group="ASSEMBLY section")
        self.add_input_file("file", "Path to the contig file in fasta format", file_format="fasta", required=True, add_to="assembly")
        self.add_parameter("software_name", "Which software was used to perform the assembly", default="", required=True, add_to="assembly")
        self.add_parameter("software_parameters", "Which parameters were used when performing the assembly", default="", add_to="assembly")
        self.add_parameter("software_version", "Which version of the assembly software was used", default="", required=True, add_to="assembly")
        self.add_parameter("comments", "Add some comments on this analysis", default="", add_to="assembly")

        # Variant parameter
        self.add_multiple_parameter("variant", "Define the variant calling process", group="VARIANT section")
        self.add_input_file("file", "Path to the variant file in VCF format", required=True, add_to="variant")
        self.add_parameter("software_name", "Which software was used to perform the variant calling analysis", default="", required=True, add_to="variant")
        self.add_parameter("software_parameters", "Which parameters were used when performing the variant calling", default="", add_to="variant")
        self.add_parameter("software_version", "Which version of the variant calling software was used", default="", required=True, add_to="variant")
        self.add_parameter("comments", "Add some comments on this analysis", default="", add_to="variant")

        # Parameters for load workflow
        if function == "load":
            # Annotation parameter
            self.add_multiple_parameter_list("assembly_annot", "Define the annotation process", required=True, group="ANNOTATION section")
            self.add_input_file("file", "Path to the annotation file in gff format", required=True, add_to="assembly_annot")
            self.add_parameter("software_name", "Which software was used to perform the annotation", add_to="assembly_annot")
            self.add_parameter("software_parameters", "Which parameters were used when performing the annotation", required=True, add_to="assembly_annot")
            self.add_parameter("software_version", "Which version of the annotation software was used", required=True, add_to="assembly_annot")
            self.add_parameter("database", "Which database was used", add_to="assembly_annot")
            self.add_parameter("database_version", "Which database version was used", add_to="assembly_annot")
            self.add_parameter("comments", "Add some comments on this analysis", add_to="assembly_annot")
            self.add_parameter("is_best", "Does this annotation file gathers all best annotations (1 line per contig)", type="bool", default=False, add_to="assembly_annot")
            # Best annotation source parameter
            self.add_parameter("best_annotation_source", "Which source should be used to define the contig best annotation (to use instead of --annotation is_best=True)", 
                               group="ANNOTATION section")
            # Prediction parameter
            self.add_multiple_parameter_list("prediction", "Define the prediction process", group="ANNOTATION section")
            self.add_input_file("file", "Path to the annotation file in gff format", required=True, add_to="prediction")
            self.add_parameter("software_name", "Which software was used to perform the prediction", add_to="prediction")
            self.add_parameter("software_parameters", "Which parameters were used when performing the prediction", required=True, add_to="prediction")
            self.add_parameter("software_version", "Which version of the prediction software was used", required=True, add_to="prediction")
            self.add_parameter("comments", "Add some comments on this analysis", add_to="prediction")
            # GO parameter
            self.add_input_file("go", "File of GO annotation see documentation for the format", group="ANNOTATION section")
            # Keyword parameter
            self.add_input_file("keyword", "file of keyword, format contig_name    kw1    kw2    key word 3", group="ANNOTATION section")
            # Variant annotation parameter
            self.add_multiple_parameter_list("variant_annot", "Define the variant annotation process", group="VARIANT section")
            self.add_input_file("file", "Path to the annotation file in gff format", required=True, add_to="variant_annot")
            self.add_parameter("software_name", "Which software was used to perform the variant annotation", required=True, add_to="variant_annot")
            self.add_parameter("software_parameters", "Which parameters were used when performing the variant annotation", add_to="variant_annot")
            self.add_parameter("software_version", "Which version of the variant annotation software was used", required=True, add_to="variant_annot")
            self.add_parameter("comments", "Add some comments on this analysis", add_to="variant_annot")
            self.add_parameter("is_best", "Does this variant annotation file gathers all best annotations (1 line per variant)", type="bool", default=False, add_to="variant_annot")
            # Variant best annotation source parameter
            self.add_parameter("variant_best_annotation_source", "Which source should be used to define the variant best annotation (to use instead of --variant-annotation is-best=True)", 
                               group="VARIANT section")
            
            self.add_multiple_parameter("variant_effect", "Define the variant effect process, use vcf file to load effect", group="VARIANT section")
            self.add_parameter("software_name", "Which software was used to perform the variant annotation", required=True, add_to="variant_effect")
            self.add_parameter("software_parameters", "Which parameters were used when performing the variant annotation", add_to="variant_effect")
            self.add_parameter("software_version", "Which version of the variant annotation software was used", required=True, add_to="variant_effect")
            self.add_parameter("comments", "Add some comments on this analysis", add_to="variant_effect")
            # Alignment parameter
            self.add_multiple_parameter_list("alignment", "Define the alignment process", required=True, group="ALIGNMENT section")
            self.add_input_file_list("files", "Path to the alignment file in bam format", required=True, add_to="alignment")
            self.add_parameter("software_name", "Which software was used to perform the alignment", required=True, add_to="alignment")
            self.add_parameter("software_parameters", "Which parameters were used when performing the alignment", add_to="alignment")
            self.add_parameter("software_version", "Which version of the alignment software was used", required=True, add_to="alignment")
            self.add_parameter("comments", "Add some comments on this analysis", add_to="alignment")
            # Count matrix parameter
            self.add_input_file("count_matrix", "File of mapped reads counts per contig (line) per library (column)", group="ALIGNMENT section")

        # Parameters for process workflow
        elif function == "process":
            # Parameter rename
            self.add_parameter("split", "Nb contig to put per split files.", display_name="Nb contig to split", type="int", add_to="assembly", 
                               default=4000, group="ASSEMBLY section")
            self.add_parameter("rename", "With this option the contigs are renamed with the name of their best hit.", display_name="Rename contigs", type="bool", 
                               default=False, group="ASSEMBLY section")
            self.add_parameter("prefix", "Prefix to add on contigs names.", display_name="Contigs prefix", default=None, group="ASSEMBLY section")
            # Parameter annotation
            self.add_parameter("min_identity", "The minimum identity to keep an HSP in annotation.", display_name="Minimum identity", type="float", default=0.5, 
                               group="ANNOTATION section")
            self.add_parameter("min_coverage", "The minimum query coverage by an HSP to keep this HSP in annotation.", display_name="Minimum query coverage", 
                               type="float", default=0.1, group="ANNOTATION section")
            #    Parameter annotation sub-parameter databanks
            self.add_multiple_parameter_list("assembly_annot_db", "Databanks used to process annotation by sequence similarity.", display_name="Assembly annotation databank",
                                             required=True, group="ANNOTATION section")
            self.add_input_file("file", "Path to the databank file (root of NCBI-Blast+ index).", required=True, add_to="assembly_annot_db")
            self.add_parameter("type", "Type of databank.", required=True, choices=["genome", "nucleic", "protein", "transcript", "unknown"], add_to="assembly_annot_db")
            self.add_parameter("name", "The name of the databank (used to trace the source of annotations).", required=True, add_to="assembly_annot_db")
            self.add_parameter("weight", "This weight modifies the hsp's score calculation (score = old_score + weight * old_score) used during selection of the best annotation.",
                               type="float", default=0.0, add_to="assembly_annot_db")
            self.add_parameter("species", "The species whose sequences come from. Fill this parameter if databank is built with only one species and if the header of sequences does not provide her name.",
                               display_name="Species", add_to="assembly_annot_db")
            self.add_parameter("evalue", "The maximum e-value for the alignements.", default="1e-5", add_to="assembly_annot_db")
            self.add_parameter("max_hit", "The maximum number of hit on databank for one query.", type="int", default=20, add_to="assembly_annot_db")
            self.add_parameter("program", "The type of NCBI-Blast+ or diamond to use for the alignment.", flag="--software", choices=["blastx","blastn","diamond"], default="blastx", add_to="assembly_annot_db")
            # Parameter gene ontology
            self.add_input_file("ontology", "The gene ontologies for the contigs.", display_name="Gene ontology", flag="--go", group="ANNOTATION section")
            # Parameter prediction and GO
            self.add_parameter("skip_interpro", "Skip interproscan step (interproscan is used to add annotations, predictions and is necessary to find GO).",
                               display_name="Skip InterProScan", flag="--skip-iprscan", type="bool", default=False,group="IPRscan section")
            # Parameter ORF
            self.add_parameter("max_orf_nb", "The maximum number of ORF by sequence.", display_name="Maximum number ORF", type="int", default=1,group="IPRscan section")
            
            self.add_parameter("skip_repeatmasker", "Skip repeatmasker step (repeatmasker is used to add structurals annotations).",
                               display_name="Skip Repeatmasker", flag="--skip-rm", type="bool", default=False, group="ANNOTATION section")
            # Parameter ncrna
            self.add_parameter("skip_trnascanse", "Skip tRNAscan-SE step (tRNAscan-SE is used to add trna predictions).",
                               display_name="Skip tRNAscan-SE", flag="--skip-trna", type="bool", default=False, group="ANNOTATION section")
            self.add_parameter("skip_rnammer", "Skip RNAmmer step (RNAmmer is used to add rRNA predictions).",
                               display_name="Skip RNAmmer", flag="--skip-rnammer", type="bool", default=False, group="ANNOTATION section")
            
            # Parameter GATK
            self.add_parameter("two_steps_calling", "The SNP calling is realised in two step. The first step (recalibration, calling, filter) has hard filters. The second step (recalibration, calling, filter) has standard filters and the variants detected in the first step are used as database of known polymorphic sites.",
                               display_name="Two steps for SNP calling", type="bool", default=False,group="VARIANT section")
            # Parameter variant annotation
            self.add_multiple_parameter("variant_annot_db", "Databank used to process variant annotation by sequence similarity.", group="VARIANT section")
            self.add_parameter("species", "Reference species used to annotate.", required=True, add_to="variant_annot_db")
            self.add_input_file("fasta", "Reference species sequences.", required=True, add_to="variant_annot_db")
            self.add_input_file("gtf", "Reference species gene and CDS annotation (GTF).", file_format="gtf", required=True, add_to="variant_annot_db")
            self.add_input_file("vcf", "Reference species known variants.", file_format="vcf", add_to="variant_annot_db")
            
    def process(self):
        JAVA_LARGE_MEM = 3
        JAVA_HUGE_MEM = 10

        # Check librairies sequencers
        for lib_arg in self.library:
            if lib_arg["sequencer"] not in ("MiSeq", "HiSeq2000","HiSeq3000","HiSeq2500", "ILLUMINA","SLX","SOLEXA","SOLID","454","COMPLETE","PACBIO","IONTORRENT","CAPILLARY","HELICOS","UNKNOWN") :
                raise ValueError( "The sequencer '" + lib_arg["sequencer"] + "' is not valid. Sequencer must be in : MiSeq, HiSeqXX00, ILLUMINA, SLX, SOLEXA, SOLID, 454, COMPLETE, PACBIO, IONTORRENT, CAPILLARY, HELICOS, UNKNOWN." )

        # Contigs split
        split = self.add_component( "SplitSeq", [self.assembly["file"], self.assembly["split"]] )
        contigs = split.output_files
        # Contigs annotation
        # ##############################
        # Blast annotation of contigs
        annotations = []
        databanks_weights = {}
        db_output_pattern = []
        blasts = []
        
        blast_index_files=[]
        blast_type=[]
        for db in self.assembly_annot_db :
            if db["program"]  != "diamond" :
                blast_index_files.append(db["file"] )
                if db["type"] == "protein":
                    blast_type.append("prot") 
                else: 
                    blast_type.append("nucl") 
        blast_index = self.add_component( "BlastIndex", [blast_index_files, blast_type] )
        idx=0
        for current_databank in self.assembly_annot_db:
            if current_databank["program"] == "diamond" :
                blast = self.add_component( "Diamond", 
                                        [ contigs, current_databank["file"], current_databank, 
                                          current_databank["evalue"], current_databank["max_hit"], self.min_identity, self.min_coverage,
                                          self.get_resource("refseq_gi_from_accession"), self.get_resource("refseq_gene_from_gi"),8
                                        ],
                                        component_prefix="db_" + str(current_databank["name"]) )
                databanks_weights[os.path.basename( current_databank["file"])]=current_databank["weight"]
            else :  
                refseq_params=[]  
                if current_databank.name.startswith("refseq"):
                    refseq_params=[self.get_resource("refseq_gi_from_accession"), self.get_resource("refseq_gene_from_gi")]
                blast = self.add_component( "Blast", 
                                        [ contigs, blast_index.databanks[idx], current_databank, current_databank["program"],
                                          current_databank["evalue"], current_databank["max_hit"], self.min_identity, self.min_coverage,
                                        ] + refseq_params,
                                        component_prefix="db_" + str(current_databank["name"]) )
                databanks_weights[os.path.basename( blast_index.databanks[idx])]=current_databank["weight"]
                idx +=1    
            annotations.extend(blast.output_files)
            
            blasts.append( blast )
            
        best_annot_search = self.add_component( "BestAnnotSearch", [contigs,annotations,databanks_weights] )
        
        # Rename contigs
        rename_rules = None
        rename_GO = None
        rename_vcf = None
        best_annotations = best_annot_search.best_annotations
        all_annotations = annotations
        if self.rename or self.prefix != None: # If rename or prefix it is ask
            # Build rename rules
            rename_rules = self.add_component( "RenameRules", [contigs, best_annotations, self.rename, self.prefix] )
            # Rename contigs
            rename_contigs = self.add_component( "RenameSeq", [contigs, rename_rules.output_file] )
            contigs = rename_contigs.output_files
            # Rename standard annotations
            rename_all_annotations = self.add_component( "RenameTab", [all_annotations, rename_rules.output_file], component_prefix="annotations" )
            all_annotations = rename_all_annotations.output_files
            # Rename best annotations
            rename_best_annotations = self.add_component( "RenameTab", [best_annotations, rename_rules.output_file], component_prefix="bestAnnot" )
            best_annotations = rename_best_annotations.output_files            
            # If user provide GO
            if self.ontology != None:
                rename_GO = self.add_component( "RenameTab", [self.ontology, rename_rules.output_file], component_prefix="userGO" )
            # If user provide variants
            if self.variant and self.variant["file"] != None:
                rename_vcf = self.add_component( "RenameVCF", [self.variant["file"], rename_rules.output_file], component_prefix="userVCF" )

        # Loads contigs
	merge_contigs = self.add_component( "ConcatenateFiles", [contigs, "contigs.fa", True], component_prefix="contigs" )
        merge_best_annot = self.add_component( "ConcatenateFiles", [best_annotations, "best_annotations.gff", True], component_prefix="bestAnnot" )
        contig_files = [merge_contigs.output_file, rename_rules.output_file] if rename_rules is not None else [merge_contigs.output_file]
        contig = self.add_biomart_load( Contig,
                                        [merge_contigs.output_file, [merge_best_annot.output_file]],
                                        "Assembly",
                                        "assembly",
                                        "0",
                                        self.assembly["software_name"],
                                        self.assembly["software_parameters"],
                                        self.assembly["software_version"],
                                        self.assembly["comments"],
                                        contig_files )
        
        self.add_blast_search( Contig, merge_contigs.output_file )
        
        
        # Loads std annotations
        for db_idx, current_databank in enumerate(self.assembly_annot_db):
            std_annot_idx_start = db_idx * len(split.output_files)
            std_annot_idx_end   = (db_idx * len(split.output_files)) + len(split.output_files)
            current_annot = all_annotations[std_annot_idx_start:std_annot_idx_end]
            merge_std_annot = self.add_component( "ConcatenateFiles", [current_annot, current_databank["name"] + ".gff", True], component_prefix="annotDB_" + str(db_idx) )
            annot = self.add_biomart_load( Annotation,
                                           [merge_std_annot.output_file, contig.new_table],
                                           "Annotation",
                                           "blast_annotation",
                                           "0",
                                           current_databank["program"],
                                           "-db " + current_databank['name'] + " " + blasts[db_idx].options + " | Filters : " + blasts[db_idx].filter_options,
                                           blasts[db_idx].get_version(),
                                           "Annotates by similarities found in a known databank.",
                                           [merge_std_annot.output_file] )
        self.dna = self.add_biomart_load( DNA, [merge_contigs.output_file] )
        # Repeat annotation
        # ##############################
        if not self.skip_repeatmasker:
            repeat_masker = self.add_component( "RepeatMasker", [contigs, self.project.species] )
            merge_repeat_predict = self.add_component( "ConcatenateFiles", [repeat_masker.output_files, "repeats_predictions.gff", True], component_prefix="repeatPredict" )
            best_annot = self.add_biomart_load( Prediction,
                                                [merge_repeat_predict.output_file],
                                                "Reapeat prediction",
                                                "repeat_prediction",
                                                "0",
                                                "RepeatMasker",
                                                repeat_masker.options,
                                                repeat_masker.get_version(),
                                                "Predicts interspersed repeats and low complexity sequences.",
                                                [merge_repeat_predict.output_file] )

        # Functional annotation
        # ##############################
        if not self.skip_trnascanse:
            trna = self.add_component( "TrnaScanSe", [contigs, "eukaryota"] )
            merge_trna_predict = self.add_component( "ConcatenateFiles", [trna.output_files, "trna_predictions.gff", True], component_prefix="trnaPredict" )
            self.add_biomart_load( Prediction,
                                       [merge_trna_predict.output_file],
                                       "Prediction",
                                       "TrnaScanSe_prediction",
                                       "0",
                                       "tRNAscanSE",
                                       trna.options,
                                       trna.get_version(),
                                       "Predicts tRNA with tRNAscan-SE.",
                                       [merge_trna_predict.output_file] )
	if not self.skip_rnammer:
            rnammer = self.add_component( "RNAmmer", [contigs, "eukaryota"] )
            merge_rnammer_predict = self.add_component( "ConcatenateFiles", [rnammer.output_files, "rnammer_predictions.gff", True], component_prefix="rnammerPredict" )
            self.add_biomart_load( Prediction,
                                       [merge_rnammer_predict.output_file],
                                       "Prediction",
                                       "RNAmmer_prediction",
                                       "0",
                                       "RNAmmer",
                                       rnammer.options,
                                       rnammer.get_version(),
                                       "Predicts rRNA with RNAmmer.",
                                       [merge_rnammer_predict.output_file] )
	if not self.skip_interpro:
            get_orf = self.add_component( "GetORF", [contigs, self.max_orf_nb] )
            # Process
            interpro = self.add_component( "InterProScan", [get_orf.output_files, "p"] )
            interpro_to_rnabrowse = self.add_component( "InterProScan2RnaBrowse", [interpro.output_files, get_orf.output_files, self.get_resource("ontology_desc")] )
            # Merge
            merge_iprscan_go = self.add_component( "ConcatenateFiles", [interpro_to_rnabrowse.go, "interproscan_GO.tsv", True], component_prefix="interproGO" )
            go_convert = self.add_component( "GO2topGO", [merge_iprscan_go.output_file], component_prefix="GOInterpro" )
            merge_iprscan_predict = self.add_component( "ConcatenateFiles", [interpro_to_rnabrowse.annotations, "interproscan_predictions.gff", True], component_prefix="interproPredict" )
            # load
            self.add_biomart_load( GO, [merge_iprscan_go.output_file], "Gene Ontology",
                                   "GO",
                                   "0",
                                   "interproscan",
                                   interpro.options,
                                   interpro.get_version(),
                                   "Predicts with InterProScan.",
                                   [merge_iprscan_go.output_file,go_convert.output]  )
            self.add_biomart_load( Prediction,
                                   [merge_iprscan_predict.output_file],
                                   "Prediction",
                                   "iprscan_prediction",
                                   "0",
                                   "interproscan",
                                   interpro.options,
                                   interpro.get_version(),
                                   "Predicts with InterProScan.",
                                   [merge_iprscan_predict.output_file] )
        
        trandecoder = self.add_component( "Transdecoder", [merge_contigs.output_file] )
        self.add_biomart_load( Prediction,
                                       [trandecoder.output_file],
                                       "Prediction",
                                       "ORF_prediction",
                                       "0",
                                       "Transdecoder",
                                       "",
                                       trandecoder.get_version(),
                                       "Predicts orf with trandecoder.",
                                       [trandecoder.output_file] )
        # User provided GO
        if self.ontology != None:
            user_go_file = ( rename_GO.output_files[0] if rename_GO != None else self.ontology )
            go_convert = self.add_component( "GO2topGO", [user_go_file], component_prefix="GOUser" )
            
            user_go_load = self.add_biomart_load( GO, [user_go_file], "Gene Ontology",
                                   "GO",
                                   "0",
                                   "GO provided by user",
                                   None,
                                   None,
                                   None,
                                   [user_go_file,go_convert.output] , None )

        # Alignment
        # ##############################
	all_bams = []
        all_bais = []
        single_fastq, pair1_fastq, pair2_fastq = [], [], []
        single_librairies_names, pair_librairies_names = [], []
        single_librairies_sequencers, pair_librairies_sequencers = [], []
        for lib_arg in self.library:
            if lib_arg["type"] in Library.PAIRED_TYPES:
                pair_librairies_names.append( lib_arg["library_name"] )
                if lib_arg["sequencer"].lower().startswith("hiseq") or lib_arg["sequencer"].lower().startswith("miseq"):
                    pair_librairies_sequencers.append( "ILLUMINA" )
                else:
                    pair_librairies_sequencers.append( lib_arg["sequencer"] )
                pair1_fastq.append(lib_arg["files"][0])
                pair2_fastq.append(lib_arg["files"][1])
            else:
                single_librairies_names.append( lib_arg["library_name"] )
                if lib_arg["sequencer"].lower().startswith("hiseq") or lib_arg["sequencer"].lower().startswith("miseq"):
                    single_librairies_sequencers.append( "ILLUMINA" )
                else:
                    single_librairies_sequencers.append( lib_arg["sequencer"] )
                single_fastq.append(lib_arg["files"][0])

        # Index contigs
        index_fai_dict = self.add_component( "IndexFaiDict", [merge_contigs.output_file, self.assembly["split"]] )
        index_bwa = self.add_component( "BWAIndex", [merge_contigs.output_file] )
        # Align paired-end reads
	if len(pair2_fastq)>0 and len(pair1_fastq)>0 :
            align_paired     = self.add_component( "BWA", [index_bwa.databank, pair1_fastq, pair2_fastq, pair_librairies_names, "mem"], component_prefix="paired" )
            index_bam_paired = self.add_component( "SamtoolsIndex", [align_paired.bam_files, 2, 2], component_prefix="paired" )
            all_bams = all_bams + index_bam_paired.sorted_bams
            all_bais = all_bais + index_bam_paired.output_files
            # add alignment analysis
            result_elements = list()
            for lib_name in pair_librairies_names:
                result_elements.append( ["library_id", self.get_library_from_name(lib_name).id, "INT", "alignment"] )
            alignment = self.add_analysis( analysis_name="Alignment",
                                           analysis_type="alignment",
                                           analysis_hidden="0",
                                           soft_name="BWA",
                                           soft_parameters=align_paired.algorithm,
                                           soft_version=align_paired.get_version(),
                                           comments="Paired alignment",
                                           result_elements=result_elements,
                                           files=index_bam_paired.sorted_bams+index_bam_paired.output_files )

        # Align single reads
	if len(single_fastq)>0 :
            align_single     = self.add_component( "BWA", [index_bwa.databank, single_fastq, None, single_librairies_names, "mem"], component_prefix="single" )
            index_bam_single = self.add_component( "SamtoolsIndex", [align_single.bam_files, 2, 2], component_prefix="single" )
            all_bams = all_bams + index_bam_single.sorted_bams
            all_bais = all_bais + index_bam_single.output_files
            # Add alignment analysis
            result_elements = list()
            for lib_name in single_librairies_names:
                result_elements.append( ["library_id", self.get_library_from_name(lib_name).id, "INT", "alignment"] )
            alignment = self.add_analysis( analysis_name="Alignment",
                                           analysis_type="alignment",
                                           analysis_hidden="0",
                                           soft_name="BWA",
                                           soft_parameters=align_single.algorithm,
                                           soft_version=align_single.get_version(),
                                           comments="Single alignment",
                                           result_elements=result_elements,
                                           files=index_bam_single.sorted_bams+index_bam_single.output_files )

        # Count and coverage on non filtered bam
	count_compute = self.add_component( "CountReads", [all_bams] )

        max_lib_size = self.project.get_max_library_size()
        step = max_lib_size/30
        rarefaction_curve = self.add_component( "RarefactionCurves", [all_bams,step] )
        coverage      = self.add_component( "Coverage", [all_bams, all_bais, self._nb_seq(self.assembly["file"]), self.assembly["split"]] )
        expression    = self.add_biomart_load( Expression, [count_compute.matrix, self.project.libraries, coverage.output_files,rarefaction_curve.stdouts] )
        #Add venn application in web page
        self.add_venn()
        #Add normalisation method
        self.add_component("NormalizeRawCount", [count_compute.matrix, len(all_bams)])
        # Finalize contigs insertion
        update_contigs = self.add_component( "UpdateContigs", [contig.new_table, expression.new_table] )

        # Variant calling and annotation
        ##############################
	vcf_file = None
        gatk_indel_filter_options=""
        gatk_snp_filter_options=""
        if self.variant and self.variant["file"] != None: # If user provide variant file
            vcf_file = self.variant["file"] if rename_vcf == None else rename_vcf.output_files[0]
        else: # Variant calling
            gatk_preprocess_bams = list()
            # Pre-process
            if len(single_fastq) > 0:
                variant_preprocess_single = self.add_component("VariantPreprocess", [index_fai_dict.databank, index_bam_single.sorted_bams, single_librairies_sequencers, False, 30, JAVA_LARGE_MEM], component_prefix="single" )
                gatk_preprocess_single    = self.add_component("GatkPreprocess", [variant_preprocess_single.output_files, variant_preprocess_single.index_files, index_fai_dict.databank, JAVA_LARGE_MEM, True], component_prefix="single" )
                gatk_preprocess_bams += gatk_preprocess_single.output_files
            if len(pair2_fastq) > 0:
                variant_preprocess_paired = self.add_component("VariantPreprocess", [index_fai_dict.databank, index_bam_paired.sorted_bams, pair_librairies_sequencers, True, 30, JAVA_LARGE_MEM], component_prefix="paired" )
                gatk_preprocess_paired    = self.add_component("GatkPreprocess", [variant_preprocess_paired.output_files, variant_preprocess_paired.index_files, index_fai_dict.databank, JAVA_LARGE_MEM, True], component_prefix="paired" )
                gatk_preprocess_bams += gatk_preprocess_paired.output_files
            # Variant calling
            pre_vcf_file = None
            if self.two_steps_calling:
                # SNP calling with hards parameters to produce a database of safe polymorphic sites
                gatk_recalibration_pre_step    = self.add_component("GatkRecalibration", [gatk_preprocess_bams, index_fai_dict.databank, None, JAVA_LARGE_MEM], component_prefix="pre_step")
                gatk_haplotype_caller_pre_step = self.add_component("GatkHaplotypeCaller", [gatk_recalibration_pre_step.output_files, index_fai_dict.databank, 30, 30, 10,index_fai_dict.output_intervals, JAVA_HUGE_MEM], component_prefix="pre_step")
                gatk_filter_pre_step           = self.add_component("GatkVariantFilter", [gatk_haplotype_caller_pre_step.output_file, index_fai_dict.databank,
                                                                                          '-window 35 -cluster 3 --filterExpression "QD < 2.0 || FS > 100.0 || MQ < 40.0 || HaplotypeScore > 13.0 || MappingQualityRankSum < -12.5 || ReadPosRankSum < -8.0" --filterName "hard_filter"', 
                                                                                          '-window 35 -cluster 3 --filterExpression "QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0" --filterName "hard_filter"',
                                                                                          JAVA_LARGE_MEM],
                                                                    component_prefix="pre_step")
                pre_vcf_file = gatk_filter_pre_step.output_file
            # SNP calling with standards parameters to produce finals variants
            gatk_recalibration    = self.add_component("GatkRecalibration", [gatk_preprocess_bams, index_fai_dict.databank, pre_vcf_file, JAVA_LARGE_MEM], component_prefix="final")
            gatk_haplotype_caller = self.add_component("GatkHaplotypeCaller", [gatk_recalibration.output_files, index_fai_dict.databank, 20, 20, 10,index_fai_dict.output_intervals, JAVA_HUGE_MEM], component_prefix="final")
            gatk_filter           = self.add_component("GatkVariantFilter", [gatk_haplotype_caller.output_file, index_fai_dict.databank,
                                                                             '-window 18 -cluster 3 -filterName FS --filterExpression \"FS > 30.0\"',
                                                                             '-window 18 -cluster 3 -filterName FS --filterExpression \"FS > 30.0\"',
                                                                             JAVA_LARGE_MEM],
                                                       component_prefix="final")
            gatk_indel_filter_options = gatk_filter.indel_filter_options
            gatk_snp_filter_options = gatk_filter.snp_filter_options
            vcf_file = gatk_filter.output_file_with_id 
        # Variant annotation
        
        variant_annotations = []
        split_vcf = self.add_component("SplitVcf", [vcf_file, contigs])
        if self.variant_annot_db["fasta"] != None:
            tsnpannot_index = self.add_component( "BlastIndex", [[self.variant_annot_db["fasta"]], ["prot"]], component_prefix="tsnpannot" )
            tsnpannot = self.add_component("tSNPannot", [split_vcf.splitted_variant_files, contigs, self.variant_annot_db["species"], tsnpannot_index.databanks[0], self.variant_annot_db["gtf"], self.variant_annot_db["vcf"]])
            merge_tsnpannot = self.add_component( "ConcatenateFiles", [tsnpannot.annotation_file,  "variant_annotation.gff", True], component_prefix="tnsannot" )
            variant_annotations = [merge_tsnpannot.output_file]
        # Variant load
        variant = None
        if self.variant and self.variant["file"] != None: # If user provide variant file
            variant = self.add_biomart_load( vVariant,
                                             [vcf_file, merge_contigs.output_file, variant_annotations],
                                             "Variants calling",
                                             "prediction_variants",
                                             "0",
                                             self.variant["software_name"],
                                             self.variant["software_parameters"],
                                             self.variant["software_version"],
                                             self.variant["comments"],
                                             [self.variant["file"]],None )
        else:
            variant = self.add_biomart_load( vVariant,
                                             [vcf_file, merge_contigs.output_file, variant_annotations],
                                             "Variants calling",
                                             "prediction_variants",
                                             "0",
                                             "GATK",
                                             "HaplotypeCaller",
                                             gatk_haplotype_caller.get_version(),
                                             "Preprocess : rmdup, qual, addreadgroup Gatkpreprocess : SplitNCigarReads, Realignment | GatkRecalibration | GatkHaplotypeCaller | GatkVariantFilter indel : " + gatk_indel_filter_options.replace('"','') + " | GatkVariantFilter snp : " + gatk_snp_filter_options.replace('"',''),
                                             [vcf_file],None )
        
        
        snp_eff = self.add_component("SnpEff", [index_fai_dict.databank, trandecoder.output_file, split_vcf.splitted_variant_files, JAVA_HUGE_MEM])
        
        variant_effect = self.add_biomart_load(Effect,[snp_eff.output_file],
                              "Variants effect prediction",
                              "variant_annotation",
                              "0",
                              "SnpEff",
                              "",
                              snp_eff.get_version(),
                              "SnpEff on transdecoder transcripts",
                              [snp_eff.output_file])    
          
        if self.variant_annot_db["fasta"] != None:
            variant_annot = self.add_biomart_load( vAnnotation,
                                                   [variant_annotations, variant.new_table],
                                                   "Variant annotation",
                                                   "variant_annotation",
                                                   "0",
                                                   "tSNPannot",
                                                   tsnpannot.options,
                                                   tsnpannot.get_version(),
                                                   "Annotates SNP by similarities with a reference species.",
                                                   variant_annotations )
        variant_count = self.add_biomart_load(CountAlleleLib, [vcf_file, self.project.libraries])
        contig_variant = self.add_biomart_load(Variant, [vcf_file, variant.new_table])

    def load(self):
        
        # first check all inputs
        contig_names = Contig.get_contigs(self.assembly["file"])
        self.check_load_format(contig_names)

        # set variables
        best_annotation_file = None
        annotation_files=[]
        for annotation_arg in self.assembly_annot:
            if annotation_arg["is_best"]:
                best_annotation_file=annotation_arg["file"]
            annotation_files.append(annotation_arg["file"])

        alignment_files=[]
        for alignment_arg in self.alignment:
            alignment_files.extend(alignment_arg["files"])
        self.check_libraries(alignment_files)

        # list fastq files to save
        fastq_files=[]
        for lib_arg in self.library:
            fastq_files+=lib_arg["files"]

        # load the contig table
        contig_parameters = []
        if best_annotation_file:

            contig_parameters = [self.assembly["file"], [best_annotation_file]]
        elif annotation_files and self.best_annotation_source:
            contig_parameters = [self.assembly["file"], annotation_files, self.best_annotation_source]
        else:
            sys.stderr.write("Please provide a best annotation file or at least the best annotation source!\n")
            sys.exit(1)
        contig = self.add_biomart_load(Contig,
                                       contig_parameters,
                                       "Assembly",
                                       "assembly",
                                       "0",
                                       self.assembly["software_name"],
                                       self.assembly["software_parameters"],
				       self.assembly["software_version"],
                                       self.assembly["comments"], [self.assembly["file"]]+fastq_files)
        
        self.add_blast_search( Contig, self.assembly["file"] )
        self.add_venn()
        # load the annotation table
        for annotation_arg in self.assembly_annot:
            annot = self.add_biomart_load(Annotation,
                                          [annotation_arg["file"],contig.new_table],
                                          "Annotation",
                                          "annotation",
                                          "0",
                                          annotation_arg["software_name"],
                                          annotation_arg["software_parameters"],
                                          annotation_arg["software_version"],
                                          annotation_arg["comments"], [annotation_arg["file"]])
        
        # load the prediction table
        for prediction_arg in self.prediction:
            predict = self.add_biomart_load(Prediction,
                                            [prediction_arg["file"]],
                                            "Prediction",
                                            "prediction",
                                            "0",
                                            prediction_arg["software_name"],
                                            prediction_arg["software_parameters"],
                                            prediction_arg["software_version"],
                                            prediction_arg["comments"],
                                            [prediction_arg["file"]])
        
        # load the dna table
        dna = self.add_biomart_load(DNA, [self.assembly["file"]])

        # load the go table
        if self.go != None:
            go_convert = self.add_component( "GO2topGO", [self.go], component_prefix="GOUser" )
            go = self.add_biomart_load(GO, [self.go], "Gene Ontology",
                                   "GO",
                                   "0",
                                   "GO provided by user",
                                   None,
                                   None,
                                   None,
                                   [self.go,go_convert.output],
                                   None)

        # load the keyword table
        if self.keyword != None:
            kw = self.add_biomart_load(Keyword, [self.keyword])

        # load the variant tables
        if self.variant["file"] != None:
            best_variant_annotation_file, variant_annotation_files = None, []
            for variant_annotation_arg in self.variant_annot:
                if variant_annotation_arg["is_best"] and variant_annotation_arg.has_key("is_best"):
                    best_variant_annotation_file = variant_annotation_arg["file"]
                variant_annotation_files.append(variant_annotation_arg["file"])
            variant_parameters = [self.variant["file"],self.assembly["file"], variant_annotation_files, best_variant_annotation_file]

            variant = self.add_biomart_load(vVariant,
                                            variant_parameters,
                                            "Variants calling",
                                            "prediction_variants",
                                            "0",
                                            self.variant["software_name"],
                                            self.variant["software_parameters"],
                                            self.variant["software_version"],
                                            self.variant["comments"], [self.variant["file"]])
            
            for variant_annotation_arg in self.variant_annot:
                annot = self.add_biomart_load(vAnnotation,
                                              [variant_annotation_files, variant.new_table],
                                              "Variant annotation",
                                              "variant_annotation",
                                              "0",
                                              variant_annotation_arg["software_name"],
                                              variant_annotation_arg["software_parameters"],
                                              variant_annotation_arg["software_version"],
                                              variant_annotation_arg["comments"], [variant_annotation_arg["file"]])
            variant_count = self.add_biomart_load(CountAlleleLib, [self.variant["file"], self.project.libraries])
            contig_variant = self.add_biomart_load(Variant, [self.variant["file"], variant.new_table])

            self.add_biomart_load(Effect,[self.variant["file"]],
                              "Variants effect prediction",
                              "variant_annotation",
                              "0",
                              self.variant_effect["software_name"],
                              self.variant_effect["software_parameters"],
                              self.variant_effect["software_version"],
                              self.variant_effect["comments"],
                              []) 
        # add alignment analysis
        for alignment_arg in self.alignment:
            bam_files, result_elements = [], []
            for bam in alignment_arg["files"]:
                bam_files.append(bam)
                lib_name = os.path.splitext(os.path.basename(bam))[0]
                result_elements.append(["library_id", self.get_library_from_name(lib_name).id, "INT", "alignment"])
            alignment = self.add_analysis(analysis_name="Alignment", analysis_type="alignment", analysis_hidden="0", 
                              soft_name=alignment_arg["software_name"], soft_parameters=alignment_arg["software_parameters"],
                              soft_version=alignment_arg["software_version"], comments=alignment_arg["comments"], 
                              result_elements=result_elements, files=bam_files)

        coverage_files = []
        rarefaction_file = None
        if len(alignment_files) > 0:
            index_bam = self.add_component("SamtoolsIndex",[alignment_files, 2, 2])
            max_lib_size = self.project.get_max_library_size()
            step = max_lib_size/30
            rarefaction_curve = self.add_component( "RarefactionCurves", [index_bam.sorted_bams,step] )
            rarefaction_files = rarefaction_curve.stdouts
            coverage = self.add_component("Coverage",[index_bam.sorted_bams, index_bam.output_files, len(contig_names.keys())])
            coverage_files = coverage.output_files
            # if there is no count matrix file provided, count the reads 
            if not self.count_matrix:
                count = self.add_component("CountReads",[index_bam.sorted_bams])
                self.count_matrix = count.matrix

        counts = self.add_biomart_load(Expression, [self.count_matrix, self.project.libraries, coverage_files, rarefaction_files])
        self.add_component("NormalizeRawCount", [self.count_matrix, len(alignment_files)])
        # finalize contigs insertion
        update_contigs = self.add_component("UpdateContigs", [contig.new_table, counts.new_table])

    
    def check_load_format(self,contig_names):

        for annot_arg in self.assembly_annot:
            err_contigs=get_gff3_unmatch_contigs(contig_names,annot_arg["file"])
            if len (err_contigs) > 0:
                sys.stderr.write('Unknown sequence name >' + ",".join(err_contigs)[:-1] + '< in GFF3 ' + annot_arg["file"] + ' \n')
                sys.exit(1)

        for predict_arg in self.prediction:
            err_contigs = get_gff3_unmatch_contigs( contig_names, predict_arg["file"] )
            if len(err_contigs) > 0:
                sys.stderr.write('Unknown sequence name >' + ",".join(err_contigs)[:-1] + '< in GFF3 ' + predict_arg["file"] + ' \n')
                sys.exit(1)

        if self.variant["file"] != None:
            err_contigs=get_vcf_unmatch_contigs(contig_names,self.variant["file"])
            if len (err_contigs) > 0:
                sys.stderr.write('Unknown sequence name >' + ",".join(err_contigs)[:-1] + '< in VCF file ' + self.variant["file"] + '\n')
                sys.exit(1)

        if self.go != None:
            err_contigs=get_GO_unmatch_contigs(contig_names,self.go)
            if len (err_contigs) > 0:
                sys.stderr.write('Unknown sequence name >' + ",".join(err_contigs)[:-1] + '< in GO file ' + self.go + '\n')
                sys.exit(1)

        if self.keyword != None:
            err_contigs=get_keyword_unmatch_contigs(contig_names,self.keyword)
            if len (err_contigs) > 0:
                sys.stderr.write('Unknown sequence name >' + ",".join(err_contigs)[:-1] + '< in keyword file ' + self.keyword + '\n')
                sys.exit(1)

    def check_libraries (self, alignment_files):
        # Do not check fastq file, done in Library
        if alignment_files:
            for bam in alignment_files:
                if not os.path.exists(bam):
                    sys.stderr.write(bam + " file does not exist. Please provide a valid bam file!\n")
                    sys.exit(1)
                lib_name = os.path.splitext(os.path.basename(bam))[0]
                if self.get_library_from_name(lib_name) == None:
                    sys.stderr.write(bam + ' file should be linked to a library named '+ lib_name + '.\n')
                    sys.exit(1)
        if self.count_matrix:
            fh = open(self.count_matrix)
            line = fh.readline()
            matrix_libs = []
            parts = line.rstrip().split("\t")
            matrix_libs = parts[1:]
            for lib_name in matrix_libs :
                if self.get_library_from_name(lib_name) == None:
                    sys.stderr.write('Library '+lib_name+' defined at the first line of the matrix file has not been defined (see --library option to define a library)\n')
                    sys.exit(1)
        if self.variant["file"] != None:
            reader = VCFReader(self.variant["file"])
            for fullpath, lib_name in reader.samples_name:
                if self.get_library_from_name(lib_name) == None:
                    sys.stderr.write('Library '+lib_name+' defined in the vcf file has not been defined (see --library option to define a library)\n')
                    sys.exit(1)

    @staticmethod
    def load_config_parser(arg_lines):
        config = ConfigParser()
        config.readfp(StringIO.StringIO('\n'.join(arg_lines)))
        arguments = []
        add_analysis_options = ['software_name', 'software_parameters',  'software_version', 'comments']

        section = 'project'
        if config.has_section(section):
            for option in config.options(section):
                if config.has_option(section, option):
                    arguments.extend( [ '--'+option.replace('_','-'),  config.get(section, option)  ] )

        section = 'assembly'
        if config.has_section(section):
            arguments.append('--assembly')
            if config.has_option(section, 'file'):
                arguments.append( 'file=' + config.get(section, 'file') )
            for option in add_analysis_options:
                if config.has_option(section, option):
                    arguments.append( option.replace('_', '-')  + '=' + config.get(section, option) )

        section = 'gene_ontology'
        if config.has_section(section):
            if config.has_option(section, 'file'):
                arguments.extend( [ '--go', config.get(section, 'file') ] )

        section = 'keyword'
        if config.has_section(section):
            if config.has_option(section, 'file'):
                arguments.extend( [ '--keyword', config.get(section, 'file') ] )

        section = 'expression'
        if config.has_section(section):
            if config.has_option(section, 'file'):
                arguments.extend( ['--count-matrix', config.get(section, 'file')] )

        section = 'variant'
        if config.has_section(section):
            arguments.append('--variant');
            if config.has_option(section, 'file'):
                arguments.append(  'file=' + config.get(section, 'file') )
            for option in add_analysis_options:
                if config.has_option(section, option):
                    arguments.append(  option.replace('_', '-')  + '=' + config.get(section, option) )

        section = 'variant_annotations'
        if config.has_section(section):
            lhash = {}
            for option in config.options(section):
                if config.get(section, option):
                    if option == 'best_annotation_source':
                        arguments.extend( ['--variant-best-annotation-source', config.get(section, option) ])
                    else:
                        name, prop = option.split('.', 2)
                        if not lhash.has_key(name):
                            lhash[name] = []
                        if prop == 'file':
                            lhash[name].append( 'file=' + config.get(section, option) )
                        else:
                            lhash[name].append( prop.replace('_', '-') +'=' + config.get(section, option) )
            for x in lhash.values():
                arguments.extend( [ '--variant-annot'] + x )
        
        section = 'variant_effect'
        if config.has_section(section):
            arguments.append('--variant-effect');
            if config.has_option(section, 'orf_gff'):
                arguments.append(  'orf-gff=' + config.get(section, 'orf_gff') )
            for option in add_analysis_options:
                if config.has_option(section, option):
                    arguments.append(  option.replace('_', '-')  + '=' + config.get(section, option) )

                
        section = 'assembly_annotations'
        if  config.has_section(section):
            lhash = {}
            for option in config.options(section):
                if config.get(section, option):
                    if option == 'best_annotation_source':
                        arguments.extend( ['--best-annotation-source', config.get(section, option) ])
                    else:
                        name, prop = option.split('.', 2)
                        if not lhash.has_key(name):
                            lhash[name] = []
                        if prop == 'file':
                            lhash[name].append( 'file=' + config.get(section, option) )
                        elif prop == 'is_best' and config.getboolean(section, option):
                            lhash[name].append('is-best')
                        else:
                            lhash[name].append( prop.replace('_', '-') +'=' + config.get(section, option) )
            for x in lhash.values():
                arguments.extend( [ '--assembly-annot'] + x )

        section = 'predictions'
        if  config.has_section(section):
            lhash = {}
            for option in config.options(section):
                if config.get(section, option):
                    name, prop = option.split('.', 2)
                    if not lhash.has_key(name):
                        lhash[name] = []
                    if prop == 'file':
                        lhash[name].append( 'file=' + config.get(section, option) )
                    else:
                        lhash[name].append( prop.replace('_', '-') +'=' + config.get(section, option) )
            for x in lhash.values() :
                arguments.extend( [ '--prediction'] + x )

        section = 'alignments'
        if config.has_section(section):
            lhash = {}
            for option in config.options(section):
                if config.get(section, option):
                    name, prop = option.split('.', 2)
                    if not lhash.has_key(name):
                        lhash[name] = ['--alignment']
                    if prop in add_analysis_options:
                        lhash[name].append( prop.replace('_', '-') +'=' + config.get(section, option) )
                    elif prop.startswith("file"):
                        lhash[name].append( 'files=' + config.get(section, option) )
            for x in lhash.values():
                arguments.extend( x )

        section = 'libraries'
        if config.has_section(section):
            lhash = {}
            for option in config.options(section):
                if config.get(section, option):
                    name, prop = option.split('.', 2)
                    if not lhash.has_key(name):
                        lhash[name] = []
                    if prop == "files":
                        for file in config.get(section, option).split(","):
                            lhash[name].append( prop.replace('_','-') +'=' + file.strip() )
                    else:
                        lhash[name].append( prop.replace('_','-') +'=' + config.get(section, option) )
            for x in lhash.values():
                arguments.extend( [ '--library'] + x)

        return arguments

    @staticmethod
    def config_parser(arg_lines):
        config = ConfigParser()
        config.readfp(StringIO.StringIO('\n'.join(arg_lines)))
        arguments = []
        add_analysis_options = ['software_name', 'software_parameters',  'software_version', 'comments']

        section = 'project'
        if config.has_section(section):
            for option in config.options(section):
                if config.has_option(section, option):
                    arguments.extend( [ '--'+option.replace('_','-'),  config.get(section, option)  ] )

        section = 'assembly'
        if config.has_section(section):
            arguments.append('--assembly')
            if config.has_option(section, 'file'):
                arguments.append(  'file=' + config.get(section, 'file') )
            if config.has_option(section, 'split'):
                arguments.append(  'split=' + config.get(section, 'split') )
            for option in add_analysis_options:
                if config.has_option(section, option):
                    arguments.append( option.replace('_', '-')  + '=' + config.get(section, option) )

        section = 'gene_ontology'
        if config.has_section(section):
            if config.has_option(section, 'file'):
                arguments.extend( [ '--go', config.get(section, 'file') ] )

        section = 'rename'
        if config.has_section(section):
            if config.has_option(section, 'best_annot') and config.getboolean(section, 'best_annot'):
                arguments.extend( [ '--rename' ] )
            if config.has_option(section, 'prefix'):
                arguments.extend( [ '--prefix', config.get(section, 'prefix') ] )

        section = 'interproscan'
        if config.has_section(section):
            if config.has_option(section, 'max_orf_nb'):
                arguments.extend( [ '--max-orf-nb', config.get(section, 'max_orf_nb') ] )
            if config.has_option(section, 'skip_iprscan') and config.getboolean(section, 'skip_iprscan'):
                arguments.extend( [ '--skip-iprscan' ] )

        section = 'ncrna'
        if config.has_section(section):
            if config.has_option(section, 'skip_trnascanse') and config.getboolean(section, 'skip_trnascanse'):
                arguments.extend( [ '--skip-trna' ] )
            if config.has_option(section, 'skip_rnammer') and config.getboolean(section, 'skip_rnammer'):
                arguments.extend( [ '--skip-rnammer' ] )

        section = 'repeatmasker'
        if config.has_section(section):
            if config.has_option(section, 'skip_rm') and config.getboolean(section, 'skip_rm'):
                arguments.extend( [ '--skip-rm' ] )

        section = 'variant_calling'
        if config.has_section(section):
            if config.has_option(section, 'two_steps_calling') and config.getboolean(section, 'two_steps_calling'):
                arguments.extend( [ '--two-steps-calling' ] )

        section = 'variant_annotation'
        if config.has_section(section):
            param = []
            if config.has_option(section, 'species'):
                param.append( 'species="' + config.get(section, 'species') + '"' )
            if config.has_option(section, 'fasta'):
                param.append( 'fasta=' + config.get(section, 'fasta') )
            if config.has_option(section, 'gtf'):
                param.append( 'gtf=' + config.get(section, 'gtf') )
            if config.has_option(section, 'vcf'):
                param.append( 'vcf=' + config.get(section, 'vcf') )
            arguments.append( '--variant-annot-db' )
            arguments.extend( param )

        section = 'variant'
        if config.has_section(section):
            arguments.append('--variant');
            if config.has_option(section, 'file'):
                arguments.append(  'file=' + config.get(section, 'file') )
            for option in add_analysis_options:
                if config.has_option(section, option):
                    arguments.append(  option.replace('_', '-')  + '=' + config.get(section, option) )

        section = 'libraries'
        if config.has_section(section):
            lhash = {}
            for option in config.options(section):
                if config.get(section, option):
                    name, prop = option.split('.', 2)
                    if not lhash.has_key(name):
                        lhash[name] = []
                    if prop == "files":
                        for file in config.get(section, option).split(","):
                            lhash[name].append( prop.replace('_','-') +'=' + file.strip() )
                    else:
                        lhash[name].append( prop.replace('_','-') +'=' + config.get(section, option) )
            for x in lhash.values():
                arguments.extend( [ '--library'] + x)

        section = 'assembly_annotations'
        if config.has_section(section):
            databanks = dict()
            items = config.items(section)
            for tag, value in items:
                if tag == 'min_identity':
                    arguments.extend( ['--min-identity', value] )
                elif tag == 'min_coverage':
                    arguments.extend( ['--min-coverage', value] )
                else:
                    databank_id, param = tag.split('.')
                    if not databanks.has_key( databank_id ):
                        databanks[databank_id] = list()
                    if param == 'file':
                        databanks[databank_id].append( 'file=' + value )
                    if param == 'type':
                        databanks[databank_id].append( 'type=' + value )
                    if param == 'name':
                        databanks[databank_id].append( 'name=' + value )
                    if param == 'weight':
                        databanks[databank_id].append( 'weight=' + value )
                    if param == 'species':
                        databanks[databank_id].append( 'species="' + value + '"' )
                    if param == 'evalue':
                        databanks[databank_id].append( 'evalue=' + value )
                    if param == 'max_hit':
                        databanks[databank_id].append( 'max-hit=' + value )
                    if param == 'program':
                        databanks[databank_id].append( 'software=' + value )
            for current_databank in databanks:
                arguments.append('--assembly-annot-db')
                for param in databanks[current_databank]:
                    arguments.append( param )

        return arguments

    def _nb_seq( self, filepath ):
        """
         @summary : Return the number of sequences.
          @param filepath : [str] the sequence file to process.
         @return : [int] The number of sequences.
        """
        nb_seq = 0
        seq_fh = SequenceReader( filepath )
        for seq_record in seq_fh:
            nb_seq += 1
        return nb_seq
