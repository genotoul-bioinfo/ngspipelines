#
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from subprocess import Popen, PIPE

from jflow.component import Component

def rnammer2gff(input_file, output_file):
    """
    """
    import re
    import jflow.seqio as seqio
    from workflows.rnaseqdenovo.lib.annotationfile import AnnotationFile, AnnotationRecord
    
    input_fh = open( input_file )
    annotation_fh = AnnotationFile( output_file, "w" )
    
    for input_line in input_fh:
        if input_line.startswith("#"):
            continue
        
        values=input_line.rstrip().split("\t")
        if len(values) > 7 :
            annotation = AnnotationRecord()
            # Sequence ID
            annotation.seq_id = values[0]
            annotation.source = values[1]
            annotation.type = values[2]
            annotation.start = values[3]
            annotation.end = values[4]
            annotation.score = values[5]
            annotation.strand = values[6]
            annotation.attributes = dict()
            annotation.setAttribute("Name", values[7])
            
                                # Delete unused attributes
            if annotation.attributes.has_key("md5"):
                del annotation.attributes["md5"]
            if annotation.attributes.has_key("Target"):
                del annotation.attributes["Target"]
            if annotation.attributes.has_key("date"):
                del annotation.attributes["date"]
            if annotation.attributes.has_key("ID"):
                del annotation.attributes["ID"]
            if annotation.attributes.has_key("status"):
                del annotation.attributes["status"]
            # Write annotations
            annotation_fh.write( annotation )

    input_fh.close()
    annotation_fh.close()

class RNAmmer (Component):
    """
     @summary : Launch RNAmmer on fasta file.
    """

    def define_parameters( self, sequences_files, kingdom="eukaryota"):
        """
         @param sequences_files : [list] Sequences files to process.
         @param kingdom : [str] Super Kingdom of species [archaea|bacteria|eukaryota].
        """
        # Parameters
        self.add_parameter( "kingdom", "Super Kingdom of species [archaea|bacteria|eukaryota].", default=kingdom )

        #     Options
        self.options = " -m lsu,ssu,tsu" # No options if eukaryota
        if (kingdom == "archaea") :
            self.options += " -S arc"
        elif (kingdom == "bacteria") :
            self.options += " -S bac"
        elif (kingdom == "eukaryota") :
            self.options = " -S euk"
        
        # Files
        self.add_input_file_list( "sequences_files", "The sequences files.", default=sequences_files, file_format="fasta", required=True )
        self.add_output_file_list( "rnammer_files", "rnammer output files.", pattern='{basename_woext}.gff1', items=sequences_files )
        self.add_output_file_list( "output_files", "rRNA Annotation gff files.", pattern='{basename_woext}.gff', items=sequences_files, file_format="gff" )
        self.add_output_file_list( "stderrs", "The stderr output file.", pattern='{basename_woext}.stderr', items=sequences_files)
        

    def get_version(self):
        cmd = [self.get_exec_path("rnammer"), "-v"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.split(' ')[2]

    def process(self):
        # Alignement
        self.add_shell_execution( self.get_exec_path("rnammer") + " " + self.options + " -gff $2 < $1 2>> $3 ",
                               cmd_format='{EXE} {IN} {OUT} ',
                               inputs=self.sequences_files, outputs=[self.rnammer_files,self.stderrs], map=True)
        # Conversion
        self.add_python_execution( rnammer2gff, inputs=self.rnammer_files, outputs=self.output_files, map=True)
                