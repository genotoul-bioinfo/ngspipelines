#
# Copyright (C) 2012 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from subprocess import Popen, PIPE

from jflow.component import Component


class tSNPannot (Component):
    """
     @summary : Annotates by similarities the SNPs called from RNSeq de novo data.
    """
    def define_parameters(self, variants_file, fasta_file, databank_species, databank_fasta, databank_annot, databank_variants=None, nb_annot=3, min_identity=0.90, min_conserv=0.90, min_conserv_border=2, nb_threads=1):
        """
        @param fasta_file : [str] Reference sequence where variants to annotate are identified.
        @param variants_file : [str] Variants to annotate.
        @param databank_species : [str] Reference species used to annotate.
        @param databank_fasta : [str] Reference species sequences.
        @param databank_annot : [str] Reference species gene and CDS annotation (GTF).
        @param databank_variants : [str] Reference species known variants.
        @param nb_annot : [int] Maximum number of annotations.
        @param min_identity : [float] Use only HSPs with at least this identity.
        @param min_conserv : [float] Use only HSPs with at least this conservation.
        @param min_conserv_border : [int] Use only HSPs with this number of amino acids conserved before and after the amino acid containing the variant.
        @param nb_threads : [int] Number of used threads in alignment.
        """
        # Parameters
        self.add_parameter( "nb_threads", "Number of used threads in alignment.", default=nb_threads, type="int" )
        self.add_parameter( "nb_annot", "Maximum number of annotations.", default=nb_annot, type="int" )
        self.add_parameter( "databank_species", "Reference species used to annotate.", default=databank_species )
        self.add_parameter( "min_identity", "Use only HSPs with at least this identity.", default=min_identity, type="float" )
        self.add_parameter( "min_conserv", "Use only HSPs with at least this conservation.", default=min_conserv, type="float" )
        self.add_parameter( "min_conserv_border", "Use only HSPs with this number of amino acids conserved before and after the amino acid containing the variant.", default=min_conserv_border, type="int" )
        self.options = " --nb-annot " + str(nb_annot) + " --min-identity " + str(min_identity) + " --min-conserv " + str(min_conserv) + " --min-conserv-border " + str(min_conserv_border)
        
        # Files
        self.add_input_file_list( "fasta_file", "Reference sequence where variants to annotate are identified.", default=fasta_file, file_format="fasta", required=True )
        self.add_input_file_list( "variants_files", "Variants to annotate.", default=variants_file, file_format="vcf", required=True )
        self.add_input_file( "databank_fasta", "Reference species sequences.", default=databank_fasta, file_format="fasta", required=True )
        self.add_input_file( "databank_annot", "Reference species gene and CDS annotation (GTF).", default=databank_annot, file_format="gtf", required=True )
        self.add_input_file( "databank_variants", "Reference species known variants.", default=databank_variants, file_format="vcf" )
        self.add_output_file_list( "carrier_file", "The new sequences file (short sequences centered on vraiants).", pattern="{basename_woext}_carrier.fa", items=fasta_file, file_format="fasta" )
        self.add_output_file_list( "blast_file", "The blast results.", pattern="{basename_woext}_blast.xml", items=fasta_file, file_format="xml" )
        self.add_output_file_list( "blast_stdout", "tSNPannotAln STDOUT.", pattern="{basename_woext}_aln.stdout", items=fasta_file )
        self.add_output_file_list( "blast_stderr", "The blast stderr output file.",  pattern="{basename_woext}_aln.stderr", items=fasta_file )
        
        self.add_output_file_list( "annotation_file", "The variants annotations.",pattern="{basename_woext}_annot.gff", items=fasta_file, file_format="gff" )
        self.add_output_file_list( "annot_stdout", "tSNPannot STDOUT.", pattern="{basename_woext}_annot.stdout", items=fasta_file )
        self.add_output_file_list( "annot_stderr", "The stderr output file.", pattern="{basename_woext}_annot.stderr", items=fasta_file )

    def get_version(self):
        cmd = [self.get_exec_path("tSNPannot.py"), "--version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout

    def process(self):

        # Alignment
        self.add_shell_execution(self.get_exec_path("tSNPannotAln.py") + " --sequences-file $1 --variants-file $2" +
                             " --aln-db " + self.databank_fasta + " -b " + self.get_exec_path("blastx") + " -t " + str(self.nb_threads) + " -q $3 -x $4 > $5 2>> $6", 
                             cmd_format='{EXE} {IN} {OUT} ', 
                             inputs=[self.fasta_file, self.variants_files], 
                             outputs=[self.carrier_file, self.blast_file, self.blast_stdout,self.blast_stderr], includes=self.databank_fasta,
                             map=True )

        # Annotation
        included_files = [self.databank_fasta, self.databank_annot]
        if self.databank_variants != None:
            included_files.append( self.databank_variants )
        self.add_shell_execution(self.get_exec_path("tSNPannot.py") + self.options +
                               " --sequences-file $1 --variants-file $2 --output $4 " +
                               "--annot-db species='" + self.databank_species + "' aln=$3 gtf=" + self.databank_annot + (" vcf=" + self.databank_variants if self.databank_variants != None else "" ) +
                               " > $5 2>> $6", cmd_format='{EXE} {IN} {OUT}', 
                               inputs=[self.carrier_file, self.variants_files, self.blast_file], 
                               outputs=[self.annotation_file, self.annot_stdout,self.annot_stderr], 
                               includes=included_files , map=True)
