#
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import pickle

from jflow.component import Component

from weaver.function import PythonFunction
from weaver.abstraction import Map
from ngspipelines.utils import Utils

def best_hit( databanks_weights, parameters , *outputs ):
    """
    @summary : Find the best hit for each query in annotations files. The comparison is based on anotation.innerscore().
    @param databanks_weights : {"swissprot" : 0.5, "refseq" : 0.2, "ensembl" : 0.6 }
    @param parameters : [str] The path to the dump file. The dump must contain a dict with inputs list,
                                best output, std outputs list, databanks weights list. Example :
                                {
                                    "inputs" : [ "/tmp/queries_split1.swissprot.gff",
                                                 "/tmp/queries_split1.refseq.gff",
                                                 "/tmp/queries_split1.ensembl.gff" ]
                                    "outputs" : [ "/tmp/queries_split1.swissprot.std_annot.gff",
                                                      "/tmp/queries_split1.refseq.std_annot.gff",
                                                      "/tmp/queries_split1.ensembl.std_annot.gff" ]
                                    "best" : "/tmp/queries_split1.best_annot.gff"
                                }
    """
    import re
    from workflows.rnaseqdenovo.lib.annotationfile import AnnotationFile, AnnotationRecord
    best_annot = dict()
    

    # Retrieve parameters
    annotation_files = parameters["inputs"]
    best_annotations = parameters["best"]
    non_best_annotations = parameters["outputs"]

    # Process
    out_annot_fh = list()
    prog = re.compile('.*\.(.*)\.gff')
    for input_idx, current_annot_file in enumerate(annotation_files):
        #extract db base name to get the weight
        databank_basename=os.path.basename(current_annot_file).replace(parameters["prefix"]+".","").replace(".gff","")
        annotations = AnnotationFile(current_annot_file, "r")
        out_annot_fh.append( AnnotationFile(non_best_annotations[input_idx], "w") )
        for current_annot in annotations:
            current_annot.addToAttribute( "databank_weight", str(databanks_weights[databank_basename]) )
            # The query already has a best HSP
            if best_annot.has_key( current_annot.seq_id ):
                if best_annot[current_annot.seq_id]["annot"].innerScore() < current_annot.innerScore():
                    # Write old best HSP
                    out_annot_fh[best_annot[current_annot.seq_id]["file_index"]].write( best_annot[current_annot.seq_id]["annot"] )
                    # Replace old best HSP
                    best_annot[current_annot.seq_id]["annot"] = current_annot
                    best_annot[current_annot.seq_id]["file_index"] = input_idx
                else:
                    out_annot_fh[input_idx].write( current_annot )
            # The query does not have a best HSP
            else :
                best_annot[current_annot.seq_id] = dict()
                best_annot[current_annot.seq_id]["annot"] = current_annot
                best_annot[current_annot.seq_id]["file_index"] = input_idx

    for current_fh in out_annot_fh:
        current_fh.close()

    # Write best annotation file
    best_annot_fh = AnnotationFile( best_annotations, "w" )
    for seq_id in best_annot.keys():
        best_annot_fh.write( best_annot[seq_id]["annot"] )
    best_annot_fh.close()
    

class BestAnnotSearch (Component):
    """
     @summary : Extract in separated file(s) the best annotation and the others for each sequence.
                 The comparison is based on anotation.innerscore(databank_weight).
    """

    def define_parameters(self, contigs_files, input_files, databanks_weights):
        """
        @param input_files : [[][]] The annotations files grouped by contigs files.
        @param databanks_weights : [list] The list of the databanks weights.
        """
        # Parameters
        self.add_input_object( "databanks_weights", "The weight of each databanks in the annotation score. calculation : score = old_score + weight * old_score.", default=databanks_weights, required=True )
        # Files
        self.add_input_file_list( "input_files", "The annotations files grouped by contigs files.\
            Example with one sequences file annotated by several databanks : [files:[annot_swissprot, annot_refseq], weight:[0.5,0.1]]. \
            Example with two sequences files annotated by several databanks (databanks in same order) : [[annot_split1_swissprot, annot_split1_refseq], [annot_split2_swissprot, annot_split2_refseq]]", 
            default=input_files, required=True )
        
        # group input by splited input file (contigs)
        groups_path_input = Utils.get_filepath_by_prefix(self.input_files)
        
        # group output by splited input file (contigs)
        self.add_output_file_list( "std_annotations", "The annotations files without the best annotation.", 
                                   pattern="{basename_woext}.std_annot.gff", items=input_files, file_format="gff" )
        groups_path_output = Utils.get_filepath_by_prefix(self.std_annotations)
        
        # get best output by prefix
        self.add_output_file_list( "best_annotations", "the annotations file with only the best annotation of each sequences.", 
                                   pattern="{basename_woext}.best.gff", items=contigs_files, file_format="gff" )
        groups_path_best = Utils.get_filepath_by_prefix(self.best_annotations)
        
        tab_element=[]
        for prefix in groups_path_input.keys():
            tab_element.append({
                               "prefix": prefix,
                               "inputs": groups_path_input[prefix],
                               "outputs": groups_path_output[prefix],
                               "best": groups_path_best[prefix][0]
                               })
        self.add_input_object_list( "input_elements", "", default=tab_element,required=True )
        
                

    def process(self):
        
        for i_elmt in self.input_elements:
            self.add_python_execution(best_hit, inputs=[self.databanks_weights, i_elmt], 
                                      outputs=[i_elmt["best"]]+i_elmt["outputs"], includes=i_elmt["inputs"])
            
            