#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component

class GatkRecalibration (Component):
    """
     @summary : Reprocess quality score for each nucleotids in reads.
    """

    def define_parameters(self, input_files, reference, vcf=None, memory=2):
        """
          @param input_files : [list] bam files.
          @param vcf : [str] vcf of known site
          @param xmx : [int] memory for jvm in Giga
        """
        # Parameters
        self.add_parameter( "memory", "The memory size to allocate (in Giga).", default=memory, type="int" )

        # Files
        self.add_input_file_list( "input_files", "The BAM files.", default=input_files, file_format="bam", required=True )
        self.add_input_file( "reference", "The reference file.", default=reference, file_format="fasta", required=True )
        self.add_input_file( "known_sites", "The VCF of known sites.", default=vcf, file_format="vcf" )

        self.add_output_file_list( "recab_files", "Recalibration table files. Content : several covariate values, num observations, num mismatches and empirical quality score.", pattern="{basename_woext}_recalib.grp", items=input_files )
        self.add_output_file_list( "recab_stderrs", "Error file for recalibration calculation step.", pattern="{basename_woext}_recalib.stderr", items=input_files )

        self.add_output_file_list( "output_files", "Recalibrated bams.", pattern="{basename_woext}.bam", items=input_files, file_format="bam" )
        self.add_output_file_list( "print_reads_stderrs", "Error file for recalibration step.", pattern="{basename_woext}_print_reads.stderr", items=input_files )

    def process(self):
        xmx_option = "-Xmx" + str(self.memory) + "g"

        # Recalibration : step 1 : base recalibration
        dbsnp_options =  " --run_without_dbsnp_potentially_ruining_quality " if self.known_sites == None else " -knownSites " + self.known_sites
        includes=[self.reference] 
        if self.known_sites != None:
            includes.append(self.known_sites) 
        self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T BaseRecalibrator -S SILENT --allow_potentially_misencoded_quality_scores"\
                                     " -I $1 -R " + self.reference + dbsnp_options + " -o $2 2>> $3",  cmd_format='{EXE} {IN} {OUT}',
                                     inputs=[self.input_files], outputs=[self.recab_files , self.recab_stderrs], 
                                     includes=includes, map=True)    

        # Recalibration : step 2 : print reads
        self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T PrintReads --allow_potentially_misencoded_quality_scores "\
                                     " -I $1 -R "+ self.reference + " -BQSR $2 -o $3 2>> $4", cmd_format='{EXE} {IN} {OUT}' ,
                                     inputs=[self.input_files, self.recab_files], outputs=[self.output_files , self.print_reads_stderrs], 
                                     includes=[self.reference], map=True)
       