#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component


def add_id( input_vcf, output_vcf ):
    """
    @summary : Add id to each variant without ID. ID = CHROM_POS_VARIANT_TYPE
    @param input_vcf : [str] The VCF to process.
    @param output_vcf : [str] The VCF with variants IDs.
    """
    import workflows.rnaseqdenovo.lib.utils as utils

    def isIndel(ref, alt):
        """
        @summary : Return True if the variant is an insertio or a deletion.
        @return : [bool]
        """
        isIndel = False
        if ref == "." or ref == "-":
            isIndel = True
        else:
            for allele in alt:
                if len(allele) != len(ref) or allele == "." or allele == "-":
                    isIndel = True
        return isIndel

    def type(ref, alt):
        """
        @summary : Returns the vrariant type.
        @return : [str] 'SNP' or 'INDEL' or 'VARIATION'.
        """
        record_type = "SNP"
        if isIndel(ref, alt):
            record_type = "INDEL"
        elif len(ref) > 1:
            record_type = "VARIATION"
        return record_type

    # Main
    FH_OUT = open( output_vcf, "w" )
    FH_IN = open( input_vcf )
    for line in FH_IN:
        if line.startswith('#'):
            FH_OUT.write(line)
        else:
            fields = line.split()
            new_line = line
            if fields[2] == "." or fields[2] == "-":
                old = "\t".join(fields[0:3])
                type = utils.get_variant_type(fields[3], fields[4].split(','))
                variant_id = utils.get_variant_name(fields[0], str(fields[1]), type)
                new = "\t".join(fields[0:2]) + "\t" + variant_id
                new_line = line.replace( old, new )
            FH_OUT.write(new_line)
    FH_OUT.close()
    FH_IN.close()


class GatkVariantFilter (Component):
    """
     @summary : Filter the variants based on differents criteria.
    """

    def define_parameters( self, variant, reference,
                             snp_filter_options='-window 18 -cluster 3 -filterName FS --filterExpression \"FS > 30.0\"',
                             indel_filter_options='-window 18 -cluster 3 -filterName FS --filterExpression \"FS > 30.0\"',
                             memory=2 ):
        """
          @param variant : [file] vcf files to filter.
          @param reference : [file] fasta reference file.
          @param snp_filter_options : [string] iOptions used in SNP filter.
          @param indel_filter_options : [string] Options used in indel filter.
          @param memory : [int] memory for jvm in Giga.
        """
        # Parameters
        self.add_parameter( "memory", "The memory size to allocate (in Giga).", default=memory, type="int" )
        self.add_parameter( "snp_filter_options", "Options used in SNP filter.", default=snp_filter_options )
        self.add_parameter( "indel_filter_options", "Options used in indel filter.", default=indel_filter_options )

        # Files
        self.add_input_file( "variant", "The variants file.", default=variant, file_format="vcf", required=True )
        self.add_input_file( "reference", "The FASTA reference file", default=reference, file_format="fasta", required=True )
        #     Filter SNP
        self.add_output_file( "snp_variants", "SNP variants in input file.", filename="variants_snp.vcf", file_format="vcf" )
        self.add_output_file( "snp_variants_stderr", "Error file for the SNP extraction step.", filename="variants_snp.stderr" )
        self.add_output_file( "snp_tagged", "SNP variants with setted tag 'FILTER'.", filename="variants_snp_tagged.vcf", file_format="vcf" )
        self.add_output_file( "snp_tagged_stderr", "Error file for the SNP filter evaluation step.", filename="variants_snp_tagged.stderr" )
        #     Filter indels
        self.add_output_file( "indel_variants", "InDel variants in input file.", filename="variants_indel.vcf", file_format="vcf" )
        self.add_output_file( "indel_variants_stderr", "Error file for the InDel extraction step.", filename="variants_indel.stderr" )
        self.add_output_file( "indel_tagged", "InDel variants with setted tag 'FILTER'.", filename="variants_indel_tagged.vcf", file_format="vcf" )
        self.add_output_file( "indel_tagged_stderr", "Error file for the InDel filter evaluation step.", filename="variants_indel_tagged.stderr" )
        #     All type tagged variants
        self.add_output_file( "tmp_file", "All variants with setted tag 'FILTER'.", filename="variants_tmp.vcf", file_format="vcf" )
        self.add_output_file( "tmp_file_stderr", "Error file for merge tagged SNP and tagged IndDel step.", filename="variants_tmp.stderr", file_format="vcf" )
        #    Outputs
        self.add_output_file( "output_file", "Filtered variants file.", filename="variants.vcf", file_format="vcf" )
        self.add_output_file( "output_file_with_id", "Filtered variants file.", filename="variants.id.vcf", file_format="vcf" )
        self.add_output_file( "filter_stderr", "Error file for remove filtered variants step.", filename="variants.stderr" )

    def process(self):
        xmx_option  = "-Xmx" + str(self.memory) + "g"

        # Selcet only SNP
        self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T SelectVariants "\
                                    " -R $1 -V $2 -selectType SNP -o $3 2>> $4", cmd_format='{EXE} {IN} {OUT}' ,
                                    inputs=[self.reference, self.variant], outputs=[self.snp_variants, self.snp_variants_stderr])
        # Filter SNP
        self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T VariantFiltration "\
                                    " -R $1 -V $2 "+ self.snp_filter_options + " -o $3 2>> $4", cmd_format='{EXE} {IN} {OUT}' ,
                                    inputs=[self.reference, self.snp_variants], outputs=[self.snp_tagged, self.snp_tagged_stderr] )
        # Select only indels
        self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T SelectVariants "\
                                      " -R $1 -V $2 -selectType INDEL -o $3 2>> $4", cmd_format='{EXE} {IN} {OUT}' ,
                                      inputs=[self.reference, self.variant], outputs=[self.indel_variants, self.indel_variants_stderr])
        # Filter indels
        self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T VariantFiltration "\
                                      " -R $1 -V $2 "+ self.indel_filter_options + " -o $3 2>> $4", cmd_format='{EXE} {IN} {OUT}' ,
                                      inputs=[self.reference, self.indel_variants], outputs=[self.indel_tagged, self.indel_tagged_stderr] )
        
        # Merge SNP and indels
        self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T CombineVariants "\
                                       "--assumeIdenticalSamples -R $1 --variant:snp $2 --variant:indel $3 -o $4 \
                                       -genotypeMergeOptions PRIORITIZE -priority snp,indel 2>> $5", cmd_format='{EXE} {IN} {OUT}' ,
                                       inputs=[self.reference, self.snp_tagged, self.indel_tagged], outputs=[self.tmp_file, self.tmp_file_stderr] )
        # Discard no passing filters
        self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T SelectVariants "\
                                     " -R $1 -V $2 --excludeFiltered -o $3 2>> $4", cmd_format='{EXE} {IN} {OUT}' ,
                                     inputs=[self.reference, self.tmp_file], outputs=[self.output_file, self.filter_stderr] )
        self.add_python_execution(add_id,inputs=self.output_file, outputs=self.output_file_with_id)
        