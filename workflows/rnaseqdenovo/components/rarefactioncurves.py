#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from subprocess import Popen, PIPE

from jflow.component import Component

class RarefactionCurves (Component):

    def define_parameters(self, bams, step=200000, properly_paired=True, primary_alignment=True):
        """
        @param bams : [list] list of bam files
        """
        # Files
        self.add_input_file_list("bams", "Which bam files should be used", file_format="bam", default=bams)
        self.add_output_file_list("stdouts", "The stdout output files", pattern="{basename_woext}.stdout", items=bams)
        self.add_output_file_list("stderrs", "The stderr output files", pattern="{basename_woext}.stderr", items=bams)
        self.add_parameter( "step", "Number of reads per step.", default=step, type="int" )
        self.add_output_file("rarefaction_values", "The output file", filename='rarefaction_values.csv')
        self.option = "-s " + str(self.step)
            
        # List of library name based on basename of bam file
        self.names_param = ""
        tmp = []
        for i in bams:
            tmp.append(os.path.splitext(os.path.basename(i))[0])
        self.names_param = ",".join(tmp)

    def process(self):
        
        for idx in range( len(self.bams) ):
            # Discard 'supplementary alignment' and 'not primary alignment'
            self.add_shell_execution(self.get_exec_path("python")+" " +self.get_exec_path("rarefaction_curve.py") + " " +\
                                     self.option + " -i $1 -o $2 2>> $3", cmd_format = '{EXE} {IN} {OUT}',
                                     inputs=self.bams[idx], outputs=[self.stdouts[idx],self.stderrs[idx]] )

        self.add_shell_execution(self.get_exec_path("python")+" " + self.get_exec_path("merge_cols.py") +" --files "+\
                                 ",".join(self.stdouts)+" --name "+self.names_param+" --output $1" , 
                                 cmd_format = '{EXE} {OUT}',
                                 outputs=self.rarefaction_values, includes=self.stdouts)
        

    def get_version(self):
        return "-"