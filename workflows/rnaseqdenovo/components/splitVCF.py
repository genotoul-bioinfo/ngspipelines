#
# Copyright (C) 2012 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from subprocess import Popen, PIPE

from jflow.component import Component


class SplitVcf (Component):
    """
     @summary : Split VCF as fasta are splitted.
    """
    def define_parameters(self, variants_file, fasta_files):
        """
        @param fasta_file : [str] Reference sequence where variants to annotate are identified.
        @param variants_file : [str] Variants to annotate.
        """
        
        # Files
        self.add_input_file_list( "fasta_files", "Reference sequence where variants to annotate are identified.", default=fasta_files, file_format="fasta", required=True )
        self.add_input_file( "variants_file", "Variants to annotate.", default=variants_file, file_format="vcf", required=True )
        self.add_output_file_list( "splitted_variant_files", "Variant splitted.", pattern="{basename_woext}.vcf", items=fasta_files, file_format="vcf" )

    def get_version(self):
        return ""

    def process(self):
        self.add_shell_execution(self.get_exec_path("vcftools")+ " `grep \">\" $1 | cut -f 1 -d \" \" | sed \"s/^>/ --chr /\" | paste -s -d \"\"`" + " --vcf "+ self.variants_file +" --recode --out $2; mv $2.recode.vcf $2", 
                                 cmd_format='{EXE} {IN} {OUT} ',
                                 inputs=self.fasta_files, outputs=self.splitted_variant_files, includes=self.variants_file,
                                 map=True )

        