#
# Copyright (C) 2012 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from subprocess import Popen, PIPE

from jflow.component import Component




class Transdecoder (Component):
    """
     @summary : Annotates by similarities the SNPs called from RNSeq de novo data.
    """
    def define_parameters(self, fasta_file):
        """
        @param fasta_file : [str] Reference sequence where variants to annotate are identified.
        @param variants_file : [str] Variants to annotate.
        """
        
        
        self.add_input_file( "fasta_file", "Reference sequence where variants to annotate are identified.", default=fasta_file, file_format="fasta", required=True )
        self.add_output_file( "databank", "The databank : fasta file linked to the dictionary and the index file.", filename=os.path.basename(fasta_file), file_format="fasta" )
        self.transdecoder_long = os.path.join(self.output_directory,os.path.basename(fasta_file)+'.transdecoder_dir','longest_orfs.gff3')

        self.add_output_file( "output_file", "Predictions of transdecoder.", filename=os.path.basename(fasta_file)+'.transdecoder.gff3')
        self.add_output_file( "transdecoder_longorfs_stderr", "The stderr transdecoder_longorfs output file.", filename="transdecoder_longorfs.stderr" )
        self.add_output_file( "transdecoder_longorfs_stdout", "The stdout transdecoder_longorfs output file.", filename="transdecoder_longorfs.stdout" )        
        self.add_output_file( "transdecoder_predict_stderr", "The stderr transdecoder_predict output file.", filename="transdecoder_predict.stderr" )
        self.add_output_file( "transdecoder_predict_stdout", "The stdout transdecoder_predict output file.", filename="transdecoder_predict.stdout" )
        
    def get_version(self):
        return ""

    def process(self):
        self.add_shell_execution("ln -s $1 $2; cd "+self.output_directory+ "; " + 
                                 self.get_exec_path("transdecoder_longorfs")+" -t $2 >$3 2>>$4 " , 
                                 cmd_format='{EXE} {IN} {OUT}', inputs=self.fasta_file, outputs=[self.databank,self.transdecoder_longorfs_stdout,self.transdecoder_longorfs_stderr, self.transdecoder_long])
        
        self.add_shell_execution("cd " + self.output_directory+ "; " + self.get_exec_path("transdecoder_predict")+" -t $1 > $2 2>> $3; sed -i \"/^$/d\" $4", 
                                 cmd_format='{EXE} {IN} {OUT}', inputs=self.databank, outputs=[self.transdecoder_predict_stdout,self.transdecoder_predict_stderr,self.output_file], includes=self.transdecoder_long)
        