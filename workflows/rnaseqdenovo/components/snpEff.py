#
# Copyright (C) 2012 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from subprocess import Popen, PIPE

from jflow.component import Component

class SnpEff (Component):
    """
     @summary : Annotates by similarities the SNPs called from RNSeq de novo data.
    """
    def define_parameters(self, fasta_file, gff, variants_files, memory=2 ):
        """
        @param fasta_file : [str] Reference sequence where variants to annotate are identified.
        @param variants_files : [str] Variants to annotate.
        """
        
        self.add_input_file( "fasta_file", "Reference sequence where variants to annotate are identified.", default=fasta_file, file_format="fasta", required=True )
        self.add_input_file( "transdecoder_predict", "Predictions of transdecoder.", default=gff,  required=True  )
        
        self.gene_file=os.path.join(self.output_directory,"data","transcript",'genes.gff');
        self.snpeff_db=os.path.join(self.output_directory,"data","transcript",'snpEffectPredictor.bin');
        
        self.add_input_file_list( "variants_files", "Variants to annotate.", default=variants_files, file_format="vcf", required=True )
        self.add_output_file_list( "snpeff_outputs", "The output file.", pattern="{basename_woext}_eff.vcf", items=variants_files)
        self.add_output_file_list( "snpeff_stderr", "The stderr output file.", pattern="{basename_woext}_eff.stderr", items=variants_files )
        self.add_output_file( "snpeff_config", "The config file of snpEff.", filename="snpEff.config" )
        self.add_output_file( "snpeff_build_stderr", "The stderr build snpeff output file.", filename="snpeff_build.stderr" )
        self.add_output_file( "snpeff_build_stdout", "The stdout build snpeff output file.", filename="snpeff_build.stdout" )
        self.add_output_file( "output_file", "Variants annotated.", filename="variant.snpeff.vcf"  )
        self.add_output_file( "stderr", "Error gatk combine variant.", filename="concat_variant.stderr"  )
        self.add_parameter( "memory", "The memory size to allocate (in Giga).", default=memory, type="int" )
        
    def get_version(self):
        cmd = [self.get_exec_path("java"), "-Xmx1g", "-jar" , self.get_exec_path("snpeff"), "-version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.rstrip('\n')

    def process(self):
        xmx_option  = "-Xmx" + str(self.memory) + "g"
        
        self.add_shell_execution("cp  "+ self.get_resource("snpeff_config") + " " + self.output_directory+"; echo \"transcript.genome: transcript\" >>$1; mkdir -p "+self.output_directory+"/data/transcript",
                                 cmd_format='{EXE} {IN} {OUT}', outputs=self.snpeff_config)
        
        #Add fasta sequence in gff3 file
        self.add_shell_execution("sed '/^$/d' $2 > $3; echo \"##FASTA\" >> $3 ; cat $1 >> $3 ", cmd_format='{EXE} {IN} {OUT}', inputs=[self.fasta_file,self.transdecoder_predict], outputs=[self.gene_file], includes=self.snpeff_config)
        
        #build database for snpeff ava -jar /usr/local/bioinfo/src/SnpEff/current/snpEff.jar build -gff3 -c snpEff.config -v transcript
        self.add_shell_execution("cd " + self.output_directory+ "; " + self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("snpeff") +
                                  " build -gff3 -c $1 -v transcript >$2 2>> $3", 
                                 cmd_format='{EXE} {IN} {OUT}', inputs=[self.snpeff_config], outputs=[self.snpeff_build_stdout,self.snpeff_build_stderr,self.snpeff_db], includes=self.gene_file )
        
        #Execute snpEff
        self.add_shell_execution("cd "+self.output_directory+ "; " + self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("snpeff") + " eff -c " + self.snpeff_config+ " transcript $1 > $2 2> $3", 
                                 cmd_format='{EXE} {IN} {OUT}', inputs=[self.variants_files], 
                                 outputs=[self.snpeff_outputs,self.snpeff_stderr], includes=[self.snpeff_db,self.snpeff_config],
                                 map=True)
        
        self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T CombineVariants "\
                               + " --genotypemergeoption UNSORTED -R $1 --variant " + " --variant ".join(self.snpeff_outputs)  + " -o $2 2>> $3" , cmd_format='{EXE} {IN} {OUT}' ,
                               inputs=self.fasta_file, outputs=[self.output_file, self.stderr], includes=self.snpeff_outputs)
        
        