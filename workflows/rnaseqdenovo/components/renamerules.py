#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, pickle

from jflow.component import Component

from weaver.function import PythonFunction


def renames_with_annot( inputs, rename_file, prefix):
    """
     @summary : Writes the new name of each annotation (the new name is built from the subject name).
                For each sequence with same subject name, an unique number is added to the new name : 
                <subject_name>.<index>.<same_subject_name_number>.
      @param sequences_files_dump : [str] path to the dump which contains the list of sequences files to process.
      @param annot_files_dump : [str] path to the dump which contains the list of the AnnotationFiles. This file must contains only one annotation by sequence.
      @param rename_file : [str] the output file. It contains correspondence between old and new name for each sequence with an annotation. 
                           Syntax of each line : old_name<tab>new_name.
    """
    
    from workflows.rnaseqdenovo.lib.annotationfile import AnnotationFile, AnnotationRecord
    import jflow.seqio as seqio
    dict_gene_contig_id = dict()
    dict_contig_id_gene = dict()

    for annot_file in inputs["annotations"] :
        annotations = AnnotationFile(annot_file, "r")
        for record in annotations:
            if record.attributes.has_key( "gene" ):
                clean_gene = ""
                gene_name = record.attributesToStr("gene")
                for char in gene_name.upper():
                    if char.isalnum() or char in ['.', '^', '*', '$', '@', '!', '+', '_', '?', '-', '|']: # GFF3 seqid specifications
                        clean_gene += char
                dict_contig_id_gene[record.seq_id] = clean_gene
                if dict_gene_contig_id.has_key(clean_gene) :
                    dict_gene_contig_id[clean_gene].append( record.seq_id )
                else:
                    dict_gene_contig_id[clean_gene] = [record.seq_id]
        annotations.close()

    # Write new names for all the sequences
    out_fh = open( rename_file, "w" )
    contig_no_name_cpt=1
    
    for sequences_file in inputs["sequences"] :
        reader = seqio.SequenceReader( sequences_file )
        for id, desc, seq, qual in reader:
            new_name = ""
            if prefix :
                new_name = prefix
            if dict_contig_id_gene.has_key(id):
                gene = dict_contig_id_gene[id]
                if len(dict_gene_contig_id[gene]) == 1:
                    new_name += gene
                # If several sequences will have this new name
                else:
                    new_name += gene + "." + str(dict_gene_contig_id[gene].index(id)+1) + "." + str(len(dict_gene_contig_id[gene]))
            else :
                new_name += "contig_%05d" % (contig_no_name_cpt,)
                contig_no_name_cpt += 1
            out_fh.write( id + "\t" + new_name + "\n" )
        
    out_fh.close()

def names_with_prefix( inputs, rename_file , prefix ):
    """
     @summary : Writes the new name of each sequence (the new name is built from the old name and a prefix).
      @param prefix : the prefix used.
      @param sequences_files_dump : [str] path to the dump which contains the list of sequences files to process.
      @param rename_file : [str] this file contains correspondence between old and new name for each sequence. 
                           Syntax of each line : old_name<tab>new_name.
    """
    
    import jflow.seqio as seqio
    # Write output
    fh_rename = open( rename_file, "w" )
    for sequences_file in inputs["sequences"] :
        reader = seqio.SequenceReader( sequences_file )
        for id, desc, seq, qual in reader:
            fh_rename.write( id + "\t" + str(prefix) + id + "\n" )
    fh_rename.close()

class RenameRules (Component):
    """
     @summary : Writes tab file with list of renames.
                Syntax of each line : old_name<tab>new_name.
    """

    def define_parameters(self, contigs_files, best_annotations, rename_with_annot, prefix=None):
        """
         @param contigs_files : [list] the files paths to the contigs files.
         @param best_annotations : [list] the files paths to the bests annotations files.
         @param rename_with_annot : [bool] true if best_annotations files are used to built the new names (use the gene name).
         @param prefix : [str] the prefix to add at each sequences's ID.
        """
        # Parameters
        self.add_parameter( "rename_with_annot", "True if best_annotations files are used to built the new names (use the gene name).", default=rename_with_annot, required=True )
        self.add_parameter( "prefix", "The prefix to add at each sequences's ID.", default=prefix )

        # Files
        self.add_input_file_list( "contigs", "The contigs files.", default=contigs_files, required=True )
        self.add_input_file_list( "best_annotations", "The best annotations files.", default=best_annotations, file_format="gff", required=True )
        tab = {"sequences":self.contigs,"annotations":self.best_annotations}
        self.add_input_object( "inputs", "Object with all inputs", default=tab)
        self.add_output_file( "output_file", "The rename rules in tabulated file (old_name<tab>new_name).", filename="rename_rules_tab.tsv", file_format="tsv" )
        

    def process(self):
        # Rename with the bests annotations
        print self.rename_with_annot
        if self.rename_with_annot == True:
            self.add_python_execution(renames_with_annot,
                                      cmd_format="{EXE} {IN} {OUT} '" + self.prefix + "'",
                                    inputs=self.inputs, outputs=self.output_file, includes=self.best_annotations)
        # Only add prefix
        elif self.prefix != None:
            # Process prefix
            self.add_python_execution(names_with_prefix,
                                      cmd_format="{EXE} {IN} {OUT} '" + self.prefix + "'",
                                    inputs=self.inputs, outputs=self.output_file, includes=self.best_annotations)
        
