#
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from subprocess import Popen, PIPE

from jflow.component import Component


class Diamond (Component):
    """
     @summary : Launch Diamond on protein databank, filter results and convert to annotation file.
    """

    def define_parameters( self, sequences_files, databank_file, databank,
                             evalue="1e-5", max_hit=20, min_identity=0.1, min_coverage=0.1, accession_to_gi_file=None, 
                             gi_to_gene_file=None , cpu=2):
        """
         @param sequences_files : [list] Sequences files to process.
         @param databank_file : [str] Databank path.
         @param databank : [dict] Information about databank (name and type are mandatory ; species is optional).
                            Example : {'name':"Ensembl_Danio_rerio", 'type':"protein", 'species':"Danio rerio"}
         
         @param evalue : [str] Max evalue.
         @param max_hit : [int] Max number of hits.
         @param min_identity : [float] The minimum identity value to keep an HSP.
         @param min_coverage : [float] The minimum coverage value to keep an HSP.
         @param accession_to_gi_file : [str] Path to NCBI's file who contains link between accession number and gene ID.
         @param gi_to_gene_file : [str] Path to NCBI's file who contains link between gene name and gene ID.
        """
        # Parameters
        self.add_parameter( "evalue", "Max evalue.", default=evalue )
        self.add_parameter( "max_hit", "Max number of hits.", default=max_hit, type="int" )
        self.add_parameter( "min_identity", "The minimum identity value to keep an HSP.", default=min_identity, type="float" )
        self.add_parameter( "min_coverage", "The minimum coverage value to keep an HSP.", default=min_coverage, type="float" )
        self.add_parameter( "databank_name", "The name of the databank.", default=databank["name"] )
        self.add_parameter( "databank_type", "The type of the databank.", default=databank["type"], choices=["genome", "nucleic", "protein", "transcript", "unknown"] )
        self.add_parameter( "databank_species", "The species of the databank.", default=databank["species"] )
        #     Options
        self.options = " -e " + str(evalue) + " -k " + str(max_hit) + " --outfmt 5 -p " + str(cpu)
        self.filter_options = " --min-identity " + str(min_identity) + " --min-coverage " + str(min_coverage)
        self.conversion_options = " -n " + databank["name"] + " -t " + databank["type"]
        if accession_to_gi_file != None:
            self.conversion_options += " -a " + accession_to_gi_file
        if gi_to_gene_file != None:
            self.conversion_options += " -g " + gi_to_gene_file
        if databank["species"] != None:
            self.conversion_options += " -s " + databank["species"]

        # Files
        self.add_input_file( "databank", "The databank path.", default=databank_file, required=True )
        self.add_input_file_list( "sequences_files", "The sequences files to align.", default=sequences_files, file_format="fasta", required=True )
        self.add_input_file( "accession_to_gi_file", "NCBI's file who contains link between accession number and gene ID.", default=accession_to_gi_file )
        self.add_input_file( "gi_to_gene_file", "NCBI's file who contains link between gene name and gene ID.", default=gi_to_gene_file )
        self.add_output_file_list( "xml_files", "Alignment files.", pattern='{basename_woext}.' + os.path.basename(databank_file) + '_diamond.xml', items=sequences_files, file_format="xml" )
        self.add_output_file_list( "output_files", "Annotation files.", pattern='{basename_woext}.' + os.path.basename(databank_file) + '.gff', items=sequences_files, file_format="gff" )
        self.add_output_file( "stderr", "The stderr output file.", filename="diamond.stderr" )

    def get_version(self):
        
        cmd = [self.get_exec_path("diamond"), "version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.split()[2]

    def process(self):
        # Alignement
        self.add_shell_execution(self.get_exec_path("diamond") + " blastx " + self.options + " --salltitles --query $1 -o $2 -d " + self.databank, cmd_format='{EXE} {IN} {OUT} 2>> ' + self.stderr ,
                                 inputs=self.sequences_files, outputs=self.xml_files, includes=self.databank,
                                 map=True)
        # Filter and conversion
        self.add_shell_execution(self.get_exec_path("blast2annot.py") + " " + self.filter_options + " " + self.conversion_options + " -i $1 -o $2 ", cmd_format='{EXE} {IN} {OUT} 2>> ' + self.stderr,
                                 inputs=self.xml_files, outputs=self.output_files,
                                 map=True)

        