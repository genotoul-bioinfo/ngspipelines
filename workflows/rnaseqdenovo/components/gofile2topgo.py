#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component
from weaver.function import PythonFunction

def go2topgo( input, output):
    """
     @summary:  Convert a GO file to a topGO file.
    """
    import re
    go_fh = open(input)
    topgo={}
    for line in go_fh:
        line = line.strip()
        tab = line.split("\t")
        if len(tab) > 1 :
            if topgo.has_key(tab[0]):
                topgo[tab[0]].append(tab[1])
            else :
                topgo[tab[0]]=[tab[1]]
        elif not topgo.has_key(tab[0]): 
            topgo[tab[0]]=[]
    go_fh.close()
        
    # Write new file
    out_fh = open( output, "w" )
    for key in topgo.keys():
        out_fh.write(key+"\t"+", ".join(topgo[key])+"\n")
    out_fh.close()
    


class GO2topGO (Component):
    """
     @summary: Convert a GO file to a topGO file.
    """

    def define_parameters(self, go_file):
        """
         @param go_file : GO file to convert.
        """
        # Files
        self.add_input_file( "go_file", "The file to convert.", default=go_file, required=True )
        self.add_output_file( "output", "The output file.", filename="topGO.tsv" )
        self.add_output_file( "stderr", "The stderr output file.", filename="topGO.stderr" )

    def process(self):
        goconv = PythonFunction(go2topgo, cmd_format='{EXE} {IN} {OUT} 2>> ' + self.stderr)
        goconv(inputs=self.go_file, outputs=self.output)