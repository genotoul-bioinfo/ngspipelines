#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from subprocess import Popen, PIPE

from jflow.component import Component


class CountReads (Component):

    def define_parameters(self, bams):
        """
        @param bams : [list] list of bam files
        """
        # Files
        self.add_input_file_list("bams", "Which bam files should be used", file_format="bam", default=bams, required=True)
        self.add_output_file_list("stdouts", "The stdout output files", pattern="{basename_woext}.stdout", items=bams)
        self.add_output_file_list("stderrs", "The stderr output files", pattern="{basename_woext}.stderr", items=bams)
        self.add_output_file("matrix", "The matrix output file", filename='count.csv')

        # List of library name based on basename of bam file
        self.names_param = ""
        tmp = []
        for i in bams:
            tmp.append(os.path.splitext(os.path.basename(i))[0])
        self.names_param = ",".join(tmp)

    def process(self):
        filtered_bams = self.get_outputs('{basename_woext}_filtered.bam', self.bams)
        bams_indexes = self.get_outputs('{basename_woext}_filtered.bam.bai', self.bams)

        for idx in range( len(self.bams) ):
            # Discard 'supplementary alignment' and 'not primary alignment'
            self.add_shell_execution(self.get_exec_path("samtools") + " view -bhF 2304 $1 2>> " + self.stderrs[idx] + " | " + \
                                    self.get_exec_path("samtools") + " sort - ${2%%.bam} 2>> " + self.stderrs[idx], 
                                    cmd_format = '{EXE} {IN} {OUT}',
                                    inputs=self.bams[idx], outputs=filtered_bams[idx] )

            # Index filtered bam
            self.add_shell_execution( self.get_exec_path("samtools") + " index $1 2>> " + self.stderrs[idx], 
                                      cmd_format='{EXE} {IN} {OUT}',
                                      inputs=filtered_bams[idx], outputs=bams_indexes[idx] )

            # Make stats
            self.add_shell_execution( self.get_exec_path("samtools") + " idxstats $1 | cut -f 1,3 > $2 2>> " + self.stderrs[idx], 
                                      cmd_format = '{EXE} {IN} {OUT}',  
                                      inputs=filtered_bams[idx], outputs=self.stdouts[idx], includes=bams_indexes[idx])

        self.add_shell_execution( " python " + self.get_exec_path("merge_cols.py") +" --files "+\
                                 ",".join(self.stdouts)+" --name "+self.names_param+" --output $1" \
                                 , cmd_format = '{EXE} {OUT}',outputs=self.matrix, includes=self.stdouts)

    def get_version(self):
        cmd = [self.get_exec_path("samtools")]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stderr.split()[10]