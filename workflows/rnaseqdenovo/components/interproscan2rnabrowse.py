#
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.utils import get_argument_pattern
from jflow.component import Component
from jflow.abstraction import MultiMap

from weaver.function import PythonFunction
from weaver.abstraction import Map

def interpro_2_go( interproscan_file, obo_file, go_file ):
    """
     @summary : Writes GO file from InterProScan GFF3.
      @param interproscan_file : [str] the path to the interproscan output to process.
      @param obo_file : [str] the path to the file to describe the GOs (format : OBO).
      @param go_file : [str] the path to the output file.
     @see : https://mulcyber.toulouse.inra.fr/plugins/mediawiki/wiki/ngspipelines/index.php/Go_file
    """
    import re
    from workflows.rnaseqdenovo.lib.obo_parser import GODag
    from jflow.featureio import GFF3IO
    ontology_abbreviation = { 'biological_process':'P', 'molecular_function':'F', 'cellular_component':'C' }

    # Retrieve GO information
    obo = GODag( obo_file )
    seq_annotations = dict()
    interproscan_file = GFF3IO( interproscan_file, "r" )
    for record in interproscan_file:
        seq_ids = record.seq_id.split("|") # Entries with the differents identifier and the same sequence will be merged into one on interproscan output.
        for id in seq_ids:
            id = "_".join(id.split("_")[:-1])
            # If annotation contains one or more GO
            if record.attributes.has_key("Ontology_term"):
                # New annotated sequence
                if not seq_annotations.has_key( id ):
                    seq_annotations[id] = dict()
                # Retrieve GO information
                matches = re.findall( "(GO:\d+)", record.attributes["Ontology_term"] )
                for current_GO in matches:
                    if seq_annotations[id].has_key( current_GO ):
                        if record.source not in seq_annotations[id][current_GO]["source"]:
                            seq_annotations[id][current_GO]["source"].append( record.source )
                    else:
                        seq_annotations[id][current_GO] = dict()
                        seq_annotations[id][current_GO]["name"] = obo[current_GO].name
                        seq_annotations[id][current_GO]["name_space"] = ontology_abbreviation[obo[current_GO].namespace]
                        seq_annotations[id][current_GO]["source"] = [record.source]
                        seq_annotations[id][current_GO]["has_child"] = "1" if len( obo[current_GO].children ) > 0 else "0"
    # Write GO information
    go_fh = open( go_file, "w" )
    go_fh.write( "#seqid" + "\t" + "GO_id" + "\t" + "Go_name" + "\t" + "GO_namespace" + "\t" + "Evidence" + "\t" + "Source" + "\t" + "Is_terminal_node" + "\n" )
    for seq_id in seq_annotations:
        for current_GO in seq_annotations[seq_id]:
            go_fh.write( seq_id + "\t" + current_GO + "\t" + seq_annotations[seq_id][current_GO]["name"] + "\t" + seq_annotations[seq_id][current_GO]["name_space"] + "\t" + "IEA" + "\t" + "interproscan:"+ "|".join(seq_annotations[seq_id][current_GO]["source"]) + "\t" + seq_annotations[seq_id][current_GO]["has_child"] + "\n" )     
    go_fh.close()

def interpro_2_annot( interproscan_file, orf_file, annotation_file ):
    """
     @summary : Writes an annotation file from InterProScan GFF3 on pre-processed ORFs.
      @param interproscan_file : [str] the path to the interproscan output to process.
      @param orf_file : [str] the path to the fasta file which provides the locations of the ORFs (format : getORF output fasta).
      @param annotation_file : [str] the path to the output file.
     @see : https://mulcyber.toulouse.inra.fr/plugins/mediawiki/wiki/ngspipelines/index.php/Annotation_file
    """
    import re
    import jflow.seqio as seqio
    from workflows.rnaseqdenovo.lib.annotationfile import AnnotationFile, AnnotationRecord

    aln_query_ratio = 3

    # Find ORF information
    orf_info = dict()
    reader = seqio.FastaReader( orf_file )
    for id, desc, seq, qual in reader:
        orf_info[id] = dict()
        orf_info[id]["seqid"] = "_".join(id.split("_")[:-1])
        # Find ORF strand
        match = re.search( "(REVERSE SENSE)", desc )
        orf_info[id]["strand"] = "+"
        if match is not None:
            orf_info[id]["strand"] = "-"
        # Find ORF start and end
        match = re.search( "^\[(\d+) - (\d+)\]", desc )
        if orf_info[id]["strand"] == "+":
            orf_info[id]["start"] = int(match.group(1))
            orf_info[id]["end"] = int(match.group(2))
        else:
            orf_info[id]["start"] = int(match.group(2))
            orf_info[id]["end"] = int(match.group(1))

    interpro_fh = open( interproscan_file )
    annotation_fh = AnnotationFile( annotation_file, "w" )

    for interproscan_line in interpro_fh:
        if not interproscan_line.startswith('#'):
            seq_ids = interproscan_line.split()[0].split("|") # Entries with the differents identifier and the same sequence will be merged into one on interproscan output.
            for id in seq_ids:
                annotation = AnnotationRecord.fromGff( interproscan_line )
                # Sequence ID
                annotation.seq_id = "_".join(id.split("_")[:-1])
                # Correct positions
                if annotation.type == "polypeptide" or annotation.type == "ORF":
                    annotation.strand = orf_info[id]["strand"]
                    annotation.start = orf_info[id]["start"]
                    annotation.end = orf_info[id]["end"]
                    # Store target coordinates
                    target_start = annotation.start
                    target_end = annotation.end
                    target_strand = annotation.strand
                    # Change type
                    annotation.source = "getORF"
                    annotation.type = "ORF"
                    if not annotation.attributes.has_key("desc"):
                        annotation.attributes["desc"] = "ORF"
                else:
                    if annotation.type == "protein_match":
                        if annotation.strand == "-":
                            raise "Not implemented."
                        if target_strand == "+":
                            annotation.start = target_start + ((annotation.start - 1) * aln_query_ratio)
                            annotation.end = (target_start - 1) + (annotation.end * aln_query_ratio)
                        else:
                            old_end = annotation.end
                            old_start = annotation.start
                            annotation.end = target_end - ((annotation.start -1) * aln_query_ratio)
                            annotation.start = annotation.end - ((old_end - old_start + 1) * aln_query_ratio) +1
                        annotation.strand = target_strand
                # Description
                if annotation.attributes.has_key("signature_desc"):
                    annotation.attributes["desc"] = annotation.attributes["signature_desc"]
                    del annotation.attributes["signature_desc"]
                # GO
                if annotation.attributes.has_key("Ontology_term"):
                    if annotation.attributes.has_key("desc"):
                        annotation.attributes["desc"] += " [" + annotation.attributes["Ontology_term"] + "]"
                    else:
                        annotation.setAttribute( "desc", annotation.attributes["Ontology_term"] )
                # Delete unused attributes
                if annotation.attributes.has_key("md5"):
                    del annotation.attributes["md5"]
                if annotation.attributes.has_key("Target"):
                    del annotation.attributes["Target"]
                if annotation.attributes.has_key("date"):
                    del annotation.attributes["date"]
                if annotation.attributes.has_key("ID"):
                    del annotation.attributes["ID"]
                if annotation.attributes.has_key("status"):
                    del annotation.attributes["status"]
                # Write annotations
                annotation_fh.write( annotation )

    interpro_fh.close()
    annotation_fh.close()


class InterProScan2RnaBrowse (Component):
    """
     @summary : Write an AnnotationFile and an GOFile from the GFF3 results of Interproscan.
    """

    def define_parameters(self, interproscan_files, orf_files, obo_file):
        # Files
        self.add_input_file_list( "interproscan_files", "The InterProScan GFF output files to process", default=interproscan_files, file_format="gff", required=True )
        self.add_input_file_list( "orf_files", "The ORF sequence files (sequences ID : ID_num [orf_start - orf_stop]).", default=orf_files, file_format="fasta", required=True )
        self.add_input_file( "obo_file", "The file to describe GO terms.", default=obo_file, file_format="obo", required=True )
        self.add_output_file_list( "annotations", "Functional annotation results.", pattern='{basename_woext}_interpro.gff', items=interproscan_files, file_format="gff" )
        self.add_output_file_list( "go", "Sequences associated GO.", pattern='{basename_woext}_GO.tsv', items=interproscan_files )
        self.add_output_file( "stderr", "The stderr output file.", filename="InterProScan2RnaBrowse.stderr" )

    def process(self):
        # Interproscan to annotation
        annotation = PythonFunction( interpro_2_annot, cmd_format='{EXE} {IN} {OUT} 2>> ' + self.stderr )
        annotation = MultiMap( annotation, inputs=[self.interproscan_files, self.orf_files], outputs=self.annotations )

        # Interproscan to GO
        gene_ontology = PythonFunction( interpro_2_go, cmd_format='{EXE} {IN} ' + self.obo_file + ' {OUT} 2>> ' + self.stderr )
        gene_ontology = Map( gene_ontology, inputs=self.interproscan_files, outputs=self.go, includes=self.obo_file )