#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from string import lower

from subprocess import Popen, PIPE

from jflow.component import Component

def split_interpro_gff( interpro_gff, out_gff, out_fasta ):
    """
     @summary : Produce one GFF3 and one Fasta from the GFF output of InterProScan.
     @note : The GFF3 format is a flat tab-delimited file. With InterproScan it also contains a FASTA format representation of the predicted protein sequences and their matches.
      @param interpro_gff : [string] the output of InterProScan with '--formats GFF'.
      @param out_gff : [string] the extracted GFF3.
      @param out_fasta : [string] the extracted Fasta.
    """
    interpro_fh = open( interpro_gff )
    gff_fh = open( out_gff, "w" )
    fasta_fh = open( out_fasta, "w" )

    fasta_area = False
    for interproscan_line in interpro_fh:
        # If this line begin a Fasta area
        if interproscan_line.startswith(">"):
            fasta_area = True
        # If this line begin a GFF area
        elif interproscan_line.startswith("#"):
            fasta_area = False
        if fasta_area:
            fasta_fh.write(interproscan_line)
        else:
            gff_fh.write(interproscan_line)

    gff_fh.close()
    fasta_fh.close()


class InterProScan (Component):
    """
     @summary : Provides functional analysis of proteins by classifying them into families and predicting domains and important sites.
    """

    def define_parameters(self, fasta_files, sequence_type="p", min_ORF_size=None, applications=None, GO_terms=True, pathways=True, output_format="GFF3"):
        """
          @param fasta_files : [list] sequences files to process.
          @param sequence_type : [char] the type of the input sequences (dna/rna (n) or protein (p)).
          @param min_ORF_size : [int] minimum nucleotide size of ORF to report. Will only be considered if n is specified as a sequence type.
          @param applications : [list] the list of analysis used. @see interproscan manual.
          @param GO_terms : [bool] add Gene Ontology annotation.
          @param pathways : [bool] add Pathway annotation.
          @param output_format : [string] the output format.
         @todo : manage 'SVG' and 'HTML' output format.
        """
        # Parameters
        self.add_parameter( "sequence_type", "The type of the input sequences (dna/rna (n) or protein (p)).", default=sequence_type, choices=["n", "p"] )
        self.add_parameter( "min_ORF_size", "Minimum nucleotide size of ORF to report. Will only be considered if n is specified as a sequence type.", 
                            default=min_ORF_size, type="int" )
        self.add_parameter_list( "applications", "The list of analysis used. By default, all available analyses are run.", default=applications, 
                                 choices=["ProDom","HAMAP","PANTHER","TIGRFAMs","SUPERFAMILY","PRINTS","PIRSF","Gene3D","COILS","PROSITE","PfamA","SMART"] )
        self.add_parameter( "GO_terms", "Process Gene Ontology annotation.", default=GO_terms, type="bool" )
        self.add_parameter( "pathways", "Process Pathway annotation.", default=pathways, type="bool" )
        self.add_parameter( "output_format", "The output format.", default=output_format, choices=["GFF3", "TSV", "XML"] )
        outputFile_formats = { "GFF3" : "gff",
                               "TSV" : "tsv",
                               "XML" : "xml" }
        # Options
        self.options = " --seqtype " + self.sequence_type
        if self.sequence_type == "n" and self.min_ORF_size != None:
            self.options += " --minsize " + str(self.min_ORF_size)
        if self.applications:
            self.options += " --applications " + ",".join(self.applications)
        if self.GO_terms:
            self.options += " --goterms"
        if self.pathways:
            self.options += " --pathways"

        # Files
        self.add_input_file_list( "fasta_files", "Sequences files to process.", default=fasta_files, file_format="fasta", required=True )
        self.add_output_file_list( "output_files", "Functional analysis results.", pattern='{basename_woext}.' + lower(output_format), items=fasta_files,
                                   file_format=outputFile_formats[output_format] )
        self.add_output_file_list( "output_fasta_files", "The stderr output files.", pattern='{basename_woext}.fasta', items=fasta_files, 
                                   file_format="fasta" ) # Only with GFF output format
        self.add_output_file_list( "stderr", "The stderr output files.", pattern='{basename_woext}.stderr', items=fasta_files )

    def get_version(self):
        cmd = [self.get_exec_path("interproscan")]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.split()[4]

    def process(self):
        # Process
        interpro_files=self.output_files
        if self.output_format == "GFF3":
            # Launch InterProScan
            interpro_files = self.get_outputs('{basename_woext}.tmp', self.fasta_files)
        
        self.add_shell_execution(self.get_exec_path("interproscan") + " --input $1 --outfile $2 --formats " + self.output_format + self.options + " --tempdir " + self.output_directory + " 2>> $3", 
                                 cmd_format='{EXE} {IN} {OUT}',
                                 inputs=self.fasta_files, outputs=[interpro_files, self.stderr] , map=True) 
        
        if self.output_format == "GFF3":
        
            # Dissociate Fasta and GFF3
            self.add_python_execution(split_interpro_gff,
                                      inputs=interpro_files, outputs=[self.output_files, self.output_fasta_files], map=True)
            