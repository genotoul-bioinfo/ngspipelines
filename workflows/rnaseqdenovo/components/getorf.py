#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from subprocess import Popen, PIPE

from jflow.component import Component

from weaver.abstraction import Map
from weaver.function import ShellFunction, PythonFunction

def filter_orf( max_orf_nb, orf_file, filtered_file ):
    """
     @summary : Filters the number of ORF by sequence on ORF file. It keeps the most longs. If several ORF have the same length they are all kept.
      @param max_orf_nb : [int] the maximum number of ORF by sequence.
      @param orf_file : [str] path to the processed file.
      @param filtered_file : [str] path to the output.
    """
    def write_ORF( orfs, max_length_orf_nb ):
        sorted_orfs = sorted( orfs, key=lambda orf: -int(len(orf[2])) ) # sort ORF by length
        nb_length = 0
        i = 0
        # While the maximum number of ORF is not exceeded and the sequence has an other ORF
        while nb_length < max_length_orf_nb and i < len(sorted_orfs):
            seqio.writefasta(out_fh, [sorted_orfs[i]])
            # If the length of the current ORF is different to the previous ORF
            if i == 0 or len(sorted_orfs[i][2]) != len(sorted_orfs[i-1][2]):
                nb_length += 1
            i += 1

    import jflow.seqio as seqio

    # Main
    previous_seq_id = None
    orfs = list()
    out_fh = open(filtered_file, "w")
    reader = seqio.FastaReader( orf_file )
    for id, desc, seq, qual in reader:
        current_seq_id = id.split("_")[:-1]
        if previous_seq_id is None:
            previous_seq_id = current_seq_id
        # If ORF target has changed
        if current_seq_id != previous_seq_id:
            write_ORF( orfs, int(max_orf_nb) )
            # Next ORF
            orfs = list()
            previous_seq_id = current_seq_id
        orfs.append( [id, desc, seq, qual] )
    # Write last sequence
    if len(orfs) != 0:  
        write_ORF( orfs, int(max_orf_nb) )


class GetORF (Component):
    """
     @summary : Find and extract open reading frames (ORFs).
     @see : http://emboss.sourceforge.net/apps/cvs/emboss/apps/getorf.html
    """

    def define_parameters(self, fasta_files, max_orf_nb=None, min_size=30, max_size=1000000, find=1, table=0):
        """
          @param fasta_files : [list] sequences files to process.
          @param max_orf_nb : [int] the maximum number of ORF by sequence (if several ORF have the same length they are all kept).
          @param min_size : [int] minimum nucleotide size of ORF to report.
          @param max_size : [int] maximum nucleotide size of ORF to report.
          @param find : [int] There are two possible definitions of an open reading frame: it can either be a region that is free of STOP codons or a region that begins with a START codon and ends with a STOP codon. The last three options are probably only of interest to people who wish to investigate the statistical properties of the regions around potential START or STOP codons. The last option assumes that ORF lengths are calculated between two STOP codons.
                        0 (Translation of regions between STOP codons)
                        1 (Translation of regions between START and STOP codons)
                        2 (Nucleic sequences between STOP codons)
                        3 (Nucleic sequences between START and STOP codons)
                        4 (Nucleotides flanking START codons)
                        5 (Nucleotides flanking initial STOP codons)
                        6 (Nucleotides flanking ending STOP codons)
          @param table : [int] Code to use.
                         0  Standard
                         1  Standard (with alternative initiation codons)
                         2  Vertebrate Mitochondrial
                         3  Yeast Mitochondrial
                         4  Mold, Protozoan, Coelenterate Mitochondrial and Mycoplasma/Spiroplasma
                         5  Invertebrate Mitochondrial
                         6  Ciliate Macronuclear and Dasycladacean
                         9  Echinoderm Mitochondrial
                         10 Euplotid Nuclear
                         11 Bacterial
                         12 Alternative Yeast Nuclear
                         13 Ascidian Mitochondrial
                         14 Flatworm Mitochondrial
                         15 Blepharisma Macronuclear
                         16 Chlorophycean Mitochondrial
                         21 Trematode Mitochondrial
                         22 Scenedesmus obliquus
                         23 Thraustochytrium Mitochondrial
        """
        # Parameters
        self.add_parameter( "min_size", "Minimum nucleotide size of ORF to report.", default=min_size, type=int )
        self.add_parameter( "max_size", "Maximum nucleotide size of ORF to report.", default=max_size, type=int )
        self.add_parameter( "find", 
                            "ORF definition : 0 (Translation of regions between STOP codons) " +\
                              "1 (Translation of regions between START and STOP codons) " +\
                              "2 (Nucleic sequences between STOP codons) " +\
                              "3 (Nucleic sequences between START and STOP codons) " +\
                              "4 (Nucleotides flanking START codons) " +\
                              "5 (Nucleotides flanking initial STOP codons) " +\
                              "6 (Nucleotides flanking ending STOP codons).",
                            default=find, choices=[0, 1, 2, 3, 4, 5, 6], type=int )
        self.add_parameter( "table", 
                            "Code to use : 0  Standard " +\
                              "1  Standard (with alternative initiation codons) " +\
                              "2  Vertebrate Mitochondrial " +\
                              "3  Yeast Mitochondrial " +\
                              "4  Mold, Protozoan, Coelenterate Mitochondrial and Mycoplasma/Spiroplasma " +\
                              "5  Invertebrate Mitochondrial " +\
                              "6  Ciliate Macronuclear and Dasycladacean " +\
                              "9  Echinoderm Mitochondrial " +\
                              "10 Euplotid Nuclear " +\
                              "11 Bacterial " +\
                              "12 Alternative Yeast Nuclear " +\
                              "13 Ascidian Mitochondrial " +\
                              "14 Flatworm Mitochondrial " +\
                              "15 Blepharisma Macronuclear " +\
                              "16 Chlorophycean Mitochondrial " +\
                              "21 Trematode Mitochondrial " +\
                              "22 Scenedesmus obliquus " +\
                              "23 Thraustochytrium Mitochondrial ",
                            default=table, choices=[0, 1, 2, 3, 4, 5, 6, 9, 10, 11, 12, 13, 14, 15, 16, 21, 22, 23], type=int )
        self.add_parameter( "max_orf_nb", "The maximum number of ORF by sequence (if several ORF have the same length they are all kept).", default=max_orf_nb, type=int )

        # Files
        self.add_input_file_list( "fasta_files", "Sequences files to process.", default=fasta_files, file_format="fasta", required=True )
        self.add_output_file_list( "output_files", "The ORFs fasta files.", pattern='{basename_woext}_orf.fasta', items=fasta_files, file_format="fasta" )
        self.add_output_file_list( "orf_stderr_files", "The ORFs stderr files.", pattern='{basename_woext}_orf.stderr', items=fasta_files, file_format="any" )
        self.add_output_file( "stderr", "The stderr output file.", filename="filter_getorf.stderr" )

    def process(self):
        # Temp files
        get_orf_outputs = self.output_files
        if self.max_orf_nb != None:
            get_orf_outputs= self.get_outputs('{basename_woext}_before_filter.tmp', self.fasta_files)

        # Find ORF
        self.add_shell_execution(self.get_exec_path("getorf") + " -sequence $1 -outseq $2 --minsize " + str(self.min_size) + " --maxsize " + str(self.max_size) +
                                 " --find " + str(self.find) + " --table " + str(self.table) + " 2>> $3", 
                                 cmd_format='{EXE} {IN} {OUT}',inputs=self.fasta_files, outputs=[get_orf_outputs,self.orf_stderr_files],map=True)
        # Filter
        if self.max_orf_nb != None:
            filter = PythonFunction( filter_orf, cmd_format='{EXE} ' + str(self.max_orf_nb) + ' {IN} {OUT} 2>> ' + self.stderr )
            Map( filter, inputs=get_orf_outputs, outputs=self.output_files )