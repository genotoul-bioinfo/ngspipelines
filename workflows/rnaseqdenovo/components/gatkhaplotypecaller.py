#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component
from subprocess import Popen, PIPE

class GatkHaplotypeCaller (Component):
    """
    @summary : Call SNPs and indels simultaneously via local de-novo assembly of haplotypes in an active region.
    @see : http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_haplotypecaller_HaplotypeCaller.html
    """
    
    def define_parameters(self, input_files, reference, min_call_confidence=30.0, min_emit_confidence=30.0, min_qual_score=10, intervals_files=None, memory=2):
        """
         @param input_files : [list] Path to the bam files processed.
         @param reference : [str] Path to the reference.
         @param min_call_confidence : [int] The minimum confidence needed for a given base for it 
                                       to be used in variant calling. 
         @param min_emit_confidence : [float] This argument allows you to emit low quality calls as
                                       filtered records. 
         @param min_qual_score : [float] The minimum phred-scaled Qscore threshold to separate high
                                  confidence from low confidence calls. Only genotypes with 
                                  confidence >= this threshold are emitted as called sites.
         @param memory : [int] the booked memory for process (in Go).
        """
        # Parameters
        self.add_parameter( "memory", "The memory size to allocate (in Giga).", default=memory, type="int" )
        self.add_parameter( "min_call_confidence", "The minimum confidence needed for a given base for it to be used in variant calling.", default=min_call_confidence, type="float" )
        self.add_parameter( "min_emit_confidence", "This argument allows you to emit low quality calls as filtered records.", default=min_emit_confidence, type="float" )
        self.add_parameter( "min_qual_score", "This argument allows you to emit low quality calls as filtered records.", default=min_qual_score, type="int" )

        # Files
        self.add_input_file_list( "input_files", "The BAM files processed.", default=input_files, file_format="bam", required=True )
        self.add_input_file_list( "intervals_files", "The intervals files to optimize computation.", default=intervals_files)
        self.add_output_file_list( "stderr_HaplotypeCaller", "The Haplotype caller stderr.", pattern="{basename_woext}.haplotypecaller.stderr", items= input_files )
        
        self.add_input_file( "reference", "The FASTA reference file.", default=reference, file_format="fasta", required=True )
        self.add_output_file( "output_file", "The variants file.", filename="variant.vcf", file_format="vcf" )
        self.add_output_file( "stderr", "The stderr output file.", filename="haplotypecaller.stderr" )

    def get_version(self):
        xmx_option  = "-Xmx1g"
        cmd = [self.get_exec_path("java"),  xmx_option , "-jar" , self.get_exec_path("gatk")]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stderr.split()[11].replace("):","")
    
    def process(self):
        xmx_option  = "-Xmx" + str(self.memory) + "g"
        tmp_outputs = self.output_file

        # Calling
        calling_options = " -dontUseSoftClippedBases" + " -stand_call_conf " + str(self.min_call_confidence) \
                        + " -stand_emit_conf " + str(self.min_emit_confidence) + " -mbq " + str(self.min_qual_score)
        #if len(self.intervals_files) > 1:
        
        if len(self.input_files) > 1: # multi-samples options
            calling_options += " -ERC GVCF -variant_index_type LINEAR -variant_index_parameter 128000"
        
        if len(self.intervals_files)== 1 and len(self.input_files)==1 :
            self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T HaplotypeCaller"\
                            + " -R " + self.reference + " -I $1" + calling_options + " -o $2 2>> $3" , cmd_format='{EXE} {IN} {OUT}',
                             inputs=self.input_files, outputs=[self.output_file,self.stderr], map=False)
            
        elif len(self.intervals_files)== 1 and len(self.input_files)>1 :
            tmp_outputs= self.get_outputs( '{basename_woext}.vcf', self.input_files)
            tmp_stderr= self.get_outputs( '{basename_woext}.stderr', self.input_files)
            self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T HaplotypeCaller"\
                         + " -R " + self.reference + " -I $1" + calling_options + " -o $2 2>> $3" , cmd_format='{EXE} {IN} {OUT}',
                         inputs=self.input_files, outputs=[tmp_outputs,tmp_stderr], includes=self.reference , map=True)
            self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T GenotypeGVCFs "\
                                   + "-R $1 --variant " + " --variant ".join(tmp_outputs)  + " -o $2 2>> " + self.stderr, cmd_format='{EXE} {IN} {OUT}' ,
                                   inputs=self.reference, outputs=self.output_file, includes=tmp_outputs )
        elif len(self.intervals_files)> 1 and len(self.input_files)==1 :
            tmp_outputs= self.get_outputs( '{basename_woext}.vcf', self.intervals_files)
            tmp_stderr= self.get_outputs( '{basename_woext}.stderr', self.intervals_files)
            self.add_shell_execution("cp $1 $1.intervals; "+self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T HaplotypeCaller"\
                            + " -L $1.intervals -R " + self.reference + " -I "+self.input_files[0] + calling_options + " -o $2 2>> $3" , cmd_format='{EXE} {IN} {OUT}',
                             inputs=self.intervals_files, outputs=[tmp_outputs,tmp_stderr], includes=[self.reference,self.input_files[0]] , map=True)
            
            stderr_combine=os.path.join(self.output_directory,"CombineVariants.stderr")
            self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T CombineVariants "\
                               + " --genotypemergeoption UNSORTED -R $1 --variant " + " --variant ".join(tmp_outputs)  + " -o $2 2>> $3" , cmd_format='{EXE} {IN} {OUT}' ,
                               inputs=self.reference, outputs=[self.output_file, stderr_combine], includes=tmp_outputs)
            #TODO            
        elif len(self.intervals_files)> 1 and len(self.input_files)>1 :
        
            tmp_intervals_files={}
            tmp_intervals_stderr={}
            # Calling par interval files
            for intervals_file in self.intervals_files :
                interval_with_ext=intervals_file+".intervals"
                key_interval=os.path.basename(intervals_file)
                tmp_intervals_files[key_interval]= self.get_outputs( '{basename_woext}.'+key_interval +'.vcf', self.input_files)
                tmp_intervals_stderr[key_interval]= self.get_outputs( '{basename_woext}.'+key_interval+'.HaplotypeCaller.stderr', self.input_files)
                #map per samples
                self.add_shell_execution("cp "+intervals_file+" "+ interval_with_ext+" ; "+ self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T HaplotypeCaller"\
                            + " -L "+interval_with_ext+" -R " + self.reference + " -I $1" + calling_options + " -o $2 2>> $3" , cmd_format='{EXE} {IN} {OUT}',
                             inputs=self.input_files, outputs=[tmp_intervals_files[key_interval],tmp_intervals_stderr[key_interval]], includes=[self.reference,intervals_file] , map=True)
     
            #fusion des echantillons par interval
            group_by_interval={}
            #transpose intervals and samples
            for intervals_file in self.intervals_files :
                key_interval=os.path.basename(intervals_file)
                if not group_by_interval.has_key(key_interval):
                    group_by_interval[key_interval]=[]
                for i,bam in enumerate(tmp_intervals_files[key_interval]) :
                    group_by_interval[key_interval].append(tmp_intervals_files[key_interval][i])
            
            vcf_by_interval=[]
        
            for i in group_by_interval.keys():
                self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T GenotypeGVCFs "\
                               + "-R $1 --variant " + " --variant ".join(group_by_interval[i])  + " -o $2 2>> $3", cmd_format='{EXE} {IN} {OUT}' ,
                               inputs=self.reference, outputs=[os.path.join(self.output_directory,str(i)+".vcf"),os.path.join(self.output_directory,str(i)+".GenotypeGVCFs.stderr")], includes=group_by_interval[i] )
                vcf_by_interval.append(os.path.join(self.output_directory,str(i)+".vcf"))
                
            #fusion des interval
            stderr_combine=os.path.join(self.output_directory,"CombineVariants.stderr")
            self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T CombineVariants "\
                               + " --genotypemergeoption UNSORTED -R $1 --variant " + " --variant ".join(vcf_by_interval)  + " -o $2 2>> $3" , cmd_format='{EXE} {IN} {OUT}' ,
                               inputs=self.reference, outputs=[self.output_file, stderr_combine], includes=vcf_by_interval)
    