#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import pickle

from jflow.component import Component

        
def update_contigs(contig, counts):
    import pickle
    # update contig depth
    if len(counts.contig_depth.keys()) > 0 :
        contig.update_depth(counts.contig_depth)
                             
class UpdateContigs (Component):
    
    def define_parameters(self, contig, counts):
        self.add_input_object("contig", "Where is stored the contig dump file", default=contig, required=True)
        self.add_input_object("counts", "Where is stored the counts dump file", default=counts, required=True)
        
    def process(self):
        self.add_python_execution(update_contigs,inputs=[self.contig, self.counts])
