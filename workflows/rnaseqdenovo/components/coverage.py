#
# Copyright (C) 2012 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, math, pickle

from jflow.component import Component

from weaver.function import PythonFunction
from weaver.abstraction import Map


def compute_coverage( exec_path, start_idx, end_idx, output_file, bams_list_dump):
    """
     @summary : Writes the coverage statistics for the regions with an index between 'start_idx' and 'end_idx'.
                 Example of output file :
                  #Region_name    Library_name    Global_avg_depth    Window_size    Avg_depth_by_window    Avg_depth_for_lib
                  contig_1    lib_A    15.0    50    10,10,10    10
                  contig_1    lib_B    15.0    50    20,20,20    20
                  contig_2    lib_A    5.0    24    5,5,5    5
                  '''
      @param exec_path : [str] path to the samtools.
      @param start_idx : [int] the index (in bam header order) of the first region processed.
      @param end_idx : [int] the index (in BAM header order) of the last region processed.
      @param output_file : [str] path to the output file.
      @param bams_list_dump : [str] path to the pickle with list of libraries alignment files.
    """
    import re
    import sys
    import pickle
    from subprocess import Popen, PIPE

    def _process_regions_sizes( bam, start_idx, end_idx ):
        """
         @summary : Returns the size of regions with an index between 'start_idx' and 'end_idx'. 
          @param bam : [str] the path to the BAM file where regions are used to reference.
          @param start_idx : [int] the index (in BAM header order) of the first region processed.
          @param end_idx : [int] the index (in BAM header order) of the last region processed.
         @return : [dict] The size by region (key : region name ; value : region size).
        """
        regions_sizes = dict()
        cmd = [exec_path, "view", "-H", bam]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        sys.stderr.write( stderr )
        current_idx = 1
        for header_line in stdout.splitlines(): # @SQ    SN:ORENI.1.1    LN:1745
            header_line = header_line.strip()
            if header_line.startswith( '@SQ' ):
                if current_idx >= int(start_idx) and current_idx <= int(end_idx):
                    match = re.match( "\@SQ\s+SN\:([^\s]+)\s+LN\:(\d+)", header_line )
                    regions_sizes[match.group(1)] = int(match.group(2))
                current_idx += 1
        return regions_sizes

    def _process_position(  current_pos, window_current_pos, window_size, current_depths, window_sum, sum_by_lib, coverage_histogram ):
        """
         @summary : Process a position in region.
          @param current_pos : [int] current position in region.
          @param window_current_pos : [int] current position in window.
          @param window_size : [int] size of the window
          @param current_depths : [dict] depths for each library at the current position.
          @param window_sum : [dict] sum of depth for the current window and by library.
          @param sum_by_lib : [dict] sum of depth for all the region by library.
          @param coverage_histogram : [dict]
         @return : [int, int] the next position on region and on window.
        """
        for lib_idx in range( len(current_depths) ):
            sum_by_lib[lib_idx] += int(current_depths[lib_idx])
            window_sum[lib_idx] += int(current_depths[lib_idx])
            # End of current window
            if window_current_pos == window_size:
                window_avg_cov = round( float(float(window_sum[lib_idx])/window_size), 2 )
                coverage_histogram[lib_idx] += str(window_avg_cov) + ","
                window_sum[lib_idx] = 0
        current_pos += 1
        if window_current_pos == window_size:
            window_current_pos = 0
        window_current_pos += 1

        return current_pos, window_current_pos

    # Load annotations files list
    bams_list_fh = open(bams_list_dump, "rb")
    bams = pickle.load(bams_list_fh)
    bams_list_fh.close()
    max_nb_plot_part = 100
    libraries_names = [os.path.splitext(os.path.basename(i))[0] for i in bams]
    region_sizes = _process_regions_sizes( bams[0], start_idx, end_idx )

    out_fh = open(output_file, "w")
    out_fh.write( "#Region_name\tLibrary_name\tGlobal_avg_depth\tWindow_size\tAvg_depth_by_window\tAvg_depth_for_lib\n" )

    for current_contig in region_sizes.keys():
        # Launch coverage
        cmd = ( exec_path, "depth", "-r", current_contig ) + bams
        p = Popen( cmd, stdout=PIPE, stderr=PIPE )

        # Set windows size
        window_size = int( round((region_sizes[current_contig]/float(max_nb_plot_part)) + 0.5, 0) )
        if window_size == 0 :
            window_size = 1

        # Process coverage result
        previous_pos = 1
        window_current_pos = 1
        sum_by_lib = [0 for elt in libraries_names]
        window_sum = [0 for elt in libraries_names]
        coverage_histogram = ["" for elt in libraries_names]
        for line in iter(p.stdout.readline, ''):
            line_fields = line.strip().split("\t")
            current_pos = int(line_fields[1])
            depths = line_fields[2:]
            # Count unaligned positions (start and internal)
            while previous_pos < current_pos:
                previous_pos, window_current_pos = _process_position( previous_pos, window_current_pos, window_size, [0 for elt in libraries_names], window_sum, sum_by_lib, coverage_histogram )
            # Count position
            previous_pos, window_current_pos = _process_position( previous_pos, window_current_pos, window_size, depths, window_sum, sum_by_lib, coverage_histogram )
        # Count unaligned positions (end)
        while previous_pos <= region_sizes[current_contig]:
            previous_pos, window_current_pos = _process_position( previous_pos, window_current_pos, window_size, [0 for elt in libraries_names], window_sum, sum_by_lib, coverage_histogram )

        # Process last incomplete windows
        if (region_sizes[current_contig] % window_size) > 0:
            for lib_idx in range( len(libraries_names) ):
                last_window_size = region_sizes[current_contig] % window_size
                window_avg_cov = round( float(float(window_sum[lib_idx])/last_window_size), 2 )
                coverage_histogram[lib_idx] += str(window_avg_cov) + ","

        # Write coverage data
        global_avg_depth = round( float(sum(sum_by_lib))/float(region_sizes[current_contig]), 2 )
        for lib_idx in range( len(libraries_names) ):
            avg_depth_lib = round( float(sum_by_lib[lib_idx])/float(region_sizes[current_contig]), 2 )
            out_fh.write( "\t".join([current_contig, libraries_names[lib_idx], str(global_avg_depth), str(window_size), coverage_histogram[lib_idx][:-1], str(avg_depth_lib)]) + "\n" )
        p.stdout.close()

        # Manage exit status
        p.wait()
        if p.returncode < 0:
            error_msg = "".join( map(str, p.stderr.readlines()) )
            raise StandardError( "Error in subprocess samtools depth.\n\tCmd : " + " ".join(cmd) + "\n\tError : " + error_msg )
        p.stderr.close()
    out_fh.close()


class Coverage (Component):

    def define_parameters( self, bams, bais, nb_regions, split_size=200 ):
        """
          @param bams : [list] paths to the bam files.
          @param bais : [list] paths to the index files.
          @param nb_regions : [int] the number of regions in bams.
          @param split_size : [int] the number of processed regions by submission thread.
        """
        # Parameters
        self.add_parameter("nb_regions", "The number of regions to consider", default=nb_regions, type="int")
        self.add_parameter("split_size", "Which size should be used to split the bams", default=split_size, type="int")
        self.nb_submissions = int( math.ceil(float(nb_regions)/split_size) )
        # Files
        self.add_input_file_list("bams", "Which bam files should be used", file_format="bam", default=bams, required=True)
        self.add_input_file_list("bais", "Which bai files should be used", file_format="bai", default=bais, required=True)
        self.add_output_file("stderr", "The stderr output file", filename="coverage.stderr")
        self.add_output_file_list("output_files", "The stderr output file", pattern="coverage_{FULL}.stdout", items=range(self.nb_submissions))

    def process( self ):
        # Serialize the list of bams
        bams_list = tuple(path for path in self.bams)
        bams_list_dump_path = self.get_temporary_file(".dump")
        bams_list_dump = open( bams_list_dump_path, "wb" )
        pickle.dump( bams_list, bams_list_dump )
        bams_list_dump.close()
        # Process
        compute = PythonFunction( compute_coverage, cmd_format="{EXE} {ARG} {OUT} {IN} 2>> " + self.stderr )
        idx_end = 0
        for idx in range( self.nb_submissions ):
            idx_start = idx_end + 1
            idx_end = idx_start + int(self.split_size) - 1
            compute( arguments=[self.get_exec_path("samtools"), idx_start, idx_end], outputs=self.output_files[idx], inputs=bams_list_dump_path, includes=[self.bams, self.bais] )