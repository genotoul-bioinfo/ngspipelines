#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component

from weaver.function import PythonFunction
from weaver.abstraction import Map

def rename_vcf( changed_column, rename_rules_file, old_vcf_file, new_vcf_file ):
    """
     @summary: Write a new_vcf_file where the ID will be replaced by the new names in vcf_rename_file.
      @param changed_column : [int] the number of the column to modify (start with 1).
      @param rename_rules_file : [str] this file contains correspondence between old and new name for sequences.
                                 Syntax of each line : old_name<tab>new_name.
      @param old_vcf_file : [str] path to the processed file.
      @param new_vcf_file : [str] path to the output file.
    """
    import re
    rename = dict()
    # Retrieve new names
    rename_rules_fh = open(rename_rules_file)
    for line in rename_rules_fh:
        line = line.strip()
        old_name, new_name = line.split("\t")
        rename[old_name] = new_name
    # Write new file
    out_fh = open( new_vcf_file, "w" )
    in_fh = open( old_vcf_file )
    for line in in_fh:
        line = line.strip()
        if line.startswith('#'):
            match = re.search( "(.+ID=)([^,]+)(,.+)", line)
            if match is not None and rename.has_key( match.group(2) ):
                line = match.group(1) + rename[match.group(2)] + match.group(3)
            out_fh.write( line + "\n" )
        else:
            line_fields = line.split("\t")
            old_name = line_fields[int(changed_column)-1]
            # Element has a new name
            if rename.has_key( old_name ):
                line_fields[int(changed_column)-1] = rename[old_name]
            out_fh.write( "\t".join( line_fields ) + "\n" )   
    out_fh.close()
    in_fh.close()


class RenameVCF (Component):
    """
     @summary: Change the values of the provided column in a VCF file.
    """

    def define_parameters(self, vcf_files, rename_rules, changed_column=1):
        """
         @param vcf_files : [list] files to process.
         @param rename_rules : [str] this file contains correspondence between old and new name for sequences.
                             Syntax of each line : old_name<tab>new_name.
         @param changed_column : [int] the number of the column to modify (start with 1).
        """
        # Parameters
        self.add_parameter( "column_change", "The index of the column to modify (start with 1).", default=changed_column, required=True )

        # Files
        self.add_input_file_list( "vcf_files", "Files to process.", default=vcf_files, file_format="vcf", required=True )
        self.add_input_file( "rename_rules", "The file which contains correspondence between old and new value. Syntax of each line : old_value<tab>new_value.", default=rename_rules, file_format="tsv", required=True )
        self.add_output_file_list( "output_files", "Files before changement of values in the provided column.", pattern="{basename}", items=vcf_files, file_format="vcf" )
        self.add_output_file( "stderr", "The stderr output file.", filename="rename_vcf.stderr" )

    def process(self):
        rename = PythonFunction(rename_vcf, cmd_format='{EXE} ' + str(self.column_change) + ' ' + self.rename_rules + ' {IN} {OUT} 2>> ' + self.stderr)
        rename = Map(rename, inputs=self.vcf_files, outputs=self.output_files, includes=self.rename_rules)