#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component
from jflow.abstraction import MultiMap

from weaver.function import ShellFunction


class VariantPreprocess (Component):
    """
     @summary : Preprocess BAM for variant calling.
                1- Delete PCR duplicat
                2- Filter on alignment quality
                3- Add read group and reorder
                5- Re-index
    """

    def define_parameters(self, fasta, input_files, sequencers, paired=True, min_mapq=30, memory=2):
        """
          @param fasta : [str] Fasta reference file.
          @param input_files : [list] bam files.
          @param sequencers : [library] list of library object.
          @param paired : [bool] False : bam comes from single library.
          @param min_mapq : [int] minimun MAPQ to keep alignment.
          @param memory : [int] memory for jvm in Giga.
        """
        # Parameters
        self.add_parameter( "min_mapq", "The minimum MAPQ.", default=min_mapq, type="int" )
        self.add_parameter( "memory", "The memory size to allocate (in Giga).", default=memory, type="int" )
        self.add_parameter_list( "sequencers", "Sequencers for each library.", default=sequencers, required=True )
        self.add_parameter( "paired", "Library are paired-end.", default=paired )

        # Files
        self.add_input_file( "fasta", "The reference sequences.", default=fasta, file_format="fasta", required=True )
        self.add_input_file_list( "input_files", "The BAM files.", default=input_files, file_format="bam", required=True )
        #    rmdup and filter
        self.add_output_file_list( "filtered_files", "The BAM after filters.", pattern="{basename_woext}_filter.bam", items=input_files, file_format="bam" )
        self.add_output_file_list( "stderrs_filter", "The filter step stderr files.", pattern="{basename_woext}_filter.stderr", items=input_files )
        #    add read group and reorder
        self.add_output_file_list( "output_files", "The BAM files after process.", pattern="{basename_woext}.bam", items=input_files, file_format="bam" )
        self.add_output_file_list( "stderrs_rg", "The read group step stderr files.", pattern="{basename_woext}_rg.stderr", items=input_files )
        #    index
        self.add_output_file_list( "index_files", "The BAI files for output BAM files.", pattern="{basename}.bai", items=input_files, file_format="bai" )
        self.add_output_file_list( "stderrs_index", "The index step stderr files.", pattern="{basename_woext}_index.stderr", items=input_files )

    def process(self):
        # Delete PCR duplicat & filter qual 30
        rmdup_options = "" if self.paired else "-s"
        self.add_shell_execution( self.get_exec_path("samtools") + " rmdup " + rmdup_options + " $1 - | " + \
                                      self.get_exec_path("samtools") + " view -h -F 256 - | awk '$1~\"^@\" || $5>" + str(self.min_mapq -1) + "' |" + \
                                      self.get_exec_path("samtools") + " view -Sb - > $2 2>> $3 ", cmd_format='{EXE} {IN} {OUT}' ,
                                      inputs=self.input_files, outputs=[self.filtered_files , self.stderrs_filter] ,map=True)
        # Add RG & reorder
        xmx_option = "-Xmx" + str(self.memory) + "g"
        for idx, lib_path in enumerate( self.input_files ):
            lib_name = os.path.basename(os.path.splitext(lib_path)[0])
            addrg_options = "SO=coordinate PU=Unknown PL=" + self.sequencers[idx] + " SM=" + lib_name + " LB=" + lib_name + " VALIDATION_STRINGENCY=SILENT" 
            self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("add_read_group") +  " I=$1 O=$2 "+ \
                                   addrg_options + " 2>> $3 ", cmd_format='{EXE} {IN} {OUT}' ,
                                   inputs=self.filtered_files[idx], outputs=[self.output_files[idx], self.stderrs_rg[idx]], includes=self.fasta)
            
        # Index files
        self.add_shell_execution(self.get_exec_path("samtools") + " index $1 2>> $2", cmd_format='{EXE} {IN} {OUT}',
                                 inputs=self.output_files, outputs=[self.stderrs_index, self.index_files], map=True)
