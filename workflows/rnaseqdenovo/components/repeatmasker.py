#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component


def cleanDesc( in_path, out_path ):
    """
     @summary : Replace invalid characters in sequence description ('>' => '%3E').
      @param in_path : [str] path to the file to convert.
      @param out_path : [str] path to the output file.
    """
    import jflow.seqio as seqio

    reader = seqio.SequenceReader( in_path )
    out_fh = open( out_path, "w" )
    for id, desc, seq, qual in reader:
        desc = desc.replace('>', '%3E')
        # If sequences file is a FASTQ
        if reader.__class__.__name__ == "FastqReader":
            seqio.writefastq(out_fh, [[id, desc, seq, qual]])
        # If sequences file is a FASTA   
        elif reader.__class__.__name__ == "FastaReader":
            seqio.writefasta(out_fh, [[id, desc, seq, qual]])
    out_fh.close()

def toGFF( in_path, out_path ):
    """
     @summary : Write a GFF file from the RepeatMasker output file.
      @param in_path : [str] path to the file to convert.
      @param out_path : [str] path to the output file.
    """
    from jflow.featureio import GFF3Record, GFF3IO

    header = True
    fh_output = GFF3IO( out_path , "w" )
    fh_rm = open( in_path )
    for line in fh_rm:
        if line.strip() == "":
            header = False
        elif not header:
            # Line example : '    11   31.6  1.7  1.6  CHMP7_CRIGR.1.1             1238  1297 (2723) + (GATGAA)n   Simple_repeat         1     61    (0)   1'
            fields = line.strip().split()
            record = GFF3Record()
            record.seq_id = fields[4]
            record.source = "RepeatMasker"
            if fields[10] == "Low_complexity" :
                record.type = "low_complexity_region"
            elif fields[10] == "Simple_repeat" :
                record.type = "repeat_region"
            else:
                record.type = "similarity"
            record.start = fields[5]
            record.end = fields[6]
            record.score = fields[0]
            record.strand = fields[8] if fields[8] == "+" else "-"
            record.phase = "."
            record.attributes = dict()
            record.addToAttribute( "sw_score", fields[0] )
            record.addToAttribute( "substitution_perc", fields[1] )
            record.addToAttribute( "deletion_perc", fields[2] )
            record.addToAttribute( "insertion_perc", fields[3] )
            record.addToAttribute( "nucleotid_after_match", fields[7][1:-1] )
            record.addToAttribute( "sbjct_start", fields[11] )
            record.addToAttribute( "sbjct_end", fields[12] )
            record.addToAttribute( "desc", fields[10] + " " + fields[9] )
            fh_output.write( record )
    fh_rm.close()


class RepeatMasker (Component):
    """
     @summary : Annotates and masks highly repetitive DNA sequences on fasta files. 
     @see : http://www.repeatmasker.org/
    """

    def define_parameters(self, input_files, species, engine="crossmatch", speed="default", gccalc=True, custom_library=None):
        """
        @param input_files : [list] fasta file to annot and mask.
        @param species : [str] query species for repeatmasker.
        @param engine  : [str] engine use when searching the sequence.
                          - 'crossmatch' is slower but often more sensitive than the other engines.
                          - 'abblast' ( formally known as WUBlast ) is very fast with a slight cost of sensitivity. 
                          - 'rmblast' is a RepeatMasker compatible version of the NCBI Blast tool suite. 
                          - 'hmmer' uses the new nhmmer program to search sequences against the new Dfam database ( human only ).
        @param speed : [str] sensitivity of your search. The more sensitive the longer the processing time.
                        - 'slow' is 0-5% more sensitive, 2-3 times slower than default.
                        - 'default'.
                        - 'quick' is 5-10% less sensitive, 2-5 times faster than default.
                        - 'rush' is about 10% less sensitive, 4->10 times faster than default.
        @param gccalc : [bool] force RepeatMasker to calculate the GC content even for batch files/small seqs.
        @param custom_library : [str] path to the custom annotate lineage specific repeats.
        """
        # Parameters
        self.add_parameter( "species", "The species of queries.", default=species, required=True )
        self.add_parameter( "engine", "Engine use when searching the sequence", default=engine, choices=["crossmatch", "abblast", "rmblast", "hmmer"] )
        self.add_parameter( "speed", "Sensitivity of your search. The more sensitive the longer the processing time.", default=speed, choices=["slow", "default", "quick", "rush"] )
        self.add_parameter( "gccalc", "Force RepeatMasker to calculate the GC content even for batch files/small seqs.", default=gccalc, type="bool" )
        self.add_parameter( "custom_library", "The custom annotate lineage specific repeats.", default=custom_library )
        # Options
        self.options = "-engine " + self.engine
        speed_flags = { "slow":"-s", "default":"", "quick":"-q", "rush":" -qq" }
        self.options += " " + speed_flags[self.speed]
        if self.gccalc:
            self.options += " -gccalc"
        if self.custom_library != None:
            self.options += " -lib " + self.custom_library
        else:
            self.options += ' -species "' + self.species + '"'

        # Files
        self.add_input_file_list( "input_files", "Fasta file to annot and mask.", default=input_files, file_format="fasta", required=True )
        self.add_output_file_list( "cleaned_fasta", "Fasta file to annot and mask without descriptions.", pattern="{basename_woext}.fasta", items=input_files, file_format="fasta" )
        self.add_output_file_list( "output_files", "The sequences annotations : location of repeats and interspersed.", pattern="{basename_woext}.gff", items=input_files, file_format="gff" )
        self.add_output_file_list( "masked", "The submitted file in which all recognized interspersed or simple repeats have been masked.", pattern="{basename_woext}.fasta.masked", items=input_files, file_format="fasta" )
        self.add_output_file_list( "summary", "A table summarizing the repeat content of the queries sequences.", pattern="{basename_woext}.fasta.tbl", items=input_files )
        #self.add_output_file_list( "aln", "Alignments of the queries with the matching repeats.", pattern="{basename_woext}.fasta.cat", items=input_files )
        self.add_output_file( "stderr", "The stderr output file.", filename="repeatmasker.stderr" )

    def get_version(self):
        cmd = self.get_exec_path("repeatmasker") + " -v"
        p = os.popen(cmd)
        return p.readline().strip().split()[2]

    def process(self):
        # Clean description
        #cmd_format='{EXE} {IN} {OUT} 2>> ' + self.stderr ,
        self.add_python_execution(cleanDesc, 
                                  inputs=self.input_files, outputs=self.cleaned_fasta , map=True)
        # Process
        RM_output = self.get_outputs( '{basename}.out', self.cleaned_fasta )
        case_no_result = "if [ $? == 0 ] && [ ! -e $3 ] ; then touch $3 $4; fi"
        self.add_shell_execution(self.get_exec_path("repeatmasker") + " -dir " + self.output_directory + " " + \
                                 self.options + " $1; "+case_no_result, cmd_format='{EXE} {IN} {OUT} 2>> ' + self.stderr,
                                 inputs=self.cleaned_fasta, outputs=[RM_output, self.masked, self.summary], map=True)
        
        # To GFF3
        #cmd_format='{EXE} {IN} {OUT} 2>> ' + self.stderr ,
        self.add_python_execution(toGFF, 
                                  inputs=RM_output, outputs=self.output_files, map=True)
        