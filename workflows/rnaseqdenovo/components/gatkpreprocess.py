#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component


class GatkPreprocess (Component):

    def define_parameters(self, input_files, input_index_files, fasta, memory=3, split_N_cigar=False, reassign_mapq=None):
        """
          @param input_files : [list] bam files.
          @param input_index_files : [list] bai files.
          @param fasta : [str] fasta reference file.
          @param memory : [int] memory for jvm in Giga.
          @param split_N_cigar : [bool] if true perform GATK SplitNCigarReads application.
          @param reassign_mapping_quality : [int] value to set for mapping quality.
        """
        # Parameters
        self.add_parameter( "memory", "The memory size to allocate (in Giga).", default=memory, type="int" )
        self.add_parameter( "split_N_cigar", "Splits reads into exon segments (getting rid of Ns but maintaining grouping information) and hard-clip any sequences overhanging into the intronic regions.", default=split_N_cigar, type="bool" )
        self.add_parameter( "reassign_mapq", "Reassign mapping quality to this value. For example STAR assigns to good alignments a MAPQ of 255 (which technically means 'unknown' and is therefore meaningless to GATK). With this parameters you can change this value.", default=reassign_mapq, type="int" )

        # Files
        self.add_input_file( "fasta", "The reference sequences.", default=fasta, file_format="fasta", required=True )
        self.add_input_file_list( "input_files", "The BAM files.", default=input_files, file_format="bam", required=True )
        self.add_input_file_list( "input_index_files", "The BAI files.", default=input_index_files, file_format="bai", required=True )
        #    Split cigar
        self.add_output_file_list( "preclean_files", "The BAM with reads splitted in unsequenced intronic regions and/or mapq reassigned.", pattern="{basename_woext}_preclean.bam", items=input_files, file_format="bam" )
        self.add_output_file_list( "preclean_stderrs", "Error file for the pre-clean step.", pattern="{basename_woext}_preclean.stderr", items=input_files )
        #    Intervals
        self.add_output_file_list( "interval_files", "Intervals models.", pattern="{basename_woext}.intervals", items=input_files )
        self.add_output_file_list( "interval_stderrs", "Error file for the intervals calculation step.", pattern="{basename_woext}_interval.stderr", items=input_files )
        #    Realign
        self.add_output_file_list( "output_files", "The BAM with reads cleaned and realigned.", pattern="{basename_woext}.bam", items=input_files, file_format="bam" )
        self.add_output_file_list( "realign_stderrs", "Error file for the realign step.", pattern="{basename_woext}_realign.stderr", items=input_files )

    def process(self):
        bam_for_next_step = self.input_files
        xmx_option = "-Xmx" + str(self.memory) + "g"

        # Filter read for RNAseq
        if (self.split_N_cigar != None and self.split_N_cigar) or self.reassign_mapq != None:
            preclean_options = ""
            if self.split_N_cigar != None and self.split_N_cigar:
                preclean_options += " -T SplitNCigarReads "
            if self.reassign_mapq != None:
                preclean_options += " -rf ReassignMappingQuality -DMQ " + str(self.reassign_mapq)
            for idx in range(len(self.input_files)):
                #clean step
                self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " "+\
                                                     preclean_options + " -I $1 -R " + self.fasta + " -U ALLOW_N_CIGAR_READS -o $2 2>> $3",\
                                                     cmd_format='{EXE} {IN} {OUT}',
                                                     inputs=self.input_files[idx], outputs=[self.preclean_files[idx] , self.preclean_stderrs[idx]], 
                                                     includes=[self.fasta, self.input_index_files[idx]])
            bam_for_next_step = self.preclean_files

        # Realignemnt : step 1 : interval calculation
        for idx in range(len(self.input_files)):
            #interval step
            self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T RealignerTargetCreator --allow_potentially_misencoded_quality_scores "\
                                        " -I $1 -R " + self.fasta + " -o $2 2>> $3",\
                                        cmd_format='{EXE} {IN} {OUT}' ,
                                        inputs=bam_for_next_step[idx], outputs=[self.interval_files[idx], self.interval_stderrs[idx]], includes=[self.fasta,self.fasta+".fai", self.input_index_files[idx]])

        # Realignemnt : step 2 : indel realigner
        self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T IndelRealigner --allow_potentially_misencoded_quality_scores "\
                                    " -I $1 -R " + self.fasta + " -targetIntervals $2 -o $3 2>> $4",\
                                    cmd_format='{EXE} {IN} {OUT}',
                                    inputs=[bam_for_next_step,self.interval_files], outputs=[self.output_files , self.realign_stderrs], 
                                    includes=self.fasta, map=True)
