#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component

from weaver.function import PythonFunction


def blast_index(exec_path, databank_type, input_fasta, databank, stdout_path, stderr_path, *null):
    """
     @param exec_path : path to makeblasdb software.
     @param databank_type : molecule type of target db ('nucl' or 'prot').
     @param input_fasta : databank fasta file.
     @param databank : name of BLAST database to be created.
     @param stdout_path : path to stdout.
     @param stderr_path : path to stderr.
    """
    from subprocess import Popen, PIPE
    import glob

    stdout = ""
    stderr = ""

    # first make the symbolic link
    if os.path.exists(databank): os.unlink(databank)
    os.symlink(input_fasta, databank)

    # index
    to_index=False
    cur_dir  = os.path.dirname(databank)
    if databank_type == 'nucl':
        try:
            if os.path.exists(input_fasta + '.nin') and os.path.exists(input_fasta + '.nhr') and os.path.exists(input_fasta + '.nsq'):
                if os.path.exists(databank + '.nin'): os.unlink(databank + '.nin')
                os.symlink(input_fasta + '.nin', databank + '.nin')
                if os.path.exists(databank + '.nhr'): os.unlink(databank + '.nhr')
                os.symlink(input_fasta + '.nhr', databank + '.nhr')
                if os.path.exists(databank + '.nsq'): os.unlink(databank + '.nsq')
                os.symlink(input_fasta + '.nsq', databank + '.nsq')
            elif os.path.exists(input_fasta + '.nal'):
                for f in glob.glob(input_fasta+".*") :
                    if os.path.exists(os.path.join(cur_dir , os.path.basename(f))): os.unlink(os.path.join(cur_dir , os.path.basename(f)))
                    os.symlink(f , os.path.join(cur_dir , os.path.basename(f)))
            else :
                to_index=True
        except OSError, e:
            stderr = e
    elif databank_type == 'prot':
        try:
            if os.path.exists(input_fasta + '.pin') and os.path.exists(input_fasta + '.phr') and os.path.exists(input_fasta + '.psq'):
                if os.path.exists(databank + '.pin'): os.unlink(databank + '.pin')
                os.symlink(input_fasta + '.pin', databank + '.pin')
                if os.path.exists(databank + '.phr'): os.unlink(databank + '.phr')
                os.symlink(input_fasta + '.phr', databank + '.phr')
                if os.path.exists(databank + '.psq'): os.unlink(databank + '.psq')
                os.symlink(input_fasta + '.psq', databank + '.psq')
            elif os.path.exists(input_fasta + '.pal'): 
                for f in glob.glob(input_fasta+".*") :
                    if os.path.exists(os.path.join(cur_dir , os.path.basename(f))): os.unlink(os.path.join(cur_dir , os.path.basename(f)))
                    os.symlink(f , os.path.join(cur_dir , os.path.basename(f)))
            else :
                to_index=True
        except OSError, e:
            stderr = e

    if to_index:
        # execute blast index
        cmd = [exec_path, "-dbtype", databank_type, "-in", databank, "-out", databank]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
    # write down the stdout
    stdoh = open(stdout_path, "w")
    stdoh.write(stdout)
    stdoh.close()
    # write down the stderr
    stdeh = open(stderr_path, "w")
    stdeh.write(stderr)
    stdeh.close()


class BlastIndex (Component):
    """
     @summary: Build index for NCBI Blast+.
    """

    def define_parameters(self, inputs_fasta, databanks_types):
        """
         @param inputs_fasta : [list] databanks fasta file.
         @param databanks_types : [list] molecule type of target db ('nucl' or 'prot').
        """
        self.add_input_file_list("inputs_fasta", "Which fasta file should be indexed", file_format="fasta", default=inputs_fasta, required=True)
        self.add_parameter_list("databank_type", "What kind of database it is", default=databanks_types, required=True)
        self.add_output_file_list("databanks", "The indexed databank to use with Blast+", pattern='{basename}', items=inputs_fasta, file_format="fasta")
        self.index_files = list()
        for idx, current_db in enumerate(inputs_fasta):
            current_indexes = list()
            if databanks_types[idx] == 'nucl':
                if os.path.exists(current_db + '.nal') :
                    current_indexes.append( os.path.join(self.output_directory, os.path.basename(current_db) + '.nal') )
                else : 
                    current_indexes.append( os.path.join(self.output_directory, os.path.basename(current_db) + '.nin') )
                    current_indexes.append( os.path.join(self.output_directory, os.path.basename(current_db) + '.nhr') )
                    current_indexes.append( os.path.join(self.output_directory, os.path.basename(current_db) + '.nsq') )
            else:
                if os.path.exists(current_db + '.pal') :
                    current_indexes.append( os.path.join(self.output_directory, os.path.basename(current_db) + '.pal') )
                else : 
                    current_indexes.append( os.path.join(self.output_directory, os.path.basename(current_db) + '.pin') )
                    current_indexes.append( os.path.join(self.output_directory, os.path.basename(current_db) + '.phr') )
                    current_indexes.append( os.path.join(self.output_directory, os.path.basename(current_db) + '.psq') )

            self.index_files.append(current_indexes)

        self.add_output_file_list("stdout", "The Blast+ stdout file", items=inputs_fasta, pattern="{basename_woext}.stdout")
        self.add_output_file_list("stderr", "The Blast+ stderr file", items=inputs_fasta, pattern="{basename_woext}.stderr")

    def process(self):
        #blastindex = PythonFunction(blast_index, cmd_format="{EXE} {ARG} {IN} {OUT}")
        for idx, current_db in enumerate(self.inputs_fasta):
            self.add_python_execution(blast_index,inputs=self.inputs_fasta[idx], 
                                      outputs=[self.databanks[idx], self.stdout[idx], self.stderr[idx], self.index_files[idx]], 
                                      arguments=[self.get_exec_path("makeblastdb"), self.databank_type[idx]])
             
