#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from ngspipelines.component_analysis import ComponentAnalysis

class NormalizeRawCount (ComponentAnalysis):
    """
     @summary: Normalize
    """

    def define_parameters(self, matrix, nb_lib):
        """
         @param inputs_fasta : matrix file.
         
        """
        self.add_input_file("matrix", "raw_count", file_format="csv", default=matrix, required=True)
        self.add_parameter("nb_lib", "The number of libraries", default=nb_lib, type="int")
        if nb_lib>2 :
            self.add_output_file("rawcount_msd", "MDS pseudo-count of raw count", file_format="svg", filename="rawcount_MDS.svg")
            self.add_output_file("rle_msd", "MDS pseudo-count of rle", file_format="svg", filename="RLE_MDS.svg")
            self.add_output_file("tmm_msd", "MDS pseudo-count of tmm", file_format="svg", filename="TMM_MDS.svg")
            self.add_output_file("upperquartile_msd", "MDS pseudo-count of upperquartile", file_format="svg", filename="upperquartile_MDS.svg")
        self.add_output_file("rawcount_boxplot", "boxplot pseudo-count of raw count", file_format="svg", filename="rawcount_boxplot.svg")
        self.add_output_file("rawcount_density", "density pseudo-count of raw count", file_format="svg", filename="rawcount_density.svg")
        
        self.add_output_file("rle_boxplot", "boxplot pseudo-count of rle", file_format="svg", filename="RLE_boxplot.svg")
        self.add_output_file("rle_density", "density pseudo-count of rle", file_format="svg", filename="RLE_density.svg")
        self.add_output_file("rle_matrix", "normalized data with rle", file_format="csv", filename="RLE_pseudocounts.csv")
        self.add_output_file("rle_info", "normalization factors with rle", file_format="csv", filename="RLE_info.txt")
        
        self.add_output_file("tmm_boxplot", "boxplot pseudo-count of tmm", file_format="svg", filename="TMM_boxplot.svg")
        self.add_output_file("tmm_density", "density pseudo-count of tmm", file_format="svg", filename="TMM_density.svg")
        self.add_output_file("tmm_matrix", "normalized data with tmm", file_format="csv", filename="TMM_pseudocounts.csv")
        self.add_output_file("tmm_info", "normalization factors with tmm", file_format="csv", filename="TMM_info.txt")
        
        self.add_output_file("upperquartile_boxplot", "boxplot pseudo-count of upperquartile", file_format="svg", filename="upperquartile_boxplot.svg")
        self.add_output_file("upperquartile_density", "density pseudo-count of upperquartile", file_format="svg", filename="upperquartile_density.svg")
        self.add_output_file("upperquartile_matrix", "normalized data with upperquartile", file_format="csv", filename="upperquartile_pseudocounts.csv")
        self.add_output_file("upperquartile_info", "normalization factors with upperquartile", file_format="csv", filename="upperquartile_info.txt")
        
        self.add_output_file("stdout", "stdout",  filename="NormalizeRawCount.stdout")
        self.add_output_file("stderr", "stderr",  filename="NormalizeRawCount.stderr")
        
    def define_analysis(self):
        self.analysis_name = "R Normalization"
        self.soft_name = "edgeR"
        self.soft_parameters = "RLE, TMM, upper-quartile"
        self.comments = ""
        self.mode = None
        self.files = [self.rle_boxplot,self.rle_density,self.rle_matrix,self.rle_info,
                      self.tmm_boxplot,self.tmm_density,self.tmm_matrix,self.tmm_info,
                      self.upperquartile_boxplot,self.upperquartile_density,self.upperquartile_matrix,self.upperquartile_info,
                      self.rawcount_boxplot,self.rawcount_density]
        if self.nb_lib>2 :
            self.files.append(self.rle_msd)
            self.files.append(self.tmm_msd)
            self.files.append(self.upperquartile_msd)
            self.files.append(self.rawcount_msd)
    def get_version(self):
        return "1.0"
    
    def post_process(self):
        self._add_result_element("method", "RLE", "string")
        self._add_result_element("method", "TMM", "string")
        self._add_result_element("method", "upperquartile", "string")
        
    
    def process(self):
        self.add_shell_execution(self.get_exec_path("Rscript")+ " "+ self.get_exec_path("Normalization.R") + " -f $1 -o `dirname $2` > $2 2>> $3",
                                 cmd_format='{EXE} {IN} {OUT} ',
                                 inputs=self.matrix, outputs=[self.stdout,self.stderr]+self.files)
      