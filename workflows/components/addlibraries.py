#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import pickle

from jflow.component import Component

def count_nb_seq(library):

    from jflow.seqio import FastqReader
    previous_nb_seq, nb_total = -1, 0
    if library["nb_sequence"] == None and library["files"] != "":
        for file in library["files"]:
            if not os.path.exists(file) :
                raise Exception(file + " doesn't exist for library " + library_name + ".")
            else :
                nb_seq = 0
                for seq in FastqReader(file) :
                    nb_seq += 1
                nb_total += nb_seq
                if not previous_nb_seq == -1 and not previous_nb_seq == nb_seq :
                    raise Exception(library_name + " files don't same number of sequence for.")
            library["nb_sequence"] = nb_total
            if previous_nb_seq == -1 :
                previous_nb_seq=nb_seq
        library["files"] = ",".join(library["files"])
    return library

                            
class CountSeqLibraries (Component):
    
    def define_parameters(self, project,tab_library):
        self.add_input_object("project", "The project object", default=project)
        self.add_input_object_list("libraries", "Array of libraries arguments", default=tab_library)
        self.add_output_object_list("libraries_count", "Array of libraries arguments with number of sequence", nb_items=len(tab_library))
    def process(self):
        self.add_python_execution(count_nb_seq,inputs=self.libraries, outputs=self.libraries_count, map=True)
        
            