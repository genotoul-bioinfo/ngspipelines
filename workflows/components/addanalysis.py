#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import pickle

from jflow.component import Component

def add_analysis(analysis_name, analysis_type, analysis_hidden, soft_name, soft_parameters, soft_version, comments, mode, project, re_param, f_dump_path,):
    import pickle
    
    #load the files array
    f_dump = open(f_dump_path, "rb")
    files = pickle.load(f_dump)
    f_dump.close()
    # add the analysis
    analysis = project.add_analysis(analysis_name=analysis_name, analysis_type=analysis_type, analysis_hidden=analysis_hidden,
                                    soft_name=soft_name, soft_parameters=soft_parameters, soft_version=soft_version, comments=comments)
    # and save the result files 
    for file in files:
        analysis.add_result_file(file,mode = mode)
    # and add the result elements
    for re in re_param:
        analysis.add_result_element(*re)
                             
                             
class AddAnalysis (Component):
    
    def define_parameters(self, project, analysis_name, analysis_type, analysis_hidden, soft_name, soft_parameters, soft_version, 
                          comments, result_elements=[], files=[], mode = "gz"):
            
        self.add_input_object("project", "The project of the processed analysis", default=project)
        self.add_input_file_list("files", "Which files should be added to the analysis", default=files)
        self.add_parameter("analysis_name", "The name of the processed analysis", default=analysis_name)
        self.add_parameter("analysis_type", "The type of the processed analysis", default=analysis_type)
        self.add_parameter("analysis_hidden", "Should this analysis stay hidden", default=analysis_hidden)
        self.add_parameter("soft_name", "What software was used to perform the analysis", default=soft_name)
        self.add_parameter("soft_parameters", "What software parameters were used to perform the analysis", default=soft_parameters)
        self.add_parameter("soft_version", "What software version was used to perform the analysis", default=soft_version)
        self.add_parameter("comments", "Add some comments on the analysis", default=comments)
        self.add_parameter("mode", "How should be compressed the files", default=mode)
        
        self.add_input_object("result_elements", "Which element should be added to the analysis", default=result_elements)
        
    def process(self):
       
        #serialize result files
        f_dump_path = self.get_temporary_file(".dump")
        f_dump = open(f_dump_path, "wb")
        pickle.dump(self.files, f_dump)
        f_dump.close()
        inputs = [self.project, self.result_elements, f_dump_path]
        arguments=["'"+str(self.analysis_name)+"'", "'"+str(self.analysis_type)+"'", "'"+str(self.analysis_hidden)+"'", "'"+str(self.soft_name)+"'", 
                       "'"+str(self.soft_parameters)+"'", "'"+str(self.soft_version)+"'", "'"+str(self.comments)+"'", "'"+str(self.mode)+"'" ]
        self.add_python_execution(add_analysis,inputs=inputs, arguments=arguments, includes = self.files)
    
            