#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component

from weaver.function import PythonFunction
from weaver.abstraction import Map

def rename_seq( tab_rename_file, old_sequences_file, new_sequences_file ):
    """
     @summary: Write a new_sequences_file where the sequences ID will be replaced by the new names in tab_rename_file.
      @param tab_rename_file : this file contains correspondence between old and new name for each sequence. Syntax of each line : old_name<tab>new_name.
      @param old_sequences_file : file to rename.
      @param new_sequences_file : file with new IDs.
    """
    import jflow.seqio as seqio

    # Retrieve new name
    rename = dict()
    names = open( tab_rename_file )
    for line in names :
        line = line.rstrip()
        old_name, new_name = line.split("\t")
        rename[old_name] = new_name
    names.close()

    # Open output
    out_fh = ""
    if new_sequences_file.endswith(".gz"):
        out_fh = seqio.xopen(new_sequences_file, "w")
    else:
        out_fh = open(new_sequences_file, "w")

    reader = seqio.SequenceReader( old_sequences_file )
    # If sequences file is a FASTQ
    if reader.__class__.__name__ == "FastqReader":
        for id, desc, seq, qual in reader:
            # If a new name exists
            if rename.has_key(id) :
                seqio.writefastq(out_fh, [[rename[id], desc, seq, qual]])
            # If there haven't new name for the sequence
            else:
                seqio.writefastq(out_fh, [[id, desc, seq, qual]])
    # If sequences file is a FASTA
    elif reader.__class__.__name__ == "FastaReader":
        for id, desc, seq, qual in reader:
            # If a new name exists
            if rename.has_key(id) :
                seqio.writefasta(out_fh, [[rename[id], desc, seq, qual]])
            # If there haven't new name for the sequence
            else:
                seqio.writefasta(out_fh, [[id, desc, seq, qual]])
    else:
        raise "File '" + old_sequences_file + "' cannot be processed in function because it is not in right format (FATSA or FASTQ)."

class RenameSeq (Component):
    """
     @summary: Change IDs in Fasta or Fastq file.
    """

    def define_parameters(self, sequences_files, rename_rules):
        """
         @param sequences_files : [list] Path to files to rename.
         @param rename_rules : [str] This file contains correspondence between old and new name for each sequence. Syntax of each line : old_name<tab>new_name.
        """
        self.add_input_file_list( "sequences_files", "Files to rename.", default=sequences_files, required=True )
        self.add_input_file( "rename_rules", "The file which contains correspondence between old and new name for each sequence. Syntax of each line : old_name<tab>new_name.", default=rename_rules, file_format="tsv", required=True )
        self.add_output_file_list( "output_files", "Sequences files after process.", pattern="{basename}", items=sequences_files, file_format=sequences_files.file_format )
        

    def process(self):
        self.add_python_execution(rename_seq, cmd_format='{EXE} ' + self.rename_rules + ' {IN} {OUT} ',
                                  inputs=self.sequences_files, outputs=self.output_files, includes=self.rename_rules , map=True)
        