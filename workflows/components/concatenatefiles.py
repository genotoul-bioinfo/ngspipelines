#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, types

from jflow.component import Component


class ConcatenateFiles (Component):
    """
      @summary : Concatenates files.
    """

    def define_parameters(self, input_files, output_name, wext=False):
        """
         @param input_files : [list] Paths to the files that will be concatenated.
         @param output_name : [str] Filename for the output file (with extension : see param wext).
         @param wext : [bool] True if the parameter output_name contains all extensions for the final file. 
                        Otherwise th extensions are automatically retrieved from the input files.
        """
        # Parameters
        self.add_parameter( "output_name", "Filename for the output file (with extension : see param wext).", default=output_name, required=True )
        self.add_parameter( "wext", "True if the parameter output_name contains all extensions for the final file. Otherwise th extensions are automatically retrieved from the input files.", default=wext, type="bool")
        final_name = output_name
        if not wext:
            extensions = os.path.basename(input_files[0]).split(".")[1:]
            final_name = output_name + ".".join( extensions )

        # Files
        self.add_input_file_list( "input_files", "Files that will be concatenated.", default=input_files, required=True )
        out_format = input_files.file_format if hasattr(input_files, 'file_format') else "any"
        self.add_output_file( "output_file", "The concatenated file.", filename=final_name, file_format=out_format )

    def process(self):
        files_list_str = " ".join( self.input_files )
        # If the files are not zip
        if not self.output_file.endswith(".gz"):
            self.add_shell_execution('cat ' + files_list_str + ' > $1', cmd_format='{EXE} {OUT}', outputs=self.output_file, includes=self.input_files)
        # If the files are zip
        else:
             self.add_shell_execution('zcat ' + files_list_str + ' | gzip - > $1', cmd_format='{EXE} {OUT}', outputs=self.output_file, includes=self.input_files)
