#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import pickle

from jflow.component import Component


def add_analysis(analysis_name, analysis_type, analysis_hidden, soft_name, soft_parameters, soft_version, comments, mode, project_parameter,*files):
    import pickle
    # add the analysis
    analysis = project_parameter.add_analysis(analysis_name=analysis_name, analysis_type=analysis_type, analysis_hidden=analysis_hidden,
                                    soft_name=soft_name, soft_parameters=soft_parameters, soft_version=soft_version, comments=comments, mode=mode)
    # and save the result files 
    for file in files:
        analysis.add_result_file(file,mode=mode)
        
def biomart_load(biomart_table, load_parameters):

    # load the table
    biomart_table.load(*(load_parameters))
    biomart_table.save()
    return biomart_table 

class BiomartLoad (Component):
    
    def define_parameters(self, biomart_table ,project_table, analysis_name=None, analysis_type=None, analysis_hidden=None, soft_name=None, 
                          soft_parameters=None, soft_version=None, comments=None, files=[], mode="gz", load_parameters=[], wait_for=[]):
        #self.biomart_table = biomart_table
        self.add_input_object("biomart_table", "The table to load ", default=biomart_table)
        self.add_input_object("project_table", "The table to add analysis ", default=project_table)
        self.add_input_file_list("files", "Which files should be added to the analysis", default=files)
        self.add_parameter("analysis_name", "The name of the processed analysis", default=analysis_name)
        self.add_parameter("analysis_type", "The type of the processed analysis", default=analysis_type)
        self.add_parameter("analysis_hidden", "Should this analysis stay hidden", default=analysis_hidden)
        self.add_parameter("soft_name", "What software was used to perform the analysis", default=soft_name)
        self.add_parameter("soft_parameters", "What software parameters were used to perform the analysis", default=soft_parameters)
        self.add_parameter("soft_version", "What software version was used to perform the analysis", default=soft_version)
        self.add_parameter("comments", "Add some comments on the analysis", default=comments)
        self.add_parameter("mode", "Zip mode for files in analysis directory", default=mode)
        self.add_input_object("load_parameters", "The parameter to use to load the table", default=load_parameters)
        self.add_parameter_list("wait_for", "Which files should the analysis wait for", default=wait_for)
        self.add_output_object("new_table", "The new table after load")
        
    def process(self):

        # add the biomart_load command lines to the make
        waitff = []
        waitff.extend(self.wait_for)
        waitff.extend(self.files)
        
        self.add_python_execution(biomart_load,
                                  inputs=[self.biomart_table,self.load_parameters], outputs=self.new_table, includes=waitff)
        
        # if analysis is defined add the add_analysis command lines to the make
        
        if self.analysis_name:
            inputs = [self.project_table]
            inputs.extend(self.files)
            self.add_python_execution(add_analysis, arguments=["'"+str(self.analysis_name)+"'", 
                                                        "'"+str(self.analysis_type)+"'", "'"+str(self.analysis_hidden)+"'", "'"+str(self.soft_name)+"'", 
                                                        "'"+str(self.soft_parameters)+"'", "'"+str(self.soft_version)+"'", "'"+str(self.comments)+"'",  "'"+str(self.mode)+"'"], 
                                        inputs=inputs)
           