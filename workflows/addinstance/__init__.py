#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import stat
import tempfile
import subprocess
import shutil
import re

from jflow.workflow import Workflow
from ngspipelines.config_reader import NGSPipelinesConfigReader
from ngspipelines.project import Project

class AddInstance (Workflow):
    
    def get_description(self):
        return "Create a brand new ngspipelines instance"

    def define_parameters(self, function="process"):
        self.add_parameter("instance_name", "Which is the name of the instance", default="default", 
                           required=True, type="instance_already_exists")
        self.add_parameter("instance_memory", "Instance allocated memory in megabytes [1024]", default=1024, type="int", flag="--mem")
        self.add_parameter("instance_port", "HTTP deployment port [9000]", default=9000, type="int", flag="--port")
        self.add_parameter("instance_url", "HTTP public url", flag="--url")
    
    def process(self):
        
        # 1- create the instance directory
        ngs_reader = NGSPipelinesConfigReader()
        
        instance_directory = ngs_reader.get_instance_directory(self.instance_name)
        os.makedirs(instance_directory, 0751)
        biomart_release = tempfile.mkdtemp()
        try :
            # 2- download biomart sources
            subprocess.check_call( self.get_exec_path("git") + ' clone ' + self.get_resource("biomart_git") + ' ' + biomart_release , shell = True)
            
            # 3- compile biomart in a temporary directory
            subprocess.check_call( self.get_exec_path("ant") + ' -f ' + os.path.join( biomart_release, 'build.xml') , shell = True)
            
            # 4- copy the dist directory
            subprocess.check_call( self.get_exec_path("cp") + ' -r ' + os.path.join( biomart_release, 'dist') + '   ' +  instance_directory, shell = True)
            
            # 5- install ngspipelines plugin directory
            CURRENT_DIR = os.path.dirname(os.path.abspath(os.path.expanduser(__file__)))
            NGSPIPELINES_PLUGIN = os.path.join( CURRENT_DIR , '..', '..', 'biomart-plugin', 'ngspipelines')
            NGSPIPELINES_DIST = os.path.join( instance_directory , 'dist')
            NGSPIPELINES_DIST_SCRIPT = os.path.join( NGSPIPELINES_DIST , 'scripts', os.path.basename(self.get_exec_path("DEG.R")))
            NGSPIPELINES_DIST_PLUGIN = os.path.join(NGSPIPELINES_DIST, 'plugins' )
            registry_basename = os.path.basename(instance_directory)
            
          
            subprocess.check_call( self.get_exec_path("cp") + ' -r ' + os.path.join( CURRENT_DIR , '..', '..', 'biomart-plugin', 'theme-rc6/*') + '  ' + os.path.join( NGSPIPELINES_DIST, 'web') , shell = True)
            subprocess.check_call( self.get_exec_path("cp") + ' -r ' + NGSPIPELINES_PLUGIN + '  ' + NGSPIPELINES_DIST_PLUGIN , shell = True)
            #plugin ngspipelines :  storagedir
            subprocess.check_call( self.get_exec_path("sed") + " -i -e 's,^storage_directory\s*=.*,storage_directory=" + os.path.join(instance_directory, Project.DIRECTORIES_STRUCTURE) + ",'    " + os.path.join( NGSPIPELINES_DIST_PLUGIN, 'ngspipelines', 'ngspipelines.properties') , shell = True)
            #plugin ngspipelines :  path to formatdb and blastall
            subprocess.check_call( self.get_exec_path("sed") + " -i -e 's,^exec.blastn\s*=.*,exec.blastn=" + self.get_exec_path("blastn")+",' " + os.path.join( NGSPIPELINES_DIST_PLUGIN, 'ngspipelines', 'ngspipelines.properties') , shell = True)
            subprocess.check_call( self.get_exec_path("sed") + " -i -e 's,^exec.tblastn\s*=.*,exec.tblastn=" + self.get_exec_path("tblastn")+ ",' " + os.path.join( NGSPIPELINES_DIST_PLUGIN, 'ngspipelines', 'ngspipelines.properties') , shell = True)
            subprocess.check_call( self.get_exec_path("sed") + " -i -e 's,^exec.blast_formatter\s*=.*,exec.blast_formatter=" + self.get_exec_path("blast_formatter")+ ",' " + os.path.join( NGSPIPELINES_DIST_PLUGIN, 'ngspipelines', 'ngspipelines.properties') , shell = True)
            #configure EdgeR and GoEnrichisment
            subprocess.check_call( self.get_exec_path("cp") + " " + self.get_exec_path("DEG.R") + " "+ NGSPIPELINES_DIST_SCRIPT , shell = True) 
            subprocess.check_call( self.get_exec_path("sed") + " -i -e 's,^exec.DEAscript\s*=.*,exec.DEAscript=" + NGSPIPELINES_DIST_SCRIPT+ ",' " + os.path.join( NGSPIPELINES_DIST_PLUGIN, 'ngspipelines', 'ngspipelines.properties') , shell = True)
            subprocess.check_call( self.get_exec_path("sed") + " -i -e 's,^exec.rscript\s*=.*,exec.rscript=" + self.get_exec_path("Rscript")+ ",' " + os.path.join( NGSPIPELINES_DIST_PLUGIN, 'ngspipelines', 'ngspipelines.properties') , shell = True)
            
            #configure biomart.properties
            subprocess.check_call( self.get_exec_path("sed") + " -i -e 's,^biomart\.registry\.key\.file,#biomart.registry.key.file,' " + os.path.join( NGSPIPELINES_DIST, 'biomart.properties'), shell = True)
            subprocess.check_call( self.get_exec_path("sed") + " -i -e 's,^http\.host\s*=.*,http.host = 0.0.0.0,' " + os.path.join( NGSPIPELINES_DIST, 'biomart.properties'), shell = True)
            subprocess.check_call( self.get_exec_path("sed") + " -i -e 's,^http\.port\s*=.*,http.port = " + str(self.instance_port) + ",' " + os.path.join( NGSPIPELINES_DIST, 'biomart.properties'), shell = True)
            subprocess.check_call( self.get_exec_path("sed") + " -i -e 's,^biomart\.registry\.file.*,biomart.registry.file = " + os.path.join( 'registry', registry_basename +'.xml' ) + ",'  " + os.path.join( NGSPIPELINES_DIST, 'biomart.properties'), shell = True)
            subprocess.check_call( self.get_exec_path("sed") + " -i -e 's,^session\.connection.*,session.connection = jdbc:mysql://{host}:{port}/biomart_sessions?user={user}\&password={passwd},'  {propfile}".format( 
                                                                                  propfile = os.path.join( NGSPIPELINES_DIST, 'biomart.properties'), **ngs_reader.get_database_info() ), shell = True)
            #create link data
            subprocess.check_call( self.get_exec_path("ln") + ' -f -s ' + os.path.join( instance_directory , 'data') + ' ' + os.path.join( NGSPIPELINES_DIST_PLUGIN, 'ngspipelines', 'public','ngspipelines' ) , shell = True)
            if self.instance_url :
                url = re.sub(r':\d+$', '', re.sub( r'\/$', '',  str(self.instance_url))) + ':' + str(self.instance_port)
                if not url.startswith("http") :
                    url = "http://"+url
                subprocess.check_call( self.get_exec_path("sed") + " -i -e 's,^#http\.url\s*=.*,http.url = " + url + ",' " + os.path.join( NGSPIPELINES_DIST, 'biomart.properties'), shell = True)
            else :
                subprocess.check_call( self.get_exec_path("sed") + " -i -e 's,^#http\.url\s*=.*,http.url = http://localhost:9000/,' " + os.path.join( NGSPIPELINES_DIST, 'biomart.properties'), shell = True)
            subprocess.check_call( self.get_exec_path("sed") + " -i -e 's,DEFAULT_MAX_HEAP=.*,DEFAULT_MAX_HEAP=\"" + str(self.instance_memory) +  "m\",' " + os.path.join( NGSPIPELINES_DIST, 'scripts', 'biomart-server.sh'), shell = True)
        finally :
            subprocess.check_call( self.get_exec_path("rm") + ' -rf ' + biomart_release , shell = True)
        