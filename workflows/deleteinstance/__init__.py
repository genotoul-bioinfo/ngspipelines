#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import os
import shutil

from jflow.workflow import Workflow
from ngspipelines.config_reader import NGSPipelinesConfigReader
from ngspipelines.project import Project

class DeleteInstance (Workflow):
    
    def get_description(self):
        return "Delete a ngspipelines instance and all linked projects"

    def define_parameters(self, function="process"):
        self.add_parameter("instance_name", "Which is the name of the instance", required=True, type="instance_exists")
    
    def process(self):
        ngs_reader = NGSPipelinesConfigReader()
        instance_directory = ngs_reader.get_instance_directory(self.instance_name)
        try:
            for dir in os.listdir(os.path.join(instance_directory, "data")):
                project = Project.get_from_directory_name(dir)
                project.delete()
        except: pass
        # delete the directories
        try:
            shutil.rmtree(instance_directory)
        except: pass