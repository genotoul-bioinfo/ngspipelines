#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import argparse
import datetime

from ngspipelines.config_reader import NGSPipelinesConfigReader


def date(datestr):
    try:
        return datetime.datetime.strptime(datestr, JFlowConfigReader().get_date_format())
    except:
        raise argparse.ArgumentTypeError("'" + datestr + "' is an invalid date!")

def project(project_name):
    if "_" in project_name:
        raise argparse.ArgumentTypeError("The project name must not contain '_'. Please provide a new project name!")
    return project_name
    
def instance_exists(instance_name):
    ngs_reader = NGSPipelinesConfigReader()
    instance_directory = ngs_reader.get_instance_directory(instance_name)
    if not os.path.isdir(instance_directory): 
        raise argparse.ArgumentTypeError("The specified instance '" + instance_name + "' does not exist! " + \
                                         "Please use the sub command addinstance before or select an existing instance.")
    return instance_name

def instance_already_exists(instance_name):
    ngs_reader = NGSPipelinesConfigReader()
    instance_directory = ngs_reader.get_instance_directory(instance_name)
    if os.path.isdir(instance_directory): 
        raise argparse.ArgumentTypeError("The specified instance '" + instance_name + "' already exists! " + \
                                         "Please use a different name for this instance.")
    return instance_name