#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import uuid
import os
import shutil
from MySQLdb import *

from ngspipelines.biomart_db import *
from ngspipelines.analysis import Analysis, Result
from ngspipelines.library import Library


class Project(BiomartMain):
    """
    """
    
    JBROWSE_DIRECTORY = "jbrowse"
    RAW_DIRECTORY = "raw"
    ANALYSIS_DIRECTORY = "analysis"
    PROJECT_TABLES = [ 'Project', 'Library', 'Analysis', 'Result']
    DIRECTORIES_STRUCTURE = "data/"
    
    
    def __init__(self, current_application=None, create=True, **args):
        """ 
        Project object builder
        """
        BiomartMain.__init__(self, self, create=create, **args)
        self.current_application = current_application
        # overwrite the directory attribute and add the project, if this one is not already in the database
        if not args.has_key("id"):
            self.directory = BiomartVARCHARField("directory", size=255, input=self.__create_directories())
            self.id = self.add()
        self.libraries = []
        self.biomart_tables = []
        # create main tables
        Library(self, create=create, categorie=self.current_application)
        Analysis(self, create=create, categorie=self.current_application)
        Result(self, create=create, categorie=self.current_application)
    
    @staticmethod
    def exists(project_name):
        exists = True
        ngs_reader = NGSPipelinesConfigReader()
        db_info = ngs_reader.get_database_info()
        req = "SELECT * FROM `" + project_name + "_project__Project__main`"
        try:
            conn = connect(db_info["host"], db_info["user"], 
                           db_info["passwd"], db_info["dbname"])
        except OperationalError, e:
            sys.stderr.write("Error: An error occurred when attempting to connect to the database, please check your database connection information!\n"+str(e)+"\n")
            sys.exit(1)
        curs = conn.cursor()
        try:
            curs.execute(req)
        except:
            exists = False
        curs.close()
        conn.close()
        return exists
    
    @staticmethod
    def get_from_name(project_name):
        try:
            # load the project itself
            ngs_reader = NGSPipelinesConfigReader()
            db_info = ngs_reader.get_database_info()
            req = "SELECT project_id_key, name, directory, species, species_common_name, description, instance_name FROM `" + project_name + "_project__Project__main`"
            conn = connect(db_info["host"], db_info["user"], 
                           db_info["passwd"], db_info["dbname"])
            curs = conn.cursor()
            curs.execute(req)
            res = curs.fetchall()[0]
            curs.close()
            conn.close()
            project = Project(instance_name=res[6], create=False, name=res[1], species=res[3], directory=res[2], species_common_name=res[4], description=res[5], id=res[0])
            # then libraries
            project.libraries = Library.get_from_project(project)
            project.biomart_tables.append(Library(project))
            # and analysis
            project.biomart_tables.append(Analysis(project))
            project.biomart_tables.append(Result(project))
            # finaly biomart tables
            req = 'SHOW TABLES FROM ' + db_info["dbname"] + ' LIKE "' + project_name + '_%"'
            conn = connect(db_info["host"], db_info["user"], 
                           db_info["passwd"], db_info["dbname"])
            curs = conn.cursor()
            curs.execute(req)
            res = curs.fetchall()
            curs.close()
            conn.close()
            for table in res:
                if BiomartTable.get_class_name_from_table_name(table[0]) not in Project.PROJECT_TABLES:
                    project.biomart_tables.append(BiomartTable.get_from_table_name(project, table[0]))
            return project
        except: return None
    
    @staticmethod
    def get_from_directory_name(directory_name):
        ngs_reader = NGSPipelinesConfigReader()
        db_info = ngs_reader.get_database_info()
        req = "SHOW TABLES FROM " + db_info["dbname"] + " LIKE \"%_project__Project__main\""
        conn = connect(db_info["host"], db_info["user"], 
                       db_info["passwd"], db_info["dbname"])
        curs = conn.cursor()
        curs.execute(req)
        results = curs.fetchall()
        for res in results:
            req = "SELECT name, directory FROM `" + res[0] + "`"
            conn = connect(db_info["host"], db_info["user"], 
                           db_info["passwd"], db_info["dbname"])
            curs = conn.cursor()
            curs.execute(req)
            result = curs.fetchall()[0]
            curs.close()
            conn.close()
            if result[1] == directory_name: return Project.get_from_name(result[0])
        return None
    
    def add_image(self, image_path):
        if os.path.isfile(image_path):
            shutil.copyfile(image_path, os.path.join(self.get_full_path_directory(), "project.png"))
    
    def get_biomart_table(self, biomart_class):
        for bt in self.biomart_tables:
            if bt.__class__ == biomart_class:
                return bt
        return None
    
    def add_biomart_table(self, biomart_class):
        bt = self.get_biomart_table(biomart_class)
        if bt == None:
            bi = biomart_class(self, create=True)
            self.biomart_tables.append(bi)
            return bi
        return bt
    
    def add_analysis(self, **args):
        a = Analysis(self, categorie=self.current_application, **args)
        return a

    def __add_display_graph(self, chart_title, graph_type, labels, values, x_title="", y_title="", chart_subtitle="", tooltip=None, categorie=None, x_type=None, y_type=None, **args):
        if categorie == None:
            categorie=self.current_application
        a = Analysis(self, categorie=categorie, analysis_type="display_graph", analysis_hidden="1", **args)
        a.add_result_element("type", graph_type, "STRING")
        a.add_result_element("x_title", x_title, "STRING")
        a.add_result_element("y_title", y_title, "STRING")
        a.add_result_element("chart_title", chart_title, "STRING")
        a.add_result_element("chart_subtitle", chart_subtitle, "STRING")
        a.add_result_element("labels", ",".join(map(str, labels)), "STRING")
        if type(values) is dict:
            for series in values:
                a.add_result_element("values", ",".join(map(str, values[series])), "STRING", series)
        else:
            a.add_result_element("values", ",".join(map(str, values)), "STRING", "no-name")
        if tooltip != None:
            a.add_result_element("tooltip", tooltip, "STRING")
        # Line graph specific
        if x_type != None:
            a.add_result_element("x_type", x_type, "STRING")
        if y_type != None:
            a.add_result_element("y_type", y_type, "STRING")
        return a
    
    # Labels displayed as they are provided
    def add_histogram(self, chart_title, labels, values, x_title="", y_title="", chart_subtitle="", tooltip=None, categorie=None, **args):
        return self.__add_display_graph( chart_title, "histogram", labels, values, x_title, y_title, chart_subtitle, tooltip, categorie, **args)
    
    # Labels recomputed (labels not continuous)
    def add_column(self, chart_title, labels, values, x_title="", y_title="", chart_subtitle="", tooltip=None, categorie=None, **args):
        return self.__add_display_graph( chart_title, "column", labels, values, x_title, y_title, chart_subtitle, tooltip, categorie, **args)
    
    def add_bargraph(self, chart_title, labels, values, x_title="", y_title="", chart_subtitle="", tooltip=None, categorie=None, **args):
        return self.__add_display_graph( chart_title, "bargraph", labels, values, x_title, y_title, chart_subtitle, tooltip, categorie, **args)

    def add_basicline(self, chart_title, labels, values, x_title="", y_title="", chart_subtitle="", tooltip=None, categorie=None, x_type=None, y_type=None, **args):
        return self.__add_display_graph( chart_title, "basicline", labels, values, x_title, y_title, chart_subtitle, tooltip, categorie, x_type, y_type, **args)

    def add_stacked_histogram(self, chart_title, labels, values, x_title="", y_title="", chart_subtitle="",tooltip=None, categorie=None, **args):
        return self.__add_display_graph( chart_title, "stacked_histogram", labels, values, x_title, y_title, chart_subtitle, tooltip, categorie, **args)

    def add_piechart(self, chart_title, labels, values, chart_subtitle="", tooltip=None, categorie=None, **args):
        return self.__add_display_graph( chart_title, "piechart", labels, values, None, None, chart_subtitle, tooltip, categorie, **args)
      
    def add_scatterplot(self, chart_title, x_values, y_values, x_title="", y_title="", labels=[], chart_subtitle="", tooltip=None, categorie=None, **args):
       
        if categorie == None:
            categorie=self.current_application
        a = Analysis(self, categorie=categorie, analysis_type="display_graph", analysis_hidden="1", **args)
        a.add_result_element("type", "scatterplot", "STRING")
        a.add_result_element("chart_title", chart_title, "STRING")
        a.add_result_element("chart_subtitle", chart_subtitle, "STRING")
        # TODO : check identical length of lists
        if isinstance(x_values, list) and  isinstance(y_values[0], list) and isinstance(labels, list) and len(labels) > 0 :
            for i in range(len(y_values)):
                a.add_result_element("y_values", ",".join(map(str, y_values[i])), "STRING",labels[i])
            a.add_result_element("x_values", ",".join(map(str, x_values)), "STRING")
            #a.add_result_element("labels_values", ",".join(map(str, labels)), "STRING")
        else :
            a.add_result_element("x_values", ",".join(map(str, x_values)), "STRING")
            a.add_result_element("y_values", ",".join(map(str, y_values)), "STRING")
        a.add_result_element("x_title", x_title, "STRING")
        a.add_result_element("y_title", y_title, "STRING")
        if tooltip != None:
            a.add_result_element("tooltip", tooltip, "STRING")
        return a

    def add_donutchart(self, chart_title, labels_up, values_up, dict_sub_label_group={}, drilldown=False, chart_subtitle="", categorie=None, tooltip=None, comments=None, **args):
        """ 
        @param labels_up : string of labels (separated by comma) for the center of the donnut
        @param values_up : string of values (separated by comma) for the center of the donnut
        @param dict_sub_label_group : dict with key corresponding to the list of up label and an array of sub section [label,values]
        @param drilldown : boolean to specify if donut must display drilldown
        """
        if categorie == None:
            categorie=self.current_application
        a = Analysis(self, categorie=categorie, analysis_type="display_graph", analysis_hidden="1", **args)
        if drilldown:
            a.add_result_element("type", "donutchart_drilldown", "STRING")
        else:
            a.add_result_element("type", "donutchart", "STRING")
        a.add_result_element("chart_title", chart_title, "STRING")
        a.add_result_element("chart_subtitle", chart_subtitle, "STRING")
        a.add_result_element("labels", labels_up, "STRING")
        a.add_result_element("values", values_up, "STRING")
        for key,values in dict_sub_label_group.items() : 
            a.add_result_element("labels", values[0], "STRING",group=key)
            a.add_result_element("values", values[1], "STRING",group=key)
        if tooltip != None:
            a.add_result_element("tooltip", tooltip, "STRING")
        if comments != None:
            a.add_result_element("comments", tooltip, "STRING")

        return a
    def add_table(self, chart_title, rows, header=None, comments="", chart_subtitle="", categorie=None, **args):        
        if categorie == None:
            categorie=self.current_application
        a = Analysis.get_from_name(self, args["analysis_name"], categorie)
        if a == None:
            a = Analysis(self, categorie=categorie, analysis_type="display_graph", analysis_hidden="1", comments=comments, **args)
            a.add_result_element("type", "table", "STRING")
            a.add_result_element("chart_title", chart_title, "STRING")
            a.add_result_element("chart_subtitle", chart_subtitle, "STRING")
            if header:
                a.add_result_element("header", ",".join(map(str, header)), "STRING")
        for row in rows:
            a.add_result_element("row_"+str(uuid.uuid4().hex[:10]), ",".join(map(str, row)), "STRING")
        return a
    
    def add_library(self, **args):
        l = Library(self, categorie=self.current_application, **args)
        self.libraries.append(l)
        return l
    
    def get_max_library_size(self):
        max=0
        for l in self.libraries :
           if l.nb_sequence > max :
                max = l.nb_sequence
        return max
    
    def delete(self):
        BiomartMain.delete(self)
        for biomart_table in self.biomart_tables:
            biomart_table.delete()
        # delete the directories
        try:
            shutil.rmtree(self.get_full_path_directory())
        except: pass
    
    def get_full_path_directory(self):
        return os.path.join(self.get_instance_directory(), Project.DIRECTORIES_STRUCTURE, self.directory)
    
    def get_full_path_work_directory(self):
        return os.path.join(self.ngs_reader.get_work_instance_directory(self.instance_name), Project.DIRECTORIES_STRUCTURE, self.directory)
    
    def get_instance_directory(self):
        return self.ngs_reader.get_instance_directory(self.instance_name)
    
    def get_xml_path(self):
        return os.path.join(self.ngs_reader.get_work_instance_directory(self.instance_name), "dist", "registry", self.instance_name + '.xml')
    
    def define_fields(self):
        self.name = BiomartVARCHARField("name", size=255)
        self.directory = BiomartVARCHARField("directory", size=255)
        self.species = BiomartVARCHARField("species", size=255)
        self.species_common_name = BiomartVARCHARField("species_common_name", size=255)
        self.description = BiomartVARCHARField("description", size=255)
        self.instance_name = BiomartVARCHARField("instance_name", size=255)

    def __create_directories(self):
        instance_dir = self.ngs_reader.get_instance_directory(self.instance_name)
        pdir = os.path.join(uuid.uuid4().hex[:10])
        while True:
            pdirp = os.path.join(instance_dir, Project.DIRECTORIES_STRUCTURE, pdir) 
            if not os.path.isdir(pdirp):
                break
            pdir = os.path.join(uuid.uuid4().hex[:10])
        os.makedirs(pdirp, 0751)
        os.makedirs(os.path.join(pdirp, self.JBROWSE_DIRECTORY), 0751)
        os.makedirs(os.path.join(pdirp, self.RAW_DIRECTORY), 0751)
        os.makedirs(os.path.join(pdirp, self.ANALYSIS_DIRECTORY), 0751)
        return pdir
