#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import datetime
import subprocess
import uuid
import os
import time    
import shutil 
from MySQLdb import *

from jflow.component import Component

from ngspipelines.biomart_db import *
from ngspipelines.config_reader import NGSPipelinesConfigReader
from ngspipelines.utils import Utils

class Analysis(BiomartMain):

    def __init__(self, project, **args):
        BiomartMain.__init__(self, project, **args)
        # if this is an analysis already in the database
        # should load the entier object, here working only to add_result_element
        if args.has_key("id"):
            self.id = args["id"]
        # if this is not an analysis already in the database and if this is not to create the table
        elif not args.has_key("id") and not args.has_key("create"):
            self.__create_analysis()
    
    def __create_analysis(self):
        # find a random directory for this analysis
        self.directory = BiomartVARCHARField("directory", size=64, input=self.__create_directory())
        self.time = BiomartVARCHARField("time", input = time.strftime('%Y-%m-%d %H:%M:%S'))        
        self.id = self.add()
    
    def get_full_path_work_directory(self):
        return os.path.join(self.project.get_full_path_work_directory(), "analysis", self.directory)
    
    def define_fields(self):
        self.analysis_name = BiomartVARCHARField("analysis_name", size=255)
        self.analysis_type = BiomartVARCHARField("analysis_type", size=64)
        self.analysis_hidden = BiomartVARCHARField("analysis_hidden", size=64)
        self.soft_name = BiomartVARCHARField("soft_name", size=64)
        self.soft_parameters = BiomartTEXTField("soft_parameters")
        self.soft_version = BiomartVARCHARField("soft_version", size=64)
        #self.dbname = BiomartVARCHARField("dbname", size=255)
        #self.dbversion = BiomartVARCHARField("dbversion", size=64)
        self.time = BiomartDATETIMEField("time")
        self.categorie = BiomartVARCHARField("categorie", size=64)
        self.directory = BiomartVARCHARField("directory", size=64)
        self.comments = BiomartTEXTField("comments")
        self.parent_id = BiomartINTField("parent_id", size=11)

    def __create_directory(self):
        adir = uuid.uuid4().hex[:10]
        while True:
            adirp = os.path.join(self.project.get_full_path_work_directory(), "analysis", adir) 
            if not os.path.isdir(adirp):
                break
            adir = uuid.uuid4().hex[:9]
        os.makedirs(adirp, 0751)
        return adir
    
    @staticmethod
    def get_from_name(project, analysis_name, categorie):
        try:
            ngs_reader = NGSPipelinesConfigReader()
            db_info = ngs_reader.get_database_info()
            req = "SELECT analysis_id_key FROM `" + project.name + "_analysis__Analysis__main` WHERE analysis_name='" + analysis_name + "' and categorie ='" + categorie + "'"
            conn = connect(db_info["host"], db_info["user"], 
                           db_info["passwd"], db_info["dbname"])
            curs = conn.cursor()
            curs.execute(req)
            res = curs.fetchall()[0]
            curs.close()
            conn.close()
            analysis = Analysis(project, id=res[0])
            return analysis
        except: return None
    
    def add_result_element(self, key, value, type, group="default"):
        result = Result(self.project, key=key, value=value, type=type, group=group)
        result.add(int(self.id))
    
    def add_result_file(self, file, file_name=None, mode="gz"):
        """
        add a file in the analysis directory
          @param file         : the path to the file to link
          @param file_name    : the file name to use to store the image
          @mparam mode         : how to zip file
        """
        if os.path.isfile(file) :
            if file_name == None: file_name = os.path.basename(file)
            if mode == "gz" and  os.path.splitext(file)[1] not in Utils.DO_NOT_COMPRESS_EXTENSION:
                file_gzipped = Utils.gzip(file, self.get_full_path_work_directory(), file_name)
                file_name = os.path.basename(file_gzipped)
            else : 
                shutil.copyfile(file, os.path.join(self.get_full_path_work_directory(), file_name))
            
            self.add_result_element("file", file_name, 'FILE')
    
    def get_version(self):
        """ 
        Return the tool version, has to be implemented by subclasses
        """
        raise NotImplementedError
    
    def define_analysis(self):
        """ 
        Define all analysis attributs, has to be implemented by subclasses
        """
        raise NotImplementedError
        
        
class Result(BiomartDimension):

    def define_fields(self):
        self.key = BiomartVARCHARField("key", size=255)
        self.type = BiomartVARCHARField("type", size=64)
        self.group = BiomartVARCHARField("group", size=64)
        self.value = BiomartTEXTField("value")
    