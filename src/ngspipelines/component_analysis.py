#
# Copyright (C) 2014 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from subprocess import Popen, PIPE
import pickle
from jflow.component import Component
              
def add_analysis(analysis_path):
    import pickle
    
    # load the analysis object
    analysis_dump = open(analysis_path, "rb")
    analysis_cpt = pickle.load(analysis_dump)
    analysis_dump.close()

    # process the parsing of the analysis
    analysis_cpt.post_process()

    # add the analysis
    analysis = analysis_cpt.project.add_analysis(analysis_name=analysis_cpt.analysis_name, analysis_type=analysis_cpt.analysis_type, 
                                                 analysis_hidden=analysis_cpt.analysis_hidden, soft_name=analysis_cpt.soft_name, 
                                                 soft_parameters=analysis_cpt.soft_parameters, soft_version=analysis_cpt.soft_version, 
                                                 comments=analysis_cpt.comments)
    
    for key in analysis_cpt.results.keys():
        for reslt in analysis_cpt.results[key]:
            analysis.add_result_element(key, reslt[0], reslt[1], reslt[2])
    
    for file in analysis_cpt.files:
        analysis.add_result_file(file,mode=analysis_cpt.mode)

class ComponentAnalysis (Component):
    """
     @summary : JFlow Component which add automaticaly an analysis.
    """
    def __init__(self, name="", comments="",analysis_hidden="1",analysis_type="", software="",
                 parameters="", version="", mode="gz"):
        """
        (analysis_name, analysis_type, analysis_hidden, soft_name, soft_parameters, soft_version, comments, project_parameter,*files):
        
        """
        Component.__init__(self)
        self.analysis_name =name
        self.analysis_type = "cpta_" + self.__class__.__name__
        self.analysis_hidden = 0
        self.comments = comments
        self.soft_name = software
        self.soft_parameters = parameters
        self.soft_version = self.get_version()
        self.project = None
        self.mode = mode
        self.results = {}
        self.files = []
       
    def execute(self):
        # first create the output directory
        if not os.path.isdir(self.output_directory):
            os.makedirs(self.output_directory, 0751)

        # then add analysis information
        self.define_analysis()

        # serialized the object
        analysis_dump_path = self.get_temporary_file(".dump")
        analysis_dump = open(analysis_dump_path, "wb")
        pickle.dump(self, analysis_dump)
        analysis_dump.close()
        
        wait_for_files = []
        for attr in self.__dict__:
            # TODO if no Output object raise error
            if self.__getattribute__(attr).__class__.__name__ == "OutputFile" or \
                self.__getattribute__(attr).__class__.__name__ == "OutputDirectory":
                wait_for_files.append(self.__getattribute__(attr))
            elif self.__getattribute__(attr).__class__.__name__ == "OutputFileList":
                wait_for_files.extend(self.__getattribute__(attr))
        
        # then run the component
        self.process()

        self.add_python_execution(add_analysis, inputs=analysis_dump_path, includes = self.files + wait_for_files)
                
    def _add_result_element(self, file, result_key, result_value, result_group="default"):
        """
        add the result row
          @param file         : the file name the result is linked to
          @param result_key   : the result key
          @param result_value : the result value associated to the key
          @param result_group : the result group it belongs to
        """
        if self.results.has_key(file):
            self.results[file].append([result_key, result_value, result_group])
        else :
            self.results[file] = [[result_key, result_value, result_group]]
        
    def _add_result_file(self, file):
        if (isinstance(file, list)):
            self.files.extend(file)
        else:
            self.files.append(file)
        
    def define_analysis(self):
        """ 
        Define all analysis attributs, has to be implemented by subclasses
        
        the structure of result_elements must be an array of [ key, value, type, group="default" ]
        """
        raise NotImplementedError
    
    def post_process(self):
        raise NotImplementedError( "ComponentAnalysis.post_process() must be implemented in " + self.__class__.__name__ )
    
    def get_version(self):
        raise NotImplementedError( "ComponentAnalysis.get_version() must be implemented in " + self.__class__.__name__ )

    def process(self):
        raise NotImplementedError( "ComponentAnalysis.process() must be implemented in " + self.__class__.__name__ )
    