#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import os
from MySQLdb import *

from ngspipelines.biomart_db import *
from jflow.seqio import FastqReader


def library_types(library_type):
    if library_type not in Library.AVAILABLE_TYPES:
        raise argparse.ArgumentTypeError("'" + values["type"] + "' is an invalid type for a library, please choose between: " + \
                                         ", ".join(Library.AVAILABLE_TYPES) + ".")
    return library_type

class Library(BiomartMain):
    
    AVAILABLE_TYPES = ["pe", "se", "ose", "ope", "mp"]
    PAIRED_TYPES = ["pe", "ope", "mp"]
    
    def __init__(self, project, **args):
        # if this is not a library already in the database and if this is not to create the table
        BiomartMain.__init__(self, project, **args)
        if not args.has_key("id") and not args.has_key("create"):
            self.id = self.add()

    def define_fields(self):
        self.library_name = BiomartVARCHARField("library_name", size=255, is_key=True)
        self.sample_name = BiomartVARCHARField("sample_name", size=255)
        self.replicat = BiomartINTField("replicat", size=4)
        self.tissue = BiomartVARCHARField("tissue", size=64, is_key=True)
        self.dev_stage = BiomartVARCHARField("dev_stage", size=64, is_key=True)
        self.remark = BiomartVARCHARField("remark", size=255)
        self.sequencer = BiomartVARCHARField("sequencer", size=64)
        self.type = BiomartVARCHARField("type", size=15)
        self.insert_size = BiomartINTField("insert_size", size=11)
        self.public = BiomartTINYINTField("public", size=1)
        self.nb_sequence = BiomartBIGINTField("nb_sequence", size=20)
        self.categorie = BiomartVARCHARField("categorie", size=45)
        self.database = BiomartVARCHARField("database", size=64)
        self.accession = BiomartVARCHARField("accession", size=64)
        self.files = BiomartVARCHARField("files", size=255)

    
    @staticmethod
    def get_from_project(project):
        libraries = []
        ngs_reader = NGSPipelinesConfigReader()
        db_info = ngs_reader.get_database_info()
        req = "SELECT `library_id_key`, `accession`, `categorie`, `database`, `dev_stage`, `files`, `insert_size`, `library_name`, `nb_sequence`, `public`, " + \
            "`remark`, `replicat`, `sample_name`, `sequencer`, `tissue`, `type` FROM `" + project.name + "_library__Library__main`"
        conn = connect(db_info["host"], db_info["user"], 
                       db_info["passwd"], db_info["dbname"])
        curs = conn.cursor()
        curs.execute(req)
        res = curs.fetchall()
        curs.close()
        conn.close()
        for lib in res:
            my_lib = Library(project, library_name=lib[7], sample_name=lib[12], replicat=lib[11], tissue=lib[14], dev_stage=lib[4], remark=lib[10],
                             sequencer=lib[13], type=lib[15], insert_size=lib[6], public=lib[9], nb_sequence=lib[8], categorie=[2], database=lib[3],
                             accession=lib[1], files=lib[5], create=False)
            my_lib.id = lib[0]
            libraries.append(my_lib)
        return libraries
    