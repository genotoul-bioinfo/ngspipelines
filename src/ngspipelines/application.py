#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import types
import pkgutil
import os
import sys
import inspect
import shutil

from ConfigParser import ConfigParser, NoOptionError

from jflow.workflow import Workflow
import ngspipelines
from ngspipelines.biomart_db import BiomartMain, BiomartTable
from ngspipelines.project import Project
from ngspipelines.library import library_types, Library
from ngspipelines.config_reader import NGSPipelinesConfigReader

# import custom types
from workflows.types import *


class ApplicationWithoutReferenceGenome(Workflow):
    
    def __init__(self, args={}, id=None, function= "process"):
        Workflow.__init__(self, args, id, None)
        self.function = function
        self.ngs_reader = NGSPipelinesConfigReader()
        self.db_info = self.ngs_reader.get_database_info()
        self.main_dump_path = {}
        self.biomart_table_loaded = {}
        self.nb_analysis_loaded = 0
        self._add_required_parameters()
        self.define_parameters(self.function)
        # if some args are provided, let's fill the parameters
        if args: self._set_parameters(args)
        
    @staticmethod
    def load_config_parser(arg_lines):
        for arg in arg_lines:
            yield arg
    
    def get_description(self):
        pass
        
    def define_parameters(self, function="process"):
        pass
    
    def wf_execution_wrapper(self):
        self.biomart_table_loaded = {}
        self.nb_analysis_loaded = 0
        super(ApplicationWithoutReferenceGenome, self).wf_execution_wrapper()
    
    def pre_process(self):
        self.instance_directory = self.ngs_reader.get_instance_directory(self.instance_name)
        assert self.is_not_locked() or self.unlock, "Instance "+self.instance_directory+" is locked, a pipeline already running on this instance. Use --unlock to force execution."
        self.work_dir = self.ngs_reader.get_work_instance_directory(self.instance_name)
        self._lock_instance()
        if Project.exists(self.project_name) and not self.force:
            sys.stderr.write("Error: Project '" + self.project_name + "' already exists! Please choose a different name for your project.\n")
            sys.exit(1)
        elif Project.exists(self.project_name) and self.force:
            try:
                project = Project.get_from_name(self.project_name)
                project.delete()
            except: pass
        self.project = Project(self.get_application_type(), instance_name=self.instance_name, name=self.project_name, species=self.species, species_common_name=self.species_common_name, 
                               description=self.project_description)
        if self.image: self.project.add_image(self.image)
        for values in self.library:
            if values["type"] in Library.PAIRED_TYPES :
                # check type VS nb fastq files
                if len(values["files"]) != 2 :
                    sys.stderr.write("Library " + values["library_name"] +" is defined as paired, expect 2 fastq files.\n" )
                    sys.exit(1)
                # check fastqs are not the same
                if values["files"][0] == values["files"][1] :
                    sys.stderr.write("Library " + values["library_name"] +"'s fastq files are the same.\n" )
                    sys.exit(1)
        
        self._copy_to_work()
        count_seq_libraries = self.add_component("CountSeqLibraries",[self.project, self.library])
        self._execute_weaver()
        for lib in count_seq_libraries.libraries_count :
            self.project.add_library(**lib)
        
    def add_biomart_load(self, biomart_class, load_parameters, analysis_name=None, analysis_type=None, analysis_hidden=None, soft_name=None, 
                         soft_parameters=None, soft_version=None, comments=None, files=[], mode="gz"):
        # add the table
        table_instance = self.project.add_biomart_table(biomart_class)
        
        # add a suffix to the prefix to handle multiple biomart load of the same table
        if self.biomart_table_loaded.has_key(table_instance.__class__.__name__):
            self.biomart_table_loaded[table_instance.__class__.__name__] += 1
        else: self.biomart_table_loaded[table_instance.__class__.__name__] = 1
        
        if isinstance(table_instance, BiomartMain):
            component = self.add_component("BiomartLoad",[table_instance, self.project, analysis_name, analysis_type, analysis_hidden, soft_name,
                                                          soft_parameters, soft_version, comments, files, mode, load_parameters], 
                                           component_prefix=table_instance.__class__.__name__+"_"+str(self.biomart_table_loaded[table_instance.__class__.__name__]))
            self.main_dump_path[table_instance.__class__.__name__] = component.new_table.dump_path
        else:
            # retrieve main class name from dm table
            main_table, main_primary_key = table_instance.get_main_primary_key_info()
            main_class = BiomartTable.get_class_name_from_table_name(main_table)
            if self.main_dump_path.has_key(main_class):
                component = self.add_component("BiomartLoad",[table_instance, self.project, analysis_name, analysis_type, analysis_hidden, soft_name,
                                                             soft_parameters, soft_version, comments, files, mode, load_parameters, [self.main_dump_path[main_class]]], 
                                              component_prefix=table_instance.__class__.__name__+"_"+str(self.biomart_table_loaded[table_instance.__class__.__name__]))
            else:
                sys.stderr.write("Please load a main table before loading dimension tables!\n")
                sys.exit(1)
        return component
    
    def add_analysis(self, analysis_name, analysis_type, analysis_hidden, soft_name, soft_parameters, soft_version, comments, 
                     result_elements=[], files=[], mode = "gz"):
        self.nb_analysis_loaded += 1
        
        component = self.add_component( "AddAnalysis",[self.project, analysis_name, analysis_type, analysis_hidden, soft_name,
                                                      soft_parameters, soft_version, comments, result_elements, files, mode], 
                                        component_prefix=str(self.nb_analysis_loaded) ) 
        return component
    
    def add_blast_search(self, target_class, filepath, databank_type = "nucl"):
        index = self.add_component("BlastIndex", [[filepath], [databank_type]], component_prefix="appBlastSearch")
        biomart_class = self.project.get_biomart_table( target_class )
        result_elements = [
                           ['databank', os.path.basename(index.databanks[0]), "STRING"],
                           ['target', biomart_class.__class__.__name__, "STRING"],
                           ['label', biomart_class.get_main_primary_key_info()[1], "STRING"]
                          ]
        self.add_analysis("Blast search", "blast_search", 1, "makeblastdb", "", "", "", result_elements, [index.databanks[0]] + index.index_files[0], "")
    
    
    def add_venn(self):        
        self.add_analysis("Venn analysis", "web_venn_analysis", 1, "web", "", "", "", [])
    
    def add_fisher_exact_test(self):        
        self.add_analysis("DDD analysis", "web_ddd_analysis", 1, "web", "", "", "", [])
    
    
    def add_component(self, component_name, args=[], kwargs={}, component_prefix="default"):
        # first build and check if this component is OK
        if self.internal_components.has_key(component_name) or self.external_components.has_key(component_name):
            
            if self.internal_components.has_key(component_name) :
                my_pckge = __import__(self.internal_components[component_name], globals(), locals(), [component_name], -1)
                # build the object and define required field
                cmpt_object = getattr(my_pckge, component_name)()
                cmpt_object.output_directory = self.get_component_output_directory(component_name, component_prefix)
                cmpt_object.prefix = component_prefix
                if kwargs: cmpt_object.define_parameters(**kwargs)
                else: cmpt_object.define_parameters(*args)
            # external components
            else :
                cmpt_object = self.external_components[component_name]()
                cmpt_object.output_directory = self.get_component_output_directory(component_name, component_prefix)
                cmpt_object.prefix = component_prefix
                # can't use positional arguments with external components
                cmpt_object.define_parameters(**kwargs)
                
            # if the built object is an analysis
            for derived_class in cmpt_object.__class__.__bases__:
                if derived_class.__name__ == "ComponentAnalysis":
                    cmpt_object.project = self.project
                    
            # there is a dynamic component
            if cmpt_object.is_dynamic():
                self.dynamic_component_present = True
                # if already init, add the component to the list and check if weaver should be executed
                if self.component_nameids_is_init:
                    # add the component
                    self.components_to_exec.append(cmpt_object)
                    self.components.append(cmpt_object)
                    self._execute_weaver()
                    # update outputs
                    for output in cmpt_object.get_dynamic_outputs():
                        output.update()
                else:
                    if self._component_is_duplicated(cmpt_object):
                        raise ValueError("Component " + cmpt_object.__class__.__name__ + " with prefix " + 
                                            cmpt_object.prefix + " already exist in this pipeline!")
                    self.component_nameids[cmpt_object.get_nameid()] = None
                    self.components_to_exec = []
                    self.components = []
            else:
                if self.component_nameids_is_init:
                    # add the component
                    self.components_to_exec.append(cmpt_object)
                    self.components.append(cmpt_object)
                elif not self.component_nameids_is_init and not self.dynamic_component_present:
                    if self._component_is_duplicated(cmpt_object):
                        raise ValueError("Component " + cmpt_object.__class__.__name__ + " with prefix " + 
                                            cmpt_object.prefix + " already exist in this pipeline!")
                    self.components_to_exec.append(cmpt_object)
                    self.components.append(cmpt_object)
                else:
                    if self._component_is_duplicated(cmpt_object):
                        raise ValueError("Component " + cmpt_object.__class__.__name__ + " with prefix " + 
                                            cmpt_object.prefix + " already exist in this pipeline!")
                    self.component_nameids[cmpt_object.get_nameid()] = None

            return cmpt_object
        else:
            raise ImportError(component_name + " component cannot be loaded, available components are: {0}".format(
                                           ", ".join(self.internal_components.keys() + self.external_components.keys())))
    
    def post_process(self):
        # first add all table that are in the schema and not used within the pipeline
        lib_dir = os.path.join(os.path.dirname(inspect.getfile(self.__class__)), "lib")
        pipeline_dir = os.path.dirname(inspect.getfile(self.__class__))
        module_to_load = []
        dm_module_to_load = {}
        for importer, modname, ispkg in pkgutil.iter_modules([lib_dir], "workflows." + os.path.basename(pipeline_dir) + ".lib."):
            try:
                m = __import__(modname)
                for class_name, obj in inspect.getmembers(sys.modules[modname], inspect.isclass):
                    if issubclass(obj, ngspipelines.biomart_db.BiomartMain) or issubclass(obj, ngspipelines.biomart_db.BiomartDimension):
                        if class_name not in ["Analysis", "Project", "Library", "Result", "BiomartDimension", "BiomartMain"]:
                            table_exists = False
                            for table_obj in self.project.biomart_tables:
                                if table_obj.__class__.__name__ == class_name: table_exists = True
                            my_pckge = __import__(modname, globals(), locals(), [class_name], -1)
                            table_class = getattr(my_pckge, class_name)
                            if table_exists and issubclass(obj, ngspipelines.biomart_db.BiomartMain):
                                module_to_load.append(modname)
                                dm_module_to_load[modname] = []
                            elif not table_exists and issubclass(obj, ngspipelines.biomart_db.BiomartDimension):
                                dm_module_to_load[modname].append(table_class)
            except Exception as e:
                pass
        for mtl in module_to_load:
            for dmtl in dm_module_to_load[mtl]:
                self.project.add_biomart_table(dmtl)

        # then add the NULL field to each records
        for table in self.project.biomart_tables:
            table.update_mart_field()
            # only dimension has to do this
            if isinstance(table, ngspipelines.biomart_db.BiomartDimension):
                table.add_null_values()
        
        self._copy_to_save()
        self._unlock_instance()
        shutil.rmtree(self.work_dir)
                
    def _add_required_parameters(self):
        # add instance parameter
        self.add_parameter("instance_name", "In which instance should be added the application", required=True, type="instance_exists")
        # add project parameters
        self.add_parameter("project_name", "Give a name to your project (has to be unique)", required=True, type="project")
        self.add_parameter("species", "Which species has been sequenced", required=True)
        self.add_parameter("species_common_name", "Which is the species common name", required=True)
        self.add_parameter("project_description", "Give a description to your project", required=True)
        # add library parameters
        self.add_multiple_parameter_list("library", "Defines libraries used in your experimentation", required=True)
        self.add_parameter("library_name", "Give a name to your library", required=True, add_to="library")
        self.add_parameter("sample_name", "Give a name to your sample", default="", add_to="library")
        self.add_parameter("replicat", "Is this a replicat", required=True, type="int", add_to="library")
        self.add_parameter("tissue", "From wich tissue has been sampled the library", default="", add_to="library")
        self.add_parameter("dev_stage", "What is the sample development stage", default="", add_to="library")
        self.add_parameter("remark", "Link some comments to the library", default="", add_to="library")
        self.add_parameter("sequencer", "Which sequencer was used to sequence the library", required=True, add_to="library")
        self.add_parameter("type", "Which type of library it is", required=True, type="library_types", add_to="library")
        self.add_parameter("insert_size", "What is the insert size", default=0, type="int", add_to="library")
        self.add_parameter("public", "Is this library public", type=int, default=0, add_to="library")
        self.add_parameter("nb_sequence", "How many sequences is included in the original fastq", type="int", add_to="library")
        self.add_parameter("database", "If this library is coming from a public database, which database it is", default="", add_to="library")
        self.add_parameter("accession", "If this library is coming from a public database, what is it's accession number", default="", add_to="library")
        self.add_input_file_list("files", "Path to the fastq files", required=True, add_to="library")
        # force options to delete a project
        self.add_parameter("force", "Replace the project if already in the database", type="bool", default=False)
        self.add_parameter("unlock", "Force to unlock an instance", type="bool", default=False)
        # image parameter to overwrite the default one
        self.add_input_file("image", "Which image should be displayed for your project.")
    
    def get_application_type(self):
        return str(self.__class__).split(".")[1]
    
    def get_library_from_name(self, name):
        """
            return an library object from a name
            @param name : string
        """
        for lib in self.project.libraries:
            if name == lib.library_name :
                 return lib
        return None
    
    def is_not_locked(self):
        save_xml = os.path.join(self.instance_directory,"dist","registry", self.instance_name + '.xml')
        if not os.path.exists(save_xml+".lock") :
            return True
        return False
        
    def _lock_instance(self):
        if os.path.exists(self.work_dir):
            try:
                shutil.rmtree(self.work_dir)
            except: pass
        os.makedirs(self.work_dir, 0751)
        if not os.path.exists(os.path.join(self.work_dir,"dist","registry")): 
            os.makedirs(os.path.join(self.work_dir,"dist","registry"), 0751)
        # copy xml file
        xml_path=os.path.join("dist","registry", self.instance_name + '.xml')
        save_xml = os.path.join(self.instance_directory,xml_path)
        if not os.path.exists(save_xml+".lock") : 
            open(save_xml+".lock", 'w').close()
        if not os.path.exists(os.path.join(self.work_dir,xml_path)) and os.path.exists(save_xml) :
            shutil.copy( save_xml, os.path.join(self.work_dir,xml_path))
             
    
    def _unlock_instance(self):
        xml_save = os.path.join(self.instance_directory,"dist","registry",os.path.basename(self.project.get_xml_path()))
        if os.path.exists(xml_save+".bk") : os.remove(xml_save+".bk")
        if os.path.exists(xml_save) : shutil.move(xml_save, xml_save+".bk")
        shutil.move(self.project.get_xml_path(), xml_save)
        os.remove(xml_save+".lock")
        
    def _copy_to_work (self):

        if not os.path.exists(os.path.join(self.work_dir,"data")) : 
            os.makedirs(os.path.join(self.work_dir,"data"), 0751) 
        
        #copy data directory in work
        shutil.move(os.path.join(self.instance_directory,"data",self.project.directory), os.path.join(self.work_dir,"data"))
         
    def _copy_to_save (self):
        shutil.move(os.path.join(self.work_dir,"data",self.project.directory),os.path.join(self.instance_directory,"data"))
        
    
class Application(ApplicationWithoutReferenceGenome):
    
    def _add_required_parameters(self):
        ApplicationWithoutReferenceGenome._add_required_parameters(self)
        # add the reference genome
        self.add_input_file("reference_genome", "Path to the reference genome in fasta format", required=True)
        
    