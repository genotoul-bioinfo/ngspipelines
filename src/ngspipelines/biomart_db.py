#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import re
import sys
import inspect
import pkgutil
import warnings
import subprocess
import datetime
import xml.etree.ElementTree as ET
import xml.dom.minidom
import copy
import collections
import time
import uuid
import operator

from MySQLdb import *

import ngspipelines
from ngspipelines.config_reader import NGSPipelinesConfigReader

from jflow.config_reader import JFlowConfigReader
from jflow.utils import which


class FilterTypes(object):
    ''' Registry field types
    '''
    TEXT = 'text'
    BOOLEAN = 'boolean'
    UPLOAD = 'upload'
    SINGLE_SELECT = 'singleSelect'
    SINGLE_SELECT_UPLOAD = 'singleSelectUpload'
    SINGLE_SELECT_BOOLEAN = 'singleSelectBoolean'
    MULTI_SELECT = 'multiSelect'
    MULTI_SELECT_UPLOAD = 'multiSelectUpload'
    MULTI_SELECT_BOOLEAN = 'multiSelectBoolean'

class FilterQualifiers(object):
    '''Filter qualifiers
    '''
    EQUALS = '='
    EQUALS_LOWER = '<='
    EQUALS_GREATER = '>='
    LOWER = '<'
    GREATER = '>'
    LIKE = 'LIKE'
    IS = 'IS'
    RANGE = 'RANGE'
    
class DBTypes(object):
    VARCHAR = "varchar"
    INT = "int"
    TINYINT = "tinyint"
    BIGINT = "bigint"
    BOOLEAN = "boolean"
    FLOAT = "float"
    MEDIUMTEXT = "mediumtext"
    DOUBLE = "double"
    TEXT = "text"
    CHAR = "char"
    DATETIME = "datetime"
    
class BiomartField(object):

    def __init__(self, name, type, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, favorites_position=-1):
        self.name = name
        if type in DBTypes.__dict__.values():
            self.type = type
        else: raise Exception("'" + type + "' is not a supported type by BiomartField!")
        self.size = size
        self.default = default
        self.is_key = is_key
        self.is_primary = is_primary
        self.is_visible = is_visible
        self.display_name = display_name if display_name else self.name.replace('_', ' ')
        self.display_name = self.display_name.capitalize()
        self._filters_def = []
        self._report_name = None
        self.favorites_position = favorites_position
    
    def add_filter(self, display_name = None, type = FilterTypes.TEXT, qualifier = FilterQualifiers.EQUALS, group = None):
        '''
            Add a new filter to this field
            @param display_name: Filter display name
            @param type: a type from FilterTypes class
            @param qualifier: a qualifier from FilterQualifiers class
            @param group: a group name. Filters will be grouped using this group name
        '''
        dname = display_name if display_name else self.name.replace('_', ' ')
        self._filters_def.append( dict( display_name = dname, type = type, qualifier = qualifier, group = group ) )

    def get_filters(self):
        return self._filters_def
    
    def add_link_to_report(self, js_plugin_name):
        '''
            Add a link to a report
            @param js_plugin_name: The name of the javascript plugin associated with the report 
        '''
        self._report_name = js_plugin_name 
        
    def linked_to_report(self):
        return self._report_name
    

class BiomartDATETIMEField(str, BiomartField):

    def __new__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input="", favorites_position=-1):
        return str.__new__(self, input)
    
    def __init__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input="", favorites_position=-1):
        BiomartField.__init__(self, name, DBTypes.DATETIME, size, default, is_key, is_primary, is_visible, display_name, favorites_position)

class BiomartCHARField(str, BiomartField):

    def __new__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input="", favorites_position=-1):
        return str.__new__(self, input)

    def __init__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input="", favorites_position=-1):
        BiomartField.__init__(self, name, DBTypes.CHAR, size, default, is_key, is_primary, is_visible, display_name, favorites_position)

class BiomartTEXTField(str, BiomartField):

    def __new__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input="", favorites_position=-1):
        return str.__new__(self, input)

    def __init__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input="", favorites_position=-1):
        BiomartField.__init__(self, name, DBTypes.TEXT, size, default, is_key, is_primary, is_visible, display_name, favorites_position)

class BiomartDOUBLEField(int, BiomartField):

    def __new__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input=0, favorites_position=-1):
        return int.__new__(self, input)

    def __init__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input=0, favorites_position=-1):
        BiomartField.__init__(self, name, DBTypes.DOUBLE, size, default, is_key, is_primary, is_visible, display_name, favorites_position)
        
class BiomartMEDIUMTEXTField(str, BiomartField):

    def __new__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input="", favorites_position=-1):
        return str.__new__(self, input)

    def __init__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input="", favorites_position=-1):
        BiomartField.__init__(self, name, DBTypes.MEDIUMTEXT, size, default, is_key, is_primary, is_visible, display_name, favorites_position)

class BiomartBOOLEANField(int, BiomartField):

    def __new__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input=0, favorites_position=-1):
        return int.__new__(self, input)

    def __init__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input=0, favorites_position=-1):
        BiomartField.__init__(self, name, DBTypes.BOOLEAN, size, default, is_key, is_primary, is_visible, display_name, favorites_position)

class BiomartFLOATField(float, BiomartField):

    def __new__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input=0.0, favorites_position=-1):
        return float.__new__(self, input)

    def __init__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input=0.0, favorites_position=-1):
        BiomartField.__init__(self, name, DBTypes.FLOAT, size, default, is_key, is_primary, is_visible, display_name, favorites_position)

class BiomartBIGINTField(int, BiomartField):

    def __new__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input=0, favorites_position=-1):
        return int.__new__(self, input)

    def __init__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input=0, favorites_position=-1):
        BiomartField.__init__(self, name, DBTypes.BIGINT, size, default, is_key, is_primary, is_visible, display_name, favorites_position)  

class BiomartTINYINTField(int, BiomartField):

    def __new__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input=0, favorites_position=-1):
        return int.__new__(self, input)

    def __init__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input=0, favorites_position=-1):
        BiomartField.__init__(self, name, DBTypes.TINYINT, size, default, is_key, is_primary, is_visible, display_name, favorites_position)    
    
class BiomartINTField(int, BiomartField):
    
    def __new__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input=0, favorites_position=-1):
        return int.__new__(self, input)
    
    def __init__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input=0, favorites_position=-1):
        BiomartField.__init__(self, name, DBTypes.INT, size, default, is_key, is_primary, is_visible, display_name, favorites_position)

class BiomartVARCHARField(str, BiomartField):
    
    def __new__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input="", favorites_position=-1):
        return str.__new__(self, input)

    def __init__(self, name, size=None, default=None, is_key=False, is_primary=False, is_visible = True, display_name = None, input="", favorites_position=-1):
        BiomartField.__init__(self, name, DBTypes.VARCHAR, size, default, is_key, is_primary, is_visible, display_name, favorites_position)

    
class BiomartTable(object):

    def __init__(self, project, **args):
        self.define_fields()
        self.project = project
        self.name_ids = {}
        self.favorite_analysis = None
        for attribute in sorted(self.__dict__.keys()):
            if isinstance( self.__dict__[attribute], BiomartField ) :
                if args.has_key(attribute):
                    # add the value to the string
                    self.__dict__[attribute].__dict__["input"] = args[attribute]
                    # delete the type biomartField attribute
                    del self.__dict__[attribute].__dict__["type"]
                    backup = self.__dict__[attribute].__dict__
                    init_args = inspect.getargspec(self.__dict__[attribute].__class__.__new__).args
                    init_args.pop(0)
                    self.__dict__[attribute] = self.__dict__[attribute].__class__(**{  k : backup.pop(k) for k in  init_args })
                    self.__dict__[attribute].__dict__.update( backup )
        self.ngs_reader = NGSPipelinesConfigReader()
        self.db_info = self.ngs_reader.get_database_info()
        self.dump_file = os.path.join(self.ngs_reader.get_tmp_directory(), self.get_table_name())
        if args.has_key('create'): 
            if args['create'] :
                if os.path.isfile(self.dump_file): os.remove(self.dump_file)
                self.__create_table()
                self._build_xml()
    
    @staticmethod
    def get_class_name_from_table_name(table_name):
        return table_name.split("__")[1]
        
    @staticmethod
    def get_from_table_name(project, table_name):
        class_name = BiomartTable.get_class_name_from_table_name(table_name)
        module_name = table_name.split("__")[0].split("_")[1]
        pckg = BiomartTable.get_class_package(module_name, class_name)
        my_pckge = __import__(pckg, globals(), locals(), [class_name], -1)
        return getattr(my_pckge, class_name)(project, create=False)
        
    @staticmethod
    def get_class_package(module_name, class_name):
        # then import pipeline packages
        application_dir = os.path.dirname(inspect.getfile(BiomartTable))
        for app_importer, app_modname, app_ispkg in pkgutil.iter_modules([os.path.join(application_dir, "..", "..", "workflows")]):
            for mart_importer, mart_modname, mart_ispkg in pkgutil.iter_modules([os.path.join(application_dir, "..", "..", "workflows", app_modname, "lib")], "workflows."+app_modname+".lib."):
                m = __import__(mart_modname)
                current_module_name = mart_modname.split(".")[-1]
                for current_class_name, obj in inspect.getmembers(sys.modules[mart_modname], inspect.isclass):
                    if issubclass(obj, BiomartTable) and current_class_name == class_name and current_module_name == module_name:
                        return mart_modname
        return None
        
    def add_histogram(self, chart_title, labels, values, x_title="", y_title="", chart_subtitle="", tooltip=None, **args):
        return self.project.add_histogram(chart_title, labels, values, x_title=x_title, y_title=y_title, chart_subtitle=chart_subtitle, tooltip=tooltip, categorie=str(self.__class__).split(".")[-2], **args)

    def add_column(self, chart_title, labels, values, x_title="", y_title="", chart_subtitle="", tooltip=None, **args):
        return self.project.add_column(chart_title, labels, values, x_title=x_title, y_title=y_title, chart_subtitle=chart_subtitle, tooltip=tooltip, categorie=str(self.__class__).split(".")[-2], **args)

    def add_bargraph(self, chart_title, labels, values, x_title="", y_title="", chart_subtitle="", tooltip=None,  **args):
        return self.project.add_bargraph(chart_title, labels, values, x_title=x_title, y_title=y_title, chart_subtitle=chart_subtitle, tooltip=tooltip, categorie=str(self.__class__).split(".")[-2], **args)

    def add_basicline(self, chart_title, labels, values, x_title="", y_title="", chart_subtitle="", tooltip=None, **args):
        return self.project.add_basicline( chart_title, labels, values, x_title=x_title, y_title=y_title, chart_subtitle=chart_subtitle, tooltip=tooltip, categorie=str(self.__class__).split(".")[-2], **args)

    def add_stacked_histogram(self, chart_title, labels, values, x_title="", y_title="", chart_subtitle="", tooltip=None, **args):        
        return self.project.add_stacked_histogram(chart_title, labels, values, x_title=x_title, y_title=y_title, chart_subtitle=chart_subtitle, tooltip=tooltip, categorie=str(self.__class__).split(".")[-2], **args)

    def add_table(self, chart_title, rows, header=None, comments="", chart_subtitle="", tooltip=None, **args):
        return self.project.add_table(chart_title, rows, header=header, comments=comments, chart_subtitle=chart_subtitle, tooltip=tooltip, categorie=str(self.__class__).split(".")[-2], **args)

    def add_piechart(self, chart_title, labels, values, chart_subtitle="", tooltip=None, **args):
        return self.project.add_piechart(chart_title, labels, values, chart_subtitle=chart_subtitle, tooltip=tooltip, categorie=str(self.__class__).split(".")[-2], **args)

    def add_scatterplot(self, chart_title, x_values, y_values, x_title="", y_title="", chart_subtitle="", tooltip=None, **args):
        return self.project.add_scatterplot(chart_title, x_values, y_values, x_title=x_title, y_title=y_title, chart_subtitle=chart_subtitle, tooltip=tooltip, categorie=str(self.__class__).split(".")[-2], **args)    
    
    def add_donutchart(self, chart_title, labels, values, dict_sub_label_group={}, chart_subtitle="", categorie=None, tooltip=None, comments=None, drilldown=False, **args):
        return self.project.add_donutchart(chart_title, labels, values, dict_sub_label_group=dict_sub_label_group, chart_subtitle=chart_subtitle, drilldown=drilldown, tooltip=tooltip, categorie=str(self.__class__).split(".")[-2], **args)
    
    def add_favorite(self, name):
        
        main_table, main_primary_key, favorite_keys, favorite_headers = None, None, {}, {}
        for class_name, obj in inspect.getmembers(sys.modules[self.__class__.__module__], inspect.isclass):
            if class_name != BiomartMain.__name__ and issubclass(obj, ngspipelines.biomart_db.BiomartMain) :
                biomart_table = getattr(sys.modules[self.__class__.__module__], class_name)(self.project, create=False)
                main_table = biomart_table.get_table_name()
                for attribute in biomart_table.__dict__:
                    if isinstance( biomart_table.__dict__[attribute], BiomartField ) and biomart_table.__dict__[attribute].is_primary:
                        main_primary_key = attribute
                    if isinstance( biomart_table.__dict__[attribute], BiomartField ) and biomart_table.__dict__[attribute].favorites_position > -1:
                        favorite_keys[attribute] = biomart_table.__dict__[attribute].favorites_position
                        favorite_headers[biomart_table.__dict__[attribute].display_name] = biomart_table.__dict__[attribute].favorites_position
                        
        favorite_keys["key"] = -1
        favorite_headers[self.get_main_internal_key()] = -1
        sorted_favorite_keys = sorted(favorite_keys.iteritems(), key=operator.itemgetter(1))
        sorted_favorite_headers = sorted(favorite_headers.iteritems(), key=operator.itemgetter(1))
        favorite_keys, favorite_headers = [], []
        for x in sorted_favorite_keys: favorite_keys.append(x[0])
        for x in sorted_favorite_headers: favorite_headers.append(x[0])
        
        main_class_r = BiomartTable.get_class_name_from_table_name(main_table)
        main_class = main_class_r.lower()
        if self.favorite_analysis == None:
            from ngspipelines.analysis import Analysis
            self.favorite_analysis = Analysis.get_from_name(self.project, main_class+" favorite", self.project.current_application)
            if self.favorite_analysis == None:
                self.favorite_analysis = Analysis(self.project, analysis_name=main_class+" favorite", analysis_type="favorite_"+main_class, 
                                                  analysis_hidden="1", soft_name="favorite_"+main_class, soft_parameters="", soft_version="", 
                                                  comments="", categorie=str(self.__class__).split(".")[-2])
                self.favorite_analysis.add_result_element("table_keys", ",".join(favorite_keys), "STRING", group="favorite")
                self.favorite_analysis.add_result_element("table_headers", ",".join(favorite_headers), "STRING", group="favorite")
                self.favorite_analysis.add_result_element("links", main_primary_key, "STRING", group="favorite")
                self.favorite_analysis.add_result_element("main_class", main_class_r, "STRING", group="favorite")
        self.favorite_analysis.add_result_element("favorite", self.get_primary_keys()[name] , "INT", group="favorite")
    
    def add_libraries_count(self, target_class, count):
        from ngspipelines.analysis import Analysis
        if os.path.isfile(count):
            # store librairies counts
            count_libraries = Analysis(self.project, analysis_name="Libraries count", analysis_type="libraries_count", 
                                       analysis_hidden="0", soft_name="-", soft_parameters="", soft_version="", 
                                       comments="", categorie=str(self.__class__).split(".")[-2])            
            count_libraries.add_result_file(count, 'merged_count.csv', None)
            biomart_class = self.project.get_biomart_table( target_class )
            count_libraries.add_result_element("target", biomart_class.__class__.__name__, "STRING")
            count_libraries.add_result_element("label", biomart_class.get_main_primary_key_info()[1], "STRING")
    
    def delete(self):
        try:
            sql_request = "DROP TABLE `" + self.get_table_name() + "`"
            conn = connect(self.db_info["host"], self.db_info["user"], 
                           self.db_info["passwd"], self.db_info["dbname"])
            curs = conn.cursor()
            curs.execute(sql_request)
            conn.commit()
            curs.close()
            conn.close()
        except: pass
        MartRegistry(self, delete = True)
    
    def truncate(self):
        sql_request = "TRUNCATE TABLE `" + self.get_table_name() + "`"
        conn = connect(self.db_info["host"], self.db_info["user"], 
                       self.db_info["passwd"], self.db_info["dbname"])
        curs = conn.cursor()
        curs.execute(sql_request)
        conn.commit()
        curs.close()
        conn.close()
        
    def get_real_table_name(self):
        return self.__class__.__name__
    
    def get_table_name(self):
        raise NotImplementedError
    
    def define_fields(self):
        raise NotImplementedError
    
    def _get_field_primary_key(self):
        raise NotImplementedError
    
    def _get_index_primary_key(self):
        raise NotImplementedError
    
    def get_main_primary_key_info(self):
        # TODO
        # find a solution to not create the object
        main_table, main_primary_key = None, None
        for class_name, obj in inspect.getmembers(sys.modules[self.__class__.__module__], inspect.isclass):
            if class_name != BiomartMain.__name__ and issubclass(obj, ngspipelines.biomart_db.BiomartMain) :
                biomart_table = getattr(sys.modules[self.__class__.__module__], class_name)(self.project, create=False)
                main_table = biomart_table.get_table_name()
                for attribute in biomart_table.__dict__:
                    if isinstance( biomart_table.__dict__[attribute], BiomartField ) and biomart_table.__dict__[attribute].is_primary :
                        main_primary_key = attribute
        if main_primary_key is None: main_primary_key = self.get_main_internal_key()
        return [main_table, main_primary_key]

    def get_main_internal_key(self):
        main_internal = None
        for class_name, obj in inspect.getmembers(sys.modules[self.__class__.__module__], inspect.isclass):
            if class_name != BiomartMain.__name__ and issubclass(obj, ngspipelines.biomart_db.BiomartMain) :
                main_internal = class_name.lower() + "_id_key"
        return main_internal
    
    def __create_table(self):
        # create the sql request        
        primary_key, key = [], []
        #sql_request = "DROP TABLE IF EXISTS `###PROJECT###_" + self.__class__.__name__ + "`;\n"
        sql_request = "CREATE TABLE IF NOT EXISTS `" + self.get_table_name() + "` ("
        sql_request += self._get_field_primary_key()
        for attribute in self._iter_fields():
            sql_request += "`" + attribute.name + "` " + attribute.type
            if attribute.size:
                sql_request += "("+str(attribute.size)+")"
            if attribute.default != None:
                sql_request += " default '"+str(attribute.default) + "'"
            if attribute.is_key:
                key.append(attribute.name)
            sql_request += ","
        for k in key: sql_request += "KEY `"+k+"` (`" + k + "`),"
        sql_request += self._get_index_primary_key()
        sql_request += ") ENGINE=MyISAM DEFAULT CHARSET=latin1;"
        # ignore drop table warnings if table does not exist
        #warnings.filterwarnings("ignore", "Unknown table.*")
        conn = connect(self.db_info["host"], self.db_info["user"], 
                       self.db_info["passwd"], self.db_info["dbname"])
        curs = conn.cursor()
        curs.execute(sql_request)
        conn.commit()
        curs.close()
        conn.close()

    def _iter_fields(self):
        '''
            iteration over all BiomartField if there are BiomartField in this table.
            Note : for a BiomartDimension table, the external key is NOT a field ....
        '''
        for attribute in sorted(self.__dict__.keys()):
            if isinstance( self.__dict__[attribute], BiomartField ) :
                yield self.__dict__[attribute]
        
    def _build_xml(self):
        '''
            Create the xml structure associated with this table and add it 
            to a mart registry 
        '''
        tablename = self.get_table_name()
        realTablename = self.get_real_table_name()
        tablename_attribute = tablename + '_attribute'
        tablename_filter = tablename + '_filter'
        tabletype = None
        tabletype = 'MAIN' if isinstance(self, BiomartMain) else 'DIMENSION'
        
        tableElement = ET.Element( 'table', dict( name=tablename, internalname=tablename, displayname=tablename, description=tablename, hide="false", type=tabletype, inpartitions='', subpartition=""))
        attributeContainerElement = ET.Element( 'container', dict( name=tablename_attribute, displayname=tablename_attribute, internalname=tablename_attribute, description=tablename_attribute, hide="false", maxcontainers="", maxattributes="", independentquerying=""))
        filterContainerElement = ET.Element('container', dict( name=tablename_filter, displayname=tablename_filter, internalname=tablename_filter, description=tablename_filter, hide="false", maxcontainers="", maxattributes="", independentquerying="")) 
        newFilterContainer = ET.Element( 'container', dict( description="newFilter", displayname="newFilter", hide="false", independentquerying="", internalname="newFilter", maxattributes="", maxcontainers="", name="newFilter" ) )
        
        # special cause it lays out the filters 
        attributeRDFContainerElement = ET.Element( 'container', dict( name=tablename_attribute, displayname=realTablename, internalname=tablename_attribute, description=tablename_attribute, hide="false", maxcontainers="", maxattributes="", independentquerying=""))
        filterRDFContainerElement = ET.Element('container', dict( name=tablename_filter, displayname=realTablename, internalname=tablename_filter, description=tablename_filter, hide="false", maxcontainers="", maxattributes="", independentquerying=""))
        
        def _aname(n):
            iii = 1
            cpp = n + '_copy'
            while len ( filterContainerElement.findall( './/filter[@name="'+n+'"]' )  + newFilterContainer.findall( './/filter[@name="'+n+'"]' ) ) > 0 :
                n = cpp
                if iii > 1 :
                    n += str(iii)
                iii+=1
            return n
        
        for field in self._iter_fields() :
            attrname = tablename + '__' + field.name
            
            ET.SubElement( tableElement, 'column', dict( name=field.name, inpartitions='') )
            ET.SubElement( attributeContainerElement, 'attribute', dict( name=attrname, internalname=attrname, displayname=field.display_name, description=attrname, hide="false", column=field.name, table=tablename, 
                                                                         attributelist="", linkouturl="", value="", inusers="", rdf="", mart="", config="", pointer="false")) 
            ET.SubElement( filterContainerElement, 'filter', dict( rdf="", required="", name=attrname, displayname=field.display_name, internalname=attrname, description=attrname, mart="", config="",
                                                                   pointer="false", hide="false", type="", attribute=attrname, spliton="", operation="", datafile="", filterlist="", qualifier="", refcontainer="", inusers="", dependson="")) 
            
            hide = 'false' if field.is_visible else 'true'
            link = field.linked_to_report() if field.linked_to_report() else ''
            ET.SubElement( attributeRDFContainerElement, 'attribute', dict( name=attrname, internalname=attrname, displayname=field.display_name, description=field.display_name, hide=hide, column=field.name, table=tablename, 
                                                                         attributelist="", linkouturl=link , value="", inusers="", rdf="", mart="", config="", pointer="false"))
            
            ET.SubElement( filterRDFContainerElement, 'filter', dict( rdf="", required="", name=attrname, displayname=field.display_name, internalname=attrname, description=attrname, mart="", config="",
                                                                   pointer="false", hide="true", type="", attribute=attrname, spliton="", operation="", datafile="", filterlist="", qualifier="", refcontainer="", inusers="", dependson=""))
            for filter in field.get_filters() :
                new_filter_name  =_aname(attrname)
                ET.SubElement( newFilterContainer, 'filter', dict( rdf="", required="", name=new_filter_name, displayname=field.display_name, internalname=attrname, description=attrname, mart="", config="",
                        pointer="false", hide="false", type="", attribute=attrname, spliton="", operation="", datafile="", filterlist="", qualifier="", refcontainer="", inusers="", dependson=""))
                
                cc = filterRDFContainerElement
                
                if filter['group'] is not None :
                    containers = filterRDFContainerElement.findall( './/container[@name="'+filter['group']+'"]' )
                    if len(containers) > 0 :
                        cc = containers[0]
                    else :
                        cc = ET.SubElement( filterRDFContainerElement, 'container', dict( name=filter['group'], description=filter['group'], displayname=filter['group'], hide="false", independentquerying="", internalname=filter['group'], maxattributes="", maxcontainers="" ) )
                    
                ET.SubElement( cc, 'filter', dict( rdf="", required="", name=new_filter_name, displayname=filter['display_name'], internalname=attrname, description=attrname, mart="", config="",
                               pointer="false", hide="false", type=filter['type'], attribute=attrname, spliton="", operation="", datafile="", filterlist="", qualifier=filter['qualifier'], refcontainer="", inusers="", dependson=""))
        
            
        # Since the id key is not defined as a field, get it from
        main_internal_key = self.get_main_internal_key()
        if not main_internal_key :
            raise ValueError("Unable to retrieve main table internal primary key! A BiomartMain class has not been defined yet!")
        
        n, tag = None, None
        attrname = tablename + '__' + main_internal_key
        
        if tabletype == 'MAIN' :
            n = main_internal_key + '_pk'
            tag = 'primarykey'
        else :
            n = main_internal_key + '_fk'
            tag = 'foreignkey'
            
        # add internal id key from main (foreign key of primary depending on the table type)
        main_internal_key_capitalize = main_internal_key.replace('_', ' ').capitalize()
        ET.SubElement( tableElement, 'column', dict(name = main_internal_key, inpartitions = '') )
        ET.SubElement( tableElement, tag, dict(name = n , internalname = n, displayname = n, description = n, column = main_internal_key) )
        
        ET.SubElement( attributeContainerElement, 'attribute', dict(name = attrname, internalname = attrname, displayname = main_internal_key_capitalize,
               description = attrname, hide = "false", column = main_internal_key, table = tablename, attributelist = "",
               linkouturl = "", value = "", inusers = "", rdf = "", mart = "" , config = "", pointer = "false") )
        ET.SubElement( filterContainerElement, 'filter', dict( name = attrname , displayname = main_internal_key_capitalize , internalname = attrname ,  
              description = attrname , mart = "", config = "", pointer = "false", hide = "false", type = "" , attribute = attrname , rdf = "", required = "",
              spliton = "", operation = "", datafile = "", filterlist = "", qualifier = "", refcontainer = "", inusers = "", dependson = "") )
        
        ET.SubElement( attributeRDFContainerElement, 'attribute', dict(name = attrname, internalname = attrname, displayname = main_internal_key_capitalize,
               description = attrname, hide = "true", column = main_internal_key, table = tablename, attributelist = "",
               linkouturl = "", value = "", inusers = "", rdf = "", mart = "" , config = "", pointer = "false") )
        ET.SubElement( filterRDFContainerElement, 'filter', dict( name = attrname , displayname = main_internal_key_capitalize , internalname = attrname ,  
              description = attrname , mart = "", config = "", pointer = "false", hide = "true", type = "" , attribute = attrname , rdf = "", required = "",
              spliton = "", operation = "", datafile = "", filterlist = "", qualifier = "", refcontainer = "", inusers = "", dependson = "") )
        
        self.tableElement = ET.tostring(tableElement, encoding="UTF-8")
        self.attributeContainerElement = ET.tostring(attributeContainerElement, encoding="UTF-8")
        self.filterContainerElement = ET.tostring(filterContainerElement, encoding="UTF-8")
        self.attributeRDFContainerElement = ET.tostring(attributeRDFContainerElement, encoding="UTF-8")
        self.filterRDFContainerElement = ET.tostring(filterRDFContainerElement, encoding="UTF-8")
        self.newFilterContainer = ET.tostring(newFilterContainer, encoding="UTF-8")
        MartRegistry(self)
        
    def __import_dump_file(self):
        # run the mysqlimport command
        config_reader = JFlowConfigReader()
        exec_path = config_reader.get_exec('mysqlimport')
        if exec_path is None: exec_path = 'mysqlimport'
        if which(exec_path) == None:
            sys.stderr.write("Error: 'mysqlimport' path cannot be retrieved either in the PATH and in the application.properties file!\n")
            sys.exit(1)
        cmd = [exec_path,  '-h', self.db_info["host"], '--local', '-u', self.db_info["user"], '-p'+self.db_info["passwd"],
               self.db_info["dbname"], self.dump_file]
        p = subprocess.Popen(cmd)
        p.wait()

    def get_primary_keys(self):
        if len(self.name_ids) == 0 :
            main_table, main_primary_key = self.get_main_primary_key_info()
            if main_table and main_primary_key:
                conn = connect(self.db_info["host"], self.db_info["user"], 
                               self.db_info["passwd"], self.db_info["dbname"])
                curs = conn.cursor()
                req = "SELECT `" + self.get_main_internal_key() +"`, `" + main_primary_key + "` FROM `" + main_table + "`;"
                curs.execute(req)
                for value in curs.fetchall():
                    self.name_ids[value[1]] = int(value[0])
                curs.close()
                conn.close()
            else:
                raise ValueError("Unable to retrieve internal primary keys links! Maybe the BiomartMain Table has not been imported yet!")
        return self.name_ids 
        
    def save(self):
        self.__import_dump_file()
    
    def delete_dump_file(self):
        if os.path.exists(self.dump_file) :
            os.unlink(self.dump_file)
            
    def update_mart_field(self):
        MartRegistry(self, update= True)
    
    #update dump filename to avoid for an object to import several times the same lines 
    def setTemporaryDumpFile(self):
        file_basename= os.path.basename(self.dump_file)
        if len(os.path.splitext(file_basename))>1 :
            file_basename = os.path.splitext(file_basename)[0]
        suffix = uuid.uuid4().hex[:10]
        while True:
            file = os.path.join(self.ngs_reader.get_tmp_directory(),file_basename+"."+suffix) 
            if not os.path.exists(file):
                break
            suffix = uuid.uuid4().hex[:10]
        
        self.dump_file = file
         
class BiomartDimension(BiomartTable):

    def __init__(self, project, **args):
        BiomartTable.__init__(self, project, **args)
    
    def add_row(self, main_primary, values={}):
        try :
            row = str(self.get_primary_keys()[main_primary]) + "\t"
            for attribute in  sorted(self.__dict__.keys()):
                if isinstance( self.__dict__[attribute], BiomartField ):
                    try:
                        row += str(values[attribute]) + "\t"
                    except: 
                        row += "\t"
            dfile = open(self.dump_file, "a")
            dfile.write(row+"\n")
            dfile.close()
        except :
            print "Warn : main key: "+main_primary+" not found while loading "+self.get_table_name()
    
    def add(self, main_primary):
        """ 
        add a single element to the table
        """
        keys, values = self.get_main_internal_key()+",", str(self.get_primary_keys()[main_primary])+","
        for attribute in self._iter_fields():
            values += "\"" + str(attribute) + "\"," if str(attribute) else "NULL,"
            keys += "`" +attribute.name + "`,"
        values = values[:-1]
        keys = keys[:-1]  
        conn = connect(self.db_info["host"], self.db_info["user"], self.db_info["passwd"], self.db_info["dbname"])
        curs = conn.cursor()
        req = "INSERT INTO " + self.get_table_name() + "(" + keys + ") VALUES (" + str(values) + ");"
        curs.execute(req)
        id = conn.insert_id()
        curs.close()
        conn.close()
        return id
    
    def _get_field_primary_key(self):
        return "`"+self.get_main_internal_key()+"` int(10) unsigned default NULL,"
    
    def _get_index_primary_key(self):
        return "KEY `" + self.get_main_internal_key() + "` (`" + self.get_main_internal_key() + "`)"
    
    def get_table_name(self):
        return self.project.name + "_" + str(self.__class__).split(".")[-2] + "__" + self.__class__.__name__ + "__dm"
    
    def add_null_values(self):
        main_table, main_primary_key = self.get_main_primary_key_info()
        if main_table and main_primary_key:
            conn = connect(self.db_info["host"], self.db_info["user"], 
                           self.db_info["passwd"], self.db_info["dbname"])
            curs = conn.cursor()
            req = "INSERT INTO  " + self.get_table_name() + " ("+self.get_main_internal_key()+") "
            req = req + "SELECT m."+self.get_main_internal_key() +" FROM " + main_table + " m WHERE "
            req = req + "m."+self.get_main_internal_key()+ " NOT IN "
            req = req + "(SELECT DISTINCT( d2."+self.get_main_internal_key()+ ") FROM "+self.get_table_name()+" d2 ) ;"
            curs.execute(req)
            curs.close()
            conn.close()

        
class BiomartMain(BiomartTable):
    
    def __init__(self, project, **args):
        BiomartTable.__init__(self, project, **args)
        self.next_id = 1
 
    def add_link_to_report(self, biomart_field):
        biomart_field.add_link_to_report(self.__class__.__name__.lower()+"report")
 
    def add_row(self, values={}):
        row = "\t"
        if values.has_key(self.__class__.__name__.lower() + "_id_key") :
            row = str(values[self.__class__.__name__.lower() + "_id_key"]) + "\t"
            
        for attribute in sorted(self.__dict__.keys()):
            if isinstance( self.__dict__[attribute], BiomartField ):
                try:
                    row += str(values[attribute]) + "\t"
                except: 
                    row += "\t"
        # add the value in the name_ids
        main_table, main_primary_key = self.get_main_primary_key_info()
        self.name_ids[values[main_primary_key]] = self.next_id
        self.next_id += 1
        dfile = open(self.dump_file, "a")
        dfile.write(row+"\n")
        dfile.close()
    
    def get_mart_display_name(self):
        return self.__class__.__module__.split('.')[-1].lower()
    
    def add(self):
        """ 
        add a single element to the table
        """
        keys, values = "", ""
        for attribute in self._iter_fields():
            values += "\"" + str(attribute) + "\"," if str(attribute) else "NULL,"
            keys += "`" +attribute.name + "`,"
        values = values[:-1]
        keys = keys[:-1]  
        conn = connect(self.db_info["host"], self.db_info["user"], self.db_info["passwd"], self.db_info["dbname"])
        curs = conn.cursor()
        req = "INSERT INTO " + self.get_table_name() + "(" + keys + ") VALUES (" + str(values) + ");"
        curs.execute(req)
        id = conn.insert_id()
        curs.close()
        conn.close()
        return id
    
    def _get_field_primary_key(self):
        return "`"+self.__class__.__name__.lower()+"_id_key` int(10) unsigned NOT NULL AUTO_INCREMENT,"
    
    def _get_index_primary_key(self):
        return "PRIMARY KEY (`" + self.__class__.__name__.lower() + "_id_key`)"
    
    def get_table_name(self):
        return self.project.name + "_" + str(self.__class__).split(".")[-2] + "__" + self.__class__.__name__ + "__main"
    
    def update_values(self, field, dict_key_value):
        """
        @param dict_key_value: dictionary of primary value -> value to update
        @param field : field to update 
        """
        self.delete_dump_file()
        # vars to retrieve the index of main key
        index = 0
        index_main_primary_key = -1     
        # build extraction query without field to update
        main_table, main_primary_key = self.get_main_primary_key_info()
        columns = "`" +self.get_main_internal_key() + "`,"
        for attribute in self._iter_fields():
            if not attribute.name == field : 
                columns += "`" +attribute.name + "`,"
            if attribute.name == main_primary_key : 
                index_main_primary_key = index
            
            index += 1
        columns = columns[:-1]
        conn = connect(self.db_info["host"], self.db_info["user"], self.db_info["passwd"], self.db_info["dbname"])
        curs = conn.cursor()
        req = "SELECT " + columns + " FROM " + self.get_table_name() +";"
        curs.execute(req)        
        # build new dump file        
        for res in curs.fetchall() :
            values={}
            values[self.get_main_internal_key()] = res[0]
            index=1
            for attribute in self._iter_fields():
                if attribute.name == field : 
                    values[field] = dict_key_value[res[index_main_primary_key]] if dict_key_value.has_key(res[index_main_primary_key]) else 0
                    index -=1
                else :
                    if res[index]==None :
                        values[attribute.name] = "\N"
                    else : 
                        values[attribute.name] = res[index]
                index += 1
            self.add_row(values)
        curs.close()
        conn.close()
        self.truncate()
        self.save()
         
    def select_values(self, field):
        # vars to retrieve the index of main key
        index, field_index, index_main_primary_key = 0, -1, -1
        main_table, main_primary_key = self.get_main_primary_key_info()
        for attribute in self._iter_fields():
            if attribute.name == field : 
                field_index = index
            if attribute.name == main_primary_key :
                index_main_primary_key = index
            index += 1
        values = {}
        # because of the internal key
        index_main_primary_key += 1
        field_index += 1
        for line in open(self.dump_file):
            parts = line.rstrip().split("\t")
            values[parts[index_main_primary_key]] = parts[field_index]
        return values

class MartRegistry(object):
    
    def __init__(self, btable, update = False, delete = False):
        '''
            This class builds the mart registry.
            @param btable: an instance of BiomartTable subclass
            @param update: If True, the martregistry will update its option part
            @param delete: If True, the mart in which the table belongs will be deleted
        '''
        self.registry_xml = btable.project.get_xml_path()
        self.marts = {}
        self.root = None
        self.guicontainerNgspipelines = None
        self.guicontainerMartform = None
        self.options = None
        
        if self.registry_xml and os.path.exists(self.registry_xml):
            self.__load_registry(btable)
        else :
            self.__new_registry()
       
        if delete :
            self._delete_mart(btable)
        else :
            if update :
                self.__update_option_filters(btable)
            else :
                self._add_table(btable)

        self._write()
        
    def __load_registry(self, btable):
        '''
            Try to load an existing martregistry from an xml file. If 
            The registry does not contain any mart, a new registry will 
            be created using self.__new_registry()
        '''
        document = None
        try :
            document = ET.parse(self.registry_xml)
        except : #TODO : Un raise exception serait plus adapte, si le fichier est corrompu on perd de l'info en le recreant
            sys.stderr.write( 'Syntax error in your xml file "{0}", building an new one '.format(self.registry_xml) )
            self.registry_xml = btable.project.name + '.xml'
            return self.__new_registry()
        
        self.root = document.getroot()
        self.guicontainerNgspipelines = self.root.find( './portal/guicontainer/guicontainer[@guitype="ngspipelines"]')
        self.guicontainerMartform = self.root.find( './portal/guicontainer/guicontainer[@guitype="martform"]')
        self.options = self.root.find( './options')
        
        if len( self.root.findall( './mart' ) ) == 0 :
            return self.__new_registry()
        
        # create marts from root
        for martElement in self.root.findall( './mart' ) :
            new_mart = Mart(registryRootElement = self.root, martElement= martElement)
            self.marts[new_mart.name] = new_mart
        
    def __new_registry(self):
        '''
            Create an empty registry xml file
        '''
        self.root = ET.fromstring('''
            <martregistry name="martregistry" internalname="martregistry" displayname="martregistry" description="martregistry">
                <portal name="portal" internalname="portal" displayname="portal" description="portal">
                    <users>
                        <group name="anonymous" internalname="anonymous" displayname="anonymous" description="anonymous" password="" hide="false" location="" config="">
                            <user name="anonymous" internalname="anonymous" displayname="anonymous" description="anonymous" password="" hide="false" openid="" />
                        </group>
                    </users>
                    <linkindices />
                    <guicontainer name="root" internalname="root" displayname="root" description="" hide="false" inusers="">
                        <guicontainer name="NGSpipelines" internalname="ngspipelines" displayname="ngspipelines" description="ngspipelines" hide="false" inusers="anonymous" guitype="ngspipelines" />
                        <guicontainer name="default" internalname="default" displayname="Search" description="default" hide="false" inusers="anonymous" guitype="martform"/>
                    </guicontainer>
                </portal>
                <sourcecontainers name="root" internalname="root" displayname="root" description="" hide="false">
                    <sourcecontainer name="default" internalname="default" displayname="default" description="default" hide="false" group="false" />
                </sourcecontainers>
                <options />
            </martregistry>''')
         
        self.guicontainerNgspipelines = self.root.find( './/guicontainer[@guitype="ngspipelines"]')
        self.guicontainerMartform = self.root.find( './/guicontainer[@guitype="martform"]')
        self.options =  self.root.find( './/options')
        
    
    def _add_table(self, btable ):
        '''
            Add a biomart table to this mart registry. BiomartMain tables must be added first, and then its
            associated dimensions. From each BiomartMain will results a Mart
            @param btable: subclass of BiomartTable  
        '''
        from ngspipelines.project import Project
        
        mart = self._get_mart(btable)
        if isinstance(btable, BiomartMain) :
            if mart :
                return
            mart = Mart(main = btable)
            self.root.insert( -1, mart.martRootElement)
        elif isinstance(btable, BiomartDimension):
            if not mart :
                raise Exception( "Can't add BiomartDimension '{0}', You must first create an instance of BiomartMain".format(btable.get_table_name()) )
            mart.add_table(btable)
        
        RDF = None
        for elmt in mart.martRootElement.findall('.//config') :
            if elmt.get('rdfclass').startswith('class:') :
                RDF = elmt.get('name')
                break
        
        if len(self.root.findall('.//martpointer[@name="'+ RDF +'"]')) == 0 :
            if btable.get_real_table_name() not in Project.PROJECT_TABLES :
                self.guicontainerMartform.append(copy.deepcopy(mart.martPointerElement) )
            self.guicontainerNgspipelines.append(mart.martPointerElement)
        
        self.marts[mart.name] = mart

    
    def _delete_mart(self, btable):
        """
            Delete a mart and all assocations 
            @param btable: subclass of BiomartTable
        """
        from ngspipelines.project import Project

        mart = self._get_mart(btable)
        
        if mart :
            mart_name = mart.name
            
            # DELETE MART
            marts = self.root.findall('./mart[@name="' + mart_name + '"]')
            for elmt in marts :
                self.root.remove(elmt)
            
            # DELETE MART POINTER
            guicontainer_ngsp = self.root.find( './/guicontainer[@name="NGSpipelines"][@displayname="ngspipelines"]')
            guicontainer_search = self.root.find( './/guicontainer[@name="default"][@displayname="Search"]')
            
            for parent in [ guicontainer_ngsp,  guicontainer_search] :
                if parent is not None :
                    for elmt in parent.findall('.//martpointer[@mart="' + mart_name + '"]') :
                        parent.remove(elmt)
            
            # DELETE OPTIONS
            options = self.root.find('./options')
            for elmt in options.findall('.//options/mart[@name="' + mart_name + '"]') :
                options.remove(elmt)
            
    
    def _get_mart(self, btable):
        '''
            Return the Mart object associated with this table if it exists.
            Else return None
            @param btable: a BiomartTable object 
            @return: a mart object
        '''
        martname = Mart.get_martname_application(btable)[0]
        if self.marts.has_key(martname):
            return  self.marts[martname]
        return None

    def _get_distinct_filter_values (self, curs, tablename, filtername ) :
        '''
            Return a list of distinctly selected values from a given table, or an empty list
            @param curs    : MySQLdb cursor object
            @param tablename: name of table
            @param filtername : name of the filter to retrieve
        '''
        filtername = filtername.replace( tablename + '__' , '')
        curs.execute("SELECT DISTINCT " + filtername + " FROM " + tablename + " WHERE " + filtername + " IS NOT NULL")
        return curs.fetchall()
    
    def __update_option_filters(self, btable):
        '''
            Update the option part from this biomart table ....
            @param btable: instance of subclass of BiomartTable
        '''
        
        mart = self._get_mart(btable)
        if not mart :
            raise Exception("Can't update table {0}, no associated mart".format(btable.get_table_name()))
        
        conn = connect(btable.project.db_info["host"], btable.project.db_info["user"], 
                       btable.project.db_info["passwd"], btable.project.db_info["dbname"])
        curs = conn.cursor()
        
        for elmt in ET.fromstring(btable.filterRDFContainerElement).findall('.//filter') :
            if elmt.get('type') in [ FilterTypes.SINGLE_SELECT, FilterTypes.SINGLE_SELECT_UPLOAD, FilterTypes.MULTI_SELECT, FilterTypes.MULTI_SELECT_UPLOAD] :
                distinct_values = self._get_distinct_filter_values(curs, btable.get_table_name(), elmt.get('internalname'))
                if distinct_values and len (distinct_values) > 0 and len( mart.martOptionConfElement.findall( './filter[@name="'+elmt.get('name')+'"]') ) == 0 :
                        optionFilter = ET.SubElement( mart.martOptionConfElement, 'filter', name = elmt.get('name') )
                        for disval in distinct_values :
                            ET.SubElement( optionFilter, 'row', data = '|'.join([ str(disval[0]), str(disval[0]),'false']) )
        curs.close()        
        conn.close()
        
        if len( mart.martOptionElement.findall('.//row')) > 0 :
            if len ( self.options.findall('./mart[@name="'+mart.name+'"]') ) == 0 :
                self.options.append(mart.martOptionElement)
    
    def _write(self):
        '''
            Write this mart registry to self.registry_xml
        '''
        for element in self.root.iter():
            element.tail = None
        
        prettyXML = xml.dom.minidom.parseString(ET.tostring(self.root, encoding="UTF-8")).toprettyxml()
        
        # clean xml file, correct bug for python2.7.2
        prettyXML = os.linesep.join([s for s in prettyXML.splitlines() if s.strip()])
        prettyXML = re.sub( r'>\s+([^<\s]{1})', r'>\1', prettyXML)
        prettyXML = re.sub ( r'([^><\s]{1})\s+<', r'\1<', prettyXML)
        ET.ElementTree(ET.fromstring( prettyXML )).write(self.registry_xml , encoding="UTF-8", xml_declaration=True)

class Mart(object):

    MART_ID = 1
    
    @staticmethod
    def get_martname_application(btable):
        '''
            Retrieve the mart name  adn the application type from a biomart table
            @param btable: biomart table
            @return: a tuple
        '''
        application = re.sub( r'_biomart$' , '', btable.__class__.__module__.split('.')[-1].lower(), re.I)
        martname = btable.project.name + '_' + application
        return (martname, application)
    
    def __init__(self, main = None, registryRootElement = None, martElement = None ):
        '''
            Create Mart object from a BiomartTable subclass instance, or from the martElement and the registryRootElement
            @param main: BiomartMain table
            @param registryRootElement: etree.Element (root element of martregistry)
            @param martElement: etree.Element (one mart element)
        '''
        self.tables = {}
    
        if main :
            self.__new_mart(main)
        elif  registryRootElement is not None and martElement is not None:
            self.__load_mart(registryRootElement = registryRootElement, martElement= martElement)
        else :
            raise Exception( 'Cannot build a mart from nothing !!!' )
        
    def __new_mart(self, main ):
        '''
            Creates new empty mart using a main table
            @param main: an instance of BiomartMain 
        '''
        self.mart_group = main.project.species + ' - ' + main.project.name 
        self.name, self.application = Mart.get_martname_application( main )
        self.id = Mart.MART_ID
        Mart.MART_ID += 1
        vals = self.__build_mart(main)
        # root element for registry root
        self.martRootElement = vals[0] 
        # attributes and filters, used when adding a table to the mart
        self.configAttributeContainer = vals[1]
        self.configFilterContainer = vals[2]
        self.configRDFAttributeContainer = vals[3]
        self.configRDFFilterContainer = vals[4]
        # martpointer xml element, to be added to guicontainer
        self.martPointerElement = self._build_mart_pointer()
        # for options in registry root
        self.martOptionElement, self.martOptionConfElement = self._build_filters_options()
        
        self.add_table(main)
    
    def __load_mart(self, registryRootElement = None, martElement= None):
        '''
            load a mart from an xml element
            @param registryRootElement: the registry root element
            @param martElement: the mart xml element
        '''
        self.martRootElement = martElement 
        self.mart_group = self.martRootElement.get('group')
        self.name = self.martRootElement.get('name')
        self.id = int(self.martRootElement.get('id'))
        
        if self.id >  Mart.MART_ID :
            Mart.MART_ID = self.id
        
        self.configAttributeContainer = None
        self.configFilterContainer = None
        self.configRDFAttributeContainer = None
        self.configRDFFilterContainer = None
        
        for elmt in self.martRootElement.findall('./config') :
            if elmt.get('rdfclass') == "" :
                self.configAttributeContainer = elmt.find('./container/container[@name="attribute"]')
                self.configFilterContainer = elmt.find('./container/container[@name="filter"]')
            elif elmt.get('rdfclass').startswith('class:') :
                self.configRDFAttributeContainer = elmt.find('./container/container[@name="attribute"]')
                self.configRDFFilterContainer = elmt.find('./container/container[@name="filter"]')

        # martpointer xml element, to be added to guicontainer
        self.martPointerElement = self._build_mart_pointer()
        
        # for options in registry root
        if len (registryRootElement.findall('./options/mart[@name="'+self.name+'"]') ) > 0 :
            self.martOptionElement = registryRootElement.find('./options/mart[@name="'+self.name+'"]')
            self.martOptionConfElement = self.martOptionElement.find('./config')
        else :
            self.martOptionElement, self.martOptionConfElement = self._build_filters_options()
        
        for elmt in self.martRootElement.findall('./table') :
            self.tables[elmt.get('name')] = 'table readed from xml file {0}'.format(elmt.get('name'))
        
    def add_table(self, btable):
        '''
            Add a biomart table to this mart
            @param btable: a BiomartTable
        '''
        if self.tables.has_key(btable.get_table_name()):
            return
        
        self.tables[btable.get_table_name()] = btable
        
        config_name = self.martRootElement.find('./config[@rdfclass=""]')[0].get('name')
        config_rdf_name = None
        rdf_class = None
        for elmt in self.martRootElement.findall('./config') :
            if elmt.get('rdfclass').startswith('class:') :
                config_rdf_name = elmt.get('name')
                rdf_class = elmt.get('rdfclass')
                break 

        inpartition = self.name + '_' + btable.project.db_info["user"]
        
        btable_tableElement = ET.fromstring( btable.tableElement )
        btable_attributeContainerElement =  ET.fromstring( btable.attributeContainerElement )
        btable_filterContainerElement = ET.fromstring( btable.filterContainerElement )
        btable_newFilterContainer = ET.fromstring( btable.newFilterContainer )
        btable_attributeRDFContainerElement =  ET.fromstring( btable.attributeRDFContainerElement )
        btable_filterRDFContainerElement = ET.fromstring( btable.filterRDFContainerElement)
        
        btable_tableElement.set('inpartitions', inpartition)
        for elmt in btable_tableElement.findall('.//column') :
            elmt.set('inpartitions', inpartition)
        
        for elmt in btable_attributeContainerElement.findall('.//attribute') + btable_filterContainerElement.findall('.//filter') \
                + btable_attributeRDFContainerElement.findall('.//attribute') + btable_filterRDFContainerElement.findall('.//filter')\
                + btable_newFilterContainer.findall('.//filter'):
            elmt.set( 'mart', self.name)
            elmt.set( 'config', config_name)
            
            if elmt.get('linkouturl') :
                elmt.set( 'linkouturl', '/ngspipelines/report.jsp?id=%s%&root=NGSpipelines&datasetType=' + self.application + '&plugin=' + elmt.get('linkouturl') + '&mart=' + self.mart_group )
        
        self.martRootElement.insert(0, btable_tableElement)
        self.configAttributeContainer.append( btable_attributeContainerElement )
        self.configFilterContainer.append( btable_filterContainerElement )
        
        if len( btable_newFilterContainer.findall('./filter')  ) > 0 :
            if len( self.configFilterContainer.findall('./container[@name="newFilter"]')) == 0 :
                self.configFilterContainer.insert(-1, btable_newFilterContainer)
            else :
                for elmt in btable_newFilterContainer.findall('./filter') :
                    self.configFilterContainer.find('./container[@name="newFilter"]').append(elmt)
        
        for elmt in self.configFilterContainer.findall('./container[@name="newFilter"]/filter') :
            elmt.set('rdf' , ';'.join( [rdf_class, "attribute:" + elmt.get('attribute'), "rdf:PlainLiteral"]  ))
            
        self.configRDFAttributeContainer.append( btable_attributeRDFContainerElement) 
        self.configRDFFilterContainer.append( btable_filterRDFContainerElement ) 
                
        for elmt in self.configRDFFilterContainer.findall( './/filter' ) + self.configRDFAttributeContainer.findall( './/attribute' ) :
            if elmt.tag == 'filter' :
                elmt.set('rdf' , ';'.join( [rdf_class, "attribute:" + elmt.get('attribute'), "rdf:PlainLiteral"]  ))
            elmt.set('config', config_rdf_name )
        
        if isinstance(btable, BiomartDimension) :
            main_table_name, k = btable.get_main_primary_key_info()
            firsttable = main_table_name
            secondtable = btable.get_table_name()
            genericname = firsttable + '__' + secondtable
            internal_key = btable.get_main_internal_key()
            
            ET.SubElement( self.martRootElement, 'relation', dict( name = genericname, internalname = genericname, displayname = genericname, description = genericname, 
                           firsttable = firsttable, secondtable = secondtable, firstkey = internal_key + "_pk", secondkey = internal_key + "_fk", type = "1:M"))
        
        
        if len ( self.configRDFFilterContainer.findall('.//filter') ) == 0 :
            self.configRDFFilterContainer.set('hide', 'true')
        else :
            self.configRDFFilterContainer.set('hide', 'false')
            
    def __build_mart(self, main):
        '''
            Define mart xml
            @param main: BiomartMain
        '''
        config_rdf_name = self.name + '_config'
        rdf_class = 'class:'+ config_rdf_name
        params = dict(
              # database
              host = main.project.db_info["host"], port = main.project.db_info["port"], database = main.project.db_info["dbname"], 
              user = main.project.db_info["user"],  pwd = main.project.db_info["passwd"], appli = main.get_mart_display_name(),
              # other  
              martname = self.name, martid  = self.id, martgroup = self.mart_group,
              unique_id = main.get_table_name() + '__' + main.get_main_internal_key(),
              rdf_class = rdf_class,
              datasetdisplayname = main.get_mart_display_name(),
              config_rdf_name = config_rdf_name)
        
        martRootElement = ET.fromstring( '''
            <mart name="{martname}" internalname="{martname}" displayname="{martname}" description="{martname}" hide="false" id="{martid}" group="{martgroup}" optimiser="NONE" indexoptimised="false" virtual="" searchfromtarget="">
                <partitiontable name="p0" internalname="p0" displayname="p0" description="p0" rows="1" cols="15" type="SCHEMA">
                    <row id="0">jdbc:mysql://{host}:{port}/|{database}|{database}|{user}|{pwd}|{martname}_{user}|false|{appli}|0.8||||||</row>
                </partitiontable>
                <config rdf="" processor="" name="{martname}" internalname="{martname}" displayname="{martname}" description="{martname}" hide="false" default="" metainfo="" datasethidevalue="(p0c6)" datasetdisplayname="(p0c7)" master="true" readonly="" password="" rdfclass="">
                    <container name="root" displayname="root" internalname="root" description="root" hide="false" maxcontainers="" maxattributes="" independentquerying="">
                        <container name="attribute" displayname="attribute" internalname="attribute" description="attribute" hide="false" maxcontainers="" maxattributes="" independentquerying=""/>
                        <container name="filter" displayname="filter" internalname="filter" description="filter" hide="false" maxcontainers="" maxattributes="" independentquerying=""/>
                    </container>
                </config>
                <config rdf="" processor="" name="{config_rdf_name}" internalname="{martname}" displayname="{config_rdf_name}" description="{martname}" hide="false" default="" metainfo="" datasethidevalue="(p0c6)" datasetdisplayname="(p0c7)" master="false" readonly="" password="" rdfclass="{rdf_class}">
                    <rdfclass name="{rdf_class}" internalname="{rdf_class}" displayname="{rdf_class}" description="{rdf_class}" hide="false" value="" uniqueid="{unique_id}" />
                    <container name="root" displayname="root" internalname="root" description="root" hide="false" maxcontainers="" maxattributes="" independentquerying="">
                        <container name="attribute" displayname="attribute" internalname="attribute" description="attribute" hide="false" maxcontainers="" maxattributes="" independentquerying=""/>
                        <container name="filter" displayname="filter" internalname="filter" description="filter" hide="false" maxcontainers="1" maxattributes="" independentquerying=""/>
                    </container>
                </config>
            </mart>
        '''.format(**params))
        
        configAttributeContainer = None
        configFilterContainer = None
        configRDFAttributeContainer = None
        configRDFFilterContainer = None
        
        for elmt in martRootElement.findall('./config') :
            if elmt.get('rdfclass') == "" :
                configAttributeContainer = elmt.find('./container/container[@name="attribute"]')
                configFilterContainer = elmt.find('./container/container[@name="filter"]')
        
            elif elmt.get('rdfclass').startswith('class:') :
                configRDFAttributeContainer = elmt.find('./container/container[@name="attribute"]')
                configRDFFilterContainer = elmt.find('./container/container[@name="filter"]')

        return (martRootElement, configAttributeContainer, configFilterContainer, configRDFAttributeContainer, configRDFFilterContainer)
        
    def _build_mart_pointer(self):
        '''
            Martpointer is builded from here. This martpointer is not part of 
            the martElement, it must be added to guicontainers.
        '''
        rdf_config_name = None
        
        for elmt in self.martRootElement.findall('./config') :
            if elmt.get('rdfclass').startswith('class:'):
                rdf_config_name = elmt.get('name')
            
        return ET.fromstring( '''
            <martpointer group="{group}" name="{rdf}" internalname="{rdf}" displayname="{rdf}" description="{rdf}" mart="{mart}" config="{rdf}" operation="singleselect" icon="" independentquerying="false" inusers="anonymous">
                <processorgroup name="Tabular" internalname="Tabular" displayname="Tabular" description="Tabular" default="HTML">
                    <processor name="HTML" internalname="HTML" displayname="HTML" description="HTML" containers="root" export="HTML,XLS" />
                    <processor name="CSV" internalname="CSV" displayname="CSV" description="CSV" containers="root" export="CSV,XLS" />
                    <processor name="TSV" internalname="TSV" displayname="TSV" description="TSV" containers="root" export="TSV,XLS" />
                </processorgroup>
                <processorgroup name="Sequences" internalname="Sequences" displayname="Sequences" description="Sequences" default="FASTA">
                    <processor name="FASTA" internalname="FASTA" displayname="FASTA" description="FASTA" containers="root" export="FASTA,TSV" />
                    <processor name="GFF" internalname="GFF" displayname="GFF" description="GFF" containers="root" export="GFF,CSV" />
                </processorgroup>
                <processorgroup name="Graphs" internalname="Graphs" displayname="Graphs" description="Graphs" default="KAPLA">
                    <processor name="KAPLA" internalname="KAPLA" displayname="KAPLA" description="KAPLA" containers="root" export="KAPLA" />
                </processorgroup>
            </martpointer>
        '''.format(group = self.mart_group, rdf = rdf_config_name, mart = self.name) )
    
    def _build_filters_options(self):
        '''
            Build all filters option values. those options are defined depending on 
            what's present on the database. So the xml must be updated each time a new
            distinct value is added to the database 
        '''
        martOptionElement = ET.Element('mart', name = self.name )
        martOptionConfElement = ET.SubElement( martOptionElement, 'config', name = self.name)
        
        return( martOptionElement, martOptionConfElement)
