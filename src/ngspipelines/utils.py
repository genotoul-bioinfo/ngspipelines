import string, gzip
from shutil import copyfile
import os
import math
import functools

class Utils(object):
    """
    Class Utils: in charge to all the programme utilities
    """
    # Files with this extensions won't be compressed if asked
    DO_NOT_COMPRESS_EXTENSION = [".tgz", ".gz", ".zip", ".bai", ".bam", ".bz", ".bz2", ".csv",".gff",".gff3"]
    
    
    @staticmethod
    def gzip(file, out_dir, file_name, delete=False):
        """
          Gzip compress the given file.
          @param file    : the file to gzip 
          @param out_dir : the out directory where to store the gziped file
          @param delete  : delete files     
          @return        : the compressed file path
        """
        # If the file is not already a compressed file
        if os.path.splitext(file)[1] not in Utils.DO_NOT_COMPRESS_EXTENSION:
            f_in = open(file, 'rb')
            f_out_name = os.path.join(out_dir, os.path.basename(file_name)) + ".gz"
            f_out = gzip.open(f_out_name, 'wb')
            f_out.writelines(f_in)
            f_out.close()
            f_in.close()
        # Otherwise, just copy it
        else:
            f_out_name = os.path.join(out_dir, os.path.basename(file_name))
            copyfile(file, f_out_name)
        
        # Finaly try to delete the original file if asked to do so
        try:
            if delete:
                os.remove(file)
        except:
            pass
        return f_out_name

    @staticmethod
    def get_filepath_by_prefix( path_list, prefixes=None ):
        """
        Gather files path with same prefix. Ex :
        list [/home/sample1_L002.fastq, /home/sample2_L002.fastq, /home/sample1_L003.fastq] 
        with prefixes [sample1, sample2]
        return {'sample1':[/home/sample1_L002.fastq, /home/sample1_L003.fastq], 'sample2':[/home/sample2_L002.fastq]}
          @param path_list : the list of files
          @param prefixes : prefix to gather , if prefix is not set, group by basename_woext
        """
        path_groups = {}
        
        if prefixes is None :
            prefixes=[]
            for file_path in path_list:
                current_prefix=os.path.basename(file_path).split(".")[0]
                if path_groups.has_key(current_prefix):
                    path_groups[current_prefix].append(file_path)
                else :
                    path_groups[current_prefix]=[file_path]
                
        else :
            
            for current_prefix in prefixes:
                path_groups[current_prefix] = []           
                for file_path in path_list:
                    if os.path.basename(file_path).startswith(current_prefix):
                        path_groups[current_prefix].append(file_path)
            
        return path_groups
    
def percentile(N, percent, key=lambda x:x):
    """
    Find the percentile of a list of values.

    @parameter N - is a list of values. Note N MUST BE already sorted.
    @parameter percent - a float value from 0.0 to 1.0.
    @parameter key - optional key function to compute value from each element of N.

    @return - the percentile of the values
    """
    if not N:
        return None
    k = (len(N)-1) * percent
    f = math.floor(k)
    c = math.ceil(k)
    if f == c:
        return key(N[int(k)])
    d0 = key(N[int(f)]) * (c-k)
    d1 = key(N[int(c)]) * (k-f)
    return d0+d1

median = functools.partial(percentile, percent=0.5)
min = functools.partial(percentile, percent=0)
max = functools.partial(percentile, percent=1)
first_quartile = functools.partial(percentile, percent=0.25)
third_quartile = functools.partial(percentile, percent=0.75) 
