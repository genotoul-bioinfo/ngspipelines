#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import inspect
import os
from ConfigParser import ConfigParser


class NGSPipelinesConfigReader(object):
    """
    """
    
    CONFIG_FILE_PATH = "../../application.properties"
    INSTANCES_DIR="instances"
    
    def __init__(self):
        """ 
        """
        self.reader = ConfigParser()
        self.full_config_file_path = os.path.join(os.path.dirname(inspect.getfile(self.__class__)), self.CONFIG_FILE_PATH)
        self.reader.read(self.full_config_file_path)
        
    def get_database_info(self):
        return {"host": self.reader.get("database", "host"),
                "user": self.reader.get("database", "user"),
                "passwd": self.reader.get("database", "passwd"),
                "dbname": self.reader.get("database", "dbname"),
                "port": self.reader.get("database", "port")}
        
    def get_save_directory(self):
        return self.reader.get("storage", "save_directory")
    
    def get_tmp_directory(self):
        return self.reader.get("storage", "tmp_directory")

    def get_instance_directory(self, instance_name):
        return os.path.join(self.get_save_directory(), instance_name.replace(" ", ""))
            
    def get_work_instance_directory(self, instance_name):
        return os.path.join(self.reader.get("storage", "work_directory"), self.INSTANCES_DIR, instance_name.replace(" ", ""))
    