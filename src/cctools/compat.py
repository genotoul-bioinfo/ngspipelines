# Copyright (c) 2011- The University of Notre Dame.
# This software is distributed under the GNU General Public License.
# See the file COPYING for details.

""" CCTools Compatibility module """

__all__ = ['map']

try:
    from itertools import imap
    map = imap
except ImportError:
    map = map

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
