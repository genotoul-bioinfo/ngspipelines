package org.biomart.processors.ngspipelines;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import org.biomart.processors.ProcessorImpl;
import org.biomart.processors.annotations.ContentType;
import org.biomart.processors.ngspipelines.utils.DBObject;
import org.biomart.queryEngine.Query;

@ContentType("application/json")
public class PluginJSFiles extends ProcessorImpl {

	private OutputStream originalOut;
	private File controllerDir;
	private File modelDir;
	private DBObject database;
	
    @Override
	public void beforeQuery(Query query, OutputStream outputHandle) {
    	database = new DBObject(query);
    	originalOut = outputHandle ;
    	out = new ByteArrayOutputStream() ;
	}
    
    @Override
    public void afterQuery() {
    	
        try {
            // then get the plugin name from the filter values
            String pluginName = database.getFilterValue("_project__Project__main__directory");
            String subDir = database.getFilterValue("_project__Project__main__name");
            if (!subDir.equals("")) { subDir = subDir + "/"; }
    		String curDir = System.getProperty("user.dir");
    		controllerDir = new File(curDir,"plugins/ngspipelines/public/ngspipelines/js/controller/" + pluginName + "/" + subDir);
    		modelDir = new File(curDir,"plugins/ngspipelines/public/ngspipelines/js/model/" + pluginName + "/" + subDir);
    		
    		if (controllerDir != null && controllerDir.isDirectory() && modelDir != null && modelDir.isDirectory()) {
    			
    			File[] controllerFiles = controllerDir.listFiles();
    			String fileList = "";
    			for (int i = 0; i < controllerFiles.length; i++) {
    				if (!controllerFiles[i].isDirectory()) {
    					String name = controllerFiles[i].getName() ;
    					fileList += "\"js/controller/" + pluginName + "/" + subDir + name + "\",";
    				}
    			}
    			File[] modelFiles = modelDir.listFiles();
    			for (int i = 0; i < modelFiles.length; i++) {
    				if (!modelFiles[i].isDirectory()) {
    					String name = modelFiles[i].getName() ;
    					fileList += "\"js/model/" + pluginName + "/" + subDir + name + "\",";
    				}
    			}
    			fileList = fileList.substring(0, fileList.length() - 1);
    			originalOut.write( ("{\"files\":[").getBytes() );
    			originalOut.write( (fileList).getBytes() );
    			originalOut.write( ("]}").getBytes() );
    			
    		} else {
    			originalOut.write( ( " { \"error\" : \"The requested plugin " + pluginName + " does not exist!\" }  " ).getBytes());
    		}

        } catch (Exception e) {
        	 try {
        		 originalOut.write( ( " { \"error\" : \"" + e.getMessage() +"\" }  " ).getBytes());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
        }
    }

    
}
