package org.biomart.processors.ngspipelines;

import java.io.File;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.biomart.common.exceptions.BioMartException;
import org.biomart.processors.ProcessorImpl;
import org.biomart.processors.annotations.ContentType;
import org.biomart.processors.ngspipelines.utils.DBObject;
import org.biomart.processors.ngspipelines.utils.NGSPUtils;
import org.biomart.processors.ngspipelines.utils.NGSPipelinesConfigReader;
import org.biomart.processors.ngspipelines.utils.RC6OutputStream;
import org.biomart.processors.ngspipelines.utils.Utils;
import org.biomart.queryEngine.Query;

@ContentType("application/json")
public class ExportFlankingSequences extends ProcessorImpl {
    
	private String towrite = "";
	OutputStream originalOut;
	String contigName ;
	private DBObject dbo;
	
	
    @Override
	public void beforeQuery(Query query, OutputStream outputHandle) {
    	dbo = new DBObject(query);
    	originalOut = outputHandle;
		out = new ExportFlankingSequencesOutputStream(outputHandle);
	}
    
    private class ExportFlankingSequencesOutputStream extends RC6OutputStream {

		public ExportFlankingSequencesOutputStream(OutputStream out) {
			super(out);
		}

		@Override
		protected void apply(String[] columns) {
			try {
            	
				if (contigName == null){
					contigName = columns[0];
				}
				
            	String line = columns[0] + "\t" + columns[3] + "\t" + columns[1] + "\t" + columns[5] + "\t" + columns[2];             	
            	towrite += line +"\n";
            } catch (Exception e) {
            	throw new BioMartException(e);
            }
		}
    };
    
    
    public void afterQuery() {
    	try {
    		Connection connectionObj = dbo.getConnection();
    		String projectTable = NGSPUtils.getProjectTableName(dbo.getProjectName());
			NGSPipelinesConfigReader config = new NGSPipelinesConfigReader();
			String storageDir = config.getStorageDirectory();
			
			String projectDir = "";
			String selectProjectDirQuery = "SELECT directory FROM " + projectTable + ";" ;
			Statement sth = connectionObj.createStatement();

			ResultSet rs = sth.executeQuery(selectProjectDirQuery);
		    while (rs.next()) {
		    	projectDir = rs.getString("directory");
		    }
			
		    String outTempDirName = storageDir + Utils.FSEP + projectDir + Utils.FSEP + Utils.TEMP_DIR;
		    File outTempDir = new File(outTempDirName);
		    
		    if ( ! outTempDir.exists() ){
		    	outTempDir.mkdirs();
		    }
		    
			File tempFastaFile = new File( outTempDir + Utils.FSEP + contigName + "_snp_flanking_sequence.tsv");
			
			if( !tempFastaFile.exists() ){
				PrintWriter pout = new PrintWriter(new FileWriter( tempFastaFile ));
		        pout.print(towrite);
				pout.close();
			}
			
	    	Utils.silentWrite("{  \"snp_file_path\" : \"" + projectDir + Utils.FSEP + Utils.TEMP_DIR + Utils.FSEP + tempFastaFile.getName() + "\"  }", originalOut);
	    	
	    	tempFastaFile.deleteOnExit();
    		
		} catch (Exception e) {
			Utils.writeMessage(e.getMessage(), out);
			e.printStackTrace();
		}
    }
}