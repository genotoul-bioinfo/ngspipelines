package org.biomart.processors.ngspipelines;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import org.biomart.common.exceptions.BioMartException;
import org.biomart.processors.ProcessorImpl;
import org.biomart.processors.annotations.ContentType;
import org.biomart.processors.ngspipelines.utils.RC6OutputStream;
import org.biomart.queryEngine.Query;

@ContentType("application/json")
public class DiskSpace extends ProcessorImpl {

    private String dataDir;
    
    public DiskSpace() {
        String curDir = System.getProperty("user.dir");
        String pluginsDir = new File(curDir, "plugins").toString();
        String ngsDir = new File(pluginsDir, "ngspipelines").toString();
        String publicDir = new File(ngsDir, "public").toString();
        String ngspDir = new File(publicDir, "ngspipelines").toString();
        dataDir = new File(ngspDir, "data").toString();
    }

    @Override
    public void beforeQuery(Query query, OutputStream outputHandle) throws IOException  {
        out = new DiskSpaceOutputStream(outputHandle);
    }
    
    private class DiskSpaceOutputStream extends RC6OutputStream {
    	
		public DiskSpaceOutputStream(OutputStream out) {
			super(out);
		}
		
		private long getFileSize(File folder) {
    		long foldersize = 0;
    		File[] filelist = folder.listFiles();
    		for (int i = 0; i < filelist.length; i++) {
    			if (filelist[i].isDirectory()) {
    				foldersize += getFileSize(filelist[i]);
    			} else {
    				foldersize += filelist[i].length();
    			}
    		}
    		return foldersize;
    	}

		@Override
		protected void apply(String[] columns) {
			String projectDir = new File(dataDir, columns[0]).toString();
       		long fileSizeByte = this.getFileSize(new File(projectDir));
       		
       		try {
                out.write( ("{\"size\": \"" + fileSizeByte + "\" }").getBytes());
    		} catch (Exception e) {
    			throw new BioMartException("DiskSpace : An error occurred during JSON creation", e);
    		}
		}
    }
}
