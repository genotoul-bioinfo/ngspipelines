package org.biomart.processors.ngspipelines.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Map.Entry;
public class NGSPUtils {

	/**
	 * 
	 * @param projectName
	 * @param application
	 * @param className
	 * @param isMain
	 * @return
	 */
	public static String getTableName(String projectName, String application, String className, Boolean isMain){
		return projectName + "_" + application + "__" + className + "__" + (( isMain ) ? "main" : "dm");
	}
	
	/**
	 * Return the project general info table name
	 * @param project : the project name
	 * @return
	 */
	public static String getProjectTableName( String project){
		return project + "_project__Project__main" ;
	}

	/**
	 * Return the project library info table name
	 * @param project: the project name
	 * @return
	 */
	public static String getLibraryTableName(String project){
		return project + "_library__Library__main" ;
	}

	/**
	 * Return the project analysis info table name
	 * @param project: the project name
	 * @return
	 */
	public static String getAnalysisTableName(String project){
		return project + "_analysis__Analysis__main" ;
	}
	
	/**
	 * Return the project general info table name
	 * @param project: the project name
	 * @return
	 */
	public static String getAnalysisResultTableName(String project){
		return project + "_analysis__Result__dm" ;
	}
	
	/**
	 * Add a new analysis to the analysis table
	 * @param connection : sql connection 
	 * @param project : project name
	 * @param params : parameters
	 * @return : analysis id key
	 * @throws Exception 
	 */
	public static int addAnalaysis( Connection connection, String project, Hashtable<String, Object> params) throws Exception{
		Hashtable<String, Object> mparams = new Hashtable<String, Object>();
		mparams.put("analysis_hidden", 0);
		mparams.put("analysis_name", "");
		mparams.put("analysis_type", "");
		mparams.put("categorie", "");
		mparams.put("comments", "");
		mparams.put("directory", "");
		mparams.put("parent_id", 0);
		mparams.put("soft_name", "");
		mparams.put("soft_parameters", "");
		mparams.put("soft_version","");	
		
		String[] requiredKeys = new String[]{ "analysis_name", "analysis_type", "categorie", "parent_id", "analysis_hidden" };
		
		for( Entry<String, Object> entry : params.entrySet()){
			if (!mparams.containsKey(entry.getKey())){
				throw new Exception( "Unknown parameter " + entry.getKey() + " for add analysis");
			}
			mparams.put(entry.getKey(), entry.getValue());
		}
		
		for (String k : requiredKeys){
			if(!mparams.containsKey(k)){
				throw new Exception( "missing required parameter " + k + " for add analysis");
			}
		}
		
		String query = "INSERT INTO " + NGSPUtils.getAnalysisTableName(project) + "(analysis_hidden, analysis_name, analysis_type, categorie," +
				"comments, directory, parent_id, soft_name, soft_parameters, soft_version, time) VALUES (" +
				"'" + mparams.get("analysis_hidden") + "' ," +
				"'" + mparams.get("analysis_name") + "' ," +
				"'" + mparams.get("analysis_type") + "' ," +
				"'" + mparams.get("categorie") + "' ," +
				"'" + mparams.get("comments") + "' ," +
				"'" + mparams.get("directory") + "' , " +
				"'" + mparams.get("parent_id") + "' , " +
				"'" + mparams.get("soft_name") + "' , " +
				"'" + mparams.get("soft_parameters") + "' , " +
				"'" + mparams.get("soft_version") + "' , " +
				"'" + Utils.getCurrentDate() + "'" +
				" ) ;";
		
		Statement sth = connection.createStatement();
		sth.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
		ResultSet rs = sth.getGeneratedKeys();
		Integer key = null;
		while (rs.next()) {
			key = rs.getInt(1);
		}
    	return key;
    }
	
	/**
	 * Add a new result to an analysis into the result analysis table
	 * @param connection : sql connection 
	 * @param project : project name
	 * @param analysis_id_key : int
	 * @param key : String
	 * @param value : String
	 * @param type : String
	 * @param group : String
	 * @return : Void
	 * @throws Exception 
	 */
	public static void addResultAnalysis( Connection connection, String project, Integer analysis_id_key, String group, String key, String type, String value) throws Exception{
		
		String sql = "INSERT INTO " + NGSPUtils.getAnalysisResultTableName(project) + " (`analysis_id_key` ,`group` ,`key` ,`type` ,`value`) VALUES (" 
			+ analysis_id_key + ", '" + group + "', '"  + key + "', '" + type + "', '" + value + "');";
    	Statement sth = connection.createStatement();
		sth.executeUpdate(sql);
			
	}
	
}
