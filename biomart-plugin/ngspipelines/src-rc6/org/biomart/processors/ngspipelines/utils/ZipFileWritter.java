package org.biomart.processors.ngspipelines.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.zip.Adler32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;


import com.google.common.io.Files;

/**
 * @author Fobec 2011
 */
public class ZipFileWritter {

	private ZipOutputStream zos;
	private ZipFile tempsZipFile;
	
	private String zipFilePath;
	private String tmpZipFilePath;
	
	private static final byte[] BUFFER = new byte[4096 * 1024];

    /**
     * copy input to output stream - available in several StreamUtils or Streams classes 
     */    
    private void copy(InputStream input, OutputStream output) throws IOException {
        int bytesRead;
        while ((bytesRead = input.read(BUFFER))!= -1) {
            output.write(BUFFER, 0, bytesRead);
        }
    }
    
	/**
	 * @param zipFile
	 * @throws IOException 
	 * @throws ZipException 
	 */
	public ZipFileWritter(String zipFile) throws ZipException, IOException {
		zipFilePath = zipFile ;
		File zipi = new File(zipFile);
		if (zipi.exists()){
			File tempZip = File.createTempFile("append" + Utils.generateString(5), ".zip");
			Files.copy(zipi,tempZip);
			tmpZipFilePath = tempZip.getAbsolutePath();
			
			ZipFile war = new ZipFile(tempZip);
			zos = new ZipOutputStream(new FileOutputStream(zipi));
			// first, copy contents from existing war
	        Enumeration<? extends ZipEntry> entries = war.entries();
	        while (entries.hasMoreElements()) {
	            ZipEntry e = entries.nextElement();
	            zos.putNextEntry(e);
	            if (!e.isDirectory()) {
	                copy(war.getInputStream(e), zos);
	            }
	            zos.closeEntry();
	        }
		}
		else{
			CheckedOutputStream checksum = new CheckedOutputStream(new FileOutputStream(zipi),	new Adler32());
			this.zos = new ZipOutputStream(new BufferedOutputStream(checksum));
		}
	}

	/**
	 * Add a new file to the zip.
	 * @param fileName
	 * @param delete : if true the file will be deleted after
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void addFile(String fileName, boolean delete) throws FileNotFoundException,	IOException {
		FileInputStream fis = new FileInputStream(fileName);
		int size = 0;
		byte[] buffer = new byte[1024];

		File file = new File(fileName);
		ZipEntry zipEntry = new ZipEntry(file.getName());
		this.zos.putNextEntry(zipEntry);

		while ((size = fis.read(buffer, 0, buffer.length)) > 0) {
			this.zos.write(buffer, 0, size);
		}

		this.zos.closeEntry();
		fis.close();
	}
	
	/**
	 * Add a new file to the zip. The file is not deleted afterwards
	 * @param fileName
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void addFile(String fileName) throws FileNotFoundException, IOException{
		this.addFile(fileName, false);
	}

	public void close() throws IOException {
		if(this.tempsZipFile != null){
			this.tempsZipFile.close();
			File src = new File(tmpZipFilePath);
			File dest = new File(zipFilePath) ;
			Files.copy(src, dest);
			src.delete();
		}
		else{
			this.zos.close();
		}
	}
}