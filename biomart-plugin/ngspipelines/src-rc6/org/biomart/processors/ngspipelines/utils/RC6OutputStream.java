package org.biomart.processors.ngspipelines.utils;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.biomart.common.constants.OutputConstants;

public abstract class RC6OutputStream  extends FilterOutputStream implements OutputConstants{

	public RC6OutputStream(OutputStream out) {
		super(out);
	}
	
	@Override
	public void write(byte[] bytes) throws IOException {
		String row = new String(bytes).trim();
		if ("".equals(row)) {
			return;
		}
		String[] columns = row.split(DELIMITER);
		apply(columns);
	}
	
	/**
	 * callback function
	 * @param columns
	 */
	protected abstract void apply (String[] columns);
}
