package org.biomart.processors.ngspipelines.utils;

import java.util.Hashtable;


public class ParametersReader {

	private Hashtable<String, Object> params ;

	/**
	 * Construct a hash table from a JSON
	 * @param jsonString
	 */
	public ParametersReader(String jsonString){
		params = Utils.jsonToHash(jsonString);
	}

	/**
	 * Retrieve parameter value from a key
	 * @param key
	 * @return
	 * @throws Exception 
	 */
	public Object get(String key) throws Exception{
		return params.get(key);
	}
	
	
	/**
	 * Retrieve parameter value from a key
	 * @param key
	 * @return
	 */
//	public <T> T get(String key){
//		if(params.containsKey(key)){
//			return (T)params.get(key);
//		}
//		return null;
//	}
//	
	/**
	 * Add a new parameter to this param reader
	 * @param key
	 * @param value
	 */
	public void add(String key, Object value){
		params.put(key, value);
	}
	
	/**
	 * Return all parameters as a Hashtable
	 * @return
	 */
	public Hashtable<String, Object> getParams() {
		return params;
	}
	
	/**
	 * True if the parameter exists
	 * @param key
	 * @return
	 */
	public boolean hasParam(String key){
		return params.containsKey(key);
	}
}
