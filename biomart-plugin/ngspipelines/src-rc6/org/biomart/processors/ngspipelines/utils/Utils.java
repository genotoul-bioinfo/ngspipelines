package org.biomart.processors.ngspipelines.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Random;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.codehaus.jackson.map.ObjectMapper;


public class Utils {
	private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
	private static final Random rng = new Random();
	public static final String FSEP = System.getProperty("file.separator");
	public static final String TEMP_DIR = ".temp";
	
	private static final Logger LOGGER = Logger.getLogger(Utils.class.getName());
	private static final File LOGGER_FILE = getLoggerFile();
	private static FileHandler logFh;
	
	/**
	 * 
	 * @param dir
	 * @param ext
	 * @return
	 */
	public static ArrayList<File> listFiles(String dir, final String ext){
		FilenameFilter fileNameFilter = new FilenameFilter() {
			@Override
			public boolean accept(File arg0, String fileName) {
				return (fileName.endsWith(ext));
			}
		};
		File parentDir = new File(dir);
		return new ArrayList<File>(Arrays.asList(  parentDir.listFiles(fileNameFilter) ));
	}
	
	public static ArrayList<File> listFiles(String dir, ArrayList<String> exts){
		ArrayList<File> files = new ArrayList<File>();
		for (String str : exts){
			files.addAll( listFiles(dir, str));
		}
		return files;
	}
	
	/**
	 * Get the logger file
	 * @return
	 */
	private static final File getLoggerFile(){
		try {
			return File.createTempFile("processors-log-" + generateString(5), ".tmp");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null ;
	}
	
	/**
	 * Generate a random string
	 * 
	 * @param length
	 * @return
	 */
	public static String generateString(int length) {
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = CHARACTERS.charAt(rng.nextInt(CHARACTERS.length()));
		}
		return new String(text);
	}
	
	public static void log(String message){
		Utils.log(message, null);
	}
	public static void log(Throwable t){
		Utils.log("",t);
	}
	
	public static void log(String message, Throwable e){
		
		if (logFh == null){
			try {
				logFh  = new FileHandler( LOGGER_FILE.getAbsolutePath());
				LOGGER.addHandler(logFh);
				logFh.setFormatter( new SimpleFormatter());  
			} catch (Exception ee) {
				ee.printStackTrace();
			}  
		}
		if (e != null){
			LOGGER.log(Level.INFO, message, e);
		}
		else{
			LOGGER.info(message);
		}
	}

	
	public static String getLogFilePaht(){
		return LOGGER_FILE.getAbsolutePath();
	}
	
	/**
	 * Create random directory and Check if not exist
	 * @param directory : directory in which random dir will be create.
	 * @param length
	 * @return
	 */
	public static String mkRandomDir(String directory , int length ){
			
		String generatedDir = Utils.generateString(10);
		File outDir = new File( directory + FSEP + generatedDir);
		
		while( outDir.exists()){
			generatedDir = Utils.generateString(10);
			outDir = new File( directory + FSEP + generatedDir);
		}
		
		outDir.mkdirs();
		return (generatedDir);
	}
	
	/**
	 * Send an email
	 * 
	 * @param from : the sender of the mail
	 * @param to : the address of the recipient
	 * @param  subject : the subject for the mail
	 * @param content  : the content of the mail
	 * @throws MessagingException
	 */
	public static void sendMail(String from, String to, String subject, String content)	throws MessagingException {
		
		String lFrom =  ( from == null )? "ngspipeline" : from;
		String lContent = ( content == null ) ? "" : content;
		
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.host", "imap.toulouse.inra.fr");
		props.setProperty("mail.user", lFrom);
		props.setProperty("mail.password", "");

		Session mailSession = Session.getDefaultInstance(props, null);
		Transport transport = mailSession.getTransport();

		MimeMessage message = new MimeMessage(mailSession);
		message.setSubject( subject );
		message.setContent( lContent , "text/plain");
		message.addRecipient(Message.RecipientType.TO, new InternetAddress( to ));

		transport.connect();
		transport.sendMessage(message,message.getRecipients(Message.RecipientType.TO));
		transport.close();
	}
	
	/**
	 * Get the current timeStamp
	 * @return
	 */
	public static long getTimeStamp(){
		return  new java.util.Date().getTime();
	}
	
	
	/**
	 * Return true if the file hash the given extension
	 * @param file
	 * @param extensions
	 * @return
	 */
	public static boolean hasExtension(File file, ArrayList<String> extensions){
		String ext = getFileExtension(file.getName());
		return extensions.contains(ext);
	}
	
	/**
	 * Get a file extension
	 * @param filePath
	 * @return
	 */
	public static String getFileExtension(String filePath) {
		File f = new File(filePath);
		String name = f.getName();
		int k = name.lastIndexOf(".");
		String ext = null;
		
		if (k != -1){
			ext = name.substring(k + 1, name.length());
		}
		return ext;
	}

	/**
	 * Return the file as a string
	 * @param pathname : path to the file
	 * @return
	 */
	public static String readFile(String pathname) {
		File f = new File(pathname);
		String content = "";
		
		if (f.exists() && f.isFile()){
			try {
				FileInputStream fis = new FileInputStream(f);
				FileChannel fc = fis.getChannel();

				MappedByteBuffer mmb = fc.map(FileChannel.MapMode.READ_ONLY, 0,	fc.size());

				byte[] buffer = new byte[(int) fc.size()];
				mmb.get(buffer);

				fis.close();
				BufferedReader in = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(buffer)));

				for (String line = in.readLine(); line != null; line = in.readLine()) {
					content += line;
				}
				in.close();
			}
			catch (Exception e) {
			}
			
		}
		
		return content;
	}
	
	
	/**
	 * Delete a directory
	 * @param path : the file that must be deleted
	 * @return
	 * @throws IOException 
	 */
	public static void deleteDirectory(File path) throws IOException {
		
		if (path.exists()) {
			if (path.isDirectory()){
				for (File child : path.listFiles()){
					deleteDirectory(child);
				}
			}
			if ( !path.delete()){
				throw new IOException("Could not delete " + path);
			}
		}
		
	}
	
	
	/**
	 * Convert a json into hash table
	 * @param jsonLine - The JSON line to convert
	 * @return
	 */
	public static Hashtable jsonToHash(String jsonLine) {
		Hashtable result = null ;
		try {
			ObjectMapper mapper = new ObjectMapper();
			result = mapper.readValue(jsonLine, Hashtable.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * convert any object (a hash is expected here) into a json
	 * @param obj : the object to transform into json
	 * @return a json line
	 * @throws Exception 
	 */
	public static String objectToJSON (Object obj) {
		ObjectMapper mapper = new ObjectMapper();
		String jsonRow = "{ }";
		
		try {
			jsonRow = mapper.writeValueAsString(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonRow;
	}
	
	/**
     * Write a String in an OuputStream to the out buffer
     * @param strMessage : the error message to write
     * @param out : outputStream
     */
	public static void writeMessage (String strMessage, OutputStream out){
    	try {
			out.write( ( "{ \"message\" : \"" + strMessage +"\"  }").getBytes() );
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	public static void silentWrite(String text, OutputStream out){
		try {
			out.write( text.getBytes() );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void writeToFile(String filename, String text) throws IOException{
		PrintWriter pout = new PrintWriter(new FileWriter( filename ));
		pout.println(text);
		pout.close();
	}
	
	/**
     * return the file basename with the extention provide in the directory
     * @param directory
     * @param exention  
     * @return string : filename
     */
	public static String getBasenameEndsWith (File directory , String pattern){
		File[] ff = directory.listFiles();
		for (int i = 0; i < ff.length; i++) {
			if (ff[i].isFile() && ff[i].getName().endsWith(pattern) ) {
				return (ff[i].getName().toString());
			}
		}
		return ("");
	}
	
	/**
     * return the current in sql datetime format
     * @return string : datetime
     */
	public static String getCurrentDate (){
		DateFormat formatter ;
		Date date = new Date();
		java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());
		formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return(formatter.format(timeStampDate));
	}
	
	/**
     * create a tempory path in directory provided 
     * @param directory
     * @return string : filename
     */
    public static String getWebTempDirectory (String directory) throws Exception {
    	try {
    		File webTempDir = new File(directory , "tmp");
	    	
    		// create dir if not exist
    		if (!webTempDir.exists()) {
    			webTempDir.mkdir();
    	    }
    		return webTempDir.toString();
    	} catch (Exception e) {
    		throw new Exception(e);
    	}
    }
    
    /**
     * Return the sequence part of a string 
     * @param str : sequence sting with or without header
     * @return
     */
    public static String getSequencePart(String str){
    	String[] t = str.split("\n");
    	return  t.length > 1 ? t[1] : t[0];
    }
    
    /**
     * Just raise an exception ... 
     * @param message
     * @throws Exception 
     */
    public static void raise(String message) throws Exception{
    	if(message == null){
    		message = "no message";
    	}
    	throw new Exception(message);
    }
	
}
