package org.biomart.processors.ngspipelines.utils;

import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;

public class NGSPipelinesConfigReader {

    private File configFile;   
    private Properties configProperties;
    
    public NGSPipelinesConfigReader() throws Exception {
        String curDir = System.getProperty("user.dir");
        String pluginsDir = new File(curDir, "plugins").toString();
        String ngsDir = new File(pluginsDir, "ngspipelines").toString();
        configFile = new File(ngsDir, "ngspipelines.properties");
        
        // Test if file exists
        if (configFile.exists()) {
            configProperties = new Properties();
            configProperties.load(new FileInputStream(configFile.toString()));
        } else {
        	throw new Exception("Config file does not exists");
        }
    }
    
    public String getStorageDirectory () throws Exception{
    	try {
    		return configProperties.getProperty("storage_directory");
    	} catch (Exception e) {
    		throw new Exception(e);
    	}
    }
    
    public String getJBrowseGlyph (String pType) throws Exception {
		String glyph = configProperties.getProperty("glyphs." + pType);
		if( glyph == null )
		{
			glyph = configProperties.getProperty("glyphs.default");
		}
    	return glyph ;
    }
    
    public String getJBrowseGlyph (List<String> types) throws Exception {
    	// First build the type
    	String finalType = StringUtils.join(types, ".");
    	try {
	    	String glyph = configProperties.getProperty("glyphs.default");
	    	for (Enumeration en = configProperties.propertyNames(); en.hasMoreElements();) {
	    		String type = (String) en.nextElement();
	    		if (type.equals("glyphs." + finalType)) {
	    			glyph = configProperties.getProperty(type);
	    		}
	    	}
	    	return glyph;
    	} catch (Exception e) {
    		throw new Exception(e);
    	}
    }

    /**
     * Get url of database
     * @param database
     * @return - String
     * @throws Exception
     */
    public String getDatabaseURL (String database) throws Exception {
    	// First build the type
    	String url = "";
    	try {
	    	for (Enumeration en = configProperties.propertyNames(); en.hasMoreElements();) {
	    		String type = (String) en.nextElement();
	    		if (type.equals("databaseurl." + database)) {
	    			url = configProperties.getProperty(type);
	    		}
	    	}
	    	return url;
    	} catch (Exception e) {
    		throw new Exception(e);
    	}
    }
    
    /**
     * Get executable path of a software
     * @param exec : executable name
     * @return - String
     * @throws Exception
     */
	public String getExecPath(String exec) throws Exception {
		// First build the type
    	String path = "";
    	try {
	    	for (Enumeration en = configProperties.propertyNames(); en.hasMoreElements();) {
	    		String type = (String) en.nextElement();
	    		if (type.equals("exec." + exec)) {
	    			path = configProperties.getProperty(type);
	    		}
	    	}
	    	
	    	if ( ( path.equals("") ) || ( path == null) ){
				throw new Exception(" Executable " + exec.toString() + " path must be define in properties file");
			}
	    	return path;
    	} catch (Exception e) {
    		throw new Exception(e);
    	}
	}
}