package org.biomart.processors.ngspipelines.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class CommandRunner {

	private ProcessBuilder processBuilder;
	private Process process;
	private StreamGobbler errorGobbler;
	private StreamGobbler outputGobbler;
	private String errorMessage = "";
	private String outMessage = "";
	
	/**
	 * Constructor
	 * @param commands - commands with parameters
	 */
	public CommandRunner( String[] commands) {
		processBuilder = new ProcessBuilder(commands) ;
	}

	public int execute() throws Exception{
		process = null;
		int exitValue = -99;
		try {
			
			process = processBuilder.start();
			errorGobbler = new StreamGobbler(process.getErrorStream());
			outputGobbler = new StreamGobbler(process.getInputStream());
			errorGobbler.start();
			outputGobbler.start();
			
			exitValue = process.waitFor();
			errorMessage = errorGobbler.getResult();
			outMessage = outputGobbler.getResult();

		} catch (Exception e) {
			errorMessage = e.getMessage().trim();
			throw new Exception(errorMessage);
		}
		return exitValue;
	}
	
	
	/**
	 * Update the value of an environment variable for this process
	 * @param key
	 * @param value
	 * @param update : update instead of overriding 
	 */
	public void setEnv(String key, String value,boolean update){
		
		if ( update && processBuilder.environment().containsKey(key)){
			String oldEnv = processBuilder.environment().get(key);
			processBuilder.environment().put(key, oldEnv + ":" + value);
		}else{
			processBuilder.environment().put(key, value);
		}
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}
	
	public String getOutMessage(){
		return this.outMessage;
	}
	
	
	private class StreamGobbler extends Thread {
		private InputStream inputStream;
		private StringBuffer result;

		/**
		 * Constructor.
		 * @param aStream - The stream to read from.
		 */
		StreamGobbler(final InputStream aStream) {
			inputStream = aStream;
			result = new StringBuffer();
		}

		/** Get the final output log. */
		public String getResult() {
			return result.toString();
		}

		/** Read until the end of stream. */
		public void run() {
			try {
				final BufferedReader buffReader = new BufferedReader( new InputStreamReader( inputStream));

				String line;
				while ((line = buffReader.readLine()) != null ) {
					result.append(line + "\n");
				}

			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}

}