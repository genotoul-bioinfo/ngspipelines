package org.biomart.processors.ngspipelines.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Set;

import org.biomart.common.exceptions.TechnicalException;
import org.biomart.configurator.utils.type.DataLinkType;
import org.biomart.objects.objects.Dataset;
import org.biomart.queryEngine.DBType;
import org.biomart.queryEngine.Query;
import org.biomart.queryEngine.QueryElement;
import org.biomart.queryEngine.SubQuery;

/**
 * A class used to get connection to database
 * @author inabihoudine
 *
 */
public class DBObject {
	protected Dataset dataset;
	protected DBType dbType;
	protected Connection connectionObj = null;
	protected SubQuery subQuery = null ;
	protected boolean stream = true;
	protected Query query;
	
	/**
	 * Construct a new Database connection object
	 * @param query : a Query object
	 */
	public DBObject (Query query){
		this.query = query;
		for (Dataset ds : this.query.getDatasetSubqueries().keySet()) {
			if (this.dataset == null ){
				this.dataset = ds;
				Set<SubQuery> sqSet = this.query.getDatasetSubqueries().get(ds);
				for (SubQuery sq : sqSet) {
					if (this.subQuery == null ){
						this.subQuery = sq;
						break;
					}
				}
			}
		}
	}
	
	/**
	 * Connection to the biomart database
	 * @throws TechnicalException
	 * @throws SQLException
	 */
	protected final void connectToDatabase() throws TechnicalException  {
		
		if (connectionObj == null) {
			String connectionString = this.dataset.getDataLinkInfo().getJdbcLinkObject().getJdbcUrl();
			String databaseUser = this.dataset.getDataLinkInfo().getJdbcLinkObject().getUserName();
			String databasePassword = this.dataset.getDataLinkInfo().getJdbcLinkObject().getPassword();
			
			// Get the database type ...
			if (this.dataset.getDataLinkInfo().getDataLinkType() == DataLinkType.SOURCE
					|| this.dataset.getDataLinkInfo().getDataLinkType() == DataLinkType.TARGET) {
				if ( connectionString.startsWith("jdbc:mysql"))
					this.dbType = DBType.MYSQL;
				else if ( connectionString.startsWith("jdbc:postgres"))
					this.dbType = DBType.POSTGRES;
				else if ( connectionString.startsWith("jdbc:oracle"))
					this.dbType = DBType.ORACLE;
				else if ( connectionString.startsWith("jdbc:sqlserver"))
					this.dbType = DBType.MSSQL;
				else if ( connectionString.startsWith("jdbc:db2"))
					this.dbType = DBType.DB2;
			} else if (this.dataset.getDataLinkInfo().getDataLinkType() == DataLinkType.URL) {
				this.dbType = DBType.WS;
			}
		   
			// ... and try connect
			try {
				
				boolean isRdbmsType;
				if (this.dbType == DBType.POSTGRES){
					isRdbmsType = true;
				}
				else{
					isRdbmsType = false;
				}
				connectionObj = (Connection) DriverManager.getConnection(
						connectionString, databaseUser, databasePassword);
				if (this.stream && isRdbmsType) {
					connectionObj.setAutoCommit(false);
				}
			} catch (SQLException e) {
				throw new TechnicalException( connectionString, e);
			}
		}
	}
	
	/**
	 * Get the Project name associated with the dataset. The project name is usually the prefix
	 * of the name of the dataset 
	 * @return- a String
	 * @throws a NullPointerException if the dataset is not initialized 
	 */
	public final String getProjectName() {
		if(this.dataset == null ){
			throw new NullPointerException("Dataset not initilized");
		}
		return this.dataset.getName().split("_")[0];
	}
	
	/**
	 * Return a connection object with an opened connection to the database
	 * @return java.sql.Connection object
	 * @throws TechnicalException 
	 */
	public Connection getConnection() throws TechnicalException{
		this.connectToDatabase();
		return this.connectionObj;
	}
	
	/**
	 * Return the string associated with a filter pattern
	 * @param pattern : a pattern to describe the filter from which you want to retrieve the value
	 * @return a String object 
	 */
	public String getFilterValue(String pattern){
		String result = "";
		for (QueryElement queryElement : this.subQuery.getQueryFilterList()) {
        	if (queryElement.getElement().getName().endsWith( pattern )) {
        		result = queryElement.getFilterValues();
        		break;
        	}
        }
		return result;
	}

	/**
	 * Return the current query object
	 * @return
	 */
	public Query getQuery() {
		return query;
	}
	
	

}
