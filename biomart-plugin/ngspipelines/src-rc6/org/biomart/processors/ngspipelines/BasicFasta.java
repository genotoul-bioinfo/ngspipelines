package org.biomart.processors.ngspipelines;

import java.io.File;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.biomart.common.exceptions.BioMartException;
import org.biomart.processors.ProcessorImpl;
import org.biomart.processors.annotations.ContentType;
import org.biomart.processors.ngspipelines.utils.DBObject;
import org.biomart.processors.ngspipelines.utils.NGSPUtils;
import org.biomart.processors.ngspipelines.utils.NGSPipelinesConfigReader;
import org.biomart.processors.ngspipelines.utils.RC6OutputStream;
import org.biomart.processors.ngspipelines.utils.Utils;
import org.biomart.queryEngine.Query;

@ContentType("application/json")
public class BasicFasta extends ProcessorImpl {
    
	private String sequence = "";
	private DBObject dbo;
	OutputStream originalOut;
	String contigName ;
	
	@Override
	public void beforeQuery(Query query, OutputStream outputHandle) {
		dbo = new DBObject(query);
		originalOut = outputHandle;
		out = new BasicFastaOutputStream(outputHandle);
	}
    
    private class BasicFastaOutputStream extends RC6OutputStream  {
    	
    	public BasicFastaOutputStream(OutputStream out) {
			super(out);
		}

		private final int LINE_SIZE = 60;

		@Override
		protected void apply(String[] columns) {
			try {
				contigName = columns[0] ;
				sequence += ">" + columns[0] + "\n";
				
				String contigSequence = Utils.getSequencePart(columns[1]);
				
            	int nbLines = (int)Math.ceil(contigSequence.length()/this.LINE_SIZE) + 1; 
            	
            	for(int i=0;i<nbLines;i++){
            		
            		if (i==nbLines-1) {
            			sequence += contigSequence.substring(i*this.LINE_SIZE) + "\n";
            		} else {
            			sequence += contigSequence.substring(i*this.LINE_SIZE,(i+1)*this.LINE_SIZE) + "\n";
            		}
            	}
            } catch (Exception e) {
            	throw new BioMartException(e);
            }
		}
    };
    
    public void afterQuery() {
    	try {
    		Connection connectionObj = dbo.getConnection();
    		String projectTable = NGSPUtils.getProjectTableName(dbo.getProjectName());
			NGSPipelinesConfigReader config = new NGSPipelinesConfigReader();
			String storageDir = config.getStorageDirectory();
			String projectDir = "";
			String selectProjectDirQuery = "SELECT directory FROM " + projectTable + ";" ;
			Statement sth = connectionObj.createStatement();

			ResultSet rs = sth.executeQuery(selectProjectDirQuery);
		    while (rs.next()) {
		    	projectDir = rs.getString("directory");
		    }
			
		    String outTempDirName = storageDir + Utils.FSEP + projectDir + Utils.FSEP + Utils.TEMP_DIR;
		    File outTempDir = new File(outTempDirName);
		    
		    if ( ! outTempDir.exists() ){
		    	outTempDir.mkdirs();
		    }
		    
			File tempFastaFile = new File( outTempDir + Utils.FSEP + contigName + "_fasta_sequence.fasta");
			
			if( !tempFastaFile.exists() ){
				PrintWriter pout = new PrintWriter(new FileWriter( tempFastaFile ));
		        pout.print(sequence);
				pout.close();
			}
			
	    	Utils.silentWrite("{  \"fasta_file_path\" : \"" + projectDir + Utils.FSEP + Utils.TEMP_DIR + Utils.FSEP + tempFastaFile.getName() + "\"  }", originalOut);
	    	
	    	tempFastaFile.deleteOnExit();
    		
		} catch (Exception e) {
			Utils.writeMessage(e.getMessage(), out);
			e.printStackTrace();
		}
    }
	
}