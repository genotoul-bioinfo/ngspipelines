package org.biomart.processors.ngspipelines;

import java.io.File;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

import org.biomart.common.exceptions.BioMartException;
import org.biomart.processors.ProcessorImpl;
import org.biomart.processors.annotations.ContentType;
import org.biomart.processors.ngspipelines.utils.DBObject;
import org.biomart.processors.ngspipelines.utils.NGSPUtils;
import org.biomart.processors.ngspipelines.utils.NGSPipelinesConfigReader;
import org.biomart.processors.ngspipelines.utils.RC6OutputStream;
import org.biomart.processors.ngspipelines.utils.Utils;
import org.biomart.processors.ngspipelines.utils.ParametersReader;
import org.biomart.queryEngine.Query;

@ContentType("application/json")
public class BasicGFF extends ProcessorImpl {

	private String annotationsData = "";
	private DBObject dbo;
	OutputStream originalOut;
	
	@Override
	public void beforeQuery(Query query, OutputStream outputHandle){
		dbo = new DBObject(query);
		originalOut = outputHandle;
		out = new BasicGFFOutputStream(outputHandle);
	}
	
	private class BasicGFFOutputStream extends RC6OutputStream {

		public BasicGFFOutputStream(OutputStream out) {
			super(out);
		}
		
		@Override
		protected void apply(String[] columns) {}
	};
	
	public void process( HashMap<String, HashMap<String, String>> dbFields ) throws Exception
	{
		String projectName = this.dbo.getProjectName() ;
		Connection connection = this.dbo.getConnection();
		Statement stmt = connection.createStatement();

		for( String group : dbFields.keySet() ) {
			// Query
			HashMap<String, String> features = dbFields.get(group);
			String region_field = (features.get("region_field") != null) ? features.get("region_field") + " AS region, " : "" ;
			String name_field = (features.get("name_field") != null) ? features.get("name_field") + " AS name, " : "" ;
			String source_field = (features.get("source_field") != null) ? features.get("source_field") + " AS source, " : "" ;
			String score_field = (features.get("score_field") != null) ? features.get("score_field") + " AS score, " : "" ;
			String strand_field = (features.get("strand_field") != null) ? features.get("strand_field") + " AS strand, " : "" ;
			String phase_field = (features.get("phase_field") != null) ? features.get("phase_field") + " AS phase, " : "" ;
			String description_field = (features.get("description_field") != null) ? features.get("description_field") + " AS description, " : "" ;
			String filter = (features.get("filter_field") != null) ? " AND " + features.get("filter_field") + "='" + features.get("filter_value") + "'" : "" ;
			String sql = "SELECT " + region_field
			                       + name_field
			                       + source_field
			                       + features.get("type_field") + " AS type, "
			                       + features.get("start_field") + " AS start, "
			                       + features.get("end_field") + " AS end, "
			                       + score_field
			                       + strand_field
			                       + phase_field
			                       + description_field ;
			sql = sql.substring( 0, sql.length() - 2 ) + " " ; // replace last ', ' by ' '
			sql = sql  + "FROM " + features.get("feature_table") + " "
				       + "WHERE " + features.get("start_field") + " IS NOT NULL" + filter + ";" ;
			ResultSet dbResults = stmt.executeQuery(sql);

			// Results
			while (dbResults.next()) {
				String region = (features.get("region_field") != null) ? dbResults.getString("region") : features.get("region_value") ;
				String name = ((features.get("name_field") != null) && (dbResults.getString("name") != null)) ? "Name=" + dbResults.getString("name") + ";" : "Name=.;" ;
				String description = ((features.get("description_field") != null) && (dbResults.getString("description") != null) && (dbResults.getString("description") != "")) ? "Note=" + dbResults.getString("description").replaceAll(";", "%3B").replaceAll(",", "%2C").replaceAll("=", "%3D") + ";" : "" ;
				String attributes = name + description ;
				String source = (features.get("source_field") != null) ? dbResults.getString("source") : "." ;
				String score = (features.get("score_field") != null) ? dbResults.getString("score") : "." ;
				String strand = (features.get("strand_field") != null) ? dbResults.getString("strand") : "." ;
				if( features.get("strand_value") != null && strand.equals(".") )
				{
					strand = features.get("strand_value") ;
				}
				if( strand.equals("-1") )
				{
					strand = "-" ;
				} else if ( strand.equals("1") || strand.equals("+1") )
				{
					strand = "+" ;
				}
				String phase = (features.get("phase_field") != null) ? dbResults.getString("phase") : "." ;
				annotationsData += region + "\t" + source + "\t" + dbResults.getString("type") + "\t" + dbResults.getString("start") + "\t" + dbResults.getString("end") + "\t" + score + "\t" + strand + "\t" + phase + "\t" + attributes + "\n" ;
			}
		}
	}
	
	public void afterQuery() {
    	try {
    		Connection connectionObj = dbo.getConnection();
    		String projectTable = NGSPUtils.getProjectTableName(dbo.getProjectName());
			NGSPipelinesConfigReader config = new NGSPipelinesConfigReader();
			String storageDir = config.getStorageDirectory();
    		
			// Param from client side
			String filterDecode = java.net.URLDecoder.decode(dbo.getFilterValue("_analysis__Analysis__main__analysis_type"), "UTF-8") ;
			ParametersReader params = new ParametersReader( filterDecode );
			HashMap dbFields = (HashMap) params.get("dbFields") ;
			
			// Process query
			this.process( dbFields );
    		
			// Write			
			String projectDir = "" ;
			String selectProjectDirQuery = "SELECT directory FROM " + projectTable + ";" ;
			Statement sth = connectionObj.createStatement();

			ResultSet rs = sth.executeQuery(selectProjectDirQuery);
		    while (rs.next()) {
		    	projectDir = rs.getString("directory");
		    }
			
		    String outTempDirName = storageDir + Utils.FSEP + projectDir + Utils.FSEP + Utils.TEMP_DIR ;
		    File outTempDir = new File(outTempDirName);
		    
		    if ( ! outTempDir.exists() ){
		    	outTempDir.mkdirs();
		    }

			java.util.Date date= new java.util.Date();		    
			File tempFastaFile = new File( outTempDir, date.getTime() + "_" + Utils.generateString(10) + ".gff" );
			
			if( !tempFastaFile.exists() ){
				PrintWriter pout = new PrintWriter(new FileWriter( tempFastaFile ));
		        pout.print(annotationsData);
				pout.close();
			}
			
	    	Utils.silentWrite("{  \"annotation_file_path\" : \"" + projectDir + Utils.FSEP + Utils.TEMP_DIR + Utils.FSEP + tempFastaFile.getName() + "\"  }", originalOut);
	    	
	    	tempFastaFile.deleteOnExit();
    		
		} catch (Exception e) {
			Utils.writeMessage(e.getMessage(), out);
			e.printStackTrace();
		}
    }
}