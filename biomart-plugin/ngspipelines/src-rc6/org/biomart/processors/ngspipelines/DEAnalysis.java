package org.biomart.processors.ngspipelines;
import java.io.*;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Collections;

import org.biomart.processors.ProcessorImpl;
import org.biomart.processors.annotations.ContentType;
import org.biomart.processors.ngspipelines.utils.CommandRunner;
import org.biomart.processors.ngspipelines.utils.DBObject;
import org.biomart.processors.ngspipelines.utils.NGSPUtils;
import org.biomart.processors.ngspipelines.utils.NGSPipelinesConfigReader;
import org.biomart.processors.ngspipelines.utils.ParametersReader;
import org.biomart.processors.ngspipelines.utils.Utils;
import org.biomart.queryEngine.Query;
import org.biomart.common.exceptions.BioMartException;

import org.biomart.processors.ngspipelines.dea.DEAUtils;

@ContentType("application/json")
public class DEAnalysis extends ProcessorImpl {

	
	private String projectName ;
	private String deaResAbsolutePath ;
	
	//params
	private ParametersReader params ;
	private String softwareParameters ;
	private String category ;
	private String locationHref;
	private Integer deaIdKey ;
	private String pool2Str ="" ;
	private String pool1Str ="" ;
	private String usermail;
	// Database
	protected Connection connection = null;
	private DBObject database;
	private File projectDir = null;
	private String query ;
	private Statement stmt = null;
	private ResultSet rs = null;
	private String analysisMain ;
	private String analysisResult ;

	// Constant
	private final static String FSEP = System.getProperty("file.separator");
	private final String ANALYSIS_DIRECTORY = "analysis";
	private final String ANALYSIS_TYPE = "DEAnalysis";
	private final String ANALYSIS_NAME = "DEAnalysis";
	private final String SOFT_NAME = "DEAnalysis.java";
	private final String RESULT_JSON_FILENAME  = "results.json";
	private final String STAT_TXT_FILENAME = "statistics.txt";
	
	// Result
	Hashtable<String, Object> results = new Hashtable<String, Object>();
	private OutputStream originalOut;
	
	
    @Override
	public void beforeQuery(Query query, OutputStream outputHandle) {
    	database = new DBObject(query);
    	originalOut = outputHandle;
    	out = new ByteArrayOutputStream();
	}

    
    @Override
    public void afterQuery() {
    	try {
    		
    		NGSPipelinesConfigReader configReader = new NGSPipelinesConfigReader();
			if (!new File(configReader.getStorageDirectory()).exists() ) {
				throw new Exception("Storage directory must be defined in ngspipelines.properties file in plugin sources");
			}
			if ( configReader.getExecPath("rscript") == "" ){
				throw new Exception("Rscript path must be defined in ngspipelines.properties file in plugin sources");
			}
			if ( configReader.getExecPath("DEAscript") == "" ){
				throw new Exception("DEAscript path must be defined in ngspipelines.properties file in plugin sources");
			}
			// database connexion
			connection = this.database.getConnection();
    		projectName = this.database.getProjectName();
    		stmt = connection.createStatement();
			
    		// Get analysis and results tables names
			analysisMain = NGSPUtils.getAnalysisTableName(projectName) ;
			analysisResult 	= NGSPUtils.getAnalysisResultTableName(projectName) ;
			
			// get project path
			query =  "SELECT directory FROM " + NGSPUtils.getProjectTableName(projectName) + ";" ;
    		rs = stmt.executeQuery(query);
    		while (rs.next()) {
    			projectDir = new File(configReader.getStorageDirectory(), rs.getString("directory"));
    		}
			
			
			// param from client side
	        params = new ParametersReader(database.getFilterValue("_analysis__Analysis__main__analysis_type"));
	        
	        String action =  (String) params.get("action");
    		category = (String) params.get("category");
    		locationHref = (String) params.get("location_href");
    		deaIdKey = (Integer) params.get("dea_id_key");
    		action = (String) params.get("action");
    		usermail = (String) params.get("usermail");
    		
    		// does the analysis exist ?
    		if (deaIdKey == null){
    			// if existing deaIdKey is initialized
    			this.isExisting();
        	} else {
        		// Get analysis_id_key if the analysis exists
        		String query = "SELECT  soft_parameters,directory FROM " + analysisMain + " WHERE analysis_type='" + ANALYSIS_TYPE + "'  AND analysis_id_key=" + deaIdKey ;
            	rs = stmt.executeQuery(query);
    			while (rs.next()) {
    				softwareParameters = rs.getString("soft_parameters") ;
    				File deaAnalysis = new File(projectDir, ANALYSIS_DIRECTORY);
    				deaResAbsolutePath = new File(deaAnalysis, rs.getString("directory")).toString() ;
    			}
        	}
			
    		//PROCESS ACTION
    		if (action.equals("getResults" )){
    			
    			if (deaIdKey == null){
    				results.put("state", "notStarted");
    				originalOut.write( ( Utils.objectToJSON(results) ).getBytes());
    			}
    			else{
    				getExistsingResults();
    			}
    		}
    		else if (action.equals("startDEA")){
    			startDEA();
    		}
    		else if (action.equals("removeResults")){
    			removeResults();
    		}
    		
    		
	    } catch (Exception e) {
	    	
	    	results.put("error", e.getMessage());
	    	try {
				originalOut.write( ( Utils.objectToJSON(results) ).getBytes());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	    }
    }
    
    
    private void isExisting(){
    	try {
	    	ArrayList<String> pool1 = (ArrayList) params.get("pool1");
			ArrayList<String> pool2 = (ArrayList) params.get("pool2");
			
			// Create softwareParameters to compare with existing analysis
    		if (pool1 != null){
        		Collections.sort(pool1);
        	}
        	if(pool2 != null){
    			Collections.sort(pool2);
    		}
        	
        	if (pool1 != null && pool2 != null){
        		for (String s : pool1){
    				pool1Str += s+",";
    			}
    			pool1Str=pool1Str.substring(0, pool1Str.length()-1);
				for (String s : pool2){
    				pool2Str += s+",";
    			}
    			pool2Str=pool2Str.substring(0, pool2Str.length()-1);
    			
        	}
        	if (params.get("norm") != null && params.get("correct") != null
        			&& params.get("alpha") != null && params.get("filter") != null){
        		softwareParameters = "pool1:"+pool1Str+";";
    			softwareParameters += "pool2:"+pool2Str+";";
    			softwareParameters += "norm:" + params.get("norm") +";";
    			softwareParameters += "correct:" + params.get("correct") +";";
    			softwareParameters += "alpha:" + params.get("alpha") +";";
    			softwareParameters += "filter:" + params.get("filter")+";";
    			softwareParameters += "maplot:" + params.get("maplot");
        	}
			
			// Get analysis_id_key if the analysis exists
	    	query = "SELECT analysis_id_key,directory FROM " + analysisMain + " WHERE analysis_type='" + ANALYSIS_TYPE + "' AND soft_parameters='" + softwareParameters + "' LIMIT 1 ";
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				deaIdKey = rs.getInt("analysis_id_key") ;
				File deaAnalysis = new File(projectDir, ANALYSIS_DIRECTORY);
				deaResAbsolutePath = new File(deaAnalysis, rs.getString("directory")).toString() ;
			}
		
    	} catch (Exception e) {
		    results.put("error", e.getMessage()); 
    	}
		
    }
    
    // get results if analysis already exists
    private void getExistsingResults() throws Exception{
    	
    	// Still no id ? must start the ddd
    	if (deaIdKey == null){
    		results.put("state", "notStarted");
	    }
	    else if(deaResAbsolutePath == null ){
	    	results.put("state", "error");
	    	results.put("message", "The provided id " + deaIdKey + " is not a valid dea id key. Please run a new process using new parameters");
	    }
	    
	    // id exists, check if the analysis is still running and get the results to the user
	    else{
 	    	File dir = new File(deaResAbsolutePath) ;
 	    	if (dir.exists()){
 	    		if (DEAUtils.errored(deaResAbsolutePath)){
 	    			results.put("state", "error");
 	    			results.put("message", DEAUtils.getErroLog(deaResAbsolutePath));
 	    		}
 	    		else if(DEAUtils.isRunning(deaResAbsolutePath)){
 	    			results.put("state", "running");
 	    		}
 	    		// Read JSON results file to pass it to the user side
 		    	else{
 		    		String path = deaResAbsolutePath + FSEP + RESULT_JSON_FILENAME ;
 					results = Utils.jsonToHash(Utils.readFile(path));
 					results.put("state", "complete"); 					
 		    	}
 	    	}
 	    	else {
 	    		results.put("state", "unavailable");
 	    	}
	    }
	    originalOut.write( ( Utils.objectToJSON(results) ).getBytes());
    }
    
    
    /*run the analysis*/
    private void startDEA() throws Exception {
    	
    	NGSPipelinesConfigReader configReader = new NGSPipelinesConfigReader();
    	// new dir for results
    	final String generatedAnalysisDir = Utils.generateString(10);
		File outDir = new File(projectDir, ANALYSIS_DIRECTORY);
		outDir = new File(outDir, generatedAnalysisDir);
		
		// add to database
		Hashtable<String, Object> addanalysisParams = new Hashtable<String, Object>(){{
			put("analysis_name", ANALYSIS_NAME);
			put("analysis_type", ANALYSIS_TYPE);
			put("categorie", category);
			put("directory", generatedAnalysisDir);
			put("analysis_hidden", "1");
			put("soft_name", SOFT_NAME);
			put("soft_parameters", softwareParameters); 
		}} ;
		deaIdKey = NGSPUtils.addAnalaysis(this.connection, this.projectName, addanalysisParams);
		
		/////////////////////
		// get raw count table info 
		String rawAnalysisId = "";
		File rawcountDir = null;
		query = "SELECT directory, analysis_id_key FROM " + analysisMain + " WHERE analysis_type='libraries_count' AND categorie = '"+ category +"';";
		rs	= stmt.executeQuery(query);
		while (rs.next()) {
			rawcountDir = new File(projectDir, ANALYSIS_DIRECTORY);
			rawcountDir = new File(rawcountDir, rs.getString("directory"));
			rawAnalysisId = rs.getString("analysis_id_key");
		}

		////Get target name Contig ? ncRNA ? Gene ?
		String target = "" ;
		rs 	= stmt.executeQuery("SELECT value FROM " + analysisResult + " analyse WHERE analyse.key = 'target' AND analysis_id_key = "+ rawAnalysisId +" ;");
		while(rs.next()){
			target =  rs.getString("value");
		}
		
		// get raw count basename
		String rawCountFileName = "";
		query = "SELECT value FROM " + analysisResult + " analyse WHERE analyse.key = 'file' AND analysis_id_key = "+ rawAnalysisId +" ;";
		rs 	= stmt.executeQuery(query);
		while(rs.next()){
			rawCountFileName =  rs.getString("value");
		}
		/////////////////////
		// get normalization factor table info 
		File NormalizationDir = null;
		query = "SELECT directory FROM " + analysisMain + " WHERE analysis_type='cpta_NormalizeRawCount' AND categorie = '"+ category +"';";
		rs 	= stmt.executeQuery(query);

		while (rs.next()) {
			NormalizationDir = new File(projectDir, ANALYSIS_DIRECTORY);
			NormalizationDir = new File(NormalizationDir, rs.getString("directory"));
		}

		// DEG execution

		String rscriptPath = configReader.getExecPath("rscript") ;
		String DEAscriptPath =  configReader.getExecPath("DEAscript");
		
		String[] cmd_DEA  = {
				rscriptPath ,
				"--vanilla", DEAscriptPath,
				"--file", new File(rawcountDir, rawCountFileName).toString(), //
				"--norm", new File(NormalizationDir, params.get("norm").toString() + "_info.txt").toString(), //
				"--out", outDir.toString(),
				"--pool1", pool1Str,
				"--pool2", pool2Str,
				"--filter", params.get("filter").toString(),
				"--alpha", params.get("alpha").toString(),
				"--correct", params.get("correct").toString(),
				"--MAplots", params.get("maplot").toString(),
		};
		try {
			CommandRunner cr = new CommandRunner(cmd_DEA);
			cr.execute();
		}catch (IOException e1) {
			e1.printStackTrace();
		}

		// Delete file Rplots created by R script
		File del = new File(System.getProperty("user.dir"), "Rplots.pdf");
		del.delete(); 

		
		// Results
		//JSON R
		String generalJSON   = outDir.getAbsolutePath() + FSEP + RESULT_JSON_FILENAME;
		String stats_txt   = outDir.getAbsolutePath() + FSEP + STAT_TXT_FILENAME ;
	
		//get R JSON statitics+top20 p-value
		Hashtable<String, Object> RresultsJSON = Utils.jsonToHash(Utils.readFile(generalJSON).replace("\n", "").replace("\r", ""));

		//add information
		RresultsJSON.put("analysis_dir", generatedAnalysisDir);
		RresultsJSON.put("result_file_path", ANALYSIS_DIRECTORY + FSEP + generatedAnalysisDir + FSEP +"resDEG.csv");
		RresultsJSON.put("images_pdf", ANALYSIS_DIRECTORY + FSEP + generatedAnalysisDir + FSEP +"de_images.pdf");
		
		//get number of DE 
		BufferedReader br = new BufferedReader(new FileReader(stats_txt));
    	String line;
    	String overexp_both_pools="0";
    	while ((line = br.readLine()) != null) {
    		if (line.startsWith("overexp_both_pools")){
    			overexp_both_pools=line.split(" ")[1];
    		}
		}
		br.close();
		
		//save number of DE in database
		NGSPUtils.addResultAnalysis(this.connection, this.projectName, deaIdKey, "default", "overexp_both_pools", "INT",overexp_both_pools);
		
		//Add parameters to json
		Hashtable<String, Object> paramToSave = params.getParams() ;
		paramToSave.remove("usermail");
		paramToSave.remove("action");
		paramToSave.remove("category");
		paramToSave.remove("location_href");
		results.put("parameters", paramToSave );
		results.put("results", RresultsJSON);
		results.put("state", "complete");
		results.put("dea_id_key", deaIdKey);
		results.put("target", target);
		Utils.writeToFile(generalJSON, Utils.objectToJSON(results));
		
		originalOut.write( ( Utils.objectToJSON(results) ).getBytes());
		if (usermail != null && usermail != "" ){
			// send an e-mail when analysis is finished
			String link = locationHref.replaceAll("\\?.+", "");
	    	link += "?actDataset="+category+"&deaAnalysisIDKey=" + deaIdKey ;
	    		
	    	String contentText = "Hello,\n\n" +
	    				"The differential expression analysis you have launched is complete\n" +
	    				"To see your results click on this link : " + link.replaceAll("\\s", "%20") + "\n\n" +
	    				"This mail is automaticaly generated, please do no respond to it";
	    	Utils.sendMail("ngspipelines-dea-noreply",	usermail, "DE analysis Results" , contentText);
		}
    }
    
    
    /**
     * A function that will remove the DDD analysis from the database and from the disk
     * @throws Exception 
     */
    private void removeResults() throws Exception {
    	if(deaIdKey != null){
    		File resDir = null ;
    		rs = stmt.executeQuery("SELECT directory FROM " + analysisMain + " WHERE analysis_name='" + ANALYSIS_NAME + "' AND analysis_id_key= " + deaIdKey );
    		while (rs.next()) {
    			resDir = new File ( projectDir + FSEP + ANALYSIS_DIRECTORY + FSEP + rs.getString("directory") );
    		}
    		
    		stmt.executeUpdate("DELETE FROM " + analysisMain + " WHERE analysis_name='" + ANALYSIS_NAME + "'  AND  analysis_id_key=" + deaIdKey);
    		if (resDir.exists()){
    			Utils.deleteDirectory(resDir) ;
    		}
    		originalOut.write( ( Utils.objectToJSON(results) ).getBytes());
    	}
    	else{
    		throw new BioMartException("The analysis id has not been found");
    	}
    }   
    
}
