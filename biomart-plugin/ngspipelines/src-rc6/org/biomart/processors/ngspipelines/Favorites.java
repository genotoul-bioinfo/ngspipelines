package org.biomart.processors.ngspipelines;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

import org.biomart.processors.ProcessorImpl;
import org.biomart.processors.annotations.ContentType;
import org.biomart.processors.ngspipelines.utils.DBObject;
import org.biomart.processors.ngspipelines.utils.NGSPUtils;
import org.biomart.queryEngine.Query;

@ContentType("application/json")
public class Favorites extends ProcessorImpl {

	private OutputStream originalOut;
	private DBObject database;
	private Connection connection;
	
    @Override
	public void beforeQuery(Query query, OutputStream outputHandle) {
    	database = new DBObject(query);
    	originalOut = outputHandle ;
    	out = new ByteArrayOutputStream() ;
	}
    
    @Override
    public void afterQuery() {
    	
        try {
            // then get the ids from the filter values
            String ids = database.getFilterValue("analysis__Result__dm__value");
            String favoriteType = database.getFilterValue("_analysis__Analysis__main__analysis_type");
            ArrayList<String> selectedIDsList = new ArrayList<String>(Arrays.asList(ids.split(",")));
            
        	// then try to connect to the database
        		
    		String outMsg = "null";
    		
    		connection = database.getConnection();
    		Statement stmt = connection.createStatement();
    		String projectPrefix = database.getProjectName();
    		String analysis_main = NGSPUtils.getAnalysisTableName(projectPrefix);
    		String analysis_results = NGSPUtils.getAnalysisResultTableName(projectPrefix);
    		
    		// first select the favorite contigs analysis 
    		String sql = "SELECT analysis_id_key FROM " + analysis_main + " WHERE analysis_type='" + favoriteType + "';";
    		ResultSet rs = stmt.executeQuery(sql);
    		
    		String analysisID = "";
    		// Fetch each row from the result set
    	    while (rs.next()) {
    	        analysisID = rs.getString("analysis_id_key");
    	    }
    	    
    	    sql = "SELECT value FROM " + analysis_results + " WHERE analysis_id_key  = '" + analysisID + "';";
    	    rs = stmt.executeQuery(sql);
    	    
    		// Fetch each row from the result set
    	    ArrayList<String> favoriteIDsList = new ArrayList<String>();
    	    while (rs.next()) {
    	    	favoriteIDsList.add(rs.getString("value"));
    	    }
    	    
    	    for (String id: selectedIDsList) {
    	        if (favoriteIDsList.contains(id)) {
        	    	sql = "DELETE FROM " + analysis_results + " WHERE analysis_id_key='" + analysisID + "' AND value ='" + id + "';";
        	    	stmt.executeUpdate(sql);
        	    	outMsg = "false";
    	        } else {
        	    	sql = 	"INSERT INTO " + analysis_results + " (`analysis_id_key`, `key`, `type`, `group` ,`value`) " +			
        	    			"VALUES ('" + analysisID + "', 'favorite_user', 'INT', 'favorite', '" + id + "');";
        	    	stmt.executeUpdate(sql);
        	    	outMsg = "true";
    	        }
    	    }
    	    
    	    originalOut.write( ( " { \"isFavorite\" : \"" + outMsg +"\" }  " ).getBytes());

        } catch (Exception e) {
        	 try {
        		 originalOut.write( ( " { \"error\" : \"" + e.getMessage() +"\" }  " ).getBytes());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
        }
    }

    
}
