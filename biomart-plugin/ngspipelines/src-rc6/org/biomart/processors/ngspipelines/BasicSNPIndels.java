package org.biomart.processors.ngspipelines;

import java.io.File;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.biomart.common.exceptions.BioMartException;
import org.biomart.processors.ProcessorImpl;
import org.biomart.processors.annotations.ContentType;
import org.biomart.processors.ngspipelines.utils.DBObject;
import org.biomart.processors.ngspipelines.utils.NGSPUtils;
import org.biomart.processors.ngspipelines.utils.NGSPipelinesConfigReader;
import org.biomart.processors.ngspipelines.utils.RC6OutputStream;
import org.biomart.processors.ngspipelines.utils.Utils;
import org.biomart.queryEngine.Query;

@ContentType("application/json")
public class BasicSNPIndels extends ProcessorImpl {
    
	private String snpdata = "";
	private DBObject dbo;
	OutputStream originalOut;
	String contigName ; ;
	
	
	@Override
	public void beforeQuery(Query query, OutputStream outputHandle){
		dbo = new DBObject(query);
		originalOut = outputHandle;
		out = new BasicSNPIndelsOutputStream(outputHandle);
	}
    
    private class BasicSNPIndelsOutputStream extends RC6OutputStream {

    	private final int CONTEXT_SIZE = 100;
    	
		public BasicSNPIndelsOutputStream(OutputStream out) {
			super(out);
		}

		@Override
		protected void apply(String[] columns) {
			try {
            	if ( contigName == null){
            		contigName = columns[0].split("_")[0];
            	}
            	
            	String contigSequence = Utils.getSequencePart(columns[1]);
            	
            	int snpIndelSize = columns[4].split("/")[0].length() - 1;
            	
            	int start = Integer.parseInt(columns[3]) - this.CONTEXT_SIZE - 1;
            	int stop = Integer.parseInt(columns[3]) + this.CONTEXT_SIZE + snpIndelSize;
            	if (start < 0) { start = 0; }
            	if (stop > contigSequence.length()) { stop = contigSequence.length(); }

            	String line = columns[0] + "\t" + columns[2] + "\t";
            	line += contigSequence.substring(start, Integer.parseInt(columns[3]) - 1);
            	line += "\t" + columns[4] + "\t";
            	line += contigSequence.substring(Integer.parseInt(columns[3]) + snpIndelSize, stop);
            	
            	snpdata += line + "\n";
            	
            } catch (Exception e) {
            	throw new BioMartException(e);
            }
		}
    };
    
    
    public void afterQuery() {
    	try {
    		Connection connectionObj = dbo.getConnection();
    		String projectTable = NGSPUtils.getProjectTableName(dbo.getProjectName());
			NGSPipelinesConfigReader config = new NGSPipelinesConfigReader();
			String storageDir = config.getStorageDirectory();
			
			String projectDir = "";
			String selectProjectDirQuery =  "SELECT directory FROM " + projectTable + ";" ;
			Statement sth = connectionObj.createStatement();

			ResultSet rs = sth.executeQuery(selectProjectDirQuery);
		    while (rs.next()) {
		    	projectDir = rs.getString("directory");
		    }
			
		    String outTempDirName = storageDir + Utils.FSEP + projectDir + Utils.FSEP + Utils.TEMP_DIR ;
		    File outTempDir = new File(outTempDirName);
		    
		    if ( ! outTempDir.exists() ){
		    	outTempDir.mkdirs();
		    }
		    
			File tempFastaFile = new File( outTempDir + Utils.FSEP + contigName + "_variations.tsv");
			
			if( !tempFastaFile.exists() ){
				PrintWriter pout = new PrintWriter(new FileWriter( tempFastaFile ));
		        pout.print(snpdata);
				pout.close();
			}
			
	    	Utils.silentWrite("{  \"variation_file_path\" : \"" + projectDir + Utils.FSEP + Utils.TEMP_DIR + Utils.FSEP + tempFastaFile.getName() + "\"  }", originalOut);
	    	
	    	tempFastaFile.deleteOnExit();
    		
		} catch (Exception e) {
			Utils.writeMessage(e.getMessage(), out);
			e.printStackTrace();
		}
    }
}