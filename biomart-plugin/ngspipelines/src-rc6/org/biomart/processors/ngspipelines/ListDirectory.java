package org.biomart.processors.ngspipelines;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;

import org.biomart.processors.ProcessorImpl;
import org.biomart.processors.annotations.ContentType;
import org.biomart.processors.ngspipelines.utils.DBObject;
import org.biomart.processors.ngspipelines.utils.NGSPUtils;
import org.biomart.processors.ngspipelines.utils.RC6OutputStream;
import org.biomart.queryEngine.Query;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

@ContentType("application/json")
public class ListDirectory extends ProcessorImpl {

	private String dataDir;
	private ArrayList<Hashtable<String, Object> > rawFilesList;
	private Hashtable<String, Object> analysisData;
	private OutputStream originalOut;
	private String projectDir;
	private DBObject database;
	protected Connection connection = null;

	public ListDirectory() {
		rawFilesList = new ArrayList<Hashtable<String, Object> >();
		analysisData = new Hashtable<String, Object>();
		String curDir = System.getProperty("user.dir");
		dataDir = new File(curDir,"plugins/ngspipelines/public/ngspipelines/data").toString();
	}

	private class ListDirectoryOutputStream extends RC6OutputStream {
		public ListDirectoryOutputStream(OutputStream out) {
			super(out);
		}

		@Override
		public void apply(String[] columns) {
			projectDir = new File(dataDir, columns[0]).toString();
			File rawDir = new File(projectDir, "raw");
			rawFilesList = getFilesFromDir(rawDir);
		}
	};

	@Override
	public void beforeQuery(Query query, OutputStream outputHandle) {
		database = new DBObject(query);
		originalOut = outputHandle;
		out = new ListDirectoryOutputStream(outputHandle);
	}

	@Override
	public void afterQuery() {

		try {
			String projectName = database.getProjectName();
			connection = database.getConnection();
			String analysisTable = NGSPUtils.getAnalysisTableName(projectName);
			String query = "SELECT analysis_name,categorie,directory FROM "	+ analysisTable + " WHERE ( NOT analysis_hidden ) AND (directory IS NOT NULL) ;";
			Statement sth = connection.createStatement();
			ResultSet rs = sth.executeQuery(query);
			
			while (rs.next()) {
				String category = rs.getString("categorie");
				String analysisName = rs.getString("analysis_name");
				String directory = "analysis/" + rs.getString("directory");

				File analysisDir = new File(projectDir, directory);
				
				if (!analysisData.containsKey(category)) {
					analysisData.put(category, new Hashtable<String, Object>());
				}
				
				Hashtable<String, Object> analysisInfo = new Hashtable<String, Object>();
				ArrayList<Hashtable<String, Object> > files = getFilesFromDir(analysisDir);
				analysisInfo.put("path", directory);
				analysisInfo.put("files", files );
				analysisInfo.put("totalFiles", files.size() );

				int cpt = 1 ;
				String original = new String(analysisName);
				while (((Hashtable<String, Object>) analysisData.get(category)).containsKey(analysisName)){
					analysisName = original + "#" + cpt;
					cpt++;
				}
				
				((Hashtable<String, Object>) analysisData.get(category)).put(analysisName, analysisInfo);
			}

			Hashtable<String, Object> rawInfos = new Hashtable<String, Object>();
			rawInfos.put("path", "raw");
			rawInfos.put("files", rawFilesList);
			rawInfos.put("totalFiles", rawFilesList.size() );
			Hashtable<String, Object> raws = new Hashtable<String, Object>();
			raws.put("raw", rawInfos);
			analysisData.put("RawData", raws);

			writeJSON();
			
		} catch (Exception ee) {
			try {
				originalOut.write(ee.getMessage().getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void writeJSON() throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		String jsonRow = mapper.writeValueAsString(analysisData);
		originalOut.write(jsonRow.getBytes());
	}
	
	private ArrayList<Hashtable<String, Object> > getFilesFromDir(File directory){
		
		ArrayList<Hashtable<String, Object> > list = new ArrayList<Hashtable<String, Object> >();

		if (directory != null && directory.isDirectory() ) {

			File[] ff = directory.listFiles();

			for (int i = 0; i < ff.length; i++) {
				if (!ff[i].isDirectory()) {
					
					long fileSize = ff[i].length() ;
					String name = ff[i].getName() ;
					int mid = name.lastIndexOf(".");
					String extension = name.substring(mid+1);
					Hashtable<String, Object> info = new Hashtable<String, Object>();
					info.put("ext", extension);
					info.put("size", fileSize);
					info.put("name", name);
					list.add(info);
				}
			}
		}
		
		// Comparator
		Comparator< Hashtable<String, Object> > comparator = new Comparator< Hashtable<String, Object> >() {
			public int compare(Hashtable<String, Object> e1, Hashtable<String, Object> e2) {
                return ((String)e1.get("name")).compareTo( (String)e2.get("name"));
            }
		};
		
		Collections.sort(list, comparator); 
		return list;
	}
	
}
