package org.biomart.processors.ngspipelines;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

import org.biomart.processors.ProcessorImpl;
import org.biomart.processors.annotations.ContentType;
import org.biomart.processors.ngspipelines.utils.CommandRunner;
import org.biomart.processors.ngspipelines.utils.DBObject;
import org.biomart.processors.ngspipelines.utils.NGSPUtils;
import org.biomart.processors.ngspipelines.utils.NGSPipelinesConfigReader;
import org.biomart.processors.ngspipelines.utils.ParametersReader;
import org.biomart.processors.ngspipelines.utils.Utils;
import org.biomart.queryEngine.Query;

@ContentType("application/json")
public class Blast extends ProcessorImpl {

	private final String ANALYSIS_DIRECTORY = "analysis";
	private OutputStream originalOut;
	private DBObject database;
	protected Connection connection = null;
	
    @Override
	public void beforeQuery(Query query, OutputStream outputHandle) {
    	database = new DBObject(query);
    	originalOut = outputHandle;
    	out = new ByteArrayOutputStream();
	}

    
    @Override
    public void afterQuery() {
    	
    	Hashtable<String, Object> results = new Hashtable<String, Object>();
    	try {
    		
    		NGSPipelinesConfigReader configReader = new NGSPipelinesConfigReader();
			if (!new File(configReader.getStorageDirectory()).exists()){
				throw new Exception("Storage directory must be defined in properties file");
			}
    		
    		// param from client side
	        ParametersReader params = new ParametersReader(database.getFilterValue("_analysis__Analysis__main__analysis_type"));
	        
	        // database connexion 
			connection 					= database.getConnection();
			Statement stmt 				= connection.createStatement();
			String projectName		    = database.getProjectName();
			String analysis_main 		= NGSPUtils.getAnalysisTableName(projectName) ;
			String analysis_result_dm 	= NGSPUtils.getAnalysisResultTableName(projectName) ;
			String application 			= params.get("categorie").toString();
			
			// get project path
			File projectDir = null;
			String sql 		=  "SELECT directory FROM " + NGSPUtils.getProjectTableName(projectName) + ";" ;
			
			ResultSet rs 	= stmt.executeQuery(sql);
			while (rs.next()) {
				projectDir = new File(configReader.getStorageDirectory(), rs.getString("directory"));
			}
			
			// get index analysis info 
			String analysisId = "";
			File indexDbDir = null;
			sql = "SELECT analysis_id_key, directory FROM " + analysis_main + " WHERE analysis_type='blast_search' AND categorie = '"+ application +"' ORDER BY analysis_id_key ASC;";
			rs 	= stmt.executeQuery(sql);
			
			while (rs.next()) {
				indexDbDir = new File(projectDir, ANALYSIS_DIRECTORY);
				indexDbDir = new File(indexDbDir, rs.getString("directory"));
				analysisId = rs.getString("analysis_id_key");
			}
			
			// get fasta basename
			String fastaDbName = "";
			String sqlDbName = "SELECT value FROM " + analysis_result_dm + " analyse WHERE analyse.key = 'databank' AND analysis_id_key = "+ analysisId +" ;";
			rs 	= stmt.executeQuery(sqlDbName);
			while(rs.next()){
				fastaDbName =  rs.getString("value");
			}
			String target = "" ;
			String sqlTarget = "SELECT value FROM " + analysis_result_dm + " analyse WHERE analyse.key = 'target' AND analysis_id_key = "+ analysisId +" ;";
			rs 	= stmt.executeQuery(sqlTarget);
			while(rs.next()){
				target =  rs.getString("value");
			}
			String label = "";
			String sqlLabel = "SELECT value FROM " + analysis_result_dm + " analyse WHERE analyse.key = 'label' AND analysis_id_key = "+ analysisId +" ;";
			rs 	= stmt.executeQuery(sqlLabel);
			while(rs.next()){
				label =  rs.getString("value");
			}
			
			// temporary files
			String webTempDir = Utils.getWebTempDirectory(projectDir.toString()); // get the web temp dir
			File output_archiveblast = new File(webTempDir, Utils.generateString(15)); // create tmp blast archive output file
			File output_tabblast = new File(webTempDir, Utils.generateString(15)); // create tmp blast_converter tabular output file
			File output_pairwise = new File(webTempDir, Utils.generateString(15)); // create tmp blast_converter pairwise output file
	        File queryfile = new File(webTempDir, Utils.generateString(15));
	        try {
	            Utils.writeToFile(queryfile.toString(), (String)params.get("blastFastaSeq"));
	        }
	        catch (Exception e) {
				throw new Exception(" Unable to write " + queryfile.toString());
	        }
			
			// Blast execution
	        String blastPath = configReader.getExecPath(params.get("blastPrg").toString()) ;
			String algo_filter = "dust" ;
			if( blastPath.endsWith("tblastn") ) {
				algo_filter = "seg" ;
			}
			String[] cmd_blast  = {
					 blastPath ,
					 "-"+ algo_filter, params.get("blastFilter").toString(),
					 "-num_descriptions", params.get("blastMaxHit").toString(),
					 "-num_alignments", params.get("blastMaxHit").toString(),
					 "-evalue", params.get("blastExpect").toString(),
					 "-query", queryfile.toString(),
					 "-db", new File(indexDbDir, fastaDbName).toString(),
					 "-out", output_archiveblast.toString(),
					 "-outfmt", "11"
					};
			CommandRunner cr = new CommandRunner(cmd_blast);
			cr.execute();
			
			// Format outputs
			String blastConverterPath = configReader.getExecPath("blast_formatter") ;
			String[] cmdConverter2tab = { blastConverterPath, 
					                      "-archive", output_archiveblast.toString(),
					                      "-outfmt", "6",
					                      "-num_descriptions", params.get("blastMaxHit").toString(),
					                      "-num_alignments", params.get("blastMaxHit").toString(),
					                      "-out", output_tabblast.toString()
			};
			cr = new CommandRunner( cmdConverter2tab );
			cr.execute();

			String[] cmdConverter2pairwise = { blastConverterPath, 
					                      "-archive", output_archiveblast.toString(),
					                      "-outfmt", "0",
					                      "-num_descriptions", params.get("blastMaxHit").toString(),
					                      "-num_alignments", params.get("blastMaxHit").toString(),
					                      "-out", output_pairwise.toString()
			};
			cr = new CommandRunner( cmdConverter2pairwise );
			cr.execute();
			
			// Build Array of Array (m8 line)    [[m8 line], [m8 line], ...]
			ArrayList<ArrayList<String>> resultTab = new ArrayList<ArrayList<String>>(); 
			BufferedReader br = new BufferedReader(new FileReader(output_tabblast));
	    	String line;
	    	String subjectsName = "" ;
	    	
	    	while ((line = br.readLine()) != null) {
	    		ArrayList<String> sAll = new ArrayList<String>();     		
				sAll.addAll(Arrays.asList(line.split("\t")));
	    		resultTab.add(sAll);
	    		subjectsName = subjectsName + "'" + sAll.get(1) + "',";
			}
			br.close();
			int size = resultTab.size();
			if (size > 0){
				// sql - retrieve the target id key (needed for links)
				subjectsName = subjectsName.substring(0, subjectsName.length()-1);
				String targetKeyDbField = target.toLowerCase() + "_id_key";
				sql = "SELECT " + label + ","+ targetKeyDbField + " FROM " + NGSPUtils.getTableName(projectName, application, target, true) + " WHERE " + label + " in (" + subjectsName + ");";
	    		rs = stmt.executeQuery(sql);
	    		Hashtable<String, String> subjectNameID = new Hashtable<String, String>();
	    		while (rs.next()) {
	    			subjectNameID.put(rs.getString(label), rs.getString(targetKeyDbField));
				}
	    		// Add subject Id to the Array of Array (m8 line) [[m8 line, subjectID], [m8 line, subjectID], ...]
	    		for (int i=0; i<size; i++) {
	    			String id = subjectNameID.get( resultTab.get(i).get(1) ).toString();
	    			resultTab.get(i).add(id);
	    		}
    		}
			results.put("m8", resultTab);
			if (params.get("blastViewAln").toString() == "true") {
				results.put("alignment", output_pairwise.toString());
			}
			originalOut.write( ( Utils.objectToJSON(results) ).getBytes());
			
			queryfile.delete();
			output_tabblast.delete();
			output_archiveblast.delete();

	    } catch (Exception e) {
	    	
	    	results.put("error", e.getMessage());
	    	try {
				originalOut.write( ( Utils.objectToJSON(results) ).getBytes());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	    }
    }
    
}
