package org.biomart.processors.ngspipelines;

import java.io.File;
import java.io.FilenameFilter;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.HashMap;

import org.biomart.common.exceptions.BioMartException;
import org.biomart.processors.ProcessorImpl;
import org.biomart.processors.annotations.ContentType;
import org.biomart.processors.ngspipelines.utils.CommandRunner;
import org.biomart.processors.ngspipelines.utils.DBObject;
import org.biomart.processors.ngspipelines.utils.NGSPUtils;
import org.biomart.processors.ngspipelines.utils.NGSPipelinesConfigReader;
import org.biomart.processors.ngspipelines.utils.RC6OutputStream;
import org.biomart.processors.ngspipelines.utils.ParametersReader;
import org.biomart.processors.ngspipelines.utils.Utils;
import org.biomart.queryEngine.Query;

@ContentType("text/html")
public class JBrowse extends ProcessorImpl {
	private String perl5libKey      = "PERL5LIB" ;
	private String jbrowseBinDir    = null ;
	private String jbrowseExtLibDir = null ;
	
	private HashMap<String, Track> jBrowseTracks = new HashMap() ;
	private File jBrowseLoadDir = null ;
	private File jBrowseFolder  = null ;
	private File tmpGFF = null ;
	private File configTmpFile = null ;
	
	private String preProcess = null ;
	
	private NGSPipelinesConfigReader configReader ; 
	
	private OutputStream originalOut;
	private DBObject database;
	protected Connection connection = null;
	
	public JBrowse() {}
	
	@Override
	public void beforeQuery(Query query, OutputStream outputHandle) {
		database = new DBObject(query);
		originalOut = outputHandle;
		out = new JBrowseOutputStream(outputHandle);
	}
	
	private class JBrowseOutputStream extends RC6OutputStream  {
		public JBrowseOutputStream(OutputStream out) {
			super(out);
		}

		@Override
		public void apply(String[] columns) {}
	};
	
	public class Track
	{
		private String label ;
		private String feature ; //Type use to find GFFline in this track
		private String tClass ;  //Glyph type
		private String key ;     //Label on panel
		
		public String toJSONString() {
			StringBuilder jsonTrack = new StringBuilder();
			jsonTrack.append("    {\n") ;
			jsonTrack.append("      \"track\"       : \"" + this.label       + "\",\n");
			jsonTrack.append("      \"feature\"     : [\"" + this.feature    + "\"],\n");
			jsonTrack.append("      \"class\"       : \"" + this.tClass      + "\",\n");
			jsonTrack.append("      \"key\"         : \"" + this.key         + "\"\n");
			jsonTrack.append("    }");
			return( jsonTrack.toString() );
		}
		
		public Track ( String pLabel, String pFeature, String pClass, String pKey ) {
			this.label   = pLabel ;
			this.feature = pFeature ;
			this.tClass  = pClass ;
			this.key     = pKey ;
		}
	}
	
	public void addDBFeatures( HashMap<String, HashMap<String, String>> dbField, File gffFeatures ) throws Exception
	{
		String projectName = this.database.getProjectName() ;
		connection = this.database.getConnection();
		Statement stmt = connection.createStatement();
		
		BufferedWriter FHGff = new BufferedWriter( new FileWriter(gffFeatures.getPath(), true) );
		try {
			for (String group : dbField.keySet()) {
				HashMap<String, String> features = dbField.get(group);

				// Check if the feature table exist
				boolean featureTableExist = true ;
	            try {
	            	String sql = "SELECT * FROM " + features.get("feature_table") + " LIMIT 1 ;" ;
	            	ResultSet dbResults = stmt.executeQuery(sql);
	            } catch( Exception excep ) {
	            	System.err.println( "jBrowse WARNING " + excep.getMessage() );
	            	featureTableExist = false ;
	            }
	            
	            // Process
	            if( featureTableExist ) {
					String region_field = (features.get("region_field") != null) ? features.get("region_field") + " AS region, " : "" ;
					String name_field = (features.get("name_field") != null) ? features.get("name_field") + " AS name, " : "" ;
					String source_field = (features.get("source_field") != null) ? features.get("source_field") + " AS source, " : "" ;
					String score_field = (features.get("score_field") != null) ? features.get("score_field") + " AS score, " : "" ;
					String strand_field = (features.get("strand_field") != null) ? features.get("strand_field") + " AS strand, " : "" ;
					String phase_field = (features.get("phase_field") != null) ? features.get("phase_field") + " AS phase, " : "" ;
					String description_field = (features.get("description_field") != null) ? features.get("description_field") + " AS description, " : "" ;
					String filter = (features.get("filter_field") != null) ? " AND " + features.get("filter_field") + "='" + features.get("filter_value") + "'" : "" ;
					String sql = "SELECT " + region_field
					                       + name_field
					                       + source_field
					                       + features.get("type_field") + " AS type, "
					                       + features.get("start_field") + " AS start, "
					                       + features.get("end_field") + " AS end, "
					                       + score_field
					                       + strand_field
					                       + phase_field
					                       + description_field ;
					sql = sql.substring( 0, sql.length() - 2 ) + " " ; // replace last ', ' by ' '
					sql = sql  + "FROM " + features.get("feature_table") + " "
						       + "WHERE " + features.get("start_field") + " IS NOT NULL" + filter + ";" ;
					ResultSet dbResults = stmt.executeQuery(sql);
	
					while (dbResults.next()) {
						String region = (features.get("region_field") != null) ? dbResults.getString("region") : features.get("region_value") ;
						String name = ((features.get("name_field") != null) && (dbResults.getString("name") != null)) ? "Name=" + dbResults.getString("name") + ";" : "Name=.;" ;
						String description = ((features.get("description_field") != null) && (dbResults.getString("description") != null)) ? "Note=" + dbResults.getString("description").replaceAll(";", "%3B").replaceAll(",", "%2C").replaceAll("=", "%3D") + ";" : "" ;
						String attributes = name + description ;
						String source = (features.get("source_field") != null) ? dbResults.getString("source") : "." ;
						String score = (features.get("score_field") != null) ? dbResults.getString("score") : "." ;
						String strand = (features.get("strand_field") != null) ? dbResults.getString("strand") : "." ;
						if( features.get("strand_value") != null && strand.equals(".") )
						{
							strand = features.get("strand_value") ;
						}
						if( strand.equals("-1") )
						{
							strand = "-" ;
	 					} else if ( strand.equals("1") || strand.equals("+1") )
	 					{
	 						strand = "+" ;
	 					}
						String phase = (features.get("phase_field") != null) ? dbResults.getString("phase") : "." ;
						String label = dbResults.getString("type") + ( !source.equals(".") ? " (" + source + ")" : "" );
						String trackType = dbResults.getString("type") + ( !source.equals(".") ? "." + source : "" );
						if( !this.jBrowseTracks.containsKey(trackType) )
						{
							// Add track
							String glyph = this.configReader.getJBrowseGlyph(dbResults.getString("type"));
							this.jBrowseTracks.put( label, new Track(trackType, trackType, glyph, label) );
						}
						FHGff.write( region + "\t" + source + "\t" + trackType + "\t" + dbResults.getString("start") + "\t" + dbResults.getString("end") + "\t" + score + "\t" + strand + "\t" + phase + "\t" + attributes + "\n" );
					}
	            }
			}
		} finally {
			FHGff.close();
		}
	}
	
	public File getTmpGFF() {
		if( this.tmpGFF == null ) {
			this.tmpGFF = new File( this.jBrowseLoadDir, this.jBrowseFolder.getName() + ".gff" );
		}
		return this.tmpGFF ;
	}
	
	public void addFileFeatures( String filepath, File gffFeatures ) throws Exception
	{
		File featuresFile = new File( filepath );
		if( filepath.contains(".gff") )
		{
			this.addGFFContent( featuresFile, gffFeatures );
		} else {
			this.addVCFContent( featuresFile, gffFeatures );
		}
	}
	
	private void addGFFContent( File source, File destination ) throws Exception
	{
		NGSPipelinesConfigReader configReader = new NGSPipelinesConfigReader();
		BufferedReader FHSource = new BufferedReader( new FileReader(source) );
		BufferedWriter FHDestination = new BufferedWriter( new FileWriter(destination, true) );
		try {
			String line ;
			while ((line = FHSource.readLine()) != null) {
				if( !line.startsWith("#") ) {
					String fields[] = line.split( "\t" );
					String label = fields[2] + ( !fields[1].equals(".") ? " (" + fields[1] + ")" : "" );
					String trackType = fields[2] + ( !fields[1].equals(".") ? "." + fields[1] : "" );
					if( !this.jBrowseTracks.containsKey(trackType) )
					{
						// Add track
						String glyph = configReader.getJBrowseGlyph(fields[2]);
						this.jBrowseTracks.put( label, new Track(trackType, trackType, glyph, label) );
					}
					FHDestination.write( line.replace(fields[1]+ "\t" + fields[2] + "\t", fields[1]+ "\t" + trackType + "\t") + "\n" );
				}
			}
		} finally {
			FHSource.close();
			FHDestination.close();
		}
	}
	
	private void addVCFContent( File vcf, File destination ) throws Exception
	{
		NGSPipelinesConfigReader configReader = new NGSPipelinesConfigReader();
		BufferedReader FHSource = new BufferedReader( new FileReader(vcf) );
		BufferedWriter FHDestination = new BufferedWriter( new FileWriter(destination, true) );
		try {
			String source = null ;
			String line   = null ;
			while ((line = FHSource.readLine()) != null) {
				if( !line.startsWith("#") ) {
					String fields[] = line.split( "\t" );
					String pos = fields[1] ;
					String ref = fields[3] ;
					String alt = fields[4] ;
					String score = fields[5] ;
					String type = "variant" ;
					String[] altList = alt.split(",");
					for(String currentAlt : altList)
					{
						if( ref.length() != currentAlt.length() || currentAlt.contains(".") ) {
							type = "indel" ;
						}
					}
					String label = type + " (" + source + ")" ;
					String trackType = type + "." + source ;
					if( !this.jBrowseTracks.containsKey(label) )
					{
						// Add track
						String glyph = configReader.getJBrowseGlyph(type);
						this.jBrowseTracks.put( label, new Track(trackType, trackType, glyph, label) );
					}
					FHDestination.write( fields[0] + "\t" + source + "\t" + trackType + "\t" + pos + "\t" + pos + "\t" + score + "\t" + "+" + "\t" + "." + "\t" + "Name=" + ref + "/" + alt.replaceAll( ",", "/" ) + ";Ref=" + ref + ";"+ "\n" );
				} else if( line.startsWith("##source=") ){
					source = line.replaceAll( "##source=", "" );
				}
			}
		} finally {
			FHSource.close();
			FHDestination.close();
		}
	}

	public void createConfig() throws Exception
	{
		this.configTmpFile = new File( this.jBrowseLoadDir, this.jBrowseFolder.getName() + ".config" );
		BufferedWriter FHConfig = new BufferedWriter( new FileWriter(this.configTmpFile) );
		try {
			FHConfig.write("{\n");
			FHConfig.write("  \"description\": \"NGSpipelines data\",\n");
			FHConfig.write("  \"db_adaptor\": \"Bio::DB::SeqFeature::Store\",\n");
			FHConfig.write("  \"db_args\": { \"-adaptor\": \"memory\",\n");
			FHConfig.write("               \"-dir\": \"" + this.jBrowseLoadDir.getPath() + "\" },\n");
			FHConfig.write("\n");
			FHConfig.write("  \"TRACK DEFAULTS\": {\n");
			FHConfig.write("   \"class\": \"feature\",\n");
			FHConfig.write("    \"autocomplete\": \"all\"\n");
			FHConfig.write("  },\n");
			FHConfig.write("\n");
			FHConfig.write("  \"tracks\": [\n");
			Set<String> tracksKeys = this.jBrowseTracks.keySet() ;
			int nbTracks = tracksKeys.size() ;
			int idx = 1 ;
		    for( String key : tracksKeys )
			{
		    	Track currentTrack = this.jBrowseTracks.get(key) ;
				FHConfig.write( currentTrack.toJSONString() );
				if( idx == nbTracks ) {
					FHConfig.write( "\n" );
				} else {
					FHConfig.write( ",\n" );
				}
				idx++ ;
			}
			FHConfig.write("  ]\n");
			FHConfig.write("}\n");
		} finally {
			FHConfig.close();
		}
	}
	
	public void loadConfig()  throws Exception
	{
		String biodbToJsonBinPath = new File( this.jbrowseBinDir, "biodb-to-json.pl" ).toString() ;
		String[] biodbToJsonArgs = { biodbToJsonBinPath, "--conf", this.configTmpFile.getPath(), "--out", this.jBrowseFolder.getPath() };
		CommandRunner biodbToJsonRunner = new CommandRunner( biodbToJsonArgs );
		biodbToJsonRunner.setEnv(perl5libKey, this.jbrowseExtLibDir, true) ; // Set perl5Lib to get extra libraries
		
		if(biodbToJsonRunner.execute() != 0 ) {
			throw new Exception( "biodbToJson : " + biodbToJsonRunner.getErrorMessage() );
		}
	}
	
	public void generateNames()  throws Exception
	{
		String generateNamesPath = new File( this.jbrowseBinDir, "generate-names.pl" ).toString() ;
		String[] generateNamesArgs = { generateNamesPath, "--dir", this.jBrowseFolder.getPath() };
		CommandRunner generateNamesRunner = new CommandRunner( generateNamesArgs );
		generateNamesRunner.setEnv(perl5libKey, this.jbrowseExtLibDir, true) ; // Set perl5Lib to get extra libraries
 
		if(generateNamesRunner.execute() != 0 ) {
			throw new Exception( "generateNames : " + generateNamesRunner.getErrorMessage() );
		}
	}
	
	public void deleteFolder( File fileToDelete )
	{
		File[] files = fileToDelete.listFiles();
	    if( files != null )
	    {
	        for( File f: files )
	        {
	        	if( f.isDirectory() )
	        	{
	                deleteFolder( f );
	            } else {
	                f.delete() ;
	            }
	        }
	    }
	    fileToDelete.delete();
	}
	
	public void deleteTmp()
	{	
		// delete files
		if( this.jBrowseLoadDir != null && this.jBrowseLoadDir.exists() ) {
			this.deleteFolder( this.jBrowseLoadDir );
		}
		
	    // init var
	    this.jBrowseLoadDir = null ;
	    this.tmpGFF = null ;
	    this.configTmpFile = null ;
	}
	
	@Override
	public void afterQuery() {
		try {
			String curDir = System.getProperty("user.dir");
			this.jbrowseBinDir = new File(curDir, "plugins/ngspipelines/public/ngspipelines/js/lib/jbrowse/bin").getPath() ;
			this.jbrowseExtLibDir = new File(curDir, "plugins/ngspipelines/public/ngspipelines/js/lib/jbrowse/extlib/lib/perl5").getPath() ;
			
			// Param from client side
			String filterDecode = java.net.URLDecoder.decode(database.getFilterValue("_analysis__Analysis__main__analysis_type"), "UTF-8") ;
			ParametersReader params = new ParametersReader( filterDecode );
			HashMap dbFields = (HashMap) params.get("dbFields") ;
			List<String> featuresFilesList = (List<String>) params.get("featuresFiles") ;
			this.preProcess = params.get("preProcess").toString() ;
			this.jBrowseFolder = new File( params.get("folderPath").toString() );
			String webPath = params.get("webPath").toString() ;

			// NGSpipelines config file
			this.configReader = new NGSPipelinesConfigReader();
			if (!new File(this.configReader.getStorageDirectory()).exists()){
				throw new Exception("Storage directory must be defined in properties file");
			}

			// If the contig has never been requested
			if ( this.preProcess.equals("true") ) {
				// Retrieve temp dir
				this.jBrowseLoadDir = new File( this.jBrowseFolder, this.jBrowseFolder.getName() + "_tmp" );
	
				// Features files
				for( String filePath : featuresFilesList )
				{
					this.addFileFeatures( filePath, this.getTmpGFF() );
				}
	
				// Features from db
				this.addDBFeatures( dbFields, this.getTmpGFF() );
	
				// Create the jbrowse config file
				this.createConfig();
	
				// Load config
				this.loadConfig();
				this.generateNames();
			}

			// Finaly return the HTML page with the jbrowse instance
			String html = "<!DOCTYPE html>\n"
						+ "<html>\n"
						+ "    <head>\n"
						+ "        <meta http-equiv=\"Content-Type\" content = \"text/html; charset=utf-8\">\n"
						+ "        <title>JBrowse</title>\n"
						+ "        <link rel=\"stylesheet\" type=\"text/css\" href=\"/ngspipelines/js/lib/jbrowse/genome.css\"></link>\n"
						+ "        <script type=\"text/javascript\" src=\"/ngspipelines/js/lib/jbrowse/src/dojo/dojo.js\" data-dojo-config=\"deps: ['JBrowse'], async: 1\"></script>\n"
						+ "        <script type=\"text/javascript\">\n"
						+ "            window.onerror=function(msg){\n"
						+ "                document.body.setAttribute(\"JSError\",msg);\n"
						+ "            }\n"
						+ "            var JBrowse;\n"
						+ "            require(\n"
						+ "                { baseUrl: '/ngspipelines/js/lib/jbrowse/src', packages: ['JBrowse','dojo','dijit','dojox','jszlib'] },\n"
						+ "                [ 'JBrowse/Browser', 'dojo/io-query' ],\n"
						+ "                function (Browser,ioQuery) {\n"
						+ "                    var queryParams = ioQuery.queryToObject( window.location.search.slice(1) );\n"
						+ "                    var dataRoot = queryParams.data || '" + webPath + "';\n"
						+ "                    JBrowse = new Browser({\n"
						+ "                        containerID: \"GenomeBrowser\",\n"
						+ "                        refSeqs: dataRoot + \"/seq/refSeqs.json\",\n"
						+ "                        baseUrl: dataRoot+'/',\n"
						+ "                        include: [\n"
						+ "                            '/ngspipelines/js/lib/jbrowse/jbrowse_conf.json',\n"
						+ "                            dataRoot + \"/trackList.json\"\n"
						+ "                        ],"
						+ "                        nameUrl: dataRoot + \"/names/root.json\",\n"
						+ "                        defaultTracks: \"DNA,gene,mRNA,noncodingRNA\",\n"
						+ "                        queryParams: queryParams,\n"
						+ "                        browserRoot: \"/ngspipelines/js/lib/jbrowse/\",\n"
						+ "                        location: queryParams.loc,\n"
		                + "                        forceTracks: queryParams.tracks,\n"
		                + "                        show_nav: queryParams.nav,\n"
		                + "                        show_tracklist: queryParams.tracklist,\n"
		                + "                        show_overview: queryParams.overview,\n"
		                + "                        config_list: queryParams.config\n"
		                + "                    });\n"
		                + "                }\n"
		                + "            );\n"
		                + "        </script>\n"
						+ "    </head>\n"
						+ "    <body>\n"
						+ "        <div id=\"GenomeBrowser\" style=\"height: 100%; width: 100%; padding: 0; border: 0;\"></div>\n"
						+ "        <div style=\"display: none\">JBrowseDefaultMainPage</div>\n"
						+ "    </body>\n"
						+ "</html>\n";
			originalOut.write(html.getBytes());
		} catch (Exception e) {
			try {
				String html = "<!DOCTYPE html>\n"
							+ "<html>\n"
							+ "	<head>\n"
							+ "		<meta http-equiv=\"Content-Type\" content = \"text/html; charset=utf-8\">\n"
							+ "		<title>JBrowse</title>\n"
							+ "	</head>\n"
							+ "	<body>\n"
							+ "		<div >An error has occured while attempting to initialize jbrowse.</div>\n"
							+ "	</body>\n"
							+ "</html>\n";
				originalOut.write(html.getBytes());
			} catch( Exception excep ) {
				// nothing
			}
			throw new BioMartException(e);

		} finally {
			// Delete temporary files
			this.deleteTmp();
		}
	}
}