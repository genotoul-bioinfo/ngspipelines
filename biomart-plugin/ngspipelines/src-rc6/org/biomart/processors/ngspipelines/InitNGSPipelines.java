
package org.biomart.processors.ngspipelines;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.biomart.common.exceptions.BioMartException;
import org.biomart.processors.ProcessorImpl;
import org.biomart.processors.ngspipelines.utils.CommandRunner;
import org.biomart.processors.ngspipelines.utils.NGSPipelinesConfigReader;
import org.biomart.queryEngine.Query;

/**
 * Class in charge to initialize the NGSPipelines plugin
 * @return error=false if initialization succed, error=true otherwise
 */
public class InitNGSPipelines extends ProcessorImpl {

    private NGSPipelinesConfigReader configReader;
    
	@Override
	public void beforeQuery(Query query, OutputStream outputHandle) throws IOException {
		out = new FilterOutputStream(new ByteArrayOutputStream());
	}

    @Override
    public void afterQuery() {
    	 try {
             configReader = new NGSPipelinesConfigReader();

             String curDir = System.getProperty("user.dir");
             String pluginsDir = new File(curDir, "plugins").toString();
             String ngsDir = new File(pluginsDir, "ngspipelines").toString();
             String publicDir = new File(ngsDir, "public").toString();
             String ngspDir = new File(publicDir, "ngspipelines").toString();
             File dataDir = new File(ngspDir, "data");
             File storageDir = new File(configReader.getStorageDirectory());

             String error = "";

             // Does the data directory exist ? Or the storageDir has changed.
             if(!dataDir.exists() || dataDir.getCanonicalPath() != storageDir.getCanonicalPath() ){
                 // Does the storage directory exist ?
                 if (storageDir.exists()){
                     // Is the storage directory empty ?
                     if(storageDir.listFiles().length == 0){
                    	 
                         error += "Data in "+storageDir.toString()+" are missing !";
                         out.write(error.getBytes());
                     }
                 }
                 // Storage directory is unknown
                 else{
                     error += "Not able to read "+storageDir.toString()+" !";
                     out.write(error.getBytes());
                 }
             }
             
         } catch (Exception e) {
        	 throw new BioMartException(e);
         }
    }

}
