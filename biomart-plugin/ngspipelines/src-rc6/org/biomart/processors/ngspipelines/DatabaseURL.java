package org.biomart.processors.ngspipelines;

import java.io.IOException;
import java.io.OutputStream;

import org.biomart.common.exceptions.BioMartException;
import org.biomart.processors.ProcessorImpl;
import org.biomart.processors.annotations.ContentType;
import org.biomart.processors.ngspipelines.utils.NGSPipelinesConfigReader;
import org.biomart.processors.ngspipelines.utils.RC6OutputStream;
import org.biomart.queryEngine.Query;

@ContentType("application/json")
public class DatabaseURL extends ProcessorImpl {

	private NGSPipelinesConfigReader configReader;
	private final String ACCESSION_PLACE_HOLDER = "##ACCESSION##";
	private String  allURLS = "";
	private OutputStream originalOut;

	public DatabaseURL(){
		
		try {
			configReader = new NGSPipelinesConfigReader();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private class DatabaseURLOutputStream extends RC6OutputStream {
		
		public DatabaseURLOutputStream(OutputStream out) {
			super(out);
		}
        
		@Override
		public void apply(String[] columns) {

			try {
				
				String url = configReader.getDatabaseURL(columns[1]);
				url = url.replace(ACCESSION_PLACE_HOLDER, columns[0]);
				String msg = "{\"accession\":\"" + columns[0] + "\" ,  \"href\" : \"" + url + "\" }";
				allURLS += msg + ",";

			} catch (Exception e) {
				throw new BioMartException(e);
			}
		}
	};

	@Override
	public void beforeQuery(Query query, OutputStream outputHandle)	throws IOException {
		originalOut = outputHandle ;
		out = new DatabaseURLOutputStream(outputHandle);
	}

	@Override
	public void afterQuery() {
		try {
			originalOut.write( ("{\"data\":[").getBytes() );
			originalOut.write( allURLS.substring(0, allURLS.length() - 1) .getBytes());
			originalOut.write( ("]}").getBytes() );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
