package org.biomart.processors.ngspipelines;

import java.io.File;
import java.io.FilenameFilter;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.HashMap;

import org.biomart.common.exceptions.BioMartException;
import org.biomart.processors.ProcessorImpl;
import org.biomart.processors.annotations.ContentType;
import org.biomart.processors.ngspipelines.utils.CommandRunner;
import org.biomart.processors.ngspipelines.utils.DBObject;
import org.biomart.processors.ngspipelines.utils.NGSPUtils;
import org.biomart.processors.ngspipelines.utils.NGSPipelinesConfigReader;
import org.biomart.processors.ngspipelines.utils.RC6OutputStream;
import org.biomart.processors.ngspipelines.utils.ParametersReader;
import org.biomart.processors.ngspipelines.utils.Utils;
import org.biomart.queryEngine.Query;

@ContentType("text/html")
public class JBrowseProcessRef extends ProcessorImpl {
	private String perl5libKey      = "PERL5LIB" ;
	private String jbrowseBinDir    = null ;
	private String jbrowseExtLibDir = null ;
	
	private File jBrowseLoadDir = null ;
	private File jBrowseFolder  = null ;
	private File referenceFile = null ;
	
	private NGSPipelinesConfigReader configReader ; 
	
	private OutputStream originalOut;
	private DBObject database;
	protected Connection connection = null;
	
	public JBrowseProcessRef() {}
	
	@Override
	public void beforeQuery(Query query, OutputStream outputHandle) {
		database = new DBObject(query);
		originalOut = outputHandle;
		out = new JBrowseOutputStream(outputHandle);
	}
	
	private class JBrowseOutputStream extends RC6OutputStream  {
		public JBrowseOutputStream(OutputStream out) {
			super(out);
		}

		@Override
		public void apply(String[] columns) {}
	};
	
	public void deleteFolder( File fileToDelete )
	{
		File[] files = fileToDelete.listFiles();
	    if( files != null )
	    {
	        for( File f: files )
	        {
	        	if( f.isDirectory() )
	        	{
	                deleteFolder( f );
	            } else {
	                f.delete() ;
	            }
	        }
	    }
	    fileToDelete.delete();
	}
	
	public void createReference( String seqName, String seqString ) throws Exception
	{
		this.referenceFile = new File( this.jBrowseLoadDir, this.jBrowseFolder.getName() + ".fa" );
		FileWriter fstream    = new FileWriter( this.referenceFile );
		BufferedWriter outf   = new BufferedWriter(fstream);
		outf.write( ">" +seqName + "\n" );
		outf.write( seqString );
		outf.close();

		// prepareSeqBinArgs
		String prepareSeqBinPath = new File( this.jbrowseBinDir, "prepare-refseqs.pl" ).toString() ;
		String[] prepareSeqBinArgs = { prepareSeqBinPath, "--fasta", this.referenceFile.getPath(), "--out", this.jBrowseFolder.getPath() };
		CommandRunner prepareSeqBinRunner = new CommandRunner( prepareSeqBinArgs );
		prepareSeqBinRunner.setEnv( this.perl5libKey, this.jbrowseExtLibDir, true) ; // Set perl5Lib to get extra libraries
		if( prepareSeqBinRunner.execute() != 0 ){
			throw new Exception( "prepareSeq : " + prepareSeqBinRunner.getErrorMessage() );
		}
	}
	
	@Override
	public void afterQuery() {
		try {
			String curDir = System.getProperty("user.dir");
			this.jbrowseBinDir = new File(curDir, "plugins/ngspipelines/public/ngspipelines/js/lib/jbrowse/bin").getPath() ;
			this.jbrowseExtLibDir = new File(curDir, "plugins/ngspipelines/public/ngspipelines/js/lib/jbrowse/extlib/lib/perl5").getPath() ;

			// Param from client side
			String filterDecode = java.net.URLDecoder.decode(database.getFilterValue("_analysis__Analysis__main__analysis_type"), "UTF-8") ;
			ParametersReader params = new ParametersReader( filterDecode );
			this.jBrowseFolder = new File( params.get("folderPath").toString() );
			String seqName = params.get("seqName").toString() ;
			String seqString = params.get("seqString").toString() ;

			// NGSpipelines config file
			this.configReader = new NGSPipelinesConfigReader();
			if (!new File(this.configReader.getStorageDirectory()).exists()){
				throw new Exception("Storage directory must be defined in properties file");
			}

			// If the contig has never been requested
			if (!this.jBrowseFolder.exists()) {
				this.deleteFolder( this.jBrowseFolder );
				this.jBrowseFolder.mkdirs();
			}

			// Create temp dir
			this.jBrowseLoadDir = new File( this.jBrowseFolder, this.jBrowseFolder.getName() + "_tmp" );
			if (!this.jBrowseLoadDir.exists()) {
				this.jBrowseLoadDir.mkdirs() ;
			}

			// Create ref file
			this.createReference( seqName, seqString );

			originalOut.write( this.referenceFile.getPath().getBytes() );
		} catch (Exception e) {
			// Delete referenceFile
			if( this.referenceFile != null && this.referenceFile.exists() )
			{
				this.referenceFile.delete() ;
				this.referenceFile = null ;
			}
			// throw exception
			throw new BioMartException(e);	
		}
	}
}