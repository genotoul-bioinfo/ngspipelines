package org.biomart.processors.ngspipelines.dea;


import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;
import java.util.StringTokenizer;

import org.biomart.processors.ngspipelines.utils.Utils;


/**
 * 
 * @author inabihoudine
 *
 */
public class DEAUtils {
	
	
	//lock
	protected static String ERROR_LOG = "error.log";
	protected static String LOCK_FILE = ".lock";
	private final static String SEP = System.getProperty("file.separator");
	
	
	/**
	 * 
	 */
	public DEAUtils(){

	}
	
	/**
	 * Return true if the dea is still running
	 * @param outputDir
	 * @return
	 */
	public static boolean isRunning( String outputDir){
		File lock = new File(outputDir + SEP + LOCK_FILE);
		return lock.exists();
	}
	
	/**
	 * Return true if the process get an error
	 * @param outputDir
	 * @return
	 */
	public static boolean errored( String outputDir){
		File errorLog = new File(outputDir + SEP + ERROR_LOG);
		return errorLog.exists();
	}
	
	public static String getErroLog( String outputDir){
		return Utils.readFile(outputDir + SEP + ERROR_LOG) ;
	}
	
}
