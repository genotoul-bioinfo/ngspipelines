<!doctype html>

<%@ page language="java" %>
<%@ page session="false" %>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/bmtaglib.tld" prefix="bm" %>

<html lang="en-ca">

<c:set var="currentPage" scope="request">
	NGSPipelines
</c:set>

<head>
	<c:import url="/conf/config.jsp" context="/"/>
	<c:import url="/_head.jsp?path=../" context="/"/>
</head>

<!--[if lt IE 7 ]> <body id="converter" class="biomart layout1 ie6 "> <![endif]--> 
<!--[if IE 7 ]>    <body id="converter" class="biomart layout1 ie7 "> <![endif]--> 
<!--[if IE 8 ]>    <body id="converter" class="biomart layout1 ie8 "> <![endif]--> 
<!--[if !IE]><!--> <body id="converter" class="biomart layout1"> <!--<![endif]--> 

<div id="biomart-top-wrapper" class="ui-corner-all clearfix">
	<div id="biomart-header">
		<div class="content">
			<jsp:include page="/_header.jsp"/>
		</div>
	</div>
	<jsp:include page="/_context.jsp">
		<jsp:param name="path" value="../"/>
	</jsp:include>
	<div id="ngspipelines-dialog" style="display: none" title="">
  		<div id="ngspipelines-dialog-body" class="login-help clearfix"> </div>
	</div>

	<div id="biomart-wrapper" class="ui-corner-all clearfix">
		<div id="biomart-results-wrapper" style="width:1000px;margin: -20px auto 20px auto;font-family:'Ubuntu',arial,serif;font-size:12px;line-height:25px"> 
			<div class="bloc">
				<div class="bloc-title"><h1>General <small> Frequently Asked Questions </small></h1></div>
				<section class="faq-section">
				    <input type="checkbox" id="g_q1">
				    <label for="g_q1">Where is the list of all the performed analysis ?</label>           
					<p>
						There are on the Project page. 							
					</p>   
				</section>
				
				<section class="faq-section">
				    <input type="checkbox" id="g_q2">
				    <label for="g_q2">How can I do Venn diagram with 5 pool ?</label>           
					<p>
						Use the following web page : <a href="http://bioinfo.genotoul.fr/index.php?id=116" > Venny demo page </a> 							
					</p>   
				</section>
				<section class="faq-section">
				    <input type="checkbox" id="g_q3">
				    <label for="g_q3">I can't open a .gz file, what is the problem ?</label>           
					<p>
						When you download an available gz file, the web server re-zip it (it's independent to our development) but the file you have is name file.gz. If you want to access to the data rename it file.gz.gz and unzip it twice. 							
					</p>   
				</section>
				<div class="bloc-footer"></div>
			</div>
	
			<div class="bloc">
				<div class="bloc-title"><h1>RNAseqDeNovo <small> Frequently Asked Questions </small></h1></div>
				<section class="faq-section">
				    <input type="checkbox" id="rd_q1">
				    <label for="rd_q1">How can I get the manual ?</label>           
					<p>
						Have a look on our <a href="http://mulcyber.toulouse.inra.fr/frs/?group_id=149">project documentation page</a>.
					</p>   
				</section>
				<section class="faq-section">
				    <input type="checkbox" id="rd_q2">
				    <label for="rd_q2">How get fasta of a list of contigs ?</label>           
					<p>
						Use the martform and select "sequence" attribute as output.  
					</p>   
				</section>
				<section class="faq-section">
				    <input type="checkbox" id="rd_q3">
				    <label for="rd_q3">How are named my contigs ?</label>           
					<p>
						Since March 2013, a contigs are prefixed by species name (max 3 letters), then the name of the best annotation gene, then 2 numbers the second is the number of contigs named with this gene the first the current index of this gene.
						e.g. DR_SACB.3.10 correspond to the third contig on ten with the gene name SACB.
					</p>   
				</section>
				<section class="faq-section">
				    <input type="checkbox" id="rd_q4">
				    <label for="rd_q4">Which statistical test is behind DDD ?</label>           
					<p>
						We perform an exact fisher test for each gene, adjust the p-value with FDR correction (Benjamini and Hochberg, 1995) and identifie in which pool it's up or down regulated by comparing the frequence.
					</p>   
				</section>
				<section class="faq-section">
				    <input type="checkbox" id="rd_q5">
				    <label for="rd_q5">How can I do DEseq or EdgeR analysis ?</label>           
					<p>
						Those kind of analysis are not implemented.
						Download the count file named merged_count.csv in download tabs and use R.
					</p>   
				</section>
				<section class="faq-section">
				    <input type="checkbox" id="rd_q6">
				    <label for="rd_q6">How can I see all my alignment ?</label>           
					<p>
						The JBrowse View may don't work, please send an email to celine.noirot@toulouse.inra.fr with an example of the error. 
						An alternative solution is to download bam files and annotation file in gff format, and use <a href="www.broadinstitute.org/igv/">IGV</a> on your desktop.
					</p>   
				</section>
				<section class="faq-section">
				    <input type="checkbox" id="rd_q7">
				    <label for="rd_q7">What are the meaning of the columns Q-cov, S-cov, Ident ?</label>           
					<p>% Qcoverage or Q-cov : Percentage of the query length covered by the alignemnt (all hsps between the contigs and the subject)</p>
					<p>% Scoverage or S-cov : Percentage of the subject covered  by the alignemnt (all hsps between the contigs and the subject)</p>
					<p>% Qidentities or ident: Percent of identity for the complete alignment (all hsp	between the contigs and the subject)</p>
					<p>% Identity : Percent of identity for the HSP alignment</p>
					<p>% Qcoverage : Percent of conserved aa for the complete alignment (all hsp between the contigs and the subject)</p>
				</section>
				<div class="bloc-footer"></div>
			</div>
			
			<div class="bloc">
				<div class="bloc-title"><h1>variantdenovo <small> Frequently Asked Questions </small></h1></div>
				<section class="faq-section">
				    <input type="checkbox" id="sd_q1">
				    <label for="sd_q1">How my SNPs are annotated ?</label>           
					<p>
						The SNPs are annotated with an homemade program (tSNPannot) which is describe <a href="https://mulcyber.toulouse.inra.fr/frs/?group_id=154">here</a>
					</p>   
				</section>
				<section class="faq-section">
				    <input type="checkbox" id="sd_q2">
				    <label for="sd_q2">How can I see my alignment for a given SNP?</label>           
					<p>
						Display the alignment for variant application is very heavy, we suggest you to download bam files and gff format file of SNPs for a given contig, and use <a href="www.broadinstitute.org/igv/">IGV</a> on your desktop. 
					</p>   
				</section>
				<div class="bloc-footer"></div>
			</div>
		</div>
	</div>
	<div id="biomart-footer" class="clearfix">
		<jsp:include page="/_footer.jsp"/>
	</div>
</div>

<c:import url="/_js_includes.jsp?path=../" context="/"/>

<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />

<style>
	.faq-section{
		margin: 20px 0;
		position: relative;
	}

	.faq-section p{
		display: none;
		padding: 0 5px;
		text-align: justify;
		/*border: 1px solid #999999;*/
		-moz-box-shadow: 0px 0px 8px #eee;
		-webkit-box-shadow: 0px 0px 8px #eee;
		box-shadow: 0px 0px 8px #eee;
	}	

	.faq-section input{
		position: absolute;
		z-index: 2;
		cursor: pointer;
		opacity: 0;			
		display: none\9; /* IE8 and below */
        margin: 0;			
		width: 100%;
		height: 36px;
	}				

	.faq-section label+p{
		display: block;	
		color: #999;
		font-size: .85em;
		-webkit-transition: all .15s ease-out; 
		-moz-transition: all .15s ease-out;
		-ms-transition: all .15s ease-out;
		-o-transition: all .15s ease-out;
		transition: all .15s ease-out;
		/* Clipping text */
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;						
	}

	.faq-section input[type=checkbox]:checked~p{
		display: block;
		color: #444;
		font-size: 99%;
		/* restore clipping defaults */
		text-overflow: clip; 
		white-space: normal;
		overflow: visible;
		box-shadow: none;
		margin: 0 8px;
	}
	.faq-section input[type=checkbox]:checked~p:last-child{
		display: block;
		color: #444;
		font-size: 99%;
		/* restore clipping defaults */
		text-overflow: clip; 
		white-space: normal;
		overflow: visible;
		-moz-box-shadow: 0px 6px 4px #EEEEEE;
		-webkit-box-shadow: 0px 6px 4px #EEEEEE;
		box-shadow: 0px 6px 4px #EEEEEE;
		margin: 0 8px;
	}

	.faq-section label{
		/*font-size: 1.2em;*/
		/*height: 20px;*/
		cursor: pointer;
		/*background: #f5f5f5;*/
		display: block;
		position: relative;
		padding: 0px 10px;
		font-weight: bold;
		border: 1px solid #ddd;
		border-left: 3px solid #888;
		text-shadow: 0 1px 0 rgba(255,255,255,.5);
		-webkit-transition: all .15s ease-out; 
		-moz-transition: all .15s ease-out;
		-ms-transition: all .15s ease-out;
		-o-transition: all .15s ease-out;
		transition: all .15s ease-out;			
	}

	.faq-section label::-moz-selection{ /* remove text selection when toggle-ing */
		background: none;
	}

	.faq-section label::selection{
		background: none;
	}

	.faq-section label:hover{
		background: #eee;
	}

	.faq-section input[type=checkbox]:checked~label{
		/* border-color: #ff7f50; */
		background: #eee;
		border-left: 3px solid #ff7f50;		
		/*
		 background: #f5deb4;
		 background-image: -webkit-gradient(linear, left top, left bottom, from(#eee), to(#f5deb4));
		 background-image: -webkit-linear-gradient(top, #eee, #f5deb4); 
		 background-image: -moz-linear-gradient(top, #eee, #f5deb4); 
		 background-image: -ms-linear-gradient(top, #eee, #f5deb4); 
		 background-image: -o-linear-gradient(top, #eee, #f5deb4); 
		 background-image: linear-gradient(to bottom, #eee, #f5deb4); 
		 -moz-box-shadow: 0 0 1px rgba(0,0,0,.4);
		 -webkit-box-shadow: 0 0 1px rgba(0,0,0,.4);
		 box-shadow: 0 0 1px rgba(0,0,0,.4);
		*/
	}

	.faq-section label::before{
		content: '';
		position: absolute;
		right: 4px;
		top: 50%;
		margin-top: -6px;
		border: 6px solid transparent;
		border-left-color: inherit;	
	}

	.faq-section input[type=checkbox]:checked~label::before{
		border: 6px solid transparent;
		border-top-color: #ff7f50;
		margin-top: -3px;
		right: 10px;	
	}
</style>

<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <style>
		.faq-section label,
		.faq-section label:hover{
			cursor: default;
			background: #eee;
		}        
		body .faq-section p{
			display: block;
			color: #444;
			font-size: 1em;
			text-overflow: clip; 
			white-space: normal;
			overflow: visible;				
		}
    </style>
<![endif]--> 	

</body>
</html>
