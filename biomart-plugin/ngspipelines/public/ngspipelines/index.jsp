<!doctype html>

<%@ page language="java" %>
<%@ page session="false" %>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/bmtaglib.tld" prefix="bm" %>

<html lang="en-ca">

<c:set var="currentPage" scope="request">
	NGSPipelines
</c:set>

<head>
	<c:import url="/conf/config.jsp" context="/"/>
	<c:import url="/_head.jsp?path=../" context="/"/>
</head>

<!--[if lt IE 7 ]> <body id="converter" class="biomart layout1 ie6 "> <![endif]--> 
<!--[if IE 7 ]>    <body id="converter" class="biomart layout1 ie7 "> <![endif]--> 
<!--[if IE 8 ]>    <body id="converter" class="biomart layout1 ie8 "> <![endif]--> 
<!--[if !IE]><!--> <body id="converter" class="biomart layout1"> <!--<![endif]--> 

<div id="biomart-top-wrapper" class="ui-corner-all clearfix">
	<div id="biomart-header">
		<div class="content">
			<jsp:include page="/_header.jsp"/>
		</div>
	</div>
	<jsp:include page="/_context.jsp">
		<jsp:param name="path" value="../"/>
	</jsp:include>
	<div id="ngspipelines-dialog" style="display: none" title="">
  		<div id="ngspipelines-dialog-body" class="login-help clearfix"> </div>
	</div>
	<div id="biomart-wrapper" class="ui-corner-all clearfix"></div>
	<div id="biomart-footer" class="clearfix">
		<jsp:include page="/_footer.jsp"/>
	</div>
</div>
<div id="biomart-loading">
	<p class="message">
    <bm:message code="loading" capitalize="true"/> NGSPipelines
	</p>
	<span class="loading"></span>
</div>

<c:import url="/_js_includes.jsp?path=../" context="/"/>

<link rel="stylesheet" type="text/css" href="css/jquery.dataTable.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />

<script type="text/javascript" src="js/lib/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/lib/TableTools-2.1.4/js/TableTools.min.js"></script>
<script type="text/javascript" src="js/lib/TableTools-2.1.4/js/ZeroClipboard.js"></script>
<link rel="stylesheet" type="text/css" href="css/TableTools.css" />
<script type="text/javascript" src="js/lib/FixedColumns.min.js"></script>

<script type="text/javascript" src="js/lib/jquery.tmpl.min.js"></script>
<script type="text/javascript" src="js/lib/jvenn.min.js"></script>
<script type="text/javascript" src="js/lib/highcharts.js"></script>
<script type="text/javascript" src="js/lib/drilldowns.js"></script>
<script type="text/javascript" src="js/lib/exporting.js"></script>
<script type="text/javascript" src="js/lib/highcharts.theme.js"></script>
<script type="text/javascript" src="js/lib/yepnope.1.5.4-min.js"></script>

<script type="text/javascript" src="js/controller/ngspipelines.js"></script>
<script type="text/javascript" src="js/model/ngspipelines.js"></script>
<script type="text/javascript" src="js/controller/download.js"></script>
<script type="text/javascript" src="js/model/download.js"></script>
<script type="text/javascript" src="js/controller/project.js"></script>
<script type="text/javascript" src="js/model/project.js"></script>
<script type="text/javascript" src="js/controller/help.js"></script>
<script type="text/javascript" src="js/model/help.js"></script>
<script type="text/javascript" src="js/controller/application/application.js"></script>
<script type="text/javascript" src="js/model/application/application.js"></script>
<script type="text/javascript" src="js/controller/application/favorites.js"></script>
<script type="text/javascript" src="js/model/application/favorites.js"></script>
<script type="text/javascript" src="js/controller/application/blast.js"></script>
<script type="text/javascript" src="js/model/application/blast.js"></script>
<script type="text/javascript" src="js/controller/application/statistics.js"></script>
<script type="text/javascript" src="js/model/application/statistics.js"></script>
<script type="text/javascript" src="js/controller/application/dea.js"></script>
<script type="text/javascript" src="js/model/application/dea.js"></script>
<script type="text/javascript" src="js/controller/application/venn.js"></script>
<script type="text/javascript" src="js/model/application/venn.js"></script>
<script type="text/javascript" src="js/controller/application/libraries.js"></script>
<script type="text/javascript" src="js/model/application/libraries.js"></script>

<script type="text/javascript" src="js/lib/ngspipelines.utils.js"></script>
<script type="text/javascript" src="js/lib/ngspipelines.ui.js"></script>

<script type="text/javascript" src="js/lib/jquery.multiDownload.js"></script>
<script type="text/javascript" src="js/lib/scientific.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
	
		biomart._state = { initialized: false };

		$.subscribe('biomart.ready', {ready:function(){
			// Trigger load datasets
        	$('#biomart-loading').fadeAndRemove();
        	biomart._state.initialized = true;
		}}, 'ready');
		
		
		$.subscribe('biomart.init', {init:function(){
	        
	        // Grab relevant info from URL fragment
	        var fragment = biomart.url.jsonify(location.href).fragment;
	        if (fragment) {
	            var urlHash = biomart.url.jsonify(fragment);
	        }
	        
	        var arr = urlHash.path.split('/'),
	            rootName = arr[0] || null,
        		martName = arr[1] || null;
        
	        ngspipelines.utils.loadMarts(rootName, martName, function(){
		    	// Build main ngspipelines plugin
	            var $biomartWrapper = $("#biomart-wrapper");
	            $biomartWrapper.ngspipelines({
	            	datasets: biomart._state.datasets,
	            	martName: biomart._state.martName,
	            	rootName: rootName,
	            	callback: function() {
	            		$.publish('biomart.ready');
	            	}
	            });
	    	});
	        
		}}, 'init');
	
		$.publish('biomart.login');	
    	$.subscribe('biomart.restart', {refresh:function(){location=location.href}}, 'refresh');
	});
</script>

</body>
</html>
