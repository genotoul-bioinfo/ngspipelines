(function($) {
$.namespace('ngspipelines.help', function(self) {	

	/* 
	 * Search available functionality set for an application (favorite?, blastsearch ?) 
	 * @param dataset the project dataset
	 * @param analysisDataset the analysis dataset
	 * @param libraryDataset the library dataset
	 * @param analysisTypes list of analysis type to search 
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams boolean set to True if the initialization is ok, False otherwise
	 */
	self.getHelpFeature = function(dataset, analysisDataset, applicationName, libraryDataset, analysisTypes ,successCB, errorCB){
		ngspipelines.utils.hasLibraries(
				libraryDataset,
				applicationName,
				function(hasLibrary) {
					ngspipelines.utils.hasAnalysis(dataset, 
							analysisDataset,
							applicationName,
							analysisTypes,
							function(data){
								// an application always has searchform and favorites
								data["searchform"]=true;
								data["favorites"]=true;
								
								data["libraries"] = hasLibrary;
								data["ddd"] = data["libraries_count"];
								data["venn"] = data["libraries_count"];
								delete data["libraries_count"];									
								successCB(data);
							},
							function(){
								errorCB("ngspipelines.help.getHelpFeature: An error occurred while attempting to load available Feature !");
							});
				
				});
	}
	
	/* 
	 * Get html content, try the specific content file, if return an error return the generic content file. 
	 * @param div div where the content have to be set (need to be set in result)
	 * @param specificFile the specific file
	 * @param genericFile the generic file
	 * @param successCB the callback function to call when loading is over and succeed
	 * @successCallBackParams result a json of div id and html content
	 */

	self.getSpecificOrGenericContent = function(div,specificFile, genericFile, successCB){
		
		$.ajax({
			  url: specificFile,
			  success: function(content) {
				  successCB({html:content,divId:div})
				  
			  },
			  error: function(XMLHttpRequest, textStatus, errorThrown) { 
				   if (XMLHttpRequest.status == 404) {
					   $.ajax({
							  url: genericFile,
							  success: function(content) {
								  successCB({html:content,divId:div})
							  },
							  error: function(XMLHttpRequest, textStatus, errorThrown) { 
								  successCB({html:"<p>Unable to retrieve help , in specific file and in generic file</p>",divId:div})
							  }
						});
				   } else {
				    	successCB({html:"<p>Unable to retrieve help , Unknow Error</p>",divId:div})
				   }     
			  }
			});
	}
	
	})

})(jQuery);
