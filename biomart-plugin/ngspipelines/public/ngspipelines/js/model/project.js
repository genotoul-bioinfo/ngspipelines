(function($) {
$.namespace('ngspipelines.project', function(self) {	

	
	/* 
	 * Return the disk space used in octet by the provided project
	 * @param projectDataset the project dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams int the size in octet of the project directory
	 */
	self.getDiskSpace = function(projectDataset, successCB, errorCB) {
		if (ngspipelines.utils.isAProjectDataset(projectDataset)) {
			var prefix = ngspipelines.utils.getDatasetPrefix(projectDataset),
				attributes = [
		  		    '<Attribute name="', prefix,'_project__Project__main__directory" />'
		  		    ].join('');
	
			$.ajax({
	  		    url: BIOMART_CONFIG.service.url + 'results',
	  		    data: {
	  		    		query: '<!DOCTYPE Query><Query client="webbrowser" processor="DiskSpace" limit="-1" header="0"><Dataset name="' + projectDataset.name + '" config="' + projectDataset.mart.name + '">' + attributes + '</Dataset></Query>'
	  		        },
	  		        success: function(results) {
	  		        	successCB(results.size);
	  		        },
	  		        error: function() {
	  		        	errorCB("ngspipelines.getDiskSpace: An error occured while attempting to access the project directory." +
	  		        			" This one not does not exists or is not readable from the web server!");
	  		        }
	  		});
		} else {
			errorCB("ngspipelines.project.getDiskSpace: An error occurred while attempting to load " + projectDataset.name + " as a project dataset!");
		}
	}
	
	/* 
	 * Project dataset loading function
	 * @param projectDataset the project dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the project object
	 */
	self.getProject = function(projectDataset, successCB, errorCB) {
		if (ngspipelines.utils.isAProjectDataset(projectDataset)) {
			var prefix = ngspipelines.utils.getDatasetPrefix(projectDataset),
				attributes = [
				    '<Attribute name="', prefix,'_project__Project__main__name" />',
				    '<Attribute name="', prefix,'_project__Project__main__species" />',
				    '<Attribute name="', prefix,'_project__Project__main__species_common_name" />',
				    '<Attribute name="', prefix,'_project__Project__main__directory" />',
				    '<Attribute name="', prefix,'_project__Project__main__description" />'
			    ].join('');
			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
			        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + projectDataset.name + '" config="' + projectDataset.mart.name + '">' + attributes + '</Dataset></Query>'
			        },
			        success: function(results) {
			        	var json = {};
	        			json["common_name"] = results.data[0]["Species common name"];
	        			json["description"] = results.data[0]["Description"];
	        			json["directory"] = results.data[0]["Directory"];
	        			json["name"] = results.data[0]["Species"];
	        			json["project_name"] = results.data[0]["Name"];
	        			json["species"] = results.data[0]["Species"];
			        	successCB(json);
			        },
			        error: function() {
			        	errorCB("ngspipelines.project.getProject: An error occurred while attempting to load the project from the database!");
			        }
			});
		} else {
			errorCB("ngspipelines.project.getProject: An error occurred while attempting to load " + projectDataset.name + " as a project!");
		}
	}
	
	
	
	self.getAnalysisByApplication = function(analysisDataset, successCB, errorCB){
		if (ngspipelines.utils.isAnAnalysisDataset(analysisDataset)) {
			var prefix = ngspipelines.utils.getDatasetPrefix(analysisDataset),
			attributes = [
						    '<Attribute name="', prefix,'_analysis__Analysis__main__analysis_id_key" />',
						    '<Attribute name="', prefix,'_analysis__Analysis__main__analysis_name" />',
						    '<Attribute name="', prefix,'_analysis__Analysis__main__analysis_type" />',
						    '<Attribute name="', prefix,'_analysis__Analysis__main__comments" />',
						    '<Attribute name="', prefix,'_analysis__Analysis__main__soft_name" />',
						    '<Attribute name="', prefix,'_analysis__Analysis__main__soft_parameters" />',
						    '<Attribute name="', prefix,'_analysis__Analysis__main__soft_version" />',
						    '<Attribute name="', prefix,'_analysis__Analysis__main__time" />',
						    '<Attribute name="', prefix,'_analysis__Analysis__main__categorie" />',
						    '<Attribute name="', prefix,'_analysis__Analysis__main__analysis_hidden" />'
					    ].join(''),
			filters =  filters = [
								    '<Filter name="', prefix,'_analysis__Analysis__main__analysis_hidden" value="0" />'
									].join('');
			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
			        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
			        },
			        success: function(results) {
			        	var	applicationsData = {},
			        		applicationsKey = new Array();

			        	for (var index in results.data) {
			        		if (results.data[index].Key != "remove" && results.data[index]["Categorie"] != undefined) {
			        			var currentAppli = results.data[index]["Categorie"];
			        			if ($.inArray(currentAppli, applicationsKey) == -1 ) {
			        				applicationsData[currentAppli]= {};
			        				applicationsKey.push(currentAppli);
			        			}
			        			
			        			applicationsData[currentAppli][results.data[index]["Analysis id key"]] = {
			        					name : results.data[index]["Analysis name"],
			        					type : results.data[index]["Analysis type"],
			        					comment : results.data[index]["Comments"],
			        					soft_name : results.data[index]["Soft name"],
			        					soft_parameters : results.data[index]["Soft parameters"],
			        					soft_version : results.data[index]["Soft version"],
			        					time : results.data[index]["Time"],
			        			};
			        		}
			        	}

			        	successCB(applicationsData);
			        },
			        error: function() {
			        	errorCB("ngspipelines.project.getAnalysisByApplication: An error occurred while attempting to load the analysis from the database!");
			        }
			});
		} else {
			errorCB("ngspipelines.project.getAnalysisByApplication: An error occurred while attempting to load " + analysisDataset.name + " as a list of analysis!");
		}
	}

	self.getAnalysisResults = function(analysisDataset, analyseID, successCB, errorCB) {
		
		var prefix = ngspipelines.utils.getDatasetPrefix(analysisDataset),
			attributes = [
			    '<Attribute name="', prefix,'_analysis__Result__dm__key" />',
			    '<Attribute name="', prefix,'_analysis__Result__dm__group" />',
			    '<Attribute name="', prefix,'_analysis__Result__dm__type" />',
			    '<Attribute name="', prefix,'_analysis__Result__dm__value" />',
			].join(''),
		    filters = [
			    
			    '<Filter name="', prefix,'_analysis__Result__dm__analysis_id_key" value = "', analyseID,'" />'
			].join('');
						
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	var	analysisData = {},
		        		analysisKey = new Array();
		        	
		        	for (var index in results.data) {
		        		if (results.data[index]["Group"] != undefined) {
			        		var currentData = results.data[index]["Group"];
			        		if ($.inArray(currentData, analysisKey) == -1 ) {
		        				analysisData[currentData]= {};
		        				analysisKey.push(currentData);
		        			}
			        		analysisData[currentData][results.data[index]["Key"]] = results.data[index]["Value"];
		        		}
		        	}
		        	successCB(analysisData);
		        },
		        error: function() {
		        	errorCB("ngspipelines.project.getAnalyseResults: An error occurred while attempting to load analysis dataset!");
		        }
		});

	}

	
});
})(jQuery);
