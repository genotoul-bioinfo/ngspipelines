(function($) {
$.namespace('ngspipelines', function(self) {	
	
	/* 
	 * Initialize NGSPipelines plugin
	 * @param projectDataset the project dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams boolean set to True if the initialization is ok, False otherwise
	 */
	self.init = function(projectDataset, successCB, errorCB) {
		if (ngspipelines.utils.isAProjectDataset(projectDataset)) {
			var prefix = ngspipelines.utils.getDatasetPrefix(projectDataset),
				attributes = [
		  		    '<Attribute name="', prefix,'_project__Project__main__directory" />'
	  		    ].join('');
	  		$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
			    	query: '<!DOCTYPE Query><Query client="webbrowser" processor="InitNGSPipelines" limit="-1" header="0"><Dataset name="' + projectDataset.name + '" config="' + projectDataset.mart.name + '">' + attributes + '</Dataset></Query>'
			    },
			    success: function(result) {
			    	if (result == "") {
			    		successCB();
			       	} else {
			       		errorCB(result);
			        }
			    }
	    	});
		} else {
			errorCB("ngspipelines.init: An error occurred while attempting to load " + projectDataset.name + " as a project dataset!");
		}
	}
	
	/* 
	 * Test if NGSPipelines required datasets are present
	 * @param project the project object
	 * @return true if all datasets are available, false otherwise
	 */
	self.datasetsAreOK = function(project) {
		if (project.dataset != null && project.analyses != null && project.libraries != null) {
			if (ngspipelines.utils.isAProjectDataset(project.dataset) && ngspipelines.utils.isAnAnalysisDataset(project.analyses) && ngspipelines.utils.isALibrariesDataset(project.libraries)) {
				return true;
	    	} else {
	    		return false;
	    	}
		} else {
			return false;
		}
	}

	
});
})(jQuery);