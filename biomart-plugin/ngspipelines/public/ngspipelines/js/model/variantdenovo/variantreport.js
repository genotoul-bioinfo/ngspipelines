(function($) {
$.namespace('ngspipelines.variantdenovo.variantreport', function(self) {	
	
	/* 
	 * Returns Id of the variant from its name
	 * @param variantName the contig Name to return
	 * @param variantDataset the variant dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json
	 */
	self.getIdFromVariantName = function(variantName, variantDataset, successCB, errorCB) {
		var prefix = ngspipelines.utils.getDatasetPrefix(variantDataset),
		attributes = [
		    '<Attribute name="', prefix,'_variantdenovo__Variant__main__variant_id_key" />',
		    '<Attribute name="', prefix,'_variantdenovo__Variant__main__variant_name" />'
	    ].join(''),
	    filters = [
		    '<Filter name="', prefix,'_variantdenovo__Variant__main__variant_name" value = "', variantName,'" />'
		].join('');
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + variantDataset.name + '" config="' + variantDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	successCB(results.data[0]["Variant id key"]);
		        },
		        error: function() {
		        	errorCB("ngspipelines.variantdenovo.variantreport.getIdFromVariantName: An error occurred while attempting to get variant " + variantName + "!");
		        }
		});
	}
	
	/* 
	 * Returns all variant's information
	 * @param variantID the variant ID to return
	 * @param variantdenovoDataset the variantdenovo dataset
	 * @param analysisDataset the analysis dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the variantdenovo
	 */
	self.getSNP = function(SNPID, variantdenovoDataset, analysisDataset, successCB, errorCB) {
		
		if (ngspipelines.utils.isAnAnalysisDataset(analysisDataset)) {
			var prefix = ngspipelines.utils.getDatasetPrefix(analysisDataset),
				attributes = [
				    '<Attribute name="', prefix,'_analysis__Result__dm__value" />',
				    '<Attribute name="', prefix,'_analysis__Analysis__main__analysis_type" />'
			    ].join(''),
			    filters = [
				    '<Filter name="', prefix,'_analysis__Analysis__main__analysis_type" value = "favorite_variant" />'
				].join('');
						
			// First load from the analysis table if the SNPs is flagged as favorites
			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
			        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
			        },
			        success: function(results) {
			        	
			        	// For each favorites variant
			        	var isFavorite = false;
			        	for (var index in results.data) {
			        		if (index != "remove") {
			        			if (results.data[index]["Value"] == SNPID) {
			        				isFavorite = true;
			        			}
			        		}
			        	}
			        	
						var prefix = ngspipelines.utils.getDatasetPrefix(variantdenovoDataset),
							attributes = [
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__variant_name" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__variant_id_key" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__type" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__alleles" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__quality" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__position" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__previous_variant" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__next_variant" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__previous_seq" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__next_seq" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__best_annot_species" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__best_annot_gene" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__best_annot_strand" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__best_annot_mutation_aa" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__best_annot_exon_five_prime_limit" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__best_annot_exon_three_prime_limit" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__best_annot_mutation_type" />',
							    '<Attribute name="', prefix,'_variantdenovo__Variant__main__contig_name" />',
						    ].join(''),
						    filters = [
							    '<Filter name="', prefix,'_variantdenovo__Variant__main__variant_id_key" value = "', SNPID,'" />'
							].join('');
						
						// Then load the SNP information
						$.ajax({
						    url: BIOMART_CONFIG.service.url + 'results',
						    data: {
						        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + variantdenovoDataset.name + '" config="' + variantdenovoDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
						        },
						        success: function(results) {

					        		data = ngspipelines.utils.reformatBioMartQueryResult(results)[0];
						        	data.isFavorite = isFavorite;
						        	data.effect=[]
						        	snp_effects = ngspipelines.variantdenovo.variantreport.getSNPEffects(SNPID, variantdenovoDataset, 
						        			function(data_effect) {
						        				data.effect=data_effect;
						        				successCB(data);
						        			}, 
						        			function(){
						        				successCB(data);
						        		    });
						        	
						        },
						        error: function() {
						        	errorCB("ngspipelines.variantdenovo.variantreport.getSNP: An error occurred while attempting to load SNP " + SNPID + "!");
						        }
						});
			        },
			        error: function() {
			        	errorCB("ngspipelines.variantdenovo.variantreport.getSNP: An error occurred while attempting to load variantdenovo favorite SNP!");
			        }
			});
		} else {
			errorCB("ngspipelines.variantdenovo.variantreport.getSNP: An error occurred while attempting to load " + analysisDataset.name + " as an analysis!");
		}				
	}

	/* 
	 * Returns all effect information
	 * @param variantdenovoID the variantdenovo ID to return
	 * @param variantdenovodenovoDataset the variantdenovodenovo dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the variantdenovo
	 */
	self.getSNPEffects = function(SNPID, variantdenovoDataset, successCB, errorCB) {

		var prefix = ngspipelines.utils.getDatasetPrefix(variantdenovoDataset),
			attributes = [
			    '<Attribute name="', prefix,'_variantdenovo__Variant__main__variant_id_key" />',
			    '<Attribute name="', prefix,'_variantdenovo__Effect__dm__aa_pos_len" />',
			    '<Attribute name="', prefix,'_variantdenovo__Effect__dm__allele" />',
			    '<Attribute name="', prefix,'_variantdenovo__Effect__dm__annotation" />',
			    '<Attribute name="', prefix,'_variantdenovo__Effect__dm__annotation_impact" />',
			    '<Attribute name="', prefix,'_variantdenovo__Effect__dm__cdna_pos_len" />',
			    '<Attribute name="', prefix,'_variantdenovo__Effect__dm__cds_pos_len" />',
			    '<Attribute name="', prefix,'_variantdenovo__Effect__dm__aa_pos_len" />',
			    '<Attribute name="', prefix,'_variantdenovo__Effect__dm__feature_id" />',
			    '<Attribute name="', prefix,'_variantdenovo__Effect__dm__feature_type" />',
			    '<Attribute name="', prefix,'_variantdenovo__Effect__dm__hgvs_c" />',
			    '<Attribute name="', prefix,'_variantdenovo__Effect__dm__hgvs_p" />',
			    '<Attribute name="', prefix,'_variantdenovo__Effect__dm__transcript_biotype" />',
			    '<Attribute name="', prefix,'_variantdenovo__Effect__dm__warning" />'			    
		    ].join(''),
		    filters = [
			    '<Filter name="', prefix,'_variantdenovo__Variant__main__variant_id_key" value = "', SNPID,'" />'
			].join('');
		
		// Then load the SNP information
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + variantdenovoDataset.name + '" config="' + variantdenovoDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	effects = ngspipelines.utils.reformatBioMartQueryResult(results);					        	
		        	successCB(effects);
		        },
		        error: function() {
		        	errorCB("ngspipelines.variantdenovo.variantreport.getSNPEffects: An error occurred while attempting to load SNP " + SNPID + "!");
		        }
		});
	}

	/* 
	 * Returns all variantdenovo's information
	 * @param variantdenovoID the variantdenovo ID to return
	 * @param variantdenovodenovoDataset the variantdenovodenovo dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the variantdenovo
	 */
	self.getSNPAnnotations = function(SNPID, variantdenovoDataset, successCB, errorCB) {

		var prefix = ngspipelines.utils.getDatasetPrefix(variantdenovoDataset),
			attributes = [
			    '<Attribute name="', prefix,'_variantdenovo__Variant__main__variant_id_key" />',
			    '<Attribute name="', prefix,'_variantdenovo__Annotation__dm__hit_accession" />',
			    '<Attribute name="', prefix,'_variantdenovo__Annotation__dm__gene" />',
			    '<Attribute name="', prefix,'_variantdenovo__Annotation__dm__species" />',
			    '<Attribute name="', prefix,'_variantdenovo__Annotation__dm__gene_description" />',
			    '<Attribute name="', prefix,'_variantdenovo__Annotation__dm__exon_five_prime_limit" />',
			    '<Attribute name="', prefix,'_variantdenovo__Annotation__dm__exon_three_prime_limit" />',
			    '<Attribute name="', prefix,'_variantdenovo__Annotation__dm__position_on_ref" />',
			    '<Attribute name="', prefix,'_variantdenovo__Annotation__dm__region" />',
			    '<Attribute name="', prefix,'_variantdenovo__Annotation__dm__identities" />',
			    '<Attribute name="', prefix,'_variantdenovo__Annotation__dm__positives" />',
			    '<Attribute name="', prefix,'_variantdenovo__Annotation__dm__align_length" />',
			    '<Attribute name="', prefix,'_variantdenovo__Annotation__dm__score" />',
			    '<Attribute name="', prefix,'_variantdenovo__Annotation__dm__evalue" />',
			    '<Attribute name="', prefix,'_variantdenovo__Annotation__dm__known_variants" />'
		    ].join(''),
		    filters = [
			    '<Filter name="', prefix,'_variantdenovo__Variant__main__variant_id_key" value = "', SNPID,'" />'
			].join('');
		
		// Then load the SNP information
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + variantdenovoDataset.name + '" config="' + variantdenovoDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	annotations = ngspipelines.utils.reformatBioMartQueryResult(results);					        	
		        	successCB(annotations);
		        },
		        error: function() {
		        	errorCB("ngspipelines.variantdenovo.variantreport.getSNPAnnotations: An error occurred while attempting to load SNP " + SNPID + "!");
		        }
		});
	}
	
	/* 
	 * Returns all allels information on SNPs
	 * @param variantdenovoID the variantdenovo ID to return
	 * @param variantdenovodenovoDataset the variantdenovodenovo dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the variantdenovo
	 */
	self.getAllelesCount = function(SNPID, variantdenovoDataset, successCB, errorCB) {

		var prefix = ngspipelines.utils.getDatasetPrefix(variantdenovoDataset),
			attributes = [
			    '<Attribute name="', prefix,'_variantdenovo__CountAlleleLib__dm__sample_name" />',
			    '<Attribute name="', prefix,'_variantdenovo__CountAlleleLib__dm__replicat" />',
			    '<Attribute name="', prefix,'_variantdenovo__CountAlleleLib__dm__allele" />',
			    '<Attribute name="', prefix,'_variantdenovo__CountAlleleLib__dm__nb_read" />',
			    '<Attribute name="', prefix,'_variantdenovo__CountAlleleLib__dm__frequency" />',
			    '<Attribute name="', prefix,'_variantdenovo__CountAlleleLib__dm__tissue" />',
			    '<Attribute name="', prefix,'_variantdenovo__CountAlleleLib__dm__dev_stage" />',
			    '<Attribute name="', prefix,'_variantdenovo__Variant__main__variant_id_key" />'
		    ].join(''),
		    filters = [
			    '<Filter name="', prefix,'_variantdenovo__Variant__main__variant_id_key" value = "', SNPID,'" />'
			].join('');
		
		// Then load the SNP information
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + variantdenovoDataset.name + '" config="' + variantdenovoDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	var allelesCount = {},
		        		alleles = new Array(),
		        		libraries = new Array(),
		        		data = {};
		        	
			    	for (var index in results.data) {
			    		if (index != "remove") {
			    			if (results.data[index]["Sample name"] != undefined) {
			    				if ($.inArray(results.data[index]["Allele"], alleles) == -1) { alleles.push(results.data[index]["Allele"]); }
			    				if ($.inArray(results.data[index]["Sample name"]+"."+results.data[index]["Replicat"], libraries) == -1) { libraries.push(results.data[index]["Sample name"]+"."+results.data[index]["Replicat"]); }
			    				if (!allelesCount.hasOwnProperty(results.data[index]["Sample name"]+"."+results.data[index]["Replicat"])) {
			    					allelesCount[results.data[index]["Sample name"]+"."+results.data[index]["Replicat"]] = {};
			    				}
		    					allelesCount[results.data[index]["Sample name"]+"."+results.data[index]["Replicat"]][results.data[index]["Allele"]] = parseInt(results.data[index]["Nb read"]);
			    			}
			    		}
			    	}
		        	data.allelesCount = allelesCount;
		        	data.alleles = alleles;
		        	data.libraries = libraries;
		        	successCB(data);
		        },
		        error: function() {
		        	errorCB("ngspipelines.variantdenovo.variantreport.getAllelesCount: An error occurred while attempting to load SNP " + SNPID + "!");
		        }
		});
	}
	
	/* 
	 * Returns the link to the SNP export
	 * @param SNPID the SNP ID to return
	 * @param variantdenovoDataset the variantdenovo dataset
	 * @param fnSuccess
	 * @param fnError
	 */
	self.getExportFlankingSequencesLink = function(SNPID, variantdenovoDataset,fnSuccess,fnError) {
		var prefix = ngspipelines.utils.getDatasetPrefix(variantdenovoDataset),
			attributes = [
			    '<Attribute name="', prefix,'_variantdenovo__Variant__main__variant_name" />',
			    '<Attribute name="', prefix,'_variantdenovo__Variant__main__previous_seq" />',
			    '<Attribute name="', prefix,'_variantdenovo__Variant__main__next_seq" />',
			    '<Attribute name="', prefix,'_variantdenovo__Variant__main__type" />',
			    '<Attribute name="', prefix,'_variantdenovo__Variant__main__position" />',
			    '<Attribute name="', prefix,'_variantdenovo__Variant__main__alleles" />',
			    '<Attribute name="', prefix,'_variantdenovo__Variant__main__variant_id_key" />'
		    ].join(''),
		    filters = [
			    '<Filter name="', prefix,'_variantdenovo__Variant__main__variant_id_key" value = "', SNPID,'" />'
			].join('');
		
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
	        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="ExportFlankingSequences" limit="-1" header="0"><Dataset name="' + variantdenovoDataset.name + '" config="' + variantdenovoDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
	        },
	        success: function(data) {
	        	fnSuccess(data);
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	        	fnError("ngspipelines.variantdenovo.variantreport.getExportFlankingSequencesLink : ajax error  while retrieving variant flanking sequence : " + errorThrown);
	        }
		});
	}
		
});
})(jQuery);
