(function($) {
$.namespace('ngspipelines.download', function(self) {	

	/* 
	 * List the raw directory files of the provided project
	 * @param projectDataset the project dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams Array the list of files in the project raw directory
	 */
	self.getListDirectory = function(projectDataset, successCB, errorCB) {
				
		if (ngspipelines.utils.isAProjectDataset(projectDataset)) {
			var prefix = ngspipelines.utils.getDatasetPrefix(projectDataset),
				attributes = [
		              	'<Attribute name="', prefix,'_project__Project__main__directory" />'
		  		    ].join('');
		  		    
		  		    
			$.ajax({
	  		    url: BIOMART_CONFIG.service.url + 'results',
	  		    data: {
	  		    		query: '<!DOCTYPE Query><Query client="webbrowser" processor="ListDirectory" limit="-1" header="0"><Dataset name="' + projectDataset.name + '" config="' + projectDataset.mart.name + '">' + attributes + '</Dataset></Query>'
	  		        },
	  		        success: function(results) {
	  		        	successCB(results);
	  		        },
	  		        error: function(jqXHR, textStatus, errorThrown) {
	  		        	errorCB("ngspipelines.download.getListDirectory: An error occured while gathering files list!");
	  		        }
	  		});
		} else {
			errorCB("ngspipelines.download.getListDirectory: An error occurred while attempting to load " + projectDataset.name + " as a project!");
		}
	}
	
});
})(jQuery);
