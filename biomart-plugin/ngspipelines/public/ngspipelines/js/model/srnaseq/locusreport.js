(function($) {
$.namespace('ngspipelines.srnaseq.locusreport', function(self) {	
	
	/* 
	 * Subfunction to getLocus
	 * @param locusID the locus ID to return
	 * @param srnaseqDataset the srnaseq dataset
	 * @param analysisDataset the analysis dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the contig
	 */
	self.__loadLocusInfo = function(locusID, srnaseqDataset, analysisDataset, successCB, errorCB, isFavorite) {		
		var prefix = ngspipelines.utils.getDatasetPrefix(srnaseqDataset),
			attributes = [
			              '<Attribute name="', prefix,'_srnaseq__Locus__main__locus_id_key" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__name" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__display_name" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__best_hit_accession" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__best_hit_database" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__best_hit_query_start" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__best_hit_query_stop" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__best_hit_subject_start" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__best_hit_subject_stop" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__best_hit_evalue" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__best_hit_score" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__best_hit_species" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__best_hit_mismatch" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__best_hit_family" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__best_hit_perc_identities" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__best_hit_description" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__chromosome" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__length" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__start" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__stop" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__strand" />',
						    '<Attribute name="', prefix,'_srnaseq__Structure__dm__structure_free_energy1" />',
						    '<Attribute name="', prefix,'_srnaseq__Structure__dm__structure_free_energy2" />',
						    '<Attribute name="', prefix,'_srnaseq__Structure__dm__structure_structure1" />',
						    '<Attribute name="', prefix,'_srnaseq__Structure__dm__structure_structure2" />',
						    '<Attribute name="', prefix,'_srnaseq__Isoform__dm__isoform_nb_reads" />',
						    '<Attribute name="', prefix,'_srnaseq__StructuralAnnotation__dm__structural_annotation_type" />',
						    '<Attribute name="', prefix,'_srnaseq__StructuralAnnotation__dm__structural_annotation_gene_id" />',
						    '<Attribute name="', prefix,'_srnaseq__StructuralAnnotation__dm__structural_annotation_distance" />',
						    '<Attribute name="', prefix,'_srnaseq__StructuralAnnotation__dm__structural_annotation_start" />',
						    '<Attribute name="', prefix,'_srnaseq__StructuralAnnotation__dm__structural_annotation_stop" />',
						    '<Attribute name="', prefix,'_srnaseq__StructuralAnnotation__dm__structural_annotation_strand" />',
						    '<Attribute name="', prefix,'_srnaseq__StructuralAnnotation__dm__structural_annotation_description" />',
						    '<Attribute name="', prefix,'_srnaseq__DNA__dm__dna_sequence" />',
						    '<Attribute name="', prefix,'_srnaseq__Prediction__dm__prediction_start" />',
						    '<Attribute name="', prefix,'_srnaseq__Prediction__dm__prediction_score" />',
						    '<Attribute name="', prefix,'_srnaseq__Prediction__dm__prediction_stop" />'
		    ].join(''),
		    filters = [
			    '<Filter name="', prefix,'_srnaseq__Locus__main__locus_id_key" value = "', locusID,'" />'
			].join('');
		
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + srnaseqDataset.name + '" config="' + srnaseqDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {

	        		var	data = ngspipelines.utils.reformatBioMartQueryResult(results)[0],
						prefix = ngspipelines.utils.getDatasetPrefix(srnaseqDataset),
						hrefs = {},
						attributes = [
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__best_hit_accession" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__best_hit_database" />',
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__locus_id_key" />'
					    ].join(''),
					    filters = [
						    '<Filter name="', prefix,'_srnaseq__Locus__main__locus_id_key" value = "', locusID,'" />'
						].join('');
					
					$.ajax({
					    url: BIOMART_CONFIG.service.url + 'results',
					    data: {
					        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="DatabaseURL" limit="-1" header="0"><Dataset name="' + srnaseqDataset.name + '" config="' + srnaseqDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
					        },
					        success: function(resultDB) {
					        	
						    	for (var index in resultDB.data) {
						    		if (index != "remove") {
						    			data.href = resultDB.data[index]["href"];
						    		}
						    	}
					        	successCB(data);										    	
					        },
					        error: function() {
					        	successCB(data);
					        }
					});
					data.isFavorite = isFavorite;
		        },
		        error: function() {
		        	errorCB("ngspipelines.srnaseq.locusreport.__loadLocusInfo: An error occurred while attempting to load locus " + locusID + "!");
		        }
		});
	}
	
	
	/* 
	 * Returns all contig's information
	 * @param contigID the contig ID to return
	 * @param rnaseqdenovoDataset the rnaseqdenovo dataset
	 * @param analysisDataset the analysis dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the contig
	 */
	self.getLocus = function(locusID, srnaseqDataset, analysisDataset, successCB, errorCB) {
		// First load from the analysis table the locus flagged as favorites
		if (ngspipelines.utils.isAnAnalysisDataset(analysisDataset)) {
			var prefix = ngspipelines.utils.getDatasetPrefix(analysisDataset),
				attributes = [
				    '<Attribute name="', prefix,'_analysis__Result__dm__value" />',
				    '<Attribute name="', prefix,'_analysis__Analysis__main__analysis_type" />'
			    ].join(''),
			    filters = [
				    '<Filter name="', prefix,'_analysis__Analysis__main__analysis_type" value = "favorite_locus" />'
				].join('');
			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
			        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
			        },
			        success: function(results) {			        	
			        	// For each favorites contig
			        	var isFavorite = false;
			        	for (var index in results.data) {
			        		if (index != "remove") {
			        			if (results.data[index]["Value"] == locusID) {
			        				isFavorite = true;
			        			}
			        		}
			        	}
			        	self.__loadLocusInfo(locusID, srnaseqDataset, analysisDataset, successCB, errorCB, isFavorite);
			        },
			        error: function() {
			        	self.__loadLocusInfo(locusID, srnaseqDataset, analysisDataset, successCB, errorCB, false);
			        }
			});
		} else {
			errorCB("ngspipelines.srnaseq.locusreport.getLocus: An error occurred while attempting to load " + analysisDataset.name + " as an analysis!");
		}	
	}
	

	/* 
	 * Returns all information needed for the depth view
	 * @param locusID the locus ID to return
	 * @param srnaseqDataset the srnaseq dataset
	 * @param analysisDataset the analysis dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the locus
	 */
	self.getDepthInfo = function(locusID, srnaseqDataset, successCB, errorCB) {
		var prefix = ngspipelines.utils.getDatasetPrefix(srnaseqDataset),
			attributes = [
			    '<Attribute name="', prefix,'_srnaseq__Locus__main__locus_id_key" />',
			    '<Attribute name="', prefix,'_srnaseq__Locus__main__name" />',
			    '<Attribute name="', prefix,'_srnaseq__Locus__main__chromosome" />',
			    '<Attribute name="', prefix,'_srnaseq__Locus__main__length" />',
			    '<Attribute name="', prefix,'_srnaseq__Locus__main__start" />',
			    '<Attribute name="', prefix,'_srnaseq__Locus__main__stop" />',
			    '<Attribute name="', prefix,'_srnaseq__Locus__main__strand" />',
			    '<Attribute name="', prefix,'_srnaseq__Structure__dm__structure_free_energy1" />',
			    '<Attribute name="', prefix,'_srnaseq__Structure__dm__structure_structure1" />',
                '<Attribute name="', prefix,'_srnaseq__Structure__dm__structure_free_energy2" />',
                '<Attribute name="', prefix,'_srnaseq__Structure__dm__structure_structure2" />',
			    '<Attribute name="', prefix,'_srnaseq__Isoform__dm__isoform_name" />',
			    '<Attribute name="', prefix,'_srnaseq__Isoform__dm__isoform_nb_reads" />',
			    '<Attribute name="', prefix,'_srnaseq__Isoform__dm__isoform_start" />',
			    '<Attribute name="', prefix,'_srnaseq__Isoform__dm__isoform_stop" />',
			    '<Attribute name="', prefix,'_srnaseq__Isoform__dm__isoform_NM" />',
			    '<Attribute name="', prefix,'_srnaseq__Isoform__dm__isoform_sequence" />',
			    '<Attribute name="', prefix,'_srnaseq__Isoform__dm__isoform_annotations" />',
			    '<Attribute name="', prefix,'_srnaseq__DNA__dm__dna_sequence" />',
			    '<Attribute name="', prefix,'_srnaseq__Prediction__dm__prediction_start" />',
			    '<Attribute name="', prefix,'_srnaseq__Prediction__dm__prediction_stop" />'
		    ].join(''),
		    filters = [
			    '<Filter name="', prefix,'_srnaseq__Locus__main__locus_id_key" value = "', locusID,'" />'
			].join('');

        	$.ajax({
    		    url: BIOMART_CONFIG.service.url + 'results',
    		    data: {
    		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + srnaseqDataset.name + '" config="' + srnaseqDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
    		        },
    		        success: function(results) {
    		        	var final_result = ngspipelines.utils.reformatBioMartQueryResult(results);
    		        	successCB(final_result)
    		        },
    		        error: function() {
    		        	errorCB("ngspipelines.srnaseq.locusreport.getDepthInfo: An error occurred while attempting to load locus " + locusID + "!");
    		        }
        	});
	}

	/* 
	 * Returns Id of the locus from its name
	 * @param locusName the locus Name to return
	 * @param srnaseqDataset the srnaseq dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams
	 */
	self.getIdFromLocusName = function(locusName, srnaseqDataset, successCB, errorCB) {
		var prefix = ngspipelines.utils.getDatasetPrefix(srnaseqDataset),
		attributes = [
		    '<Attribute name="', prefix,'_srnaseq__Locus__main__locus_id_key" />',
		    '<Attribute name="', prefix,'_srnaseq__Locus__main__name" />'
	    ].join(''),
	    filters = [
		    '<Filter name="', prefix,'_srnaseq__Locus__main__name" value = "', locusName,'" />'
		].join('');
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + srnaseqDataset.name + '" config="' + srnaseqDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	successCB(results.data[0]["Locus id key"]);
		        },
		        error: function() {
		        	errorCB("ngspipelines.srnaseq.locusreport.getIdFromLocusName: An error occurred while attempting to get locus " + locusName + "!");
		        }
		});
	}
	
	
	/* 
	 * Returns the link to the locus in fasta format
	 * @param locusID the locus ID to return
	 * @param srnaseqDataset the srnaseq dataset
	 * @param fnSuccess
	 * @param fnError
	 */
self.getExportLocusSequenceLink = function(locusID, srnaseqDataset, fnSuccess, fnError) {
		
		var prefix = ngspipelines.utils.getDatasetPrefix(srnaseqDataset),
			attributes = [
			    '<Attribute name="', prefix,'_srnaseq__Locus__main__name" />',
			    '<Attribute name="', prefix,'_srnaseq__DNA__dm__dna_sequence" />',
			    '<Attribute name="', prefix,'_srnaseq__Locus__main__locus_id_key" />'
		    ].join(''),
		    filters = [
			    '<Filter name="', prefix,'_srnaseq__Locus__main__locus_id_key" value = "', locusID,'" />'
			].join('');
		
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
	        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="BasicFasta" limit="-1" header="0"><Dataset name="' + srnaseqDataset.name + '" config="' + srnaseqDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
	        },
	        success: function(data) {
	        	fnSuccess(data);
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	        	fnError("ngspipelines.srnaseq.locusreport.getExportContigSequenceLink : ajax error  while retrieving fasta file export link : " + errorThrown);
	        }
		});
	}
	
	/* 
	 * Returns Chromosome of the locus from its Id
	 * @param locusId the locus Id to return
	 * @param srnaseqDataset the srnaseq dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams the name
	 */
	self.getChrFromLocusId = function(locusId, srnaseqDataset, successCB, errorCB) {
		var prefix = ngspipelines.utils.getDatasetPrefix(srnaseqDataset),
		attributes = [
		    '<Attribute name="', prefix,'_srnaseq__Locus__main__locus_id_key" />',
		    '<Attribute name="', prefix,'_srnaseq__Locus__main__chromosome" />'
	    ].join(''),
	    filters = [
		    '<Filter name="', prefix,'_srnaseq__Locus__main__locus_id_key" value = "', locusId,'" />'
		].join('');
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + srnaseqDataset.name + '" config="' + srnaseqDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	successCB(results.data[0]["chromosome"]);
		        },
		        error: function() {
		        	errorCB("ngspipelines.srnaseq.locusreport.getNameFromLocusId: An error occurred while attempting to get locus " + locusId + "!");
		        }
		});
	}
	
	/* 
	 * Returns the link to the locus annotations in gff
	 * @param locusID the locus ID to return
	 * @param srnaseqDataset the srnaseq dataset
	 * @param fnSuccess
	 * @param fnErrors
	 */
	self.getExportLocusAnnotationsLink = function(locusID, chromosome, analysisDataset, fnSuccess, fnError) {
		/*var prefix = ngspipelines.utils.getDatasetPrefix(srnaseqDataset),
			attributes = [
						    '<Attribute name="', prefix,'_srnaseq__Locus__main__chromosome" />',
			    '<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_database" />',
			    '<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_type" />',
			    '<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_query_start" />',
			    '<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_query_stop" />',
			    '<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_score" />',
			    '<Attribute name="', prefix,'_srnaseq__Locus__main__strand" />',
			    '<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_accession" />',
			    '<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_description" />',
			    '<Attribute name="', prefix,'_srnaseq__Locus__main__locus_id_key" />'
		    ].join(''),
		    filters = [
					    '<Filter name="', prefix,'_srnaseq__Locus__main__locus_id_key" value = "', locusID,'" />'
			].join('');
		
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
	        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="BasicGFF" limit="-1" header="0"><Dataset name="' + srnaseqDataset.name + '" config="' + srnaseqDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
	        },
	        success: function(data) {
	        	fnSuccess(data);
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	        	fnError("ngspipelines.srnaseq.locusreport.getExportLocusAnnotationsLink : ajax error  while retrieving annotations file : " + errorThrown);
	        }
		});
		*/
		
		var prefix = ngspipelines.utils.getDatasetPrefix(analysisDataset) ;
		var dbFields = {
						'annot' : {
							     'feature_table' : prefix + '_srnaseq__Annotation__dm',
		                         'name_field'    : 'annotation_hit_accession',
		                         'source_field'  : 'annotation_hit_database',
		                         'type_field'    : 'annotation_hit_type',
		                         'start_field'   : 'annotation_hit_query_start',
		                         'end_field'     : 'annotation_hit_query_stop',
		                         'score_field'   : 'annotation_hit_score',
		                         'description_field' : 'annotation_hit_description',
		                         'filter_field'  : 'locus_id_key',
		                         'filter_value'  : locusID,
		                         'region_value'  : chromosome
						}
		};
		attributes = "<Attribute name='" + prefix + "_analysis__Analysis__main__analysis_type' />" ;
		filters = "<Filter name='" + prefix + "_analysis__Analysis__main__analysis_type' value = '" + encodeURI( JSON.stringify({'dbFields' : dbFields}) )+ "' />" ;	    		

		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
	        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="BasicGFF" limit="-1" header="0"><Dataset name="' + analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
	        },
	        success: function(data) {
	        	fnSuccess(data);
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	        	fnError("ngspipelines.rnaseqdenovo.contigsreport.getExportContigAnnotationsLink : ajax error  while retrieving annotations file : " + errorThrown);
	        }
		});
		
	}
	
	/* 
	 * Returns all locus annotations
	 * @param locusID the locus ID to return
	 * @param srnaseqDataset the srnaseq dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the locus
	 */
	self.getLocusAnnotations = function(locusID, srnaseqDataset, successCB, errorCB) {
		var prefix = ngspipelines.utils.getDatasetPrefix(srnaseqDataset),
		attributes = [
		    '<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_accession" />',
		    '<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_database" />',
		    '<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_score" />',
		    '<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_evalue" />',
		    //'<Attribute name="', prefix,'_srnaseq__Annotation__dm__hit_species" />',
		    //'<Attribute name="', prefix,'_srnaseq__Annotation__dm__hit_perc_Qcoverage" />',
		    //'<Attribute name="', prefix,'_srnaseq__Annotation__dm__hit_perc_Scoverage" />',
		    //'<Attribute name="', prefix,'_srnaseq__Annotation__dm__hit_perc_Qidentities" />',
		    '<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_query_start" />',
		    '<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_query_stop" />',
		    //'<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_query_strand" />',
		    '<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_description" />',
		    '<Attribute name="', prefix,'_srnaseq__Locus__main__locus_id_key" />'
	    ].join(''),
	    filters = [
		    '<Filter name="', prefix,'_srnaseq__Locus__main__locus_id_key" value = "', locusID,'" />'
		].join('');
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + srnaseqDataset.name + '" config="' + srnaseqDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(annots) {
		        	
					var prefix = ngspipelines.utils.getDatasetPrefix(srnaseqDataset),
						hrefs = {},
					attributes = [
					    '<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_accession" />',
					    '<Attribute name="', prefix,'_srnaseq__Annotation__dm__annotation_hit_database" />',
					    '<Attribute name="', prefix,'_srnaseq__Locus__main__locus_id_key" />'
				    ].join(''),
				    filters = [
					    '<Filter name="', prefix,'_srnaseq__Locus__main__locus_id_key" value = "', locusID,'" />'
					].join('');

					$.ajax({
					    url: BIOMART_CONFIG.service.url + 'results',
					    data: {
					        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="DatabaseURL" limit="-1" header="0"><Dataset name="' + srnaseqDataset.name + '" config="' + srnaseqDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
					        },
					        success: function(results) {
						    	for (var index in results.data) {
						    		if (index != "remove") {
						    			hrefs[results.data[index]["accession"]] = results.data[index]["href"];
						    		}
						    	}
						    
						    	successCB(ngspipelines.utils.reformatBioMartQueryResult(annots), hrefs);
					        },
					        error: function() {
					        	errorCB("ngspipelines.srnaseq.locusreport.getLocus: An error occurred while attempting to load locus " + locusID + "!");
					        }
					});
		        },
		        error: function() {
		        	errorCB("ngspipelines.locusreport.getLocus: An error occurred while attempting to load srnaseq favorite locus!");
		        }
		});
	}

	
	/* 
	 * Returns all locus informations, relative to libraries
	 * @param locusID the locus ID to return
	 * @param srnaseqDataset the srnaseq dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the locus
	 */
	self.getLocusLibraries = function(locusID, srnaseqDataset, successCB, errorCB) {
		var prefix = ngspipelines.utils.getDatasetPrefix(srnaseqDataset),
		attributes = [
		    '<Attribute name="', prefix,'_srnaseq__ReadLibraryCount__dm__readlibrarycount_library_id" />',
		    '<Attribute name="', prefix,'_srnaseq__ReadLibraryCount__dm__readlibrarycount_sample_name" />',
		    '<Attribute name="', prefix,'_srnaseq__ReadLibraryCount__dm__readlibrarycount_nb_read" />',
		    '<Attribute name="', prefix,'_srnaseq__ReadLibraryCount__dm__readlibrarycount_coverage" />',
		    '<Attribute name="', prefix,'_srnaseq__ReadLibraryCount__dm__readlibrarycount_mean_depth" />',
		    '<Attribute name="', prefix,'_srnaseq__ReadLibraryCount__dm__readlibrarycount_tissue" />',
		    '<Attribute name="', prefix,'_srnaseq__ReadLibraryCount__dm__readlibrarycount_dev_stage" />',
		    '<Attribute name="', prefix,'_srnaseq__ReadLibraryCount__dm__readlibrarycount_replicat" />',
		    '<Attribute name="', prefix,'_srnaseq__Locus__main__locus_id_key" />'
	    ].join(''),
	    filters = [
	               '<Filter name="', prefix,'_srnaseq__Locus__main__locus_id_key" value = "', locusID,'" />'
		].join('');
								
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + srnaseqDataset.name + '" config="' + srnaseqDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	var libraryIDs = new Array(),
		        		libraryNames = new Array(),
		        		libraries = {};
		        	
			    	for (var index in results.data) {
			    		if (index != "remove") {
			    			if (!(libraries.hasOwnProperty(results.data[index]["Readlibrarycount sample name"]))) {
			    				libraries[results.data[index]["Readlibrarycount sample name"]] = new Array();
			    			}
			    		}
			    	}
			    	
			    	// Filter values from the read library count table
			    	for (var index in results.data) {
			    		if (index != "remove") {
			    			if ($.inArray(results.data[index]["Readlibrarycount library id"], libraryIDs) == -1) {
			    				libraries[results.data[index]["Readlibrarycount sample name"]].push({
			    					sample_name: results.data[index]["Readlibrarycount sample name"],
			    					nb_read: results.data[index]["Readlibrarycount nb read"],
			    					coverage: results.data[index]["Readlibrarycount coverage"],
			    					mean_depth: results.data[index]["Readlibrarycount mean depth"],
			    					replicat: results.data[index]["Readlibrarycount replicat"],
			    					tissue: results.data[index]["Readlibrarycount tissue"],
			    					dev_stage: results.data[index]["Readlibrarycount dev stage"],
			    					//window_size: results.data[index]["Window size"]
			    				});
			    				libraryIDs.push(results.data[index]["Readlibrarycount library id"]);
			    				if ($.inArray(results.data[index]["Readlibrarycount sample name"], libraryNames) == -1) {
			    					libraryNames.push(results.data[index]["Readlibrarycount sample name"]);
			    				}
			    			}
			    		}
			    	}
		        	
		        	successCB(libraries, libraryNames);
		        },
		        error: function() {
		        	errorCB("ngspipelines.srnaseq.locusreport.getLocusLibraries: An error occurred while attempting to load contig " + locusID + "!");
		        }
		});
	}
	
	self.getLocusIsoforms = function(locusID, srnaseqDataset, successCB, errorCB) {
		var prefix = ngspipelines.utils.getDatasetPrefix(srnaseqDataset),
		attributes = [
		    '<Attribute name="', prefix,'_srnaseq__Isoform__dm__isoform_name" />',
		    '<Attribute name="', prefix,'_srnaseq__Isoform__dm__isoform_annotations" />',
		    '<Attribute name="', prefix,'_srnaseq__Isoform__dm__isoform_sequence" />',
		    '<Attribute name="', prefix,'_srnaseq__Locus__main__locus_id_key" />'
	    ].join(''),
	    filters = [
		    '<Filter name="', prefix,'_srnaseq__Locus__main__locus_id_key" value = "', locusID,'" />'
		].join('');
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + srnaseqDataset.name + '" config="' + srnaseqDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	var final_result = ngspipelines.utils.reformatBioMartQueryResult(results);
		        	successCB(final_result)
		        },
		        error: function() {
		        	errorCB("ngspipelines.locusreport.getLocus: An error occurred while attempting to load srnaseq favorite locus!");
		        }
		});
	}
	
	
});
})(jQuery);