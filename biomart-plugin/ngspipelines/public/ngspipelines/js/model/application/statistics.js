(function($) {
$.namespace('ngspipelines.application', function(self) {	
	/* 
	 * 
	 * @param analysisDataset the analysis dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the stats object
	 */
	self.getDisplayGraphAnalyses = function(analysisDataset, categorie, successCB, errorCB) {
		
		var prefix = ngspipelines.utils.getDatasetPrefix(analysisDataset),
			attributes = [
			    '<Attribute name="', prefix,'_analysis__Analysis__main__analysis_name" />',
			    '<Attribute name="', prefix,'_analysis__Analysis__main__analysis_id_key" />',
			    '<Attribute name="', prefix,'_analysis__Analysis__main__comments" />',
			    '<Attribute name="', prefix,'_analysis__Result__dm__key" />',
			    '<Attribute name="', prefix,'_analysis__Result__dm__group" />',
			    '<Attribute name="', prefix,'_analysis__Result__dm__value" />',
			].join(''),
		    filters = [
			    '<Filter name="', prefix,'_analysis__Analysis__main__analysis_type" value = "display_graph" />',
			    '<Filter name="', prefix,'_analysis__Analysis__main__categorie" value = "', categorie,'" />'
			].join('');
						
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	var analyses = {},
		        		ids = new Array();	   
		        	
			    	for (var index in results.data) {
			    		if (index != "remove") {
			    			if (ids.indexOf(results.data[index]["Analysis id key"]) != -1 ) {
			    				if( results.data[index]["Group"] != "default" ) {
				    				var group = results.data[index]["Group"] ;
				    				if( !analyses[results.data[index]["Analysis id key"]].hasOwnProperty(group) ) {
				    					analyses[results.data[index]["Analysis id key"]][group] = {};
				    				}
				    				analyses[results.data[index]["Analysis id key"]][group][results.data[index]["Key"]] = results.data[index]["Value"];
			    				} else {
			    					analyses[results.data[index]["Analysis id key"]][results.data[index]["Key"]] = results.data[index]["Value"];
			    				}
			    			} else {
			    				analyses[results.data[index]["Analysis id key"]] = {};
			    				analyses[results.data[index]["Analysis id key"]]["analysis_name"] = results.data[index]["Analysis name"];
			    				analyses[results.data[index]["Analysis id key"]]["analysis_id_key"] = results.data[index]["Analysis id key"];
			    				analyses[results.data[index]["Analysis id key"]]["comments"] = results.data[index]["Comments"];
			    				if( results.data[index]["Group"] != "default" ) {
				    				var group = results.data[index]["Group"] ;
				    				analyses[results.data[index]["Analysis id key"]][group] = {};
				    				analyses[results.data[index]["Analysis id key"]][group][results.data[index]["Key"]] = results.data[index]["Value"];	
			    				} else {
			    					analyses[results.data[index]["Analysis id key"]][results.data[index]["Key"]] = results.data[index]["Value"];
			    				}
			    				ids.push(results.data[index]["Analysis id key"]);
			    			}
			    		}
			    	}
		        	successCB(analyses, ids[0]);
		        },
		        error: function() {
		        	errorCB("ngspipelines.application.getDisplayGraphAnalyses: An error occurred while attempting to load analysis dataset!");
		        }
		});

	}
});
})(jQuery);	
