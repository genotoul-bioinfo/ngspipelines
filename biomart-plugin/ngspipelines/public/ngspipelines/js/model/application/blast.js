(function($) {
	$.namespace('ngspipelines.application.blast', function(self) {	

		/* 
		 * Run blast on the contig database from user sequence
		 */
		self.runBlast = function(params, analysisDataset, successCB, errorCB) {
			var prefix = ngspipelines.utils.getDatasetPrefix(analysisDataset),
				attributes = '<Attribute name="' + prefix + '_analysis__Analysis__main__analysis_type" />',
			    filters = "<Filter name='" + prefix + "_analysis__Analysis__main__analysis_type' value = '" + JSON.stringify(params) + "' />";

			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
			        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="Blast" limit="-1" header="0"><Dataset name="' + analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
			        },
			        success: function(results) {
        	
			        	if ( results.error ) {
			        		errorCB("An error occurred during blast : " + results.error);
			        	}
			        	else {
			        		successCB(results);
			        	}
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			        	errorCB("An error occurred during blast : " + errorThrown.error);
			        }
			});
		}
	});
})(jQuery);
