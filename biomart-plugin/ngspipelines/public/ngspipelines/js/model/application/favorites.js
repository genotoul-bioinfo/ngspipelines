(function($) {
$.namespace('ngspipelines.application', function(self) {	

	/* 
	 * favorites loading function
	 * @param dataset the dataset
	 * @param analysisDataset the analysis dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the object
	 */
	self.getFavorites = function(dataset, analysisDataset, aprefix, analysisType, mainTableName, fields, successCB, errorCB) {
		// First load from the analysis table the favorites
		if (ngspipelines.utils.isAnAnalysisDataset(analysisDataset)) {
			var prefix = ngspipelines.utils.getDatasetPrefix(analysisDataset),
				attributes = [
				    '<Attribute name="', prefix,'_analysis__Result__dm__value" />'
			    ].join(''),
			    filters = [
				    '<Filter name="', prefix,'_analysis__Analysis__main__analysis_type" value = "', analysisType,'" />'
				].join('');
			
			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
			        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
			        },
			        success: function(results) {
			        				        	
			        	// For each favorites
			        	var favorite_ids = ""
			        	for (var index in results.data) {
			        		if (index != "remove") {
			        			favorite_ids += results.data[index]["Value"] + ',';
			        		}
			        	}
			    		var prefix = ngspipelines.utils.getDatasetPrefix(dataset),
						    attributes = '',
						    filters = [
							    '<Filter name="', prefix,'_', aprefix, '__', mainTableName, '__main__', fields["key"], '" value = "' + favorite_ids + '" />'
							].join('');
			    		
			    		for (field in fields) {
			    			if (field == "key") {
			    				attributes += '<Attribute name="'+prefix+'_'+aprefix+'__'+mainTableName+'__main__'+fields[field]+'" />';
			    			} else {
			    				attributes += '<Attribute name="'+prefix+'_'+aprefix+'__'+mainTableName+'__main__'+field+'" />';
			    			}
			    			
			    		}
			    		
						$.ajax({
						    url: BIOMART_CONFIG.service.url + 'results',
						    data: {
						        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + dataset.name + '" config="' + dataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
						        },
						        success: function(results) {
						        	successCB(ngspipelines.utils.reformatBioMartQueryResult(results));
						        },
						        error: function() {
						        	errorCB("ngspipelines.application.getFavorites: An error occurred while attempting to load favorites!");
						        }
						});
			        },
			        error: function() {
			        	errorCB("ngspipelines.application.getFavorites: An error occurred while attempting to load favorites!");
			        }
			});
		} else {
			errorCB("ngspipelines.application.getFavorites: An error occurred while attempting to load " + analysisDataset.name + " as an analysis!");
		}
	}
	
	

	
	
});
})(jQuery);
