(function($) {
	$.namespace('ngspipelines.application', function(self) {	

		self.getMainTableName = function(dataset, successCB, errorCB) {
			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'attributes.json',
			    data: {
		    		datasets: dataset.name,
		    		config: dataset.mart.name
			    },
		        success: function(attributes) {
		        	var mainName = "";
		        	for (var index in attributes) {
		        		if (ngspipelines.utils.endsWith(attributes[index].parent, "__main_attribute")) {
		        			mainName = attributes[index].parent.split("__")[1];
		        			break;
		        		}
		        	}
		        	successCB(mainName);
		        },
		        error: function() {
		        	errorCB("ngspipelines.application.getMainTableName: An error occurred while attempting to retrieve " + dataset.name + " main table name!");
		        }
			});
		}
		
		
		self.getFavoritesConfig = function(dataset, favorite_analysis, successCB, errorCB) {
			var prefix = ngspipelines.utils.getDatasetPrefix(dataset),
				attributes = [
				    '<Attribute name="', prefix, '_analysis__Result__dm__key" />',
				    '<Attribute name="', prefix, '_analysis__Result__dm__value" />'
				].join(''),
			    filters = "<Filter name='" + prefix + "_analysis__Analysis__main__analysis_type' value = '" + favorite_analysis + "' />";
			
			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
			        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + dataset.name + '" config="' + dataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
			        },
			        success: function(results) {
			        	var table_headers = null,
			        		table_keys = null,
			        		final_table = {},
			        		links = "";
			        	for (var index in results.data) {
			        		if (results.data[index].Key == "table_headers") {
			        			table_headers = results.data[index].Value.split(",");
			        		} else if (results.data[index].Key == "table_keys") {
			        			table_keys = results.data[index].Value.split(",");
			        		} else if (results.data[index].Key == "links") {
			        			links = results.data[index].Value;
			        		}
			        	}
			        	for (var index in table_keys) {
			        		if (index != "remove") {
			        			final_table[table_keys[index]] = table_headers[index];
			        		}
			        	}
			        	successCB(final_table, links);
			        },
			        error: function() {
			        	errorCB("ngspipelines.application.getFavoritesConfig: An error occurred while attempting to retrieve favorites configuration!");
			        }
			});
		}
		

		
	});
})(jQuery);