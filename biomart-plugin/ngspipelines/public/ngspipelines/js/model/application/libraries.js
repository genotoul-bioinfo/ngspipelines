(function($) {
$.namespace('ngspipelines.application.libraries', function(self) {	
	
	/* 
	 * Library dataset loading function
	 * @param libraryDataset the library dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the library object
	 */
	self.getLibraries = function(libraryDataset, datasetType, successCB, errorCB) {
		
		if (ngspipelines.utils.isALibrariesDataset(libraryDataset)) {			
			var prefix = ngspipelines.utils.getDatasetPrefix(libraryDataset),
				attributes = [
				    '<Attribute name="', prefix,'_library__Library__main__library_name" />',
				    '<Attribute name="', prefix,'_library__Library__main__sample_name" />',
				    '<Attribute name="', prefix,'_library__Library__main__replicat" />',
				    '<Attribute name="', prefix,'_library__Library__main__tissue" />',
				    '<Attribute name="', prefix,'_library__Library__main__dev_stage" />',
				    '<Attribute name="', prefix,'_library__Library__main__remark" />',
				    '<Attribute name="', prefix,'_library__Library__main__sequencer" />',
				    '<Attribute name="', prefix,'_library__Library__main__type" />',
				    '<Attribute name="', prefix,'_library__Library__main__insert_size" />',
				    '<Attribute name="', prefix,'_library__Library__main__nb_sequence" />',
				    '<Attribute name="', prefix,'_library__Library__main__categorie" />',
				    '<Attribute name="', prefix,'_library__Library__main__files" />'
				].join(''),
			    filters = [
				    '<Filter name="', prefix,'_library__Library__main__categorie" value = "', datasetType, '" />'
				].join('');
	
			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
			        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + libraryDataset.name + '" config="' + libraryDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
			        },
			        success: function(results) {
			        	successCB(ngspipelines.utils.reformatBioMartQueryResult(results));
			        },
			        error: function() {
			        	errorCB("ngspipelines.application.getLibraries: An error occurred while attempting to load libraries libraries!");
			        }
			});
		} else {
			errorCB("ngspipelines.application.getLibraries: An error occurred while attempting to load " + libraryDataset.name + " as a library!");
		}
	}
	
	/* 
	 * Library dataset loading function
	 * @param analysisDataset the library dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the analysis object
	 */
	self.getAnalysesByType = function(analysisDataset, successCB, errorCB) {
		if (ngspipelines.utils.isAnAnalysisDataset(analysisDataset)) {
			var prefix = ngspipelines.utils.getDatasetPrefix(analysisDataset),
				attributes = [
				    '<Attribute name="', prefix,'_analysis__Analysis__main__analysis_name" />',
				    '<Attribute name="', prefix,'_analysis__Analysis__main__analysis_id_key" />',
				    '<Attribute name="', prefix,'_analysis__Analysis__main__categorie" />',
				    '<Attribute name="', prefix,'_analysis__Analysis__main__analysis_type" />',
				    '<Attribute name="', prefix,'_analysis__Analysis__main__comments" />',
				    '<Attribute name="', prefix,'_analysis__Analysis__main__directory" />',
				    '<Attribute name="', prefix,'_analysis__Analysis__main__soft_name" />',
				    '<Attribute name="', prefix,'_analysis__Analysis__main__soft_parameters" />',
				    '<Attribute name="', prefix,'_analysis__Analysis__main__soft_version" />'
			    ].join('');
			
			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
			        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + attributes + '</Dataset></Query>'
			        },
			        success: function(results) {
			        	var analyses = {};		        	
			        	for (var index in results.data) {
			        		if (index != "remove") {
			        			
			        			if (analyses.hasOwnProperty(results.data[index]["Analysis type"])) {
			        				analyses[results.data[index]["Analysis type"]].push({
			        					analysis_name: results.data[index]["Analysis name"],
			        					categorie: results.data[index]["Categorie"],
			        					analysis_id_key: results.data[index]["Analysis id key"],
			        					comments: results.data[index]["Comments"],
			        					directory: results.data[index]["Directory"],
			        					soft_name: results.data[index]["Soft name"],
			        					soft_parameters: results.data[index]["Soft parameters"],
			        					soft_version: results.data[index]["Soft version"]
				        			});
			        			} else {
			        				analyses[results.data[index]["Analysis type"]] = new Array({
			        					analysis_name: results.data[index]["Analysis name"],
			        					categorie: results.data[index]["Categorie"],
			        					analysis_id_key: results.data[index]["Analysis id key"],
			        					comments: results.data[index]["Comments"],
			        					directory: results.data[index]["Directory"],
			        					soft_name: results.data[index]["Soft name"],
			        					soft_parameters: results.data[index]["Soft parameters"],
			        					soft_version: results.data[index]["Soft version"]
				        			});
			        			}
			        		}
			        	}
			        	successCB(analyses);
			        },
			        error: function() {
			        	errorCB("ngspipelines.application.getAnalyses: An error occurred while attempting to load libraries analysis!");
			        }
			});
		} else {
			errorCB("ngspipelines.application.getAnalyses: An error occurred while attempting to load " + analysisDataset.name + " as an analysis!");
		}
	}
	
});
})(jQuery);
	