(function($) {
	$.namespace('ngspipelines.application.dea', function(self) {	
		
		/**
		 * Process an action using the DDD processor
		 * @param action : one of getResults, DEAnalysis, deleteResults
		 * @param proc_args : object with given keys
		 * 			- pool1 : an array with libraries from pool1
		 * 			- pool2 : an array with libraries from pool2
		 * 			- alpha : an alpha value to be used for the test
		 * 			- category : category
		 * 			- dea_id_key : optional dea analysis id key from url
		 * @param analysisDataset :
		 * @param success : success callback
		 * @param error : error callback
		 */
		self.processAction = function (action, proc_args, analysisDataset, success, error){
			var project_name = ngspipelines.utils.getDatasetPrefix(analysisDataset);
			proc_args.action = action;
			var attributes = '<Attribute name="' + project_name + '_analysis__Analysis__main__analysis_type" />';
			var filters =  "<Filter name='" + project_name + "_analysis__Analysis__main__analysis_type' value='" + JSON.stringify(proc_args) + "' />" ;
			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="DEAnalysis" limit="-1" header="0"><Dataset name="' + 
		        		analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(data) {
		        	if (data.error){
		        		error("ngspipelines.application.dea.processAction : </br>error : "+ data.error );
		        	}
		        	else{
		        		success(data);
		        	}
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		        	error("ngspipelines.application.dea.processAction : ajax error : "+ errorThrown );
		        }
			});
		};
		
		
		/**
		 * Return an array with all usable alpha values
		 */
		self.getAlphaValues = function(){
			return [ 0.05, 0.02, 0.01, 0.001, 0.0001 ];
		};
		self.getNormValues = function(){
			return [ "RLE","TMM","upperquartile" ];
		};
		
		
		self.getAnalysisResults = function(analysisDataset, analyseID, successCB, errorCB) {
			
			var prefix = ngspipelines.utils.getDatasetPrefix(analysisDataset),
				attributes = [
				    '<Attribute name="', prefix,'_analysis__Result__dm__key" />',
				    '<Attribute name="', prefix,'_analysis__Result__dm__group" />',
				    '<Attribute name="', prefix,'_analysis__Result__dm__type" />',
				    '<Attribute name="', prefix,'_analysis__Result__dm__value" />',
				].join(''),
			    filters = [
				    
				    '<Filter name="', prefix,'_analysis__Result__dm__analysis_id_key" value = "', analyseID,'" />'
				].join('');
							
			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
			        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
			        },
			        success: function(results) {
			        	var	analysisData = {},
			        		analysisKey = new Array();
			        	
			        	for (var index in results.data) {
			        		if (results.data[index]["Group"] != undefined) {
				        		var currentData = results.data[index]["Group"];
				        		if ($.inArray(currentData, analysisKey) == -1 ) {
			        				analysisData[currentData]= {};
			        				analysisKey.push(currentData);
			        			}
				        		analysisData[currentData][results.data[index]["Key"]] = results.data[index]["Value"];
			        		}
			        	}
			        	successCB(analysisData);
			        },
			        error: function() {
			        	errorCB("ngspipelines.project.getAnalyseResults: An error occurred while attempting to load analysis dataset!");
			        }
			});

		}

		
		
		self.getComputedResults = function (pool1, pool2, analysisDataset, success, error){
			var filter_param = "pool1:"+pool1.join(',')+";"+"pool2:"+pool2.join(',');
			var project_name = ngspipelines.utils.getDatasetPrefix(analysisDataset);
			var attributes = '<Attribute name="' + project_name + '_analysis__Analysis__main__analysis_name" />'+
			 				 '<Attribute name="' + project_name + '_analysis__Analysis__main__soft_parameters" />' +
			 				 '<Attribute name="' + project_name + '_analysis__Analysis__main__analysis_id_key" />'+
			 				 '<Attribute name="' + project_name + '_analysis__Result__dm__key" />'+
			 				 '<Attribute name="' + project_name + '_analysis__Result__dm__group" />'+
			 				 '<Attribute name="' + project_name + '_analysis__Result__dm__type" />'+
			 				 '<Attribute name="' + project_name + '_analysis__Result__dm__value" />';
			var filters =  	"<Filter name='" + project_name + "_analysis__Analysis__main__analysis_name' value='DEAnalysis' />";
			
			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + 
		        		analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + filters + attributes + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	var json = new Array();
			    	for (var index in results.data) {
			    		if ((index != "remove") && (index != undefined) ){ 
			    			if (results.data[index]["Soft parameters"].substring(0, filter_param.length) === filter_param) {
				    			//check if parameter startswith pools options
				    			var queryResult = {};
				    			for (var key in results.data[index]) {
				    				var modified_key = key.charAt(0).toLowerCase() + key.slice(1);
				    				modified_key = modified_key.replace(/ /g, "_");
				    				if (key  == "Soft parameters") {
				    					var dict_parameter={};
				    					var list_parameter=results.data[index][key].split(";");
				    					for (i_str in list_parameter) {
				    						if ((typeof i_str == "string") && (typeof list_parameter[i_str] == "string")){ 
				    							var param_array = list_parameter[i_str].split(":");
				    							dict_parameter[param_array[0]]=param_array[1];
				    						}
				    					}
		    							queryResult[modified_key] = dict_parameter;
				    				}
				    				else if (key == "Value"){
				    					queryResult["overexp_both_pools"] = results.data[index][key];
				    				}
				    				else if (!( key in ["Key","Group","Type"])) {
				    					queryResult[modified_key] = results.data[index][key];
				    				}
				    			}
				    			json.push(queryResult);
				    		}
			    		}
			    	}
		        	success(json);
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		        	success([]);
		        }
			});
		}; //end function getComputedResults
		
	}); //end namespace
})(jQuery);
