(function($) {
$.namespace('ngspipelines.application.venn', function(self) {	
	
	/* 
	 * Load the countFile and format the data to the venny format
	 * @param pools the sample pools
	 * @param countFile the WEB path to the countFile
	 * @param callback the callback function to call when loading is done
	 * @callbackParams json the data table
	 */
	self.loadAndFormatData = function(pools, countFile, callback) {
		$.get(countFile, function(data) {
			var lines = data.split("\n"),
				mainTable = {},
				mainLink = new Array(),
				mainIDs = new Array();
			for (var i=0; i<lines.length; i++) {
				if (lines[i] != "") {
					var parts = lines[i].split("\t");
					if (ngspipelines.utils.startsWith(parts[0], "#")) {
						for (var j=1; j<parts.length; j++) {
							mainTable[parts[j]] = new Array();
							mainLink.push(parts[j]);
						}
					} else if (!ngspipelines.utils.startsWith(lines[i], "#")) {
						for (var j=0; j<mainLink.length; j++) {
							if (parseInt(parts[j+1]) > 0) {
								mainTable[mainLink[j]].push(parts[0]);
								mainIDs[parts[0]] = parts[0];
							}
						}
					}
				}
			}
			var seriesTable = new Array();
			for (var key in pools) {
				if (pools.hasOwnProperty(key) && pools[key].length > 0) {
					var dataTable = new Array();
			        for (var i = 0; i<pools[key].length; i++) {
			        	$.merge(dataTable, mainTable[pools[key][i]]);
			        }
			        seriesTable.push({
			        	name: key,
			        	data: dataTable
			        });
			    }
			}
			callback(seriesTable, mainIDs);
		});
	}
	
});
})(jQuery);
