(function($) {
$.namespace('ngspipelines.transcript.contigsreport', function(self) {	
	
	
	/* 
	 * Subfunction to getContig
	 * @param contigID the contig ID to return
	 * @param transcriptDataset the transcript dataset
	 * @param analysisDataset the analysis dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the contig
	 */
	self.__loadContigInfo = function(contigID, transcriptDataset, analysisDataset, successCB, errorCB, isFavorite) {		
		var prefix = ngspipelines.utils.getDatasetPrefix(transcriptDataset),
			attributes = [
			    '<Attribute name="', prefix,'_transcript__Contig__main__contig_id_key" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__name" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__length" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__depth" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_database" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_species" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_gene" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_accession" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_query_start" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_query_stop" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_subject_start" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_subject_stop" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_evalue" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_score" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_perc_ident" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_perc_Scoverage" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_perc_Qconserved" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_description" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_perc_Qcoverage" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_perc_Qidentities" />'
		    ].join(''),
		    filters = [
			    '<Filter name="', prefix,'_transcript__Contig__main__contig_id_key" value = "', contigID,'" />'
			].join('');
		
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + transcriptDataset.name + '" config="' + transcriptDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	var data = ngspipelines.utils.reformatBioMartQueryResult(results, [])[0],
						prefix = ngspipelines.utils.getDatasetPrefix(transcriptDataset),
						hrefs = {},
						attributes = [
						    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_accession" />',
						    '<Attribute name="', prefix,'_transcript__Contig__main__best_hit_database" />',
						    '<Attribute name="', prefix,'_transcript__Contig__main__contig_id_key" />'
					    ].join(''),
					    filters = [
						    '<Filter name="', prefix,'_transcript__Contig__main__contig_id_key" value = "', contigID,'" />'
						].join('');
					$.ajax({
					    url: BIOMART_CONFIG.service.url + 'results',
					    data: {
					        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="DatabaseURL" limit="-1" header="0"><Dataset name="' + transcriptDataset.name + '" config="' + transcriptDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
					        },
					        success: function(resultDB) {
					        	
						    	for (var index in resultDB.data) {
						    		if (index != "remove") {
						    			data.href = resultDB.data[index]["href"];
						    		}
						    	}
					        	successCB(data);										    	
					        },
					        error: function() {
					        	successCB(data);
					        }
					});
		        },
		        error: function() {
		        	errorCB("ngspipelines.transcript.contigsreport.getContig: An error occurred while attempting to load contig " + contigID + "!");
		        }
		});
	}
	
	/* 
	 * Returns all contig's information
	 * @param contigID the contig ID to return
	 * @param transcriptDataset the transcript dataset
	 * @param analysisDataset the analysis dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the contig
	 */
	self.getContig = function(contigID, transcriptDataset, analysisDataset, successCB, errorCB) {
		// First load from the analysis table the contigs flagged as favorites
		if (ngspipelines.utils.isAnAnalysisDataset(analysisDataset)) {
			var prefix = ngspipelines.utils.getDatasetPrefix(analysisDataset),
				attributes = [
				    '<Attribute name="', prefix,'_analysis__Result__dm__value" />',
				    '<Attribute name="', prefix,'_analysis__Analysis__main__analysis_type" />'
			    ].join(''),
			    filters = [
				    '<Filter name="', prefix,'_analysis__Analysis__main__analysis_type" value = "favorite_contig" />'
				].join('');
			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
			        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
			        },
			        success: function(results) {			        	
			        	// For each favorites contig
			        	var isFavorite = false;
			        	for (var index in results.data) {
			        		if (index != "remove") {
			        			if (results.data[index]["Value"] == contigID) {
			        				isFavorite = true;
			        			}
			        		}
			        	}
			        	self.__loadContigInfo(contigID, transcriptDataset, analysisDataset, successCB, errorCB, isFavorite);
			        },
			        error: function() {
			        	self.__loadContigInfo(contigID, transcriptDataset, analysisDataset, successCB, errorCB, false);
			        }
			});
		} else {
			errorCB("ngspipelines.transcript.contigsreport.getContig: An error occurred while attempting to load " + analysisDataset.name + " as an analysis!");
		}	
	}
	
	/* 
	 * Returns all contig's annotations
	 * @param contigID the contig ID to return
	 * @param transcriptDataset the transcript dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the contig
	 */
	self.getContigAnnotations = function(contigID, transcriptDataset, successCB, errorCB) {
		var prefix = ngspipelines.utils.getDatasetPrefix(transcriptDataset),
		attributes = [
		    '<Attribute name="', prefix,'_transcript__Annotation__dm__hit_accession" />',
		    '<Attribute name="', prefix,'_transcript__Annotation__dm__hit_database" />',
		    '<Attribute name="', prefix,'_transcript__Annotation__dm__hit_score" />',
		    '<Attribute name="', prefix,'_transcript__Annotation__dm__hit_evalue" />',
		    '<Attribute name="', prefix,'_transcript__Annotation__dm__hit_species" />',
		    '<Attribute name="', prefix,'_transcript__Annotation__dm__hit_perc_Qcoverage" />',
		    '<Attribute name="', prefix,'_transcript__Annotation__dm__hit_perc_Scoverage" />',
		    '<Attribute name="', prefix,'_transcript__Annotation__dm__hit_perc_Qidentities" />',
		    '<Attribute name="', prefix,'_transcript__Annotation__dm__hit_query_start" />',
		    '<Attribute name="', prefix,'_transcript__Annotation__dm__hit_query_stop" />',
		    '<Attribute name="', prefix,'_transcript__Annotation__dm__hit_query_strand" />',
		    '<Attribute name="', prefix,'_transcript__Annotation__dm__hit_description" />',
		    '<Attribute name="', prefix,'_transcript__Contig__main__contig_id_key" />'
	    ].join(''),
	    filters = [
		    '<Filter name="', prefix,'_transcript__Contig__main__contig_id_key" value = "', contigID,'" />'
		].join('');
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + transcriptDataset.name + '" config="' + transcriptDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(annots) {
		        	
					var prefix = ngspipelines.utils.getDatasetPrefix(transcriptDataset),
						hrefs = {},
					attributes = [
					    '<Attribute name="', prefix,'_transcript__Annotation__dm__hit_accession" />',
					    '<Attribute name="', prefix,'_transcript__Annotation__dm__hit_database" />',
					    '<Attribute name="', prefix,'_transcript__Contig__main__contig_id_key" />'
				    ].join(''),
				    filters = [
					    '<Filter name="', prefix,'_transcript__Contig__main__contig_id_key" value = "', contigID,'" />'
					].join('');

					$.ajax({
					    url: BIOMART_CONFIG.service.url + 'results',
					    data: {
					        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="DatabaseURL" limit="-1" header="0"><Dataset name="' + transcriptDataset.name + '" config="' + transcriptDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
					        },
					        success: function(results) {
					        	
						    	for (var index in results.data) {
						    		if (index != "remove") {
						    			hrefs[results.data[index]["accession"]] = results.data[index]["href"];
						    		}
						    	}
						    	successCB(ngspipelines.utils.reformatBioMartQueryResult(annots), hrefs);
					        },
					        error: function() {
					        	errorCB("ngspipelines.transcript.contigsreport.getContig: An error occurred while attempting to load contig " + contigID + "!");
					        }
					});
		        },
		        error: function() {
		        	errorCB("ngspipelines.contigsreport.getContig: An error occurred while attempting to load transcript favorite contigs!");
		        }
		});
	}

	
	/* 
	 * Returns all contig's information
	 * @param contigID the contig ID to return
	 * @param transcriptDataset the transcript dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the contig
	 */
	self.getContigLibraries = function(contigID, transcriptDataset, successCB, errorCB) {
		var prefix = ngspipelines.utils.getDatasetPrefix(transcriptDataset),
		attributes = [
		    '<Attribute name="', prefix,'_transcript__Expression__dm__library_id" />',
		    '<Attribute name="', prefix,'_transcript__Expression__dm__sample_name" />',
		    '<Attribute name="', prefix,'_transcript__Expression__dm__nb_read" />',
		    '<Attribute name="', prefix,'_transcript__Expression__dm__window_size" />',
		    '<Attribute name="', prefix,'_transcript__Expression__dm__coverage" />',
		    '<Attribute name="', prefix,'_transcript__Expression__dm__mean_depth" />',
		    '<Attribute name="', prefix,'_transcript__Expression__dm__tissue" />',
		    '<Attribute name="', prefix,'_transcript__Expression__dm__dev_stage" />',
		    '<Attribute name="', prefix,'_transcript__Expression__dm__replicat" />',
		    '<Attribute name="', prefix,'_transcript__Contig__main__contig_id_key" />'
	    ].join(''),
	    filters = [
		    '<Filter name="', prefix,'_transcript__Contig__main__contig_id_key" value = "', contigID,'" />'
		].join('');
								
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + transcriptDataset.name + '" config="' + transcriptDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	
		        	var libraryIDs = new Array(),
		        		libraryNames = new Array(),
		        		libraries = {};
		        	
			    	for (var index in results.data) {
			    		if (index != "remove") {
			    			var current_lib_name = results.data[index]["Sample name"] +
				               (results.data[index]["Tissue"] != "" ? '.' + results.data[index]["Tissue"] : "") + 
				               (results.data[index]["Dev stage"] != "" ? '.' + results.data[index]["Dev stage"] : "") ; // Gather replicates
			    			    if (!(libraries.hasOwnProperty(current_lib_name))) {
			    			    	libraries[current_lib_name] = new Array();
			    			    }
			    		}
			    	}
			    	
			    	// Filter values from the read library count table
			    	for (var index in results.data) {
			    		if (index != "remove") {
			    			if ($.inArray(results.data[index]["Library id"], libraryIDs) == -1) {
			    				var current_lib_name = results.data[index]["Sample name"] +
			    				               (results.data[index]["Tissue"] != "" ? '.' + results.data[index]["Tissue"] : "") + 
			    				               (results.data[index]["Dev stage"] != "" ? '.' + results.data[index]["Dev stage"] : "") ; // Gather replicates
			    				libraries[current_lib_name].push({
			    					sample_name: results.data[index]["Sample name"],
			    					nb_read: results.data[index]["Nb read"],
			    					coverage: results.data[index]["Coverage"],
			    					mean_depth: results.data[index]["Mean depth"],
			    					replicat: results.data[index]["Replicat"],
			    					tissue: results.data[index]["Tissue"],
			    					dev_stage: results.data[index]["Dev stage"],
			    					window_size: results.data[index]["Window size"],
			    				    library_id: results.data[index]["Library id"]
			    				});
			    				libraryIDs.push(results.data[index]["Library id"]);
			    				if ($.inArray(current_lib_name, libraryNames) == -1) {
			    					libraryNames.push(current_lib_name);
			    				}
			    			}
			    		}
			    	}
		        	
		        	successCB(libraries, libraryNames);
		        	
		        },
		        error: function() {
		        	errorCB("ngspipelines.transcript.contigsreport.getContig: An error occurred while attempting to load contig " + contigID + "!");
		        }
		});
	}

	/* 
	 * Returns sequence of the contig
	 * @param contigID the contig ID to return
	 * @param transcriptDataset the transcript dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json of the fasta sequence
	 */
	self.getContigSequence = function(contigID, transcriptDataset, successCB, errorCB) {
		var prefix = ngspipelines.utils.getDatasetPrefix(transcriptDataset),
		attributes = [
		    '<Attribute name="', prefix,'_transcript__DNA__dm__sequences" />',
		    '<Attribute name="', prefix,'_transcript__Contig__main__name" />',
		    '<Attribute name="', prefix,'_transcript__Contig__main__length" />',
		    '<Attribute name="', prefix,'_transcript__Contig__main__contig_id_key" />'
	    ].join(''),
	    filters = [
		    '<Filter name="', prefix,'_transcript__Contig__main__contig_id_key" value = "', contigID,'" />'
		].join('');
								
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + transcriptDataset.name + '" config="' + transcriptDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	var data = results.data[0],
		        		seq  = ngspipelines.utils.getSequenceString(data.Sequences);
	        		
		        	data.Sequences = seq;
		        	successCB(data);
		        },
		        error: function() {
		        	errorCB("ngspipelines.transcript.contigsreport.getContigSequence: An error occurred while attempting to get sequence of contig " + contigID + "!");
		        }
		});
	}
	
	/* 
	 * Returns clusters of the contig
	 * @param contigID the contig ID to return
	 * @param transcriptDataset the transcript dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json of the fasta sequence
	 */
	self.getContigClusters = function(contigID, transcriptDataset, successCB, errorCB) {
		var prefix = ngspipelines.utils.getDatasetPrefix(transcriptDataset),
		attributes = [
		    '<Attribute name="', prefix,'_transcript__Clusters__dm__centroid_name" />',
		    '<Attribute name="', prefix,'_transcript__Clusters__dm__contig_name" />',
		    '<Attribute name="', prefix,'_transcript__Clusters__dm__library_name" />',
		    '<Attribute name="', prefix,'_transcript__Clusters__dm__length" />',
		    '<Attribute name="', prefix,'_transcript__Clusters__dm__similarity" />',
		    '<Attribute name="', prefix,'_transcript__Contig__main__contig_id_key" />'
	    ].join(''),
	    filters = [
		    '<Filter name="', prefix,'_transcript__Contig__main__contig_id_key" value = "', contigID,'" />'
		].join('');
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + transcriptDataset.name + '" config="' + transcriptDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	successCB(ngspipelines.utils.reformatBioMartQueryResult(results));
		        },
		        error: function() {
		        	errorCB("ngspipelines.transcript.contigsreport.getContigClusters: An error occurred while attempting to get clusters of contig " + contigID + "!");
		        }
		});
	}
	/* 
	 * Returns Id of the contig from its name
	 * @param contigName the contig Name to return
	 * @param transcriptDataset the transcript dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json of the fasta sequence
	 */
	self.getIdFromContigName = function(contigName, transcriptDataset, successCB, errorCB) {
		var prefix = ngspipelines.utils.getDatasetPrefix(transcriptDataset),
		attributes = [
		    '<Attribute name="', prefix,'_transcript__Contig__main__contig_id_key" />',
		    '<Attribute name="', prefix,'_transcript__Contig__main__name" />'
	    ].join(''),
	    filters = [
		    '<Filter name="', prefix,'_transcript__Contig__main__name" value = "', contigName,'" />'
		].join('');
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + transcriptDataset.name + '" config="' + transcriptDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	for (var index in results.data) {
		        		if (results.data[index]["Name"] == contigName ) {
		        			successCB(results.data[index]["Contig id key"]);
		        		}
		        	}
		        },
		        error: function() {
		        	errorCB("ngspipelines.transcript.contigsreport.getIdFromContigName: An error occurred while attempting to get contig " + contigName + "!");
		        }
		});
	}
	
	/* 
	 * Returns Name of the contig from its Id
	 * @param contigId the contig Id to return
	 * @param transcriptDataset the transcript dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams the name
	 */
	self.getNameFromContigId = function(contigId, transcriptDataset, successCB, errorCB) {
		var prefix = ngspipelines.utils.getDatasetPrefix(transcriptDataset),
		attributes = [
		    '<Attribute name="', prefix,'_transcript__Contig__main__contig_id_key" />',
		    '<Attribute name="', prefix,'_transcript__Contig__main__name" />'
	    ].join(''),
	    filters = [
		    '<Filter name="', prefix,'_transcript__Contig__main__contig_id_key" value = "', contigId,'" />'
		].join('');
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + transcriptDataset.name + '" config="' + transcriptDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	successCB(results.data[0]["Name"]);
		        },
		        error: function() {
		        	errorCB("ngspipelines.transcript.contigsreport.getNameFromContigId: An error occurred while attempting to get contig " + contigId + "!");
		        }
		});
	}
	
	/* 
	 * Returns the link to the contig in fasta format
	 * @param contigID the contig ID to return
	 * @param transcriptDataset the transcript dataset
	 * @param fnSuccess
	 * @param fnError
	 */
	self.getExportContigSequenceLink = function(contigID, transcriptDataset, fnSuccess, fnError) {
		
		var prefix = ngspipelines.utils.getDatasetPrefix(transcriptDataset),
			attributes = [
			    '<Attribute name="', prefix,'_transcript__Contig__main__name" />',
			    '<Attribute name="', prefix,'_transcript__DNA__dm__sequences" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__contig_id_key" />'
		    ].join(''),
		    filters = [
			    '<Filter name="', prefix,'_transcript__Contig__main__contig_id_key" value = "', contigID,'" />'
			].join('');
		
		
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
	        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="BasicFasta" limit="-1" header="0"><Dataset name="' + transcriptDataset.name + '" config="' + transcriptDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
	        },
	        success: function(data) {
	        	fnSuccess(data);
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	        	fnError("ngspipelines.transcript.contigsreport.getExportContigSequenceLink : ajax error  while retrieving fasta file export link : " + errorThrown);
	        }
		});
	}	
	
	/* 
	 * Returns the link to the contig annotations and predictions in gff
	 * @param contigID the contig ID to return
	 * @param transcriptDataset the transcript dataset
	 * @param fnSuccess
	 * @param fnErrors
	 */
	self.getExportContigFeaturesLink = function(contigID, contigName, analysisDataset, fnSuccess, fnError) {
		var prefix = ngspipelines.utils.getDatasetPrefix(analysisDataset) ;
		var dbFields = {
						'annot' : {
							     'feature_table' : prefix + '_transcript__Annotation__dm',
							     'region_field'  : 'name',
		                         'name_field'    : 'hit_accession',
		                         'source_field'  : 'hit_database',
		                         'type_field'    : 'hit_type',
		                         'start_field'   : 'hit_query_start',
		                         'end_field'     : 'hit_query_stop',
		                         'score_field'   : 'hit_score',
		                         'strand_field'  : 'hit_query_strand',
		                         'description_field' : 'hit_description',
		                         'filter_field'  : 'contig_id_key',
		                         'filter_value'  : contigID
						},
						'predict' : {
						         'feature_table' : prefix + '_transcript__Prediction__dm',
						         'name_field'    : 'name',
						         'source_field'  : 'source',
						         'type_field'    : 'type',
						         'start_field'   : 'start',
						         'end_field'     : 'stop',
						         'score_field'   : 'score',
						         'strand_field'  : 'strand',
						         'description_field' : 'description',
						         'filter_field'  : 'contig_id_key',
						         'filter_value'  : contigID,
						         'region_value'  : contigName
					    }
		};
		attributes = "<Attribute name='" + prefix + "_analysis__Analysis__main__analysis_type' />" ;
		filters = "<Filter name='" + prefix + "_analysis__Analysis__main__analysis_type' value = '" + encodeURI( JSON.stringify({'dbFields' : dbFields}) )+ "' />" ;	    		

		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
	        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="BasicGFF" limit="-1" header="0"><Dataset name="' + analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
	        },
	        success: function(data) {
	        	fnSuccess(data);
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	        	fnError("ngspipelines.transcript.contigsreport.getExportContigAnnotationsLink : ajax error  while retrieving annotations file : " + errorThrown);
	        }
		});
	}
	
	/* 
	 * Returns the link to the contig SNPs / Indels
	 * @param contigID the contig ID to return
	 * @param transcriptDataset the transcript dataset
	 * @param fnSuccess
	 * @param fnErrors
	 */
	self.getExportContigSNPIndelsLink = function(contigID, transcriptDataset,  fnSuccess, fnError) {
		
		var prefix = ngspipelines.utils.getDatasetPrefix(transcriptDataset),
			attributes = [
			    '<Attribute name="', prefix,'_transcript__Variant__dm__variant_name" />',
			    '<Attribute name="', prefix,'_transcript__DNA__dm__sequences" />',
			    '<Attribute name="', prefix,'_transcript__Variant__dm__type" />',
			    '<Attribute name="', prefix,'_transcript__Variant__dm__position" />',
			    '<Attribute name="', prefix,'_transcript__Variant__dm__alleles" />',
			    '<Attribute name="', prefix,'_transcript__Contig__main__contig_id_key" />'
		    ].join(''),
		    filters = [
			    '<Filter name="', prefix,'_transcript__Contig__main__contig_id_key" value = "', contigID,'" />'
			].join('');
		
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
	        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="BasicSNPIndels" limit="-1" header="0"><Dataset name="' + transcriptDataset.name + '" config="' + transcriptDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
	        },
	        success: function(data) {
	        	fnSuccess(data);
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	        	fnError("ngspipelines.transcript.contigsreport.getExportContigSNPIndelsLink : ajax error  while retrieving variation (snp/indels) file : " + errorThrown);
	        }
		});
	}

	/* 
	 * Prepares one reference sequence for jBrowse (prepare-refseqs.pl)
	 * @param seqName the sequence name
	 * @param seqString the sequence string
	 * @param jBrowseFolder the jbrowse folder path (folder above the sequence folder)
	 * @param transcriptDataset the transcript dataset
	 * @param analysisDataset the analysis dataset
	 * @param fnSuccess the function executed if createReference is succeed
	 * @param fnError the function executed if createReference is failed
	 */
	self.createReference = function( seqName, seqString, jBrowseFolder, transcriptDataset, analysisDataset, fnSuccess, fnError ) {
		var prefix = ngspipelines.utils.getDatasetPrefix(transcriptDataset) ;
		var attributes = "<Attribute name='" + prefix + "_analysis__Analysis__main__analysis_type' />" ;
	    var filters = "<Filter name='" + prefix + "_analysis__Analysis__main__analysis_type' value='" + encodeURI( JSON.stringify({'seqName': seqName, 'seqString' : seqString, 'folderPath' : jBrowseFolder + "/" + seqName }) )+ "' />";
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		    	query: "<!DOCTYPE Query><Query client='webbrowser' processor='JBrowseProcessRef' limit='-1' header='0'><Dataset name='" + analysisDataset.name + "' config='" + analysisDataset.mart.name + "'>" + attributes + filters + "</Dataset></Query>"
	        },
	        success: function(data) {
	        	fnSuccess() ;
	        }, error: function( jqXHR, textStatus, errorThrown ) {
	        	fnError( errorThrown );
    		}
	    });
	}
	
	/* 
	 * Returns an URL to connect with the JBrowse processor
	 * @param referenceID the contig ID to return
	 * @param transcriptDataset the transcript dataset
	 * @param analysisDataset the analysis dataset
	 */
	self.getJBrowseURL = function( referenceID, transcriptDataset, analysisDataset, fnSuccess, fnError ) {
		var webPath       = "/ngspipelines/data/ae0e74ca6b/jbrowse/" ;
		var jBrowseFolder = "plugins/ngspipelines/public" + webPath ;
		var seqName = null ;
		var seqString = null ;
		var errorHTML = '<!DOCTYPE html>\n<html>\n<body>\n<div >An error has occured while attempting to initialize jbrowse.</div></body>\n</html>\n' ;
		
		var jBrowse = function( preProcess ) {
			preProcess = (preProcess == null) ? "true" : preProcess ;
			var prefix = ngspipelines.utils.getDatasetPrefix(transcriptDataset) ;
			var featuresFiles = new Array() ;
   			var dbFields = {
   							'annot' : {
   								     'feature_table' : prefix + '_transcript__Annotation__dm',
   								     'region_field'  : 'name',
   			                         'name_field'    : 'hit_accession',
   			                         'source_field'  : 'hit_database',
   			                         'type_field'    : 'hit_type',
   			                         'start_field'   : 'hit_query_start',
   			                         'end_field'     : 'hit_query_stop',
   			                         'score_field'   : 'hit_score',
   			                         'strand_field'  : 'hit_query_strand',
  			                         'description_field' : 'hit_description',
   			                         'filter_field'  : 'contig_id_key',
   			                         'filter_value'  : referenceID
  							},
   							'predict' : {
   							         'feature_table' : prefix + '_transcript__Prediction__dm',
   							         'name_field'    : 'name',
  							         'source_field'  : 'source',
   							         'type_field'    : 'type',
   							         'start_field'   : 'start',
   							         'end_field'     : 'stop',
   							         'score_field'   : 'score',
   							         'strand_field'  : 'strand',
   							         'description_field' : 'description',
   							         'filter_field'  : 'contig_id_key',
   							         'filter_value'  : referenceID,
   							         'region_value'   : seqName
   						    },
  							'variant' : {
   						             'feature_table' : prefix + '_variantdenovo__Variant__main',
   						             'region_field'  : 'contig_name',
   						             'name_field'    : 'variant_name',
   						             'type_field'    : 'type',
   						             'start_field'   : 'position',
   						             'end_field'     : 'position',
   						             'description_field' : 'alleles',
   						             'filter_field'  : 'contig_name',
   						             'filter_value'  : seqName,
   							         'strand_value'  : '+'
   					        }
   			};		
			attributes = "<Attribute name='" + prefix + "_analysis__Analysis__main__analysis_type' />" ;
			filters = "<Filter name='" + prefix + "_analysis__Analysis__main__analysis_type' value = '" + encodeURI( JSON.stringify({'folderPath' : jBrowseFolder + '/' + seqName, 'webPath' : webPath + '/' + seqName, 'dbFields' : dbFields, 'featuresFiles' : featuresFiles, 'preProcess' : preProcess}) )+ "' />" ;	    		

			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
			    	query: "<!DOCTYPE Query><Query client='webbrowser' processor='JBrowse' limit='-1' header='0'><Dataset name='" + analysisDataset.name + "' config='" + analysisDataset.mart.name + "'>" + attributes + filters + "</Dataset></Query>"
		        },
		        success: function(data) {
		        	fnSuccess( data );
		        },
		        error: function(data) {
		        	fnError( errorHTML );
		        }
		    });
		}
		// Process ContigSequenceLink and launch createReference
		var getSeqSuccess = function( filePath ) {
			$.get( "/ngspipelines/data/" + filePath.fasta_file_path, function( fileContent ) {
				var lines = fileContent.split("\n") ;
				seqName = lines[0].substring(1)  ;
				seqString = "" ;
				for( var idx = 1 ; idx < lines.length ; idx++ ) {
					seqString += lines[idx] ;
				}
				
				// Sequence pre-process
				$.ajax({
				    type: 'HEAD',
				    url: webPath + '/' + seqName + "/trackList.json",
				    success: function() { // The sequence has already been pre-processed
				    	jBrowse( "false" );
				    },  
				    error: function() { // Pre-process
				    	self.createReference( seqName, seqString, jBrowseFolder, transcriptDataset, analysisDataset, jBrowse, fnError );
				    }
				});
			})
		};
		self.getExportContigSequenceLink( referenceID, transcriptDataset, getSeqSuccess, fnError );
	}
});
})(jQuery);
