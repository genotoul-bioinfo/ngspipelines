
function FilterAndCollapseFastqs () {

	var	modified_values,
		categories;
	
	$("[id^='globalclean-link-']").click(function(){
		var display_id = $(this).attr("id").split("-")[3];
		
		$("[id^='globalclean-link-']").removeClass("li-active");
		$(this).addClass("li-active");
		
		$("div[id^=globalclean-graphics-wrapper-]").css("display", "none");
		$("#globalclean-graphics-wrapper-"+display_id).css("display", "inline");
		
		modified_values = new Array();
		categories = new Array();

		if(display_id == 2) {
			modified_values.push( {
				'name' : "Reads",
				'data' : [parseInt($("[name^='gcw"+display_id+"-reads']").val())]
			});
			modified_values.push( {
				'name' : "Sequences merged by sample",
				'data' : [parseInt($("[name^='gcw"+display_id+"-seq']").val())]
			});
			modified_values.push( {
				'name' : "Sequences merged",
				'data' : [parseInt($("[name^='gcw"+display_id+"-merged-seq']").val())]
			});

			$('#globalclean-graphics-graph-2').highcharts({
			    chart: {
		            type: 'column'
		        },
				credits: { enabled: false },
		        title: { text: null },
		        xAxis: {
		        	labels:{enabled:false},
		        	tickLength: 0
		        },
		        yAxis: {
		            min: 0,
		            title: {
		                text: 'Sequence number'
		            }
		        },
		        tooltip: {
		        	enabled: false
		        },
		        plotOptions: {
		            column: {
		                pointPadding: 0.2,
		                borderWidth: 0
		            },
		            series: {
		            	borderWidth: 0,
		            	dataLabels: {
		            		enabled: true,
		            		style: {
			                    fontWeight: 'bold',
			                    color: 'gray'
			                }
		            	}
		            }
		        },
		        series: modified_values
		    });
		}
		
		if(display_id == 3) {
			modified_values.push( {
				'name' : "Lost",
				'data' : [parseInt($("[name^='gcw"+display_id+"-lostr']").val()),
				          parseInt($("[name^='gcw"+display_id+"-losts']").val())]
			});
			modified_values.push( {
				'name' : "Kept",
				'data' : [parseInt($("[name^='gcw"+display_id+"-filteredr']").val()),
				          parseInt($("[name^='gcw"+display_id+"-filtereds']").val())]
			});
			$('#globalclean-graphics-graph-3').highcharts({
			    chart: {
		            type: 'column'
		        },
				credits: { enabled: false },
		        title: { text: null },
		        xAxis: {
		            categories: ["Reads", "Sequences"],
		        },
		        yAxis: {
		            min: 0,
		            title: {
		                text: 'Sequence number'
		            },
		            stackLabels: {
		                enabled: true,
		                style: {
		                    fontWeight: 'bold',
		                    color: 'gray'
		                }
		            }
		        },
		        tooltip: {
		            headerFormat: '<span style="font-size:11px"><b>{point.key}</b></span><table>',
		            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		                '<td style="padding:0"><b>{point.y}</b></td></tr>',
		            footerFormat: '</table>',
		            shared: true,
		            useHTML: true
		        },
		        plotOptions: {
	                column: {
	                    stacking: 'normal'
	                }
	            },
		        series: modified_values
		    });
		}
		
		if(display_id == 4) {
			var	tmp = $("[name^='gcw"+display_id+"-length']").val().split(',');
				data = new Array();
			for (i = 0; i < tmp.length; i++) {
				categories.push(parseInt(tmp[i]));
			}
			tmp = $("[name^='gcw"+display_id+"-reads']").val().split(',');
			for (i = 0; i < tmp.length; i++) {
				data.push(parseInt(tmp[i]));
			}
			modified_values.push( {
				'name' : "Reads",
				'data' : data
			});
			tmp = $("[name^='gcw"+display_id+"-seq']").val().split(',');
			data = new Array();
			for (i = 0; i < tmp.length; i++) {
				data.push(parseInt(tmp[i]));
			}
			modified_values.push( {
				'name' : "Sequences",
				'data' : data
			});
			
			$('#globalclean-graphics-graph-4').highcharts({
				chart: {
					type: "line",
					width: 670,
					spacingLeft: 0,
					zoomType: 'x',
					spacingRight: 20
				},
			    title: { text: null },
				credits: { enabled: false },
			    xAxis: {
					categories: categories,
					title: { text: 'Length (bp)' },
			    },
			    yAxis: {
			        title: { text: 'Read/sequence number' },
			        min: 0,
			        plotLines: [{
			            value: 0,
			            width: 1,
			            color: '#808080'
			        }]
			    },
			    tooltip: {
			        headerFormat: '<span style="font-size:11px"><b>Length {point.key}bp</b></span><table>',
		            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		                '<td style="padding:0"><b>{point.y}</b></td></tr>',
		            footerFormat: '</table>',
		            shared: true,
		            useHTML: true
			    },
			    legend: {
			        layout: 'vertical',
			        align: 'right',
			        verticalAlign: 'middle',
			        borderWidth: 0
			    },
			    series: modified_values
			});
		}
	});
}