
function BAMToLocus () {

	var	modified_values,
		categories;
	
	$("[id^='bamtolocus-link-']").click(function(){
		var display_id = $(this).attr("id").split("-")[3];
		
		$("[id^='bamtolocus-link-']").removeClass("li-active");
		$(this).addClass("li-active");
		
		$("div[id^=bamtolocus-graphics-wrapper-]").css("display", "none");
		$("#bamtolocus-graphics-wrapper-"+display_id).css("display", "inline");
		
		var	minlen	 = $("[name^='btlw2-min_size_locus']").val(),
			miniso	 = $("[name^='btlw2-min_nb_isoforms']").val();
		if(display_id == 2) {
			var header	 = ["","Build","Too few sequences (<"+miniso+")","Too short length (<"+minlen+"bp)","Filtered"],
				filtered = $("[name^='btlw"+display_id+"-filtered_locus']").val(),
				html = 'The build and filter process produced:<strong>'+filtered+' loci</strong><br/><br/>';
			html += '<table id="btlw'+display_id+'-table"><thead><tr>';
			for (var i=0; i<header.length; i++) {
				html += "<th>" + header[i] +"</th>"; 
			}
			html += "</tr></thead><tbody>";
			
			var values = ["Loci"]
			values.push(parseInt($("[name^='btlw"+display_id+"-locus_number']").val()));
			values.push(parseInt($("[name^='btlw"+display_id+"-too_few_seq']").val()));
			values.push(parseInt($("[name^='btlw"+display_id+"-too_short']").val()));
			values.push(parseInt($("[name^='btlw"+display_id+"-filtered_locus']").val()));

			html += "<tr>";
			for (var i=0; i<values.length; i++) {
				html += "<td>"+values[i]+"</td>";
			}
			html += "</tr></tbody></table>";
						
			$('#bamtolocus-graphics-graph-2').html(html);
		
			$("#btlw"+display_id+"-table").dataTable({
				"bFilter": false,
				"bInfo": false,
				"bPaginate": false,
				"aoColumnDefs": [ { "bSortable": false, "aTargets": [0,1,2,3,4] } ]
			});
		}
		
		if(display_id == 3) {
			modified_values = new Array();
			var	length_keys   = $("[name^='btlw"+display_id+"-length_keys']").val().split(','),
				length_values = $("[name^='btlw"+display_id+"-length_values']").val().split(',');
			var tmp = []
			for (i = 0; i < length_keys.length; i++) {
				tmp.push({x:parseInt(length_keys[i]), y:parseInt(length_values[i])});
			}
			modified_values.push( {
				'data' : tmp
			});
			$('#bamtolocus-graphics-graph-3').highcharts({
				chart: {
					type: "line",
					width: 670,
					spacingLeft: 0,
					zoomType: 'x',
					spacingRight: 20
				},
			    title: { text: null },
				credits: { enabled: false },
			    xAxis: {
					title: { text: 'Length (bp)' },
					plotLines: [{
					    color: 'red',
					    value: parseInt(minlen),
					    width: 2,
					    label: {
				    		text:"Minimum length of locus: " + minlen + "bp",
				    		textAlign: 'right',
		                    rotation:270,
		                    x:15
					    }
					  }]
			    },
			    yAxis: {
			        title: { text: 'Loci number' },
			        min: 0,
			        plotLines: [{
			            value: 0,
			            width: 1,
			            color: '#808080'
			        }]
			    },
			    tooltip: {
			        headerFormat: '<span style="font-size:11px"><b>Length {point.key}bp: </b></span>',
		            pointFormat: '{point.y} loci',
		            useHTML: true
			    },
			    legend: {
			        enabled: false
			    },
			    series: modified_values
			});
		}
		
		if(display_id == 4) {
			modified_values = new Array();
			var	isoforms_keys   = $("[name^='btlw"+display_id+"-isoforms_keys']").val().split(','),
				isoforms_values = $("[name^='btlw"+display_id+"-isoforms_values']").val().split(',');
			var tmp = []
			for (i = 0; i < isoforms_keys.length; i++) {
				tmp.push({x:parseInt(isoforms_keys[i]), y:parseInt(isoforms_values[i])});
			}
			modified_values.push( {
				'data' : tmp
			});
			$('#bamtolocus-graphics-graph-4').highcharts({
				chart: {
					type: "line",
					width: 670,
					spacingLeft: 0,
					zoomType: 'x',
					spacingRight: 20
				},
			    title: { text: null },
				credits: { enabled: false },
			    xAxis: {
					title: { text: 'Isoform number' },
					plotLines: [{
					    color: 'red',
					    value: parseInt(miniso),
					    width: 2,
					    label: {
					    	text:"Minimum number of sequences: " + miniso,
					    	textAlign: 'right',
		                    rotation:270,
		                    x:15
					    	}
					  }]
			    },
			    yAxis: {
			        title: { text: 'Loci number' },
			        min: 0,
			        plotLines: [{
			            value: 0,
			            width: 1,
			            color: '#808080'
			        }]
			    },
			    tooltip: {
			        headerFormat: '<span style="font-size:11px"><b>Isoform number {point.key}: </b></span>',
		            pointFormat: '{point.y} loci',
		            useHTML: true
			    },
			    legend: {
			        enabled: false
			    },
			    series: modified_values
			});
		}
	});
}