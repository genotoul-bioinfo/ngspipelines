
function CleanFastq () {
	
	var	modified_values,
		categories;
	
	$("[id^='cleanfq-link-']").click(function(){
		var display_id = $(this).attr("id").split("-")[3];
		
		$("[id^='cleanfq-link-']").removeClass("li-active");
		$(this).addClass("li-active");
		
		$("div[id^=cleanfq-graphics-wrapper-]").css("display", "none");
		$("#cleanfq-graphics-wrapper-"+display_id).css("display", "inline");
	
		modified_values = new Array();
		categories = new Array();

		if(display_id == 2) {
			$("[name^='cgw"+display_id+"-short']").each(function(){
				var sample = $(this).attr("name").split("-")[2];
				categories.push(sample);
			});
			var	nbreads    = new Array(),
				short      = new Array(),
				long       = new Array(),
				lowcomplex = new Array(),
				cleanedr   = new Array(),
				minlen     = "",
				maxlen     = "";
			for (i=0; i<categories.length; i++) {
				short.push(     parseInt($("[name^='cgw"+display_id+"-short-"+categories[i]+"']").val()));
				long.push(      parseInt($("[name^='cgw"+display_id+"-long-"+categories[i]+"']").val()));
				lowcomplex.push(parseInt($("[name^='cgw"+display_id+"-lowcomplex-"+categories[i]+"']").val()));
				cleanedr.push(  parseInt($("[name^='cgw"+display_id+"-cleanedr-"+categories[i]+"']").val()));
				
				var tmparray = $("[name^='cgw"+display_id+"-length-"+categories[0]+"']").val().split(',');
				minlen = tmparray[0];
				maxlen = tmparray[tmparray.length-1];
			}
			modified_values.push( {
				'name' : "Too short (<" + minlen + "bp)",
				'data' : short
			});
			modified_values.push( {
				'name' : "Too long (>" + maxlen + "bp)",
				'data' : long
			});
			modified_values.push( {
				'name' : "Low complex",
				'data' : lowcomplex
			});
			modified_values.push( {
				'name' : "Cleaned reads",
				'data' : cleanedr
			});
						
			$('#cleanfq-graphics-graph-2').highcharts({
			    chart: {
		            type: 'column'
		        },
				credits: { enabled: false },
		        title: { text: null },
		        xAxis: {
		            categories: categories,
		            crosshair: true
		        },
		        yAxis: {
		            min: 0,
		            title: {
		                text: 'Sequence number'
		            },
		            stackLabels: {
		                enabled: true,
		                style: {
		                    fontWeight: 'bold',
		                    color: 'gray'
		                }
		            }
		        },
		        tooltip: {
		            headerFormat: '<span style="font-size:11px"><b>{point.key}</b></span><table>',
		            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		                '<td style="padding:0"><b>{point.y}</b></td></tr>',
		            footerFormat: '</table>',
		            shared: true,
		            useHTML: true
		        },
		        plotOptions: {
	                column: {
	                    stacking: 'normal'
	                }
	            },
		        series: modified_values
		    });
		}
		
		if(display_id == 3) {
			$("[name^='cgw"+display_id+"-cleanedr']").each(function(){
				var sample = $(this).attr("name").split("-")[2];
				categories.push(sample);
			});
			var	cleanedr = new Array(),
				cleaneds = new Array();
			for (i=0; i<categories.length; i++) {
				cleanedr.push(parseInt($("[name^='cgw"+display_id+"-cleanedr-"+categories[i]+"']").val()));
				cleaneds.push(parseInt($("[name^='cgw"+display_id+"-cleaneds-"+categories[i]+"']").val()));
			}
			modified_values.push( {
				'name' : "Cleaned reads",
				'data' : cleanedr
			});
			modified_values.push( {
				'name' : "Cleaned sequences",
				'data' : cleaneds
			});
			
			$('#cleanfq-graphics-graph-3').highcharts({
			    chart: {
		            type: 'column'
		        },
				credits: { enabled: false },
		        title: { text: null },
		        xAxis: {
		            categories: categories,
		            crosshair: true
		        },
		        yAxis: {
		            min: 0,
		            title: {
		                text: 'Sequence number'
		            }
		        },
		        tooltip: {
		            headerFormat: '<span style="font-size:11px"><b>{point.key}</b></span><table>',
		            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		                '<td style="padding:0"><b>{point.y}</b></td></tr>',
		            footerFormat: '</table>',
		            shared: true,
		            useHTML: true
		        },
		        plotOptions: {
		            column: {
		                pointPadding: 0.2,
		                borderWidth: 0
		            }
		        },
		        series: modified_values
		    });
		}
		
		if(display_id == 4) {
			$("[name^='cgw"+display_id+"-xaxis']").each(function(){
				var sample = $(this).attr("name").split("-")[2];
				var xaxis = $(this).val().split(',');
				var yaxis = $("[name^='cgw"+display_id+"-yaxis-"+sample+"']").val().split(',');
				var data  = new Array();
				for (i = 0; i < xaxis.length; i++) {
					data.push(new Array(parseInt(xaxis[i]), parseInt(yaxis[i])));
				}
				modified_values.push( {
					'name' : sample,
					'data' : data
				});
			});
			
			$('#cleanfq-graphics-graph-4').highcharts({
				chart: {
					type: "line",
					width: 670,
					spacingLeft: 0,
					zoomType: 'x',
					spacingRight: 20
				},
			    title: { text: null },
				credits: { enabled: false },
			    xAxis: {
			    	title: { text: 'Length (bp)' }
			    },
			    yAxis: {
			        title: { text: 'Sequence number' },
			        min: 0,
			        plotLines: [{
			            value: 0,
			            width: 1,
			            color: '#808080'
			        }]
			    },
			    tooltip: {
			        headerFormat: '<span style="font-size:11px"><b>Length {point.key}bp</b></span><table>',
		            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		                '<td style="padding:0"><b>{point.y}</b></td></tr>',
		            footerFormat: '</table>',
		            shared: true,
		            useHTML: true
			    },
			    legend: {
			        layout: 'vertical',
			        align: 'right',
			        verticalAlign: 'middle',
			        borderWidth: 0
			    },
			    series: modified_values
			});
		}
		
		if(display_id == 5) {
			$("[name^='cgw"+display_id+"-xaxis']").each(function(){
				var sample = $(this).attr("name").split("-")[2];
				var xaxis = $(this).val().split(',');
				var yaxis = $("[name^='cgw"+display_id+"-yaxis-"+sample+"']").val().split(',');
				var data  = new Array();
				for (i = 0; i < xaxis.length; i++) {
					data.push(new Array(parseInt(xaxis[i]), parseInt(yaxis[i])));
				}
				modified_values.push( {
					'name' : sample,
					'data' : data
				});
			});
			
			$('#cleanfq-graphics-graph-5').highcharts({
				chart: {
					type: "line",
					width: 670,
					spacingLeft: 0,
					zoomType: 'x',
					spacingRight: 20
				},
			    title: { text: null },
				credits: { enabled: false },
			    xAxis: {
			    	title: { text: 'Length (bp)' }
			    },
			    yAxis: {
			        title: { text: 'Sequence number' },
			        min: 0,
			        plotLines: [{
			            value: 0,
			            width: 1,
			            color: '#808080'
			        }]
			    },
			    tooltip: {
			        headerFormat: '<span style="font-size:11px"><b>Length {point.key}bp</b></span><table>',
		            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		                '<td style="padding:0"><b>{point.y}</b></td></tr>',
		            footerFormat: '</table>',
		            shared: true,
		            useHTML: true
		        },
			    legend: {
			        layout: 'vertical',
			        align: 'right',
			        verticalAlign: 'middle',
			        borderWidth: 0
			    },
			    series: modified_values
			});
		}
	});
	
}