
function SequencesAnnotation () {

	var	modified_values,
		categories;
	
	$("[id^='seqannot-link-']").click(function(){
		var display_id = $(this).attr("id").split("-")[3];
		
		$("[id^='seqannot-link-']").removeClass("li-active");
		$(this).addClass("li-active");
		
		$("div[id^=seqannot-graphics-wrapper-]").css("display", "none");
		$("#seqannot-graphics-wrapper-"+display_id).css("display", "inline");
		
		modified_values = new Array();
		categories = new Array();

		if(display_id == 3) {
			var	meheader = $("[name^='saw"+display_id+"-meheader']").val().split(","),
				mematrix = $("[name^='saw"+display_id+"-mematrix']").val().split("#");
			
			html = '<table id="saw'+display_id+'-table"><thead><tr>';
			for (var i=0; i<meheader.length; i++) {
				html += "<th>" + meheader[i] +"</th>"; 
			}
			html += "</tr></thead><tbody>";
			for (var i=0; i<mematrix.length; i++) {
				html += "<tr>";
				var items = mematrix[i].split(",");
				for (var j=0; j<items.length; j++) {
					html += "<td>"+items[j]+"</td>";
				}
				html += "</tr>";
			}
			html += "</tbody></table>";
			$('#seqannot-graphics-graph-3').html(html);
		
			$("#saw"+display_id+"-table").dataTable({
				"sPaginationType": "full_numbers",
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"aaSorting": [[ 2, "desc" ]],
				"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
	                $('td', nRow).attr('nowrap','nowrap');
	                return nRow;
	            }
			});
		}
		
		if(display_id == 4) {
			function updateJvenn() {
				var seriesTable = new Array();
				$(".venn-active").each(function(){
					var bank_name = $(this).attr("id").replace("venn-pool-", "");
					for (var i=0; i<banks.length; i++) {
						if(banks[i].name == bank_name) {
							seriesTable.push(banks[i]);
						}
					}
				});
				$("#saw"+display_id+"-venn").jvenn({
					series: seriesTable,
					displayMode: $("#display-mode").val(),
					shortNumber: ($("#question-mark").val() == "true"),
					displayStat: ($("#display-stat").val() == "true"),
					fontFamily:  $("#font-family").val(),
					fontSize:    $("#font-size").val(),
					fnClickCallback: function() {
					}
				});
			}
			
			var	banks = new Array(),
				activebanks = {};
			$("[name^='saw"+display_id+"-']").each(function(){
				var bank = $(this).attr("name").replace("saw4-", "");
				banks.push({
					name: $(this).attr("name").replace("saw4-", ""),
					data: $(this).val().split(",")
				});
			});
			html = 'Please click on the box below to select the databanks you want to compare:<br/>';
			for (var i=0; i<banks.length; i++) {
				html += '<div id="venn-pool-' + banks[i].name;
				if(i>5) {
					html += '" class="venn-pool venn-pool0"';
					activebanks[banks[i].name] = false;
				}
				else {
					html += '" class="venn-active venn-pool venn-pool' + (i+1) + '"';
					activebanks[banks[i].name] = true;
				}
				html +=	' style="cursor:pointer"><h2>' + banks[i].name + '</h2>' +
						'<div class="venn-pool-res';
				if(i>5) {
					html += ' venn-pool-res0';
				}
				html += '"><b>' + banks[i].data.length + '</b> annotated sequences' +
						'<br/></div></div>';
			}
			html += '<div style="clear:both"></div>';
			html += '<div id="saw'+display_id+'-venn" style="margin-top:20px;float:left;width:200px"></div>' +
					'<div class="venn-conf">' +
					'<div class="venn-conf-title"><b>Venn configuration</b></div>' +
					'<ul>' +
					'<li class="li-simple" style="margin-top:10px">Mode: &nbsp;' +
				    '<select id="display-mode" class="venn-action">' +
				    '  <option selected="selected" value="classic">Classic</option>' +
				    '  <option value="edwards">Edwards\'</option>' +
				    '</select></li>' +
					'<li class="li-simple" style="margin-top:10px">Display statistics: &nbsp;' +
					'<select id="display-stat" class="venn-action">' +
					'  <option value="true">Yes</option>' +
					'  <option value="false" selected="selected">No</option>' +
					'</select></li>' +
					'<li class="li-simple" style="margin-top:10px">Question mark: &nbsp;' +
					'<select id="question-mark" class="venn-action">' +
					'  <option value="true">Yes</option>' +
					'  <option value="false" selected="selected">No</option>' +
					'</select></li>' +
					'<li class="li-simple" style="margin-top:10px">Font: &nbsp;' +
					'<select id="font-family" class="venn-action">' +
					'  <option value="Arial" selected="selected">Arial</option>' +
					'  <option value="Sans-serif">Sans-serif</option>' +
					'  <option value="Serif">Serif</option>' +
					'  <option value="Monospace">Monospace</option>' +
					'</select></li>' +
					'<li class="li-simple" style="margin-top:10px">Font size: &nbsp;' +
					'<select id="font-size" class="venn-action">' +
					'  <option value="8px">8px</option>' +
					'  <option value="9px">9px</option>' +
					'  <option value="10px">10px</option>' +
					'  <option value="11px">11px</option>' +
					'  <option value="12px" selected="selected">12px</option>' +
					'  <option value="13px">13px</option>' +
					'  <option value="14px">14px</option>' +
					'</select></li>' +
					'</ul></div>';
			html += '<div style="clear:both"></div>';

			$('#seqannot-graphics-graph-4').html(html);
			updateJvenn();
			
			$(".venn-action").click(function() {
				updateJvenn();				
			});
			
			$("[id^='venn-pool-']").click(function(){
				var bankname = $(this).attr("id").replace("venn-pool-", "");
				if($(this).hasClass("venn-active")) {
					activebanks[bankname] = false;
				}
				else {
					var cpt=0;
					for (var k in activebanks) {
						if(activebanks[k]) { cpt++; }
					}
					if(cpt<6) {
						activebanks[bankname] = true;
					}
					else {
						ngspipelines.ui.display("error", "Up to 6 databanks can be provided!", $("#seqannot-error-4"));
						$("#seqannot-error-4").delay(1500).fadeOut(800);
					}
				}
				var cpt=1;
				for (var k in activebanks) {
					if(activebanks[k]) {
						$('#venn-pool-'+k).removeClass().addClass('venn-active venn-pool venn-pool'+cpt);
						cpt++;
						$('#venn-pool-'+k).find('.venn-pool-res').removeClass().addClass('venn-pool-res');
					}
					else {
						$('#venn-pool-'+k).removeClass().addClass("venn-pool venn-pool0");
						$('#venn-pool-'+k).find('.venn-pool-res').removeClass().addClass('venn-pool-res venn-pool-res0');
					}
				}
				updateJvenn();
			});
		}
	});
}