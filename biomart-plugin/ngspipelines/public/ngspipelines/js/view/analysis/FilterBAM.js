
function FilterBAM () {

	var	modified_values,
		categories;
	
	$("[id^='filterbam-link-']").click(function(){
		var display_id = $(this).attr("id").split("-")[3];
		
		$("[id^='filterbam-link-']").removeClass("li-active");
		$(this).addClass("li-active");
		
		$("div[id^=filterbam-graphics-wrapper-]").css("display", "none");
		$("#filterbam-graphics-wrapper-"+display_id).css("display", "inline");
		
		modified_values = new Array();
		categories = new Array();

		if(display_id == 2) {
			//Bowtie statistics
			var	header = ["","Version"];
			header = header.concat($("[name^='fbw"+display_id+"-bowtie-keys']").val().split(","));
			var	html = '<img src="/ngspipelines/img/bullet.png" /> <strong>Bowtie alignment statistics:</strong>' +
					   '<br/><br/><table id="fbw'+display_id+'-bowtie-table"><thead><tr>';
			for (var i=0; i<header.length; i++) {
				html += "<th>" + header[i] +"</th>"; 
			}
			html += "</tr></thead><tbody>";
			
			$(".bowtie").each(function(){
				var	name = $(this).attr("name"),
					ref  = name.replace("fbw"+display_id+"-bowtie-",""); 
				if (name.indexOf("keys") == -1) {
					var values = [ref, $("[name^='fbw"+display_id+"-version-"+ref+"']").val()];
					values = values.concat($(this).val().split(","));
					html += "<tr>";
					for (var i=0; i<values.length; i++) {
						html += "<td>"+values[i]+"</td>";
					}
					html += "</tr>";
				}
			});
			html += "</tbody></table>";
			
			//Filter statistics
			header = [""];
			header = header.concat($("[name^='fbw"+display_id+"-filter-keys']").val().split(","));
			html  += '<br/><br/>' +
					 '<img src="/ngspipelines/img/bullet.png" /> <strong>Filter alignment statistics:</strong>' +
					 '<br/><br/><table id="fbw'+display_id+'-filter-table"><thead><tr>';
			for (var i=0; i<header.length; i++) {
				html += "<th>" + header[i] +"</th>"; 
			}
			html += "</tr></thead><tbody>";

			$(".filter").each(function(){
				var	name = $(this).attr("name"),
					ref  = name.replace("fbw"+display_id+"-filter-",""); 
				if (name.indexOf("keys") == -1) {
					var values = [ref];
					values = values.concat($(this).val().split(","));
					html += "<tr>";
					for (var i=0; i<values.length; i++) {
						html += "<td>"+values[i]+"</td>";
					}
					html += "</tr>";
				}
			});
			html += "</tbody></table>";
			
			$('#filterbam-graphics-graph-2').html(html);
		
			$("#fbw"+display_id+"-bowtie-table").dataTable({
				"sPaginationType": "full_numbers",
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"bFilter": false,
				"aaSorting": [[ 0, "asc" ]],
				"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
	                $('td', nRow).attr('nowrap','nowrap');
	                return nRow;
	            }
			});
			
			$("#fbw"+display_id+"-filter-table").dataTable({
				"sPaginationType": "full_numbers",
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"bFilter": false,
				"aaSorting": [[ 0, "asc" ]],
				"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
	                $('td', nRow).attr('nowrap','nowrap');
	                return nRow;
	            }
			});
		}
		
		if(display_id == 3) {
			categories = $("[name^='fbw"+display_id+"-keys']").val().split(',');
			categories.shift();
			var	tmpdata = $("[name^='fbw"+display_id+"-values']").val().split(','),
				total = parseInt(tmpdata.shift()),
				data  = new Array();
			for (var i = 0; i < tmpdata.length; i++) {
				data.push(parseInt(tmpdata[i])/total*100);
			}
			modified_values.push( {
				'data' : data
			});
			$('#filterbam-graphics-graph-3').highcharts({
		        chart: {
		        	type: 'bar'
		        },
		        title: { text: null },
		        xAxis: {
		        	categories: categories,
		        	title: {text: null}
		        },
		        yAxis: {
		        	min: 0,
		        	max:100,
		        	title: {
		        		text: 'Percentage of total sequences (%)',
		        		align: 'high',
		        		labels: {
		        			overflow: 'justify'}
		        		}
		        },
		        tooltip: {
		        	formatter: function() {
		                return '<b>' + Highcharts.numberFormat(this.y/100*total, 0) +
		                		'/' + total +
		                		'</b> mapped sequences'
		        	}
		        },
		        plotOptions: {
		        	bar: {
		        		dataLabels: {
		        			enabled: true,
		        			format: '<b>{point.y:.2f}%</b>',
		        		    style: {
		        		    	fontWeight: 'bold',
				                color: 'gray'
				            }
		        		}
		        	}
		        },
		        legend: { enabled:false },
		        credits: { enabled: false },
		        series: modified_values
		    });
		}
	});
}