<p>The contig page gives access to different pages including :</p>
<ol class="help">
	<li> general information</li>
	<li> sequence view</li>
	<li> jbrowse view</li>
	<li> depth view</li>
</ol>
<p>The view are accessed through the menu located on the top of the page and presented in the
next screen-shot.</p>

<h2 class="help">Contig general information page</h2>
<p>The first page of the contig section gives general informations about the contigs, its annotation, prediction and variant.</p>
<img src="js/view/rnaseqdenovo/help_img/report_contig_1.jpg"/>
<img src="js/view/rnaseqdenovo/help_img/report_contig_2.jpg"/>

<p>The previous screen-shot presents :</p>
<ol class="help">
	<li> the contig menu</li>
	<li> general information section</li>
	<li> best hit section</li>
	<li> Uniprot-Swissprot keyword section</li>
	<li> GO section</li>
	<li> annotation section</li>
	<li> prediction section</li>
	<li> List of SNP/Indels if contig contains it.</li>
</ol>
<p>
At the bottom of this webpage you will also find the SNP Indel section.
The general information section contains :
</p>
<ol class="help">
	<li>  the name, length and global expression value of the contigs</li>
	<li>  a button to remove this contig from the favorites</li>
	<li>  a button to export the contig sequence in fasta format</li>
	<li>  a button to export the annotation in GFF format</li>
	<li>  a button to export the SNP INDEL of the contig in a tabulate file</li>
</ol>
<p></p>
<img src="js/view/rnaseqdenovo/help_img/report_general_info.jpg"/>


<h2 class="help">Sequence view</h2>
<p>The next page of the contig visualisation part gives access to various informations about the
contig sequence and some tools to analyse it.<br/>The screen-shot here over presents :
</p>

<img src="js/view/rnaseqdenovo/help_img/report_sequence_view.jpg"/>
<ol class="help">
	<li>  the contig browsing menu,</li>
	<li>  the sequence informations such as nucleotide content and longest open reading frame
(ORF),</li>
	<li>  the view button presenting the longest ORF in the sequence view,</li>
	<li>  the sequence view with possible starts in green, stops in red and ORFs in blue,</li>
	<li>  the query form permitting different action on the sequence such as extraction, reverse
complementation, translation in different frames, ORF presentation and text search.</li>
</ol>
<h2 class="help">Contig jbrowse view</h2>
<p>To give a graphical view of the annotations and other features on the contigs, jbrowse (a
genome browser) has been included in the environment. The jbrowse view uses the contig as
reference and presents the features as different drawings on the reference.<br/>
The screen-shot hereunder presents the different elements of the jbrowse view :</p>
<ol class="help">
	<li>the available tracks list presenting the features which can be displayed on the contig,</li>
	<li>the reference ruler which enables to move on the reference by dragging the red rectangle,</li>
	<li>the arrows to move to the left and right on the contig and the zoom in and out menu,</li>
	<li>the browsed contig,</li>
	<li>the location of the view on the contig,</li>
	<li>the display panel.</li>
</ol>
<p></p>
<img src="js/view/rnaseqdenovo/help_img/report_jbrowse_view.jpg"/>
<p>To add a feature to the display panel simply drag and drop it from the available tracks list. To
remove a feature click on the cross located at the left of the feature name.</p>
<h2 class="help">Contig depth view</h2>
<p>
The contig depth view enables to visualise the coverage of the reads of the different libraries
on the contig. Each of the library has a colour in the table and on the graphic.<br/>
The screen-shot hereunder presents the different elements of the depth view :</p>
<ol class="help">
	<li> the library table containing informations about the libraries such as average read depth and
total number of sequences for the contig,</li>
	<li> The graphical depth overview presents a different locations of the contig the depth of
aligned sequences for each library,</li>
	<li> the library can be removed or added to the graphical view by clicking on their name in the
menu.</li>
</ol>
<p></p>
<img src="js/view/rnaseqdenovo/help_img/report_depth_view.jpg"/>
<div style="">
<p>It is possible to modify the graphical
layout by averaging different library depth.</p>
<ol class="help">
	<li> This is done by first ticking the check
boxes in front of the libraries and the by
clicking on the “apply label” button at the left
bottom side of the table.</li>
	<li> The window present here will open
and permit to select the name given to the
previously select libraries or to create a new
name using the add button.</li>
	<li> When the modification is applied the
different libraries have the same name and the
depth values in the graphic are the average
depth values of these libraries.</li>
</ol>
</div>
<div >
<img src="js/view/rnaseqdenovo/help_img/report_depth_view_label.jpg"/>
</div>
<p>This can typically be used when you want
to merge libraries.
The table (1) and graphic (2) have been updated.
</p>
<img src="js/view/rnaseqdenovo/help_img/report_depth_view_label_updated.jpg"/>