(function($) {
	$.fn.locusreport = function(options) {
		var _template = "js/view/srnaseq/locusreport.tmpl", _depthTableTemplate = "js/view/srnaseq/depthtable.tmpl", _featuresTableTemplate = "js/view/srnaseq/featurestable.tmpl", _isoformsTableTemplate = "js/view/srnaseq/isoformstable.tmpl", _pluginIsInit = new Array(),

		// default option values

		defaults = {
			id : "",
			dataset : {},
			analyses : {},
			project : {},
			rootName : "",
			martName : "",
			datasetType : "",
			prefix : "",
			labelColors : Highcharts.theme.colors,
			callback : function() {
			}
		};

		// merge default option values and provided parameters
		var opts = $.extend(defaults, options);

		var parseFloat10 = function(val) {
			return parseFloat(val, 10);
		}

		var _updateDepthGraph = function(libraries, prefix) {
			ngspipelines.ui.wait("Please wait while generating the graph ...",
					"center", $("#" + opts.prefix + "-depth-graph"));

			// first parse the library table in order to know labels and
			// libraries associated
			var labels = {}, step = -1, series = new Array(), categories = new Array(), nbStep = -1, labelColors = {};

			// first gather all coverage libraries per labels
			$("[id^=library_]")
					.each(
							function() {
								var index = $(this).attr("id").split("_")
										.slice(-1)[0], sampleName = $(
										"#sample_" + index).html(), clibrary = sampleName
										.split(".")[0], creplicat = sampleName
										.split(".")[1], ccoverage = "";

								for ( var libindex in libraries[clibrary]) {
									if (step == -1) {
										step = parseInt(libraries[clibrary][libindex].window_size);
									}
									if (libindex != "remove"
											&& libraries[clibrary][libindex].replicat == creplicat) {
										ccoverage = libraries[clibrary][libindex].coverage;
									} else if (libindex != "remove"
											&& creplicat == undefined) {
										ccoverage = libraries[clibrary][libindex].coverage;
									}
								}

								if (labels.hasOwnProperty($(this).html())) {
									labels[$(this).html()].push(ccoverage
											.split(",").map(parseFloat10));
								} else {
									labels[$(this).html()] = new Array();
									labels[$(this).html()].push(ccoverage
											.split(",").map(parseFloat10));
									labelColors[$(this).html()] = $(this).css(
											"background-color");
								}

							});

			// then create the series table
			for ( var key in labels) {
				var mcoverage = new Array();
				for (var mindex = 0; mindex < labels[key][0].length; mindex++) {
					if (nbStep == -1) {
						nbStep = labels[key][0].length;
					}
					var cvalue = 0.0;
					for ( var covindex in labels[key]) {
						if (covindex != "remove") {
							cvalue += labels[key][covindex][mindex];
						}
					}
					cvalue = cvalue / labels[key].length;
					mcoverage.push(cvalue + 1); // +1 due to log transform see
					// below yAxis
				}
				series.push({
					name : key,
					color : labelColors[key],
					data : mcoverage
				});
			}

			var catindex = step / 2;
			// finaly create the cathegories array
			for (var cindex = 0; cindex < nbStep; cindex++) {
				categories.push(catindex.toString());
				catindex += step;
			}

			var depthGraph = new Highcharts.Chart({
				chart : {
					renderTo : 'locus-depth-graph',
					type : 'line',
				},
				title : {
					text : null
				},
				credits : {
					enabled : false
				},
				xAxis : {
					title : {
						text : 'Position on the locus'
					},
					labels : {
						enabled : false
					},
					categories : categories
				},
				yAxis : {
					min : 1,
					title : {
						text : 'Depth in bp'
					},
					type : 'logarithmic',
					minorTickInterval : 0.1,
					labels : {
						formatter : function() {
							return this.value - 1;
						}
					}
				},
				tooltip : {
					formatter : function() {
						return '<b>' + this.series.name + '</b><br/>'
								+ Highcharts.numberFormat(this.y - 1, 0)
								+ ' bp at position '
								+ (parseInt(this.point.x) + 1);
					}
				},
				series : series,
				plotOptions : {
					series : {
						lineWidth : 1,
						marker : {
							radius : 3
						}
					}
				}
			});
		}

		var cmpfound = 0;
		jQuery.fn.highlight = function(str) {
			var regex = new RegExp(str, "gi");
			return this.each(function() {
				$(this).contents().filter(function() {
					return this.nodeType == 3;
				}).replaceWith(
						function() {
							return (this.nodeValue || "").replace(regex,
									function(match) {
										cmpfound++;
										return "<span class=\"highlight-nuc\">"
												+ match + "</span>";
									});
						});
			});
		}

		var getPos = function(seqLen, twostruct, nostruct) {
			var res = "<br/>", lineLen = 90;
			if (!nostruct) {
				for (k = lineLen; k < seqLen; k += lineLen) {
					res += k + "<br/><br/><br/>";
				}
				if (twostruct) {
					res += "<br/>";
				}
			} else {
				for (k = lineLen; k < seqLen; k += lineLen) {
					res += k + "<br/>";
				}
			}
			res += seqLen;
			return res;
		}

		/*
		 * Update Sequence view with user params Param 1 : biomart query result
		 * Param 2 : boolean for highlight (search nuc)
		 */
		var displaySeqView = function(data, highlighter, activestruct) {
			var from = $('#srnaseq-extract-from').val() - 1, to = $(
					'#srnaseq-extract-to').val(), len = to - from;

			var seqAndStruct = "", tmpStruct1 = "", tmpStruct2 = "", cpt = 0, in_pred = false, start_prediction = data.prediction_start
					- data.start - from, stop_prediction = data.prediction_stop
					- data.start + 1 - from, mature_highlight_start = "", mature_highlight_end = "";

			if (data.strand == "-1") {
				start_prediction = data.stop - data.prediction_stop - from;
				stop_prediction = data.stop - data.prediction_start + 1 - from;
			}

			if (data.type == "miRNA") {
				if (!highlighter || $('#srnaseq-search-nuc').val().length <= 2) {
					mature_highlight_start = '<span class="highlight-nuc">';
					mature_highlight_end = '</span>';
				}
			}

			var twostruct = false, nostruct = false;
			if (data.structure_structure2.length > 0
					&& data.structure_structure1.length > 0) {
				twostruct = true;
				$('#view-struct').show();
			} else if (data.structure_structure1.length == 0) {
				$("#locus-struct-infos").remove();
				$('#sequence-display').css("margin-top", "88px");
				$('#sequence-pos').css("margin-top", "73px");
				nostruct = true;
			}
			for (k = from; k < to; k++) {
				var seq = data.dna_sequence.substr(k, 1), struct1 = data.structure_structure1
						.substr(k, 1), struct2 = "";

				if (twostruct) {
					struct2 = data.structure_structure2.substr(k, 1);
				}

				// Highlight prediction positions
				if (cpt == start_prediction) {
					in_pred = true;
					seqAndStruct += mature_highlight_start;
				}

				cpt++;

				// Space added every 10 bases
				if (cpt % 10 == 0) {
					if (cpt == stop_prediction) {
						in_pred = false;
						seqAndStruct += seq + mature_highlight_end + " ";
						tmpStruct1 += struct1 + " ";
						if (twostruct) {
							tmpStruct2 += struct2 + " ";
						}
					} else {
						seqAndStruct += seq + " ";
						tmpStruct1 += struct1 + " ";
						if (twostruct) {
							tmpStruct2 += struct2 + " ";
						}
					}
				} else {
					if (cpt == stop_prediction) {
						in_pred = false;
						seqAndStruct += seq + mature_highlight_end;
						tmpStruct1 += struct1;
						if (twostruct) {
							tmpStruct2 += struct2;
						}
					} else {
						seqAndStruct += seq;
						tmpStruct1 += struct1;
						if (twostruct) {
							tmpStruct2 += struct2;
						}
					}
				}

				// Back to line every 90 bases or at end
				if (cpt % 90 == 0 || cpt == len) {
					if (in_pred) {
						seqAndStruct += mature_highlight_end;
					}
					if (!nostruct) {
						seqAndStruct += '<br/><span class="struct1">'
								+ tmpStruct1 + '</span><br/>';
						if (twostruct) {
							seqAndStruct += '<span class="struct2">'
									+ tmpStruct2 + '</span><br/>';
						}
						if (cpt != len) {
							seqAndStruct += "<br/>";
						}
					} else {
						seqAndStruct += '<br/><span class="struct1">';
					}
					tmpStruct1 = "";
					tmpStruct2 = "";
					if (in_pred) {
						seqAndStruct += mature_highlight_start;
					}
				}
			}
			$('#sequence-pos').html(getPos(len, twostruct, nostruct));
			$('#sequence-display').html(
					'>' + data.name + '<br/>' + seqAndStruct);
			if (highlighter && $('#srnaseq-search-nuc').val().length > 2) {
				var searchNuc = "";
				for (k = 0; k < $('#srnaseq-search-nuc').val().length; k++) {
					var x = $('#srnaseq-search-nuc').val().substr(k, 1);
					if (x != " ") {
						searchNuc += " ?" + x;
					}
				}
				cmpfound = 0;
				$('#sequence-display').highlight(searchNuc);
				var mess = "Search: ";
				if (cmpfound == 0) {
					mess += "not found!";
				} else if (cmpfound == 1) {
					mess += "<strong>" + cmpfound + "</strong> match!";
				} else {
					mess += "<strong>" + cmpfound + "</strong> matches!";
				}
				ngspipelines.ui.display("info", mess,
						$("#report-srnaseq-tab-error"));
			}
			if (activestruct == 1) {
				$(".struct2").fadeTo(0, 0.33);
			} else {
				$(".struct1").fadeTo(0, 0.33);
			}
		}
		
		//FORNA EXPORT MODULE
		//options: scale, responsive, width, height
		var exportToMod = function(el, name, options){
		  var doctype = '<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';

		  function isElement(obj) {
		    return obj instanceof HTMLElement || obj instanceof SVGElement;
		  };

		  function requireDomNode(el) {
		    if (!isElement(el)) {
		      throw new Error('an HTMLElement or SVGElement is required; got ' + el);
		    }
		  };

		  function isExternal(url) {
		    return url && url.lastIndexOf('http',0) == 0 && url.lastIndexOf(window.location.host) == -1;
		  };

		  function inlineImages(el, callback) {
		    requireDomNode(el);

		    var images = el.querySelectorAll('image'),
		        left = images.length,
		        checkDone = function() {
		          if (left === 0) {
		            callback();
		          }
		        };

		    checkDone();
		    for (var i = 0; i < images.length; i++) {
		      (function(image) {
		        var href = image.getAttributeNS("http://www.w3.org/1999/xlink", "href");
		        if (href) {
		          if (isExternal(href.value)) {
		            console.warn("Cannot render embedded images linking to external hosts: "+href.value);
		            return;
		          }
		        }
		        var canvas = document.createElement('canvas');
		        var ctx = canvas.getContext('2d');
		        var img = new Image();
		        href = href || image.getAttribute('href');
		        if (href) {
		          img.src = href;
		          img.onload = function() {
		            canvas.width = img.width;
		            canvas.height = img.height;
		            ctx.drawImage(img, 0, 0);
		            image.setAttributeNS("http://www.w3.org/1999/xlink", "href", canvas.toDataURL('image/png'));
		            left--;
		            checkDone();
		          }
		          img.onerror = function() {
		            console.log("Could not load "+href);
		            left--;
		            checkDone();
		          }
		        } else {
		          left--;
		          checkDone();
		        }
		      })(images[i]);
		    }
		  };

		  function styles(el, selectorRemap) {
		    var css = "";
		    var sheets = document.styleSheets;
		    for (var i = 0; i < sheets.length; i++) {
		      try {
		        var rules = sheets[i].cssRules;
		      } catch (e) {
		        console.warn("Stylesheet could not be loaded: "+sheets[i].href);
		        continue;
		      }

		      if (rules != null) {
		        for (var j = 0; j < rules.length; j++) {
		          var rule = rules[j];
		          if (typeof(rule.style) != "undefined") {
		            var match, selectorText;

		            try {
		              selectorText = rule.selectorText;
		            } catch(err) {
		              console.warn('The following CSS rule has an invalid selector: "' + rule + '"', err);
		            }

		            try {
		              if (selectorText) {
		                match = el.querySelector(selectorText);
		              }
		            } catch(err) {
		              console.warn('Invalid CSS selector "' + selectorText + '"', err);
		            }

		            if (match) {
		              var selector = selectorRemap ? selectorRemap(rule.selectorText) : rule.selectorText;
		              css += selector + " { " + rule.style.cssText + " }\n";
		            } else if(rule.cssText.match(/^@font-face/)) {
		              css += rule.cssText + '\n';
		            }
		          }
		        }
		      }
		    }
		    return css;
		  };

		  function download(name, uri) {
		    var a = document.createElement('a');
		    a.download = name;
		    a.href = uri;
		    document.body.appendChild(a);
		    a.addEventListener("click", function(e) {
		      a.parentNode.removeChild(a);
		    });
		    a.click();
		  };

		  function getDimension(el, clone, dim) {
		    var v = (el.viewBox && el.viewBox.baseVal && el.viewBox.baseVal[dim]) ||
		      (clone.getAttribute(dim) !== null && !clone.getAttribute(dim).match(/%$/) && parseInt(clone.getAttribute(dim))) ||
		      el.getBoundingClientRect()[dim] ||
		      parseInt(clone.style[dim]) ||
		      parseInt(window.getComputedStyle(el).getPropertyValue(dim));
		    return (typeof v === 'undefined' || v === null || isNaN(parseFloat(v))) ? 0 : v;
		  }

		  function reEncode(data) {
		    data = encodeURIComponent(data);
		    data = data.replace(/%([0-9A-F]{2})/g, function(match, p1) {
		      var c = String.fromCharCode('0x'+p1);
		      return c === '%' ? '%25' : c;
		    });
		    return decodeURIComponent(data);
		  }

		  var svgAsDataUri = function(el, options, cb) {
		    requireDomNode(el);

		    options = options || {};
		    options.scale = options.scale || 1;
		    options.responsive = options.responsive || false;
		    var xmlns = "http://www.w3.org/2000/xmlns/";

		    inlineImages(el, function() {
		      var outer = document.createElement("div");
		      var clone = el.cloneNode(true);
		      var width, height;
		      if(el.tagName == 'svg') {
		        width = options.width || getDimension(el, clone, 'width');
		        height = options.height || getDimension(el, clone, 'height');
		      } else if(el.getBBox) {
		        var box = el.getBBox();
		        width = box.x + box.width;
		        height = box.y + box.height;
		        clone.setAttribute('transform', clone.getAttribute('transform').replace(/translate\(.*?\)/, ''));

		        var svg = document.createElementNS('http://www.w3.org/2000/svg','svg')
		        svg.appendChild(clone)
		        clone = svg;
		      } else {
		        console.error('Attempted to render non-SVG element', el);
		        return;
		      }

		      clone.setAttribute("version", "1.1");
		      if (!clone.getAttribute('xmlns')) {
		        clone.setAttributeNS(xmlns, "xmlns", "http://www.w3.org/2000/svg");
		      }
		      if (!clone.getAttribute('xmlns:xlink')) {
		        clone.setAttributeNS(xmlns, "xmlns:xlink", "http://www.w3.org/1999/xlink");
		      }

		      if (options.responsive) {
		        clone.removeAttribute('width');
		        clone.removeAttribute('height');
		        clone.setAttribute('preserveAspectRatio', 'xMinYMin meet');
		      } else {
		        clone.setAttribute("width", width * options.scale);
		        clone.setAttribute("height", height * options.scale);
		      }

		      clone.setAttribute("viewBox", [
		        options.left || 0,
		        options.top || 0,
		        width,
		        height
		      ].join(" "));

		      var fos = clone.querySelectorAll('foreignObject > *');
		      for (var i = 0; i < fos.length; i++) {
		        if (!fos[i].getAttributeNS('xml', 'xmlns')) {
		          fos[i].setAttributeNS(xmlns, "xmlns", "http://www.w3.org/1999/xhtml");
		        }
		      }

		      outer.appendChild(clone);

		      var css = styles(el, options.selectorRemap);
		      var s = document.createElement('style');
		      s.setAttribute('type', 'text/css');
		      s.innerHTML = "<![CDATA[\n" + css + "\n]]>";
		      var defs = document.createElement('defs');
		      defs.appendChild(s);
		      clone.insertBefore(defs, clone.firstChild);

		      var svg = doctype + outer.innerHTML;
		      var uri = 'data:image/svg+xml;base64,' + window.btoa(reEncode(svg));
		      if (cb) {
		        cb(uri);
		      }
		    });
		  };

		  var svgAsPngUri = function(el, options, cb) {
		    requireDomNode(el);

		    return svgAsDataUri(el, options, function(uri) {
		      var image = new Image();
		      image.onload = function() {
		        var canvas = document.createElement('canvas');
		        canvas.width = image.width;
		        canvas.height = image.height;
		        var context = canvas.getContext('2d');
		        if(options && options.backgroundColor){
		          context.fillStyle = options.backgroundColor;
		          context.fillRect(0, 0, canvas.width, canvas.height);
		        }
		        context.drawImage(image, 0, 0);
		        var a = document.createElement('a'), png;
		        try {
		          png = canvas.toDataURL('image/png');
		        } catch (e) {
		          if ((typeof SecurityError !== 'undefined' && e instanceof SecurityError) || e.name == "SecurityError") {
		            console.error("Rendered SVG images cannot be downloaded in this browser.");
		            return;
		          } else {
		            throw e;
		          }
		        }
		        cb(png);
		      }
		      image.onerror = function() {
		        console.error(
		          'There was an error loading the data URI as an image on the following SVG\n',
		          window.atob(uri.slice(26)), '\n',
		          "Open the following link to see browser's diagnosis\n",
		          uri);
		      }
		      image.src = uri;
		    });
		  };

		  var saveSvg = function(el, name, options) {
		    requireDomNode(el);

		    options = options || {};
		    return svgAsDataUri(el, options, function(uri) {
		      download(name, uri);
		    });
		  }

		  var saveSvgAsPng = function(el, name, options) {
		    requireDomNode(el);

		    options = options || {};
		    return svgAsPngUri(el, options, function(uri) {
		      download(name, uri);
		    });
		  };

		  return {
		    svg: saveSvg,
		    png: saveSvgAsPng
		  }
		}();
		
		
		
		// FORNA
		fornacDisplay = function() {

			var initSize = null;
			var options = null;
			var containerElement = null;
			var container = null;
			var exportTo = null;

			var changeColor = function(event) {
				if (typeof event.data.color === 'undefined') {
					throw new Exception('Undefined color event');
				}
				var c = event.data.color;
				if (c !== 'sequence' && c !== 'structure' && c !== 'positions') {
					throw new Exception(
							'Color must be "sequence", "structure" or "positions"');
				}
				container.changeColorScheme(c);
			};

			var requestFullscreen = function() {
				if (screenfull.enabled) {
					screenfull.request($(containerElement)[0]);
				}
			};

			var center = function() {
				container.centerView();
			};

			var toggleNumbering = function() {
				container
						.displayNumbering(!container.displayParameters.displayNumbering);
			};

			var toggleNodeOutline = function() {
				container
						.displayNodeOutline(!container.displayParameters.displayNodeOutline);
			};

			var toggleNodeLabel = function() {
				container
						.displayNodeLabel(!container.displayParameters.displayNodeLabel);
			};

			var toggleAnimation = function() {
				(container.animation) ? container.stopAnimation() : container
						.startAnimation();
			};

			var getWindowDimensions = function() {
				return {
					width : window.innerWidth
							|| document.documentElement.clientWidth
							|| document.body.clientWidth,
					height : window.innerHeight
							|| document.documentElement.clientHeight
							|| document.body.clientHeight
				};
			};

			var init = function(sequence, structure, interactionStart,
					interactionStop, htmlContainer) {
				containerElement = htmlContainer;
				initSize = [ 467, 450 ]; // width, height
				options = {
					'sequence' : sequence,
					'structure' : structure
				};
				// if no interaction to highlight
				if (interactionStart === null) {
					interactionStart = 0;
					interactionStop = 0;
				}
				exportTo = exportToMod;

				// init container for forna
				container = new fornac.FornaContainer(containerElement, {
					'applyForce' : true,
					'allowPanningAndZooming' : true,
					'displayAllLinks' : true,
					'labelInterval' : 10,
					'initialSize' : initSize,
					// added for sRNA-TaBac
					'interactionStart' : interactionStart,
					'interactionStop' : interactionStop
				// 'transitionDuration': 300,
				});
				container.stopAnimation();
				container.displayNodeLabel(false);
				var rnaJSON = container.addRNA(options.structure, options);

				// hide fullscreen
				$(document).bind(screenfull.raw.fullscreenchange, function() {
					if (!screenfull.isFullscreen) {
						container.setSize(initSize);
					}
				});
				$(document)
						.bind(
								screenfull.raw.fullscreenerror,
								function(event) {
									console.error(
											'Failed to enable fullscreen',
											event);
								});
			};

			var screenSize = function() {
				return [ screen.width, screen.height ];
			};

			var getRNAName = function(){
				return $('ul > li:has(strong:contains("Name:"))').text().trim().split(' ')[2];
			};
			
			var exportSvg = function() {
				exportTo.svg(document.getElementById('plotting-area'), getRNAName() + '.svg');
				
			};

			var exportPng = function() {
				exportTo.svg(document.getElementById('plotting-area'), getRNAName() + '.png', {scale: 4});
			};

			return {
				init : init,
				changeColor : changeColor,
				requestFullscreen : requestFullscreen,
				center : center,
				toggleNumbering : toggleNumbering,
				toggleNodeOutline : toggleNodeOutline,
				toggleNodeLabel : toggleNodeLabel,
				toggleAnimation : toggleAnimation,
				exportPng : exportPng,
				exportSvg : exportSvg
			};
		}();

		var getForna = function(data, activestruct) {
			var fornaCode = '<p>No structure to display</p>';
			if (data.structure_structure1 != "") {
				fornaCode = '<div id="rna_ss"></div>'
						+ '<div class="inter-bloc">'
						+ ' <div class="inter-bloc-title">Controls</div>'
						+ ' <ul><li class="li-single">'
						+ '  <button id="fornac-control-center" class="ngsp-button ngsp-btn-white ngsp-btn-small"><i class="icon-align-center"></i></button>'
						+ '  <button id="fornac-control-fullscreen" class="ngsp-button ngsp-btn-white ngsp-btn-small"><i class="icon-fullscreen"></i></button>'
						+ '  <span class="vertical-separator"></span>'
						+ '  <button id="fornac-control-toggle-numbering" class="ngsp-button ngsp-btn-white ngsp-btn-small"><i class="icon-info-sign"></i></button>'
						+ '  <button id="fornac-control-toggle-label" class="ngsp-button ngsp-btn-white ngsp-btn-small"><i class="icon-font"></i></button>'
						+ '  <button id="fornac-control-toggle-outline" class="ngsp-button ngsp-btn-white ngsp-btn-small"><i class="icon-check"></i></button>'
						+ '  <button id="fornac-control-toggle-animate" class="ngsp-button ngsp-btn-white ngsp-btn-small"><i class="icon-play"></i></button>'
						+ ' </li></ul>'
						+ ' <ul><li class="li-single">'
						+ '  <span>Color</span>'
						+ '  <button id="fornac-control-choice-color-sequence" class="ngsp-button ngsp-btn-white ngsp-btn-small">Sequence</button>'
						+ '  <button id="fornac-control-choice-color-structure" class="ngsp-button ngsp-btn-white ngsp-btn-small">Structure</button>'
						+ '  <button id="fornac-control-choice-color-position" class="ngsp-button ngsp-btn-white ngsp-btn-small">Position</button>'
						+ ' </li></ul>'
						+ ' <ul><li class="li-single">'
						+ '  <span>Export</span>'
						+ '  <button id="fornac-control-export-png" class="ngsp-button ngsp-btn-white ngsp-btn-small"><i class="icon-download"></i>PNG</button>'
						+ '  <button id="fornac-control-export-svg" class="ngsp-button ngsp-btn-white ngsp-btn-small"><i class="icon-download"></i>SVG</button>'
						+ ' </li></ul>' 
						+ '</div>';

				// insert forna container and controls
				$("#forna").html(fornaCode);
				$('#forna').ready(function() {
					// get structure
					var struct = (activestruct == 2) ? data.structure_structure2 : data.structure_structure1;
					// highlight miRNA if present
					var miRNAStart = null;
					var miRNAStop = null;
					if (data.type == "miRNA") {
						miRNAStart = data.prediction_start - data.start + 1;
						miRNAStop = data.prediction_stop - data.start + 1;
					}
					// init forna display
					fornacDisplay.init(data.dna_sequence, struct, miRNAStart, miRNAStop, '#rna_ss');
					// when ready
					$('#rna_ss').ready(function() {
						$('#zrect').removeAttr('stroke');
						$('#zrect').removeAttr('stroke-width');
						// general button toggle
						// style blue/white
						$('[id^="fornac-control-toggle-"]').bind('click',function() {
							if ($(this).hasClass('ngsp-btn-white')) {
								$(this).removeClass('ngsp-btn-white')
									.addClass('ngsp-btn-blue');
							} else {
								$(this).removeClass('ngsp-btn-blue')
									.addClass('ngsp-btn-white');
							}
						});
						// general button choice
						// style blue/white
						$('[id^="fornac-control-choice-"]').bind('click',function() {
							$('#fornac-control-choice-color-sequence')
								.removeClass('ngsp-btn-white')
								.removeClass('ngsp-btn-blue')
								.addClass('ngsp-btn-white');
							$('#fornac-control-choice-color-structure')
								.removeClass('ngsp-btn-white')
								.removeClass('ngsp-btn-blue')
								.addClass('ngsp-btn-white');
							$('#fornac-control-choice-color-position')
								.removeClass('ngsp-btn-white')
								.removeClass('ngsp-btn-blue')
								.addClass('ngsp-btn-white');
							$(this).addClass('ngsp-btn-blue');
						});
						// fornac events
						$('#fornac-control-center').bind('click',fornacDisplay.center);
						$('#fornac-control-fullscreen').bind('click',fornacDisplay.requestFullscreen);
						$('#fornac-control-toggle-numbering').bind('click',fornacDisplay.toggleNumbering);
						$('#fornac-control-toggle-label').bind('click',fornacDisplay.toggleNodeLabel);
						$('#fornac-control-toggle-outline').bind('click',fornacDisplay.toggleNodeOutline);
						$('#fornac-control-toggle-animate').bind('click',fornacDisplay.toggleAnimation);
						$('#fornac-control-choice-color-sequence').bind('click',{color : 'sequence'},fornacDisplay.changeColor);
						$('#fornac-control-choice-color-structure').bind('click',{color : 'structure'},fornacDisplay.changeColor);
						$('#fornac-control-choice-color-position').bind('click',{color : 'positions'},fornacDisplay.changeColor);
						$('#fornac-control-export-svg').bind('click',fornacDisplay.exportSvg);
						$('#fornac-control-export-png').bind('click',fornacDisplay.exportPng);
						// activate buttons
						$('#fornac-control-toggle-numbering').removeClass('ngsp-btn-white').addClass('ngsp-btn-blue');
						$('#fornac-control-toggle-outline').removeClass('ngsp-btn-white').addClass('ngsp-btn-blue');
						$('#fornac-control-choice-color-structure').removeClass('ngsp-btn-white').addClass('ngsp-btn-blue');
					});
				});
			} else {
				$("#forna").html(fornaCode);
			}
		}
		var getStructInfo = function(data, activestruct) {
			var struct = data.structure_structure1, freenrj = data.structure_free_energy1;
			if (activestruct == 2) {
				struct = data.structure_structure2;
				freenrj = data.structure_free_energy2;
			}

			var loops = struct.match(/\(\.+\)/g) || [], maxLoop = Math.max
					.apply(Math, $.map(loops, function(el) {
						return el.length
					})) - 2, bulges = struct.match(/\(\.+\(|\)\.+\)/g) || [], maxBulge = Math.max
					.apply(Math, $.map(bulges, function(el) {
						return el.length
					})) - 2;

			$("#struct-info-freenrj").fadeTo("slow", 0.33, function() {
				$(this).html(freenrj);
				$(this).fadeTo("slow", 1);
			});
			$("#struct-info-paired").fadeTo("slow", 0.33, function() {
				$(this).html((struct.match(/[\(\)]/g) || []).length);
				$(this).fadeTo("slow", 1);
			});
			$("#struct-info-unpaired").fadeTo("slow", 0.33, function() {
				$(this).html((struct.match(/\./g) || []).length);
				$(this).fadeTo("slow", 1);
			});
			$("#struct-info-loop").fadeTo("slow", 0.33, function() {
				$(this).html(loops.length);
				$(this).fadeTo("slow", 1);
			});
			$("#struct-info-maxloop").fadeTo("slow", 0.33, function() {
				$(this).html(maxLoop + 'bp');
				$(this).fadeTo("slow", 1);
			});
			$("#struct-info-bulge").fadeTo("slow", 0.33, function() {
				$(this).html(bulges.length);
				$(this).fadeTo("slow", 1);
			});
			$("#struct-info-maxbulge").fadeTo("slow", 0.33, function() {
				$(this).html(maxBulge + 'bp');
				$(this).fadeTo("slow", 1);
			});
			$("#struct-info-singlebulge").fadeTo("slow", 0.33, function() {
				$(this).html((struct.match(/\(\.\(|\)\.\)/g) || []).length);
				$(this).fadeTo("slow", 1);
			});
			$("#struct-info-junction").fadeTo("slow", 0.33, function() {
				$(this).html((struct.match(/\)\.*\(/g) || []).length);
				$(this).fadeTo("slow", 1);
			});
		}

		var _initGeneralInfo = function(data) {
			/*
			 * if (data.strand == "-1"){ data.dna_sequence =
			 * jquery.bio.revCompSeq(data.dna_sequence); if
			 * (data.structure_structure1 != ""){ data.structure_structure1 =
			 * data.structure_structure1.split("").reverse().join("").replace(/\(/g,
			 * "&").replace(/\)/g, "(").replace(/\&/g, ")"); } if
			 * (data.structure_structure2 != ""){ data.structure_structure2 =
			 * data.structure_structure2.split("").reverse().join("").replace(/\(/g,
			 * "&").replace(/\)/g, "(").replace(/\&/g, ")"); } }
			 */
			$("#locus-delete-add-btn")
					.click(
							function() {
								ngspipelines.utils
										.updateFavoriteStatus(
												[ opts.id ],
												"favorite_locus",
												opts.analyses,
												function(isFavorite) {
													if (isFavorite) {
														$(
																"#locus-delete-add-btn")
																.html(
																		"<i class='icon-star'></i> Delete from favorites");
														$("#favorite-star")
																.html(
																		"<i class='icon-star'></i>");
													} else {
														$(
																"#locus-delete-add-btn")
																.html(
																		"<i class='icon-star-empty'></i> Add to favorites");
														$("#favorite-star")
																.html("");
													}
												},
												function(errorMsg) {
													ngspipelines.ui.display(
															"error", errorMsg,
															$("#report-error"));
												});
							});

			if (data.isFavorite) {
				$("#locus-delete-add-btn").html(
						"<i class='icon-star'></i> Delete from favorites");
				$("#favorite-star").html("<i class='icon-star'></i>");
			} else {
				$("#locus-delete-add-btn").html(
						"<i class='icon-star-empty'></i> Add to favorites");
				$("#favorite-star").html("");
			}

			$("#locus-export-sequence")
					.click(
							function() {
								$("#forna").hide();
								ngspipelines.srnaseq.locusreport
										.getExportLocusSequenceLink(
												opts.id,
												opts.dataset,
												function(data) {
													$dialog = $(
															'#ngspipelines-dialog')
															.dialog(
																	{
																		draggable : true,
																		resizable : false,
																		modal : true,
																		autoOpen : false,
																		width : 600,
																		position : [
																				'center',
																				20 ],
																		title : "Locus sequence",
																		buttons : {
																			"close" : function() {
																				$(
																						this)
																						.dialog(
																								"close");
																			}
																		},
																		close : function(
																				event,
																				ui) {
																			$(
																					"#forna")
																					.show();
																		}
																	});
													$
															.get(
																	"/ngspipelines/data/"
																			+ data.fasta_file_path,
																	function(
																			seq) {
																		$(
																				'#ngspipelines-dialog-body')
																				.html(
																						' <textarea readonly style="resize:none;width:99%;height:400px;font-family:monospace;font-size:90%">'
																								+ seq
																								+ '</textarea>'
																								+ '</div>');
																	});
													$dialog.dialog('open');
												},
												function(errorMsg) {
													ngspipelines.ui.display(
															"error", errorMsg,
															$("#report-error"));
												});
							});

			if (data.best_hit_database != "") {
				$("#locus-export-annotations")
						.click(
								function() {
									$("#forna").hide();

									ngspipelines.srnaseq.locusreport
											.getExportLocusAnnotationsLink(
													opts.id,
													"scaffold_14",
													opts.analyses,
													function(data) {
														$dialog = $(
																'#ngspipelines-dialog')
																.dialog(
																		{
																			draggable : true,
																			resizable : false,
																			modal : true,
																			autoOpen : false,
																			width : 950,
																			position : [
																					'center',
																					20 ],
																			title : "Locus annotations",
																			buttons : {
																				"close" : function() {
																					$(
																							this)
																							.dialog(
																									"close");
																				}
																			},
																			close : function(
																					event,
																					ui) {
																				$(
																						"#forna")
																						.show();
																			}
																		});
														$
																.get(
																		"/ngspipelines/data/"
																				+ data.annotation_file_path,
																		function(
																				annot) {
																			$(
																					'#ngspipelines-dialog-body')
																					.html(
																							' <textarea readonly style="resize:none;width:99%;height:400px;font-family:monospace;font-size:90%">'
																									+ annot
																									+ '</textarea>'
																									+ '</div>');
																		});
														$dialog.dialog('open');
													},
													function(errorMsg) {
														ngspipelines.ui
																.display(
																		"error",
																		errorMsg,
																		$("#report-error"));
													});
								});
			} else {
				$("#locus-export-annotations").css("display", "none");
				// $("#locus-export-annotations").children().attr("disabled",
				// true);
			}

			var seqComp = jquery.bio.compSeq(data.dna_sequence);
			var seqCompGC = Math.round((seqComp[2] + seqComp[3]) / data.length
					* 10000) / 100;

			// Generates highchart

			var compseqGraph = new Highcharts.Chart(
					{
						chart : {
							renderTo : 'locus-compseq-graph',
							plotBackgroundColor : null,
							plotBorderWidth : 0,
							width : 360,
							height : 180,
							margin : [ -60, -25, -70, -25 ]
						},
						title : {
							text : 'Seq. composition<br/>(GC%: <b>' + seqCompGC
									+ '</b>)',
							align : 'center',
							verticalAlign : 'middle',
							y : 50,
							style : {
								"font-size" : "13px"
							}
						},
						credits : false,
						tooltip : {
							formatter : function() {
								return '<b>' + this.key + '</b>: ' + this.y;
							}
						},
						plotOptions : {
							pie : {
								dataLabels : {
									enabled : true,
									distance : -40,
									style : {
										fontWeight : 'bold',
										color : 'white',
										textShadow : '0px 1px 2px black'
									},
									format : '{point.name}: <b>{point.percentage:.1f}%</b>'
								},
								startAngle : -90,
								endAngle : 90,
								center : [ '50%', '75%' ]
							}
						},
						series : [ {
							type : 'pie',
							name : 'Sequence composition',
							innerSize : '50%',
							data : [ [ 'A', seqComp[0] ], [ 'T', seqComp[1] ],
									[ 'C', seqComp[2] ], [ 'G', seqComp[3] ],
									[ 'Other', seqComp[4] ] ]
						} ]
					});

			var activestruct = 1;
			// getVarna(data, activestruct);
			getForna(data, activestruct);
			getStructInfo(data, activestruct);
			displaySeqView(data, false, activestruct);
			$(".struct2").fadeTo("slow", 0.33);

			// from to
			var formValid = [ true, true ], errorMessage = "";
			$('#srnaseq-extract-from')
					.focusout(
							function() {
								if (!ngspipelines.utils.regex($(
										'#srnaseq-extract-from').val(),
										/^\d+$/i)) {
									errorMessage = "Unexpected value for 'from', must be a positive integer.";
									formValid[0] = false;
								} else if (parseInt($('#srnaseq-extract-to')
										.val()) < parseInt($(
										'#srnaseq-extract-from').val())) {
									errorMessage = "'to' value must be higher than 'from'.";
									formValid[0] = false;
								} else {
									formValid[0] = true;
								}

								if (formValid[0] && formValid[1]) {
									$("#report-srnaseq-tab-error").fadeOut(800);
									$('#srnaseq-search-nuc').attr("disabled",
											"");
									displaySeqView(data, true, activestruct);
								} else {
									$('#srnaseq-search-nuc').attr("disabled",
											"disabled");
									ngspipelines.ui.display("error",
											errorMessage,
											$("#report-srnaseq-tab-error"));
								}
							});

			$('#srnaseq-extract-to')
					.focusout(
							function() {
								if (!ngspipelines.utils.regex($(
										'#srnaseq-extract-to').val(), /^\d+$/i)) {
									errorMessage = "Unexpected value for 'to', must be a positive integer.";
									formValid[1] = false;
								} else if (parseInt($('#srnaseq-extract-to')
										.val()) > data.length) {
									errorMessage = "'to' value cannot be higher than locus length ("
											+ data.length + ").";
									formValid[1] = false;
								} else if (parseInt($('#srnaseq-extract-to')
										.val()) < parseInt($(
										'#srnaseq-extract-from').val())) {
									errorMessage = "'to' value must be higher than 'from'.";
									formValid[1] = false;
								} else {
									formValid[1] = true;
								}

								if (formValid[0] && formValid[1]) {
									$("#report-srnaseq-tab-error").fadeOut(800);
									$('#srnaseq-search-nuc').attr("disabled",
											"");
									displaySeqView(data, true, activestruct);
								} else {
									$('#srnaseq-search-nuc').attr("disabled",
											"disabled");
									ngspipelines.ui.display("error",
											errorMessage,
											$("#report-srnaseq-tab-error"));
								}
							});

			$('#srnaseq-search-nuc')
					.keyup(
							function() {
								if (formValid[0] && formValid[1]) {
									if ($('#srnaseq-search-nuc').val().length >= 1
											&& !/^[CAGTNncagt]+$/.test($(
													'#srnaseq-search-nuc')
													.val())) {
										displaySeqView(data, false,
												activestruct);
										ngspipelines.ui
												.display(
														"warning",
														"Character not allowed (ATGCN).",
														$("#report-srnaseq-tab-error"));
									} else if ($('#srnaseq-search-nuc').val().length > 2) {
										displaySeqView(data, false,
												activestruct);
										displaySeqView(data, true, activestruct);
									} else if ($('#srnaseq-search-nuc').val().length >= 1) {
										displaySeqView(data, false,
												activestruct);
										ngspipelines.ui
												.display(
														"warning",
														"Please enter more than 2 nucleotides.",
														$("#report-srnaseq-tab-error"));
									} else {
										$("#report-srnaseq-tab-error").fadeOut(
												800);
										displaySeqView(data, false,
												activestruct);
									}
								}
							});

			$('#srnaseq-reset-btn').click(function() {
				$('#srnaseq-extract-from').val(1);
				$('#srnaseq-extract-to').val(data.length);
				$('#srnaseq-search-nuc').val("");
				$('#srnaseq-search-nuc').attr("disabled", "");
				$("#report-srnaseq-tab-error").fadeOut(800);
				displaySeqView(data, false, activestruct);
				formValid = [ true, true ];
			});

			$('#view-struct').click(
					function() {
						if (activestruct == 1) {
							$(".struct1").fadeTo("slow", 0.33);
							$(".struct2").fadeTo("slow", 1);
							$(this).css("margin-top",
									parseInt($(this).css("margin-top")) - 16);
							activestruct = 2;
							getStructInfo(data, activestruct);
							// getVarna(data, activestruct);
							getForna(data, activestruct);
						} else {
							$(".struct1").fadeTo("slow", 1);
							$(".struct2").fadeTo("slow", 0.33);
							$(this).css("margin-top",
									parseInt($(this).css("margin-top")) + 16);
							activestruct = 1;
							getStructInfo(data, activestruct);
							// getVarna(data, activestruct);
							getForna(data, activestruct);
						}
					});

			ngspipelines.ui.wait(
					"Please wait until annotations are being loaded ...",
					"center", $("#locus-features"));
			ngspipelines.srnaseq.locusreport
					.getLocusAnnotations(
							opts.id,
							opts.dataset,
							function(annotations, hrefs) {
								$("#locus-features").html("");
								ngspipelines.utils
										.tmpl(
												$("#locus-features"),
												_featuresTableTemplate,
												{
													annotations : annotations,
													hrefs : hrefs
												},
												function() {
													// features table actions
													// and initialization
													var featuredatatable = $(
															'#features-table')
															.dataTable(
																	{
																		"aaSorting" : [ [
																				1,
																				"desc" ] ],
																		"sPaginationType" : "full_numbers",
																		"sScrollX" : "100%",
																		"sScrollXInner" : "150%",
																		"bScrollCollapse" : true
																	});
													new FixedColumns(
															featuredatatable,
															{
																"iLeftColumns" : 1
															});
												});
							},
							function(errorMsg) {
								$("#locus-features").html("<p>No features</p>");
							});

			ngspipelines.ui.wait(
					"Please wait until isoforms are being loaded ...",
					"center", $("#locus-isoforms"));
			ngspipelines.srnaseq.locusreport
					.getLocusIsoforms(
							opts.id,
							opts.dataset,
							function(isoforms) {
								$("#locus-isoforms").html("");
								for ( var i in isoforms) {
									if (isoforms[i]["isoform_name"] != undefined) {
										isoforms[i]["nb_samples"] = isoforms[i]["isoform_name"]
												.split('#')[1]
										isoforms[i]["expression"] = isoforms[i]["isoform_name"]
												.split('#')[2]
										isoforms[i]["isoform_name"] = isoforms[i]["isoform_name"]
												.split('#')[0]
									}
								}
								ngspipelines.utils
										.tmpl(
												$("#locus-isoforms"),
												_isoformsTableTemplate,
												{
													isoforms : isoforms
												},
												function() {
													// features table actions
													// and initialization
													var isoformsdatatable = $(
															'#isoforms-table')
															.dataTable(
																	{
																		"aaSorting" : [ [
																				1,
																				"desc" ] ],
																		"sPaginationType" : "full_numbers",
																	// "sScrollX":
																	// "100%",
																	// "sScrollXInner":
																	// "150%",
																	// "bScrollCollapse":
																	// true
																	});
													new FixedColumns(
															isoformsdatatable,
															{
																"iLeftColumns" : 1
															});
												});
							},
							function(errorMsg) {
								$("#locus-isoforms").html("<p>No isoforms</p>");
							});

		}

		var _initLocusView = function($t) {
			ngspipelines.srnaseq.locusreport
					.getDepthInfo(
							opts.id,
							opts.dataset,
							function(locus) {
								// Sort loci by isoform_nb_reads
								function sortByKey(array, key) {
									return array.sort(function(a, b) {
										var x = parseInt(a[key]);
										var y = parseInt(b[key]);
										return ((x > y) ? -1
												: ((x < y) ? 1 : 0));
									});
								}
								locus = sortByKey(locus, 'isoform_nb_reads');
								var h_seq = {};
								var h_nuc = {};
								var h_expr = {};
								// var h_strand = {};
								var h_annot = {}
								// h_strand['3']=-20;
								// h_strand['5']=-1665;
								var tab_freq = new Array();
								h_seq['seq. consensus'] = new Array();
								h_seq['seq. structure'] = new Array();
								h_expr['maj'] = new Array();

								// Manage locus informations by strand

								if (locus[0].strand == "-1") {
									// h_strand['3']=-1665;
									// h_strand['5']=-20;

									for (var i = 0; i < locus.length; i++) {
										isoform_length = locus[i].isoform_sequence.length
										locus[i].isoform_start = parseInt(locus[i].start)
												+ (parseInt(locus[i].stop) - parseInt(locus[i].isoform_stop));
										locus[i].isoform_stop = parseInt(locus[i].isoform_start)
												+ isoform_length - 1;
										prediction_length = parseInt(locus[i].prediction_stop)
												- parseInt(locus[i].prediction_start)
												+ 1
										locus[i].prediction_start = parseInt(locus[i].start)
												+ (parseInt(locus[i].stop) - parseInt(locus[i].prediction_stop));
										locus[i].prediction_stop = parseInt(locus[i].prediction_start)
												+ prediction_length - 1;
									}
								}

								var tmpSeqArr = locus[0].dna_sequence.split('');
								var tmpStrArr = new Array();
								tmpStrArr = locus[0].structure_structure2
										.split("");

								// Stores consensus and structure series
								for (var i = 0; i < tmpSeqArr.length; i++) {
									h_seq['seq. consensus'].push(""
											+ tmpSeqArr[i] + "");
									if (tmpStrArr.length == 0) {
										h_seq['seq. structure'].push(" ");
									} else {
										h_seq['seq. structure'].push(""
												+ tmpStrArr[i] + "");
									}
									h_expr['maj'].push(null);
									tab_freq.push(0);
								}

								var mature_nb_reads = 0;
								var maj_seq_name = "";
								for (var i = 0; i < locus.length; i++) {
									if ((locus[i].isoform_start == locus[i].prediction_start)
											&& (locus[i].isoform_stop == locus[i].prediction_stop)) {
										mature_nb_reads = locus[i].isoform_nb_reads;
									}
								}

								// Stores expression of majoritary sequence
								for (var i = (parseInt(locus[0].prediction_start) - parseInt(locus[0].start)); i <= (parseInt(locus[0].prediction_stop) - parseInt(locus[0].start)); i++) {
									h_expr['maj'][i] += parseInt(mature_nb_reads);
								}
								var h_aln = {};
								// var h_nm = {};
								var h_nuc_displayed = {};
								h_aln['seq. consensus'] = [
										locus[0].chromosome,
										parseInt(locus[0].start),
										parseInt(locus[0].stop) ];
								var foldScore = parseInt(locus[0].structure_free_energy1);
								var h_expLevel = {};
								var h_expIn = {};
								var tab_series = [];
								var h_seq_too_long = {};
								cpt = 2;
								nb_data = 0;

								// Determines how many isoforms to display
								var nb_series;
								if (locus.length < 40) {
									nb_series = locus.length;
								} else {
									nb_series = 40;
									ngspipelines.ui
											.display(
													"warning",
													"Only the "
															+ nb_series
															+ " most expressed over the "
															+ locus.length
															+ " isoforms are displayed!",
													$("#locus-view-error"));
								}

								for ( var iso in locus) {
									if (iso != 'remove') {
										var n = locus[iso].isoform_name
												.split('#')[0]
												+ "_"
												+ locus[iso].isoform_start;
										if ((locus[iso].isoform_start == locus[iso].prediction_start)
												&& (locus[iso].isoform_stop == locus[iso].prediction_stop)) {
											maj_seq_name = n;
										}
										// Stores values for each isoform
										h_expLevel[n] = locus[iso].isoform_nb_reads;
										h_expIn[n] = parseInt(locus[iso].isoform_name
												.split('#')[1]);
										// h_nm[n] = locus[iso].isoform_nm;
										h_aln[n] = [
												locus[iso].chromosome,
												parseInt(locus[iso].isoform_start),
												parseInt(locus[iso].isoform_stop) ];

										var seqtab = locus[0].dna_sequence
												.substring(
														h_aln[n][1]
																- h_aln['seq. consensus'][1],
														h_aln[n][1]
																- h_aln['seq. consensus'][1]
																+ h_aln[n][2]
																- h_aln[n][1]
																+ 1);
										seqtab = locus[iso].isoform_sequence
												.substring(
														h_aln[n][1],
														h_aln[n][1]
																- Math
																		.min(
																				h_aln[n][1],
																				h_aln['seq. consensus'][2]));
										h_seq[n] = new Array();
										h_nuc[n] = new Array();
										if (locus[iso].isoform_annotations.length > 0) {
											h_annot[n] = locus[iso].isoform_annotations;
										}
										h_seq_too_long[n] = false;
										if (locus[iso].isoform_start < locus[iso].start
												|| locus[iso].isoform_stop > locus[iso].stop) {
											h_seq_too_long[n] += true;
										}
										// Initialisation of hashes
										for (var i = 0; i < h_aln[n][1]
												- h_aln['seq. consensus'][1]; i++) {
											h_seq[n].push("");
											h_nuc[n].push(null);
										}
										// Stores bases for each isoform
										// Initialisation
										for (var i = 0; i < seqtab.length; i++) {
											h_nuc[n].push(cpt);
										}
										// Stores bases
										for (var i = 0; i < seqtab.length; i++) {
											h_seq[n].push("" + seqtab.charAt(i)
													+ "");
										}
										// Stores frequencies
										for (var i = h_aln[n][1]
												- h_aln['seq. consensus'][1]; i <= h_aln[n][2]
												- h_aln['seq. consensus'][1]; i++) {
											tab_freq[i] += parseInt(locus[iso].isoform_nb_reads);
										}
										tab_freq = tab_freq.slice(0,
												parseInt(locus[iso]["length"]));
										cpt += 1;
										nb_data += 1;
										if (nb_data <= nb_series) {
											tab_series.push({
												name : n,
												data : h_nuc[n],
												yAxis : 1,
												color : '#FFFFFF'
											});
										}
									}
								}

								var seqCons = new Array();
								var seqStruct = new Array();
								// Initialises series of consensus and structure
								for (var i = 0; i < h_aln['seq. consensus'][2]
										- h_aln['seq. consensus'][1] + 1; i++) {
									seqCons.push(0);
									seqStruct.push(1);
								}

								tab_series.push({
									name : 'freq',
									data : tab_freq,
									dataLabels : {
										enabled : false
									},
									type : 'area',
									lineWidth : 0.8,
									color : "#2f7ed8"
								});
								tab_series.push({
									name : 'freqmaj2',
									data : h_expr['maj'],
									dataLabels : {
										enabled : false
									},
									color : '#4075A4',
									type : 'area',
									lineWidth : 0
								});
								tab_series.push({
									name : 'seq. structure',
									data : seqStruct,
									yAxis : 1,
									dataLabels : {
										color : '#f28f43'
									},
									color : '#FFFFFF'
								});
								tab_series.push({
									name : 'seq. consensus',
									data : seqCons,
									yAxis : 1,
									dataLabels : {
										color : '#2f7ed8'
									},
									color : '#FFFFFF'
								});
								// Displaying options
								h_display = {};
								h_display['chart_height'] = Math.max(
										nb_series * 35, 600);
								h_display['xAxis_offset'] = 240 - h_display['chart_height'];
								h_display['yAxis_categories'] = [ 'Cons.',
										'Struc.' ];
								for (var i = 1; i <= nb_series; i++) {
									h_display['yAxis_categories'].push(i);
								}
								h_display['yAxis_categories_height'] = h_display['chart_height'] - 300;
								h_display['yAxis_expression_offset'] = 40;
								h_display['yAxis_sequences_offset'] = 40;

								var start_prediction = locus[0].prediction_start
										- locus[0].start;
								var stop_prediction = locus[0].prediction_stop
										- locus[0].start + 1;

								var strand = "Forward";
								if (locus[0].strand == -1) {
									strand = "Reverse";
								}

								// Generates highchart
								var all_displayed = true;
								var disable_tooltip = false;
								var highchart_data = {
									exporting : {
										buttons : {
											annotButton : {
												text : "View annotated only",
												align : 'right',
												hoverSymbolFill : '#779ABF',
												onclick : function() {
													if (all_displayed == true) {
														this.exportSVGElements[2]
																.attr({
																	text : 'View all isoforms'
																});
														all_displayed = false;
														var opt = this.options;
														for (i = 0; i < this.series.length - 4; i++) {
															if (this.series[i].name in h_annot) {
																var opt = this.series[i].options;
																this.series[i].options.dataLabels.enabled = true;
																this.series[i]
																		.update(opt);
															} else {
																var opt = this.series[i].options;
																this.series[i].options.dataLabels.enabled = false;
																this.series[i]
																		.update(opt);
															}
														}
													} else {
														var opt = this.options;
														for (i = 0; i < this.series.length - 4; i++) {
															var opt = this.series[i].options;
															this.series[i].options.dataLabels.enabled = true;
															this.series[i]
																	.update(opt);
														}
														this.exportSVGElements[2]
																.attr({
																	text : 'View annotated only'
																});
														all_displayed = true;
													}
												}
											},
											tooltipButton : {
												text : "Disable tooltips",
												align : 'right',
												symbolFill : '#B5C9DF',
												hoverSymbolFill : '#779ABF',
												onclick : function() {
													if (disable_tooltip == true) {
														this.options.tooltip.formatter = function() {
															var res = "";
															if (!this.series.name
																	.indexOf('seq')) {
																var expI = h_expIn[this.series.name];
																var expL = h_expLevel[this.series.name];
																var len = h_seq[this.series.name]
																		.filter(function(
																				i) {
																			return i !== '';
																		}).length;
																res = '<div style="text-align:right"><b> - '
																		+ this.series.name
																				.split("_")[0]
																		+ ' - </b></div>';
																res += '<i>Position on ref:</i> <b>'
																		+ (parseInt(h_aln["seq. consensus"][1])
																				+ parseInt(this.x) - 1)
																		+ '</b>';
																res += '<br><i>Position on locus:</i> <b>'
																		+ (this.x)
																		+ '</b>';
																res += '<br><i>Length:</i> <b>'
																		+ len
																		+ '</b> bp';
																if (typeof expL != 'undefined') {
																	res += '<br><i>Expression level:</i> <b>'
																			+ expL
																			+ '</b>';
																	res += '<br><i>Expressed in:</i> <b>'
																			+ expI
																			+ ' </b> sample(s)';
																}
																if (typeof h_aln[this.series.name] != 'undefined') {
																	var ref = h_aln[this.series.name][0];
																	var start = h_aln[this.series.name][1];
																	var end = h_aln[this.series.name][2];
																	res += '<br><i>Reference:</i> <b>'
																			+ ref
																			+ '</b>';
																	res += '<br><i>Ref. start-end:</i> <b>'
																			+ start
																			+ '-'
																			+ end
																			+ ' </b>';
																} else {
																	res += '<br><i>Fold score:</i> <b>'
																			+ foldScore
																			+ '</b>';
																}
															} else {
																res = '<i>Position on ref:</i> <b>'
																		+ (parseInt(h_aln["seq. consensus"][1])
																				+ parseInt(this.x) - 1)
																		+ '</b>';
																res += '<br><i>Position on locus:</i> <b>'
																		+ (this.x)
																		+ '</b>';
																res += '<br><i>Expression level:</i> <b>'
																		+ this.y
																		+ '</b>';
															}
															try {
																if (h_annot[this.series.name].length > 0) {
																	var tab = h_annot[this.series.name]
																			.split(",");
																	for (i = 0; i < tab.length; i++) {
																		var infos = tab[i]
																				.split(":");
																		var database = infos[0];
																		var accession = infos[1];
																		var family = infos[2];
																		var displayed_infos = database
																				+ ":"
																				+ family;
																		if (family == "None") {
																			displayed_infos = database;
																		}
																		res += '<br><i>Annotation'
																				+ parseInt(i + 1)
																				+ ':</i> <b>'
																				+ displayed_infos
																				+ '</b>';
																	}
																}
															} catch (err) {
															}
															return res;
														}
														this.exportSVGElements[4]
																.attr({
																	text : 'Disable tooltips'
																});
														disable_tooltip = false;
													} else {
														this.options.tooltip.formatter = function() {
															return false
														}
														this.exportSVGElements[4]
																.attr({
																	text : 'Enable tooltips'
																});
														disable_tooltip = true;
													}
												}
											},
											contextButton : {
												enabled : true
											}
										}
									},
									chart : {
										renderTo : 'locus-view-graph',
										type : 'line',
										zoomType : 'x',
										spacingBottom : 8,
										height : h_display['chart_height']
									},
									title : {
										text : locus[0].name
									},
									credits : false,
									legend : {
										enabled : false
									},
									subtitle : {
										text : locus[0].chromosome + ":"
												+ locus[0].start + "-"
												+ locus[0].stop + " - "
												+ strand + " - " + locus.length
												+ " isoforms"
									},
									xAxis : {
										labels : {
											step : 2
										},
										tickmarkPlacement : 'on',
										title : {
											enabled : false
										},
										offset : h_display['xAxis_offset'],
										min : -1,
										gridLineWidth : 1,
										gridLineDashStyle : 'dot',
										minRange : 10
									},
									yAxis : [
											{
												height : 160,
												title : {
													text : 'Expression',
													offset : h_display['yAxis_expression_offset'],
													x : -15
												},
												min : 0
											},
											{
												title : {
													text : 'Sequences',
													offset : h_display['yAxis_sequences_offset'],
													x : -15
												},
												labels : {
													formatter : function() {
														if (this.value != -1) {
															if (parseInt(this.value) > 0) {
																return h_expLevel[this.chart.series[parseInt(this.value) - 1].name];
															} else {
																return this.value;
															}
														}
													}
												},
												categories : h_display['yAxis_categories'],
												top : 260,
												height : h_display['yAxis_categories_height'],
												offset : 0,
												lineWidth : 2,
												reversed : true,
												min : -1
											} ],
									tooltip : {
										enabled : true,
										useHTML : true,
										crosshairs : true,
										formatter : function() {
											var is_annot = false;
											try {
												if (h_annot[this.series.name].length > 0) {
													is_annot = true;
												}
											} catch (err) {

											}

											var res = "";
											if (!this.series.name
													.indexOf('seq')) {
												var expI = h_expIn[this.series.name];
												var expL = h_expLevel[this.series.name];
												var len = h_seq[this.series.name]
														.filter(function(i) {
															return i !== '';
														}).length;

												res = '<div style="text-align:right"><b> - '
														+ this.series.name
																.split("_")[0]
														+ ' - </b></div>';
												res += '<i>Position on ref:</i> <b>'
														+ (parseInt(h_aln["seq. consensus"][1])
																+ parseInt(this.x) - 1)
														+ '</b>';
												res += '<br><i>Position on locus:</i> <b>'
														+ (this.x) + '</b>';
												res += '<br><i>Length:</i> <b>'
														+ len + '</b> bp';
												if (typeof expL != 'undefined') {
													res += '<br><i>Expression level:</i> <b>'
															+ expL + '</b>';
													res += '<br><i>Expressed in:</i> <b>'
															+ expI
															+ ' </b> sample(s)';
												}
												if (typeof h_aln[this.series.name] != 'undefined') {
													var ref = h_aln[this.series.name][0];
													var start = h_aln[this.series.name][1];
													var end = h_aln[this.series.name][2];
													res += '<br><i>Reference:</i> <b>'
															+ ref + '</b>';
													res += '<br><i>Ref. start-end:</i> <b>'
															+ start
															+ '-'
															+ end
															+ ' </b>';
												} else {
													res += '<br><i>Fold score:</i> <b>'
															+ foldScore
															+ '</b>';
												}
											} else {
												res = '<i>Position on ref:</i> <b>'
														+ (parseInt(h_aln["seq. consensus"][1])
																+ parseInt(this.x) - 1)
														+ '</b>';
												res += '<br><i>Position on locus:</i> <b>'
														+ (this.x) + '</b>';
												res += '<br><i>Expression level:</i> <b>'
														+ this.y + '</b>';
											}
											try {
												if (h_annot[this.series.name].length > 0) {
													var tab = h_annot[this.series.name]
															.split(",");
													for (i = 0; i < tab.length; i++) {
														var infos = tab[i]
																.split(":");
														var database = infos[0];
														var accession = infos[1];
														var family = infos[2];
														var displayed_infos = database
																+ ":" + family;
														if (family == "None") {
															displayed_infos = database;
														}
														res += '<br><i>Annotation'
																+ parseInt(i + 1)
																+ ':</i> <b>'
																+ displayed_infos
																+ '</b>';
													}
												}

											} catch (err) {

											}
											return res;
										}
									},
									plotOptions : {
										area : {
											pointStart : 1,
											fillOpacity : 0.5,
											marker : {
												radius : 0,
												symbol : 'circle'
											}
										},
										line : {
											pointStart : 1,
											dataLabels : {
												enabled : true,
												y : 9,
												x : -1,
												formatter : function() {
													var currBase = h_seq[this.series.name][this.x - 1];
													var consBase = h_seq['seq. consensus'][this.x - 1];
													if (this.series.name == 'seq. consensus') {
														if (this.x - 1 >= start_prediction
																&& this.x - 1 < stop_prediction) {
															return '<b>'
																	+ currBase
																	+ '</b>';
														} else {
															return currBase;
														}
													} else if (this.series.name != 'seq. structure') {
														if (h_seq_too_long[this.series.name]) {
															if (currBase === consBase) {
																return '<span style="color:green">'
																		+ currBase
																		+ '<span style="color:green">';
															} else {
																return '<span style="color:red">'
																		+ currBase
																		+ '</span>';
															}
														} else {
															if (this.series.name == maj_seq_name) {
																if (currBase === consBase) {
																	return '<span style="font-weight:bold">'
																			+ currBase
																			+ '</span>';
																} else {
																	return '<span style="color:red;font-weight:bold">'
																			+ currBase
																			+ '</span>';
																}
															} else {
																if (currBase === consBase) {
																	return currBase;
																} else {
																	return '<span style="color:red">'
																			+ currBase
																			+ '</span>';
																}
															}
														}
													} else {
														return currBase;
													}
												}
											},
											enableMouseTracking : true,
											marker : {
												radius : 0,
												symbol : 'circle'
											}
										}
									},
									series : tab_series
								};
								var depthGraph = new Highcharts.Chart(
										highchart_data);

							}, function(errorMsg) {
								ngspipelines.utils.tmpl($t, _template, {
									locus : {}
								},
										function() {
											ngspipelines.ui.display("error",
													errorMsg,
													$("#report-error"),
													withBox = true);
											opts.callback();
										});
							});
		}

		var _initPlugin = function($t) {
			ngspipelines.srnaseq.locusreport
					.getLocus(
							opts.id,
							opts.dataset,
							opts.analyses,
							function(locus) {
								ngspipelines.utils
										.tmpl(
												$t,
												_template,
												{
													locus : locus
												},
												function() {
													var pname = opts.martName
															.split(" ");
													for (var i = 0; i < pname.length - 1; i++) {
														pname[i] = pname[i]
																.charAt(0);
													}
													$("#back")
															.html(
																	"<button class='ngsp-button ngsp-btn-white ngsp-btn-small ngsp-button-back'>"
																			+ "<i class='icon-arrow-left'></i> "
																			+ pname
																					.join(". ")
																			+ "</button>");

													$("#report-tabs").tabs();
													$("#back")
															.click(
																	function() {
																		window.location.href = "/ngspipelines/#!/"
																				+ opts.rootName
																				+ "/"
																				+ opts.martName
																				+ "?actDataset="
																				+ opts.datasetType;
																	});
													_initGeneralInfo(locus);
													$("#report-tabs")
															.bind(
																	"tabsselect",
																	function(
																			event,
																			ui) {
																		var type = $(
																				ui.tab)
																				.attr(
																						"href")
																				.substring(
																						1);
																		if (type == "locusview"
																				&& $
																						.inArray(
																								type,
																								_pluginIsInit) == -1) {
																			_initLocusView();
																			_pluginIsInit
																					.push(type);
																		} else if (type == "depth"
																				&& $
																						.inArray(
																								type,
																								_pluginIsInit) == -1) {
																			_initDepthTable();
																			_pluginIsInit
																					.push(type);
																		}
																	});
													opts.callback();
												});
							}, function(errorMsg) {
								ngspipelines.utils.tmpl($t, _template, {
									locus : {}
								},
										function() {
											ngspipelines.ui.display("error",
													errorMsg,
													$("#report-error"),
													withBox = true);
											opts.callback();
										});
							});
		}

		// Depth View
		var _initDepthTable = function() {
			ngspipelines.ui.wait(
					"Please wait until libraries are being loaded ...",
					"center", $("#locus-depth-table"));
			ngspipelines.ui.wait("Please wait while generating the graph ...",
					"center", $("#locus-depth-graph"));

			ngspipelines.srnaseq.locusreport
					.getLocusLibraries(
							opts.id,
							opts.dataset,
							function(libraries, libraryNames) {
								$("#locus-depth-table").html("");
								ngspipelines.utils
										.tmpl(
												$("#locus-depth-table"),
												_depthTableTemplate,
												{
													libraries : libraries,
													labelColors : opts.labelColors
												},
												function() {

													// Init the table itself
													var depthLibTable = $(
															"#depth-libraries-table")
															.dataTable(
																	{
																		"aaSorting" : [ [
																				2,
																				"asc" ] ],
																		"sPaginationType" : "full_numbers",
																		"aoColumnDefs" : [ {
																			"bSortable" : false,
																			"aTargets" : [
																					0,
																					1 ]
																		} ],
																		"oLanguage" : {
																			"sLengthMenu" : 'Show <select>'
																					+ '<option value="10">10</option>'
																					+ '<option value="25">25</option>'
																					+ '<option value="50">50</option>'
																					+ '<option value="100">100</option>'
																					+ '<option value="-1">All</option>'
																					+ '</select> entries'
																		},
																		"iDisplayLength" : 25,
																		"sDom" : 'lfrtiTp',
																		"oTableTools" : {
																			"sSwfPath" : "js/lib/TableTools-2.1.4/swf/copy_csv_xls_pdf.swf",
																			"aButtons" : [
																					{
																						"sExtends" : "copy",
																						"sButtonText" : "Copy"
																					},
																					{
																						"sExtends" : "csv",
																						"sButtonText" : "Save to CSV",
																						"sTitle" : "depth_view_libraries"
																					} ]
																		}
																	});

													// Define apply table button
													// action
													$("#apply-label")
															.click(
																	function() {
																		if ($(
																				":checked[id^=chk_library_]")
																				.size() == 0) {
																			ngspipelines.ui
																					.display(
																							"error",
																							NO_LIBRARY_SELECTED_ERROR_MSG,
																							$("#depth-locus-error"));
																			$(
																					"#depth-locus-error")
																					.delay(
																							1500)
																					.fadeOut(
																							800);
																		} else {
																			// find
																			// out
																			// which
																			// label
																			// should
																			// be
																			// checked
																			// by
																			// default
																			var libraryCount = new Array(), libMax = "";
																			for ( var index in libraryNames) {
																				if (index != "remove") {
																					libraryCount
																							.push(0);
																				}
																			}
																			$(
																					":checked[id^=chk_library_]")
																					.each(
																							function() {
																								var index = $(
																										this)
																										.attr(
																												"id")
																										.split(
																												"_")
																										.slice(
																												1)
																										.join(
																												"_");
																								libraryCount[libraryNames
																										.indexOf($(
																												"#"
																														+ index)
																												.html())] += 1;
																							});
																			libMax = libraryNames[libraryCount
																					.indexOf(ngspipelines.utils
																							.max(libraryCount))];

																			var labelHTML = '';
																			for ( var index in libraryNames) {
																				if (index != "remove") {
																					labelHTML += '<div class="row-fluid">';
																					if (libMax == libraryNames[index]) {
																						labelHTML += '<div class="span1 offset1"><input checked id="radio_'
																								+ libraryNames[index]
																								+ '_'
																								+ index
																								+ '" type="radio" name="labels" value="'
																								+ libraryNames[index]
																								+ '"></div>';
																					} else {
																						labelHTML += '<div class="span1 offset1"><input id="radio_'
																								+ libraryNames[index]
																								+ '_'
																								+ index
																								+ '" type="radio" name="labels" value="'
																								+ libraryNames[index]
																								+ '"></div>';
																					}
																					labelHTML += '<div class="span2" style="margin-top:-4px;"><input id="input_'
																							+ libraryNames[index]
																							+ '_'
																							+ index
																							+ '" type="text" value="'
																							+ libraryNames[index]
																							+ '"></div>';
																					labelHTML += '<div class="span3 offset3"><span class="label" id="label_'
																							+ libraryNames[index]
																							+ '_'
																							+ index
																							+ '" style="background-color:'
																							+ opts.labelColors[index]
																							+ '">'
																							+ libraryNames[index]
																							+ '</span></div>';
																					labelHTML += '</div>';
																				}
																			}

																			labelHTML += '<div id="add-label-row" class="row-fluid">';
																			labelHTML += '<div class="span2 offset2" style="margin-top:4px;"><input id="add-label-input" type="text" maxlength="15" value="new"></div>';
																			labelHTML += '<div class="span3 offset3" style="margin-top:6px;"><button id="add-label" class="ngsp-button ngsp-btn-white ngsp-btn-small" type="button" ><i class="icon-plus-sign"></i> Add</button></div>';
																			labelHTML += '</div>';
																			$(
																					'#report-dialog-body')
																					.html(
																							labelHTML);

																			var inputChangeHandler = function() {
																				var parts = $(
																						this)
																						.attr(
																								"id")
																						.split(
																								"_"), cLibName = parts
																						.slice(
																								1,
																								-1)
																						.join(
																								"_");
																				$(
																						"#label_"
																								+ cLibName
																								+ "_"
																								+ parts
																										.slice(-1)[0])
																						.html(
																								$(
																										this)
																										.val());
																				$(
																						"#radio_"
																								+ cLibName
																								+ "_"
																								+ parts
																										.slice(-1)[0])
																						.val(
																								$(
																										this)
																										.val());
																			}

																			$(
																					"[id^=input_]")
																					.bind(
																							"change",
																							inputChangeHandler);
																			$(
																					"#add-label")
																					.click(
																							function() {

																								var index = $(
																										"[id^=label_]")
																										.size(), html = '<div class="row-fluid">';

																								html += '<div class="span1 offset1"><input id="radio_'
																										+ $(
																												"#add-label-input")
																												.val()
																										+ '_'
																										+ index
																										+ '" type="radio" name="labels" value="'
																										+ $(
																												"#add-label-input")
																												.val()
																										+ '"></div>';
																								html += '<div class="span2" style="margin-top:-4px;"><input id="input_'
																										+ $(
																												"#add-label-input")
																												.val()
																										+ '_'
																										+ index
																										+ '" type="text" value="'
																										+ $(
																												"#add-label-input")
																												.val()
																										+ '"></div>';
																								html += '<div class="span3 offset3"><span class="label" id="label_'
																										+ $(
																												"#add-label-input")
																												.val()
																										+ '_'
																										+ index
																										+ '" style="background-color:'
																										+ opts.labelColors[index]
																										+ '">'
																										+ $(
																												"#add-label-input")
																												.val()
																										+ '</span></div>';
																								html += '</div>';
																								$(
																										"#add-label-row")
																										.before(
																												html);
																								$(
																										"#input_"
																												+ $(
																														"#add-label-input")
																														.val()
																												+ '_'
																												+ index)
																										.bind(
																												"change",
																												inputChangeHandler);
																								$(
																										"#add-label-input")
																										.val(
																												"new");

																							});
																			$dialog = $(
																					'#report-dialog')
																					.dialog(
																							{
																								draggable : true,
																								resizable : false,
																								modal : true,
																								autoOpen : false,
																								width : 450,
																								height : 500,
																								title : "Label as",
																								buttons : {
																									"Cancel" : function() {
																										$(
																												this)
																												.dialog(
																														'close');
																									},
																									"Apply" : function() {

																										var index = $(
																												'input[name=labels]:checked')
																												.attr(
																														"id")
																												.split(
																														"_")
																												.slice(
																														1)
																												.join(
																														"_"), label = $(
																												'input[name=labels]:checked')
																												.attr(
																														"id")
																												.split(
																														"_")
																												.slice(
																														1,
																														-1)
																												.join(
																														"_"), style = $(
																												"#label_"
																														+ index)
																												.attr(
																														"style");
																										// modify
																										// checked
																										// values
																										$(
																												":checked[id^=chk_library_]")
																												.each(
																														function() {
																															var parts = $(
																																	this)
																																	.attr(
																																			"id")
																																	.split(
																																			"_");
																															$(
																																	"#library_"
																																			+ parts
																																					.slice(-1)[0])
																																	.html(
																																			$(
																																					'input[name=labels]:checked')
																																					.val());
																															$(
																																	"#library_"
																																			+ parts
																																					.slice(-1)[0])
																																	.attr(
																																			"style",
																																			style);
																															$(
																																	this)
																																	.attr(
																																			'checked',
																																			false);
																														});
																										// and
																										// all
																										// library
																										// using
																										// the
																										// same
																										// labels
																										$(
																												"[id^=library_]")
																												.each(
																														function() {
																															if ($(
																																	this)
																																	.html() == label) {
																																$(
																																		this)
																																		.html(
																																				$(
																																						'input[name=labels]:checked')
																																						.val());
																															}
																														});
																										libraryNames = new Array();
																										$(
																												"[id^=label_]")
																												.each(
																														function() {
																															libraryNames
																																	.push($(
																																			this)
																																			.html());
																														});
																										_updateDepthGraph(libraries);
																										$(
																												this)
																												.dialog(
																														'close');
																									}
																								}
																							});
																			$dialog
																					.dialog('open');
																		}
																	});
													$("#meandepth-min")
															.keyup(
																	function() {
																		depthLibTable
																				.fnDraw();
																	});
													$("#meandepth-max")
															.keyup(
																	function() {
																		depthLibTable
																				.fnDraw();
																	});
													$("#nbofseq-min")
															.keyup(
																	function() {
																		depthLibTable
																				.fnDraw();
																	});
													$("#nbofseq-max")
															.keyup(
																	function() {
																		depthLibTable
																				.fnDraw();
																	});
													$("#lib-name")
															.keyup(
																	function() {
																		depthLibTable
																				.fnDraw();
																	});

													$.fn.dataTableExt.afnFiltering
															.push(function(
																	oSettings,
																	aData,
																	iDataIndex) {
																var iMin = $(
																		"#meandepth-min")
																		.val() * 1;
																var iMax = $(
																		"#meandepth-max")
																		.val() * 1;
																var iVersion = aData[5] == "-" ? 0
																		: aData[5] * 1;
																if (iMin == ""
																		&& iMax == "") {
																	return true;
																} else if (iMin == ""
																		&& iVersion < iMax) {
																	return true;
																} else if (iMin < iVersion
																		&& "" == iMax) {
																	return true;
																} else if (iMin < iVersion
																		&& iVersion < iMax) {
																	return true;
																}
																return false;
															});
													$.fn.dataTableExt.afnFiltering
															.push(function(
																	oSettings,
																	aData,
																	iDataIndex) {
																var iMin = $(
																		"#nbofseq-min")
																		.val() * 1;
																var iMax = $(
																		"#nbofseq-max")
																		.val() * 1;
																var iVersion = aData[6] == "-" ? 0
																		: aData[6] * 1;
																if (iMin == ""
																		&& iMax == "") {
																	return true;
																} else if (iMin == ""
																		&& iVersion < iMax) {
																	return true;
																} else if (iMin < iVersion
																		&& "" == iMax) {
																	return true;
																} else if (iMin < iVersion
																		&& iVersion < iMax) {
																	return true;
																}
																return false;
															});
													$.fn.dataTableExt.afnFiltering
															.push(function(
																	oSettings,
																	aData,
																	iDataIndex) {
																var libNames = $(
																		"#lib-name")
																		.val()
																		.split(
																				/\s+/);
																if ($
																		.inArray(
																				aData[2],
																				libNames) == -1) {
																	return true;
																}
																return false;
															});

													$("#depth-graph-redraw")
															.click(
																	function() {
																		_updateDepthGraph(libraries);
																	});

													_updateDepthGraph(libraries);
												});
							},
							function(errorMsg) {
								$("#locus-depth-table").html("");
								ngspipelines.utils
										.tmpl(
												$("#locus-depth-table"),
												_depthTableTemplate,
												{},
												function() {
													ngspipelines.ui
															.display(
																	"error",
																	errorMsg,
																	$("#report-depth-table-error"));
													// Init the table itself
													$("#depth-libraries-table")
															.dataTable(
																	{
																		"aaSorting" : [ [
																				2,
																				"asc" ] ],
																		"iDisplayLength" : 25,
																		"sPaginationType" : "full_numbers",
																		"aoColumnDefs" : [ {
																			"bSortable" : false,
																			"aTargets" : [
																					0,
																					1 ]
																		} ]
																	});
												});
							})
		}

		// Main plugin function
		this.each(function() {
			var $t = $(this);
			$t.html("");
			if (opts.id == parseInt(opts.id, 10)) {
				_initPlugin($t);
			} else {
				ngspipelines.srnaseq.locusreport.getIdFromLocusName(opts.id,
						opts.dataset, function(id) {
							opts.id = id
							_initPlugin($t);
						}, function(errorMsg) {
							ngspipelines.utils.tmpl($t, _template, {
								locus : {}
							}, function() {
								ngspipelines.ui.display("error", errorMsg,
										$("#report-error"), withBox = true);
								opts.callback();
							});
						});
			}
		});
		return this;
	};
})(jQuery);