(function($) {
	$.fn.download = function(options) {
		
		var _template = "js/view/download.tmpl",

	        // Default option values
			defaults = {
	            dataset: {},
	            projectDirectory: ""
	    	};
 
		var opts = $.extend(defaults, options);
		
		this.each(function() {
			
			var $t = $(this);
			ngspipelines.ui.wait("Please wait until files list is being loaded ...", "center", $t, withBox=true);
	    	ngspipelines.download.getListDirectory(
	    		opts.dataset, 
	    		
	    		// Define the ngspipelines.download.getListDirectory success callback function
	    		function(results) {
	    			
	    			// convert size to human readable
	    			for (var application in results) {
	    				if (results.hasOwnProperty(application)) {
	    					var appliData = results[application] ;
	    					for (analysis in appliData){
	    						if (appliData.hasOwnProperty(analysis)) {
	    							var analysisInfo = appliData[analysis];
	    							if (analysisInfo.hasOwnProperty("files")) {
	    								for (var i = 0; i < analysisInfo.files.length; i++) {
	    									var file = analysisInfo.files[i] ;
		    								var humanSize = ngspipelines.utils.getOctetStringRepresentation(file.size.toString());
		    								file.size = humanSize;
	    								}
	    							}
	    						}
	    					}
	    				}
    				}
	    			var tmplJSON = {
	    				data : results,
	    				webPath : "/ngspipelines/data/" + opts.projectDirectory 
	    			}
	    			$t.html("");
	    			ngspipelines.utils.tmpl($t, _template, tmplJSON, function() {
	    				$("[class^=down]").each( function() {
	    					$(this).multiDownloadAdd($(this).attr("class").split('#')[1]);
	    				});
	    				$("[class^=triggerdown]").each( function() {
		    				$(this).multiDownload('click', $(this).attr("class").split('#')[1]);
	    				});
	    					
	    				$('[class^=tooltip]').poshytip({
	    					className: 'tip-twitter',
	    					showTimeout: 1,
	    					alignTo: 'target',
	    					alignX: 'center',
	    					offsetY: 5,
	    					allowTipHover: false
	    				});
	    				
	    				$('[id^=get-all-url]').click(function() {
	    					var mess = "";
	    					var application = $(this).attr("id").split('#')[1];
	    					$("[class^=down#"+application+"]").each( function() {
	    						var a_href = $(this).attr('href');
		    					mess += "http://" + window.location.host + a_href + "<br/>";
	    					});
	    					
	    					$dialog = $('#ngspipelines-dialog').dialog({
	    			            draggable: false,
	    			            resizable: false,
	    			            modal: true,
	    			            autoOpen: false,
	    			            width: 'auto',
	    			            title: "Download links for " + application,
	    			            buttons: {
	    			            	"Cancel": function() {	$(this).dialog('close'); }
	    			            }
	    			        });
	    					
	    			        $('#ngspipelines-dialog-body').html(
	    			        		"<div class='bloc-small-dialog'>" +
	    			        		" <pre>" + mess + "</pre>" +
	    			        		"</div>"
	    			        );
	    			        $dialog.dialog('open');	
	    				});
	    			});
	    		},
	    		
	    		// Define the ngspipelines.download.getListDirectory error callback function
	    		function(errorMsg) {
	    			$t.html("");
	    			ngspipelines.utils.tmpl($t, _template, {}, function() {
	    				ngspipelines.ui.display("error", errorMsg, $("#download-error"), true);
	    			});
	    		}
	    	);
	    	
		});
		return this;
	};
})(jQuery);