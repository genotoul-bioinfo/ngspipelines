(function($) {
	$.fn.application = function(options) {
		
		var _template = "js/view/application/application.tmpl",
						
	        // default option values
			defaults = {
	            dataset: {},
	            libraries: {},
	            analyses: {},
	            projectDirectory: "",
	            martName: "",
	            rootName: "",
	            prefix: ""
	    	};
 
		var opts = $.extend(defaults, options);

		
		this.each(function() {
			var $t = $(this);
			
			ngspipelines.utils.hasLibraries(
				opts.libraries,
				opts.prefix,
				function(hasLibrary) {
					
					ngspipelines.utils.tmpl($t, _template, {prefix: opts.prefix, mainObjectName: opts.dataset.displayName, hasLibrary: hasLibrary}, function(){

						// init the statistics plugin
						$("#" + opts.prefix + "-statistics-wrapper").statistics({
							analyses: opts.analyses,
							graphicsWrapperID: opts.prefix+"-graphics-wrapper",
							graphicsTitleWrapperID: opts.prefix+"-graphics-title-wrapper",
							graphicsSubTitleWrapperID: opts.prefix+"-graphics-subtitle-wrapper",
							prefix: opts.prefix
						});
						
						// retrieve the main table name to initialize plugins
						ngspipelines.application.getMainTableName(opts.dataset, function(mainTable){
							
							// init the libraries plugin
							$("#" + opts.prefix + "-libraries-wrapper").libraries({
					            libraries: opts.libraries,
					            analyses: opts.analyses,
					            projectDirectory: opts.projectDirectory,
					            martName: opts.martName,
					            rootName: opts.rootName,
					            report: mainTable.toLowerCase()+"report",
					            prefix: opts.prefix
							});	

							ngspipelines.application.getFavoritesConfig(
								opts.analyses, 
								"favorite_"+mainTable.toLowerCase(),
								function(table_header, links) {
									// init the favorite plugin
									$("#" + opts.prefix + "-favorite-wrapper").favorites({
										dataset: opts.dataset,
										analyses: opts.analyses,
										rootName: opts.rootName,
										martName: opts.martName,
										prefix: opts.prefix,
										fields: table_header,
										links: links,
										report: mainTable.toLowerCase()+"report",
										analysisType: "favorite_"+mainTable.toLowerCase(),
										mainTableName: mainTable,
										tableHeader: "On the table bellow are listed all your favorite "+mainTable.toLowerCase()+"."
									});
								},
								function(errorMsg) {
									ngspipelines.ui.display("error", errorMsg, $("#" + opts.prefix + "-favorite-wrapper"));
								}
							)

						}, function(errorMsg){
							ngspipelines.ui.display("error", errorMsg, $("#" + opts.prefix + "-favorite-wrapper"));
							ngspipelines.ui.display("error", errorMsg, $("#" + opts.prefix + "-libraries-wrapper"));
						});
											
					});
					
				}
			)

		});
		return this;
	};
})(jQuery);