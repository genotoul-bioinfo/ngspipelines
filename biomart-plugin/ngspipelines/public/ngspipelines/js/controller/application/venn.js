(function($) {
	$.fn.venn = function(options) {
		
		var _template = "js/view/application/venn.tmpl",
		
	        // default option values
			defaults = {
	            countFile: "",
	            pools: {},
				rootName: "",
				martName: "",
				report: "",
				prefix: ""
	    	};
	    
	    // merge default option values and provided parameters 
		var opts = $.extend(defaults, options);
		
		var _updateVenn = function($t,opts,vseries,mainIDs, type, stat){			
			$("#" + opts.prefix + "-venn-container").jvenn({
				series: vseries,
	            fnClickCallback: function() {
	            	var value = "";
	            	if (this.listnames.length == 1) {
	            		value += "Elements only in ";
	            	} else {
	            		value += "Common elements in ";
	            	}
	            	for (name in this.listnames) {
	            		if (name != "remove") {
	            			value += this.listnames[name] + " ";
	            		}
	            	}
	            	value += ":<br />";
	            	for (val in this.list) {
	            		if (val != "remove") {
	            			value += "<a href='/ngspipelines/report.jsp?id=" + mainIDs[this.list[val]] +"&root=" + opts.rootName + "&datasetType=" + opts.prefix + "&plugin=" + opts.report + "&mart=" + opts.martName +"'>" + this.list[val] + "</a><br />";
	            		}
	            	}
	            	$("#" + opts.prefix + "-venn-area").html(value);
	            },
	            searchInput:  $("#" + opts.prefix + "-search-field"),
	            searchMinSize: 1,
	            displayStat: stat,
	            displayMode: type
			});
		};

		// Main plugin function
		this.each(function() {
			var $t = $(this);
			$t.html("");
			ngspipelines.application.venn.loadAndFormatData(opts.pools, opts.countFile, function(vseries, mainIDs) {
				ngspipelines.utils.tmpl($t, _template, {pools: opts.pools, contigs: vseries, prefix: opts.prefix}, 
					function() {
						_updateVenn($t, opts, vseries, mainIDs, "classic");
						$("#" + opts.prefix + "-venn-type").change(function() {
							_updateVenn($t, opts, vseries, mainIDs, $(this).val(), $("#" + opts.prefix + "-view-stats").is(':checked'));
						});	
						$("#" + opts.prefix + "-view-stats").change(function() {
							_updateVenn($t, opts, vseries, mainIDs, $("#" + opts.prefix + "-venn-type").val(), $("#" + opts.prefix + "-view-stats").is(':checked'));
						});
					}
				);
			});
				
				
				
			
			
			
		});
		
		return this;
		
	};
})(jQuery);
