(function($) {

	$.fn.blast = function(options) {
		
		var _template    = "js/view/application/blast.tmpl";
		var _templateres = "js/view/application/blastres.tmpl";
		
        // default option values
		var defaults = {
				dataset: {},
	            analyses: {},
	            rootName: "",
            	martName: "",
            	favoritesID: [],
            	prefix: "",
            	fields: {},
            	links: "",
            	report: "",
            	analysisType: "",
            	mainTableName: "",
            	tableHeader: ""
    	};
		
		// merge default option values and provided parameters 
		var opts = $.extend(defaults, options);
		
	    // Main plugin function
		this.each(function() {
			var $t = $(this);
			$t.html("");
			ngspipelines.utils.tmpl($t, _template, {prefix: opts.prefix}, function() {
				
				$('#blast-run-btn').click( function () {
					// Clear result box if re run 
					$("#blast-result-table").html("");
					$("#blast-result").css("display", "block");
					$("#blast-wait").css("display", "block");
					$("#blast-result-error").css("display", "none");
					
					//check parameters
					
                    //fasta format
                    var formValid=true,
                    errorMessage="";
                    if ($('#blast-fastaseq').val() == "") {
                    	errorMessage="Please provide query sequence(s).";
                    	formValid=false;
                    }
                    else if (! ngspipelines.utils.regex($('#blast-fastaseq').val(),/^(>\S+.*\n([a-z\s]+\n?)+\n*)+$/i)) {
                    	errorMessage="Query sequence(s) must be in (multi)FASTA format.";
                    	formValid=false;
                    }
                    else if (! ngspipelines.utils.regex($('#blast-fastaseq').val(),/^(>\S+.*\n([ACDEFGHIKLMNOPQRSTUVWYB\s]+\n?)+\n*)+$/i)) {
                    	errorMessage="Sequence(s) must be nucleic or proteic.";
                    	formValid=false;
					}
                    else if (! ngspipelines.utils.regex($('#blast-fastaseq').val(),/^(>\S+.*\n([ABCDGHKMNRSTUVWXY\s]+\n?)+\n*)+$/i) &&
                    		  $('#blast-prg').val() == "blastn") {
                    	errorMessage="Protein FASTA provided for nucleotide sequence.";
                    	formValid=false;
					}
                    	
                    //check evalue
                    if (! ngspipelines.utils.regex($('#blast-expect').val(),/^\d+(e-\d+)*$/i)) {
                    	errorMessage="Unexpected e-value.";
                    	formValid=false;
                    }
                    //check max hit
                    if (! ngspipelines.utils.regex($('#blast-maxhit').val(),/^\d+$/i)) {
                    	errorMessage="Unexpected value for max hit.";
                    	formValid=false;
                    }
                  
                    var filterValue = 'no';
                    if ( $('#blast-filter').is(':checked')) {
                    	filterValue = 'yes';                    	
                    }
                    
                    var viewAln = false;
                    if( $('#blast-alnview').is(':checked')) {
                    	viewAln = true;
                    }
                    
                    if ( ! formValid ){
                    	ngspipelines.ui.display("error", errorMessage, $("#blast-error"));
                    	$("#blast-result-table").html("");
                    	$("#blast-result").css("display", "none");
    					$("#blast-wait").css("display", "none");
                    }
                    else{
                    	$("#blast-error").fadeOut(800);
						ngspipelines.ui.wait("Please wait while blast running ...", "center", $("#blast-wait"));
						ngspipelines.application.blast.runBlast (
							{
								"blastFastaSeq"	  : $('#blast-fastaseq').val(),
								"blastPrg"	      : $('#blast-prg').val(),
								"blastFilter"	  : filterValue,
								"blastExpect"	  : $('#blast-expect').val(),
								"blastMaxHit"	  : $('#blast-maxhit').val(),
								"blastViewAln"    : viewAln,
								"categorie"			: opts.prefix
							},
							opts.analyses,
							function(result) {
								$("#blast-wait").css("display", "none");
								if (typeof result.alignment !== 'undefined') {
									alnpath = "/ngspipelines/data/" + result.alignment.replace(/.*data/,"");
								}
								if (result.m8.length==0){
									$("#blast-wait").css("display", "none");
									ngspipelines.ui.display("warning", "No results", $("#blast-result-return"));
								}
								else{
									ngspipelines.utils.tmpl(
										$("#blast-result-table"),
										_templateres,
										{m8: result.m8, root: opts.rootName, mart: opts.martName, favoritesID: opts.favoritesID, prefix: opts.prefix, report: opts.report},
										function() {
											// Blast res table actions and initialization
											$('#blast-table').dataTable({
										  		"sPaginationType": "full_numbers",
										  		"aaSorting": [[ 2, "asc" ]],
										  		"aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0 ] } ]
											});
											
											$('[name^="'+opts.prefix+'_blast_"]').click(function () {
												if($(this).is(':checked')) {
													$('[name=' + $(this).val() + ']').each(function() {
														$(this).attr('checked', true);
													});
												}
												else {
													$('[name=' + $(this).val() + ']').each(function() {
														$(this).attr('checked', false);
													});
												}
											});
											$("#" + opts.prefix + "-add-blast-favorites-btn").click( function () {
												var cfavoritesID   = new Array(),
													id = "";
												$(':checked[name^="'+opts.prefix+'_blast_"]').each(function() {
													id = $(this).attr("name").split("_")[2];
													cfavoritesID.push(id);
												});
												if($('[name^="'+opts.prefix+'_blast_"]').length == 0) {
													ngspipelines.ui.display("error", "All raw(s) are already favorites!", $("#blastres-error"));
													$("#blastres-error").delay(1500).fadeOut(800);
												}
												else if(cfavoritesID.length == 0) {
													ngspipelines.ui.display("error", "No raw selected!", $("#blastres-error"));
													$("#blastres-error").delay(1500).fadeOut(800);
												}
												else {
													ngspipelines.utils.updateFavoriteStatus(
															cfavoritesID,
															opts.analysisType,
															opts.analyses,
															function (isFavorites) {
																for(var i=0; i<cfavoritesID.length; i++) {
																     $('[name='+opts.prefix+'_span_blast_' + cfavoritesID[i] + ']').html("<center><i class='icon-star'></i></center>");
																}
																
																// Update the list of favorites
																opts.favoritesID += "," + cfavoritesID;
																
																$("#" + opts.prefix + "-favorite-wrapper").favorites({
																	dataset: opts.dataset,
																	analyses: opts.analyses,
																	rootName: opts.rootName,
																	martName: opts.martName,
																	prefix: opts.prefix,
																	fields: opts.fields,
																	links: opts.links,
																	report: opts.report,
																	analysisType: opts.analysisType,
																	mainTableName: opts.mainTableName,
																	tableHeader: opts.tableHeader
																});
															}
													)
													ngspipelines.ui.display("success", "Selected raw(s) have been added to favorites!", $("#blastres-error"));
													$("#blastres-error").delay(1500).fadeOut(800);
												}
											});
											
											if (typeof result.alignment !== 'undefined') {
												$.ajax({
													   type: "GET",
													   url: alnpath,
													   error:function(msg){
														   ngspipelines.ui.display("error", msg, $("#blast-alndiv"));
													   },
													   success:function(data){
														   $('#blast-alignment').text(data);
												}});
											}
											else {
												$('#blast-alndiv').css("display", "none");
											}
										}
								);
								}
							},
	        				function(errorMsg) {
								$("#blast-wait").css("display", "none");
								ngspipelines.ui.display("error", errorMsg, $("#blast-result-return"));
	        				}
						)
                    }
                });
				
				$('#blast-clear-btn').click( function () {
					$('#blast-fastaseq').val("");
					$('#blast-prg').val("blastn");
					$('#blast-filter').attr('checked', true);
					$('#blast-expect').val(10);
					$('#blast-maxhit').val(10);
					$("#blast-result").css("display", "none");
					$("#blast-error").fadeOut(800);
				});
			});
		});
		return this;
	};
})(jQuery);
