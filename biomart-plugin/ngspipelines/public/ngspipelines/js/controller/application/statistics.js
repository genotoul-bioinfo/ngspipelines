(function($) {
	$.fn.statistics = function(options) {
		
		var _template = "js/view/application/statistics.tmpl",
			_tableTemplate = "js/view/application/table.tmpl",
						
			// default option values
			defaults = {
				analyses: {},
				graphicsWrapperID: "",
				graphicsTitleWrapperID: "",
				graphicsSubTitleWrapperID: "",
				prefix: ""
			};
 
		var opts = $.extend(defaults, options),
			graphics = {};
		
		function isNumber (o) {
			return ! isNaN (o-0) && o !== null && o.replace(/^\s\s*/, '') !== "" && o !== false;
		}
		
		/* 
		 * Subfunction to _donutchart_drilldown
		 * @param containerID : the container of the graphics
		 * @param results : result to display in a donut
		 * @param tooltip
		 * @param comments
		 */
		var _donutchart_drilldown = function (containerID, results, tooltip, comments) {
			// results must contains list of labels and values [key] in default group
			var parseInt10 = function(val) { return parseInt(val,10) }
			var colors = Highcharts.getOptions().colors;
			var data = new Array(),
				categories = results.labels.split(","),
				categoriesCount = results.values.split(",").map(parseInt10),
				categoriesAllValues = {}, 
				total = 0;
			for (var i = 0; i < categoriesCount.length; i++) {
				total += categoriesCount[i] << 0;
				categoriesAllValues[categories[i]] = categoriesCount[i];
			}
		   			
			// Build the data arrays
			var upData = [],
				subData = [],
				tab_sub_categories = [],
				sub_categories = {},
				sub_total = 0;
			for(var index = 0; index < categories.length; index++) {
				sub_categories[categories[index]]=new Array();
			}
			
			for (var index=0;index<categories.length;index++){
				subLabels = results[categories[index]].labels.split(',');
				subValues = results[categories[index]].values.split(',').map(parseInt10);
				sub_total = 0
				for (var i=0;i<subLabels.length;i++){
					sub_total += subValues[i];
				}
				for (var j=0;j<subLabels.length;j++){
					sub_categories[categories[index]].push([subLabels[j],Math.round((parseFloat(subValues[j])/parseFloat(sub_total))*(parseFloat(100))*100)/100]);
				}
			}
			tab_sub_categories.push(sub_categories);
			
			for (var i = 0; i < categories.length; i++) {
				// add categorie data
				upData.push({
					name: categories[i],
					y:Math.round((parseFloat(categoriesCount[i])/parseFloat(total))*(parseFloat(100))*100)/100,
					color: colors[i],
					drilldown: categories[i]
				});
				//add sub categorie data
				var brightness = 0.2 - (i / categories.length) / 5 ;
				subData.push({
					color: Highcharts.Color(colors[i]).brighten(brightness).get(),
					id: categories[i],
					name:categories[i],
					data:tab_sub_categories[0][categories[i]]
				}); 
			}
			
			graphics[containerID] = new Highcharts.Chart({
				chart: {
					renderTo: containerID,
					type: 'pie',
					width: 670,
					spacingLeft: 0,
					spacingRight: 20
				},
				subtitle: {
					text: 'Click the slices to zoom into a categorie'
				},
				lang: {
					drillUpText: 'Zoom out'
				},
				title: { text : null },
				plotOptions: {
					series: {
						dataLabels: {
							enabled: true,
							format: '{point.name}: {point.y:.1f}%'
						}
					}
				},
				tooltip: {
					headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
					pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
				},
				series: [{
					name: 'Categorie',
					colorByPoint: true,
					data: upData
				}],
				drilldown: {
					series: subData
				}
			});	
		}

		/* 
		 * Subfunction to _donutchart_drilldown
		 * @param containerID : the container of the graphics
		 * @param results : result to display in a donut
		 * @param tooltip
		 * @param comments
		 */
		var _donutchart = function (containerID, results, tooltip, comments) {
			// results must contains list of labels and values [key] in default group
			var parseInt10 = function(val) { return parseInt(val,10) }
			var colors = Highcharts.getOptions().colors;
			var data = new Array(),
				categories = results.labels.split(","),
				categoriesCount = results.values.split(",").map(parseInt10),
				categoriesAllValues = {}, 
				total = 0;
			for (var i = 0; i < categoriesCount.length; i++) {
				total += categoriesCount[i] << 0;
				categoriesAllValues[categories[i]] = categoriesCount[i];
			}
			for(var index = 0; index < categories.length; index++) {
				var subCategoriesPercent = new Array(), 
				subCategories = new Array(), 
				subCategoriesCount = new Array();
				if ( !(results[categories[index]] === undefined)) {
						subCategories = results[categories[index]].labels.split(","),
						subCategoriesCount = results[categories[index]].values.split(",").map(parseInt10);
				} else {
						subCategories.push("No annotation");
						subCategories.push(categoriesCount[index]);
				}
				//create working variable
				for (var iresult in subCategoriesCount) {
					if (iresult != "remove") {
						subCategoriesPercent.push(Math.round(((parseFloat(subCategoriesCount[iresult])/parseFloat(total))*(parseFloat(100)))*100)/100);
						categoriesAllValues[categories[index]+'.'+subCategories[iresult]] = subCategoriesCount[iresult];
					}
				}
				
				//json for donut
				if (subCategories.length > 0) {
					data.push({
						y: (Math.round((parseFloat(categoriesCount[index])/parseFloat(total))*(parseFloat(100))*100)/100),
						color: colors[index],
						drilldown: {
							categories: subCategories,
							data: subCategoriesPercent,
							color: colors[index]
						}
					});
				} else {
					data.push({
						y: (Math.round((parseFloat(categoriesCount[index])/parseFloat(total))*(parseFloat(100))*100)/100),
						color: colors[index],
						drilldown: {
							categories: [categories[index]],
							data: [(Math.round((parseFloat(categoriesCount[index])/parseFloat(total))*(parseFloat(100))*100)/100)],
							color: colors[index]
						}
					});
				}
			}
			// Build the data arrays
			var upData = [];
			var subData = [];
			for (var i = 0; i < data.length; i++) {
				// add categorie data
				upData.push({
					name: categories[i],
					y: data[i].y,
					color: data[i].color,
					categorie: ""
				});
				// add sub categorie data
				for (var j = 0; j < data[i].drilldown.data.length; j++) {
					var brightness = 0.2 - (j / data[i].drilldown.data.length) / 5 ;
					subData.push({
						name: data[i].drilldown.categories[j],
						y: data[i].drilldown.data[j],
						color: Highcharts.Color(data[i].color).brighten(brightness).get(),
						categorie: categories[i]
					});
				}
			}
			graphics[containerID] = new Highcharts.Chart({
				chart: {
					renderTo: containerID,
					type: 'pie',
					width: 670,
					spacingLeft: 0,
					spacingRight: 20
				},
				title : { text : null },
				subtitle : {
					text : comments,
					align: 'left',
				},
				plotOptions: {
					pie: {
						shadow: false
					}
				},
				tooltip: {
					formatter: function() {
						if (this.point.categorie == "" ) {
							return tooltip.replace("###X###", Highcharts.numberFormat(categoriesAllValues[this.point.name], 0) ).replace("###Y###", this.point.name);
						}else{
							return tooltip.replace("###X###", Highcharts.numberFormat(categoriesAllValues[this.point.categorie+"."+this.point.name], 0) ).replace("###Y###", this.point.name +" ("+this.point.categorie+")");
						}
					}
				},
				credits: { enabled: false },
				series: [{
					name: 'Percentage',
					data: upData,
					size: '60%',
					dataLabels: {
						formatter: function() {
							return this.y > 5 ? this.point.name : null;
						},
						color: 'white',
						distance: -50
					}
				}, {
					name: 'Percentage',
					data: subData,
					size: '80%',
					innerSize: '60%',
					dataLabels: {
						enabled: true,
						color: '#000000',
						formatter: function() {
							// display only if larger than 1	
							return '<b>'+this.point.categorie+' <br />'+ this.point.name +'</b>: '+ this.y +' %';
						}
					}
				}]
			});	
		

				   
		}
		/* 
		 * Subfunction to _histogram (histogram chart)
		 * @param containerID : the container of the graphics
		 * @param labels : labels of the graph
		 * @param values : values of the graph ( array or dict if graph have several series)
		 * @param x_title
		 * @param y_title
		 * @param tooltip
		 */
		var _histogram = function (containerID, labels, values,  x_title, y_title, tooltip) {
			var parseInt10 = function(val) { return parseInt(val,10) },
			isNumber = function(str) {
				   var pattern = /^\d+$/;
				   return pattern.test(str);  // returns a boolean
				},
				
			legend = {},
			nb_series = 0,
			modified_values = new Array(),
			plotOptions = {series:{turboThreshold:0}};
			
			for(var series_name in values ) {
				if( values.hasOwnProperty(series_name) ) {
					nb_series += 1;
					modified_values.push( {
						'name' : series_name,
						'data' : values[series_name].split(",").map(parseInt10)
					});
				}
			}
			
			if(nb_series > 1){
				legend = {
						enabled:true
				};
			}
			else {
				legend = {
					enabled:false
				};
			}

			var xAxis = {
					categories:labels,
					labels: {
							rotation: -45,
							align: 'right'
						},
						title: { text: x_title }
					}, yAxis = {
						min: 0,
						title: {
							text: y_title
					}};

			graphics[containerID] = _highcharts(containerID,"column",xAxis,yAxis,modified_values,legend,labels,plotOptions,tooltip)
		}
		
		/* 
		 * Subfunction to _column (histogram chart)
		 * @param containerID : the container of the graphics
		 * @param labels : labels of the graph
		 * @param values : values of the graph ( array or dict if graph have several series)
		 * @param x_title
		 * @param y_title
		 * @param tooltip
		 */
		var _column = function (containerID, labels, values,  x_title, y_title, tooltip) {
			var parseInt10 = function(val) { return parseInt(val,10) },	
			legend = {},
			nb_series = 0,
			modified_values = new Array(),
			plotOptions = {series:{turboThreshold:0}};
			
			for(var series_name in values ) {
				if( values.hasOwnProperty(series_name) ) {
					nb_series += 1;
					var new_tab = [],
			   			original_tab_values = values[series_name].split(",").map(parseInt10);
					
					for (var i=0;i<labels.length;i++){
						if (original_tab_values[i] != 0){
							new_tab.push({x:parseInt10(labels[i]),y:original_tab_values[i]});
						}
					}
					modified_values.push( {
						'name' : series_name,
						'data' : new_tab
					});
				}
			}
			
			if(nb_series > 1){
				legend = {
						enabled:true
				};
			}
			else {
				legend = {
					enabled:false
				};
			}

			var xAxis = {
					labels: {
							rotation: -45,
							align: 'right'
						},
						title: { text: x_title }
					}, yAxis = {
						min: 0,
						title: {
							text: y_title
					}};

			graphics[containerID] = _highcharts(containerID,"column",xAxis,yAxis,modified_values,legend,labels,plotOptions,tooltip)
		}
		
		
		/* 
		 * Subfunction to _column
		 * @param containerID : the container of the graphics
		 * @param labels : labels of the graph
		 * @param values : values of the graph ( array or dict if graph have several series)
		 * @param x_title : title for x axis
		 * @param y_title : title for y axis
		 * @param tooltip : tooltip string or json
		 */
		var _bargraph = function (containerID, labels, values, x_title, y_title, tooltip) {
			var parseInt10 = function(val) { return parseInt(val,10) },
			legend = {},
			nb_series = 0,
			modified_values = new Array(),
			plotOptions= {};
			
			for(var series_name in values ) {
				if( values.hasOwnProperty(series_name) ) {
					nb_series += 1;
					modified_values.push( {
						'name' : series_name,
						'data' : values[series_name].split(",").map(parseInt10)
					});
				}
			}
			
			if(nb_series > 1){
					legend = {
							enabled:true
					};
					plot_options = {
						bar: {
							dataLabels: {
								enabled: false
							}
						},
						series:{
							marker: {
								radius: 3
							}
						}
					};
			 } else {
					legend = {
						enabled:false
					};
					plotOptions = {
						bar: {
							dataLabels: {
								enabled: true
							}
						},
						series:{
							marker: {
								radius: 3
							}
						}
					};
			}
			
			var xAxis = {
				categories: labels,
				labels: {},
				title: { text: null }
			}, yAxis = {
				min: 0,
				title: {
					text: x_title,
					align: 'high'
				},
				labels: {
					overflow: 'justify'
				}
			};
			graphics[containerID] = _highcharts(containerID,"bar",xAxis,yAxis,modified_values,legend,labels,plotOptions,tooltip)
			
		}
		
		/* 
		 * Subfunction to _column
		 * @param containerID : the container of the graphics
		 * @param labels : labels of the graph
		 * @param values : values of the graph ( array or dict if graph have several series)
		 * @param x_title : title for x axis
		 * @param y_title : title for y axis
		 * @param tooltip : tooltip string or json
		*/
		var _linegraph = function (containerID, labels, values, x_title, y_title, tooltip) {
			var parseInt10 = function(val) { return parseInt(val,10) },
			legend = {},
			nb_series = 0,
			modified_values = new Array();
			
			for(var series_name in values ) {
				if( values.hasOwnProperty(series_name) ) {
					nb_series += 1;
					modified_values.push( {
						'name' : series_name,
						'data' : values[series_name].split(",").map(parseInt10)
					});
				}
			}
		
			var xAxis = {
				categories : labels,
				labels: {
					rotation: -45,
					align: 'right'
				},
				title: {
					text: x_title
				}
				
			}, yAxis = {
				plotLines: [{
					value: 0,
					width: 1
				}],
				min: 0,
				title: {
					text: y_title
				}
			};
			
			if(nb_series > 1){
				legend = {
					layout: 'vertical',
					align: 'right',
					verticalAlign: 'middle',
					borderWidth: 0,
					enabled:true
				};
			}
			else {
				legend = {
						 enabled:false
				}
			}
			graphics[containerID] = _highcharts(containerID,"line",xAxis,yAxis,modified_values,legend,labels,{},tooltip)
		}

		/* 
		 * 
		 * @param containerID : the container of the graphics
		 * @param labels : labels of the graph
		 * @param values : values of the graph ( array or dict if graph have several series)
		 * @param x_title : title for x axis
		 * @param y_title : title for y axis
		 * @param tooltip : tooltip string or json
		 * @param x_type : type for x axis (eg : linear, logarithmic, ...)
		 * @param y_type : type for y axis (eg : linear, logarithmic, ...)
		*/
		var _linegraph_specific_scale = function (containerID, labels, values, x_title, y_title, tooltip, x_type, y_type) {
			var parseInt10 = function(val) { return parseInt(val,10) } ;
			
			// Series
			var nb_series = 0 ;
			var modified_values = new Array();
			for(var series_name in values ) {
				if( values.hasOwnProperty(series_name) ) {
					nb_series += 1;
					var data = new Array() ;
					var series_values = values[series_name].split(",").map(parseInt10) ;
					for( var idx = 0 ; idx < series_values.length ; idx++ ) {
						data.push( new Array(parseInt(labels[idx]), series_values[idx]) )
					}
					modified_values.push( {
						'name' : series_name,
						'data' : data
					});
				}
			}

			// Axis
			var xAxis = {
				title: {
					text: x_title
				},
				"type": x_type
			};
			
			var yAxis = {
				plotLines: [{
					value: 0,
					width: 1
				}],
				min: 0,
				title: {
					text: y_title
				}
			};
			if( y_type != null ){
				yAxis['type'] = y_type ;
			};
			
			// Legend
			var legend = {} ;
			if(nb_series > 1){
				legend = {
					layout: 'vertical',
					align: 'right',
					verticalAlign: 'middle',
					borderWidth: 0,
					enabled:true
				};
			} else {
				legend = { enabled:false }
			}
			
			// Tooltip
			var tooltipChart={}
			if(typeof tooltip == 'string') {
				tooltipChart={
						formatter: function() {
							var x = Highcharts.numberFormat(this.x, 0);
							return tooltip.replace("###X###", x).replace("###Y###", Highcharts.numberFormat(this.y, 0)).replace("###Z###",this.series.name);
						}
					}
			} else {
				tooltipChart=tooltip;
			}

			// Chart
			graphics[containerID] = new Highcharts.Chart({
				chart: {
					renderTo: containerID,
					type: "line",
					width: 670,
					spacingLeft: 0,
					zoomType: 'x',
					spacingRight: 20
				},
				title: { text: null },
				credits: { enabled: false },
				xAxis: xAxis,
				yAxis: yAxis,
				legend:legend,
				tooltip: tooltipChart,
				series: modified_values
			});
		}

		/* 
		 * Subfunction to _column
		 * @param containerID : the container of the graphics
		 * @param labels : labels of the graph
		 * @param values : values of the graph ( array or dict if graph have several series)
		 * @param x_title : title for x axis
		 * @param y_title : title for y axis
		 * @param tooltip : tooltip string or json
		 */
		var _stacked_histogram = function (containerID, labels, values, x_title, y_title) {
			var parseInt10 = function(val) { return parseInt(val,10) },
			modified_values = new Array() ;
			
			for(var series_name in values ) {
				if( values.hasOwnProperty(series_name) ) {
					modified_values.push( {
						'name' : series_name,
						'data' : values[series_name].split(",").map(parseInt10)
					});
				}
			};
			// Display of labels according to labels size
			var xAxis_labels = {
				rotation: -45,
				align: 'right'
			};
		
			var xAxis = {
				categories: labels,
				labels: {
					rotation: -45,
					align: 'right'
				},
				title: { text: null }
			}, yAxis = {
				min: 0,
				title: {
					text: x_title,
					align: 'high'
				}
			};
			
			// if each stacks has space for a stackLabels
			if( labels.length < 10 ) {
				// Display count sum on stacks
				yAxis['stackLabels'] = {
					enabled: true,
					style: {
						fontWeight: 'bold',
						color: 'black'
					}
				};
			}
			var plotOptions= {
				column: {
					stacking: 'normal',
				},
				bar: {
					stacking: 'normal',
				}
			};
			var tooltipJson = {
				pointFormat: '{series.name} : <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
				shared: true
			}
			graphics[containerID] = _highcharts(containerID,"column",xAxis,yAxis,modified_values,{enabled: true},labels,plotOptions,tooltipJson)

		}
		/* 
		 * Subfunction to _highcharts : create an HighChart graph
		 * @param containerID : the container of the graphics
		 * @param type : type of graph
		 * @param xAxis : json of xAxis
		 * @param yAxis : json of yAxis
		 * @param series : data to plot
		 * @param legend : json of legend
		 * @param categorie : list of categorie
		 * @param plotOptions : json of options
		 * @param tooltip : string for tooltip formater or a json directly usable by highchart
		 */
		var _highcharts= function (containerID,type,xAxis,yAxis,series,legend,labels,plotOptions,tooltip){
			

			if (xAxis.hasOwnProperty('labels')) {
				if (labels.length>25) {
					xAxis['labels']['step'] = 2;
				}
				if (labels.length>=50){
					xAxis['labels']['style'] = {font:'9px Helvetica'};
					xAxis['labels']['rotation']=-90;
				}
				xAxis['labels']['formatter'] = function() {
					var xlabel = this.value ;
					if( isNumber(labels[0]) ) {
						if (labels[0] % 1 === 0){
							xlabel = Highcharts.numberFormat(this.value, 0) ;
						}
						else{
							xlabel = Highcharts.numberFormat(this.value, 1) ;
						}
						
					} else {
						if( this.value.length > 22 ) {
							xlabel = this.value.substring(0,19) + "..." ;
						}
					}
					return xlabel ;
				};
			}
			var tooltipChart={}
			if (typeof tooltip == 'string'){
				tooltipChart={
						formatter: function() {
							if (isNumber(labels[0])) {
								var x = Highcharts.numberFormat(this.x, 0);
							} else {
								var x = this.x;
							}
							return tooltip.replace("###X###", x).replace("###Y###", Highcharts.numberFormat(this.y, 0)).replace("###Z###",this.series.name);
						}
					}
				
			}
			else {
				tooltipChart=tooltip;
			}
			return new Highcharts.Chart({
				chart: {
					renderTo: containerID,
					type: type,
					width: 670,
					spacingLeft: 0,
					zoomType: 'x',
					spacingRight: 20
				},
				categories:labels,
				title: { text: null },
				credits: { enabled: false },
				xAxis: xAxis,
				yAxis: yAxis,
				legend:legend,
				plotOptions: plotOptions,
				tooltip: tooltipChart,				
				series: series
			});
		}
		/* 
		 * Subfunction to _scatterplot
		 * @param containerID : the container of the graphics
		 * @param x_values : x coordinate
		 * @param y_values : y coordinate
		 * @param x_title : title for x axis
		 * @param y_title : title for y axis
		 * @param tooltip : string for tooltip formater 
		 */
		var _scatterplot = function (containerID, x_values, y_values, x_title, y_title, tooltip) {

			var data = new Array();
			var parseInt10 = function(val) { return parseInt(val,10) };
			var legend = false;
			var modified_values = [];
			var multiple_series = false;

			if (typeof(y_values) === 'object'){
				multiple_series = true;
			}

			if(multiple_series){
				// TO REPLACE SOMEWHERE ELSE
				colors = []
				colors.push('rgba(223, 83, 83, .5)')
				colors.push('rgba(119, 152, 191, .5)')

				legend = true;
				dic = {};
				for(var series_name in y_values) {
					dic[series_name]=new Array();
					for (var i=0;i<y_values[series_name].length;i++){
						if (! isNaN(parseInt10(y_values[series_name].split(",")[i]))){
							dic[series_name].push({x:i+1,y:parseInt10(y_values[series_name].split(",")[i])});
						}
					}
				}
				var idx = 0;
				for (var series_name in dic){
					modified_values.push( {
						'name' : series_name,
						'data' : dic[series_name],
						'color' : colors[idx]
					});
					idx+=1;
				}
			}	
			else{
				var x_values = x_values.split(",").map(parseInt10),
				y_values = y_values.split(",").map(parseInt10);
				for (var index in x_values) {
					data.push(new Array(x_values[index], y_values[index]));
				}
				modified_values = [{
					data: data
				}];
			}

			new Highcharts.Chart({
				chart: {
					renderTo: containerID,
					type: 'scatter',
					zoomType: 'xy',
					width: 670,
					spacingLeft: 0,
					spacingRight: 20
				},
				title: { text: null },
				xAxis: {
					min: 0,
					title: {
						enabled: true,
						text: x_title
					},
					startOnTick: true,
					endOnTick: true,
					showLastLabel: true
				},
				credits: { enabled: false },
				yAxis: {
					min: 0,
					title: { text: y_title	}
				},
				tooltip: {
					formatter: function() {
						if (multiple_series){
							return tooltip.replace("###X###", this.x).replace("###Y###", Highcharts.numberFormat(this.y, 0)).replace("###Z###",this.series.name);
						}
						else{
						}
						return tooltip.replace("###X###", Highcharts.numberFormat(this.x, 0)).replace("###Y###", Highcharts.numberFormat(this.y, 0));
					}
				},
				legend: { enabled: legend },
				plotOptions: {
					scatter: {
						states: {
							hover: {
								marker: {
									enabled: false
								}
							}
						}
					}
				},
				series: modified_values
			});
		}
		/* 
		 * Subfunction to _piechart
		 * @param containerID : the container of the graphics
		 * @param labels : array of labels for the graph
		 * @param values : values of the graph ( array or dict if graph have several series)
		 * @param tooltip : string for tooltip formater 
		 */
		var _piechart = function (containerID, labels, values, tooltip) {
			var parseInt10 = function(val) { return parseInt(val,10) },
				values = values.split(",").map(parseInt10),
				max = 0,
				max_index = 0,
				sum = 0,
				count = {},
				series = new Array();
			for (var index = 0; index < values.length; index++) {
				if (values[index] > max) {
					max = values[index];
					max_index = index;
				}
				sum += values[index];
			}
			for (var index = 0; index < values.length; index++) {
				if (max_index == index) {
					series.push({
						name: labels[index],
						y: Math.round((values[index]/sum)*10000)/100,
						sliced: true,
						selected: true
					});
				} else {
					series.push(new Array(labels[index], Math.round((values[index]/sum)*10000)/100));
				}
				count[labels[index]] = values[index];
			}
			graphics[containerID] = new Highcharts.Chart({
				chart: {
					renderTo: containerID,
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false,
					width: 670,
					spacingLeft: 0,
					spacingRight: 20
				},
				title: { text: null },
				credits: { enabled: false },
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
							enabled: true,
							color: '#000000',
							formatter: function() {
								return '<b>'+ this.point.name +'</b>: <br />'+ this.y +' %';
							}
						}
					}
				},
				tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage}%</b>',
					percentageDecimals: 1,
					formatter: function() {
						return tooltip.replace("###X###", this.point.name).replace("###Y###", Highcharts.numberFormat(count[this.point.name], 0));
					}
				},
				series: [{
					type: 'pie',
					data: series
				}]
			});
		}
		/* 
		 * Subfunction to _table
		 * @param containerID : the container of the graphics
		 */
		var _table = function (containerID, header, rows, comments) {
			var header = header.split(","),
				rows_splited = new Array(),
				sortable = new Array();
			for (var index = 0; index < rows.length; index++) {
				rows_splited.push(rows[index].split(","));
			}
			for (var index = 0; index < header.length; index++) {
				sortable.push(index);
			}
			var analysis_id = containerID.split("-")[3];
			ngspipelines.utils.tmpl($("#"+containerID), _tableTemplate, {prefix: opts.prefix, comments:comments, header: header, rows:rows_splited,id:analysis_id}, function(){
				$("#"+opts.prefix+"-general-stats-table-"+analysis_id).dataTable({
					"bFilter": false,
					"bInfo": false,
					"bPaginate": false,
					"aoColumnDefs": [ { "bSortable": false, "aTargets": sortable } ]
				});
			});
		}
		/* 
		 * Subfunction to _initGraphic
		 * @param containerID : the container of the graphics
		 * @param data : data for the graphics
		 */
		var _initGraphic = function(containerID, data) {
			var values = new Array(),
			nb_series=0,
			key_series="";
			for(var key in data) {
				if( !(data[key] === undefined) && data[key].hasOwnProperty('values') ) {
					values[key] = data[key]['values'] ;
					nb_series+=1;
					key_series=key;
				}
				else if(!(data[key] === undefined)  && data[key].hasOwnProperty('y_values') ){
					if (data.y_values === undefined){
						data.y_values = {};
					}
					data.y_values[key] = data[key]['y_values'];
					nb_series+=1;
					key_series=key;
				}
			}

			var labels = new Array();
			if (data.hasOwnProperty("labels")) {
				labels = data["labels"].split(",");
			}
			if (! data.hasOwnProperty("tooltip")) {
				data["tooltip"]="###X### : ###Y###";
			}

			if (data.type == "histogram" ){
				_histogram(containerID, labels, values, data.x_title, data.y_title, data.tooltip);
			} else if (data.type == "column") {
				_column(containerID, labels, values, data.x_title, data.y_title, data.tooltip);
			}
			else if (data.type == "stacked_histogram") {
				_stacked_histogram(containerID, labels, values, data.x_title, data.y_title, data.tooltip);
			} else if (data.type == "bargraph") {
				_bargraph(containerID, labels, values,  data.x_title, data.y_title, data.tooltip);
			} else if (data.type == "basicline") {
				if( data.hasOwnProperty("x_type") && data.x_type != "category" && data.x_type != "linear" ){
					var y_type = null ;
					if( data.hasOwnProperty("y_type") ){
						y_type = data.y_type ;
					}
					_linegraph_specific_scale(containerID, labels, values,  data.x_title, data.y_title, data.tooltip, data.x_type, y_type);
				} else {
					_linegraph(containerID, labels, values,  data.x_title, data.y_title, data.tooltip);
				}
			} else if (data.type == "piechart") {
				if (nb_series == 1){ 
					_piechart(containerID, labels, values[key_series], data.tooltip);
				}
			} else if (data.type == "scatterplot") {
				_scatterplot(containerID, data.x_values, data.y_values, data.x_title, data.y_title, data.tooltip);
			} else if (data.type == "donutchart" ) {
				_donutchart(containerID, data, data.tooltip, data.comments);
			} else if (data.type == "donutchart_drilldown") {
				_donutchart_drilldown(containerID, data, data.tooltip, data.comments);
			}
			else if (data.type == "table") {
				var rows = new Array();
				for (var index in data) {
					if (index.indexOf("row_") == 0) {
						rows.push(data[index]);
					}
				}
				_table(containerID, data.header, rows, data.comments);
			}
		}
		
		var _linkHandler = function (analysis_id, results) {
			$("#"+opts.graphicsTitleWrapperID).html(results[analysis_id]["chart_title"]);
			var sub_title=results[analysis_id]["chart_subtitle"];
			if (sub_title == undefined ) {
				sub_title = " chart"
			};
			$("#"+opts.graphicsSubTitleWrapperID).html(sub_title);
			$("[id^='"+opts.prefix+"-statistics-links-']").removeClass("li-active");
			$("#"+opts.prefix+"-statistics-links-"+analysis_id).addClass("li-active");
			if ($("#"+opts.prefix+"-highcharts-wrapper-"+analysis_id).length == 0) {
				$("#"+opts.graphicsWrapperID).append('<div id="'+opts.prefix+'-highcharts-wrapper-'+analysis_id+'"> </div>');
				_initGraphic(opts.prefix+'-highcharts-wrapper-'+analysis_id, results[analysis_id]);
			}
			$("div[id^="+opts.prefix+"-highcharts-wrapper-]").css("display", "none");
			$("#"+opts.prefix+"-highcharts-wrapper-"+analysis_id).css("display", "inline"); 
		}
		
		this.each(function() {
			
			var $t = $(this);

			ngspipelines.application.getDisplayGraphAnalyses(
				opts.analyses,
				opts.prefix,
				function (results, firstID) {
					ngspipelines.utils.tmpl($t, _template, {prefix: opts.prefix, analyses:results}, function(){						
						// handle the click on the link
						_linkHandler(firstID, results);
						$("[id^='"+opts.prefix+"-statistics-links-']").click(function(){
							var analysis_id = $(this).attr("id").split("-")[3];
							_linkHandler(analysis_id, results);
						});
					});
				},
				function(errorMsg) { 
					ngspipelines.ui.display("error", errorMsg, $("#"+opts.prefix+"-statistics-error"));
				}
			);
			
		});
		return this;
	};
})(jQuery);