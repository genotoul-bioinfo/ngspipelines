(function($) {
	$.fn.favorites = function(options) {
		
		var _template = "js/view/application/favorites.tmpl",

			// error messages
			NO_SELECTED_ERROR_MSG = "No favorite selected!",
			NO_SEARCH_AVAILABLE = "No mart search is settled, ask your biomart administrator to set one so you can access this functionality!",
			
	        // default option values
			defaults = {
	            dataset: {},
	            analyses: {},
	            rootName: "",
            	martName: "",
            	prefix: "",
            	fields: {},
            	links: "",
            	report: "",
            	analysisType: "",
            	mainTableName: "",
            	tableHeader: "",
            	disableBlastFeature: false
	    	};
	    
	    // merge default option values and provided parameters 
		var opts = $.extend(defaults, options);
		
		// function in charge to handle clicks on the #delete-favorites-btn button
		var deleteFavoritesHandler = function() {
			
			var favorites = new Array(),
				favoritesIDs = new Array(),
				id = "";
			$(':checked[id^="chk_'+opts.prefix+'_"]').each(function() {
				id = $(this).attr("id").split("_")[2];
				favorites.push($("#" + opts.prefix + "_favorite_"+id).html());
				favoritesIDs.push(id);
			});
			
			if ($(':checked[id^="chk_'+opts.prefix+'_"]').size() >= 1) {
		        $dialog = $('#ngspipelines-dialog').dialog({
		            draggable: false,
		            resizable: false,
		            modal: true,
		            autoOpen: false,
		            width: 450,
		            title: "Are you sure you want to delete following favorites",
		            buttons: {
		            	"Delete from favorites": function() {
		            		ngspipelines.ui.wait("Please wait until deletion from your favorites is over ...", "center", $("#ngspipelines-dialog-body"));
		        			ngspipelines.utils.updateFavoriteStatus ( 
		        				favoritesIDs, 
	        					opts.analysisType,
	        					opts.analyses,
	        					function() {
	        						// unbind button actions
	        						$("#" + opts.prefix + "-delete-favorites-btn").unbind('click', deleteFavoritesHandler);
	        						$("#" + opts.prefix + "-favorite-wrapper").favorites({
	        							dataset: opts.dataset,
	        							analyses: opts.analyses,
	        							rootName: opts.rootName,
		        			            martName: opts.martName,
		        			            prefix: opts.prefix,
		        		            	fields: opts.fields,
		        		            	links: opts.links,
		        		            	report: opts.report,
		        		            	analysisType: opts.analysisType,
		        		            	mainTableName: opts.mainTableName,
		        		            	disableBlastFeature: opts.disableBlastFeature,
		        		            	tableHeader: opts.tableHeader
	        						});
	        					},
	        					function(errorMsg) {
	        						ngspipelines.ui.display("error", errorMsg, $("#ngspipelines-dialog-body"));
	        					}
		        			);
        					$(this).dialog('close');
		            	},
		                "Cancel": function() {
		                    $(this).dialog('close');
		                }
		            }
		        });
		        $('#ngspipelines-dialog-body').html(
		        		"<div class='bloc-small-dialog'>" +
		        		"	<ul>" +
		        		"		<li class='li-single'><i class='icon-remove-circle'></i> " +
		        		favorites.join("</li><li class='li-single'><i class='icon-remove-circle'></i> ") +
		        		"	</li></ul>" +
		        		"</div>"
		        );
		        $dialog.dialog('open');	
			}
			else {
		        ngspipelines.ui.display("error", NO_SELECTED_ERROR_MSG, $("#" + opts.prefix + "-favorites-table-error"));
				$("#" + opts.prefix + "-favorites-table-error").delay(1500).fadeOut(800);
			}
		}
		
		var _initFavoriteTemplate = function (martformInfo, cdata) {
			// Init array of favorites ID from cdata
			var len = cdata.length;
			var favoritesID=[];
			$.each(cdata, function (i,e) {favoritesID.push(e[opts.fields["key"]])});
			
			// favorites table actions and initialization
			var ctgdatatable = $('#' + opts.prefix + '-favorite-table').dataTable({
		  		"sPaginationType": "full_numbers",
		  		"aaSorting": [[ 2, "desc" ]],
		  		"aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0 ] } ],
		  		"sDom": 'lfrtiTp',
		  		"oTableTools": {
					"sSwfPath": "js/lib/TableTools-2.1.4/swf/copy_csv_xls_pdf.swf",
					"aButtons": [
					             {
					            	 "sExtends": "copy",
					            	 "sButtonText": "Copy"
					             },
					             {
					            	 "sExtends": "csv",
					            	 "sButtonText": "Save to CSV",
					            	 "sTitle": opts.prefix+"_favorites"
					             }
					            ]
				},
				"sScrollX": "100%",
				"bScrollCollapse": true
		  	});
			
			// bind button actions						
			$("#" + opts.prefix + "-delete-favorites-btn").bind('click', deleteFavoritesHandler);
			
			if (! martformInfo.error ) {
				
				$("#" + opts.prefix + "-search-btn").click(function(){
					location.href = "/martform/#!/" + martformInfo.gui + "/" + martformInfo.config + "?datasets=" + opts.dataset.name;
				});
			}
			else {
				$("#" + opts.prefix + "-search-btn").click(function(){
                	ngspipelines.ui.display("error", NO_SEARCH_AVAILABLE, $("#" + opts.prefix + "-favorite-error"));
                	$("#" + opts.prefix + "-contigs-error").delay(2000).fadeOut(800);
				});
			}
			
			// Blast search
			$("#" + opts.prefix + "-blast-btn").click(function(){
				    $dialog = $('#ngspipelines-dialog').dialog({
			            draggable: true,
			            resizable: false,
			            modal: true,
			            autoOpen: false,
			            width: 960,
			            position: ['center',20],
			            title: "Blast your query against the database",
			            buttons : { 
			            	"close": function() { 
			            		$(this).dialog("close"); 
			            	}
			            }
			        });
			        $('#ngspipelines-dialog-body').blast({
			        	dataset: opts.dataset,
			        	analyses: opts.analyses,
			        	martName: opts.martName,
			        	rootName: opts.rootName,
			        	favoritesID: favoritesID,
			        	fields: opts.fields,
			        	links: opts.links,
			        	report: opts.report,
			        	analysisType: opts.analysisType,
			        	mainTableName: opts.mainTableName,
			        	tableHeader: opts.tableHeader,
			        	prefix: opts.prefix
			        });
			        $dialog.dialog('open');
			});
			
		}
		
	    // Main plugin function
		this.each(function() {
			// init the container
			var $t = $(this),
				keys = new Array(),
				lowerCaseFields = {};
			ngspipelines.ui.wait("Please wait until your favorites are being loaded ...", "center", $t);
			// reload the data
			for (value in opts.fields) {
				if (value != "key") { keys.push(value.toLowerCase()); }
				lowerCaseFields[value.toLowerCase()] = opts.fields[value];
			}
			
			ngspipelines.application.getFavorites(
				opts.dataset, 
				opts.analyses,
				opts.prefix,
				opts.analysisType,
				opts.mainTableName,
				opts.fields,
				function(favorites) {
					// check if blast is available and show the button (with option disableBlastFeature)
					ngspipelines.utils.hasBlastFeature(
							opts.dataset, 
							opts.analyses,
							opts.prefix,
							"blast_search",
							function(hasBlastFeature){
								ngspipelines.utils.getSearchGUI(
						        	function(martformInfo) {
						        		$t.html("");
										ngspipelines.utils.tmpl($t, _template, {favorites: favorites, root: opts.rootName, mart: opts.martName, prefix: opts.prefix, 
											fields: lowerCaseFields, keys: keys, links:opts.links, report: opts.report, tableHeader: opts.tableHeader, 
											disableBlastFeature: !hasBlastFeature }, function(){
											_initFavoriteTemplate(martformInfo, favorites);
										});
						        	},
						        	ngspipelines.utils.getDatasetType(opts.dataset),
						        	ngspipelines.utils.getDatasetPrefix(opts.dataset)
						        )
							},
							function(){
								ngspipelines.utils.getSearchGUI(
						        	function(martformInfo) {
						        		$t.html("");
										ngspipelines.utils.tmpl($t, _template, {favorites: {}, root: opts.rootName, mart: opts.martName, prefix: opts.prefix, 
											fields: opts.fields, keys: keys, links: opts.links, report: opts.report, tableHeader: opts.tableHeader,
											disableBlastFeature: opts.disableBlastFeature}, function(){
											_initFavoriteTemplate(martformInfo, {});
										});
						        	},
						        	ngspipelines.utils.getDatasetType(opts.dataset),
						        	ngspipelines.utils.getDatasetPrefix(opts.dataset)
						        );
							}
					);
				},
				function(errorMsg) { 
			        ngspipelines.utils.getSearchGUI(
			        	function(martformInfo) {
			        		$t.html("");
							ngspipelines.utils.tmpl($t, _template, {favorites: {}, root: opts.rootName, mart: opts.martName, prefix: opts.prefix, 
								fields: opts.fields, keys: keys, links: opts.links, report: opts.report, tableHeader: opts.tableHeader,
								disableBlastFeature: opts.disableBlastFeature}, function(){
								_initFavoriteTemplate(martformInfo, {});
							});
			        	},
			        	ngspipelines.utils.getDatasetType(opts.dataset),
			        	ngspipelines.utils.getDatasetPrefix(opts.dataset)
			        );
				}
			);
			
		});
		return this;
		
	};
})(jQuery);