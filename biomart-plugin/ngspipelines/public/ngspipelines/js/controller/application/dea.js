(function($) {
	$.fn.deaform = function(options) {
		var _template = "js/view/application/deaform.tmpl",
		_template_deares = "js/view/application/deares.tmpl",
		_template_deago = "js/view/application/goform.tmpl",
		defaults = {
			pool1: [],
			pool2: [],
			analysisDataset: {},
			projectDirectory:  "",
			rootName:  "",
			martName:  "",
			report:  "",
			prefix:"",
			dea_id_key:undefined
		},
		location_no_param = location.href.split('?')[0],
		opts = $.extend(defaults, options);
		opts.pool1 = opts.pool1.sort();
		opts.pool2 = opts.pool2.sort();
		
		var template_args = {
			state			: 'notStarted',
			pool1			: opts.pool1,
			pool2			: opts.pool2,
			category		: opts.prefix,
			alphaArray: ngspipelines.application.dea.getAlphaValues(),
			normalizationArray: ngspipelines.application.dea.getNormValues(),
			computedResults	: [],
			location_href	: location_no_param,
			prefix			: opts.prefix,
			dea_id_key		: opts.dea_id_key
		};

		// PRINT FROM BUTTON RUN DEA ONLY	    	
		_print_result_from_run = function(processor_args){
			$("#dea-wait").html("");
			$("#dea-wait").html("The DEA analysis <strong>has been launched</strong>. </br></br>You can now close this window and come back later or wait while running.");
			$("#dea-content").html("");
			$("#dea-error").hide();
			$("#dea-results").show();
	
			ngspipelines.application.dea.processAction('startDEA', processor_args, opts.analysisDataset,
				function (data){
					if (data.state == "complete" ){
			
						$("#dea-start-btn")[0].disabled=false;
						$("#dea-res-row-running").remove();
						$("button[id^=dea-retrieve-result]").each(function() {
							$(this).html("<i class=\"icon-eye-close\"></i>");
						});
						$new_content='<tr id="dea-res-row-'+data.dea_id_key+'">\
							<td>'+data.parameters.norm+'</td>\
							<td>'+data.parameters.filter+'</td>\
							<td>'+data.parameters.alpha+'</td>\
							<td>'+data.parameters.correct+'</td>\
							<td>'+data.parameters.maplot+'</td>\
							<td>'+data.results.stat.overexp_both_pools+'</td>\
							<td><button class="ngsp-button ngsp-btn-white ngsp-btn-small" id="dea-retrieve-result-'+data.dea_id_key+'">&nbsp;<i class=\"icon-eye-open\"></i>&nbsp;</button></td></tr>'
						$("#"+opts.prefix+"-form-dea-table tbody").append($new_content);
                                                        
						$("#dea-retrieve-result-"+data.dea_id_key).click(function(){
							$("button[id^=dea-retrieve-result]").each(function() {
        	                                                $(this).html("<i class=\"icon-eye-close\"></i>");
	                                                });
                                                        $(this).html("&nbsp;<i class=\"icon-eye-open\"></i>&nbsp;");
                                                        _print_result_from_id($("#dea-results"), data.dea_id_key);
	                                                });

						_print_tab_dea_result(data)
					} else {
						ngspipelines.ui.display("error", message, $("#dea-error") );
						$("#dea-error").fadeOut(1500);
					}
				}, 
				function(message){
					
					$("#dea-start-btn")[0].disabled=false;
					ngspipelines.ui.display("error", message, $("#dea-error") );
					ngspipelines.application.dea.processAction('removeResults', processor_args, opts.analysisDataset,
						function(){
							$("#dea-wait").html("");
							$("#dea-wait").html("An error occurs while running R script, please contact your adminitrator.");
						},
						function(){}
					);
				}
			);
		}; //End print_result

		// PRINT FROM BUTTON RUN DEA ONLY	    	
		_get_result_from_run = function(processor_args){
			ngspipelines.application.dea.processAction('getResults', processor_args, opts.analysisDataset,
				function (data){
					if (data.state == "complete" ){
						$("#dea-wait").html("");
					   	$("#dea-content").html("");
					   	$("#dea-error").hide();
						$("#dea-results").show();
						_print_tab_dea_result(data)
					} else {
						if (data.state == "notStarted" ){
							$new_content='<tr id="dea-res-row-running">\
								<td>'+processor_args.norm+'</td>\
								<td>'+processor_args.filter+'</td>\
								<td>'+processor_args.alpha+'</td>\
								<td>'+processor_args.correct+'</td>\
								<td>'+processor_args.maplot+'</td>\
								<td id="res_na">N/A</td>\
								<td><button class="ngsp-button ngsp-btn-white ngsp-btn-small" disabled><i class="icon-cog"></i></button></td></tr>'
							$("#"+opts.prefix+"-form-dea-table tbody").append($new_content);
						        $("#dea-start-btn")[0].disabled=true;
							_print_result_from_run(processor_args)
						} else {
							ngspipelines.ui.display("error", data.state, $("#dea-error") );
							$("#dea-error").fadeOut(1500);
						}
					}
				}, 
				function(message){
					ngspipelines.ui.display("error", message, $("#dea-error") );
					ngspipelines.application.dea.processAction('removeResults', processor_args, opts.analysisDataset,
						function(){
							$("#dea-wait").html("");
							$("#dea-wait").html("An error occurs while running R script, please contact your adminitrator.");
						},
						function(){}
					);
				}
			);
		}; //End print_result
	    
		// PRINT FROM ID
		_print_result_from_id = function($container, id, go_args){
			var processor_args = {
				category : opts.prefix,
				location_href : opts.location_no_param,
				dea_id_key : id
			};
			ngspipelines.application.dea.processAction('getResults', processor_args, opts.analysisDataset,
				function (data){
					if ($(".venn-pool-res").text() == ""){ //come from email link
						template_args["pool1"]=data.parameters.pool1;
						template_args["pool2"]=data.parameters.pool2;
						ngspipelines.utils.tmpl( $container, _template, template_args, function(){
							$("#dea-form").hide();
							_print_tab_dea_result(data);
							$("#dea-results").show();
						});
					} else {
						_print_tab_dea_result(data);
						$("#dea-results").show();
					}
				},
				function(message){
					ngspipelines.ui.display("error", message, $("#dea-error") );
				});
		};

		//PRINT CONTENT OF DEA RES TAB
		_print_tab_dea_result = function(data){
			data["prefix"] = opts.prefix;
			data["root"]   = opts.rootName;
			data["report"] = opts.report;
			data["mart"]   = opts.martName;
			data["project_dir"] = opts.projectDirectory
			$("#dea-wait").html("");
			$("#dea-content").html("");
			$("#dea-error").hide();

			ngspipelines.utils.tmpl( $("#dea-content"), _template_deares, data, function(){
				$("#dea-table").dataTable({
					"paging": false,
					"bPaginate": false,
					"bLengthChange": false,
					"columnDefs": [{ type: 'scientific', targets: 3 }]
				});
				
				$("#download-dea-results").click(function(){
					if (data.results.result_file_path) {
						window.location.assign( opts.projectDirectory + "/" + data.results.result_file_path ) ;
					}
				});
				
				$("#download-dea-graphics").click(function(){
					if (data.results.images_pdf) {
						window.open(
							opts.projectDirectory + "/" + data.results.images_pdf,
							'_blank' // <- This is what makes it open in a new window.
						);
					}
				});
			});
		}
	    

		/**
		 * Plugin main function
		 */
		return this.each(function() {
			var $container = $(this);
			$container.html("");
			if (opts.dea_id_key != undefined ){ //come from email link
				_print_result_from_id($container,opts.dea_id_key);
			} else {
				ngspipelines.application.dea.getComputedResults(opts.pool1,opts.pool2, opts.analysisDataset,function(available_results){
				    	template_args["computedResults"]=available_results;
				    	ngspipelines.utils.tmpl( $container, _template, template_args,function(){
				    		$('[class^=tooltip]').poshytip({
		    					className: 'tip-twitter',
		    					showTimeout: 1,
		    					showOn: 'hover',
							alignTo: 'target',
							alignY: 'center',
		    					offsetX: 10,
		    					allowTipHover: false,
		    					keepInViewport: true
		    				});
				    	
				    		$("button[id^=dea-retrieve-result]").click(function(){
							$("button[id^=dea-retrieve-result]").each(function() {
								$(this).html("<i class=\"icon-eye-close\"></i>");
							});
							$(this).html("&nbsp;<i class=\"icon-eye-open\"></i>&nbsp;");
				   			var dea_id_key = parseInt($(this).attr("id").split("-")[3]);
				   			_print_result_from_id($container, dea_id_key);
			   			});
					    	
				    		// DEA simple
				    		$("#dea-start-btn").click(function(){
							var processor_args = {
								pool1	: opts.pool1,
								pool2	: opts.pool2,
								category: opts.prefix,
								norm	: $("#" + opts.prefix + "-dea-normalization-select option:selected").attr("value"),
								filter	: $("#" + opts.prefix + "-dea-filter-checkbox").is(":checked") ? "TRUE":"FALSE",
								alpha	: $("#" + opts.prefix + "-dea-alpha-select option:selected").attr("value"),
								correct	: $("#" + opts.prefix + "-dea-correction-select option:selected").attr("value"),
								maplot	: $("#" + opts.prefix + "-dea-maplot-checkbox").is(":checked") ? "TRUE":"FALSE",
								location_href : location_no_param
							};
						
							processor_args.usermail = "";
							//init processor argument :
							_get_result_from_run(processor_args);
						});
			    		});
				});
			}; //fin else
		}); //end main function
	}; //end function deaform
})(jQuery);
