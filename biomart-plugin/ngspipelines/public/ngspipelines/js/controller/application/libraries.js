(function($) {
	$.fn.libraries = function(options) {
		
		var _template = "js/view/application/libraries.tmpl",
			COUNT_FILE_NAME = "merged_count.csv",
			_run = {},
			
	        // default option values
			defaults = {
	            libraries: {},
	            analyses: {},
	            projectDirectory: "",
	            martName: "",
	            rootName: "",
	            report: "",
	            prefix: ""
	    	};
 
		var opts = $.extend(defaults, options);
		
		var _load = function(libraries, analyses, successCB, errorCB) {
			ngspipelines.application.libraries.getLibraries(
				libraries, 
				opts.prefix,
				function(ldata) {
					ngspipelines.application.libraries.getAnalysesByType(
						analyses, 
						function(adata) {
							successCB(ldata, adata);
						},
						function(errorMsg) { errorCB(errorMsg); }
					);
				},
				function(errorMsg) { errorCB(errorMsg); }
			);
		}
		
		var _initLibrariesTable = function(adata) {
			
			// libraries table actions and initialization
			var libDatatable = $("#" + opts.prefix + "-libraries-table").dataTable({
		  		"sPaginationType": "full_numbers",
		  		"aoColumnDefs": [ { "bSortable": false, "aTargets": [ 8,9,10,11,12 ] } ],
		  		"oLanguage": {
		  	         "sLengthMenu": 'Show <select>'+
		             '<option value="10">10</option>'+
		             '<option value="25">25</option>'+
		             '<option value="50">50</option>'+
		             '<option value="100">100</option>'+
		             '<option value="-1">All</option>'+
		             '</select> entries'
		         },
				"iDisplayLength": 10,
		  		"sDom": 'lfrtiTp',
		  		"oTableTools": {
					"sSwfPath": "js/lib/TableTools-2.1.4/swf/copy_csv_xls_pdf.swf",
					"aButtons": [
					             {
					            	 "sExtends": "copy",
					            	 "sButtonText": "Copy"
					             },
					             {
					            	 "sExtends": "csv",
					            	 "sButtonText": "Save to CSV",
					            	 "sTitle": "libraries"
					             }
					            ]
				}
			});
			
			//first init si seul une analyse differentielle est dispo : supprimer les colonnes en trop
			if ($("#" + opts.prefix + "-analyses").val() == "dea") {
				$("#" + opts.prefix + "-dea-form").show();
				/* Hide cols 10 11 and 12 related to Pool3 4 and 5 */
				var bVis = libDatatable.fnSettings().aoColumns[10].bVisible;
				libDatatable.fnSetColumnVis( 10, bVis ? false : true );
				libDatatable.fnSetColumnVis( 11, bVis ? false : true );
				libDatatable.fnSetColumnVis( 12, bVis ? false : true );
				libDatatable.fnSetColumnVis( 13, bVis ? false : true );
			};
			// pre check some libraries
			$("#" + opts.prefix + "-libraries-table > tbody > tr").each(function(i) {
				$(this).children("td").each(function(){
					if ($(this).hasClass("radio"+i)) {
						$(this).children("input").attr('checked','checked');
					}
				});
			});
			//on change
			$("#" + opts.prefix + "-analyses").change(function() {
				if ($(this).val() == "venn") {
					/* Show cols 10 11 and 12 related to Pool3 4 and 5 */
					var bVis = libDatatable.fnSettings().aoColumns[10].bVisible;
					libDatatable.fnSetColumnVis( 10, bVis ? false : true );
					libDatatable.fnSetColumnVis( 11, bVis ? false : true );
					libDatatable.fnSetColumnVis( 12, bVis ? false : true );
					libDatatable.fnSetColumnVis( 13, bVis ? false : true );
				} else if ($(this).val() == "dea") {
					/* Hide cols 10 11 and 12 related to Pool3 4 and 5 */
					var bVis = libDatatable.fnSettings().aoColumns[10].bVisible;
					libDatatable.fnSetColumnVis( 10, bVis ? false : true );
					libDatatable.fnSetColumnVis( 11, bVis ? false : true );
					libDatatable.fnSetColumnVis( 12, bVis ? false : true );
					libDatatable.fnSetColumnVis( 13, bVis ? false : true );
				}
			});
			$("#" + opts.prefix + "-run-btn").click(function() {
				if ($("#" + opts.prefix + "-analyses").val() == "venn") {
					var countFileWebPath = opts.projectDirectory + "/analysis/" + adata.libraries_count[0].directory + "/" + COUNT_FILE_NAME;
					_run.venn(countFileWebPath);
				} else if ($("#" + opts.prefix + "-analyses").val() == "dea") {
					_run.dea();
				}
			});	
		}
		
		_run.venn = function(countFileWebPath) {
			
			ngspipelines.ui.wait("Please wait while generating the Venn diagramm ...", "center", $("#ngspipelines-dialog-body"));
			
			// then grab all radio group info
			var r1 = new Array(),
				r2 = new Array(),
				r3 = new Array(),
				r4 = new Array(),
				r5 = new Array();
				r6 = new Array();
			$("input.radio0").each(function() {
				if ($(this).is(':checked')) {
					r1.push($(this).attr("name"))
				}
			});
			$("input.radio1").each(function() {
				if ($(this).is(':checked')) {
					r2.push($(this).attr("name"))
				}
			});
			$("input.radio2").each(function() {
				if ($(this).is(':checked')) {
					r3.push($(this).attr("name"))
				}
			});
			$("input.radio3").each(function() {
				if ($(this).is(':checked')) {
					r4.push($(this).attr("name"))
				}
			});
			$("input.radio4").each(function() {
				if ($(this).is(':checked')) {
					r5.push($(this).attr("name"))
				}
			});
			$("input.radio5").each(function() {
				if ($(this).is(':checked')) {
					r6.push($(this).attr("name"))
				}
			});
			
			var	nr1 = r1.length > 0 ? 1 : 0, 
				nr2 = r2.length > 0 ? 1 : 0,
				nr3 = r3.length > 0 ? 1 : 0,
				nr4 = r4.length > 0 ? 1 : 0,
				nr5 = r5.length > 0 ? 1 : 0;
				nr6 = r6.length > 0 ? 1 : 0;
			if( nr1 + nr2 + nr3 + nr4 + nr5 + nr6 < 2 ) {
				ngspipelines.ui.display("error", 'Select at least 1 library in each pool!', $("#" + opts.prefix + "-run-error"));
				$("#" + opts.prefix + "-run-error").delay(1500).fadeOut(800);
			}
			else {
		        var $dialog = $('#ngspipelines-dialog').dialog({
		            draggable: true,
		            resizable: false,
		            modal: true,
		            autoOpen: false,
		            width: 960,
		            position: ['center',20],
		            title: "Venn diagram analysis",
		            buttons : { 
		            	"close": function() { 
		            		$(this).dialog("close"); 
		            	}
		            }
		        });
		        $('#ngspipelines-dialog-body').venn({
					countFile: countFileWebPath,
					pools: {Pool1: r1, Pool2: r2, Pool3: r3, Pool4: r4, Pool5: r5, Pool6: r6},
					rootName: opts.rootName,
					martName: opts.martName,
					report: opts.report,
					prefix: opts.prefix
				});
		        $dialog.dialog('open');
			}
		}
		
		_run.dea = function() {
			// Retrieve pool1_array and pool2_array library names (radio0 for pool1_array and radio1 for pool2_array
			var	pool1_array = [],
				pool2_array = [];
			
			$("#" + opts.prefix + "-libraries-table td[class^=radio]").each(function(){
				var $td = $(this);
				
				if( $td.hasClass("radio0")  && $td.children("input[type=radio]:checked").size() > 0 ){
					pool1_array.push($td.children("input").attr("name"));
				}
				
				if( $td.hasClass("radio1")  && $td.children("input[type=radio]:checked").size() > 0 ){
					pool2_array.push($td.children("input").attr("name"));
				}
			});
			
			if( pool1_array.length == 0 | pool2_array.length == 0  ){
				ngspipelines.ui.display("error", 'Select at least 1 library in each pool!', $("#" + opts.prefix + "-run-error"));
				$("#" + opts.prefix + "-run-error").delay(1500).fadeOut(800);
			}
			else {
				var $dialog = $('#ngspipelines-dialog').dialog({
		            draggable: true,
		            resizable: false,
		            modal: true,
		            autoOpen: false,
		            width: 900,
		            position: ['center',20],
		            title: "DEAnalysis",
		            buttons : { 
		            	"close": function() { 
		            		$(this).dialog("close"); 
		            	}
		            }
		        });
				$('#ngspipelines-dialog-body').deaform({
					pool1: pool1_array,
					pool2: pool2_array ,
					analysisDataset: opts.analyses,
					projectDirectory: opts.projectDirectory,
					rootName: opts.rootName,
					martName: opts.martName,
					report: opts.report,
					prefix: opts.prefix
				});
				
				$dialog.dialog('open');
			}
		};
		
		/**
		 * Run the dea from an analysis id key
		 */
		
		_run.deaFromId = function( dea_id_key ) {
			
			var $dialog = $('#ngspipelines-dialog').dialog({
			 draggable: true,
	            resizable: false,
	            modal: true,
	            autoOpen: false,
	            width: 900,
	            position: ['center',20],
	            title: "DEAnalysis",
	            buttons : { 
	            	"close": function() { 
	            		$(this).dialog("close"); 
	            	}
	            }
	        });
			$('#ngspipelines-dialog-body').deaform({
				analysisDataset: opts.analyses,
				rootName: opts.rootName,
				projectDirectory: opts.projectDirectory,
				martName: opts.martName,
				rootName: opts.rootName,
				prefix: opts.prefix,
				report: opts.report,
				dea_id_key: dea_id_key
			});
			$dialog.dialog('open');
		};
		
		var _initListeners = function () {
			var radioCheck;
			$("#" + opts.prefix + "-libraries-table td[class^=radio] input[type=radio]").bind({
				
				click : function(){ 
					if ( radioCheck ){
						$(this).attr("checked", false);
					}
					else{
						$(this).attr("checked", true);
					}
				},
				mousedown : function(){
					radioCheck = $(this).attr('checked');
				}
			}); 
		}
		
		this.each(function() {
			
			var $t = $(this);
			ngspipelines.ui.wait("Please wait until libraries are being loaded ...", "center", $t);
			_load(
				opts.libraries, 
				opts.analyses, 
				function(ldata, adata) {
					$t.html("");
					var view_analysis = {"web_analysis": false, "count" : false, "dea": false, "venn": false};
					if (adata.hasOwnProperty("libraries_count")) {
						view_analysis['count'] = true;
					}
					if (adata.hasOwnProperty("web_venn_analysis")) {
						view_analysis['venn'] = true;
						view_analysis['web_analysis'] = true;
					}
					if (adata.hasOwnProperty("cpta_NormalizeRawCount")){
						view_analysis['dea'] = true;
						view_analysis['web_analysis'] = true;
					}
					ngspipelines.utils.tmpl($t, _template, {libraries: ldata, prefix: opts.prefix, view_analysis: view_analysis}, function(){						
						_initLibrariesTable(adata);
						_initListeners();						
						//DEA results from a link
						var params = ngspipelines.utils.simpleQueryParams(location.href),
							dea_id_key =  params["deaAnalysisIDKey"];
						if (dea_id_key){
							_run.deaFromId(parseInt(dea_id_key));
						}
					});
				},
				function(errorMsg) {
					$t.html("");
					ngspipelines.utils.tmpl($t, _template, {libraries: {}, alphaArray : ngspipelines.application.dea.getAlphaValues(),normalizationArray:ngspipelines.application.dea.getNormValues(), prefix: opts.prefix, has_count_analysis: false}, function(){	
						_initLibrariesTable();
						_initListeners();
					});
				}
			);

		});
		return this;
	};
})(jQuery);