(function($) {
	$.fn.variantreport = function(options) {
		
		var _template = "js/view/variantdenovo/variantreport.tmpl",
			_allelesTemplate = "js/view/variantdenovo/alleles.tmpl",
			_featuresTemplate = "js/view/variantdenovo/featurestable.tmpl",
			
			_pluginIsInit = new Array(),
			
	        // default option values
			defaults = {
				id: "",
				dataset: {},
				analyses: {},
				project : {},
				rootName: "",
	        	martName: "",
	        	datasetType: "",
				callback: function() {}
	    	};
	    
	    // merge default option values and provided parameters 
		var opts = $.extend(defaults, options);
		
		var _initFeatures = function() {
			ngspipelines.ui.wait("Please wait until features are being loaded ...", "center", $("#variant-features"));
			ngspipelines.variantdenovo.variantreport.getSNPAnnotations(
				opts.id, 
				opts.dataset,
				function (annotations) {
					$("#variant-features").html("");
					ngspipelines.utils.tmpl($("#variant-features"), _featuresTemplate, {annotations: annotations}, function(){
						// features table actions and initialization
						$('#features-table').dataTable({
					  		"sPaginationType": "full_numbers",
					  		"aaSorting": [[ 1, "desc" ]],
					  		"sScrollX": "100%",
							"sScrollXInner": "150%",
							"bScrollCollapse": true
						});
					});
				}, 
				function (errorMsg) {
					$("#variant-features").html("");
					ngspipelines.utils.tmpl($("#variant-features"), _featuresTemplate, {annotations: {}}, function(){
						// features table actions and initialization
						$('#features-table').dataTable({
					  		"sPaginationType": "full_numbers",
					  		"aaSorting": [[ 1, "asc" ]]
						});
					});
				}
			)
		}
		
		var _initAlleles = function() {
			ngspipelines.ui.wait("Please wait until alleles are being loaded ...", "center", $("#alleles-table-wrapper"));
			ngspipelines.variantdenovo.variantreport.getAllelesCount(
				opts.id, 
				opts.dataset,
				function (data) {
					
					var series = new Array(),
						values = new Array(),
						allelesTotal = {},
						libsTotal = {},
						total = 0;
					
					for (var index in data.libraries) {
		        		if (index != "remove") {
		        			libsTotal[data.libraries[index]] = 0;
		        			for (var index2 in data.alleles) {
				        		if (index2 != "remove") {
				        			if (data.allelesCount[data.libraries[index]].hasOwnProperty(data.alleles[index2])) {
				        				libsTotal[data.libraries[index]] += parseInt(data.allelesCount[data.libraries[index]][data.alleles[index2]]);
				        			}
				        		}
		        			}
		        		}
					}
					
		        	for (var index in data.alleles) {
		        		if (index != "remove") {
		        			values = new Array();
		        			allelesTotal[data.alleles[index]] = 0;
		        			for (var index2 in data.libraries) {
				        		if (index2 != "remove") {
				        			if (data.allelesCount[data.libraries[index2]].hasOwnProperty(data.alleles[index])) {
				        				values.push(data.allelesCount[data.libraries[index2]][data.alleles[index]]);
				        				allelesTotal[data.alleles[index]] += parseInt(data.allelesCount[data.libraries[index2]][data.alleles[index]]);
				        				total += parseInt(data.allelesCount[data.libraries[index2]][data.alleles[index]]);
				        			} else {
				        				values.push(0);
				        			}
				        		}
				        	}
		        			series.push({
		        				name: data.alleles[index],
				                data: values
		        			});
		        		}
		        	}
		        	
					$("#alleles-table-wrapper").html("");
					ngspipelines.utils.tmpl($("#alleles-table-wrapper"), _allelesTemplate, 
											{alleles: data.alleles, allelesCount: data.allelesCount, 
											libraries: data.libraries, allelesTotal: allelesTotal, 
											libsTotal: libsTotal, total: total}, function(){
						
						$('#alleles-table').dataTable({
							"bFilter": false,
							"bInfo": false,
							"bPaginate": false
							//"aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0,1,2,3,4,5,6 ] } ]
						});
						
						chart = new Highcharts.Chart({
				            chart: {
				                renderTo: 'alleles-graph',
				                type: 'column'
				            },
				            title: { text: null },
				            xAxis: {
				                categories: data.libraries
				            },
				            credits: { enabled: false },
				            yAxis: {
				                min: 0,
				                title: {
				                    text: 'Depth'
				                }
				            },
				            tooltip: {
				                formatter: function() {
				                    return 'Allele <b>'+
				                        this.series.name +'</b> is present <b>'+ this.y +' times </b> ('+ Math.round(this.percentage) +'%) in sample <b>' + this.x + '</b>';
				                }
				            },
				            plotOptions: {
				                column: {
				                    stacking: 'percent'
				                }
				            },
				            series: series
				        });
					});
				}, 
				function (errorMsg) {
					$("#alleles-table-wrapper").html("");
					ngspipelines.utils.tmpl($("#alleles-table-wrapper"), _allelesTemplate, {}, function(){});
				}
			)
		}
		
		var _initGeneralInfo = function(data) {
			
			if (data.isFavorite) {
				$("#variant-delete-add-btn").html("<i class='icon-star'></i> Delete from favorites");
				$("#favorite-star").html("<i class='icon-star'></i>");
			} else {
				$("#variant-delete-add-btn").html("<i class='icon-star-empty'></i> Add to favorites");
				$("#favorite-star").html("");
			}
			
			$("#variant-delete-add-btn").click(function() {
				ngspipelines.utils.updateFavoriteStatus(
					[opts.id],
					"favorite_variant",
					opts.analyses,
					function (isFavorites) {
						if (isFavorites) {
							$("#variant-delete-add-btn").html("<i class='icon-star'></i> Delete from favorites");
							$("#favorite-star").html("<i class='icon-star'></i>");
						} else {
							$("#variant-delete-add-btn").html("<i class='icon-star-empty'></i> Add to favorites");
							$("#favorite-star").html("");
						}
					},
					function (errorMsg) {
						ngspipelines.ui.display("error", errorMsg, $("#report-error"));
					}
				);
			});
			
			$("#variant-export-flanking").click(function() {
				
				ngspipelines.variantdenovo.variantreport.getExportFlankingSequencesLink( 
						opts.id, 
						opts.dataset,
						function (data){
							$dialog = $('#ngspipelines-dialog').dialog({
								draggable: true,
						        resizable: false,
						        modal: true,
						        autoOpen: false,
						        width: 720,
						        position: ['center',20],
						        title: "Flanking sequences",
						        buttons : { 
						        	"close": function() { 
						        		$(this).dialog("close"); 
						            }
						        }
							});
							$.get( "/ngspipelines/data/" + data.snp_file_path, function( variation ) {
								$('#ngspipelines-dialog-body').html(
					    			' <textarea readonly style="resize:none;width:99%;height:400px;font-family:monospace;font-size:90%">' + variation + '</textarea>' +
						        	'</div>');
							});
						    $dialog.dialog('open');
						},
						function (error){
							ngspipelines.ui.display("error", errorMsg, $("#variant-report-error"));
						}
				);
			});
			
		}
		
		var _initPlugin = function($t) {
			ngspipelines.variantdenovo.variantreport.getSNP(
				opts.id, 
				opts.dataset,
				opts.analyses,
				function(data) {
					// first load the modaltemplate

					ngspipelines.utils.tmpl($t, _template, {variant: data, root: opts.rootName, mart: opts.martName}, function(){
						var pname = opts.martName.split(" ");
    					for(var i= 0; i<pname.length-1; i++) {
    					     pname[i] = pname[i].charAt(0);
    					}
    					$("#back").html("<button class='ngsp-button ngsp-btn-white ngsp-btn-small ngsp-button-back'>" +
    									"<i class='icon-arrow-left'></i> " +
    									pname.join(". ") +
    									"</button>");
						
    					var effectdatatable = $('#effect-table').dataTable({
							"aaSorting": [[ 1, "desc" ]],
					  		"sPaginationType": "full_numbers",
					  		"sScrollX": "100%",
					  		"bScrollCollapse": true
							});
    			
    					$("#report-tabs").tabs();
						$("#back").click(function(){
							window.location.href = "/ngspipelines/#!/" + opts.rootName + "/" + opts.martName + "?actDataset=" + opts.datasetType;
						});
						
						_initGeneralInfo(data);
						
						$("#report-tabs").bind("tabsselect", function(event, ui) {
				        	var type = $(ui.tab).attr("href").substring(1);
				        	if (type == "alleles" && $.inArray(type, _pluginIsInit) == -1) {
				        		_initAlleles();
				        		_pluginIsInit.push(type);
				        	} else if (type == "features" && $.inArray(type, _pluginIsInit) == -1) {
				        		_initFeatures();
				        		_pluginIsInit.push(type);
				        	}
				        });

						opts.callback();				
					});
				},
				function(errorMsg) {
					ngspipelines.utils.tmpl($t, _template, {variant: {}}, function(){
						ngspipelines.ui.display("error", errorMsg, $("#variant-report-error"));
					});
				}
			);
		}
		
		
		// Main plugin function
		this.each(function() {
			
			var $t = $(this);
			$t.html("");
			if (opts.id == parseInt(opts.id, 10)) {
				_initPlugin($t);
			} else {
				ngspipelines.variantdenovo.variantreport.getIdFromVariantName(
					opts.id, 
					opts.dataset,
					function(id) {
						opts.id = id
						_initPlugin($t);
					},
					function(errorMsg) {
						ngspipelines.utils.tmpl($t, _template, {variant: {}, root: opts.rootName, mart: opts.martName}, function(){
							ngspipelines.ui.display("error", errorMsg, $("#variant-report-error"),withBox=true);
							opts.callback();
						});
					}
				);
			}
		});
		return this;
		
	};
})(jQuery);