(function($) {
	$.fn.project = function(options) {
		
		var _template = "js/view/project.tmpl",
        _analysis_template = "js/view/analysis/analysis.tmpl",
        _analysis_template_path = "js/view/analysis/",
	        // Default option values
			defaults = {
				project : {},
	    	};

		var opts = $.extend(defaults, options);
		var project = opts.project;

		var _init_project = function ($t, _template, template_data) {
			ngspipelines.utils.tmpl($t, _template, template_data, 
				// Define template success callback function
				
				function() {
					var applicationType; 
    				$("div[id$=-log-wrapper]").css("display", "none");
    				$("li[id$=-log]:first").each(function(){
    					$(this).addClass("li-active");
    					applicationType = $(this).attr("id").split(/-/)[0];
        				$("#"+applicationType+"-log-wrapper").css("display", "inline");
        				$("#log-title").html(project.applications[applicationType].displayName);
    				})
    				$("li[id$=-log]").click(function(){
    					applicationType=$(this).attr("id").split(/-/)[0];
    					$("div[id$=-log-wrapper]").css("display", "none");
    					$("#"+applicationType+"-log-wrapper").css("display", "inline"); 
    					$("#log-title").html(project.applications[applicationType].displayName);
    					$("li[id$=-log]").removeClass("li-active");
    					$(this).addClass("li-active");
    					var table = $("#"+applicationType+"-analysis-table").dataTable();
    					table.fnDraw();
    		            table.fnAdjustColumnSizing();
    				});
    				$("[id^=analysis-link-]").click(function(){
    					var analysis_id=$(this).attr("id").split(/-/)[2];
    					var current_analysis=template_data["applications"][applicationType][analysis_id]
    					$dialog = $('#ngspipelines-dialog').dialog({
				            draggable: true,
				            resizable: false,
				            modal: true,
				            autoOpen: false,
				            width: 1025,
				            position: ['center',20], 
				            title: "Analysis details  for " + current_analysis.name,
				            buttons : { 
				            	"close": function() { 
				            		$(this).dialog("close"); 
				            	}
				            }
				        });
    					$('#ngspipelines-dialog').html("");
    					//get results
    					ngspipelines.project.getAnalysisResults(analysesDataset, analysis_id,  function(results){
    						//call specific template
    						var class_name = current_analysis.type.split("cpta_")[1];
    						ngspipelines.utils.tmpl($('#ngspipelines-dialog'), 
    										_analysis_template_path+class_name+".tmpl", 
    										{analysis: current_analysis, results: results}, 
    										function() {
    											yepnope({
    							        			load: _analysis_template_path+class_name+".js",
    							        			complete: function () {
    							        				window[class_name]();
    							        				$dialog.dialog('open');
    							        			}
    							        		});
    										},
    										//if specific template doesn't exist ErrorCB : call generic template
    										function() {
    											ngspipelines.utils.tmpl($('#ngspipelines-dialog'), 
        												_analysis_template, 
        												{analysis: current_analysis },
        												function() {
        													$dialog.dialog('open');
        												})
    										}
    								);
    					});		
    				});
    				$("[id$=-analysis-table]").dataTable({
    					"sPaginationType": "full_numbers",
    					"sScrollX": "100%",
    					"aaSorting": [[ 1, "asc" ]],
    					"aoColumns": [
    					              { "bSortable": false },
    					              { "bVisible": false },
    					              { "bSortable": false },
    					              { "bSortable": false },
    					              { "bSortable": false },
    					              { "bSortable": false }
    					             ],
    					"bScrollCollapse": true,
    					"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
    						$('td', nRow).attr('nowrap','nowrap');
    	                    return nRow;
    	                }
					});	
			});	
		}

		this.each(function() {
			
			var $t = $(this);
			ngspipelines.ui.wait("Please wait while loading PROJECT information ...", "center", $t, withBox=true);
            ngspipelines.project.getDiskSpace(
            	project.dataset, 
				// Define the ngspipelines.project.getDiskSpace success callback function
				function(size){
				    try {
				    	project.diskspace = ngspipelines.utils.getOctetStringRepresentation(size);
				    } catch(err) {
				    	project.diskspace = "--"
				    	ngspipelines.ui.display("error", err);
				    }
				    template_data = {"project" : project}
		            analysesDataset = ngspipelines.utils.getAnalysesDataset(biomart._state.datasets);				    
		            ngspipelines.project.getAnalysisByApplication(analysesDataset , function(resultsAnalysis){
		            	$t.html("");
		            	template_data.applications = resultsAnalysis;
		            	_init_project($t, _template, template_data);
            		},
	            	function(errorMsg) {
	            		$t.html("");
	            		_init_project($t, _template, template_data);
	            	});
    			},
			    // Define the ngspipelines.getDiskSpace error callback function
			    function(errorMsg) {
			    	ngspipelines.ui.display("error", errorMsg, $t, true);
			    }
    		);
	    	
		});
		return this;
	};
})(jQuery);