(function($) {
	$.fn.help = function(options) {
		
		var _template = "js/view/help.tmpl",
			_applicationTemplatePath = "js/view/",
			_applicationTemplatesHelpPath = "js/view/help/",
	        // Default option values
			defaults = {
				applications : {},
	    	};
		
		
		var opts = $.extend(defaults, options);
		var _linkHandler = function (help_link_id) {
			$("#help-title-wrapper").text($("#"+help_link_id).text());
			$("[id^='help-link-']").removeClass("li-active");
			$("#"+help_link_id).addClass("li-active");
			$("div[id^=help-content-]").css("display", "none");
			
			var id_splitted=help_link_id.split("-")
			var application = id_splitted[2];
			
			var div_wrapper="#help-content-"+application;
			if (id_splitted.length > 3){ 
				var sub_menu_value = id_splitted[3];
				div_wrapper="#help-content-"+application+"-"+sub_menu_value;
			}
			$(div_wrapper).css("display", "inline");
		}			
		
		var _setSubMenu = function (applicationName, applicationFeatures) {
			ngspipelines.utils.tmpl($("#help-link-"+applicationName), _applicationTemplatesHelpPath+"submenu.tmpl", { applicationName: applicationName, feature:applicationFeatures }, function(){
				$("[id^='help-link-"+applicationName+"-']").click(function(){
					_linkHandler( $( this ).attr("id") );
				});
			});
		}
		var _setContentDiv = function (applicationName, applicationFeatures) {
			
			for (var feature in applicationFeatures ) {
				featureName = feature.replace('_','');
				// if feature content doesn't allready exist and feature is true
				if (applicationFeatures[feature]){
					ngspipelines.help.getSpecificOrGenericContent('help-content-'+applicationName+'-'+featureName,_applicationTemplatePath+applicationName+"/help_"+featureName+".tmpl",_applicationTemplatesHelpPath+"/"+featureName+".tmpl",function(results){
						$("#help-contents-wrapper").append('<div id="'+results.divId+'" style="display:none;">'+results.html+'</div>');
					});
				}
			}
			
		}
		
		var _setStaticContent = function () {
			$.each(["ngspipelines","download","project"],function(index,menuElement){
				$("#help-link-"+menuElement).click(function(){
					_linkHandler( $( this ).attr("id") );
				});
				
				ngspipelines.utils.tmpl($("#help-content-"+menuElement),
					_applicationTemplatesHelpPath+"/"+menuElement+".tmpl", {},
					function(){	
						$("#help-content-"+menuElement).css("display", "none");
						_linkHandler("help-link-ngspipelines");
					});
			});
		}
		
		
		//main
		this.each(function() {
			var $t = $(this);
			
			//ngspipelines.ui.wait("Please wait until help is being loaded ...", "center", $t, withBox=true);
		    ngspipelines.utils.tmpl($t, _template, {applications: opts.applications} , function() {
		    	_setStaticContent();
		    	$.each(opts.applications, function(application, application_content){ 
					ngspipelines.help.getHelpFeature(
							opts.dataset, 
							opts.analyses,
							application,
							opts.libraries,
							["blast_search","libraries_count"],
							function(results){
								var all_feature={};
								$.extend(all_feature, results, {"report":true,"statistics":true,"overview":true});
								
								_setSubMenu(application,all_feature);
								_setContentDiv(application,all_feature);
							},
							function(errorMsg){
				    			ngspipelines.utils.tmpl($t, _template, {}, function() {
				    				ngspipelines.ui.display("error", errorMsg, $("#help-error"), true);
				    			});
							})
				});
		    	
	    		}
		    
	    	);
			

		});
		return this;
	};
})(jQuery);