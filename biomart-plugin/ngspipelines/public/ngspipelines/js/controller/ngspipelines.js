(function($) {
	$.fn.ngspipelines = function(options) {
		
		var _template = "js/view/ngspipelines.tmpl",
			_projectTemplate = "js/view/project.tmpl",
			_project = {},
			_applicationTypeIndex = {},
			_pluginIsInit = new Array(),
			
			// error messages 
			DATASETS_ERROR_MSG = "An error occurred while loading datasets: 3 datasets are required to " +
					"run NGSPipelines plugins: general, analysis and library! Those datasets should be grouped " +
					"using the project name. For more information check the NGSPipelines documentation.",

	        // default option values
			defaults = {
	            datasets: {},
	            callback: function() {},
	            martName: "",
	            rootName: ""
	    	};
	    
	    // merge default option values and provided parameters 
		var opts = $.extend(defaults, options);
	    
		var _initNGSpipelines = function(item) {
			ngspipelines.ui.wait("Please wait while initializing ngspipelines plugin ...", "center", item, withBox=true);
			
	    	// initialize the plugin
	    	ngspipelines.init(
	    		_project.dataset, 
	    		// Define the ngspipelines.init success callback function
	    		function() {
	    			
	    			ngspipelines.ui.wait("Please wait while loading project information ...", "center", item, withBox=true);
	    			
			        // load the data
			    	ngspipelines.project.getProject(
			    		_project.dataset, 
			    		// Define the ngspipelines.getProject success callback function
			    		function(json) {
			    			
			    			_project.info = json;
				    		_project.info.name = opts.martName;

	    				    // re-init the item
	    				    item.html("");
	    				    
				    		ngspipelines.utils.tmpl(item, _template, {project: _project}, function() {
				    			var pname = _project.info.name.split(" ");
    	    					for(var i= 0; i<pname.length-1; i++) {
    	    					     pname[i] = pname[i].charAt(0);
    	    					}
    	    					$("#tab-pname").text(pname.join(". "));
				    			
		    				    // Init the tabs
						        var params = ngspipelines.utils.simpleQueryParams(location.href),
					        		actDataset =  params["actDataset"];
						        
						        var actIndex = 0;
						        $("#menu-tabs ul li").each(function(i){
						        	if ($(this).children("a").attr("href") == "#"+actDataset) {
						        		actIndex = i;
						        	}
						        });
						        if (actDataset) { 
						        	$("#menu-tabs").tabs({ 
						        		selected: actIndex
						        	});
						        	ngspipelines.ui.wait("Please wait while initializing ngspipelines plugin ...", "center", $("#" + actDataset +"-wrapper"), withBox=true);
						        	$("#" + actDataset +"-wrapper").html("");
					        		// building the application
						        	ngspipelines.utils.loadPluginJSFile(
						        		_project.dataset, 
						        		_project.applications[_applicationTypeIndex[actDataset]],
						        		function() {
							        		$("#" + actDataset +"-wrapper").application({
							                	dataset: _project.applications[_applicationTypeIndex[actDataset]],
							                	libraries: _project.libraries,
							                	analyses: _project.analyses,
							                	projectDirectory: "/ngspipelines/data/" + _project.info.directory,
							                	martName: opts.martName,
							                	rootName: opts.rootName,
							                	prefix: _applicationTypeIndex[actDataset]
							                });
							        		_pluginIsInit.push(actDataset);
						        		},
					        			function(errorMsg) { 
						        			//TODO
						        		}
					        		);
						        } else {
						        	$("#menu-tabs").tabs();
						        }
						        $("#project-wrapper").project({	project: _project});
						        
						        $("#menu-tabs").bind("tabsselect", function(event, ui) {
						        	var type = $(ui.tab).attr("href").substring(1);
						        	if (type == "download" && $.inArray("download", _pluginIsInit) == -1) {
						    	    	$("#download-wrapper").download({
						    				dataset: _project.dataset,
						    				projectDirectory: _project.info.directory
						    			});
						    	    	_pluginIsInit.push(type);
						        	} else if ( type == "project" && $.inArray("project", _pluginIsInit) == -1) {
					    	    		$("#project-wrapper").project({
					    	    			project: _project
					    	    			});
					    	    		_pluginIsInit.push(type);
							        } else if ( type == "help" && $.inArray("help", _pluginIsInit) == -1) {
					    	    		$("#help-wrapper").help({
					    	    			applications: _project.applications,
					    	    			libraries: _project.libraries,
						                	analyses: _project.analyses
					    	    			});
					    	    		_pluginIsInit.push(type);
						        	} 
						        	else if (type != "project" && $.inArray(type, _pluginIsInit) == -1) {
						        		ngspipelines.ui.wait("Please wait while initializing ngspipelines plugin ...", "center", $("#" + type +"-wrapper"), withBox=true);
						        		// building the application
						        		ngspipelines.utils.loadPluginJSFile(
						        			_project.dataset, 
						        			_project.applications[_applicationTypeIndex[type]],
						        			function() {						        				
						        				$("#" + type +"-wrapper").html("");
						        				$("#" + type +"-wrapper").application({
								                	dataset: _project.applications[_applicationTypeIndex[type]],
								                	libraries: _project.libraries,
								                	analyses: _project.analyses,
								                	projectDirectory: "/ngspipelines/data/" + _project.info.directory,
								                	martName: opts.martName,
								                	rootName: opts.rootName,
								                	prefix: _applicationTypeIndex[type]
								                });
								        		_pluginIsInit.push(type);
						        			},
						        			function(errorMsg) {
						        				//TODO
						        			}
						        		);
						        	}	
						        });
				    		});
			    		},
			    		// Define the ngspipelines.getProject error callback function
			    		function(errorMsg) {
			    			ngspipelines.ui.display("error", errorMsg, item, true);
			    		}
			    	);
			    },
			    // Define the ngspipelines.init error callback function
			    function(errorMsg) {
			    	ngspipelines.ui.display("error", errorMsg, item, true);
			    }
	    	);
	    }

	    // Main plugin function
		this.each(function() {
			// First grab parameters
			_project.applications = {};
            for (var i=0, ds; ds=opts.datasets[i]; i++) {
            	if (ngspipelines.utils.isAProjectDataset(ds)) {            		
            		_project.dataset = ds;
            	} else if (ngspipelines.utils.isAnAnalysisDataset(ds)) {
            		_project.analyses = ds;
            	} else if (ngspipelines.utils.isALibrariesDataset(ds)) {
                 	_project.libraries = ds;
                } else {
                	// add application type
                	var type = ngspipelines.utils.getDatasetType(ds);
                	ds.appType = type;
			    	_project.applications[type] = ds;
			    	_applicationTypeIndex[type] = type;
            	}
            }
            // Then check if required dataset are present
            var $t = $(this);
            if (ngspipelines.datasetsAreOK(_project)) {
                _initNGSpipelines($t);
            } else {
            	ngspipelines.ui.display("error", DATASETS_ERROR_MSG);
            }
            opts.callback();
		});
		return this;
	};
})(jQuery);
