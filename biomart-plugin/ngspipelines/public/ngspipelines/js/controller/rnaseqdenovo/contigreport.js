(function($) {
	$.fn.contigreport = function(options) {
		
		var	_template                 = "js/view/rnaseqdenovo/contigsreport.tmpl",
			_depthTableTemplate       = "js/view/rnaseqdenovo/depthtable.tmpl",
			_featuresTableTemplate    = "js/view/rnaseqdenovo/featurestable.tmpl",
			_predictionsTableTemplate = "js/view/rnaseqdenovo/predictionstable.tmpl",
			_variantsTableTemplate 	  = "js/view/rnaseqdenovo/variantstable.tmpl",
			_sequenceTabTemplate      = "js/view/rnaseqdenovo/sequencetab.tmpl",
			
			NO_LIBRARY_SELECTED_ERROR_MSG = "No library selected!",
			_pluginIsInit = new Array(),
			
	        // default option values
			defaults = {
				id: "",
				dataset: {},
				analyses: {},
				project : {},
				rootName: "",
	        	martName: "",
	        	datasetType: "",
	        	labelColors: Highcharts.theme.colors,
	        	application: "rnaseqdenovo",
				callback: function() {}
	    	};
	    
	    // merge default option values and provided parameters 
		var opts = $.extend(defaults, options);
		
		var parseFloat10 = function(val) { return parseFloat(val,10); }
		
		var _updateDepthGraph = function(libraries) {
			ngspipelines.ui.wait("Please wait while generating the graph ...", "center", $("#contig-depth-graph"));			

			// first parse the library table in order to know labels and libraries associated
			var labels = {},
				step = -1,
				series = new Array(),
				categories = new Array(),
				nbStep = -1,
				labelColors = {};
			
			// first gather all coverage libraries per labels
			$("[id^=library_]").each(function(){
				
				var index = $(this).attr("id").split("_").slice(-1)[0],
					ccoverage = "";

				var lib_id = $("#sample_"+index).attr("library_id") ;
				for( var libName in libraries ) {
					for( var libindex in libraries[libName] ) {
						if( step == -1 ) { step = parseInt(libraries[libName][libindex].window_size); }
						if( libindex != "remove" && libraries[libName][libindex].library_id == lib_id ) {
							ccoverage = libraries[libName][libindex].coverage ;
						}
					}
				}

				if (labels.hasOwnProperty($(this).html())) {
					labels[$(this).html()].push(ccoverage.split(",").map(parseFloat10));
				} else {
					labels[$(this).html()] = new Array();
					labels[$(this).html()].push(ccoverage.split(",").map(parseFloat10));
					labelColors[$(this).html()] = $(this).css("background-color");
				}
			});
			
			// then create the series table
			for (var key in labels) {				
				var mcoverage = new Array();
				for (var mindex=0; mindex<labels[key][0].length; mindex++) {
					if (nbStep == -1) { nbStep = labels[key][0].length; }
					var cvalue = 0.0;
					for (var covindex in labels[key]) {
						if (covindex != "remove") {
							cvalue += labels[key][covindex][mindex];
						}
					}
					cvalue = cvalue / labels[key].length;
					mcoverage.push(cvalue + 1);  // +1 due to log transform see below yAxis
				}
				series.push({
					name: key,
					color: labelColors[key],
					data: mcoverage
				});
			}
			
			var catindex = 1 ;
			// finaly create the cathegories array
			for (var cindex=0; cindex<nbStep; cindex++) {
				categories.push( "[" + Highcharts.numberFormat(catindex.toString(), 0) + " ; " + Highcharts.numberFormat((catindex + step -1).toString(), 0) + "]" );
				catindex += step;
			}

			var depthGraph = new Highcharts.Chart({
				chart: {
					renderTo: 'contig-depth-graph',
					type: 'line',
				},
				title: { text: null },
				credits: { enabled: false },
				xAxis: {
					title: {
						text: 'Position on the contig'
					},
					labels: { enabled: false },
					categories: categories
				},
				yAxis: {
					min: 1,
					title: {
						text: 'Depth in bp'
					},
					type: 'logarithmic',
		            minorTickInterval: 0.1,
		            labels: {
		            	formatter: function() {
		            		return this.value-1 ;
		            	}
		            }
				},
				tooltip: {
					formatter: function() {
							return '<b>'+ this.series.name + '</b><br/>'+
							Highcharts.numberFormat(this.y-1, 1) + 'bp between positions ' + this.x;
					}
				},
				series: series
			});
		}

		var _initDepthTable = function() {
			ngspipelines.ui.wait("Please wait until libraries are being loaded ...", "center", $("#contig-depth-table"));
			ngspipelines.ui.wait("Please wait while generating the graph ...", "center", $("#contig-depth-graph"));			

			ngspipelines.rnaseqdenovo.contigsreport.getContigLibraries(
				opts.id, 
				opts.dataset,
				function (libraries, libraryNames) {
					$("#contig-depth-table").html("");
					ngspipelines.utils.tmpl($("#contig-depth-table"), _depthTableTemplate, {libraries: libraries, labelColors: opts.labelColors}, function(){
						
						// Init the table itself
						var depthLibTable = $("#depth-libraries-table").dataTable({
							"aaSorting": [[ 2, "asc" ]],
					  		"sPaginationType": "full_numbers",
					  		"aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0, 1 ] } ],
					  		"oLanguage": {
					  	         "sLengthMenu": 'Show <select>'+
					             '<option value="10">10</option>'+
					             '<option value="25">25</option>'+
					             '<option value="50">50</option>'+
					             '<option value="100">100</option>'+
					             '<option value="-1">All</option>'+
					             '</select> entries'
					         },
							"iDisplayLength": 25,
					  		"sDom": 'lfrtiTp',
					  		"oTableTools": {
								"sSwfPath": "js/lib/TableTools-2.1.4/swf/copy_csv_xls_pdf.swf",
								"aButtons": [
								             {
								            	 "sExtends": "copy",
								            	 "sButtonText": "Copy"
								             },
								             {
								            	 "sExtends": "csv",
								            	 "sButtonText": "Save to CSV",
								            	 "sTitle": "depth_view_libraries"
								             }
								            ]
							}
						});
						
						// Define apply table button action
						$("#apply-label").click(function(){
							if ($(":checked[id^=chk_library_]").size() == 0) {
								ngspipelines.ui.display("error", NO_LIBRARY_SELECTED_ERROR_MSG, $("#depth-contig-error"));
								$("#depth-contig-error").delay(1500).fadeOut(800);
							}
							else {
								// find out which label should be checked by default
								var libraryCount = new Array(),
									libMax = "";
								for (var index in libraryNames) {
			            			if (index != "remove") {
			            				libraryCount.push(0);
			            			}
								}
								$(":checked[id^=chk_library_]").each(function(){
									var index = $(this).attr("id").split("_").slice(1).join("_");
									libraryCount[libraryNames.indexOf($("#"+index).html())] +=1;
								});
								libMax = libraryNames[libraryCount.indexOf(ngspipelines.utils.max(libraryCount))];
								
						        var labelHTML = '';
			            		for (var index in libraryNames) {
			            			if (index != "remove") {
			            				labelHTML += '<div class="row-fluid">';
			            				if (libMax == libraryNames[index]) {
			            					labelHTML += '<div class="span1 offset1"><input checked id="radio_' + libraryNames[index] + '_' + index + '" type="radio" name="labels" value="' + libraryNames[index] + '"></div>';
			            				} else {
			            					labelHTML += '<div class="span1 offset1"><input id="radio_' + libraryNames[index] + '_' + index + '" type="radio" name="labels" value="' + libraryNames[index] + '"></div>';
			            				}
			            				labelHTML += '<div class="span2" style="margin-top:-4px;"><input id="input_' + libraryNames[index] + '_' + index + '" type="text" value="' + libraryNames[index] + '"></div>';
			            				labelHTML += '<div class="span3 offset3"><span class="label" id="label_' + libraryNames[index] + '_' + index + '" style="background-color:' + opts.labelColors[index] + '">' + libraryNames[index] + '</span></div>';
			            				labelHTML += '</div>';
			            			}
			            		}
			            		
		        				labelHTML += '<div id="add-label-row" class="row-fluid">';
		        				labelHTML += '<div class="span2 offset2" style="margin-top:4px;"><input id="add-label-input" type="text" maxlength="15" value="new"></div>';
		        				labelHTML += '<div class="span3 offset3" style="margin-top:6px;"><button id="add-label" class="ngsp-button ngsp-btn-white ngsp-btn-small" type="button" ><i class="icon-plus-sign"></i> Add</button></div>';
		        				labelHTML += '</div>';
		        				$('#report-dialog-body').html(labelHTML);
						        
		        				var inputChangeHandler = function() {
		        					var parts = $(this).attr("id").split("_"),
					        			cLibName = parts.slice(1,-1).join("_");
		        					$("#label_"+cLibName+"_"+parts.slice(-1)[0]).html($(this).val());
		        					$("#radio_"+cLibName+"_"+parts.slice(-1)[0]).val($(this).val());
		        				}

						        $("[id^=input_]").bind("change", inputChangeHandler);
						        $("#add-label").click(function(){
						        	
						        	var index = $("[id^=label_]").size(),
						        		html = '<div class="row-fluid">';

						        	html += '<div class="span1 offset1"><input id="radio_' + $("#add-label-input").val() + '_' + index + '" type="radio" name="labels" value="' + $("#add-label-input").val() + '"></div>';
						        	html += '<div class="span2" style="margin-top:-4px;"><input id="input_' + $("#add-label-input").val() + '_' + index + '" type="text" value="' + $("#add-label-input").val() + '"></div>';
						        	html += '<div class="span3 offset3"><span class="label" id="label_' + $("#add-label-input").val() + '_' + index + '" style="background-color:' + opts.labelColors[index] + '">' + $("#add-label-input").val() + '</span></div>';
						        	html += '</div>';
						        	$("#add-label-row").before(html);
						        	$("#input_" + $("#add-label-input").val() + '_' + index).bind("change", inputChangeHandler);
						        	$("#add-label-input").val("new");
						        	
						        });
						        $dialog = $('#report-dialog').dialog({
						            draggable: true,
						            resizable: false,
						            modal: true,
						            autoOpen: false,
						            width: 450,
						            height: 500,
						            title: "Label as",
						            buttons: {
						                "Cancel": function() {
						                    $(this).dialog('close');
						                },
						            	"Apply": function() {
						            		
						            		var index = $('input[name=labels]:checked').attr("id").split("_").slice(1).join("_"),
						            			label = $('input[name=labels]:checked').attr("id").split("_").slice(1,-1).join("_"),
						            			style = $("#label_"+index).attr("style");
						            		// modify checked values
											$(":checked[id^=chk_library_]").each(function(){
												var parts = $(this).attr("id").split("_");
												$("#library_" + parts.slice(-1)[0]).html($('input[name=labels]:checked').val());
												$("#library_" + parts.slice(-1)[0]).attr("style", style);
												$(this).attr('checked', false);
											});
											// and all library using the same labels
											$("[id^=library_]").each(function(){
												if ($(this).html() == label) {
													$(this).html($('input[name=labels]:checked').val());
												}
											});
											libraryNames = new Array();
											$("[id^=label_]").each(function(){
												libraryNames.push($(this).html());
											});
						            		_updateDepthGraph(libraries);
						            		$(this).dialog('close');
						            	}
						            }
						        });
						        $dialog.dialog('open');
							}
						});
						$("#meandepth-min").keyup( function() { depthLibTable.fnDraw(); } );
						$("#meandepth-max").keyup( function() { depthLibTable.fnDraw(); } );
						$("#nbofseq-min").keyup( function() { depthLibTable.fnDraw(); } );
						$("#nbofseq-max").keyup( function() { depthLibTable.fnDraw(); } );
						$("#lib-name").keyup( function() { depthLibTable.fnDraw(); } );
						
						$.fn.dataTableExt.afnFiltering.push(
						    function( oSettings, aData, iDataIndex ) {
						        var iMin = $("#meandepth-min").val() * 1;
						        var iMax = $("#meandepth-max").val() * 1;
						        var iVersion = aData[5] == "-" ? 0 : aData[5]*1;
						        if ( iMin == "" && iMax == "" ) { return true; }
						        else if ( iMin == "" && iVersion < iMax ) { return true; }
						        else if ( iMin < iVersion && "" == iMax ) { return true; }
						        else if ( iMin < iVersion && iVersion < iMax ) { return true; }
						        return false;
						    }
						);
						$.fn.dataTableExt.afnFiltering.push(
						    function( oSettings, aData, iDataIndex ) {
						        var iMin = $("#nbofseq-min").val() * 1;
						        var iMax = $("#nbofseq-max").val() * 1;
						        var iVersion = aData[6] == "-" ? 0 : aData[6]*1;
						        if ( iMin == "" && iMax == "" ) { return true; }
						        else if ( iMin == "" && iVersion < iMax ) { return true; }
						        else if ( iMin < iVersion && "" == iMax ) { return true; }
						        else if ( iMin < iVersion && iVersion < iMax ) { return true; }
						        return false;
						    }
						);
						$.fn.dataTableExt.afnFiltering.push(
						    function( oSettings, aData, iDataIndex ) {
						        var libNames = $("#lib-name").val().split(/\s+/);
						        if ($.inArray(aData[2], libNames) == -1) { return true; }
						        return false;
						    }
						);
						
						$("#depth-graph-redraw").click(function(){
							_updateDepthGraph(libraries);
						});
						
						_updateDepthGraph(libraries);
					});
				}, 
				function (errorMsg) {
					$("#contig-depth-table").html("");
					ngspipelines.utils.tmpl($("#contig-depth-table"), _depthTableTemplate, {}, function(){
						ngspipelines.ui.display("error", errorMsg, $("#report-depth-table-error"));
						// Init the table itself
						$("#depth-libraries-table").dataTable({
							"aaSorting": [[ 2, "asc" ]],
							"iDisplayLength": 25,
					  		"sPaginationType": "full_numbers",
					  		"aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0, 1 ] } ]
						});
					});
				}
			)
		}
		
		var _initGeneralInfo = function (data) {
			$("#contig-delete-add-btn").click(function() {
				ngspipelines.utils.updateFavoriteStatus(
					[opts.id],
					"favorite_contig",
					opts.analyses,
					function (isFavorites) {
						if (isFavorites) {
							$("#contig-delete-add-btn").html("<i class='icon-star'></i> Delete from favorites");
							$("#favorite-star").html("<i class='icon-star'></i>");
						} else {
							$("#contig-delete-add-btn").html("<i class='icon-star-empty'></i> Add to favorites");
							$("#favorite-star").html("");
						}
					},
					function (errorMsg) {
						ngspipelines.ui.display("error", errorMsg, $("#report-error"));
					}
				);
			});
			
			if (data.isFavorite) {
				$("#contig-delete-add-btn").html("<i class='icon-star'></i> Delete from favorites");
				$("#favorite-star").html("<i class='icon-star'></i>");
			} else {
				$("#contig-delete-add-btn").html("<i class='icon-star-empty'></i> Add to favorites");
				$("#favorite-star").html("");
			}
									
			$("#contig-export-sequence").click(function() {
				ngspipelines.rnaseqdenovo.contigsreport.getExportContigSequenceLink( 
						opts.id, 
						opts.dataset, 
						function ( data ) {
							$dialog = $('#ngspipelines-dialog').dialog({
								draggable: true,
						        resizable: false,
						        modal: true,
						        autoOpen: false,
						        width: 600,
						        position: ['center',20],
						        title: "Contig sequence",
						        buttons : { 
						        	"close": function() { 
						        		$(this).dialog("close"); 
						            }
						        }
							});
							$.get( "/ngspipelines/data/" + data.fasta_file_path, function( seq ) {
								$('#ngspipelines-dialog-body').html(
					    			' <textarea readonly style="resize:none;width:99%;height:400px;font-family:monospace;font-size:90%">' + seq + '</textarea>' +
						        	'</div>');
							});
						    $dialog.dialog('open');
						},
						function ( errorMsg) {
							ngspipelines.ui.display("error", errorMsg, $("#report-error"));
						}
				);
			});

			$("#contig-export-features").click(function() {
				ngspipelines.rnaseqdenovo.contigsreport.getNameFromContigId( 
						opts.id,
						opts.dataset,
						function ( contigName ) {
							ngspipelines.rnaseqdenovo.contigsreport.getExportContigFeaturesLink( 
									opts.id,
									contigName,
									opts.analyses,
									function ( data ) {
										$dialog = $('#ngspipelines-dialog').dialog({
											draggable: true,
									        resizable: false,
									        modal: true,
									        autoOpen: false,
									        width: 950,
									        position: ['center',20],
									        title: "Contig annotations",
									        buttons : { 
									        	"close": function() { 
									        		$(this).dialog("close"); 
									            }
									        }
										});
										$.get( "/ngspipelines/data/" + data.annotation_file_path, function( annot ) {
											$('#ngspipelines-dialog-body').html(
								    			' <textarea readonly style="resize:none;width:99%;height:400px;font-family:monospace;font-size:90%">' + annot + '</textarea>' +
									        	'</div>');
										});
									    $dialog.dialog('open');
									},
									function ( errorMsg) {
										ngspipelines.ui.display("error", errorMsg, $("#report-error"));
									}
							);
						},
						function ( errorMsg) {
							ngspipelines.ui.display("error", errorMsg, $("#report-error"));
						}
				);
			});

			$("#contig-export-variants").click(function() {
				ngspipelines.rnaseqdenovo.contigsreport.getExportContigSNPIndelsLink( 
						opts.id, 
						opts.dataset, 
						function ( data ) {
							$dialog = $('#ngspipelines-dialog').dialog({
								draggable: true,
						        resizable: false,
						        modal: true,
						        autoOpen: false,
						        width: 720,
						        position: ['center',20],
						        title: "Contig variations",
						        buttons : { 
						        	"close": function() { 
						        		$(this).dialog("close"); 
						            }
						        }
							});
							$.get( "/ngspipelines/data/" + data.variation_file_path, function( variation ) {
								$('#ngspipelines-dialog-body').html(
					    			' <textarea readonly style="resize:none;width:99%;height:400px;font-family:monospace;font-size:90%">' + variation + '</textarea>' +
						        	'</div>');
							});
						    $dialog.dialog('open');
						},
						function ( errorMsg) {
							ngspipelines.ui.display("error", errorMsg, $("#report-error"));
						}
				);
			});
			
			ngspipelines.ui.wait("Please wait until annotations are being loaded ...", "center", $("#contig-annotations"));
			ngspipelines.rnaseqdenovo.contigsreport.getContigAnnotations(
				opts.id, 
				opts.dataset,
				function (annotations, hrefs) {
					$("#contig-annotations").html("");
					ngspipelines.utils.tmpl($("#contig-annotations"), _featuresTableTemplate, {annotations: annotations, hrefs: hrefs}, function(){
						// features table actions and initialization
						var featuredatatable = $('#features-table').dataTable({
							"aaSorting": [[ 1, "desc" ]],
					  		"sPaginationType": "full_numbers",
					  		"sScrollX": "100%",
							"sScrollXInner": "150%",
							"bScrollCollapse": true
						});
						new FixedColumns( featuredatatable,  {"iLeftColumns": 1} );
					});
				}, 
				function (errorMsg) {
					$("#contig-annotations").html("<p>No features available.</p>");
				}
			)
			
			ngspipelines.ui.wait("Please wait until domains are being loaded ...", "center", $("#contig-predictions"));
			ngspipelines.rnaseqdenovo.contigsreport.getContigPredictions(
				opts.id, 
				opts.dataset,
				function (predictions) {
					$("#contig-predictions").html("");
					ngspipelines.utils.tmpl($("#contig-predictions"), _predictionsTableTemplate, { predictions: predictions }, function(){
						// predictions table actions and initialization
						var predictiondatatable = $('#predictions-table').dataTable({
							"aaSorting": [[ 1, "desc" ]],
							"sPaginationType": "full_numbers",
							"sScrollX": "100%",
							"sScrollXInner": "100%",
							"bScrollCollapse": true
						});
						new FixedColumns( predictiondatatable,  {"iLeftColumns": 1} );
					});	
				},
				function (errorMsg) {
					$("#contig-predictions").html("<p>No domains available.</p>");
				}
			)
			
			if (Object.keys(data.variants).length > 0)	{
				ngspipelines.utils.tmpl($("#contig-variants"), _variantsTableTemplate,	{ variants: data.variants, root: opts.rootName, mart: opts.martName }, function(){
					// variants table actions and initialization
					var featuredatatable = $('#variants-table').dataTable({
						"aaSorting": [[ 3, "asc" ]],
						"iDisplayLength": 10,
						"sPaginationType": "full_numbers",
						"sScrollX": "100%",
						"sScrollXInner": "150%",
						"bScrollCollapse": true
					});
					new FixedColumns( featuredatatable,  {"iLeftColumns": 1} );
				});	
			}
			else{
				$("#contig-variants").html("<p>No variants available.</p>")
			}
		
		}

		var _initJBrowse = function () {
			ngspipelines.ui.wait("Please wait while initializing JBrowse ...", "center", $("#jbrowse-loading"));
			$("#GenomeBrowser").html("");
			ngspipelines.rnaseqdenovo.contigsreport.getJBrowseURL(
					opts.id, 
					opts.dataset, 
					opts.analyses, 
					function( url ) {
						$("#GenomeBrowser").html("");
						$("#GenomeBrowser").append($('<iframe id="jbrowse-iframe" frameBorder="0" width="100%" height="500px"></iframe>'));
					    var iframe = document.getElementById('jbrowse-iframe');
					    iframe = (iframe.contentWindow) ? iframe.contentWindow : (iframe.contentDocument.document) ? iframe.contentDocument.document : iframe.contentDocument;
					    iframe.document.open();
					    iframe.document.write(url);
					    iframe.document.close();
						$('#jbrowse-iframe').load(function () {
							$("#jbrowse-loading").html("");
						})
					}, 
					function( url ) {
						$("#GenomeBrowser").html("");
						$("#GenomeBrowser").append($('<iframe id="jbrowse-iframe" frameBorder="0" width="100%" height="500px"></iframe>'));
					    var iframe = document.getElementById('jbrowse-iframe');
					    iframe = (iframe.contentWindow) ? iframe.contentWindow : (iframe.contentDocument.document) ? iframe.contentDocument.document : iframe.contentDocument;
					    iframe.document.open();
					    iframe.document.write(url);
					    iframe.document.close();
						$("#jbrowse-loading").html("");
					}
			);
		}
		
		// Methods for Sequence View tab
		var htmlStringToFasta = function (sequence) {
			var	tmpSeq   = sequence,
				fastaSeq = "",
				inBalise = false,
				cmp      = 0;
			for (k=0; k<sequence.length; k++) {
				var x = sequence.substr(k,1);
				if (x == "<") {
					inBalise = true;
				}
				else if (x == ">") {
					inBalise = false;
					fastaSeq = fastaSeq + x;
					continue;
				}
				if (! inBalise) {
					if(cmp == parseInt($('#seq-separator').val())) {
						fastaSeq = fastaSeq + " ";
						cmp = 0;
					}
					cmp++;
				}
				fastaSeq = fastaSeq + x;
			}
			return fastaSeq;
		}

		var cmpfound = 0;
		jQuery.fn.highlight = function (str) {
		    var regex = new RegExp(str, "gi");
		    return this.each(function () {
		        $(this).contents().filter(function() {
		            return this.nodeType == 3;
		        }).replaceWith(function() {
		            return (this.nodeValue || "").replace(regex, function(match) {
		            	cmpfound++;
		                return "<span class=\"highlight-nuc\">" + match + "</span>";
		            });
		        });
		    });
		}
		
		var searchOrf = function (str, frame) {
			var	res        = str.substr(0,frame-1),
				inorf      = false,
				tmpOrf     = 0,
				longestOrf = 0,
				tmpStart   = 0,
				start      = 0,
				tmpEnd     = 0,
				end        = 0;
			for (k=frame-1; k<str.length; k=k+3) {
				var	sstr = str.substr(k,3);
				if(/ATG/i.test(sstr)) {
					if(!inorf) {
						res = res + "<span class=\"highlight-orf\">";
						inorf    = true;
						tmpOrf   = 0;
						tmpStart = k;
					}
					res = res + "<span class=\"codon-atg\">" + sstr + "</span>";
					tmpOrf += 3;
				}
				else if(/TAA|TAG|TGA/i.test(sstr)) {
					res = res + "<span class=\"codon-stop\">" + sstr + "</span>";
					if(inorf) {
						res = res + "</span>";
						inorf  = false;
						tmpEnd = k;
						if(tmpOrf > longestOrf) {
							longestOrf = tmpOrf;
							start      = tmpStart + 1;
							end        = tmpEnd + 3;
						}
					}
				}
				else {
					res = res + sstr;
					tmpOrf += 3;
				}
				// 3' truncated seq
				if(k+3 >= str.length && inorf && tmpOrf > longestOrf) {
					res = res + "</span>";
					longestOrf = tmpOrf-3 + sstr.length;
					start      = tmpStart + 1;
					end        = "...";
				}
			}
			var mess = "Longest ORF for the ";
			if( $('#seq-revseq').is(':checked') ) { mess += "<strong>reverse</strong> "; }
			else { mess += "<strong>forward</strong> " }
			mess += "strand in frame <strong>" + frame + "</strong>: <strong>" + longestOrf + "</strong>bp";
			if(longestOrf != 0) {
				mess += " (start: <strong>" + start + "</strong> - end: <strong>" + end + "</strong>)";
			}
			ngspipelines.ui.display("info", mess, $("#seq-orf-error"));
			return res;
		}
		
		var getLongestOrf = function (str) {
			// Tab with 6 values corresponding to frames 1 2 3 -1 -2 -3
			var	inorf      = [false, false, false, false, false, false],
				tmpOrf     = [0, 0, 0, 0, 0, 0],
				longestOrf = [0, 0, 0, 0, 0, 0],
				revstr     = jquery.bio.revCompSeq(str);
			//         Frame Length
			var res = [0,    0];
			
			for (k=0; k<str.length; k=k+3) {
				var	sstr = [ str.substr(k,3),
				   	         str.substr(k+1,3),
				   	         str.substr(k+2,3),
				   	         revstr.substr(k,3),
				   	         revstr.substr(k+1,3),
				   	         revstr.substr(k+2,3) ];

				for (i=0; i<6; i++) {
					if(/ATG/i.test(sstr[i])) {
						if(!inorf[i]) {
							inorf[i]  = true;
							tmpOrf[i] = 0;
						}
						tmpOrf[i] += 3;
					}
					else if(/TAA|TAG|TGA/i.test(sstr[i])) {
						if(inorf[i]) {
							inorf[i] = false;
							if(tmpOrf[i] > longestOrf[i]) {
								longestOrf[i] = tmpOrf[i];
							}
						}
					}
					else {
						tmpOrf[i] += 3;
					}
					// 3' truncated seq
					if(k+3 > str.length && inorf[i] && tmpOrf[i] > longestOrf[i]) {
						longestOrf[i] = tmpOrf[i]-3 + sstr[i].length;
					}
				}
			}
			var longestIndex = 0;
			for (i=1; i<6; i++) {
				if(longestOrf[i] > longestOrf[longestIndex]) {
					longestIndex = i;
				}
			}
			if		(longestIndex == 0) { res[0] = 1;  }
			else if	(longestIndex == 1) { res[0] = 2;  }
			else if	(longestIndex == 2) { res[0] = 3;  }
			else if	(longestIndex == 3) { res[0] = -1; }
			else if	(longestIndex == 4) { res[0] = -2; }
			else if	(longestIndex == 5) { res[0] = -3; }
			res[1] = longestOrf[longestIndex];
			return res;
		}
		
		var getPos = function() {
			var	res     = "<br/>",
				lineLen = parseInt($('#seq-separator').val()),
				seqLen  = parseInt($('#seq-extract-to').val()) - parseInt($('#seq-extract-from').val()) + 1;
			if(lineLen == 10) { lineLen = 90; }
			if(lineLen == 3)  { lineLen = 75; }
			
			for (k=lineLen; k<seqLen; k+=lineLen) {
				res = res + k + "<br/>";
			}
			res = res + seqLen;
			return res;
		}
		
		/*
		 * Update Sequence view with user params
		 * Param 1 : biomart query result
		 * Param 2 : boolean for highlight (search nuc)
		 */
		var displaySeqView = function (result, highlighter) {
			// Extractseq
			var fastaSeq = result.Sequences.substring($('#seq-extract-from').val()-1,$('#seq-extract-to').val());
			// Revseq
			if ( $('#seq-revseq').is(':checked')) { fastaSeq = jquery.bio.revCompSeq(fastaSeq); }
			// ORF
			if($('#seq-orf-frame').val() != 0) {
				fastaSeq = searchOrf(fastaSeq, parseInt($('#seq-orf-frame').val()));
			}
			// Display
			$('#sequence-display').html(">" + result.Name + "<br />" + htmlStringToFasta(fastaSeq));
			// Highlight
			if(highlighter && $('#seq-search-nuc').val().length > 2) {
				var searchNuc = "";
				for (k=0; k<$('#seq-search-nuc').val().length; k++) {
					var x = $('#seq-search-nuc').val().substr(k,1);
					if (x != " ")	{ searchNuc += " ?" + x; }
				}
				cmpfound = 0;
				$('#sequence-display').highlight(searchNuc);
				var mess = "Search: ";
				if(cmpfound==0)		  { mess += "not found!";	}
				else if(cmpfound==1) { mess += "<strong>" + cmpfound + "</strong> match!"; }
				else			      { mess += "<strong>" + cmpfound + "</strong> matches!"; }
				ngspipelines.ui.display("info", mess, $("#seq-orf-error"));
			}
			// Pos
			$('#sequence-pos').html(getPos());
		}
		
		var _initSequenceTab = function () {
			ngspipelines.ui.wait("Please wait while initializing sequence view ...", "center", $("#contig-sequence"));
			ngspipelines.rnaseqdenovo.contigsreport.getContigSequence(
				opts.id, 
				opts.dataset,
				function (result) {
					$("#contig-sequence").html(
						"<div id=\"sequence-pos\"     class=\"sequence-view-pos\"> </div>" +
						"<div id=\"sequence-display\" class=\"sequence-view\"> </div>"
					);
					$('#sequence-display').html(">" + result.Name + "<br />" + htmlStringToFasta(result.Sequences));
                	$('#sequence-pos').html(getPos());

                	var seqComp = jquery.bio.compSeq(result.Sequences);
                	$('#GC').html(Math.round((seqComp[2]+seqComp[3])/result.Length*10000)/100);
					$('#A-Nb').html(seqComp[0] + " (" + Math.round(seqComp[0]/result.Length*10000)/100 + "%)");
					$('#T-Nb').html(seqComp[1] + " (" + Math.round(seqComp[1]/result.Length*10000)/100 + "%)");
					$('#C-Nb').html(seqComp[2] + " (" + Math.round(seqComp[2]/result.Length*10000)/100 + "%)");
					$('#G-Nb').html(seqComp[3] + " (" + Math.round(seqComp[3]/result.Length*10000)/100 + "%)");
					$('#other-Nb').html(seqComp[4] + " (" + Math.round(seqComp[4]/result.Length*10000)/100 + "%)");
					
					var longestOrf = getLongestOrf(result.Sequences);
					$('#longestORF').html(longestOrf[0] + " (" + longestOrf[1] + "bp)");
					
					//                  from     to
					var	formValid    = [true, true],
                	    errorMessage = "";
					
					$('#seq-extract-from').focusout( function () {
						if (! ngspipelines.utils.regex($('#seq-extract-from').val(),/^\d+$/i)) {
	                    	errorMessage = "Unexpected value for 'from', must be a positive integer.";
	                    	formValid[0] = false;
	                    }
						else if (parseInt($('#seq-extract-to').val()) < parseInt($('#seq-extract-from').val())) {
	                    	errorMessage = "'to' value must be higher than 'from'.";
	                    	formValid[0] = false;
	                    }
						else { formValid[0] = true; }
						
						if (formValid[0] && formValid[1]) {
							$("#report-sequence-tab-error").fadeOut(800);
	                    	displaySeqView(result, true);
	                    }
	                    else {
	                    	ngspipelines.ui.display("error", errorMessage, $("#report-sequence-tab-error"));
	                    }
					});
					
					$('#seq-extract-to').focusout( function () {
						if (! ngspipelines.utils.regex($('#seq-extract-to').val(),/^\d+$/i)) {
	                    	errorMessage = "Unexpected value for 'to', must be a positive integer.";
	                    	formValid[1] = false;
	                    }
						else if (parseInt($('#seq-extract-to').val()) > result.Length) {
							errorMessage = "'to' cannot be higher than contig length (" + result.Length + ").";
	                    	formValid[1] = false;
						} 
						else if (parseInt($('#seq-extract-to').val()) < parseInt($('#seq-extract-from').val())) {
	                    	errorMessage = "'to' value must be higher than 'from'.";
	                    	formValid[1] = false;
	                    }
						else { formValid[1] = true; }
						
						if (formValid[0] && formValid[1]) {
							$("#report-sequence-tab-error").fadeOut(800);
	                    	displaySeqView(result, true);
						}
	                    else {
	                    	ngspipelines.ui.display("error", errorMessage, $("#report-sequence-tab-error"));
	            		}
					});
					
					$('#seq-revseq').click( function () {
						if (formValid[0] && formValid[1]) {
							displaySeqView(result, true);
						}
					});
					
					$('#seq-orf-frame').change( function () {
						$('#seq-search-nuc').val("");
						if($('#seq-orf-frame').val() == 0) {
				        	$("#seq-orf-error").fadeOut(800);
						}
						if(formValid[0] && formValid[1]) {
							displaySeqView(result, true);
						}
					});
					
					$('#seq-search-nuc').keyup( function () {
						if (formValid[0] && formValid[1] && $('#seq-search-nuc').val().length > 2) {
							$('#seq-orf-frame').val("");
							displaySeqView(result, false);
							displaySeqView(result, true);
						}
						else if($('#seq-orf-frame').val() == 0) {
							displaySeqView(result, false);
				        	$("#seq-orf-error").fadeOut(800);
						}
					});
					$('#seq-search-nuc').focusout( function () {
						if (formValid[0] && formValid[1] && $('#seq-search-nuc').val().length > 2) {
							$('#seq-orf-frame').val("");
							displaySeqView(result, false);
							displaySeqView(result, true);
						}
						else if($('#seq-orf-frame').val() == 0) {
				        	$("#seq-orf-error").fadeOut(800);
						}
					});
					
					$('#seq-separator').click( function () {
						if(formValid[0] && formValid[1]) {
							displaySeqView(result, true);
						}
					});
					
					$("#seq-translate-btn").click(function(){
					    $dialog = $('#ngspipelines-dialog').dialog({
				            draggable: true,
				            resizable: false,
				            modal: true,
				            autoOpen: false,
				            width: 960,
				            position: ['center',20],
				            title: result.Name + " frame translation",
				            buttons : { 
				            	"close": function() { 
				            		$(this).dialog("close"); 
				            	}
				            }
				        });
					    
					    var	seq   = result.Sequences.substring($('#seq-extract-from').val()-1,$('#seq-extract-to').val());
					    	start = "start:" + $('#seq-extract-from').val(),
					    	end   = "end:" + $('#seq-extract-to').val(),
					    	frame = "";
					    if ($('#seq-revseq').is(':checked')) { frame = "-" }
					    if ($('#seq-orf-frame').val() == 0)  { frame = "1" }
					    else { frame += $('#seq-orf-frame').val(); }
					    
				        $('#ngspipelines-dialog-body').html(
				        		'<div class="bloc" style="margin-top:10px;margin-bottom:15px">' +
				        		' <div class="bloc-title"><h1>Translated sequence view <small> ' + start + ' - ' + end + ' - frame:' + frame + '</small></h1></div>' +
				        		' <div class="sequence-view\"> >' + result.Name + '#' + start + '#' + end + '#frame:' + frame +
				        		'  <br/>' + jquery.bio.translate(seq, parseInt(frame)) +
				        		' </div>' +
				        		' <div class="bloc-footer bloc-dialog-footer"></div>' +
				        		'</div>');
				        
				        $dialog.dialog('open');
					});
					
					$("#seq-translate-all-btn").click(function(){
					    $dialog = $('#ngspipelines-dialog').dialog({
				            draggable: true,
				            resizable: false,
				            modal: true,
				            autoOpen: false,
				            width: 960,
				            position: ['center',20],
				            title: result.Name + " all frame translation",
				            buttons : { 
				            	"close": function() { 
				            		$(this).dialog("close"); 
				            	}
				            }
				        });
					    
					    var	seq   = result.Sequences.substring($('#seq-extract-from').val()-1,$('#seq-extract-to').val());
				    		start = "start:" + $('#seq-extract-from').val(),
					    	end   = "end:" + $('#seq-extract-to').val();
					    
				        $('#ngspipelines-dialog-body').html(
				        		'<div class="bloc" style="margin-top:10px;margin-bottom:15px">' +
				        		' <div class="bloc-title"><h1>Translated sequence view <small> ' + start + ' - ' + end + '</small></h1></div>' +
				        		' <div class="sequence-view\">' +
				        		'  >' + result.Name + '#' + start + '#' + end + '#frame:+1' +
				        		'  <br/>' + jquery.bio.translate(seq, 1) +
				        		'  <br/><br/>' +
				        		'  >' + result.Name + '#' + start + '#' + end + '#frame:+2' +
				        		'  <br/>' + jquery.bio.translate(seq, 2) +
				        		'  <br/><br/>' +
				        		'  >' + result.Name + '#' + start + '#' + end + '#frame:+3' +
				        		'  <br/>' + jquery.bio.translate(seq, 3) +
				        		'  <br/><br/>' +
				        		'  >' + result.Name + '#' + start + '#' + end + '#frame:-1' +
				        		'  <br/>' + jquery.bio.translate(seq, -1) +
				        		'  <br/><br/>' +
				        		'  >' + result.Name + '#' + start + '#' + end + '#frame:-2' +
				        		'  <br/>' + jquery.bio.translate(seq, -2) +
				        		'  <br/><br/>' +
				        		'  >' + result.Name + '#' + start + '#' + end + '#frame:-3' +
				        		'  <br/>' + jquery.bio.translate(seq, -3) +
				        		' </div>' +
				        		' <div class="bloc-footer bloc-dialog-footer"></div>' +
				        		'</div>');
				        
				        $dialog.dialog('open');
					});
							
					$('#view-longestOrf-btn').click( function () {
						$('#seq-extract-from').val("1");
						$('#seq-extract-to').val(result.Length);
						$('#seq-search-nuc').val("");
						if(longestOrf[0]<0) {
							$('#seq-revseq').attr('checked', true);
						}
						else {
							$('#seq-revseq').attr('checked', false);
						}
						$('#seq-orf-frame').val(Math.abs(longestOrf[0]));
						displaySeqView(result, true);
					});
					
					$('#seq-reset-btn').click( function () {
						$('#seq-extract-from').val(1);
						$('#seq-extract-to').val(result.Length);
						$('#seq-revseq').attr('checked', false);
						$('#seq-search-nuc').val("");
						$('#seq-orf-frame').val("");
						$('#seq-separator').val("");
						$("#report-sequence-tab-error").fadeOut(800);
                    	$('#sequence-display').html(">" + result.Name + "<br />" + htmlStringToFasta(result.Sequences));
                    	$('#sequence-pos').html(getPos());
                    	$("#seq-orf-error").fadeOut(800);
                    	formValid = [true, true];
					});
				}, 
				function (errorMsg) {
					//TODO
				}
			)
		}
		
		var _initPlugin = function($t) {
			ngspipelines.rnaseqdenovo.contigsreport.getContig(
				opts.id, 
				opts.dataset,
				opts.analyses,
				function(data) {
					ngspipelines.rnaseqdenovo.contigsreport.getContigExternalLinks(
						opts.analyses,
						function(urls) {
							var replaced_urls = {},
								is_empty = true;
							for (key in urls) {
								replaced_urls[key] = urls[key].replace("{name}", data.name);
								is_empty = false;
							}
							ngspipelines.utils.tmpl($t, _template, {contig: data, urls: replaced_urls, is_empty: is_empty}, function(){
								var pname = opts.martName.split(" ");
    	    					for(var i= 0; i<pname.length-1; i++) {
    	    					     pname[i] = pname[i].charAt(0);
    	    					}
    	    					$("#back").html("<button class='ngsp-button ngsp-btn-white ngsp-btn-small ngsp-button-back'>" +
    	    									"<i class='icon-arrow-left'></i> " +
    	    									pname.join(". ") +
    	    									"</button>");
								
								$("#report-tabs").tabs();
								$("#back").click(function(){
									window.location.href = "/ngspipelines/#!/" + opts.rootName + "/" + opts.martName + "?actDataset=" + opts.datasetType;
								});
								_initGeneralInfo(data);
						        $("#report-tabs").bind("tabsselect", function(event, ui) {
						        	var type = $(ui.tab).attr("href").substring(1);
						        	if (type == "jbrowse" && $.inArray(type, _pluginIsInit) == -1) {
						        		_initJBrowse();
						        		_pluginIsInit.push(type);
						        	} else if (type == "depth" && $.inArray(type, _pluginIsInit) == -1) {
						        		_initDepthTable();
						        		_pluginIsInit.push(type);
						        	} else if (type == "sequence" && $.inArray(type, _pluginIsInit) == -1) {
						        		_initSequenceTab();
						        		_pluginIsInit.push(type);
						        	}
						        });
						        
						        $("#morelessGOB-button").click(function(){
						        	if($("#morelessGOB-button").hasClass("ngsp-hide")) {
						        		$("[id^=GO-B-]").slideDown();
						        		$("#morelessGOB-button").html("<i class='icon-arrow-up'></i>");
						        		$("#morelessGOB-label").html("Show less");
						        	}
						        	else {
						        		$("[id^=GO-B-]").slideUp();
									    $("#morelessGOB-button").html("<i class='icon-arrow-down'></i>");
									    $("#morelessGOB-label").html("Show more [...]");
						        	}
						        	$("#morelessGOB-button").toggleClass("ngsp-hide");
						        });
						        
						        $("#morelessGOC-button").click(function(){
						        	if($("#morelessGOC-button").hasClass("ngsp-hide")) {
						        		$("[id^=GO-C-]").slideDown();
						        		$("#morelessGOC-button").html("<i class='icon-arrow-up'></i>");
						        		$("#morelessGOC-label").html("Show less");
						        	}
						        	else {
						        		$("[id^=GO-C-]").slideUp();
									    $("#morelessGOC-button").html("<i class='icon-arrow-down'></i>");
									    $("#morelessGOC-label").html("Show more [...]");
						        	}
						        	$("#morelessGOC-button").toggleClass("ngsp-hide");
						        });
						        
						        $("#morelessGOM-button").click(function(){
						        	if($("#morelessGOM-button").hasClass("ngsp-hide")) {
						        		$("[id^=GO-M-]").slideDown();
						        		$("#morelessGOM-button").html("<i class='icon-arrow-up'></i>");
						        		$("#morelessGOM-label").html("Show less");
						        	}
						        	else {
						        		$("[id^=GO-M-]").slideUp();
									    $("#morelessGOM-button").html("<i class='icon-arrow-down'></i>");
									    $("#morelessGOM-label").html("Show more [...]");
						        	}
						        	$("#morelessGOM-button").toggleClass("ngsp-hide");
						        });
						        
								opts.callback();		
							});
						}
					);
				},
				function(errorMsg) {
					ngspipelines.utils.tmpl($t, _template, {contig: {}, urls: {}, is_empty: true}, function(){
						ngspipelines.ui.display("error", errorMsg, $("#report-error"),withBox=true);
						opts.callback();
					});
				}
			);
		}
		
	    //  Main plugin function
		this.each(function() {
			var $t = $(this);
			$t.html("");			
			if (opts.id == parseInt(opts.id, 10)) {
				_initPlugin($t);
			} else {
				ngspipelines.rnaseqdenovo.contigsreport.getIdFromContigName(
					opts.id, 
					opts.dataset,
					function(id) {
						opts.id = id
						_initPlugin($t);
					},
					function(errorMsg) {
						
						ngspipelines.utils.tmpl($t, _template, {contig: {}, urls: {}, is_empty: true}, function(){
							ngspipelines.ui.display("error", errorMsg, $("#report-error"),withBox=true);
							opts.callback();
						});
					}
				);
			}

		});
		return this;
	};
})(jQuery);
