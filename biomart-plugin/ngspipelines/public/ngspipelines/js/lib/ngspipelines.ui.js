(function($) {
$.namespace('ngspipelines.ui', function(self) {
	
	/* Display a warning / error / information message
	 */
	self.display = function(type, message, div, withBox) {
		
		if (withBox) {
			msgHTML = '<div id="biomart-results-wrapper">';
			msgHTML += ' <div style="margin-left: auto;margin-right: auto;width: 1000px;font-family: \'Ubuntu\',arial,serif;font-size:13px">';
			msgHTML += '  <div class="bloc" style="margin-top:-5px;margin-bottom:15px">';
			var msg = "";
			if (type == "error")   { msg = "<strong> Error:</strong> "   + message;	}
			if (type == "success") { msg = "<strong> Success:</strong> " + message;	}
			if (type == "warning") { msg = "<strong> Warning:</strong> " + message;	}
			if (type == "info")    { msg = message;	}
			msgHTML += '<div id="error-msg">' + msg + '</div>';
			msgHTML += '   <div class="bloc-footer"></div> </div> </div> </div>';
			div.html(msgHTML);
			$("#error-msg").removeClass("alert-error");
			$("#error-msg").removeClass("alert-success");
			$("#error-msg").removeClass("alert-info");
			$("#error-msg").addClass("alert");
			if (type == "error")   { $("#error-msg").addClass("alert-error");   }
			if (type == "success") { $("#error-msg").addClass("alert-success"); }
			if (type == "info")    { $("#error-msg").addClass("alert-info");    }
		} else {
			var msg = "";
			if (type == "error")   { msg = "<strong> Error:</strong> "   + message;	}
			if (type == "success") { msg = "<strong> Success:</strong> " + message;	}
			if (type == "warning") { msg = "<strong> Warning:</strong> " + message;	}
			if (type == "info")    { msg = message;	}
			div.html(msg);
			div.removeClass("alert-error");
			div.removeClass("alert-success");
			div.removeClass("alert-info");
			div.addClass("alert");
			if (type == "error")   { div.addClass("alert-error");   }
			if (type == "success") { div.addClass("alert-success"); }
			if (type == "info")    { div.addClass("alert-info");    }
			div.fadeIn(400);
		}
	}
	
	/* Display a wait message
	 */
	self.wait = function(message, position, div, withBox) {
		var waitHTML = '';
		
		if (withBox) {
			waitHTML += '<div id="biomart-results-wrapper">';
			waitHTML += ' <div style="margin-left: auto;margin-right: auto;width: 1000px;font-family: \'Ubuntu\',arial,serif;font-size:13px">';
			waitHTML += '  <div class="bloc" style="margin-top:-5px;margin-bottom:15px">';
			waitHTML += '   <div class="bloc-title"><h1>Wait</h1></div>';
		}
		
		if (position == "center") {
			waitHTML += '<div class="mini-wait-center">';
		} else if (position == "right") {
			waitHTML += '<div class="mini-wait-right">';
		} else {
			waitHTML += '<div>';
		}
		waitHTML += '<img src="/ngspipelines/img/mini_light_wait.gif" style="padding-right:5px">' + message + '</div>';
		
		if (withBox) {
			waitHTML += '   <div class="bloc-footer"></div> </div> </div> </div>';
		}
		
		div.html(waitHTML);
	}

});

})(jQuery);
