define('dojo/nls/dojo_fr-fr',{
'dijit/nls/loading':{"loadingState":"Chargement...","errorState":"Une erreur est survenue"}
,
'dijit/nls/common':{"buttonOk":"OK","buttonCancel":"Annuler","buttonSave":"Sauvegarder","itemClose":"Fermer"}
,
'dijit/form/nls/validate':{"invalidMessage":"La valeur indiquée n'est pas correcte.","missingMessage":"Cette valeur est requise.","rangeMessage":"Cette valeur n'est pas comprise dans la plage autorisée."}
,
'dijit/form/nls/ComboBox':{"previousMessage":"Choix précédents","nextMessage":"Plus de choix"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"#,##0.00 ¤","perMille":"‰","group":" ","percentFormat":"#,##0 %","decimalFormat":"#,##0.###","decimal":",","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});