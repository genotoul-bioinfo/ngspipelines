define('dojo/nls/dojo_cs',{
'dijit/nls/loading':{"loadingState":"Probíhá načítání...","errorState":"Omlouváme se, došlo k chybě"}
,
'dijit/nls/common':{"buttonOk":"OK","buttonCancel":"Storno","buttonSave":"Uložit","itemClose":"Zavřít"}
,
'dijit/form/nls/validate':{"invalidMessage":"Zadaná hodnota není platná.","missingMessage":"Tato hodnota je vyžadována.","rangeMessage":"Tato hodnota je mimo rozsah."}
,
'dijit/form/nls/ComboBox':{"previousMessage":"Předchozí volby","nextMessage":"Další volby"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"#,##0.00 ¤","perMille":"‰","group":" ","percentFormat":"#,##0 %","decimalFormat":"#,##0.###","decimal":",","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});