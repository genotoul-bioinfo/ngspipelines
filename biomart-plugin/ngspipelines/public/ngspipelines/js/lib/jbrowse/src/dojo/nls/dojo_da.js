define('dojo/nls/dojo_da',{
'dijit/nls/loading':{"loadingState":"Indlæser...","errorState":"Der er opstået en fejl"}
,
'dijit/nls/common':{"buttonOk":"OK","buttonCancel":"Annullér","buttonSave":"Gem","itemClose":"Luk"}
,
'dijit/form/nls/validate':{"invalidMessage":"Den angivne værdi er ikke gyldig.","missingMessage":"Værdien er påkrævet.","rangeMessage":"Værdien er uden for intervallet."}
,
'dijit/form/nls/ComboBox':{"previousMessage":"Forrige valg","nextMessage":"Flere valg"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":",","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"#,##0.00 ¤","perMille":"‰","group":".","percentFormat":"#,##0 %","decimalFormat":"#,##0.###","decimal":",","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});