define('dojo/nls/dojo_es-es',{
'dijit/nls/loading':{"loadingState":"Cargando...","errorState":"Lo siento, se ha producido un error"}
,
'dijit/nls/common':{"buttonOk":"Aceptar","buttonCancel":"Cancelar","buttonSave":"Guardar","itemClose":"Cerrar"}
,
'dijit/form/nls/validate':{"invalidMessage":"El valor especificado no es válido.","missingMessage":"Este valor es necesario.","rangeMessage":"Este valor está fuera del intervalo."}
,
'dijit/form/nls/ComboBox':{"previousMessage":"Opciones anteriores","nextMessage":"Más opciones"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"¤ #,##0.00","perMille":"‰","group":".","percentFormat":"#,##0%","decimalFormat":"#,##0.###","decimal":",","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});