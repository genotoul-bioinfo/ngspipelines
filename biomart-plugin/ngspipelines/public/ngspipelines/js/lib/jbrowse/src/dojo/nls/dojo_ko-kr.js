define('dojo/nls/dojo_ko-kr',{
'dijit/nls/loading':{"loadingState":"로드 중...","errorState":"죄송합니다. 오류가 발생했습니다."}
,
'dijit/nls/common':{"buttonOk":"확인","buttonCancel":"취소","buttonSave":"저장","itemClose":"닫기"}
,
'dijit/form/nls/validate':{"invalidMessage":"입력된 값이 올바르지 않습니다.","missingMessage":"이 값은 필수입니다.","rangeMessage":"이 값은 범위를 벗어납니다."}
,
'dijit/form/nls/ComboBox':{"previousMessage":"이전 선택사항","nextMessage":"기타 선택사항"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"¤#,##0.00","perMille":"‰","group":",","percentFormat":"#,##0%","decimalFormat":"#,##0.###","decimal":".","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});