define('dojo/nls/dojo_fi-fi',{
'dijit/nls/loading':{"loadingState":"Lataus on meneillään...","errorState":"On ilmennyt virhe."}
,
'dijit/nls/common':{"buttonOk":"OK","buttonCancel":"Peruuta","buttonSave":"Tallenna","itemClose":"Sulje"}
,
'dijit/form/nls/validate':{"invalidMessage":"Annettu arvo ei kelpaa.","missingMessage":"Tämä arvo on pakollinen.","rangeMessage":"Tämä arvo on sallitun alueen ulkopuolella."}
,
'dijit/form/nls/ComboBox':{"previousMessage":"Edelliset valinnat","nextMessage":"Lisää valintoja"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"epäluku","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"#,##0.00 ¤","perMille":"‰","group":" ","percentFormat":"#,##0 %","decimalFormat":"#,##0.###","decimal":",","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});