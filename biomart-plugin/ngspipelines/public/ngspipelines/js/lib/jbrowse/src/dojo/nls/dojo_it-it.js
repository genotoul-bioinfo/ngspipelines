define('dojo/nls/dojo_it-it',{
'dijit/nls/loading':{"loadingState":"Caricamento in corso...","errorState":"Si è verificato un errore"}
,
'dijit/nls/common':{"buttonOk":"OK","buttonCancel":"Annulla","buttonSave":"Salva","itemClose":"Chiudi"}
,
'dijit/form/nls/validate':{"invalidMessage":"Il valore immesso non è valido.","missingMessage":"Questo valore è obbligatorio.","rangeMessage":"Questo valore non è compreso nell'intervallo."}
,
'dijit/form/nls/ComboBox':{"previousMessage":"Scelte precedenti","nextMessage":"Altre scelte"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"¤ #,##0.00","perMille":"‰","group":".","percentFormat":"#,##0%","decimalFormat":"#,##0.###","decimal":",","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});