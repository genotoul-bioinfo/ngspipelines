require({cache:{
'JBrowse/Model/XHRBlob':function(){
define( [ 'dojo/_base/declare',
          'JBrowse/Model/FileBlob',
          'JBrowse/Store/RemoteBinaryFile'
        ],
        function( declare, FileBlob, RemoteBinaryFileCache ) {
var globalCache = new RemoteBinaryFileCache({
    name: 'XHRBlob'
});

var XHRBlob = declare( FileBlob,
/**
 * @lends JBrowse.Model.XHRBlob.prototype
 */
{

    /**
     * Blob of binary data fetched with an XMLHTTPRequest.
     *
     * Adapted by Robert Buels from the URLFetchable object in the
     * Dalliance Genome Explorer, which was is copyright Thomas Down
     * 2006-2011.
     * @constructs
     */
    constructor: function(url, start, end, opts) {
        if (!opts) {
            if (typeof start === 'object') {
                opts = start;
                start = undefined;
            } else {
                opts = {};
            }
        }

        this.url = url;
        this.start = start || 0;
        if (end) {
            this.end = end;
        }
        this.opts = opts;
    },

    slice: function(s, l) {
        var ns = this.start, ne = this.end;
        if (ns && s) {
            ns = ns + s;
        } else {
            ns = s || ns;
        }
        if (l && ns) {
            ne = ns + l - 1;
        } else {
            ne = ne || l - 1;
        }
        return new XHRBlob(this.url, ns, ne, this.opts);
    },

    fetch: function( callback, failCallback ) {
        globalCache.get({
            url: this.url,
            start: this.start,
            end: this.end,
            success: callback,
            failure: failCallback || function() {}
        });
    },

    read: function( offset, length, callback, failCallback ) {
        var start = this.start + offset,
            end = start + length;

        globalCache.get({
            url: this.url,
            start: start,
            end: end,
            success: callback,
            failure: failCallback || function() {}
        });
    }
});
return XHRBlob;
});
},
'JBrowse/Model/FileBlob':function(){
define([ 'dojo/_base/declare', 'dojo/has'],
       function( declare, has ) {
var FileBlob = declare( null,
/**
 * @lends JBrowse.Model.FileBlob.prototype
 */
{

    /**
     * Blob of binary data fetched from a local file (with FileReader).
     *
     * Adapted by Robert Buels from the BlobFetchable object in the
     * Dalliance Genome Explorer, which was is copyright Thomas Down
     * 2006-2011.
     * @constructs
     */
    constructor: function(b) {
        this.blob = b;
        this.size = b.size;
        this.totalSize = b.size;
    },

    slice: function(start, length) {
        var sliceFunc = this.blob.webkitSlice || this.blob.mozSlice || this.blob.slice;
        return new JBrowse.Model.Blob(
            length ? sliceFunc.call( this.blob, start, start + length )
                   : sliceFunc.call( this.blob, start )
        );
    },

    fetch: function(callback) {
        var that = this,
            reader = new FileReader();
        reader.onloadend = function(ev) {
            callback( that._stringToBuffer( reader.result ) );
        };
        reader.readAsBinaryString( this.blob );
    },

    _stringToBuffer: function(result) {
        if( ! result || typeof Uint8Array != 'function' )
            return null;

        var ba = new Uint8Array( result.length );
        for ( var i = 0; i < ba.length; i++ ) {
            ba[i] = result.charCodeAt(i);
        }
        return ba.buffer;
    }

});
return FileBlob;
});
},
'JBrowse/Store/RemoteBinaryFile':function(){
define("JBrowse/Store/RemoteBinaryFile", [
           'dojo/_base/declare',
           'dojo/_base/array',
           'dojo/has',
           'JBrowse/Store/LRUCache',
           'jszlib/arrayCopy'
       ],
       function( declare, array, has, LRUCache, arrayCopy ) {

// contains chunks of files, stitches them together if necessary, wraps, and returns them
// to satisfy requests
return declare( null,

/**
 * @lends JBrowse.Store.RemoteBinaryFile
 */
{
    constructor: function( args ) {
        this.name = args.name;

        this._fetchCount = 0;
        this._arrayCopyCount = 0;

        this.minChunkSize = 'minChunkSize' in args ? args.minChunkSize : 32768;
        this.chunkCache = new LRUCache({
            name: args.name + ' chunk cache',
            fillCallback: dojo.hitch( this, '_fetch' ),
            maxSize: args.maxSize || 20000000 // 20MB max cache size
        });

        this.totalSizes = {};
    },

    _escapeRegExp: function(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    },

    _relevantExistingChunks: function( url, start, end ) {
        // we can't actually use any existing chunks if we don't have an
        // end defined.  not possible in the HTTP spec to ask for all except
        // the first X bytes of a file
        if( !end )
            return [];

        start = start || 0;

        var fileChunks = this.chunkCache
                .query( new RegExp( '^'+this._escapeRegExp( url + ' (bytes' ) ) );

        // set 'start' and 'end' on any records that don't have them, but should
        array.forEach( fileChunks, function(c) {
                           if( c.size ) {
                               if( ! c.key.start )
                                   c.key.start = 0;
                               if( ! c.key.end )
                                   c.key.end = c.key.start + ( c.size || c.value.byteLength );
                           }
                       });

        // sort the records by start coordinate, then by length descending (so that we preferentially use big chunks)
        fileChunks = fileChunks.sort( function( a, b ) {
            return ( a.key.start - b.key.start ) || ((b.key.end - b.key.start) - ( a.key.end - a.key.start ));
        });

        // filter for the chunks that can actually be used for this request
        return array.filter( fileChunks,
                             function( chunk ) {
                                 return !( chunk.key.start > end || chunk.key.end < start );
                             }, this);
    },

    _fetchChunks: function( url, start, end, callback ) {
        start = start || 0;

        // if we already know how big the file is, use that information for the end
        if( typeof end != 'number' && this.totalSizes[url] ) {
            end = this.totalSizes[ url ]-1;
        }
        // if we know the size of the file, and end is beyond it, then clamp it
        else if( end >= this.totalSizes[url] ) {
            end = this.totalSizes[url] - 1;
        }
        // NOTE: if end is undefined, we take that to mean fetch all the way to the end of the file

        // what chunks do we already have in the chunk cache?
        var existingChunks = this._relevantExistingChunks( url, start, end );
        this._log( 'existing', existingChunks );

        var chunkToString = function() {
            return this.url+" (bytes "+this.start+".."+this.end+")";
        };

        // assemble a 'golden path' of chunks to use to fulfill this
        // request, using existing chunks where we have them cached,
        // and where we don't, making records for chunks to fetch
        var goldenPath = [];
        if( typeof end != 'number' ) { // if we don't have an end coordinate, we just have to fetch the whole file
            goldenPath.push({ key: { url: url, start: 0, end: undefined, toString: chunkToString } } );
        }
        else {
            for( var currOffset = start; currOffset <= end; currOffset = goldenPath[goldenPath.length-1].key.end+1 ) {
                if( existingChunks[0] && existingChunks[0].key.start <= currOffset ) {
                    goldenPath.push( existingChunks.shift() );
                } else {
                    goldenPath.push({ key: {
                                          url: url,
                                          start: currOffset,
                                          end: existingChunks[0] ? existingChunks[0].key.start-1 : end,
                                          toString: chunkToString
                                      }
                                    });
                }
            }
        }

        // filter the blocks in the golden path that
        // have not already been fetched to try to align them to chunk boundaries: multiples of minChunkSize
        array.forEach( goldenPath, function(c) {
                           if( c.value )
                               return;
                           var k = c.key;
                           k.start = Math.floor( k.start / this.minChunkSize ) * this.minChunkSize;
                           if( k.end )
                               k.end = Math.ceil( (k.end+1) / this.minChunkSize ) * this.minChunkSize - 1;
                       }, this );

        // merge and filter request blocks in the golden path
        goldenPath = this._optimizeGoldenPath( goldenPath );

        var needed = array.filter( goldenPath, function(n) { return ! n.value; });

        this._log( 'needed', needed );

        // now fetch all the needed chunks
        // remember that chunk records in the 'needed' array are also
        // present in the 'goldenPath' array, so setting their value
        // will affect both places
        if( needed.length ) {
            var fetchedCount = 0;
            array.forEach( needed, function( c ) {
                this.chunkCache.get( c.key, function( data ) {
                    c.value = data;
                    if( ++fetchedCount == needed.length )
                        callback( goldenPath );
                });
            }, this );
        }
        // or we might already have all the chunks we need
        else {
            callback( goldenPath );
        }
    },

    _optimizeGoldenPath: function( goldenPath ) {
        var goldenPath2 = [ goldenPath[0] ];
        for( var i = 1; i<goldenPath.length; i++ ) {
            var chunk = goldenPath[i];
            var prev = goldenPath[i-1];
            var lastGolden = goldenPath2[ goldenPath2.length-1];

            if( chunk.value ) { // use an existing chunk if it is not rendered superfluous by the previous chunk
                if( chunk.key.end > lastGolden.key.end )
                    goldenPath2.push( chunk );
                // else don't use this chunk
            }
            else {
                // if the last thing on the golden path is also
                // something we need to fetch, merge with it
                if( ! lastGolden.value ) {
                    lastGolden.key.end = chunk.key.end;
                }
                // otherwise, use this fetch
                else {
                    goldenPath2.push( chunk );
                }
            }
        }
        return goldenPath2;
    },

    _fetch: function( request, callback, attempt, truncatedLength ) {

        this._log( 'fetch', request.url, request.start, request.end );
        this._fetchCount++;

        attempt = attempt || 1;
        if( attempt > 3 ) {
            callback(null);
            return;
        }

        var req = new XMLHttpRequest();
        var length;
        var url = request.url;
        if( has("safari") ) {
            url = url + ( url.indexOf('?') > -1 ? '&' : '?' ) + 'safari_cache_bug=' + Date.now();
        }
        req.open('GET', url, true );
        if( req.overrideMimeType )
            req.overrideMimeType('text/plain; charset=x-user-defined');
        if (request.end) {
            req.setRequestHeader('Range', 'bytes=' + request.start + '-' + request.end);
            length = request.end - request.start + 1;
        }
        req.responseType = 'arraybuffer';

        var respond = function( response ) {
            if( response ) {
                if( ! request.start )
                    request.start = 0;
                if( ! request.end )
                    request.end = request.start + response.byteLength;
            }
            callback( response );
        };

        req.onreadystatechange = dojo.hitch( this, function() {
            if (req.readyState == 4) {
                if (req.status == 200 || req.status == 206) {

                    // if this response tells us the file's total size, remember that
                    this.totalSizes[request.url] = (function() {
                        var contentRange = req.getResponseHeader('Content-Range');
                        if( ! contentRange )
                            return undefined;
                        var match = contentRange.match(/\/(\d+)$/);
                        return match ? parseInt(match[1]) : undefined;
                    })();

                    var response = req.response || req.mozResponseArrayBuffer || (function() {
                        try{
                            var r = req.responseText;
                            if (length && length != r.length && (!truncatedLength || r.length != truncatedLength)) {
                                this._fetch( request, callback, attempt + 1, r.length );
                                return;
                            } else {
                                respond( this._stringToBuffer(req.responseText) );
                                return;
                            }
                        } catch (x) {
                            console.error(''+x, x.stack, x);
                            // the response must have successful but
                            // empty, so respond with a zero-length
                            // arraybuffer
                            respond( new ArrayBuffer() );
                            return;
                        }
                    }).call(this);
                    if( response ) {
                        callback( response );
                    }
                } else {
                    return this._fetch( request, callback, attempt + 1);
                }
            }
            return null;
        });
        // if (this.opts.credentials) {
        //     req.withCredentials = true;
        //  }
        req.send('');
    },

    /**
     * @param args.url     {String} url to fetch
     * @param args.start   {Number|undefined} start byte offset
     * @param args.end     {Number|undefined} end byte offset
     * @param args.success {Function} success callback
     * @param args.failure {Function} failure callback
     */
    get: function( args ) {
        this._log( 'get', args.url, args.start, args.end );

        var start = args.start || 0;
        var end = args.end;
        if( start && !end )
            throw "cannot specify a fetch start without a fetch end";

        this._fetchChunks(
            args.url,
            start,
            end,
            dojo.hitch( this,  function( chunks ) {

                 var totalSize = this.totalSizes[ args.url ];

                 this._assembleChunks(
                         start,
                         end,
                         function( resultBuffer ) {
                             if( typeof totalSize == 'number' )
                                 resultBuffer.fileSize = totalSize;
                             try {
                                 args.success.call( this, resultBuffer );
                             } catch( e ) {
                                 console.error(''+e, e.stack, e);
                                 if( args.failure )
                                     args.failure( e );
                             }
                         },
                         args.failure,
                         chunks
                 );
            })
        );
    },

    _assembleChunks: function( start, end, successCallback, failureCallback, chunks ) {
        this._log( 'golden path', chunks);

        var returnBuffer;

        if( ! Uint8Array ) {
            failureCallback( 'Browser does not support typed arrays');
            return;
        }

        // if we just have one chunk, return either it, or a subarray of it.  don't have to do any array copying
        if( chunks.length == 1 && chunks[0].key.start == start && (!end || chunks[0].key.end == end) ) {
            returnBuffer = chunks[0].value;
        } else {

            // calculate the actual range end from the chunks we're
            // using, can't always trust the `end` we're passed,
            // because it might actually be beyond the end of the
            // file.
            var fetchEnd = Math.max.apply(
                Math,
                array.map(
                    chunks,
                    function(c) {
                        return c.key.start + ((c.value||{}).byteLength || 0 ) - 1;
                    })
            );

            // if we have an end, we shouldn't go larger than it, though
            if( end )
                fetchEnd = Math.min( fetchEnd, end );

            var fetchLength = fetchEnd - start + 1;

            // stitch them together into one ArrayBuffer to return
            returnBuffer = new Uint8Array( fetchLength );
            var cursor = 0;
            array.forEach( chunks, function( chunk ) {
                if( !( chunk.value && chunk.value.byteLength ) ) // skip if the chunk has no data
                    return;

                var b = new Uint8Array( chunk.value );
                var bOffset = (start+cursor) - chunk.key.start; if( bOffset < 0 ) this._error('chunking error');
                var length = Math.min( b.byteLength - bOffset, fetchLength - cursor );
                this._log( 'arrayCopy', b, bOffset, returnBuffer, cursor, length );
                arrayCopy( b, bOffset, returnBuffer, cursor, length );
                this._arrayCopyCount++;
                cursor += length;
            },this);
            returnBuffer = returnBuffer.buffer;
        }

        // return the data buffer
        successCallback( returnBuffer );
    },

    _stringToBuffer: function(result) {
        if( ! result || typeof Uint8Array != 'function' )
            return null;

        var ba = new Uint8Array( result.length );
        for ( var i = 0; i < ba.length; i++ ) {
            ba[i] = result.charCodeAt(i);
        }
        return ba.buffer;
    },

    _log: function() {
        //console.log.apply( console, this._logf.apply(this,arguments) );
    },
    _warn: function() {
        console.warn.apply( console, this._logf.apply(this,arguments) );
    },
    _error: function() {
        console.error.apply( console,  this._logf.apply(this,arguments) );
        throw 'file error';
    },
    _logf: function() {
        arguments[0] = this.name+' '+arguments[0];
        if( typeof arguments[0] == 'string' )
            while( arguments[0].length < 15 )
                arguments[0] += ' ';
        return arguments;
    }

});
});
},
'JBrowse/Store/LRUCache':function(){
define("JBrowse/Store/LRUCache", [
           'dojo/_base/declare',
           'dojo/_base/array',
           'JBrowse/Util'
       ],
       function( declare, array, Util ) {

return declare( null,

/**
 * @lends JBrowse.Store.LRUCache
 */
{

    /**
     * An LRU cache.
     *
     * @param args.fill
     * @param args.maxSize
     * @param args.sizeFunction
     * @param args.keyFunction
     * @param args.name
     * @constructs
     */
    constructor: function( args ) {
        this.fill = args.fillCallback;
        this.maxSize = args.maxSize || 1000000;

        this.name = args.name || 'cache';

        this._size = args.sizeFunction || this._size;
        this._keyString = args.keyFunction || this._keyString;

        this.itemCount = 0;
        this.size = 0;

        this._cacheByKey = {};

        // each end of a doubly-linked list, sorted in usage order
        this._cacheOldest = null;
        this._cacheNewest = null;

        // we aggregate cache fill calls that are in progress, indexed
        // by cache key
        this._inProgressFills = {};
    },

    get: function( inKey, callback ) {
        var keyString = this._keyString( inKey );
        var record = this._cacheByKey[ keyString ];

        if( !record ) {
            this._log( 'miss', keyString );

            // call our fill callback if we can
            this._attemptFill( inKey, keyString, callback );
            return;

        } else {
            this._log( 'hit', keyString, record.value );
            this._touch( record );
            window.setTimeout( function() {
                callback( record.value );
            }, 1 );
        }
    },

    query: function( keyRegex ) {
        var results = [];
        var cache = this._cacheByKey;
        for( var k in cache ) {
            if( keyRegex.test( k ) && cache.hasOwnProperty(k) )
                results.push( cache[k] );
        }
        return results;
    },

    touch: function( inKey ) {
        this._touch( this._cacheByKey[ this._keyString( inKey ) ] );
    },
    _touch: function( record ) {
        if( ! record )
            return;

        // already newest, nothing to do
        if( this._cacheNewest === record )
            return;

        // take it out of the linked list
        this._llRemove( record );

        // add it back into the list as newest
        this._llPush( record );
    },

    // take a record out of the LRU linked list
    _llRemove: function( record ) {
        if( record.prev )
            record.prev.next = record.next;
        if( record.next )
            record.next.prev = record.prev;

        if( this._cacheNewest === record )
            this._cacheNewest = record.prev;

        if( this._cacheOldest === record )
            this._cacheOldest = record.next;

        record.prev = null;
        record.next = null;
    },

    _llPush: function( record ) {
        if( this._cacheNewest ) {
            this._cacheNewest.next = record;
            record.prev = this._cacheNewest;
        }
        this._cacheNewest = record;
        if( ! this._cacheOldest )
            this._cacheOldest = record;
    },

    _attemptFill: function( inKey, keyString, callback ) {
        if( this.fill ) {
            var fillRecord = this._inProgressFills[ keyString ] || { callbacks: [], running: false };
            fillRecord.callbacks.push( callback );
            if( ! fillRecord.running ) {
                fillRecord.running = true;
                this.fill( inKey, dojo.hitch( this, function( keyString, inKey, fillRecord, value, error ) {
                    delete this._inProgressFills[ keyString ];
                    fillRecord.running = false;

                    if( value ) {
                        this._log( 'fill', keyString );
                        this.set( inKey, value );
                    }
                    array.forEach( fillRecord.callbacks, function( cb ) {
                                       try {
                                           cb.call( this, value, error );
                                       } catch(x) {
                                           console.error(''+x, x.stack, x);
                                       }
                                   }, this );
                }, keyString, inKey, fillRecord ));
            }
            this._inProgressFills[ keyString ] = fillRecord;
        }
        else {
            try {
                callback( undefined );
            } catch(x) {
                console.error(x);
            }
        }
    },

    set: function( inKey, value ) {
        var keyString = this._keyString( inKey );
        if( this._cacheByKey[keyString] ) {
            return;
        }

        // make a cache record for it
        var record = {
            value: value,
            key: inKey,
            keyString: keyString,
            size: this._size( value )
        };

        if( record.size > this.maxSize ) {
            this._warn( 'cannot fit', keyString, '('+Util.addCommas(record.size) + ' > ' + Util.addCommas(this.maxSize)+')' );
            return;
        }

        this._log( 'set', keyString, record, this.size );

        // evict items if necessary
        this._prune( record.size );

        // put it in the byKey structure
        this._cacheByKey[keyString] = record;

        // put it in the doubly-linked list
        this._llPush( record );

        // update our total size and item count
        this.size += record.size;
        this.itemCount++;

        return;
    },

    _keyString: function( inKey ) {
        var type = typeof inKey;
        if( type == 'object' && typeof inKey.toString == 'function' ) {
            return inKey.toString();
        }
        else {
            return ''+inKey;
        }
    },

    _size: function( value ) {
        var type = typeof value;
        if( type == 'object' ) {
            var sizeType = typeof value.size;
            if( sizeType == 'number' ) {
                return sizeType;
            }
            else if( sizeType == 'function' ) {
                return value.size();
            }
            else if( value.byteLength ) {
                return value.byteLength;
            } else {
                var sum = 0;
                for( var k in value ) {
                    if( value.hasOwnProperty( k ) ) {
                        sum += this._size( value[k] );
                    }
                }
            }
            return sum;
        } else if( type == 'string' ) {
            return value.length;
        } else {
            return 1;
        }
    },

    _prune: function( newItemSize ) {
        while( this.size + (newItemSize||0) > this.maxSize ) {
            var oldest = this._cacheOldest;
            if( oldest ) {
                this._log( 'evict', oldest );

                // // update the oldest and newest pointers
                // if( ! oldest.next ) // if this was also the newest
                //     this._cacheNewest = oldest.prev; // probably undef
                // this._cacheOldest = oldest.next; // maybe undef

                // take it out of the linked list
                this._llRemove( oldest );

                // delete it from the byKey structure
                delete this._cacheByKey[ oldest.keyString ];

                // remove its linked-list links in case that makes it
                // easier for the GC
                delete oldest.next;
                delete oldest.prev;

                // update our size and item counts
                this.itemCount--;
                this.size -= oldest.size;
            } else {
                // should usually not be reached
                this._error( "eviction error", this.size, newItemSize, this );
                return;
            }
        }
    },

    _log: function() {
        //console.log.apply( console, this._logf.apply(this,arguments) );
    },
    _warn: function() {
        console.warn.apply( console, this._logf.apply(this,arguments) );
    },
    _error: function() {
        console.error.apply( console, this._logf.apply(this,arguments) );
    },
    _logf: function() {
        arguments[0] = this.name+' '+arguments[0];
        if( typeof arguments[0] == 'string' )
            while( arguments[0].length < 15 )
                arguments[0] += ' ';
        return arguments;
    }
});
});
},
'JBrowse/Store/SeqFeature/BAM/Util':function(){
define( [ 'jszlib/inflate',
          'jszlib/arrayCopy',
          'JBrowse/Util'
        ],
        function( inflate, arrayCopy, Util ) {

var VirtualOffset = Util.fastDeclare({
    constructor: function(b, o) {
        this.block = b;
        this.offset = o;
    },
    toString: function() {
        return '' + this.block + ':' + this.offset;
    },
    cmp: function(b) {
        var a = this;
        return b.block - a.block || b.offset - a.offset;
    }
});

/**
 * @lends JBrowse.Store.SeqFeature.BAM.Util
 * Package of utility functions used in various places in the BAM code.
 */
var Utils = {

    readInt: function(ba, offset) {
        return (ba[offset + 3] << 24) | (ba[offset + 2] << 16) | (ba[offset + 1] << 8) | (ba[offset]);
    },

    readShort: function(ba, offset) {
        return (ba[offset + 1] << 8) | (ba[offset]);
    },

    readFloat: function(ba, offset) {
        var temp = new Uint8Array( 4 );
        for( var i = 0; i<4; i++ ) {
            temp[i] = ba[offset+i];
        }
        var fa = new Float32Array( temp.buffer );
        return fa[0];
    },

    readVirtualOffset: function(ba, offset) {
        //console.log( 'readVob', offset );
        var block = (ba[offset+6] & 0xff) * 0x100000000
            + (ba[offset+5] & 0xff) * 0x1000000
            + (ba[offset+4] & 0xff) * 0x10000
            + (ba[offset+3] & 0xff) * 0x100
            + (ba[offset+2] & 0xff);
        var bint = (ba[offset+1] << 8) | ba[offset];
        if (block == 0 && bint == 0) {
            return null;  // Should only happen in the linear index?
        } else {
            return new VirtualOffset(block, bint);
        }
    },

    unbgzf: function(data, lim) {
        lim = Math.min( lim || Infinity, data.byteLength - 27);
        var oBlockList = [];
        var totalSize = 0;

        for( var ptr = [0]; ptr[0] < lim; ptr[0] += 8) {

            var ba = new Uint8Array( data, ptr[0], 18 );

            // check the bgzf block magic
            if( !( ba[0] == 31 && ba[1] == 139 ) ) {
                console.error( 'invalid BGZF block header, skipping', ba );
                break;
            }

            var xlen = Utils.readShort( ba, 10 );
            var compressedDataOffset = ptr[0] + 12 + xlen;

            // var inPtr = ptr[0];
            // var bSize = Utils.readShort( ba, 16 );
            // var logLength = Math.min(data.byteLength-ptr[0], 40);
            // console.log( xlen, bSize, bSize - xlen - 19, new Uint8Array( data, ptr[0], logLength ), logLength );

            var unc;
            try {
                unc = inflate(
                    data,
                    compressedDataOffset,
                    data.byteLength - compressedDataOffset,
                    ptr
                );
            } catch( inflateError ) {
                // if we have a buffer error and we have already
                // inflated some data, there is probably just an
                // incomplete BGZF block at the end of the data, so
                // ignore it and stop inflating
                if( /^Z_BUF_ERROR/.test(inflateError.statusString) && oBlockList.length ) {
                    break;
                }
                // otherwise it's some other kind of real error
                else {
                    throw inflateError;
                }
            }
            if( unc.byteLength ) {
                totalSize += unc.byteLength;
                oBlockList.push( unc );
            }
            // else {
            //     console.error( 'BGZF decompression failed for block ', compressedDataOffset, data.byteLength-compressedDataOffset, [inPtr] );
            // }
        }

        if (oBlockList.length == 1) {
            return oBlockList[0];
        } else {
            var out = new Uint8Array(totalSize);
            var cursor = 0;
            for (var i = 0; i < oBlockList.length; ++i) {
                var b = new Uint8Array(oBlockList[i]);
                arrayCopy(b, 0, out, cursor, b.length);
                cursor += b.length;
            }
            return out.buffer;
        }
    }
};

return Utils;

});
},
'JBrowse/Store/SeqFeature/BAM/File':function(){
define( [
            'dojo/_base/declare',
            'dojo/_base/array',
            'JBrowse/Util',
            'JBrowse/Store/LRUCache',
            './Util',
            './Feature'
        ],
        function( declare, array, Util, LRUCache, BAMUtil, BAMFeature ) {

var BAM_MAGIC = 21840194;
var BAI_MAGIC = 21578050;

var dlog = function(){ console.error.apply(console, arguments); };

//
// Binning (transliterated from SAM1.3 spec)
//

/* calculate bin given an alignment covering [beg,end) (zero-based, half-close-half-open) */
function reg2bin(beg, end)
{
    --end;
    if (beg>>14 == end>>14) return ((1<<15)-1)/7 + (beg>>14);
    if (beg>>17 == end>>17) return ((1<<12)-1)/7 + (beg>>17);
    if (beg>>20 == end>>20) return ((1<<9)-1)/7 + (beg>>20);
    if (beg>>23 == end>>23) return ((1<<6)-1)/7 + (beg>>23);
    if (beg>>26 == end>>26) return ((1<<3)-1)/7 + (beg>>26);
    return 0;
}

/* calculate the list of bins that may overlap with region [beg,end) (zero-based) */
var MAX_BIN = (((1<<18)-1)/7);
function reg2bins(beg, end)
{
    var k, list = [];
    --end;
    list.push(0);
    for (k = 1 + (beg>>26); k <= 1 + (end>>26); ++k) list.push(k);
    for (k = 9 + (beg>>23); k <= 9 + (end>>23); ++k) list.push(k);
    for (k = 73 + (beg>>20); k <= 73 + (end>>20); ++k) list.push(k);
    for (k = 585 + (beg>>17); k <= 585 + (end>>17); ++k) list.push(k);
    for (k = 4681 + (beg>>14); k <= 4681 + (end>>14); ++k) list.push(k);
    return list;
}

var Chunk = Util.fastDeclare({
    constructor: function(minv,maxv,bin) {
        this.minv = minv;
        this.maxv = maxv;
        this.bin = bin;
    },
    toString: function() {
        return this.minv+'..'+this.maxv+' (bin '+this.bin+')';
    }
});

var readInt   = BAMUtil.readInt;
var readVirtualOffset = BAMUtil.readVirtualOffset;

var BamFile = declare( null,


/**
 * @lends JBrowse.Store.SeqFeature.BAM.File
 */
{

    /**
     * Low-level BAM file reading code.
     *
     * Adapted by Robert Buels from bam.js in the Dalliance Genome
     * Explorer which is copyright Thomas Down 2006-2010
     * @constructs
     */
    constructor: function( args ) {
        this.store = args.store;
        this.data  = args.data;
        this.bai   = args.bai;
    },

    init: function( args ) {
        var bam = this;
        var successCallback = args.success || function() {};
        var failCallback = args.failure || function() {};

        this._readBAI( dojo.hitch( this, function() {
            this._readBAMheader( function() {
                successCallback();
            }, failCallback );
        }), failCallback );
    },

    _readBAI: function( successCallback, failCallback ) {
        // Do we really need to fetch the whole thing? :-(
        this.bai.fetch( dojo.hitch( this, function(header) {
            if (!header) {
                dlog("No data read from BAM index (BAI) file");
                failCallback("No data read from BAM index (BAI) file");
                return;
            }

            if( ! Uint8Array ) {
                dlog('Browser does not support typed arrays');
                failCallback('Browser does not support typed arrays');
                return;
            }

            var uncba = new Uint8Array(header);
            if( readInt(uncba, 0) != BAI_MAGIC) {
                dlog('Not a BAI file');
                failCallback('Not a BAI file');
                return;
            }

            var nref = readInt(uncba, 4);

            this.indices = [];

            var p = 8;
            for (var ref = 0; ref < nref; ++ref) {
                var blockStart = p;
                var nbin = readInt(uncba, p); p += 4;
                for (var b = 0; b < nbin; ++b) {
                    var bin = readInt(uncba, p);
                    var nchnk = readInt(uncba, p+4);
                    p += 8 + (nchnk * 16);
                }
                var nintv = readInt(uncba, p); p += 4;
                // as we're going through the index, figure out the smallest
                // virtual offset in the indexes, which tells us where
                // the BAM header ends
                var firstVO = nintv ? readVirtualOffset(uncba,p) : null;
                if( firstVO && ( ! this.minAlignmentVO || this.minAlignmentVO.cmp( firstVO ) < 0 ) )
                    this.minAlignmentVO = firstVO;

                //console.log( ref, ''+firstVO );
                p += nintv * 8;
                if( nbin > 0 || nintv > 0 ) {
                    this.indices[ref] = new Uint8Array(header, blockStart, p - blockStart);
                }
            }

            this.empty = ! this.indices.length;

            successCallback( this.indices, this.minAlignmentVO );
        }), failCallback );
    },

    _readBAMheader: function( successCallback, failCallback ) {
        // We have the virtual offset of the first alignment
        // in the file.  Cannot completely determine how
        // much of the first part of the file to fetch to get just
        // up to that, since the file is compressed.  Thus, fetch
        // up to the start of the BGZF block that the first
        // alignment is in, plus 64KB, which should get us that whole
        // BGZF block, assuming BGZF blocks are no bigger than 64KB.
        this.data.read(
            0,
            this.minAlignmentVO ? this.minAlignmentVO.block + 65535 : null,
            dojo.hitch( this, function(r) {
                var unc = BAMUtil.unbgzf(r);
                var uncba = new Uint8Array(unc);

                if( readInt(uncba, 0) != BAM_MAGIC) {
                    dlog('Not a BAM file');
                    failCallback( 'Not a BAM file' );
                    return;
                }

                var headLen = readInt(uncba, 4);
                // var header = '';
                // for (var i = 0; i < headLen; ++i) {
                //     header += String.fromCharCode(uncba[i + 8]);
                // }

                var nRef = readInt(uncba, headLen + 8);
                var p = headLen + 12;

                this.chrToIndex = {};
                this.indexToChr = [];
                for (var i = 0; i < nRef; ++i) {
                    var lName = readInt(uncba, p);
                    var name = '';
                    for (var j = 0; j < lName-1; ++j) {
                        name += String.fromCharCode(uncba[p + 4 + j]);
                    }
                    var lRef = readInt(uncba, p + lName + 4);
                    // dlog(name + ': ' + lRef);
                    this.chrToIndex[name] = i;
                    if (name.indexOf('chr') == 0) {
                        this.chrToIndex[name.substring(3)] = i;
                    } else {
                        this.chrToIndex['chr' + name] = i;
                    }

                    this.indexToChr.push(name);

                    p = p + 8 + lName;
                }

                successCallback();
        }));
    },

    blocksForRange: function(refId, min, max) {
        var index = this.indices[refId];
        if (!index) {
            return [];
        }

        var intBinsL = reg2bins(min, max);
        var intBins = [];
        for (var i = 0; i < intBinsL.length; ++i) {
            intBins[intBinsL[i]] = true;
        }
        var leafChunks = [], otherChunks = [];

        var nbin = readInt(index, 0);
        var p = 4;
        for (var b = 0; b < nbin; ++b) {
            var bin = readInt(index, p);
            var nchnk = readInt(index, p+4);
    //        dlog('bin=' + bin + '; nchnk=' + nchnk);
            p += 8;
            if (intBins[bin]) {
                for (var c = 0; c < nchnk; ++c) {
                    var cs = readVirtualOffset(index, p);
                    var ce = readVirtualOffset(index, p + 8);
                    (bin < 4681 ? otherChunks : leafChunks).push(new Chunk(cs, ce, bin));
                    p += 16;
                }
            } else {
                p +=  (nchnk * 16);
            }
        }
    //    dlog('leafChunks = ' + miniJSONify(leafChunks));
    //    dlog('otherChunks = ' + miniJSONify(otherChunks));

        var nintv = readInt(index, p);
        var lowest = null;
        var minLin = Math.min(min>>14, nintv - 1), maxLin = Math.min(max>>14, nintv - 1);
        for (var i = minLin; i <= maxLin; ++i) {
            var lb =  readVirtualOffset(index, p + 4 + (i * 8));
            if (!lb) {
                continue;
            }
            if (!lowest || lb.block < lowest.block || lb.offset < lowest.offset) {
                lowest = lb;
            }
        }
        // dlog('Lowest LB = ' + lowest);

        var prunedOtherChunks = [];
        if (lowest != null) {
            for (var i = 0; i < otherChunks.length; ++i) {
                var chnk = otherChunks[i];
                if (chnk.maxv.block >= lowest.block && chnk.maxv.offset >= lowest.offset) {
                    prunedOtherChunks.push(chnk);
                }
            }
        }
        // dlog('prunedOtherChunks = ' + miniJSONify(prunedOtherChunks));
        otherChunks = prunedOtherChunks;

        var intChunks = [];
        for (var i = 0; i < otherChunks.length; ++i) {
            intChunks.push(otherChunks[i]);
        }
        for (var i = 0; i < leafChunks.length; ++i) {
            intChunks.push(leafChunks[i]);
        }

        intChunks.sort(function(c0, c1) {
            var dif = c0.minv.block - c1.minv.block;
            if (dif != 0) {
                return dif;
            } else {
                return c0.minv.offset - c1.minv.offset;
            }
        });
        var mergedChunks = [];
        if (intChunks.length > 0) {
            var cur = intChunks[0];
            for (var i = 1; i < intChunks.length; ++i) {
                var nc = intChunks[i];
                if (nc.minv.block == cur.maxv.block /* && nc.minv.offset == cur.maxv.offset */) { // no point splitting mid-block
                    cur = new Chunk(cur.minv, nc.maxv, 'linear');
                } else {
                    mergedChunks.push(cur);
                    cur = nc;
                }
            }
            mergedChunks.push(cur);
        }
    //    dlog('mergedChunks = ' + miniJSONify(mergedChunks));

        return mergedChunks;
    },

    fetch: function(chr, min, max, callback) {

        var chrId = this.chrToIndex[chr];
        var chunks;
        if (chrId === undefined) {
            chunks = [];
        } else {
            chunks = this.blocksForRange(chrId, min, max);
            if (!chunks) {
                callback(null, 'Error in index fetch');
            }
        }

        // toString function is used by the cache for making cache keys
        chunks.toString = function() {
            return this.join(', ');
        };

        this.featureCache = this.featureCache || new LRUCache({
            name: 'bamFeatureCache',
            fillCallback: dojo.hitch(this, '_fetchChunkFeatures' ),
            sizeFunction: function( features ) {
                return features.length;
            },
            maxSize: 100000 // cache up to 100,000 BAM features
        });

        try {
            this.featureCache.get( chunks, function( features, error ) {
                if( error ) {
                    callback( null, error );
                } else {
                    features = array.filter( features, function( feature ) {
                        return ( !( feature.get('end') < min || feature.get('start') > max )
                                 && ( chrId === undefined || feature._refID == chrId ) );
                    });
                    callback( features );
                }
            });
        } catch( e ) {
            callback( null, e );
        }
    },

    _fetchChunkFeatures: function( chunks, callback ) {
        var thisB = this;
        var features = [];
        var chunksProcessed = 0;

        if( ! chunks.length ) {
            callback([]);
            return;
        }

        var error;
        array.forEach( chunks, function( c ) {
                var fetchMin = c.minv.block;
                var fetchMax = c.maxv.block + (1<<16); // *sigh*

                thisB.data.read(fetchMin, fetchMax - fetchMin + 1, function(r) {
                    try {
                        var data = BAMUtil.unbgzf(r, c.maxv.block - c.minv.block + 1);

                        thisB.readBamFeatures( new Uint8Array(data), c.minv.offset, features, function() {
                            if( ++chunksProcessed == chunks.length )
                                callback( features, error );
                        });
                    } catch( e ) {
                        error = e;
                        if( ++chunksProcessed == chunks.length )
                                callback( null, error );
                    }
                });
        });
    },

    readBamFeatures: function(ba, blockStart, sink, callback ) {
        var that = this;
        var featureCount = 0;

        var maxFeaturesWithoutYielding = 300;

        while ( true ) {
            if( blockStart >= ba.length ) {
                // if we're done, call the callback and return
                callback( sink );
                return;
            }
            else if( featureCount <= maxFeaturesWithoutYielding ) {
                // if we've read no more than 200 features this cycle, read another one
                var blockSize = readInt(ba, blockStart);
                var blockEnd = blockStart + 4 + blockSize - 1;

                // only try to read the feature if we have all the bytes for it
                if( blockEnd < ba.length ) {
                    var feature = new BAMFeature({
                        store: this.store,
                        file: this,
                        bytes: { byteArray: ba, start: blockStart, end: blockEnd }
                     });
                    sink.push(feature);
                    featureCount++;
                }

                blockStart = blockEnd+1;
            }
            else {
                // if we're not done but we've read a good chunk of
                // features, put the rest of our work into a timeout to continue
                // later, avoiding blocking any UI stuff that's going on
                window.setTimeout( function() {
                    that.readBamFeatures( ba, blockStart, sink, callback );
                }, 1);
                return;
            }
        }
    }
});

return BamFile;

});

},
'JBrowse/Store/SeqFeature/BAM/Feature':function(){
define( ['dojo/_base/array',
         'JBrowse/Util',
         './Util'
        ],
        function( array, Util, BAMUtil ) {


var SEQRET_DECODER = ['=', 'A', 'C', 'x', 'G', 'x', 'x', 'x', 'T', 'x', 'x', 'x', 'x', 'x', 'x', 'N'];
var CIGAR_DECODER  = ['M', 'I', 'D', 'N', 'S', 'H', 'P', '=', 'X', '?', '?', '?', '?', '?', '?', '?'];

var readInt   = BAMUtil.readInt;
var readShort = BAMUtil.readShort;
var readFloat = BAMUtil.readFloat;

var Feature = Util.fastDeclare(

/**
 * @lends JBrowse.Store.BAM.Feature
 */
{

    /**
     * Feature object used for the JBrowse BAM backend.
     * @param args.store the BAM store this feature comes from
     * @param args.record optional BAM record (a plain object) containing the data for this feature
     * @constructs
     */
    constructor: function( args ) {
        this.file = args.file;
        this.store = args.store;

        var data = args.data
            || args.bytes && this._fromBytes( args.bytes.byteArray, args.bytes.start, args.bytes.end )
            || args.record && dojo.clone(args.record);

        // figure out start and end
        data.start = data.start || data.pos;
        data.end = data.end || ( data.length_on_ref ? data.pos + data.length_on_ref : data.seq ? data.pos + data.seq.length : undefined );

        // trying to determine orientation from 'XS' optional field,
        // or from the seq_reverse_complemented flag
        data.strand = ('XS' in data )                ? ( data.XS == '-' ? -1 : 1 ) :
                       data.seq_reverse_complemented ? -1                          :
                                                       1;

        data.score = data.mapping_quality || data.MQ || data.mq;
        data.type = data.type || 'match';
        data.source = args.store.source;

        if( data.qual && data.qual.join )
            data.qual = data.qual.join(' ');

        if( data.readName ) {
            data.name = data.readName;
            delete data.readName;
        }
        delete data.pos;

        this._refID = data._refID;
        delete data._refID;

        this.data = data;
        this._subCounter = 0;
        this._uniqueID = args.parent ? args.parent._uniqueID + '-' + ++args.parent._subCounter
                                     : this.data.name+' at '+ data.seq_id + ':' + data.start + '..' + data.end;

        // var cigar = data.CIGAR || data.cigar;
        // this.data.subfeatures = [];
        // if( cigar ) {
        //     this.data.Gap = this._cigarToGap( cigar );
        //     this.data.subfeatures.push.apply( this.data.subfeatures, this._cigarToSubfeats( cigar, this ) );
        // }
    },

    _fromBytes: function( byteArray, blockStart, blockEnd ) {
        var record = {};

        var refID = readInt(byteArray, blockStart + 4);
        var pos = readInt(byteArray, blockStart + 8);

        var bmn = readInt(byteArray, blockStart + 12);
        //var bin = (bmn & 0xffff0000) >> 16;
        var mq = (bmn & 0xff00) >> 8;
        var nl = bmn & 0xff;

        var flag_nc = readInt(byteArray, blockStart + 16);
        this._decodeFlags( record, (flag_nc & 0xffff0000) >> 16 );

        record.template_length = readInt(byteArray, blockStart + 32);

        var lseq = readInt(byteArray, blockStart + 20);
        record.seq_length = lseq;

        // If the read is unmapped, no assumptions can be made about RNAME, POS,
        // CIGAR, MAPQ, bits 0x2, 0x10 and 0x100 and the bit 0x20 of the next
        // segment in the template.
        var numCigarOps = flag_nc & 0xffff;

        // if this is a multi-segment read (e.g. mate pairs), parse
        // out the position of the next segment and format it as a
        // locstring
        if( record.multi_segment_template ) {
            var nextRefID = readInt(byteArray, blockStart + 24);
            var nextSegment = this.file.indexToChr[nextRefID];
            if( nextSegment )
                record.next_segment_position = nextSegment+':'+readInt(byteArray, blockStart + 28);
        }

        if( ! record.unmapped ) {
            var readName = '';
            for (var j = 0; j < nl-1; ++j) {
                readName += String.fromCharCode(byteArray[blockStart + 36 + j]);
            }
        }

        var p = blockStart + 36 + nl;
        var cigar = '';
        var lref = 0;
        for (var c = 0; c < numCigarOps; ++c) {
            var cigop = readInt(byteArray, p);
            var lop = cigop >> 4;
            var op = CIGAR_DECODER[cigop & 0xf];
            cigar = cigar + lop + op;
            switch (op) {
            case 'M':
            case 'D':
            case 'N':
            case '=':
            case 'X':
                lref += lop;
                break;
            }
            p += 4;
        }
        if( ! record.unmapped ) {
            record.cigar = cigar;
            record.length_on_ref = lref;
        }

        var seq = '';
        var seqBytes = (lseq + 1) >> 1;
        for (var j = 0; j < seqBytes; ++j) {
            var sb = byteArray[p + j];
            seq += SEQRET_DECODER[(sb & 0xf0) >> 4];
            seq += SEQRET_DECODER[(sb & 0x0f)];
        }
        p += seqBytes;
        record.seq = seq;

        if( ! record.unmapped ) {
            var qseq = [];
            for (var j = 0; j < lseq; ++j) {
                qseq.push( byteArray[p + j] );
            }
        }

        p += lseq;

        if( ! record.unmapped ) {
            record.qual = qseq;

            record.pos = pos;
            if( mq != 255 ) // value of 255 means MQ is not available
                record.mapping_quality = mq;
            record.readName = readName;
            record.seq_id = this.file.indexToChr[refID];
            record._refID = refID;
        }

        while( p < blockEnd ) { // really should be blockEnd - 3, but this works too and is faster
            var tag = String.fromCharCode(byteArray[p]) + String.fromCharCode(byteArray[p + 1]);
            var origType = String.fromCharCode(byteArray[p + 2]);
            var type = origType.toLowerCase();
            p += 3;

            var value;
            if (type == 'a') {
                value = String.fromCharCode( byteArray[p] );
                p += 1;
            } else if (type == 'i' ) {
                value = readInt(byteArray, p );
                p += 4;
            } else if (type == 'c' ) {
                value = byteArray[p];
                p += 1;
            } else if (type == 's' ) {
                value = readShort(byteArray, p);
                p += 2;
            } else if (type == 'f') {
                value = readFloat( byteArray, p );
                p += 4;
            } else if ( type == 'z' || type == 'h' ) {
                value = '';
                while( p <= blockEnd ) {
                    var cc = byteArray[p++];
                    if( cc == 0 ) {
                        break;
                    }
                    else {
                        value += String.fromCharCode(cc);
                    }
                }
            } else {
                console.warn( "Unknown BAM tag type '"+origType
                              +'\', tags for '+(readName||'(unnamed read)')
                              +' may be incomplete'
                            );
                value = undefined;
                p = blockEnd+1; // stop parsing tags
            }
            record[tag] = value;
        }
        return record;
    },

    /**
     * Decode the BAM flags field and set them in the record.
     */
    _decodeFlags: function( record, flags ) {
        // the following explanations are taken verbatim from the SAM/BAM spec

        // 0x1 template having multiple segments in sequencing
        // If 0x1 is unset, no assumptions can be made about 0x2, 0x8, 0x20,
        // 0x40 and 0x80.
        if( flags & 0x1 ) {
            record.multi_segment_template = true;
            // 0x2 each segment properly aligned according to the aligner
            record.multi_segment_all_aligned = !!( flags & 0x2 );
            // 0x8 next segment in the template unmapped
            record.multi_segment_next_segment_unmapped = !!( flags & 0x8 );
            // 0x20 SEQ of the next segment in the template being reversed
            record.multi_segment_next_segment_reversed = !!( flags & 0x20 );

            // 0x40 the first segment in the template
            var first = !!( flags & 0x40 );
            // 0x80 the last segment in the template
            var last =  !!( flags & 0x80 );
            // * If 0x40 and 0x80 are both set, the segment is part of a linear
            // template, but it is neither the first nor the last segment. If both
            // 0x40 and 0x80 are unset, the index of the segment in the template is
            // unknown. This may happen for a non-linear template or the index is
            // lost in data processing.
            if( first && last ) {
                record.multi_segment_inner = true;
            }
            else if( first && !last ) {
                record.multi_segment_first = true;
            }
            else if( !first && last ) {
                record.multi_segment_last = true;
            }
            else {
                record.multi_segment_index_unknown = true;
            }
        }

        // 0x4 segment unmapped
        // * Bit 0x4 is the only reliable place to tell whether the segment is
        // unmapped. If 0x4 is set, no assumptions can be made about RNAME, POS,
        // CIGAR, MAPQ, bits 0x2, 0x10 and 0x100 and the bit 0x20 of the next
        // segment in the template.
        // only set unmapped if true
        if( flags & 0x4 ) {
            record.unmapped = true;
        } else {
            // 0x10 SEQ being reverse complemented
            record.seq_reverse_complemented = !!(flags & 0x10);

            // 0x100 secondary alignment
            // * Bit 0x100 marks the alignment not to be used in certain analyses
            // when the tools in use are aware of this bit.
            if( flags & 0x100 )
                record.secondary_alignment = true;
        }

        // 0x200 not passing quality controls
        // only set qc_failed if it is true
        if( flags & 0x200 )
            record.qc_failed = true;

        // 0x400 PCR or optical duplicate
        // only set duplicate if true
        if ( flags & 0x400 )
            record.duplicate = true;
    },

    _parseCigar: function( cigar ) {
        return array.map( cigar.match(/\d+\D/g), function( op ) {
           return [ op.match(/\D/)[0].toUpperCase(), parseInt( op ) ];
        });
    },

    /**
     *  take a cigar string, and initial position, return an array of subfeatures
     */
    _cigarToSubfeats: function(cigar, parent)    {
        var subfeats = [];
        var min = parent.get('start');
        var max;
        var ops = this._parseCigar( cigar );
        for (var i = 0; i < ops.length; i++)  {
            var lop = ops[i][1];
            var op = ops[i][0];  // operation type
            // converting "=" to "E" to avoid possible problems later with non-alphanumeric type name
            if (op === "=")  { op = "E"; }

            switch (op) {
            case 'M':
            case 'D':
            case 'N':
            case 'E':
            case 'X':
                max = min + lop;
                break;
            case 'I':
                max = min;
                break;
            case 'P':  // not showing padding deletions (possibly change this later -- could treat same as 'I' ?? )
            case 'H':  // not showing hard clipping (since it's unaligned, and offset arg meant to be beginning of aligned part)
            case 'S':  // not showing soft clipping (since it's unaligned, and offset arg meant to be beginning of aligned part)
                break;
                // other possible cases
            }
            var subfeat = new Feature({
                store: this.store,
                file: this.file,
                data: {
                    type: 'match_part',
                    start: min,
                    end: max,
                    strand: parent.get('strand'),
                    cigar_op: lop+op
                },
                parent: this
            });
            if (op !== 'N')  {
                subfeats.push(subfeat);
            }
            min = max;
        }
        return subfeats;
    },

    /**
     * Takes a CIGAR string, translates to a GFF3 Gap attribute
     */
    _cigarToGap: function(cigar)    {
        return array.map( this._parseCigar( cigar ), function( op ) {
            return ( {
                         // map the CIGAR operations to the less-descriptive
                         // GFF3 gap operations
                         M: 'M',
                         I: 'I',
                         D: 'D',
                         N: 'D',
                         S: 'M',
                         H: 'M',
                         P: 'I'
                     }[op[0]] || op[0]
                   )+op[1];
        }).join(' ');
    },

    get: function(name) {
        return this.data[ name ];
    },

    tags: function() {
        var t = [];
        var d = this.data;
        for( var k in d ) {
            if( d.hasOwnProperty( k ) )
                t.push( k );
        }
        return t;
    }

});

return Feature;
});

}}});
define( "JBrowse/Store/SeqFeature/BAM", [
            'dojo/_base/declare',
            'dojo/_base/array',
            'dojo/_base/Deferred',
            'dojo/_base/lang',
            'JBrowse/Util',
            'JBrowse/Store/SeqFeature',
            'JBrowse/Model/XHRBlob',
            './BAM/Util',
            './BAM/File'
        ],
        function( declare, array, Deferred, lang, Util, SeqFeatureStore, XHRBlob, BAMUtil, BAMFile ) {

var BAMStore = declare( SeqFeatureStore,

/**
 * @lends JBrowse.Store.SeqFeature.BAM
 */
{
    /**
     * Data backend for reading feature data directly from a
     * web-accessible BAM file.
     *
     * @constructs
     */
    constructor: function( args ) {
        var bamBlob = args.bam || (function() {
                                       var url = Util.resolveUrl(
                                           args.baseUrl || '/',
                                           Util.fillTemplate( args.urlTemplate || 'data.bam',
                                           {'refseq': (this.refSeq||{}).name }
                                                            )
                                       );
                                       return new XHRBlob( url );
                                   }).call(this);
        var baiBlob = args.bai || (function() {
                                      var url = Util.resolveUrl(
                                          args.baseUrl || '/',
                                          Util.fillTemplate( args.baiUrlTemplate || args.urlTemplate+'.bai' || 'data.bam.bai',
                                                             {'refseq': (this.refSeq||{}).name }
                                                           )
                                      );
                                      return new XHRBlob( url );
                                  }).call(this);

        this.bam = new BAMFile({
                store: this,
                data: bamBlob,
                bai: baiBlob
        });

        this.source = bamBlob.url ? bamBlob.url.match( /\/([^/\#\?]+)($|[\#\?])/ )[1] : undefined;

        this._loading = new Deferred();
        if( args.callback )
            this._loading.then(
                function() { args.callback(bwg); },
                function() { args.callback(null, 'Loading failed!'); }
            );
        this._loading.then( dojo.hitch( this, function() {
                                            this._loading = null;
                                        }));
    },

    load: function() {
        this.bam.init({
            success: dojo.hitch( this, '_estimateGlobalStats',
                                 dojo.hitch( this, function(error) {
                                     if( error )
                                         this.loadFail( error );
                                     else
                                         this.loadSuccess();

                                 })),
            failure: dojo.hitch( this, 'loadFail' )
        });
    },

    /**
     * Fetch a region of the current reference sequence and use it to
     * estimate the feature density in the BAM file.
     * @private
     */
    _estimateGlobalStats: function( finishCallback ) {

        var statsFromInterval = function( refSeq, length, callback ) {
            var sampleCenter = refSeq.start*0.75 + refSeq.end*0.25;
            var start = Math.round( sampleCenter - length/2 );
            var end = Math.round( sampleCenter + length/2 );
            this.bam.fetch( refSeq.name, start, end, dojo.hitch( this, function( features, error) {
                if ( error ) {
                    console.error( error );
                    callback.call( this, length,  null, error );
                }
                else if( features ) {
                    features = array.filter( features, function(f) { return f.get('start') >= start && f.get('end') <= end; } );
                    callback.call( this, length,
                                   {
                                       featureDensity: features.length / length,
                                       _statsSampleFeatures: features.length,
                                       _statsSampleInterval: { ref: refSeq.name, start: start, end: end, length: length }
                                   });
                }
            }));
        };

        var maybeRecordStats = function( interval, stats, error ) {
            if( error ) {
                finishCallback( error );
            } else {
                var refLen = this.refSeq.end - this.refSeq.start;
                 if( stats._statsSampleFeatures >= 300 || interval * 2 > refLen || error ) {
                     this.globalStats = stats;
                     console.log( 'BAM statistics: '+this.source, stats );
                     finishCallback();
                 } else {
                     statsFromInterval.call( this, this.refSeq, interval * 2, maybeRecordStats );
                 }
            }
        };

        statsFromInterval.call( this, this.refSeq, 100, maybeRecordStats );
    },

    loadSuccess: function() {
        this.inherited(arguments);
        this._loading.resolve({success: true });
    },

    loadFail: function() {
        this.inherited(arguments);
        this._loading.resolve({success: false });
    },

    whenReady: function() {
        var f = lang.hitch.apply(lang, arguments);
        if( this._loading ) {
            this._loading.then( f );
        } else {
            f();
        }
    },

    iterate: function( start, end, featCallback, endCallback, errorCallback ) {
        if( this._loading ) {
            this._loading.then( lang.hitch( this, 'iterate', start, end, featCallback, endCallback ) );
            return;
        }

        var maxFeaturesWithoutYielding = 300;
        this.bam.fetch( this.refSeq.name, start, end, function( features, error) {
                if ( error ) {
                    console.error( 'error fetching BAM data: ' + error );
                    if( errorCallback ) errorCallback( error );
                    return;
                }
                if( features ) {
                    var i = 0;
                    var readFeatures = function() {
                        for( ; i < features.length; i++ ) {
                            var feature = features[i];
                            // skip if this alignment is unmapped, or if it does not actually overlap this range
                            if (! (feature.get('unmapped') || feature.get('end') <= start || feature.get('start') >= end) )
                                featCallback( feature );

                            if( i && !( i % maxFeaturesWithoutYielding ) ) {
                                window.setTimeout( readFeatures, 1 );
                                i++;
                                break;
                            }
                        }
                        if( i >= features.length )
                            endCallback();
                    };

                    readFeatures();

                }
            });
    }

});

return BAMStore;
});