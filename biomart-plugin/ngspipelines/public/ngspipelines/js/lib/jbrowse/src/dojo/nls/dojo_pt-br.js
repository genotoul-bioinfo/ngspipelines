define('dojo/nls/dojo_pt-br',{
'dijit/nls/loading':{"loadingState":"Carregando...","errorState":"Desculpe, ocorreu um erro"}
,
'dijit/nls/common':{"buttonOk":"OK","buttonCancel":"Cancelar","buttonSave":"Salvar","itemClose":"Fechar"}
,
'dijit/form/nls/validate':{"invalidMessage":"O valor inserido não é válido.","missingMessage":"Este valor é necessário.","rangeMessage":"Este valor está fora do intervalo. "}
,
'dijit/form/nls/ComboBox':{"previousMessage":"Opções anteriores","nextMessage":"Mais opções"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"¤#,##0.00;(¤#,##0.00)","perMille":"‰","group":".","percentFormat":"#,##0%","decimalFormat":"#,##0.###","decimal":",","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});