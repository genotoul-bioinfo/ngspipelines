define('dojo/nls/dojo_pt-pt',{
'dijit/nls/loading':{"loadingState":"A carregar...","errorState":"Lamentamos, mas ocorreu um erro"}
,
'dijit/nls/common':{"buttonOk":"OK","buttonCancel":"Cancelar","buttonSave":"Guardar","itemClose":"Fechar"}
,
'dijit/form/nls/validate':{"invalidMessage":"O valor introduzido não é válido.","missingMessage":"Este valor é requerido.","rangeMessage":"Este valor encontra-se fora do intervalo."}
,
'dijit/form/nls/ComboBox':{"previousMessage":"Opções anteriores","nextMessage":"Mais opções"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"#,##0.00 ¤","perMille":"‰","group":" ","percentFormat":"#,##0%","decimalFormat":"#,##0.###","decimal":",","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});