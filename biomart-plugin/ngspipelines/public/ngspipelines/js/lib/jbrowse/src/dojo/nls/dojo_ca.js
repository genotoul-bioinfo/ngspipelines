define('dojo/nls/dojo_ca',{
'dijit/nls/loading':{"loadingState":"S'està carregant...","errorState":"Ens sap greu. S'ha produït un error."}
,
'dijit/nls/common':{"buttonOk":"D'acord","buttonCancel":"Cancel·la","buttonSave":"Desa","itemClose":"Tanca"}
,
'dijit/form/nls/validate':{"invalidMessage":"El valor introduït no és vàlid","missingMessage":"Aquest valor és necessari","rangeMessage":"Aquest valor és fora de l'interval"}
,
'dijit/form/nls/ComboBox':{"previousMessage":"Opcions anteriors","nextMessage":"Més opcions"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"#,##0.00 ¤","perMille":"‰","group":".","percentFormat":"#,##0%","decimalFormat":"#,##0.###","decimal":",","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});