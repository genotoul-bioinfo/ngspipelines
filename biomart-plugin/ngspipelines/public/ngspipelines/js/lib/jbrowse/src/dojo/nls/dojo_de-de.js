define('dojo/nls/dojo_de-de',{
'dijit/nls/loading':{"loadingState":"Wird geladen...","errorState":"Es ist ein Fehler aufgetreten."}
,
'dijit/nls/common':{"buttonOk":"OK","buttonCancel":"Abbrechen","buttonSave":"Speichern","itemClose":"Schließen"}
,
'dijit/form/nls/validate':{"invalidMessage":"Der eingegebene Wert ist ungültig. ","missingMessage":"Dieser Wert ist erforderlich.","rangeMessage":"Dieser Wert liegt außerhalb des gültigen Bereichs. "}
,
'dijit/form/nls/ComboBox':{"previousMessage":"Vorherige Auswahl","nextMessage":"Weitere Auswahlmöglichkeiten"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"#,##0.00 ¤","perMille":"‰","group":".","percentFormat":"#,##0 %","decimalFormat":"#,##0.###","decimal":",","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});