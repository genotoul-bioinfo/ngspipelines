define('dojo/nls/dojo_sv',{
'dijit/nls/loading':{"loadingState":"Läser in...","errorState":"Det uppstod ett fel."}
,
'dijit/nls/common':{"buttonOk":"OK","buttonCancel":"Avbryt","buttonSave":"Spara","itemClose":"Stäng"}
,
'dijit/form/nls/validate':{"invalidMessage":"Det angivna värdet är ogiltigt.","missingMessage":"Värdet är obligatoriskt.","rangeMessage":"Värdet är utanför intervallet."}
,
'dijit/form/nls/ComboBox':{"previousMessage":"Föregående alternativ","nextMessage":"Fler alternativ"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"−","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"¤¤¤","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"#,##0.00 ¤","perMille":"‰","group":" ","percentFormat":"#,##0 %","decimalFormat":"#,##0.###","decimal":",","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"×10^"}
});