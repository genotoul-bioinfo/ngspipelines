define('dojo/nls/dojo_he-il',{
'dijit/nls/loading':{"loadingState":"טעינה...‏","errorState":"אירעה שגיאה"}
,
'dijit/nls/common':{"buttonOk":"אישור","buttonCancel":"ביטול","buttonSave":"שמירה","itemClose":"סגירה"}
,
'dijit/form/nls/validate':{"invalidMessage":"הערך שצוין אינו חוקי.","missingMessage":"זהו ערך דרוש.","rangeMessage":"הערך מחוץ לטווח."}
,
'dijit/form/nls/ComboBox':{"previousMessage":"האפשרויות הקודמות","nextMessage":"אפשרויות נוספות"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"#,##0.00 ¤","perMille":"‰","group":",","percentFormat":"#,##0%","decimalFormat":"#,##0.###","decimal":".","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});