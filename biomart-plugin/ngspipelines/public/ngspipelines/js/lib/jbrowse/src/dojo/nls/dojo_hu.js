define('dojo/nls/dojo_hu',{
'dijit/nls/loading':{"loadingState":"Betöltés...","errorState":"Sajnálom, hiba történt"}
,
'dijit/nls/common':{"buttonOk":"OK","buttonCancel":"Mégse","buttonSave":"Mentés","itemClose":"Bezárás"}
,
'dijit/form/nls/validate':{"invalidMessage":"A megadott érték érvénytelen.","missingMessage":"Meg kell adni egy értéket.","rangeMessage":"Az érték kívül van a megengedett tartományon."}
,
'dijit/form/nls/ComboBox':{"previousMessage":"Előző menüpontok","nextMessage":"További menüpontok"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"#,##0.00 ¤","perMille":"‰","group":" ","percentFormat":"#,##0%","decimalFormat":"#,##0.###","decimal":",","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});