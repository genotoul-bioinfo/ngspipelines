define('dojo/nls/dojo_tr',{
'dijit/nls/loading':{"loadingState":"Yükleniyor...","errorState":"Üzgünüz, bir hata oluştu"}
,
'dijit/nls/common':{"buttonOk":"Tamam","buttonCancel":"İptal","buttonSave":"Kaydet","itemClose":"Kapat"}
,
'dijit/form/nls/validate':{"invalidMessage":"Girilen değer geçersiz.","missingMessage":"Bu değer gerekli.","rangeMessage":"Bu değer aralık dışında."}
,
'dijit/form/nls/ComboBox':{"previousMessage":"Önceki seçenekler","nextMessage":"Diğer seçenekler"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"#,##0.00 ¤","perMille":"‰","group":".","percentFormat":"% #,##0","decimalFormat":"#,##0.###","decimal":",","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});