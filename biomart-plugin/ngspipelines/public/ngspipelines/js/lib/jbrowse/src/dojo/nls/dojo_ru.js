define('dojo/nls/dojo_ru',{
'dijit/nls/loading':{"loadingState":"Загрузка...","errorState":"Извините, возникла ошибка"}
,
'dijit/nls/common':{"buttonOk":"ОК","buttonCancel":"Отмена","buttonSave":"Сохранить","itemClose":"Закрыть"}
,
'dijit/form/nls/validate':{"invalidMessage":"Указано недопустимое значение.","missingMessage":"Это обязательное значение.","rangeMessage":"Это значение вне диапазона."}
,
'dijit/form/nls/ComboBox':{"previousMessage":"Предыдущие варианты","nextMessage":"Следующие варианты"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"#,##0.00 ¤","perMille":"‰","group":" ","percentFormat":"#,##0 %","decimalFormat":"#,##0.###","decimal":",","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});