define('dojo/nls/dojo_ar',{
'dijit/nls/loading':{"loadingState":"جاري التحميل...","errorState":"عفوا، حدث خطأ"}
,
'dijit/nls/common':{"buttonOk":"حسنا","buttonCancel":"الغاء","buttonSave":"حفظ","itemClose":"اغلاق"}
,
'dijit/form/nls/validate':{"invalidMessage":"القيمة التي تم ادخالها غير صحيحة.","missingMessage":"يجب ادخال هذه القيمة.","rangeMessage":"هذه القيمة ليس بالمدى الصحيح."}
,
'dijit/form/nls/ComboBox':{"previousMessage":"الاختيارات السابقة","nextMessage":"مزيد من الاختيارات"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":"؛","percentSign":"٪","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"ليس رقم","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"¤ #,##0.00;¤ #,##0.00-","perMille":"؉","group":"٬","percentFormat":"#,##0%","decimalFormat":"#,##0.###;#,##0.###-","decimal":"٫","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"اس"}
});