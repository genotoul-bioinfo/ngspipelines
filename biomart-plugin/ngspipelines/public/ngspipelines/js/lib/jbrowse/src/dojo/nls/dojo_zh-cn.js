define('dojo/nls/dojo_zh-cn',{
'dijit/nls/loading':{"loadingState":"正在加载...","errorState":"对不起，发生了错误"}
,
'dijit/nls/common':{"buttonOk":"确定","buttonCancel":"取消","buttonSave":"保存","itemClose":"关闭"}
,
'dijit/form/nls/validate':{"invalidMessage":"输入的值无效。","missingMessage":"此值是必需值。","rangeMessage":"此值超出范围。"}
,
'dijit/form/nls/ComboBox':{"previousMessage":"先前选项","nextMessage":"更多选项"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"¤#,##0.00","perMille":"‰","group":",","percentFormat":"#,##0%","decimalFormat":"#,##0.###","decimal":".","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});