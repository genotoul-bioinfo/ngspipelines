define('dojo/nls/dojo_nb',{
'dijit/nls/loading':{"loadingState":"Laster inn...","errorState":"Det oppsto en feil"}
,
'dijit/nls/common':{"buttonOk":"OK","buttonCancel":"Avbryt","buttonSave":"Lagre","itemClose":"Lukk"}
,
'dijit/form/nls/validate':{"invalidMessage":"Den angitte verdien er ikke gyldig.","missingMessage":"Denne verdien er obligatorisk.","rangeMessage":"Denne verdien er utenfor gyldig område."}
,
'dijit/form/nls/ComboBox':{"previousMessage":"Tidligere valg","nextMessage":"Flere valg"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"¤ #,##0.00","perMille":"‰","group":" ","percentFormat":"#,##0 %","decimalFormat":"#,##0.###","decimal":",","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});