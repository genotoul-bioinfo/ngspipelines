define('dojo/nls/dojo_zh-tw',{
'dijit/nls/loading':{"loadingState":"載入中...","errorState":"抱歉，發生錯誤"}
,
'dijit/nls/common':{"buttonOk":"確定","buttonCancel":"取消","buttonSave":"儲存","itemClose":"關閉"}
,
'dijit/form/nls/validate':{"invalidMessage":"輸入的值無效。","missingMessage":"必須提供此值。","rangeMessage":"此值超出範圍。"}
,
'dijit/form/nls/ComboBox':{"previousMessage":"前一個選擇項","nextMessage":"其他選擇項"}
,
'dojo/cldr/nls/number':{"scientificFormat":"#E0","currencySpacing-afterCurrency-currencyMatch":"[:letter:]","infinity":"∞","list":";","percentSign":"%","minusSign":"-","currencySpacing-beforeCurrency-surroundingMatch":"[:digit:]","decimalFormat-short":"000T","currencySpacing-afterCurrency-insertBetween":" ","nan":"NaN","nativeZeroDigit":"0","plusSign":"+","currencySpacing-afterCurrency-surroundingMatch":"[:digit:]","currencySpacing-beforeCurrency-currencyMatch":"[:letter:]","currencyFormat":"¤#,##0.00","perMille":"‰","group":",","percentFormat":"#,##0%","decimalFormat":"#,##0.###","decimal":".","patternDigit":"#","currencySpacing-beforeCurrency-insertBetween":" ","exponential":"E"}
});