(function($) {
$.namespace('jquery.bio', function(self) {

	self.revCompSeq = function (sequence) {
		var res = "";
		// Go in reverse
		for (k=sequence.length-1; k>=0; k--) {
			var x = sequence.substr(k,1);
			if		 (x=="a") n="t"; else if (x=="A") n="T";
			else if (x=="g") n="c"; else if (x=="G") n="C";
			else if (x=="c") n="g"; else if (x=="C") n="G";
			else if (x=="t") n="a"; else if (x=="T") n="A";
			else if (x=="u") n="a"; else if (x=="U") n="A"; // RNA 
			else if (x=="r") n="y"; else if (x=="R") n="Y"; // IUPAC
			else if (x=="y") n="r"; else if (x=="Y") n="R";
			else if (x=="k") n="m"; else if (x=="K") n="M";
			else if (x=="m") n="k"; else if (x=="M") n="K";
			else if (x=="b") n="v"; else if (x=="B") n="V";
			else if (x=="d") n="h"; else if (x=="D") n="H";
			else if (x=="h") n="d"; else if (x=="H") n="D";
			else if (x=="v") n="b"; else if (x=="V") n="B";
			else n = x;                                      // S, W and unknown unchanged
			
			res = res + n;
		}
		return res;
	}

	self.compSeq = function (sequence) {
		//         A  T  C  G  Other
		var res = [0, 0, 0, 0, 0];
		for (k=0; k<sequence.length; k++) {
			var x = sequence.substr(k,1);
			if		(x=="a" || x=="A")	{ res[0]++; }
			else if(x=="t" || x=="T")	{ res[1]++; }
			else if(x=="c" || x=="C")	{ res[2]++; }
			else if(x=="g" || x=="G")	{ res[3]++; }
			else { res[4]++; }
		}
		return res;
	}

	self.translate = function (sequence, frame) {
		var	res = "",
			cmp = -1;
		if(frame<0) {
			sequence = self.revCompSeq(sequence);
			frame    = Math.abs(frame);
		}
		for (k=frame-1; k<sequence.length; k=k+3) {
			var	sstr3 = sequence.substr(k,3).toUpperCase(),
				sstr2 = sequence.substr(k,2).toUpperCase();
			cmp++;
			if(cmp % 90 == 0) { res += " "; }
			switch(sstr2) {
			case "GC":
				res += "A";
				break;
			case "CG":
				res += "R";
				break;
			case "GG":
				res += "G";
				break;
			case "CT":
				res += "L";
				break;
			case "CC":
				res += "P";
				break;
			case "TC":
				res += "S";
				break;
			case "AC":
				res += "T";
				break;
			case "GT":
				res += "V";
				break;
			default:
				switch(sstr3) {
				case "AGA":
					res += "R";
					break;
				case "AGG":
					res += "R";
					break;
				case "AAT":
					res += "N";
					break;
				case "AAC":
					res += "N";
					break;
				case "GAT":
					res += "D";
					break;
				case "GAC":
					res += "D";
					break;
				case "TGT":
					res += "C";
					break;
				case "TGC":
					res += "C";
					break;
				case "CAA":
					res += "Q";
					break;
				case "CAG":
					res += "Q";
					break;
				case "GAA":
					res += "E";
					break;
				case "GAG":
					res += "E";
					break;
				case "CAT":
					res += "H";
					break;
				case "CAC":
					res += "H";
					break;
				case "ATT":
					res += "I";
					break;
				case "ATC":
					res += "I";
					break;
				case "ATA":
					res += "I";
					break;
				case "ATG":
					res += "M";
					break;
				case "TTA":
					res += "L";
					break;
				case "TTG":
					res += "L";
					break;
				case "AAA":
					res += "K";
					break;
				case "AAG":
					res += "K";
					break;
				case "TTT":
					res += "F";
					break;
				case "TTC":
					res += "F";
					break;
				case "AGT":
					res += "S";
					break;
				case "AGC":
					res += "S";
					break;
				case "TGG":
					res += "W";
					break;
				case "TAT":
					res += "Y";
					break;
				case "TAC":
					res += "Y";
					break;
				case "TAA":
					res += "*";
					break;
				case "TGA":
					res += "*";
					break;
				case "TAG":
					res += "*";
					break;
				case "MGR":
					res += "R";
					break;
				case "AAY":
					res += "N";
					break;
				case "GAY":
					res += "D";
					break;
				case "TGY":
					res += "C";
					break;
				case "CAR":
					res += "Q";
					break;
				case "GAR":
					res += "E";
					break;
				case "CAY":
					res += "H";
					break;
				case "ATH":
					res += "I";
					break;
				case "YTR":
					res += "L";
					break;
				case "AAR":
					res += "K";
					break;
				case "TTY":
					res += "F";
					break;
				case "AGY":
					res += "S";
					break;
				case "TAY":
					res += "Y";
					break;
				case "TAR":
					res += "*";
					break;
				case "TRA":
					res += "*";
					break;
				default:
					res += "X";
				}
			}
		}
		return res;
	}
	
});

})(jQuery);
