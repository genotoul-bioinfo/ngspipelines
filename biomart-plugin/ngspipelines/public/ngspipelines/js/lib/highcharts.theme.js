/**
 * NGSPipelines theme for Highcharts JS
 */


/**
 * A small plugin for getting the CSV of a categorized chart
 */
(function (Highcharts) {
    
    // Options
    var itemDelimiter = '&#09;',  // use ';' for direct import to Excel
        lineDelimiter = '\n';
        
    var each = Highcharts.each;
    Highcharts.Chart.prototype.getCSV = function () {
        var columns = [],
        line,
        csv = '',
        row,
        col,
        maxRows,
        options = (this.options.exporting || {}).csv || {},

        // Options
        dateFormat = options.dateFormat || '%Y-%m-%d %H:%M:%S',
        itemDelimiter = options.itemDelimiter || ';', // use ';' for direct import to Excel
        lineDelimiter = options.lineDelimiter || '\n'; // '\n' isn't working with the js csv data extraction

	    var is_first_series = true ;
	    var is_multi_series = this.series.length > 1 ;
	    var nb_series = this.series.length ;
	    var series_idx = 0 ;
	    each(this.series, function (series) {
	        if (series.options.includeInCSVExport !== false) {
	        	var series_name = series.name ;
	            if (series.xAxis) { // bar, column, line
	                var xData = series.xData.slice(),
	                    xTitle = 'X values';
	                if (series.xAxis.isDatetimeAxis) {
	                    xData = Highcharts.map(xData, function (x) {
	                        return Highcharts.dateFormat(dateFormat, x);
	                    });
	                } else if (series.xAxis.categories) {
	                    xData = Highcharts.map(xData, function (x) {
	                        return Highcharts.pick(series.xAxis.categories[x], x);
	                    });
	                    xTitle = 'Category' ;
	                }
	                if( series.xAxis.axisTitle !== undefined ) {
	                	xTitle = series.xAxis.axisTitle.textStr ;
	                }
	                if( series.yAxis.axisTitle !== undefined && !is_multi_series ) {
	                	series_name = series.yAxis.axisTitle.textStr ;
	                }
	                
	                // Add title column
	                if( is_first_series ) {
	                	columns.push(xData);
	                	columns[columns.length - 1].unshift(xTitle);
	                	is_first_series = false ;
	                }
	                
	                // Add y data
	                columns.push(series.yData.slice());
	                columns[columns.length - 1].unshift(series_name);
	            } else if( series.options.type == "pie" && !is_multi_series ) { // pie
	            	// Categories
	                xData = series.xData.slice();
	                xData = Highcharts.map(xData, function (x) {
	                    return Highcharts.pick(series.data[x].name, x);
	                });
	                xData.unshift( 'Category' );
	                columns.push(xData);
	                
	                // Values
	                columns.push(series.yData.slice());
	                columns[columns.length - 1].unshift( "Percentage" );
	            } else if( series.chart.options.chart.type == "pie" && nb_series == 2 ) { // donut with depth == 2
	                if( series_idx + 1 == nb_series ) {
	                	var series_category = new Array() ;
	                    var series_titles = new Array() ;
	                    var series_values = new Array() ;
	                    for( var idx in series.data ) {
	                    	if( series.data[idx].percentage !== undefined ) {
	                    		series_category.push( series.data[idx].categorie );
	                    		series_titles.push( series.data[idx].name );
	                    		series_values.push( series.data[idx].percentage );
	                    	}
	                    }
	                    series_category.unshift( "Category" );
	                    series_titles.unshift( "Sub-Category" );
	                    series_values.unshift( "Percentage" );
	                    columns.push( series_category );
	                	columns.push( series_titles );
	                	columns.push( series_values );
	                }
	            }
	        }
	        series_idx++ ;
	    });
	
	    // Transform the columns to CSV
	    maxRows = Math.max.apply(this, Highcharts.map(columns, function (col) { return col.length; }));
	    for (row = 0; row < maxRows; row = row + 1) {
	        line = [];
	        for (col = 0; col < columns.length; col = col + 1) {
	            line.push(columns[col][row]);
	        }
	        csv += line.join(itemDelimiter) + lineDelimiter;
	    }
	
	    return csv;
    };    
}(Highcharts));

Highcharts.getOptions().exporting.buttons.contextButton.menuItems.push({
	separator: true
});
Highcharts.getOptions().exporting.buttons.contextButton.menuItems.push({
    text: 'Export data to CSV',
    onclick: function () {
    	$dialog = $('#ngspipelines-dialog').dialog({
    		draggable: false,
    		resizable: false,
    		modal: true,
    		autoOpen: false,
    		width: 600,
	        title: "Export chart data to CSV",
	        buttons: {
	          	"close": function() {
	          		$(this).dialog('close');
	            }
	        }
	    });
    	$('#ngspipelines-dialog-body').html(
    		"<br/>Please click on the textarea below and use <b>CTRL-A CTRL-C</b> to copy to clipboard.<br/><br/>" +
    		"<textarea style='resize: none; height: 326px; width: 570px;'>" + this.getCSV() + "</textarea><br/><br/>"
    	);
    	$dialog.dialog('open');	
    }
});

//Colors added
colors = [ "#2f7ed8", "#c42525", "#0d233a", "#1aadce", "#a6c96a", "#492970", "#f28f43", "#77a1e5",
	       "#3a87ad", "#7798BF", "#b94a48", "#f89406", "#333333", "#0d456f", "#DDDF0D", "#55BF3B",
	       "#DF5353", "#aaeeee", "#ff0066", "#55BF3B", "#DF5353", "#468847", "#8bbc21", "#910000" ];
new_colors = [ "#0099FF", "#CC0066", "#66FF33", "#CC9900", "#cebbb4", "#00CCFF", "#CC6699", "#99FF66",
               "#CCCC00", "#e8d9d7", "#0066FF", "#CC0000", "#33CC33", "#FF9900", "#b69f93" ];
colors.push.apply(colors, new_colors);
Highcharts.getOptions().colors = colors;

// Radialize the colors
Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function(color) {
    return {
        radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
        stops: [
            [0, color],
            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
        ]
    };
});		

Highcharts.theme = {
	colors: colors,	
	chart: {
		backgroundColor: {
			linearGradient: [0, 0, 0, 200],
			stops: [
				[0, 'rgb(55, 64, 71)'],
				[1, 'rgb(42, 45, 48)']
			]
		},
		borderWidth: 1,
		borderColor: 'silver',
		borderRadius: 15,
		plotBackgroundColor: null,
		plotShadow: false,
		plotBorderWidth: 0
	},
	title: {
		style: { 
			color: '#FFF',
			fontSize: '14px'
		}
	},
	credits: {
		enabled: false
	},
	subtitle: {
		style: { 
			color: '#DDD',
			font: '12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
		}
	},
	xAxis: {
		gridLineWidth: 0,
		lineColor: '#999',
		tickColor: '#999',
		labels: {
			style: {
				color: '#999',
				fontWeight: 'bold'
			}
		},
		title: {
			style: {
				color: '#AAA',
				font: 'bold 12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
			}				
		}
	},
	yAxis: {
		alternateGridColor: null,
		minorTickInterval: null,
		gridLineColor: 'rgba(255, 255, 255, .1)',
		lineWidth: 0,
		tickWidth: 0,
		labels: {
			style: {
				color: '#999',
				fontWeight: 'bold'
			}
		},
		title: {
			style: {
				color: '#AAA',
				font: 'bold 12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
			}				
		}
	},
	legend: {
		itemStyle: {
			color: '#CCC'
		},
		itemHoverStyle: {
			color: '#FFF'
		},
		itemHiddenStyle: {
			color: '#333'
		}
	},
	labels: {
		style: {
			color: '#CCC'
		}
	},
	tooltip: {
		backgroundColor: {
			linearGradient: [0, 0, 0, 50],
			stops: [
				[0, 'rgba(96, 96, 96, .8)'],
				[1, 'rgba(16, 16, 16, .8)']
			]
		},
		borderWidth: 0,
		style: {
			color: '#FFF'
		}
	},
	
	
	plotOptions: {
		series: {
			dataLabels: {
				color: '#CCC',
				connectorColor: '#CCC'
			}
		},
		line: {
			dataLabels: {
				color: '#CCC'
			},
			marker: {
				lineColor: '#333'
			}
		},
		spline: {
			marker: {
				lineColor: '#333'
			}
		},
		scatter: {
			marker: {
				lineColor: '#333'
			}
		},
		candlestick: {
			lineColor: 'white'
		}
	},
	
	toolbar: {
		itemStyle: {
			color: '#CCC'
		}
	},
	
	navigation: {
		buttonOptions: {
			backgroundColor: {
				linearGradient: [0, 0, 0, 20],
				stops: [
					[0.4, '#606060'],
					[0.6, '#333333']
				]
			},
			borderColor: '#000000',
			symbolStroke: '#C0C0C0',
			hoverSymbolStroke: '#FFFFFF'
		}
	},
	
	exporting: {
		buttons: {
			exportButton: {
				symbolFill: '#55BE3B'
			},
			printButton: {
				symbolFill: '#7797BE'
			}
		}
	},
	
	// scroll charts
	rangeSelector: {
		buttonTheme: {
			fill: {
				linearGradient: [0, 0, 0, 20],
				stops: [
					[0.4, '#888'],
					[0.6, '#555']
				]
			},
			stroke: '#000000',
			style: {
				color: '#CCC',
				fontWeight: 'bold'
			},
			states: {
				hover: {
					fill: {
						linearGradient: [0, 0, 0, 20],
						stops: [
							[0.4, '#BBB'],
							[0.6, '#888']
						]
					},
					stroke: '#000000',
					style: {
						color: 'white'
					}
				},
				select: {
					fill: {
						linearGradient: [0, 0, 0, 20],
						stops: [
							[0.1, '#000'],
							[0.3, '#333']
						]
					},
					stroke: '#000000',
					style: {
						color: 'yellow'
					}
				}
			}					
		},
		inputStyle: {
			backgroundColor: '#333',
			color: 'silver'
		},
		labelStyle: {
			color: 'silver'
		}
	},
	
	navigator: {
		handles: {
			backgroundColor: '#666',
			borderColor: '#AAA'
		},
		outlineColor: '#CCC',
		maskFill: 'rgba(16, 16, 16, 0.5)',
		series: {
			color: '#7798BF',
			lineColor: '#A6C7ED'
		}
	},
	
	scrollbar: {
		barBackgroundColor: {
				linearGradient: [0, 0, 0, 20],
				stops: [
					[0.4, '#888'],
					[0.6, '#555']
				]
			},
		barBorderColor: '#333',
		buttonArrowColor: '#CCC',
		buttonBackgroundColor: {
				linearGradient: [0, 0, 0, 20],
				stops: [
					[0.4, '#888'],
					[0.6, '#555']
				]
			},
		buttonBorderColor: '#333',
		rifleColor: '#FFF',
		trackBackgroundColor: {
			linearGradient: [0, 0, 0, 10],
			stops: [
				[0, '#000'],
				[1, '#333']
			]
		},
		trackBorderColor: '#333'
	},
	
	// special colors for some of the demo examples
	legendBackgroundColor: 'rgba(48, 48, 48, 0.8)',
	legendBackgroundColorSolid: 'rgb(70, 70, 70)',
	dataLabelsColor: '#444',
	textColor: '#E0E0E0',
	maskColor: 'rgba(255,255,255,0.3)'
};

// Apply the theme
//var highchartsOptions = Highcharts.setOptions(Highcharts.theme);