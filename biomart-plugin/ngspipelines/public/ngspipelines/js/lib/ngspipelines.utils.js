(function($) {
$.namespace('ngspipelines.utils', function(self) {
	
	/* 
	 * Load datasets and marts linked to the provided root and mart. 
	 * Also initialized required biomart global variables.
	 * @param rootName the mart group name
	 * @param martName the mart config name
	 * @param callBackFunction the function to call when loading is over
	 */
    self.loadMarts = function (rootName, martName, callBackFunction) {
    	
    	// Init all required variables
        biomart._state.root = rootName;
        biomart._state.martName = martName;
        biomart._state.martParam = '';
        biomart._state.mart = new Array();
        biomart._state.datasets = new Array();
    	
    	// First load all marts from the specified gui
        biomart.resource.load('gui', function(json) {
        	biomart._state.martGroupMap = {}; // for grouping
            for (var i=0, mart; mart=json.marts[i]; i++) {
                if (!mart.group) {
                	if (mart.name==biomart._state.martName) {
                		biomart._state.martParam = mart.name;
                		biomart._state.resource = 'datasets';
                		biomart._state.mart = mart;
                	}
                } else {
                	if (mart.group==biomart._state.martName) {
                    	biomart._state.martParam += mart.name + ",";
                    	biomart._state.resource = 'datasets/mapped';
                    	biomart._state.mart.push(mart);
                	}
                }
            };
            // Delete the last coma
            if (biomart._state.resource=='datasets/mapped') {
            	biomart._state.martParam = biomart._state.martParam.substring(0, biomart._state.martParam.length-1);
            }
            // Then load all datasets to the concerned marts
    	    biomart.resource.load(biomart._state.resource, function(jsond) {    	
    	    	var isMapped = biomart.utils.hasGroupedMarts(),
    	    		ds = '';
    	    	if (isMapped) {
    	    		for (var k in jsond) {
                    	var datasets = jsond[k],
                    		mart;
                        for (var i=0, curr; curr=biomart._state.mart[i]; i++) {
                            if (curr.name == k) {
                                mart = curr;
                                break;
                            }
                        }
                        for (i=0, ds; ds=datasets[i]; i++) {
                        	ds.mart = mart;
                        	biomart._state.datasets.push(ds);
                        }
                    }
    	    	}
    	    	callBackFunction();
    	    }, {mart: biomart._state.martParam});
        }, {name: biomart._state.root});
    }
	
	/* 
	 * Return the value to use to build the search link to the martform
	 * @param type the biomart plugin type used to make the search
	 * @param callback the callback function to call
	 * @param prefix : the current dataset prefix
	 */
    self.getSearchGUI = function(callback, datasetType,prefix, searchType) {
    	searchType = searchType || "martform";
		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'portal.json?guitype=' + searchType,
	        success: function(data) {
	        	
	        	if (data.guiContainers.length > 1) {
	        		callback({ 
    	        		"error"		: true,
    	        		"message"	: "ngspipelines.utils.getSearchGUI: An error occurred while attempting to get the martform gui value!"
        			});
	        	} else if (data.guiContainers.length == 0) {
	        		callback({ 
    	        		"error"		: true,
    	        		"message"	: "ngspipelines.utils.getSearchGUI: No gui found for type " + searchType + "!"
        			});
	        	} else {
	        		
	        		var guicontainer = data.guiContainers[0] ,
	        			martformInfo = {
	        				"gui" 		: guicontainer.name ,
	        				"config" 	: "",
	        				"error"		: false
		        		};
	        		
	        		for (var index in data.guiContainers[0].marts) {
	        			
	        			if (index != "remove") {
	        				
	        				var mart_object = data.guiContainers[0].marts[index],
	        					mart_name = mart_object.name,
	        					mart_config = mart_object.config,
		        				mart_prefix = mart_name.split("_")[0],	
		        				mart_type = self.getType(mart_config),
		        				mart_group = mart_object.group;
		        				
		        			if ( ( mart_type == datasetType)  && (mart_prefix == prefix) ){
		        				martformInfo.config = mart_group;
		        				break;
		        			}
	        			}
	        		}
	        		callback( martformInfo );
	        	}
	        },
	        error: function() {
	        	callback( { 
	        		"error"		: true,
	        		"message"	: "ngspipelines.utils.getSearchGUI: An error occurred while attempting to get the martform gui value!"
    			});
	        }
		});
    }
    
	/* 
	 * Return a NGSPipelines plugin name considering a martname
	 * @param martName the martname
	 * @return string the plugin name linked to the martname
	 */
	self.getPluginName = function(martName) {
	    return martName.toLowerCase();
	}
    
	/* 
	 * Return the plugin JS files to import
	 * @param projectDataset the project dataset (only here to request on a basic table)
	 * @param projectDataset the project dataset (only here to request on a basic table)
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @param subDir which subdir should be used
	 * @successCallBackParams Array list of JS files to import
	 */
	self.loadPluginJSFile = function(projectDataset, applicationDataset, successCB, errorCB, subDir) {
		
		if (!subDir) { subDir = ""; }
		var prefix = ngspipelines.utils.getDatasetPrefix(projectDataset),
			appType = ngspipelines.utils.getType(applicationDataset.name),
			pluginName = ngspipelines.utils.getPluginName(appType),
			attributes = [
	  		    '<Attribute name="', prefix,'_project__Project__main__directory" />'
  		    ].join(''),
		    filters = [
		        '<Filter name="', prefix,'_project__Project__main__directory" value = "' + pluginName + '" />',
		        '<Filter name="', prefix,'_project__Project__main__name" value = "' + subDir + '" />'
	        ].join('');
				
  		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		    	query: '<!DOCTYPE Query><Query client="webbrowser" processor="PluginJSFiles" limit="-1" header="0"><Dataset name="' + projectDataset.name + '" config="' + projectDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		    },
	        success: function(data) {
	        	if (data.hasOwnProperty("error")) {
	        		errorCB("ngspipelines.utils.getPluginJSFile: An error occured while attempting to load " + pluginName + " plugin javascript files!");
	        	} else {
	        		yepnope({
	        			load: data.files,
	        			complete: function () {
	        				successCB()
	        			}
	        		});
	        	}
	        },
	        error: function() {
	        	errorCB("ngspipelines.utils.getPluginJSFile: An error occured while attempting to load " + pluginName + " plugin javascript files!");
	        }
    	});
	}
	
	/* 
	 * Format a number of octet to a human readable string
	 * @param size a number of octet to convert
	 * @return string the string representation of a byte
	 */
	self.getOctetStringRepresentation = function(size) {
		if (size == 0 ) return "0 bytes";

		var octetsLink = ["bytes", "Kb", "Mb", "Gb", "Tb", "Pb", "Eb", "Zb"],
	    	p = parseInt(Math.ceil(parseFloat(size.length)/parseFloat(3) - parseFloat(1))),
	    	pow_needed = p * 10;
	    	pow_needed = Math.pow(2, pow_needed);

	    var value = String(parseFloat(size)/parseFloat(pow_needed)),
	    	tmp = value.split(".");
	    
	    if ( p > 0 && tmp.length > 1){
	    	value = tmp[0] + "." + tmp[1].slice(0,2);
	    }

	    try {
	    	value = value + " " + octetsLink[p]
	    	return value;
	    } catch(err) {
	    	throw "ngspipelines.getOctetStringRepresentation: Unexpected input value for disk space size: " + size;
	    }
	}
    
	/* 
	 * Search for an analysis dataset from all provided datasets
	 * @param datasets list of datasets within the requested dataset
	 * @return json the analysis dataset
	 */
	self.getAnalysesDataset = function(datasets) {
        for (var i=0, ds; ds=datasets[i]; i++) {
        	if (ngspipelines.utils.isAnAnalysisDataset(ds)) {            		
        		return ds;
        	}
        }
	}
    
	/* 
	 * Search for a project dataset from all provided datasets
	 * @param datasets list of datasets within the requested dataset
	 * @return json the project dataset
	 */
	self.getProjectDataset = function(datasets) {
        for (var i=0, ds; ds=datasets[i]; i++) {
        	if (ngspipelines.utils.isAProjectDataset(ds)) {            		
        		return ds;
        	}
        }
	}
	
    /*
     * Very simple but very fast query param parsing.
     * If multiple entries exist for a param, the value of the first occurrence is used.
     * This is not normal behaviour, but is not allowed in our application anyway.
	 * @param s the url
     */
    self.simpleQueryParams = function(s) {
        try {
        	if (!s) return null;
        	var url = s.split('?'), arr = url[1].split('&'), n = arr.length, dict = {}; 
        	while(n--) { var me = arr[n]; me = me.split('='); dict[me[0]] = decodeURIComponent(me[1]).replace(/\+/g, ' ') }
        	return dict;
        } catch (err) {
        	return {};
        }
    }
    
	/* 
	 * Return the max of a table
	 * @param values the table of values
	 * @return int the max value
	 */
	self.max = function(values){
	    return Math.max.apply( Math, values );
	};

	/* 
	 * Return the min of a table
	 * @param values the table of values
	 * @return int the min value
	 */
	self.min = function(values){
	    return Math.min.apply( Math, values );
	};
	
	/* 
	 * Reformat Biomart query result to a more usable way
	 * @param bioMartQueryResult the query result in json format
	 * @param exceptions table of values to except
	 * @return json the reformated result
	 */
	self.reformatBioMartQueryResult = function(bioMartQueryResult , exceptions) {
		if (exceptions == null) { exceptions = new Array();	}
		try {
	    	var json = new Array();	        	
	    	for (var index in bioMartQueryResult.data) {
	    		if (index != "remove") {
	    			var queryResult = {};
	    			for (var key in bioMartQueryResult.data[index]) {
	    				if ($.inArray(key, exceptions) == -1) {
		    				var modified_key = key.charAt(0).toLowerCase() + key.slice(1);
		    				modified_key = modified_key.replace(/ /g, "_");
		    				queryResult[modified_key] = bioMartQueryResult.data[index][key];
	    				}
	    			}
	    			json.push(queryResult);
	    		}
	    	}
	    	return (json);
		} catch (err) {
			return (bioMartQueryResult);
		}
	}
	
	/* 
	 * @param IDs an Array of object IDS to delete/add from favorites 
	 * @param dataset the variantdenovo dataset
	 * @param analysisType the type of the analysis used to describe the favorites
	 * @param successCB the callback function to call when updating is over and succeed
	 * @param errorCB the callback function to call when updating is over and failed
	 */
	self.updateFavoriteStatus = function(IDs, analysisType, analysisDataset, successCB, errorCB) {

		var prefix = ngspipelines.utils.getDatasetPrefix(analysisDataset),
			ids = IDs.join(',')
			attributes = [
			    '<Attribute name="', prefix,'_analysis__Analysis__main__analysis_type" />',
			    '<Attribute name="', prefix,'_analysis__Result__dm__value" />'
			].join(''),
		    filters = [
			    '<Filter name="', prefix,'_analysis__Analysis__main__analysis_type" value = "', analysisType,'" />',
			    '<Filter name="', prefix,'_analysis__Result__dm__value" value = "', ids,'" />'
			].join('');			

		$.ajax({
		    url: BIOMART_CONFIG.service.url + 'results',
		    data: {
		        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="Favorites" limit="-1" header="0"><Dataset name="' + analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
		        },
		        success: function(results) {
		        	var isFavorite = null ;
		        	
		        	if (results.isFavorite == "true" ){
		        		isFavorite = true ;
		        	}
		        	else if (results.isFavorite == "false" ){
		        		isFavorite = false ;
		        	}
		        	else {
		        		return ;
		        	}
		        	successCB( isFavorite );
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		        	errorCB("ngspipelines.utils.updateFavoriteStatus: An error occurred while attempting to update favorite statos of the following objects ids=" + IDs + "!" + errorThrown);
		        }
		});
	}
	
	/* 
	 * Trim a string from both begining and end
	 * @param str a string to trim
	 * @return string the trimmed string
	 */
	self.trim = function(str) {
		return (str.replace(/^[\s\xA0]+/, "").replace(/[\s\xA0]+$/, ""));
	}
	
	/* 
	 * Does the string ends with the specified pattern
	 * @param str the string to test
	 * @param pattern the pattern to test
	 * @return boolean True if str ends with the specified pattern, False otherwise
	 */
	self.endsWith = function(str, pattern) {
		return (str.match(pattern+"$")==pattern);
	}
	
	/* 
	 * Does the string starts with the specified pattern
	 * @param str the string to test
	 * @param pattern the pattern to test
	 * @return boolean True if str starts with the specified pattern, False otherwise
	 */
	self.startsWith = function(str, pattern) {
		return (str.match("^"+pattern)==pattern);
	}
	
	/* 
	 * Does the string match with the specified pattern
	 * @param str the string to test
	 * @param pattern the pattern to test
	 * @return boolean True if str match with the specified pattern, False otherwise
	 */
	self.regex = function(str,pattern) { 
       return (str.match(pattern)) ? true : false; 
    };
	/* 
	 * Fill the specified template with the specified data on the item
	 * @param item the DOM object where the content will be displayed
	 * @param template the jquery.tmpl template url
	 * @param data the data in json format
	 * @param callback what should be done when the template is filled out
	 * @callbackParams none
	 */
	self.tmpl = function(item, template, data, callback, callback_err) {
		if (callback_err == undefined) { callback_err = function() {} }
		$.ajax({
			url: template,
			dataType: "text",
			success: function(val, status, xhr) {
				$.tmpl(val,data).appendTo(item);
				callback();
			},
			error: function(val, status, xhr) {
				callback_err();
			}
		});
	}
	
	/* 
	 * What is the dataset prefix
	 * @param dataset the dataset
	 * @return string the dataset prefix
	 */
	self.getDatasetPrefix = function(dataset) {
		var parts = dataset.name.split("_");
		return parts[0];
	}
	
	/* 
	 * What is the string type
	 * @param string the string to test
	 * @return string the type
	 */
	self.getType = function(string) {
		var parts = string.split("_");
		return parts[1];
	}
	
	/* 
	 * What is the dataset type
	 * @param dataset the dataset
	 * @return string the dataset type
	 */
	self.getDatasetType = function(dataset) {
		return self.getType(dataset.name);
	}
	
	/* 
	 * Is the dataset a project dataset
	 * @param dataset the dataset
	 * @return boolean True if it's a project dataset, False otherwise
	 */
	self.isAProjectDataset = function(dataset) {
		return (self.getDatasetType(dataset) === "project"); 
	}
	
	/* 
	 * Is the dataset an analysis dataset
	 * @param dataset the dataset
	 * @return boolean True if it's an analysis dataset, False otherwise
	 */
	self.isAnAnalysisDataset = function(dataset) {
		return (self.getDatasetType(dataset) === "analysis"); 
	}
	
	/* 
	 * Is the dataset a library dataset
	 * @param dataset the dataset
	 * @return boolean True if it's a library dataset, False otherwise
	 */
	self.isALibrariesDataset = function(dataset) {
		return (self.getDatasetType(dataset) === "library"); 
	}
	
	/* 
	 * Search for a ngspipelines dataset from all provided datasets
	 * @param datasets list of datasets within the requested dataset
	 * @param pluginDataset the dataset to use for the report
	 * @return json the variantdenovo dataset
	 */
	self.getReportDataset = function(datasets, pluginDataset) {
        for (var i=0, ds; ds=datasets[i]; i++) {
        	if (ngspipelines.utils.getDatasetType(ds) === pluginDataset) {            		
        		return ds;
        	}
        }
	}
	
	/**
	 * Retrieve the sequence part from a sequence string
	 * @param str : a sequence string (with or without header)
	 * @returns a string 
	 */
	self.getSequenceString = function (str){
		if ( str == null ){	return ""; }
		sp = str.split('\n');
	    return sp.length > 1 ? sp[1] : sp[0];
	}
		
	/* 
	 * Library dataset loading function
	 * @param libraryDataset the library dataset
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the library object
	 */
	self.hasLibraries = function(libraryDataset, datasetType, successCB) {
		
		if (ngspipelines.utils.isALibrariesDataset(libraryDataset)) {			
			var prefix = ngspipelines.utils.getDatasetPrefix(libraryDataset),
				attributes = [
				    '<Attribute name="', prefix,'_library__Library__main__library_name" />'
				].join(''),
			    filters = [
				    '<Filter name="', prefix,'_library__Library__main__categorie" value = "', datasetType, '" />'
				].join('');
	
			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
			        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="JSON" limit="-1" header="0"><Dataset name="' + libraryDataset.name + '" config="' + libraryDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
			        },
			        success: function(results) {
			        	successCB(true);
			        },
			        error: function() {
			        	successCB(false);
			        }
			});
		} else {
			errorCB("ngspipelines.utils.hasLibraries: An error occurred while attempting to load " + libraryDataset.name + " as a library!");
		}
	}
	
	/* 
	 * favorites loading function
	 * @param dataset the dataset
	 * @param analysisDataset the analysis dataset
	 * @param applicationName the application name
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the object
	 */
	self.hasBlastFeature = function(dataset, analysisDataset, applicationName, analysisType, successCB, errorCB) {
		
		// First load from the analysis table the favorites
		if (ngspipelines.utils.isAnAnalysisDataset(analysisDataset)) {
			var prefix = ngspipelines.utils.getDatasetPrefix(analysisDataset),
				attributes = [
				    '<Attribute name="', prefix,'_analysis__Analysis__main__analysis_name" />'
			    ].join(''),
			    filters = [
				    '<Filter name="', prefix,'_analysis__Analysis__main__analysis_type" value = "', analysisType,'" />',
				    '<Filter name="', prefix,'_analysis__Analysis__main__categorie" value = "', applicationName,'" />'
				].join('');
			
			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
			        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="TSV" limit="-1" header="0"><Dataset name="' + analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
			        },
			        success: function(results) {
			        	hasBlastFeature = false;
			        	if (results){
			        		hasBlastFeature = true;
			        	}
			        	successCB(hasBlastFeature);
			        },
			        error: function(errorMessage) {
			        	errorCB(errorMessage);
			        }
			});
		} else {
			errorCB("ngspipelines.application.hasBlastFeature: An error occurred while attempting to load " + analysisDataset.name + " as an analysis!");
		}
		
	}
	
	
	/* 
	 * favorites loading function
	 * @param dataset the dataset
	 * @param analysisDataset the analysis dataset
	 * @param applicationName the application name
	 * @param successCB the callback function to call when loading is over and succeed
	 * @param errorCB the callback function to call when loading is over and failed
	 * @successCallBackParams json the object
	 */
	self.hasAnalysis = function(dataset, analysisDataset, applicationName, analysisTypes, successCB, errorCB) {
		
		// First load from the analysis table the favorites
		if (ngspipelines.utils.isAnAnalysisDataset(analysisDataset)) {
			var prefix = ngspipelines.utils.getDatasetPrefix(analysisDataset),
				attributes = [
				    '<Attribute name="', prefix,'_analysis__Analysis__main__analysis_name" />'
			    ].join(''),
			    filters = [
				    '<Filter name="', prefix,'_analysis__Analysis__main__analysis_type" value = "', analysisTypes.join(),'" />',
				    '<Filter name="', prefix,'_analysis__Analysis__main__categorie" value = "', applicationName,'" />'
				].join('');
			$.ajax({
			    url: BIOMART_CONFIG.service.url + 'results',
			    data: {
			        	query: '<!DOCTYPE Query><Query client="webbrowser" processor="TSV" limit="-1" header="0"><Dataset name="' + analysisDataset.name + '" config="' + analysisDataset.mart.name + '">' + attributes + filters + '</Dataset></Query>'
			        },
			        success: function(results) {
			        	final_result={};
			        	$.map( analysisTypes, function( val, i ) {
			        		// Do something
			        		final_result[val]=false
			        		});
			        	$.map( results.split("\n"), function( val, i ) {
			        		// Do something
			        			if (val != "") {
			        			final_result[val.toLowerCase().replace(" ","_")]=true;
			        			}
			        		});
			        	
			        	successCB(final_result);	
			        },
			        error: function(errorMessage) {
			        	errorCB(errorMessage);
			        }
			});
		} else {
			errorCB("ngspipelines.application.hasBlastFeature: An error occurred while attempting to load " + analysisDataset.name + " as an analysis!");
		}
		
	}
	
});

})(jQuery);
