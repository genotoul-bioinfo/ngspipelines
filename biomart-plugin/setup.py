# -*- coding: utf-8 -*-

import sys

if float(str(sys.version_info.major) + '.' + str(sys.version_info.minor)) < 2.7 :
    sys.exit('Required python version2.7 or higher ...\n')

import os
import argparse
import shutil
import subprocess
import errno
import logging
import re

CURRENT_DIR = os.path.dirname(os.path.abspath(os.path.expanduser(__file__)))
DISTRIB_DIR = os.path.dirname(CURRENT_DIR)
CURRENT_DIR_BASENAME = os.path.basename(CURRENT_DIR)
NGSPIPELINES_DIST = os.path.join( DISTRIB_DIR , 'dist')
NGSPIPELINES_PLUGIN = os.path.join( CURRENT_DIR , 'ngspipelines')

def execute_command(command , verbose = True):
    p = subprocess.Popen( command, shell = True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
    if verbose :
        while True:
            line = p.stdout.readline()
            if line == '' and p.poll() != None:
                break
            logging.info( line.rstrip() ) 
    return p.returncode

def install( biomart_release, storage = None, memory = None, host = None,
             port = None, url = None, registry = None, force_comp = False, overwrite = False):
    '''
        Installation ...
        @param biomart_release: biomart release path
        @param storage: ngspipelines storage directory
        @param memory: allocated memory in Mb
        @param host: host ip
        @param port: port
        @param url: deployment url
        @param registry: registry xml file
        @param force_comp: force biomart compilation
        @param overwrite: overwrite existing instance distribution
    '''
    logging.info('Start ngspipelines contig browser install')
    biomart_dist = os.path.join( biomart_release, 'dist')
    biomart_build = os.path.join( biomart_release, 'build')
    
    if os.path.exists(NGSPIPELINES_DIST) :
        if overwrite :
            execute_command( 'rm -rf  ' + NGSPIPELINES_DIST )
        else :
            logging.info( 'ngspipeline dist exists already in ' + DISTRIB_DIR )
            sys.exit()
    
    if not os.path.exists( biomart_dist )  or force_comp  :
        logging.info('Compile biomart release ...')
        shutil.rmtree(path = biomart_dist , ignore_errors = True)
        shutil.rmtree(path = biomart_build, ignore_errors = True)
        execute_command( 'ant -f  ' + os.path.join(biomart_release,"build.xml") )
    
    logging.info('Copy of biomart dist')
    execute_command( 'cp -r ' + biomart_dist + '  ' + NGSPIPELINES_DIST )
    
    logging.info('Copy of ngspipelines theme ')
    execute_command( "rsync -az --exclude '*.svn*'  " + os.path.join( CURRENT_DIR, 'theme-rc6' , '*') + '   ' + os.path.join( NGSPIPELINES_DIST, 'web') )
    
    logging.info('ngspipelines plugin link')
    execute_command( 'ln -sf  ' + os.path.join( '..', '..', CURRENT_DIR_BASENAME, 'ngspipelines') + '   ' + os.path.join( NGSPIPELINES_DIST , 'plugins') )
    execute_command( "sed -i -e 's,##STORAGE_DIR##," + storage + ",'    " + os.path.join( NGSPIPELINES_PLUGIN, 'ngspipelines.properties') )
    
    #logging.info('Compile jbrowse dependencies')
    #execute_command( 'cd ' + os.path.join( NGSPIPELINES_PLUGIN, 'public', 'ngspipelines', 'js', 'lib', 'jbrowse') + ' ; sh ./setup.sh')
    
    registry_basename = ''
    if registry :
        execute_command( 'cp  ' + registry + '  ' + os.path.join(NGSPIPELINES_DIST, 'registry') )
        registry_basename = os.path.basename(registry)
        execute_command( "sed -i -e 's,^biomart\.registry\.file.*,biomart.registry.file = " + os.path.join( 'registry', registry_basename ) + ",'  " + os.path.join( NGSPIPELINES_DIST, 'biomart.properties'))
        
        aaa = subprocess.check_output( "grep -m 1 'jdbc' " + registry , shell = True)
        jdbc_line = None
        for l in aaa.split() :
            if 'jdbc' in l :
                jdbc_line = l
                break
        if jdbc_line :
            splited =  jdbc_line.split('|')
            database = re.sub( r'/$' , '' , re.sub( r".*jdbc:mysql://", "", splited[0]))
            user = splited[3]
            pwd = splited[4]
            execute_command( "sed -i -e 's,^session\.connection.*,session.connection = jdbc:mysql://" + database + "/biomart_sessions?user=" + user + "\&password=" + pwd +",' " + os.path.join( NGSPIPELINES_DIST, 'biomart.properties'))
    
    execute_command( "sed -i -e 's,^biomart\.registry\.key\.file,#biomart.registry.key.file,' " + os.path.join( NGSPIPELINES_DIST, 'biomart.properties'))
    execute_command( "sed -i -e 's,^http\.host\s*=.*,http.host = " + str(host) + ",' " + os.path.join( NGSPIPELINES_DIST, 'biomart.properties'))
    execute_command( "sed -i -e 's,^http\.port\s*=.*,http.port = " + str(port) + ",' " + os.path.join( NGSPIPELINES_DIST, 'biomart.properties'))
   
    if url :
        url = re.sub(r':\d+$', '', re.sub( r'\/$', '',  str(url ))) + ':' + str(port)
        execute_command( "sed -i -e 's,^#http\.url\s*=.*,http.url = " + url + ",' " + os.path.join( NGSPIPELINES_DIST, 'biomart.properties'))
        
    execute_command( "sed -i -e 's,DEFAULT_MAX_HEAP=.*,DEFAULT_MAX_HEAP=\"" + str(memory) +  "m\",' " + os.path.join( NGSPIPELINES_DIST, 'scripts', 'biomart-server.sh'))
    
    with open( os.path.join( DISTRIB_DIR, 'ngspipelines-server') , 'w') as f:
        f.write('''#!/bin/bash
CURRENT_PATH=$(cd $(dirname $0); pwd -P)
NGSPIPELINES_DIST_DIR=${CURRENT_PATH}/dist
export BIOMART_HOME=${NGSPIPELINES_DIST_DIR}
cd ${NGSPIPELINES_DIST_DIR} ; scripts/biomart-server.sh $1 -Dbiomart.properties=${NGSPIPELINES_DIST_DIR}/biomart.properties
''' )
        
    execute_command( 'chmod +x ' + os.path.join( DISTRIB_DIR, 'ngspipelines-server'))

if __name__ == '__main__':
    
    '''
        Installation tree :
        -------------------
        
        Instance_name/
            biomart-plugin/
                setup.py
            dist/
                biomart.properties
                ...
                pungins/
                    ngspipelines --> ../../biomart-plugin/ngspipelines
                ...
                
            ngspipelines-server *
    '''
    
    logging.basicConfig( format='%(asctime)s : %(message)s', level=logging.DEBUG )
    parser = argparse.ArgumentParser( description = 'NGSPipelines install tool' )
    parser.add_argument( '-b','--biomart', dest="biomart_release", help = 'Biomart release path (relative or absolute)', default = '')
    parser.add_argument( '-s', '--storage' , dest='storage_dir', help='Path to ngspipelines data storage directory', default = '')
    parser.add_argument( '-m', '--mem' , dest='mem', help='Instance allocated memory in megabytes [1024]', default= 1024 , type = int )
    #parser.add_argument( '-H', '--host' , dest='host', help='HTTP host [0.0.0.0]', default= '0.0.0.0' )
    parser.add_argument( '-p', '--port' , dest='port', help='HTTP deployment port [9000]', default = 9000 , type = int )
    parser.add_argument( '-u', '--url' , dest='url', help='HTTP public url', default = None)
    parser.add_argument( '-r', '--registry' , dest='registry_file', help='Biomart registry file', default = None)
    parser.add_argument( '-f', '--force' , dest='force', help='Force biomart release compilation', action="store_true", default=False)
    parser.add_argument( '--overwrite' , dest='overwrite', help='Overwrite existing installation', action='store_true', default= False )
    parser.add_argument( '-U', '--update' , dest='update', help='Update theme modificationForce biomart release compilation', action="store_true", default=False)
    options = parser.parse_args()
    
    # update don't need something else
    if options.update :
        logging.info('Start ngspipelines update')
        execute_command( "cp -r " + os.path.join( CURRENT_DIR, 'theme-rc6' , '*') + '   ' + os.path.join( NGSPIPELINES_DIST, 'web') )
        sys.exit()
    
    if not os.path.exists(options.biomart_release) :
        parser.error( 'biomart release dir "' + str(options.biomart_release) + '" does not exists');
    
    if not os.path.exists(options.biomart_release) :
        parser.error( 'storage dir "' + str(options.storage_dir) + '" does not exists');
        
    storage_dir = None
    registry_file = None
    biomart_release = os.path.abspath(os.path.expanduser(options.biomart_release))
    try : storage_dir = os.path.abspath(os.path.expanduser(options.storage_dir))
    except : pass
    try : registry_file = os.path.abspath(os.path.expanduser(options.registry_file))
    except : pass
    
    install( biomart_release = biomart_release , storage = storage_dir ,  registry = registry_file,
             memory = options.mem, host = '0.0.0.0', port = options.port, url = options.url, 
             force_comp = options.force, overwrite = options.overwrite)
