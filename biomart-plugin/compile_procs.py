# -*- coding: utf-8 -*-

import setup

import os
import sys
import argparse
import shutil
import subprocess
import logging

from os.path import join

#
# Developer configuration parameters :
#    - SHOW_COMPILE_PROCESSORS   : if True the compile processors option will be available in the script options
#    - EXTERNAL_JAVA_LIBS        : external java libraries used to compile processors
#    - BIOMART_JAVA_LIBS         : biomart libraries used to compile processors
#
EXTERNAL_JAVA_LIBS      = [ 'activation.jar', 'mail.jar', 'jsc.jar', 'commons-compress-1.4.1.jar' ]
BIOMART_JAVA_LIBS       = ['commons-lang.jar', 'biomart-0.8.jar' , 'guava-r09.jar', 'jackson-core-asl-1.4.0.jar', 
                            'jackson-mapper-asl-1.4.0.jar', 'jdom-1.0.jar', 'opencsv-2.1.jar' ]


DEFAULT_BIOMART_RELEASE_PATH = 'fake_path'

def compile_processors( biomart_plugin, javalibs):
    logging.info('Processor compilation')
    processors_build_path = join(biomart_plugin,"build")

    if not os.path.exists(processors_build_path) : 
        os.mkdir( processors_build_path )
    if not os.path.exists(join(processors_build_path,"classes")) : 
        os.mkdir( join(processors_build_path,"classes") )
    
    javac_ant_file = join( processors_build_path , 'javac-ngspipelines.xml')
    
    with open(javac_ant_file, 'w') as f :
        content = """
<project default="compile">
    <target name="compile">
        <javac srcdir="{src}" destdir="{classes}" classpath="{libraries}" />
        <jar destfile="{jarloc}/{jarname}">
            <fileset dir="{classes}" />
            <fileset dir="{src}"/>
        </jar>
    </target>
</project>""".format( classes       = join(processors_build_path,"classes"),
                      src           = join(biomart_plugin,"ngspipelines","src-rc6"),
                      jarname       = "ngspipelines-processors.jar",
                      jarloc        = join(biomart_plugin,"ngspipelines","lib"),
                      libraries     = javalibs )
        f.write(content)
    
    setup.execute_command( 'ant -f  ' + javac_ant_file )
    shutil.rmtree( processors_build_path , ignore_errors = True)

if __name__ == '__main__':
    logging.basicConfig( format='%(asctime)s : %(message)s', level=logging.DEBUG )
    
    parser = argparse.ArgumentParser( description = 'Developer tool to compile ngspipelines processors' )
    parser.add_argument( '-b','--biomart', dest="biomart_release", help = "Biomart release path (relative or absolute)", default = DEFAULT_BIOMART_RELEASE_PATH)
    args = parser.parse_args()
    
    biomart_release = args.biomart_release
    if not os.path.exists(biomart_release) :
        parser.error( str(biomart_release) + ' does not exists');
        
    biomart_release     = os.path.abspath(os.path.expanduser(args.biomart_release))
    biomart_plugin      = os.path.dirname(os.path.abspath(__file__))
    ngspipelines_plugin = join( biomart_plugin, 'ngspipelines') 
    ngspipelines_libs   = ':'.join( [ join( ngspipelines_plugin, 'lib', x ) for x in EXTERNAL_JAVA_LIBS ] )
    biomart_libs        = ':'.join( [ join( join( biomart_release, 'dist') , 'lib', x ) for x in BIOMART_JAVA_LIBS ] ) 
    javalibs            = ngspipelines_libs + ':' + biomart_libs
    
    compile_processors(biomart_plugin, javalibs)