<%@ page language="java"%>
<%@ page session="false" %>
<%@ page contentType="text/html;charset=UTF-8"%>

<div class="ngsp-footer">
 <div class="ngsp-wrapper">
  <ul id="ngsp-footer-cols">
   <li class="ngsp-col">
	   <h6>Links</h6>
	    <ul>
	     <li class="ngsp-page_item">
	      <a href="http://bioinfo.genotoul.fr/" target="_blank" style="margin-left: 0px;">GenoToul Bioinfo platform</a>
	     </li>
	     <li class="ngsp-page_item">
	      <a href="http://www.sigenae.org/" target="_blank" style="margin-left: 0px;">Sigenae platform</a>
	     </li>
	     <li class="ngsp-page_item">
	      <a href="http://www.international.inra.fr/" target="_blank" style="margin-left: 0px;">INRA - National Institute for Agricultural Research</a>
	     </li>
	    </ul>
   </li>
   <li class="ngsp-col ngsp-col2">
	   <h6>About NGS Pipelines</h6>
	    Second generation sequencing platforms provide new insight of the biological phenomenons taking place in studied samples.
	    NGS Pipelines is a set of workflows and a query and visualisation environment build upon
	    <a class="ngsp-footer-link" href="http://www.biomart.org/" target="_blank">biomart</a> which aims at simplifying biological knowledge extraction.
	    <br/><br/>A <a class="ngsp-footer-link" href="/ngspipelines/faq.jsp">FAQ</a> is available to give some information about the most usual questions asked.
   </li>
  </ul>
 </div>
</div>
<div class="ngsp-bottom">
 <div class="ngsp-bottom2">
 Copyright © 2012, INRA | Designed by <a class="ngsp-footer-link" href="http://bioinfo.genotoul.fr/" target="_blank">GenoToul Bioinfo</a>
 and <a class="ngsp-footer-link" href="http://www.sigenae.org/" target="_blank">Sigenae</a> teams | Optimized for 
 <a class="tooltip-firefox firefox" href="http://www.mozilla.org/firefox" target="_blank" title="Mozilla Firefox"> </a>
 <a class="tooltip-chrome chrome" href="http://www.google.com/chrome" target="_blank" title="Google Chrome"> </a>
</div>