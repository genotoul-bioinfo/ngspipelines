\documentclass{bioinfo}
\copyrightyear{2013}
\pubyear{2013}

\begin{document}
\firstpage{1}

\title[short Title]{RNAbrowse: RNA-seq de novo assembly results browser}
\author[Mariette \textit{et~al}]{J\'{e}r\^{o}me Mariette\,$^{1,*}$, C\'{e}line
Noirot\,$^{1,*}$,
Ibounyamine Nabihoudine\,$^1$, Philippe Bardou\,$^2$, Claire Hoede\,$^1$,
Anis Djari\,$^2$, C\'{e}dric Cabau\,$^2$, Christophe
Klopp\,$^{1,2,}$\footnote{to
whom correspondence should be addressed}}
\address{$^{1}$Plate-forme bio-informatique Genotoul, INRA, Biom\'{e}trie et
Intelligence Artificielle, BP 52627, 31326 Castanet-Tolosan Cedex, France.\\
$^{2}$Plate-forme SIGENAE, INRA, G\'{e}n\'{e}tique Cellulaire, BP 52627, 31326
Castanet-Tolosan Cedex, France.}

\history{Received on XXXXX; revised on XXXXX; accepted on XXXXX}

\editor{Associate Editor: XXXXXXX}

\maketitle

\begin{abstract}

\section{Summary:}
Transcriptome analysis based on a de novo assembly of next generation RNA
sequences is now performed routinely in many laboratories. The generated
results, including contig sequences, quantification figures, functional
annotations and variation discovery outputs are usually very diverse and bulky.
This article presents an user oriented storage and visualisation environment
permitting to explore the data in a top-down manner, going from general
graphical views to all possible details. The software package is based on
biomart, easy to install and populate with local data.

\section{Availability:}
The software package is available under the GNU General Public License (GPL) at
http://bioinfo.genotoul.fr/RNAbrowse.

\section{Contact:}
\href{support.genopole@toulouse.inra.fr}{support.genopole@toulouse.inra.fr}
\end{abstract}

\section{Introduction}

The massive sequencing cost decrease has attracted a large community of new
users, some of them studying organisms for which the reference genome sequence
is still not available. When trying to understand mechanisms taking place at the
gene level they usually would start with a de novo transcriptome assembly
approach. Software packages such as Trinity (Grabherr et al., 2011) or Oases 
(Schulz et al., 2012) are mature enough to produce reliable  contigs from short 
reads. The analysis performed on the assembled contigs usually produce a large 
amount of heterogeneous results including variations, functional annotation 
and expression measurements. Even if there are key elements found in close to
all result sets, the processing pipelines and the reference databases used can 
vary quiet a lot. To provide access to this data, teams usually set up a web
server with a BLAST query form, static result pages describing the contigs with 
corresponding annotations and download links.


Cbrowse (Pei et al., 2012) is one example of an environment giving an organised
access to this kind of results. It offers some graphical view and query form.
The functional annotation part is not implemented yet and the query
possibilities are very limited.

In its last version (0.8), biomart (Smedley et al., 2009) is developed as an
easily extensible query infrastructures which can be specialized in the
presentation of focused data types. On top of the database and beside the
proposed query forms it is possible to add new pages as plug-ins in order to
present data in a user-friendly way. This approach has been used to develop
RNAbrowse.

\begin{methods}
\section{Software}

RNAbrowse includes two components: a web-based user interface and an
administration command line tool presented here-after.

\subsection{Implementation and installation}

RNAbrowse is based on a newly defined  biomart schema specialized in RNA-Seq de
novo assembly result data which includes two marts storing contig and variant
data and some extra tables for meta-data. The WEB-based user interface is
implemented as a biomart plugin using the latest jquery and highcharts features.
To perform all third parts requests, the software takes advantage of the biomart
API and implements several biomart processors to prepare the data before
presenting it.

The command line administration tool is based on  a workflow execution
environment called jflow. It is developed in Python. The installation requires a
standard unix server and a MySQL database. The downloadable installation
archive, provided on the project web-site, has only to be uncompressed before
setting up a first instance. In order to ease the testing, a minimal and a
complete example dataset are included in the archive.

\subsection{WEB interface}

The web interface was designed to display the data starting with general views
before zooming into detailed ones. It is organised in four levels. The first,
called the instance level, corresponds to the home page of the web-site
presenting all the available projects. A project gathers the data of an
assembly. The second level is entered after having chosen a project. This
project introduction page contains a picture of the assembles species provided
using the command line, a description text and a table including all the
analysis processing steps with software names, parameters and versions, thus to
ease material and method writing.

The third level is accessed through the menu bar items at the top of the project
page and presents general informations about contigs and variant. It also
includes the download page. The contigs and variants overview pages include a
set of graphics showing general statistics, containing for example the contig
length histogram, alignment rate bar-chart, mostly represented species in the
functional annotation histogram or variants types distribution among a
pie-chart. These graphs enable rapid assembly validation and comparison. For
contigs, the next section includes detailed information about the different
libraries sequenced in the study and provides access to tools such as Venn
diagrams and a differential digital display. The last section corresponds to the
favourite contigs or variants table which can be updated by the user while
exploring detailed views. The forth level sections gathers all information on a
contig or a variant and provides multiple tools to analyse them further.

To illustrate the provided features we could imagine a simple use case in which a
user would like to find all contigs corresponding to a gene of interest in
another species. This can be done by an alignment versus the contigs using the
blast query form or by a name or description search using the biomart form. The
user can then add the found contigs to the favourite table. For each contig, the
sequence can be extracted to perform a multiple alignment in order to check if
different splice forms have been assembled. All possible open reading frames can
be sought. Annotations can be graphically display in jbrowse (Oscar et al.,
2013). It is also possible to graphically verify if the expression levels along
the contig are conserved between libraries. 

All generated graphics can directly be printed from the web-page or downloaded
in JPG, PNG, PDF, SVG formats or as a CSV file.  Tables can be sorted on all
columns, text searched and exported to the clipboard or as CSV files. The
download page files are organised as topics. It is possible to download a single
file, all the files of a topic or to get the text file containing all the URLS
of a topic in order to download the files from the command line. Downloadable
files gathers raw files but also processed ones so the user can perform further
analyses by himself.

\subsection{Administration tools}

The administration interface is a command line tool enabling to set up the
environment, including the database creation, to load the data and start/stop
the web server. The loading process is using standard file formats provided by
most commonly used tools. During the database upload phase, datafile format
compliance is checked. It is possible to set up a minimum environment with just
three files: a contig Fasta file, the corresponding annotation file and an
alignment file. A user made reads quantification matrix can be provided to
produce Venn diagrams or digital differential display. If this last one is
missing, the matrix will be computed using only properly paired reads. The
number of input files been potentially quiet important for large projects, a
configuration file template is provided, that can be  used as a parameter of the
loading script..

\subsection{Extensions and security}

Users with some programming knowledge can add new graphics by loading the
corresponding data in the analysis table and writing a javascript module. 

The website can be secured using the biomart Open-ID or jetty realm features.

\end{methods}


\section{Conclusion}

RNAbrowse http://bioinfo.genotoul.fr/RNAbrowse is a simple and efficient
solution to give access to RNA-Seq de novo results on the Internet. It includes
many features easing the biologists pain to analyse and extract biologically
meaningful information from the data. The installation manual, user manual and
general documentation are available on the project website.

\section*{Acknowledgement}
We would like to acknowledge all our users for providing us useful feedback on
the system and for pointing out features worth developing.

\begin{thebibliography}{}

\bibitem[Grabherr {\it et~al}., 2011]{Grabherr} Grabherr,M.G., et al. (2011)
Full-length transcriptome assembly from RNA-Seq data without a reference
genome, {\it Nat. Biotechnol}, {\bf 29}, 644-652.

\bibitem[Schulz {\it et~al}., 2012]{Schulz} Schulz, M.H., Zerbino, D.R.,
Vingron, M.and Birney, E. (2012) Oases: robust de novo RNA-seq assembly across
the dynamic range of expression levels. {\it Bioinformatics}, {\bf28},
1086-1092.

\bibitem[Pei {\it et~al}., 2012]{Pei} Pei Li, et al. (2012)
CBrowse: a SAM/BAM-based contig browser for transcriptome assembly visualization
and analysis, {\it Bioinformatics}, {\bf 28(18)}, 2382-2384.

\bibitem[Smedley {\it et~al}., 2009]{Smedley} Smedley,D., et al. (2009)
BioMart - biological queries made easy, {\it BMC Genomics}, {\bf 10}, 22.

\bibitem[Oscar {\it et~al}., 2013]{Oscar} Oscar Westesson, Mitchell Skinner
and Ian Holmes (2013) Visualizing next-generation sequencing data with Jbrowse,
{\it Briefings in bioinformatics}, {\bf 14 (2)}, 172-177.

\end{thebibliography}
\end{document}
